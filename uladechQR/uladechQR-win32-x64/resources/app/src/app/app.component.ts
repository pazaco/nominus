import { Component } from '@angular/core';
import * as moment from 'moment';
import { interval } from 'rxjs';
import { HttpClient } from '@angular/common/http';


declare var require: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  // aula = require("../assets/config.json").aula;
  aula;
  hora;
  hash;
  jsonQR;

  constructor(private http: HttpClient) {
    let  hora = moment().clone();
    http.get("c:\\program files\\dotnet\\config.json").toPromise().then(cl => {
      this.aula = cl["aula"];
      hora = moment().clone();
      this.hora = hora.format('YYYY-MM-DDTHH:mm:ss');
      this.hash = this.minutero(this.aula + '&&uladech-asistencia');
      this.jsonQR = JSON.stringify({ aula: this.aula, hora: this.hora, hash: this.hash });
    });
    const relojito = interval(60000)
      .subscribe(minuto => {
        hora = moment().clone();
        this.hora = hora.format('YYYY-MM-DDTHH:mm:ss');
        this.hash = this.minutero(this.aula + '&&uladech-asistencia');
        const oJson: any = {
          aula: this.aula,
          hora: this.hora,
          hash: this.hash
        };
        this.jsonQR = JSON.stringify(oJson);
      });
  }

  public minutero(sujeto): string {
    let hashPassword = "";

    const passwd = moment.utc().format("mmDDMM[Y]YY[T]mmHH");
    let hashSemilla = sujeto + passwd;

    // const pass1 = moment.utc().clone();
    // const passwd = pass1.format("mmDDMM[Y]YY[T]mmHH");
    // let hashSemilla = sujeto + passwd;
    const lpo =
      "ab9cK4fstv5^*dCDSxy7zAB#$%0&=TUV6OPZ1emnE2gQRWXYhjklLMNpqrFG3H8I@)J";
    let nLen = 0;
    let nSeed: number;
    let nPass: number;
    let pos = 1;
    let hPwd = "";
    let pChar: number;
    while (hashSemilla.length < 128) {
      hashSemilla = hashSemilla + hashSemilla;
    }
    while (hPwd.length < 128) {
      hPwd = hPwd + passwd;
    }
    while (pos <= passwd.length) {
      nLen += passwd.substr(pos - 1, 1).charCodeAt(0);
      pos++;
    }
    pos = 1;
    while (pos <= 128) {
      nSeed = lpo.indexOf(hashSemilla.substr(pos - 1, 1)) + 1;
      nPass = hPwd.substr(pos - 1, 1).charCodeAt(0);
      pChar = 1 + ((2 * nSeed + 5 * nPass + 3 * nLen) % 67);
      hashPassword += lpo.substr(pChar - 1, 1);
      pos++;
    }
    return hashPassword;
  }

}
