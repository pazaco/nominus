import { Component, OnInit, ViewChild } from "@angular/core";
import { NavService } from "./recursos/nav.service";
import { AuthService } from "./recursos/auth.service";
import { Router } from "@angular/router";
import { ToastrService, ToastContainerDirective } from "ngx-toastr";
import { map, tap } from "rxjs/operators";
import { environment } from "../environments/environment";
import { MessagingService } from "./recursos/messaging.service";

declare var require: any;

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent implements OnInit {
  @ViewChild(ToastContainerDirective, { static: false }) toastContainer: ToastContainerDirective;
  siMenu: any[];
  sesion: any;
  API = environment.urlAPI;
  appVersion: string = require("../../package.json").version;

  constructor(
    public navi: NavService,
    public authService: AuthService,
    private router: Router,
    public toast: ToastrService
  ) {
    authService.sesion().subscribe(rsp => (this.sesion = rsp));
  }

  ngOnInit() {
    this.navi.Menu().subscribe(rsp => {
      this.siMenu = rsp;
    });
    this.toast.overlayContainer = this.toastContainer;
    this.authService.setBrowser(navigator.userAgent);
  }
  aLog() {
    this.router.navigate(["/login"]);
  }
}
