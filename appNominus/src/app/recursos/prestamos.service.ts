import { Injectable } from '@angular/core';
import { DataService } from './data.service';
import { AuthService } from './auth.service';
import { BehaviorSubject } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { map, tap, filter } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PrestamosService {
  private __carga = new BehaviorSubject<boolean>(false);
  public carga = this.__carga.asObservable();
  private __prestamo = new BehaviorSubject<any[]>([]);
  public prestamo = this.__prestamo.asObservable();
  private token;


  constructor(private data: DataService, private auth: AuthService, private toast: ToastrService) {
    auth.sesion().subscribe(rsp => { this.token = rsp.token; });
  }

  getPrestamos(tContrato) {
    this.__carga.next(true);
    this.data.prestamos(this.token, tContrato).subscribe(_prestamo => {
      this.__prestamo.next(_prestamo);
      this.__carga.next(false);
    });
  }

}


