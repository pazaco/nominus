import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { NavService } from '../services/nav.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  _platform;

  constructor(public auth: AuthService, public nav: NavService) {
    this._platform = Object.keys(this.nav.platform);
  }

  ngOnInit() {
  }

  camposGlobal() {
    return Object.keys(this.auth.global);
  }

}
