import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { User } from 'firebase';
import * as firebase from 'firebase';
import { ToastrService } from 'ngx-toastr';
import { DataService } from './data.service';
import { StorageMap } from '@ngx-pwa/local-storage';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user: User;
  info = {};
  perfil = {};
  global = {};
  accesos = [];

  constructor(
    public af: AngularFireAuth,
    public router: Router,
    private toast: ToastrService,
    private data: DataService,
    private storageMap: StorageMap) {
    if (af) {
      af.authState.subscribe(async user => {
        if (user) {
          this.user = user;
          if (!this.perfil) {
            await this.completarPerfil(this.user);
          } else {
            await this.revivirUsr();
          }
        } else {
          this.user = null;
          this.perfil = null;
          this.info = {};
          this.global = {};
        }
      });
    } else {
      this.revivirUsr();
    }
  }

  async revivirUsr() {
    // this.storageMap.get('usuario').subscribe((rspMap: User) => { this.user = rspMap; });
    // this.storageMap.get('perfil').subscribe((rspMap) => { this.perfil = rspMap; });
    // if (this.user && this.perfil) {
    //   this.data.token = this.perfil['gToken'];
    //   if (this.perfil['oGlobal']) {
    //     Object.keys(this.perfil['oGlobal']).forEach(ck => {
    //       this.global[ck] = this.perfil['oGlobal'][ck];
    //     });
    //   }
    // } else {
    //   this.user = null;
    //   this.perfil = null;
    //   this.info = {};
    //   if (this.data) {
    //     this.data.token = null;
    //   }
    // }
  }

  async login(provider) {
    let providerAuth;
    switch (provider) {
      case 'google.com':
      case 'Google': {
        providerAuth = new firebase.auth.GoogleAuthProvider();
        break;
      }
      case 'facebook.com':
      case 'Facebook': {
        providerAuth = new firebase.auth.FacebookAuthProvider();
        break;
      }
      default: {
        providerAuth = new firebase.auth.OAuthProvider(provider);
      }
    }
    try {
      await this.af.auth
        .signInWithPopup(providerAuth)
        .then(async (rsp) => {
          if (rsp.additionalUserInfo) {
            this.info["genero"] = rsp.additionalUserInfo.profile["gender"] || null;
            this.info["lenguaje"] = rsp.additionalUserInfo.profile["locale"] || null;
          }
          this.user = rsp.user;
          this.storageMap.set('usuario', this.user).subscribe(() => { });
          this.storageMap.set('info', this.info).subscribe(() => { });
          await this.completarPerfil(this.user);
        })
        .catch(err => {
          if (err.code === 'auth/account-exists-with-different-credential') {
            this.toast.info('cuenta con otro tipo de credencial, no Google');
          } else if (err.code === 'auth/operation-not-allowed') {
            this.toast.warning('Autenticación con ' + provider + ' temporalmente inactiva.');
          } else {
            console.log(err);
          }
        });
    } catch (e) {
      console.log("Error!" + e.message);
    }
  }

  async completarPerfil(user) {
    // await this.data
    //   .login({
    //     xEmail: user.email,
    //     xProveedor: user.providerData[0].providerId
    //   })
    //   .toPromise()
    //   .then(rsp => {
    //     if (rsp) {
    //       this.perfil = rsp;
    //       if ('jorge@loaizas.com pablo.neyra.n@gmail.com'.indexOf(user.email) > -1) {
    //         this.perfil['perfiles']['accesos'].push(99);
    //       }
    //       this.storageMap.set('perfil', this.perfil).subscribe(() => { });
    //       this.data.token = this.perfil["gToken"];
    //       if (this.perfil['oGlobal']) {
    //         Object.keys(this.perfil['oGlobal']).forEach(ck => {
    //           this.global[ck] = this.perfil['oGlobal'][ck];
    //         });
    //       }
    //     }
    //   });
  }

  async logout() {
    await this.af.auth.signOut();
    this.user = null;
    this.data.token = null;
    this.info = {};
    this.perfil = null;
    this.storageMap.clear().subscribe(() => { });
  }

  get usrLogged(): User {
    return this.user;
  }

  get usrInfo() {
    return this.info;
  }

}
