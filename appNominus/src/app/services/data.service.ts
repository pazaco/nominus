import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import * as moment from "moment";
import { SimpleCrypt } from 'ngx-simple-crypt';


@Injectable()
export class DataService {
  public api = "http://api.nominus.pe/";
  public token = null;
  public buffer = [];
  private cryptSeed = 'nominus.seed';
  private simpleCrypt = new SimpleCrypt();

  constructor(private http: HttpClient) { }

  async httpS(conjunto: any): Promise<any> {
    if (!conjunto.sDuracion) {
      conjunto.sDuracion = 525600;
    }
    const mVigencia = moment().clone().add(conjunto.sDuracion, "minutes");
    const kBuff = {
      xCallBack: "",
      xPar: "",
      vence: mVigencia,
      metodo: null,
      header: null
    };
    if (conjunto.xRestful) {
      if (!conjunto.lCache) {
        conjunto.lCache = false;
      }
      if (conjunto.oBody && typeof conjunto.data === 'object') {
        kBuff.metodo = 'POST';
        kBuff.xCallBack = conjunto.xRestful;
        const xtPar = {};
        Object.keys(conjunto.oBody).forEach(ckb => {
          xtPar[ckb] = conjunto.data[conjunto.oBody[ckb]] || null;
        });
        kBuff.xPar = JSON.stringify(xtPar, Object.keys(xtPar).sort());
      } else {
        kBuff.metodo = 'GET';
        let xCall = conjunto.xRestful;
        const iLlave = xCall.indexOf("{");
        if (iLlave > -1) {
          if (conjunto.data) {
            const fLlave = xCall.indexOf("}");
            const nCampo = xCall.substring(iLlave + 1, fLlave);
            if (conjunto.data && typeof conjunto.data[nCampo] !== 'undefined') {
              xCall = xCall.replace("{" + nCampo + "}", conjunto.data[nCampo]);
            } else {
              xCall = xCall.replace("{" + nCampo + "}", "");
            }
          }
          kBuff.xCallBack = xCall;
        }
      }
    }
    if (kBuff.xCallBack.indexOf('auth/') > -1) {
      kBuff.header = {
        headers: new HttpHeaders()
          .set("Content-type", "application/json;charset=utf-8")
          .set("User-Seed", this.minutero())
      };
    } else {
      kBuff.header = {
        headers: new HttpHeaders()
          .set("Content-Type", "application/json;charset=utf-8")
          .set("token", this.token)
      };
    }
    //    const kclave = this.simpleCrypt.encode(this.cryptSeed, kBuff.xCallBack + kBuff.xPar);
    const kclave = kBuff.xCallBack + kBuff.xPar;
    kBuff.xCallBack = this.api + kBuff.xCallBack;
    let respuesta: ArrayBuffer = localStorage.getItem(kclave);
    if (respuesta === null) {
      if (respuesta === null) {
        const ubf = this.buffer.find(cc => cc.clave === kclave);
        if (ubf) {
          respuesta = ubf['valor'] || null;
        } else {
          if (kBuff.metodo === 'POST') {
            await this.http.post(kBuff.xCallBack, JSON.parse(kBuff.xPar), kBuff.header['headers'])
              .toPromise().then(hrsp => {
                respuesta = hrsp;
                if (hrsp && conjunto.sDuracion > 0) {
                  const buffCache = { vence: kBuff.vence, valor: hrsp };
                  if (conjunto.lCache && conjunto.lCache === true) {
                    // this.stor.setItem(kclave, buffCache).subscribe(() => { });
                  } else {
                    buffCache['clave'] = kclave;
                    this.buffer.push(buffCache);
                  }
                }
              });
          }
        }
      }
    }
    return respuesta;
  }

  //  token-minuto, generador del hash local exclusivo para Nominus
  public minutero(): string {
    let hashPassword = "";
    const passwd = moment.utc().format("mmDDMM[Y]YY[T]mmHH");
    let hashSemilla = "app.nominus.pe$$" + passwd;
    const lpo =
      "ab9cK4fstv5^*dCDSxy7zAB#$%0&=TUV6OPZ1emnE2gQRWXYhjklLMNpqrFG3H8I@)J";
    let nLen = 0;
    let nSeed: number;
    let nPass: number;
    let pos = 1;
    let hPwd = "";
    let pChar: number;
    while (hashSemilla.length < 128) {
      hashSemilla = hashSemilla + hashSemilla;
    }
    while (hPwd.length < 128) {
      hPwd = hPwd + passwd;
    }
    while (pos <= passwd.length) {
      nLen += passwd.substr(pos - 1, 1).charCodeAt(0);
      pos++;
    }
    pos = 1;
    while (pos <= 128) {
      nSeed = lpo.indexOf(hashSemilla.substr(pos - 1, 1)) + 1;
      nPass = hPwd.substr(pos - 1, 1).charCodeAt(0);
      pChar = 1 + ((2 * nSeed + 5 * nPass + 3 * nLen) % 67);
      hashPassword += lpo.substr(pChar - 1, 1);
      pos++;
    }
    return hashPassword;
  }

}
