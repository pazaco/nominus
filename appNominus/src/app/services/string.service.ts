import { Injectable } from "@angular/core";

@Injectable()
export class StringService {

  constructor() { }

  digrafoIgual(s1: string, s2: string): boolean {
    return this.puro(s1) === this.puro(s2);
  }

  digrafoContiene(s1: string, s2: string): boolean {
    return this.puro(s1).indexOf(this.puro(s2)) !== -1;
  }

  reemplazos(plantilla: string, objeto: any): string {
    if (!plantilla) {
      plantilla = '';
    }
    let rsp = plantilla;
    if (objeto) {
      let reits = 20;
      while (rsp.indexOf('${') !== -1 && reits > 0) {
        reits--;
        const xDesde = rsp.indexOf('${');
        const xHasta = rsp.indexOf('}', xDesde);
        if (xHasta !== -1) {
          const cNombreCampo = rsp.substring(xDesde + 2, xHasta);
          if (objeto[cNombreCampo]) {
            rsp = rsp.replace('${' + cNombreCampo + '}', objeto[cNombreCampo] || '');
          }
        }
      }
    } else {
      rsp = '';
    }
    if(rsp.indexOf('${') !== -1){
      rsp = '';
    }
    return rsp;
  }

  leadZeros(valor: number, zeros: number): string {
    let ct = valor.toString();
    return ct.length >= zeros ? ct : '0'.repeat(zeros - ct.length) + ct;
  }

  jsonValido(jsonStr: string): boolean {
    try {
      return (JSON.parse(jsonStr) && !!jsonStr);
    }
    catch (e) {
      return false;
    }
  }

  private puro(str: string): string {
    return str
      .toLowerCase()
      .replace('á', 'a')
      .replace('é', 'e')
      .replace('í', 'i')
      .replace('ó', 'ó')
      .replace('ú', 'u')
      .replace('ü', 'u')
      .replace('ñ', 'n')
  }

  traducirCampos(cadena): any[] {
    let campos = [];
    let nPivot = cadena.indexOf("{");
    let uPivot: number;
    while (nPivot !== -1) {
      uPivot = cadena.indexOf("}", nPivot);
      let cPrefijo = nPivot > 0 ? cadena.substr(nPivot - 1) : "?";
      if ("$#%".indexOf(cPrefijo) === -1) { cPrefijo = ""; }
      campos.push({
        cCampo: cadena.substring(nPivot + 1, uPivot),
        xCadena: cPrefijo + "{" + cadena.substring(nPivot + 1, uPivot) + "}"
      });
      nPivot = cadena.indexOf("{", uPivot);
    }
    return campos;
  }

  reemplazarCampos(cadena, valor): string {
    let kCade = cadena;
    let nPivot = cadena.indexOf("{");
    let uPivot: number;
    while (nPivot !== -1) {
      uPivot = cadena.indexOf("}", nPivot);
      let cPrefijo = nPivot > 0 ? cadena.substr(nPivot - 1) : "?";
      if ("$#%".indexOf(cPrefijo) === -1) { cPrefijo = ""; }
      const fRepl = {
        cCampo: cadena.substring(nPivot + 1, uPivot),
        xCadena: cPrefijo + "{" + cadena.substring(nPivot + 1, uPivot) + "}"
      };
      kCade = kCade.replace(fRepl.xCadena, valor[fRepl.cCampo]);
      nPivot = cadena.indexOf("{", uPivot);
    }
    return kCade;
  }

}
