import { Component, OnInit } from "@angular/core";
import { AuthService } from "../recursos/auth.service";
import { DataService } from "../recursos/data.service";
import { PrestamosService } from "../recursos/prestamos.service";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-prestamos",
  templateUrl: "./prestamos.component.html",
  styleUrls: ["./prestamos.component.css"]
})
export class PrestamosComponent implements OnInit {
  selContrato;
  _excepcional;
  iniciando = true;
  selPrestamo;
  colsPrestamo = ["iPrestamo", "dPrestamo", "mValor", "mPagos", "mSaldo"];
  colsPagos = ["dFecha", "mValor", "iCalculoPlanilla"];
  cuotas = [];

  constructor(public authService: AuthService, public prestamos: PrestamosService, private router: Router, private toast: ToastrService) { }

  ngOnInit() {
    this.authService.sesion().subscribe(ses => {
      if (ses) {
        if (ses.perfiles.length === 0) {
          this._excepcional = "Su correo no se encuentra asociado a ningún perfil de ninguna de las empresas registradas en nuestro sistema.  De acuerdo a Ley, usted debe registrar su correo " + ses.usuario.email + " en la empresa donde labora.";
          this.iniciando = false;
        } else {
          if (ses.perfiles.indexOf(4) === -1) {
            this._excepcional = "No tiene permisos o no se encontraron prestamos.";
            this.iniciando = false;
          } else {
            this.authService.dropContratos.subscribe(rsp => {
              this.selContrato = rsp.filter(cco => cco.lVigente)[0].zContrato;
              this.chContrato();
              this.iniciando = false;
            });
          }
        }
      } else {
        this.toast.warning("Debe tener una sesión abierta para ver préstamos.");
        this.router.navigateByUrl("/login");
        this.iniciando = false;
      }
    });
  }

  chContrato() {
    const tParContrato = { sServidor: this.selContrato % 100, iContrato: Math.floor(this.selContrato / 100) };
    this.prestamos.getPrestamos(tParContrato);
    this.selPrestamo = null;
    this.cuotas = [];
  }

  chPrestamo(prestamo) {
    this.selPrestamo = prestamo.iPrestamo;
    this.cuotas = prestamo.pagos;
  }

}
