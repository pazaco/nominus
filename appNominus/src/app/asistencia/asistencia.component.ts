import { Component, OnInit, HostBinding } from "@angular/core";
import { AuthService } from "../recursos/auth.service";
import { DataService } from "../recursos/data.service";

@Component({
  selector: "app-asistencia",
  templateUrl: "./asistencia.component.html",
  styleUrls: ["./asistencia.component.css"]
})
export class AsistenciaComponent implements OnInit {
  asistencia: any[];
  cBusy = false;
  dBusy = false;

  constructor(public authService: AuthService, public data: DataService) {}

  ngOnInit() {}
}
