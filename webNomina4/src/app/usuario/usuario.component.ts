import { Component, OnInit, HostBinding, OnDestroy } from "@angular/core";
import { AuthService } from "../recursos/auth.service";
import { DataService } from "../recursos/data.service";
import { Subscription } from "rxjs";
import * as moment from "moment";

@Component({
  selector: "app-usuario",
  templateUrl: "./usuario.component.html",
  styleUrls: ["./usuario.component.css"]
})
export class UsuarioComponent implements OnInit, OnDestroy {
  sesion;
  private _sesion: Subscription;
  cBusy = false;

  constructor(public authService: AuthService, public data: DataService) {}

  ngOnInit() {
    this.cBusy = true;
    this._sesion = this.authService.sesion().subscribe(
      rsp => {
        this.sesion = rsp;
        this.cBusy = false;
      },
      error => {
        this.cBusy = false;
      }
    );
  }

  ngOnDestroy() {
    this._sesion.unsubscribe();
  }

  listaContratos() {
    if (this.sesion) {
      const lstE = [];
      this.sesion.contratos.filter(cse => {
        cse.empresas.filter(cem => {
          cem.contratos.filter(ctt => {
            lstE.push({
              sServidor: cse.sServidor,
              sEmpresa: cem.sEmpresa,
              xAbreviado: cem.xAbreviado,
              xRazonSocial: cem.xRazonSocial,
              jImagen: "data:image/png;base64," + cem.jImagen,
              iContrato: ctt.iContrato,
              sCargo: ctt.sCargo,
              xCargo: ctt.xCargo,
              dDesde: ctt.dDesde,
              dHasta: ctt.dHasta,
              lVigente: ctt.dHasta == null || moment().isBefore(ctt.dHasta)
            });
          });
        });
      });
      return lstE;
    } else {
      return [];
    }
  }
}
