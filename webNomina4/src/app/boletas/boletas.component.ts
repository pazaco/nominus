import { Component, OnInit, OnDestroy } from "@angular/core";
import { Subscription } from "rxjs";
import { DataService } from "../recursos/data.service";
import { AuthService } from "../recursos/auth.service";
// import { ReactiveFormsModule } from '@angular/forms';
// import { take, map, tap, switchMap } from "rxjs/operators";
// import { QRCodeComponent } from "angular2-qrcode";

@Component({
  selector: "app-boletas",
  templateUrl: "./boletas.component.html",
  styleUrls: ["./boletas.component.css"]


})
export class BoletasComponent implements OnInit, OnDestroy {
  tCalculo: any;
  calculos: any[];
  calculo: any;
  selCalculo: number;
  boleta: any;
  contratos: any[] = [];
  contrato: any;
  selContrato: number;
  periodos: string[];
  xPeriodo: string;
  parciales: any[];
  private primerSel = true;
  _boletas: Subscription;
  _sesion: Subscription;
  private token;

  constructor(public authService: AuthService, public data: DataService) { }

  ngOnInit() {

    if (this.data.getCalculoPlanilla() !== 0) {
      const oCalc = this.data.getCalculoPlanilla();
      this.data.setCalculoPlanilla(0);
      this.tCalculo = { iCalculoPlanilla: Math.floor(oCalc / 100), sServidor: oCalc % 100 };
    } else {
      this.tCalculo = null;
    }
    console.log(this.tCalculo);
    this._sesion = this.authService.sesion().subscribe(ses => {
      if (ses) {
        if (ses.contratos.length > 0) {
          ses.contratos.filter(cse => {
            cse.empresas.filter(cem => {
              cem.contratos.filter(cct => {
                this.contratos.push({
                  iContrato: cct.iContrato,
                  xContrato: cct.xCargo + " /" + cem.xAbreviado,
                  sServidor: cse.sServidor,
                  zContrato: cct.iContrato * 100 + cse.sServidor
                });
              });
            });
          });
          this.token = ses.token;
          this._boletas = this.data.boletasIndice(this.token).subscribe(bol => {
            this.calculos = bol;
            if (this.calculos.length > 0) {
              this.calculos.filter(
                ccal => {
                  ccal.zCalculo = ccal.iCalculoPlanilla * 100 + ccal.sServidor;
                }
              );
              if (this.tCalculo !== null) {
                this.calculo = this.calculos.filter( ccal => ccal.iCalculoPlanilla === this.tCalculo.iCalculoPlanilla && ccal.sServidor === this.tCalculo.sServidor )[0];
              }
              if (!this.calculo) {
                this.calculo = this.calculos[0];
              }
              console.log('clcs', this.calculo);
              this.contrato = this.contratos.filter(
                cct =>
                  cct.iContrato === this.calculo.iContrato &&
                  cct.sServidor === this.calculo.sServidor
              )[0];
              this.selCalculo = this.calculo.zCalculo;
              this.selContrato = this.contrato.zContrato;
              this.chContrato();
            }
          });
        }
      }
    });
  }

  ngOnDestroy() {
    if (this._boletas) {
      this._boletas.unsubscribe();
      if (this._sesion) {
        this._sesion.unsubscribe();
      }
    }
  }

  toggle(cpar) {
    if (document.getElementById(cpar + "Cn").classList.contains("show")) {
      document.getElementById(cpar + "Ico").classList.remove("fa-rotate-180");
    } else {
      document.getElementById(cpar + "Ico").classList.add("fa-rotate-180");
    }
  }

  chContrato() {
    const zContrato = Math.floor(this.selContrato);
    this.periodos = [];
    this.contrato = this.contratos.filter(
      ctt => ctt.zContrato === zContrato
    )[0];
    this.calculos.filter(c => {
      if (
        c.iContrato === this.contrato.iContrato &&
        c.sServidor === this.contrato.sServidor &&
        this.periodos.indexOf(c.iAnoMes) === -1
      ) {
        this.periodos.push(c.iAnoMes);
        if (this.primerSel && c.zCalculo === this.selCalculo ) {
          this.xPeriodo = c.iAnoMes;
        }
      }
    });
    if (this.periodos.indexOf(this.xPeriodo) === -1) {
      this.xPeriodo = this.periodos[0];
    }
    this.chPeriodo();
  }

  chPeriodo() {
    this.parciales = [];
    const xPer = +this.xPeriodo;
    this.calculos.filter(ccal => {
      if (
        this.contrato.iContrato === ccal.iContrato &&
        this.contrato.sServidor === ccal.sServidor &&
        xPer === ccal.iAnoMes
      ) {
        this.parciales.push(ccal);
        if (this.primerSel && ccal.zCalculo === this.selCalculo ) {
          this.calculo = ccal;
        }
      }
    });
    if (!this.calculo || this.parciales.indexOf(this.calculo.zCalculo) === -1) {
      this.calculo = this.parciales[0];
    }
    this.selCalculo = this.calculo.zCalculo;
    this.chParcial();
  }

  chParcial() {
    this.primerSel = false;
    const zCalc = Math.floor(this.selCalculo);
    this.calculo = this.calculos.filter(cc => cc.zCalculo === zCalc)[0];
    this.data.printBoleta(this.token, this.calculo).subscribe(rsp => {
      this.boleta = rsp;
    }, exc => {
    });
  }

  sumaMonto(e) {
    let suma = 0;
    e.filter(cada => {
      suma += cada.mMonto;
    });
    return suma;
  }
}
