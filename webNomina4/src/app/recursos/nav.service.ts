import { BehaviorSubject, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { MathService } from './math.service';
import { Router } from '@angular/router';

export const MENU = [
  { id: 'hom', menu: 'Notificaciones', icono: {base: 'bell'}, link: '/home'},
  { id: 'bol', menu: 'Boletas', icono: {base: 'cash'}, link: '/boletas'},
  { id: 'asi', menu: 'Asistencia', icono: { base: 'clock' }, link: '/asistencia'},
  { id: 'vac', menu: 'Vacaciones', icono: { base: 'beach' }, link: '/vacaciones'},
  { id: 'prs', menu: 'Préstamos', icono: {base: 'cash-usd'}, link: '/prestamos'},
  { id: 'usr', menu: 'Contratos', icono: { base: 'account-box-multiple' }, link: '/usuario'},
  {id: 'hlp', menu: 'Ayuda', icono: {base: 'help-box' }, link: '/ayuda' }
];

@Injectable()
export class NavService {
  public mn = MENU;
  private tamanho = 'mini';

  private menu: any[] = this._defineMenu([]);
  private __menu = new BehaviorSubject<any[]>(this.menu);
  private __tamanho = new BehaviorSubject<string>('mini');

  constructor(private math: MathService, private router: Router) { }

  Menu(): Observable<any[]> {
    return this.__menu.asObservable();
  }

  Tamanho(): Observable<string> {
    return this.__tamanho.asObservable();
  }

  defineMenu(perfil: any[]) {
    this.menu = this._defineMenu(perfil);
    this.__menu.next(this.menu);
  }

  crumbs() {
    const routeActivo = this.router.routerState.snapshot.url;
    const oRsp = { menu: null, opcion: null, icono: null };
    let menuAct = this.menu.filter(cmn => cmn.link === routeActivo);
    if (menuAct.length > 0) {
      oRsp.menu = "";
      oRsp.opcion = "";
      oRsp.icono = menuAct[0].icono;
    } else {
      menuAct = this.menu.filter(cmn => cmn.id === routeActivo.substring(1, 4));
      if (menuAct.length > 0) {
      const subMenu = menuAct[0].submenu.filter(csm => csm.link === routeActivo);
      if (subMenu.length > 0) {
        oRsp.menu = menuAct[0].menu;
        oRsp.opcion = subMenu[0].opcion;
        oRsp.icono = subMenu[0].icono;
      }}
    }
    return oRsp;
  }

  crumb() {
    const routeActivo = this.router.routerState.snapshot.url;
    const oRsp = { menu: null, icono: null };
    const menuAct = this.mn.filter(cmn => cmn.link === routeActivo)[0];
    return {menu: menuAct.menu, icono: menuAct.icono};
  }

  colapsarMinis() {
    this.menu.filter(csm => csm.miniEx = 'no');
    this.__menu.next(this.menu);
  }

  private _defineMenu(perfil: any[]): any[] {
    return this.mn;
  }

  toggle() {
    switch (this.tamanho) {
      case 'maxi': this.tamanho = 'nada'; break;
      case 'nada': this.tamanho = 'mini'; break;
      case 'mini': this.tamanho = 'maxi'; break;
    }
    this.__tamanho.next(this.tamanho);
  }

  swipeLeft() {
    if (this.tamanho === 'mini') {
      this.tamanho = 'nada';
    }
    if (this.tamanho === 'maxi') {
      this.tamanho = 'mini';
    }
    this.__tamanho.next(this.tamanho);
  }

  swipeRight() {
    if (this.tamanho === 'mini') {
      this.tamanho = 'maxi';
    }
    if (this.tamanho === 'nada') {
      this.tamanho = 'mini';
    }
    this.__tamanho.next(this.tamanho);
  }

  toggleVisible() {
    this.tamanho = this.tamanho !== 'maxi' ? 'maxi' : 'mini';
    this.__tamanho.next(this.tamanho);
  }

}

