import { Injectable, Directive, forwardRef, Attribute } from "@angular/core";
import { Observable, BehaviorSubject } from "rxjs";
import { SimpleCrypt } from "ngx-simple-crypt";
import { HttpHeaders, HttpClient } from "@angular/common/http";
import { LocalStorage } from "@ngx-pwa/local-storage";

@Injectable()
export class LoginService {
  public sesion: any;
  private key = "remunerarte.com-sess";
  public apis = { general: "https://api.remunerarte.com/" };
  constructor(private http: HttpClient, protected localStorage: LocalStorage) {
    // this.getConfig().subscribe(rsp => {
    //   this.apis = rsp.apis;
    // });
  }

  private __sesion = new BehaviorSubject<any>(null);

  public getHeaders(): any {
    if (this.sesion != null) {
      return {
        headers: new HttpHeaders()
          .set("content-type", "application/json;charset=utf-8")
          .set("sesionPersona", this.sesion.token)
      };
    } else {
      return {
        headers: new HttpHeaders().set(
          "content-type",
          "application/json;charset=utf-8"
        )
      };
    }
  }

  Sesion(): Observable<any> {
    return this.__sesion.asObservable();
  }

  // getConfig(): Observable<any> {
  //   return this.http.get("../../assets/config.json");
  // }

  public getApi(cad) {
    return this.apis[cad];
  }

  public getUsuario() {
    return this.sesion.avatar;
  }

  public getAvatarRegistro(registro: string) {
    return this.http.get(
      this.apis.general + "auth/avatarRegistro/" + registro,
      this.getHeaders()
    );
  }

  public getSesion() {
    return this.sesion;
  }

  public getClaims() {
    return this.sesion.claims || [];
  }

  public putSesion(ses: any) {
    this.sesion = ses;
    this.__sesion.next(this.sesion);
  }

  public putLogin(login: any): Observable<any> {
    return this.http.post(
      this.apis.general + "auth/login",
      login,
      this.getHeaders()
    );
  }

  public cambiarPassword(pass: any): Observable<any> {
    return this.http.post(
      this.apis.general + "auth/cambiaPassword",
      pass,
      this.getHeaders()
    );
  }

  public cerrarSesion() {
    this.localStorage.clear().subscribe(() => {});
    this.sesion = null;
    this.__sesion.next(this.sesion);
  }

  public storage() {
    if (this.sesion == null) {
      const simpleCrypt = new SimpleCrypt();
      let cryptoSess: any;
      this.localStorage.getItem<string>("pwaSesion").subscribe(sesion => {
        cryptoSess = sesion;
      });
      if (cryptoSess != null) {
        this.sesion = JSON.parse(simpleCrypt.decode(this.key, cryptoSess));
        this.__sesion.next(this.sesion);
      }
    }
  }

  public registrarSesion(ses: any) {
    this.putSesion(ses);
    const simpleCrypt = new SimpleCrypt();
    const cryptoSess = simpleCrypt.encode(this.key, JSON.stringify(ses));
    this.localStorage.setItem("pwaSesion", cryptoSess).subscribe(() => {});
  }
}

export interface Logged {
  token: string;
  respuesta: string;
  claims: Claim[];
  avatar: Avatar;
}

export interface Claim {
  iUsuario: number;
  cClaim: string;
  xValor: any;
}

export interface Avatar {
  iPersona: number;
  xTip: string;
  xDocIdentidad: string;
  xUsuario: string;
  jFoto: any;
  cGenero: string;
}
