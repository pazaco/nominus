import { Component, OnInit, HostBinding } from '@angular/core';
import { AuthService } from '../recursos/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, Validators, FormControl } from '@angular/forms';
// import { SnotifyService } from 'ng-snotify';
import { SimpleCrypt } from 'ngx-simple-crypt';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  logForm: FormGroup;
  regForm: FormGroup;
  chpForm: FormGroup;
  valForm: FormGroup;
  esLogin = true;
  hide = true;

  constructor(public authService: AuthService, private router: Router, private route: ActivatedRoute, localStorage: LocalStorage, public toast: ToastrService) {
  }

  ngOnInit() {
    const regxEmail = /^[\w!#$%&'*+\-/=?\^_`{|}~]+(\.[\w!#$%&'*+\-/=?\^_`{|}~]+)*@((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))$/;
    this.logForm = new FormGroup({
      email: new FormControl('', [
        Validators.required,
        Validators.pattern(regxEmail)
      ]),
      password: new FormControl('', [
        Validators.minLength(6),
        Validators.required
      ])
    });
    this.regForm = new FormGroup({
      email: new FormControl('', [
        Validators.required,
        Validators.pattern(regxEmail)
      ])
    });

  }

  onSubmitLogin() {
    this.authService.loginEmail(this.logForm.value.email, this.logForm.value.password);
   }

  onSubmitRegistro() {
    this.authService.solicitarEmail(this.regForm.value.email);
    this.regForm.value.email = '';
    this.router.navigate(['/home']);
  }

  onClickGoogleLogin() {
    this.authService.loginGoogle();
    this.authService.user.subscribe(rUsr => {
      if (rUsr) {
        this.router.navigate(['/home']);
      }
    });
  }

  onClickFacebookLogin() {
    this.authService.loginFacebook();
    this.authService.user.subscribe(rUsr => {
      if (rUsr) {
        this.router.navigate(['/home']);
      }
    });
  }

  cerrarSesion() {
    this.authService.logout();
  }

  toggleLogin() {
    this.esLogin = !this.esLogin;
  }

  getLogEmail() {
    return this.logForm.get('email').hasError("required") ? "debe digitar un correo" :
    this.logForm.get('email').hasError("pattern") ? "no es un correo válido" :
     "";
  }

  getLogPassword() {
    return this.logForm.get('password').hasError("required") ? "debe digitar una contraseña" :
    this.logForm.get('password').hasError("minlength") ? "mínimo 6 teclas" :
     "";
  }

  getRegEmail() {
    return this.regForm.get('email').hasError("required") ? "debe digitar un correo" :
    this.regForm.get('email').hasError("pattern") ? "no es un correo válido" :
     "";
  }

}
