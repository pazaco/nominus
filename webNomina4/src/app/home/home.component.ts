import { Component, OnInit, HostBinding, OnDestroy } from "@angular/core";
import { AuthService } from "../recursos/auth.service";
import { Subscription } from "rxjs";
import { DataService } from "../recursos/data.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"]
})
export class HomeComponent implements OnInit, OnDestroy {
  // private sesion_: Subscription;
  notifica: any[];
  _notifica: Subscription;
  _sesion: Subscription;
  token: string;

  constructor(public authService: AuthService, public data: DataService, private router: Router) { }

  ngOnInit() {
    this._sesion = this.authService.sesion().subscribe(ses => {
      if (ses) {
        this.token = ses.token;
        const iconos = ['cash', 'beach', 'clock', 'bulletin-board'];
        const links = ['/boletas', '/vacaciones', '/asistencia', '/board'];
        this._notifica = this.data.notificaciones(this.token).subscribe(rsp => {
          this.notifica = rsp;
          this.notifica.filter(cno => {
            cno.xIcono = iconos[cno.bTipoNotificacion - 1];
            cno.xLink = links[cno.bTipoNotificacion - 1];
            ses.contratos.filter(c1 => {
              if (c1.sServidor === 1) {
                c1.empresas.filter(c2 => {
                  c2.contratos.filter(c3 => {
                    if (c3.iContrato === cno.iContrato) {
                      cno.sEmpresa = c2.sEmpresa;
                      cno.jImagen = "data:image/png;base64," + c2.jImagen;
                    }
                  });
                });
              }
            });
          });
          console.log(this.notifica);
        });
      }
    });
  }

  ngOnDestroy() {
    if (this._notifica) {
      this._notifica.unsubscribe();
      this._sesion.unsubscribe();
    }
  }

  lanzarUrl(oj) {
    if (oj.bTipoNotificacion === 1) {
      this.data.setCalculoPlanilla(oj.iReferencia * 100 + oj.sServidor);
      this.router.navigateByUrl("/boletas");
    }
  }
}
