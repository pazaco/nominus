import { Component, OnInit } from "@angular/core";
import { AuthService } from "../recursos/auth.service";
import { DataService } from "../recursos/data.service";

@Component({
  selector: "app-prestamos",
  templateUrl: "./prestamos.component.html",
  styleUrls: ["./prestamos.component.css"]
})
export class PrestamosComponent implements OnInit {
  prestamos: any[];
  public cBusy = false;
  public dBusy = false;

  constructor(public authService: AuthService, public data: DataService) {}

  ngOnInit() {}
}
