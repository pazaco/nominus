import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BoletasComponent } from './boletas/boletas.component';
import { AsistenciaComponent } from './asistencia/asistencia.component';
import { VacacionesComponent } from './vacaciones/vacaciones.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './recursos/auth.guard';
import { UsuarioComponent } from './usuario/usuario.component';
import { RegistroComponent } from './registro/registro.component';
import { PrestamosComponent } from './prestamos/prestamos.component';
import { HelpComponent } from './help/help.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'boletas', component: BoletasComponent, canActivate: [AuthGuard]  },
  { path: 'asistencia', component: AsistenciaComponent, canActivate: [AuthGuard]  },
  { path: 'vacaciones', component: VacacionesComponent, canActivate: [AuthGuard] },
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent, },
  { path: 'registro', component: RegistroComponent },
  { path: 'registro/:registro', component: RegistroComponent },
  { path: 'prestamos', component: PrestamosComponent, canActivate: [AuthGuard]},
  { path: 'ayuda', component: HelpComponent },
  { path: 'usuario', component: UsuarioComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
