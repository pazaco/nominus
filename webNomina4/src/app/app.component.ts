import { Component, OnInit, ViewChild } from '@angular/core';
import { NavService } from './recursos/nav.service';
import { AuthService } from './recursos/auth.service';
import { Router } from '@angular/router';
import { ToastrService, ToastContainerDirective } from 'ngx-toastr';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  @ViewChild(ToastContainerDirective) toastContainer: ToastContainerDirective;
  siMenu: any[];

  constructor(public navi: NavService, public authService: AuthService, private router: Router, public toast: ToastrService) { }

  ngOnInit() {
    this.siMenu = this.navi.mn;
    this.toast.overlayContainer = this.toastContainer;
  }

  aLog() {
    this.router.navigate(['/login']);
  }
}
