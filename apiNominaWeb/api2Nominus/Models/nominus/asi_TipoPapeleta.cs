﻿using System;
using System.Collections.Generic;

namespace api2Nominus.Models.nominus
{
    public partial class asi_TipoPapeleta
    {

        public byte bTipoPapeleta { get; set; }
        public string xTipoPapeleta { get; set; }
        public bool? lGoceHaberes { get; set; }
        public bool? lAutorizacionJefe { get; set; }

    }
}
