﻿using System;
using System.Collections.Generic;

namespace api2Nominus.Models.nominus
{
    public partial class webNotificacion
    {

        public int iNotificacion { get; set; }
        public byte? bTipoNotificacion { get; set; }
        public int? iReferencia { get; set; }
        public int? iContrato { get; set; }
        public DateTime? dEnvio { get; set; }
        public string xAsunto { get; set; }
        public int? iUrl { get; set; }

    }
}
