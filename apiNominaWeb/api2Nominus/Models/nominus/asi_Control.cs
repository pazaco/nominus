﻿using System;
using System.Collections.Generic;

namespace api2Nominus.Models.nominus
{
    public partial class asi_Control
    {
        public short bControl { get; set; }
        public string xControl { get; set; }
        public string cTipoControl { get; set; }
        public short? bControlPrevio { get; set; }
        public TimeSpan? cHora { get; set; }
        public short? sDuracion { get; set; }
        public bool? lMarcaQR { get; set; }
    }
}
