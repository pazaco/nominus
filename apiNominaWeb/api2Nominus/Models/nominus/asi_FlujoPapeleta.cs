﻿using System;
using System.Collections.Generic;

namespace api2Nominus.Models.nominus
{
    public partial class asi_FlujoPapeleta
    {
        public byte bTipoPapeleta { get; set; }
        public int iPapeleta { get; set; }
        public int iConsecutivo { get; set; }
        public string cTipoFlujo { get; set; }
        public DateTime? dFlujo { get; set; }
        public int? iEmpleado { get; set; }
        public string xObservacion { get; set; }
    }
}
