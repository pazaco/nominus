﻿using System;
using System.Collections.Generic;

namespace api2Nominus.Models.nominus
{
    public partial class asi_Flujo
    {
         public int iAsistencia { get; set; }
        public short bControl { get; set; }
        public DateTime dHoraHorario { get; set; }
        public DateTime? dHoraControl { get; set; }
        public int? iMarcacion { get; set; }
        public short? sIncidencia { get; set; }

    }
}
