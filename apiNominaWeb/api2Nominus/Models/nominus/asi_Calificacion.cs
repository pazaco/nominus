﻿using System;
using System.Collections.Generic;

namespace api2Nominus.Models.nominus
{
    public partial class asi_Calificacion
    {
        public short bControl { get; set; }
        public short sCalificador { get; set; }
        public short? sDiferencia { get; set; }
        public byte? bControlPasivo { get; set; }
        public byte? bControlActivo { get; set; }
        public bool? lNoEstablecimiento { get; set; }
        public short? sIncidencia { get; set; }

    }
}
