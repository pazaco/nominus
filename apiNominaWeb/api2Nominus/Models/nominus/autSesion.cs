﻿using System;
using System.Collections.Generic;

namespace api2Nominus.Models.nominus
{
    public partial class autSesion
    {
        public Guid gSesion { get; set; }
        public int iUsuario { get; set; }
        public string cClaim { get; set; }
        public DateTime? dVigencia { get; set; }
        public short? sNavegador { get; set; }

    }
}
