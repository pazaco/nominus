﻿using System;
using System.Collections.Generic;

namespace api2Nominus.Models.nominus
{
    public partial class asi_Jefatura
    {
        public int iContrato { get; set; }
        public int iContratoJefe { get; set; }
        public DateTime? dVigente { get; set; }
        public byte? bTipoJefe { get; set; }
    }
}
