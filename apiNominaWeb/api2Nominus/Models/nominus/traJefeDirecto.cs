﻿using System;
using System.Collections.Generic;

namespace api2Nominus.Models.nominus
{
    public partial class traJefeDirecto
    {
        public int iJefeDirecto { get; set; }
        public int iContrato { get; set; }
        public int iTrabajador { get; set; }
    }
}
