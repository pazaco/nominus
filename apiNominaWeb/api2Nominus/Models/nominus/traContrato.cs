﻿using System;
using System.Collections.Generic;

namespace api2Nominus.Models.nominus
{
    public partial class traContrato
    {
        public int iContrato { get; set; }
        public int iTrabajador { get; set; }
        public short sEmpresa { get; set; }
        public short sSede { get; set; }
        public short sCargo { get; set; }

    }
}
