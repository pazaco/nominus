﻿using System;
using System.Collections.Generic;

namespace api2Nominus.Models.nominus
{
    public partial class vozTrack
    {

        public int iTrack { get; set; }
        public int? iPersona { get; set; }
        public byte? bFrase { get; set; }
        public bool? lAbierto { get; set; }

    }
}
