﻿using System;
using System.Collections.Generic;

namespace api2Nominus.Models.nominus
{
    public partial class vozFingerPrint
    {
        public int iFingerPrint { get; set; }
        public int? iTrack { get; set; }
        public string xSoporte { get; set; }
        public string xFingerPrint { get; set; }
    }
}
