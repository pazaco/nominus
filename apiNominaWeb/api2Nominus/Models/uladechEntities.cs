﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using api2Nominus.Models.uladech;
using System.Configuration;

namespace api2Nominus.Models
{
    public partial class uladechEntities : DbContext
    {
        public uladechEntities()
        {
        }

        public uladechEntities(DbContextOptions<uladechEntities> options)
            : base(options)
        {
            Database.SetCommandTimeout(10000);
        }

        public virtual DbSet<DEPARTMENTS> DEPARTMENTS { get; set; }
        public virtual DbSet<TEMPLATE> TEMPLATE { get; set; }
        public virtual DbSet<USERINFO> USERINFO { get; set; }
        public virtual DbSet<acc_levelset> acc_levelset { get; set; }
        public virtual DbSet<acc_levelset_emp> acc_levelset_emp { get; set; }
        public virtual DbSet<acc_monitor_log> acc_monitor_log { get; set; }
        public virtual DbSet<personnel_area> personnel_area { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {

                string cs = ConfigurationManager.ConnectionStrings["uladechEntities"].ConnectionString;
                optionsBuilder.UseSqlServer(cs);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.4-servicing-10062")
                .HasAnnotation("Relational:DefaultSchema", "radical_uuladech");

            modelBuilder.Entity<DEPARTMENTS>(entity =>
            {
                entity.HasKey(e => e.DEPTID)
                    .HasName("DEPTID");

                entity.ToTable("DEPARTMENTS", "dbo");

                entity.Property(e => e.ATT).HasDefaultValueSql("((1))");

                entity.Property(e => e.AutoSchPlan).HasDefaultValueSql("((1))");

                entity.Property(e => e.DEPTNAME).HasMaxLength(30);

                entity.Property(e => e.DefaultSchId).HasDefaultValueSql("((1))");

                entity.Property(e => e.Holiday).HasDefaultValueSql("((1))");

                entity.Property(e => e.InLate).HasDefaultValueSql("((1))");

                entity.Property(e => e.InheritDeptRule).HasDefaultValueSql("((1))");

                entity.Property(e => e.InheritDeptSch).HasDefaultValueSql("((1))");

                entity.Property(e => e.InheritDeptSchClass).HasDefaultValueSql("((1))");

                entity.Property(e => e.InheritParentSch).HasDefaultValueSql("((1))");

                entity.Property(e => e.MinAutoSchInterval).HasDefaultValueSql("((24))");

                entity.Property(e => e.OutEarly).HasDefaultValueSql("((1))");

                entity.Property(e => e.OverTime).HasDefaultValueSql("((1))");

                entity.Property(e => e.RegisterOT).HasDefaultValueSql("((1))");

                entity.Property(e => e.SUPDEPTID).HasDefaultValueSql("((1))");

                entity.Property(e => e.change_operator).HasMaxLength(50);

                entity.Property(e => e.change_time).HasColumnType("datetime");

                entity.Property(e => e.code).HasMaxLength(50);

                entity.Property(e => e.create_operator).HasMaxLength(50);

                entity.Property(e => e.create_time).HasColumnType("datetime");

                entity.Property(e => e.delete_operator).HasMaxLength(50);

                entity.Property(e => e.delete_time).HasColumnType("datetime");

                entity.Property(e => e.invalidate).HasColumnType("datetime");

                entity.Property(e => e.type).HasMaxLength(50);
            });

            modelBuilder.Entity<TEMPLATE>(entity =>
            {
                entity.ToTable("TEMPLATE", "dbo");

                entity.Property(e => e.BITMAPPICTURE).HasColumnType("image");

                entity.Property(e => e.BITMAPPICTURE2).HasColumnType("image");

                entity.Property(e => e.BITMAPPICTURE3).HasColumnType("image");

                entity.Property(e => e.BITMAPPICTURE4).HasColumnType("image");

                entity.Property(e => e.EMACHINENUM).HasMaxLength(3);

                entity.Property(e => e.FINGERID).HasDefaultValueSql("('0')");

                entity.Property(e => e.Fpversion).HasMaxLength(50);

                entity.Property(e => e.StateMigrationFlag)
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.TEMPLATE1)
                    .HasColumnName("TEMPLATE")
                    .HasColumnType("image");

                entity.Property(e => e.TEMPLATE11)
                    .HasColumnName("TEMPLATE1")
                    .HasColumnType("image");

                entity.Property(e => e.TEMPLATE2).HasColumnType("image");

                entity.Property(e => e.TEMPLATE3).HasColumnType("image");

                entity.Property(e => e.TEMPLATE4).HasColumnType("image");

                entity.Property(e => e.UTime).HasColumnType("datetime");

                entity.Property(e => e.Valid).HasDefaultValueSql("('1')");

                entity.Property(e => e.bio_type).HasDefaultValueSql("('0')");

                entity.Property(e => e.change_operator).HasMaxLength(50);

                entity.Property(e => e.change_time).HasColumnType("datetime");

                entity.Property(e => e.create_operator).HasMaxLength(50);

                entity.Property(e => e.create_time).HasColumnType("datetime");

                entity.Property(e => e.delete_operator).HasMaxLength(50);

                entity.Property(e => e.delete_time).HasColumnType("datetime");
            });

            modelBuilder.Entity<USERINFO>(entity =>
            {
                entity.HasKey(e => e.USERID)
                    .HasName("PK__USERINFO__7B9E7F3526E04DD0");

                entity.ToTable("USERINFO", "dbo");

                entity.Property(e => e.ATT).HasDefaultValueSql("((1))");

                entity.Property(e => e.AutoSchPlan).HasDefaultValueSql("((1))");

                entity.Property(e => e.BIRTHDAY).HasColumnType("datetime");

                entity.Property(e => e.Badgenumber).HasMaxLength(50);

                entity.Property(e => e.CITY).HasMaxLength(50);

                entity.Property(e => e.CardNo).HasMaxLength(20);

                entity.Property(e => e.Cuser1).HasMaxLength(50);

                entity.Property(e => e.Cuser2).HasMaxLength(50);

                entity.Property(e => e.Cuser3).HasMaxLength(50);

                entity.Property(e => e.Cuser4).HasMaxLength(50);

                entity.Property(e => e.Cuser5).HasMaxLength(50);

                entity.Property(e => e.DEFAULTDEPTID).HasDefaultValueSql("((1))");

                entity.Property(e => e.Duser1).HasColumnType("datetime");

                entity.Property(e => e.Duser2).HasColumnType("datetime");

                entity.Property(e => e.Duser3).HasColumnType("datetime");

                entity.Property(e => e.Duser4).HasColumnType("datetime");

                entity.Property(e => e.Duser5).HasColumnType("datetime");

                entity.Property(e => e.Education).HasMaxLength(50);

                entity.Property(e => e.FPHONE).HasMaxLength(20);

                entity.Property(e => e.Gender).HasMaxLength(8);

                entity.Property(e => e.HIREDDAY).HasColumnType("datetime");

                entity.Property(e => e.HOLIDAY).HasDefaultValueSql("((1))");

                entity.Property(e => e.INLATE).HasDefaultValueSql("((1))");

                entity.Property(e => e.MINZU).HasMaxLength(8);

                entity.Property(e => e.MinAutoSchInterval).HasDefaultValueSql("((24))");

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.Notes).HasColumnType("image");

                entity.Property(e => e.OPHONE).HasMaxLength(20);

                entity.Property(e => e.OUTEARLY).HasDefaultValueSql("((1))");

                entity.Property(e => e.OVERTIME).HasDefaultValueSql("((1))");

                entity.Property(e => e.OfflineBeginDate).HasColumnType("datetime");

                entity.Property(e => e.OfflineEndDate).HasColumnType("datetime");

                entity.Property(e => e.PAGER).HasMaxLength(20);

                entity.Property(e => e.PASSWORD).HasMaxLength(20);

                entity.Property(e => e.PHOTO).HasColumnType("image");

                entity.Property(e => e.Political).HasMaxLength(50);

                entity.Property(e => e.RegisterOT).HasDefaultValueSql("((1))");

                entity.Property(e => e.SEP).HasDefaultValueSql("((1))");

                entity.Property(e => e.SSN).HasMaxLength(20);

                entity.Property(e => e.STATE).HasMaxLength(50);

                entity.Property(e => e.TITLE).HasMaxLength(20);

                entity.Property(e => e.TimeZones).HasMaxLength(50);

                entity.Property(e => e.UTime).HasColumnType("datetime");

                entity.Property(e => e.ZIP).HasMaxLength(50);

                entity.Property(e => e.acc_enddate).HasColumnType("datetime");

                entity.Property(e => e.acc_startdate).HasColumnType("datetime");

                entity.Property(e => e.bankcode1).HasMaxLength(50);

                entity.Property(e => e.bankcode2).HasMaxLength(50);

                entity.Property(e => e.birthplace).HasMaxLength(50);

                entity.Property(e => e.carBrand).HasMaxLength(50);

                entity.Property(e => e.carColor).HasMaxLength(50);

                entity.Property(e => e.carNo).HasMaxLength(50);

                entity.Property(e => e.carType).HasMaxLength(50);

                entity.Property(e => e.change_operator).HasMaxLength(50);

                entity.Property(e => e.change_time).HasColumnType("datetime");

                entity.Property(e => e.contry).HasMaxLength(50);

                entity.Property(e => e.create_operator).HasMaxLength(50);

                entity.Property(e => e.create_time).HasColumnType("datetime");

                entity.Property(e => e.delete_operator).HasMaxLength(50);

                entity.Property(e => e.delete_time).HasColumnType("datetime");

                entity.Property(e => e.email).HasMaxLength(50);

                entity.Property(e => e.firedate).HasColumnType("datetime");

                entity.Property(e => e.homeaddress).HasMaxLength(50);

                entity.Property(e => e.identitycard).HasMaxLength(50);

                entity.Property(e => e.lastname).HasMaxLength(50);

                entity.Property(e => e.mverifypass).HasMaxLength(10);

                entity.Property(e => e.privilege).HasDefaultValueSql("((0))");

                entity.Property(e => e.status).HasDefaultValueSql("('0')");

                entity.Property(e => e.street).HasMaxLength(80);
            });

            modelBuilder.Entity<acc_levelset>(entity =>
            {
                entity.ToTable("acc_levelset", "dbo");

                entity.HasIndex(e => e.level_name)
                    .HasName("UQ__acc_leve__F94299E912DF21F9")
                    .IsUnique();

                entity.Property(e => e.change_operator).HasMaxLength(30);

                entity.Property(e => e.change_time).HasColumnType("datetime");

                entity.Property(e => e.create_operator).HasMaxLength(30);

                entity.Property(e => e.create_time).HasColumnType("datetime");

                entity.Property(e => e.delete_operator).HasMaxLength(30);

                entity.Property(e => e.delete_time).HasColumnType("datetime");

                entity.Property(e => e.level_name)
                    .HasMaxLength(30)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.status).HasDefaultValueSql("('0')");
            });

            modelBuilder.Entity<acc_levelset_emp>(entity =>
            {
                entity.ToTable("acc_levelset_emp", "dbo");

                entity.HasIndex(e => new { e.acclevelset_id, e.employee_id })
                    .HasName("UQ__acc_leve__C9BFF556E4447A8A")
                    .IsUnique();
            });

            modelBuilder.Entity<acc_monitor_log>(entity =>
            {
                entity.ToTable("acc_monitor_log", "dbo");

                entity.HasIndex(e => new { e.time, e.pin, e.card_no, e.device_id, e.verified, e.state, e.event_type, e.description, e.event_point_type, e.event_point_id })
                    .HasName("UQ__acc_moni__EC0AB1E4334F9708")
                    .IsUnique();

                entity.Property(e => e.card_no).HasMaxLength(50);

                entity.Property(e => e.change_operator).HasMaxLength(50);

                entity.Property(e => e.change_time).HasColumnType("datetime");

                entity.Property(e => e.create_operator).HasMaxLength(50);

                entity.Property(e => e.create_time).HasColumnType("datetime");

                entity.Property(e => e.delete_operator).HasMaxLength(50);

                entity.Property(e => e.delete_time).HasColumnType("datetime");

                entity.Property(e => e.description).HasMaxLength(200);

                entity.Property(e => e.device_name).HasMaxLength(50);

                entity.Property(e => e.device_sn).HasMaxLength(50);

                entity.Property(e => e.event_point_id).HasDefaultValueSql("('-1')");

                entity.Property(e => e.event_point_name).HasMaxLength(200);

                entity.Property(e => e.event_point_type).HasDefaultValueSql("('-1')");

                entity.Property(e => e.pin).HasMaxLength(50);

                entity.Property(e => e.status).HasDefaultValueSql("('0')");

                entity.Property(e => e.time).HasColumnType("datetime");

                entity.Property(e => e.verified).HasDefaultValueSql("('200')");
            });

            modelBuilder.Entity<personnel_area>(entity =>
            {
                entity.ToTable("personnel_area", "dbo");

                entity.Property(e => e.areaid).HasMaxLength(50);

                entity.Property(e => e.areaname).HasMaxLength(50);

                entity.Property(e => e.change_operator).HasMaxLength(50);

                entity.Property(e => e.change_time).HasColumnType("datetime");

                entity.Property(e => e.create_operator).HasMaxLength(50);

                entity.Property(e => e.create_time).HasColumnType("datetime");

                entity.Property(e => e.delete_operator).HasMaxLength(50);

                entity.Property(e => e.delete_time).HasColumnType("datetime");

                entity.Property(e => e.parent_id).HasDefaultValueSql("((0))");

                entity.Property(e => e.remark).HasMaxLength(50);

                entity.Property(e => e.status).HasDefaultValueSql("('0')");
            });
        }
    }
}
