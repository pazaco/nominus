﻿using System;
using System.Collections.Generic;

namespace api2Nominus.Models.auth
{
    public partial class authServidor
    {
        public short sServidor { get; set; }
        public string xDescripcion { get; set; }
        public string xConexion { get; set; }
        public string xModeloAPI { get; set; }

    }
}
