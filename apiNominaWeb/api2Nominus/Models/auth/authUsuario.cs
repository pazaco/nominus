﻿using System;
using System.Collections.Generic;

namespace api2Nominus.Models.auth
{
    public partial class authUsuario
    {
        public Guid gUsuario { get; set; }
        public string email { get; set; }
        public string photo { get; set; }
        public string phoneNumber { get; set; }
        public string displayName { get; set; }
        public byte? bProvider { get; set; }
    }
}
