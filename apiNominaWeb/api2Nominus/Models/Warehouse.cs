﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;
using api2Nominus.Models;
using api2Nominus.Models.auth;
using api2Nominus.Models.uladech;
using api2Nominus.Models.nominus;
using System.Threading.Tasks;
//using System.Web.Script.Serialization;

namespace api2Nominus.Models
{
    public class WareHouse
    {
        private static string apiUladech = "https://api.uladech.edu.pe:8088/";
        private static string xSujeto = "asistencia-from-nominus.pe";


        public static async Task<List<authServidor>> ServerxPersona(int iPersona)
        {
            nominusAppEntities dbA = new nominusAppEntities();
            List<authServidor> resp = new List<authServidor>();
            List<authServidor> _servidores = dbA.authServidor.ToList();
            List<authAcceso> _accesos = dbA.authAcceso.Where(c => c.iPersona == iPersona).ToList();
            resp = _servidores
                .Join(_accesos, srv => srv.sServidor, acc => acc.sServidor, (srv, acc) => new { srv, acc })
                .Select(c => new authServidor { sServidor = c.srv.sServidor, xConexion = c.srv.xConexion, xDescripcion = c.srv.xDescripcion, xModeloAPI = c.srv.xModeloAPI })
                .ToList();
            await Task.Delay(1);
            return resp;
        }

        public static async Task<List<authServidor>> ServerxEmail(string xEmail)
        {
            nominusAppEntities dbA = new nominusAppEntities();
            List<authServidor> resp = new List<authServidor>();
            List<authServidor> _servidores = dbA.authServidor.ToList();
            authUsuario _usuario = dbA.authUsuario.FirstOrDefault(c => c.email == xEmail);
            if (_usuario != null)
            {
                List<authAcceso> _accesos = dbA.authAcceso.Where(c => c.gUsuario == _usuario.gUsuario).ToList();
                resp = _servidores
                    .Join(_accesos, srv => srv.sServidor, acc => acc.sServidor, (srv, acc) => new { srv, acc })
                    .Select(c => new authServidor { sServidor = c.srv.sServidor, xConexion = c.srv.xConexion, xDescripcion = c.srv.xDescripcion, xModeloAPI = c.srv.xModeloAPI })

                    .ToList();
            }
            await Task.Delay(1);
            return resp;
        }

        public static async Task<List<authServidor>> ServersxPersona(Guid gUsuario)
        {
            nominusAppEntities dbA = new nominusAppEntities();
            List<authServidor> csxp = await dbA.authServidoresPersona(gUsuario);
            return csxp;
        }

        public static async Task<List<aut_Contratos>> Contratos(string xEmail)
        {
            nominusAppEntities dbA = new nominusAppEntities();
            List<aut_Contratos> resp = new List<aut_Contratos>();
            Guid gUsuario = dbA.authUsuario.FirstOrDefault(c => c.email == xEmail).gUsuario;
            List<authServidor> _srvs = await ServerxEmail(xEmail);
            foreach (authServidor _srv in _srvs)
            {
                nominaEntities db = new nominaEntities(_srv.xConexion);

                foreach (authAcceso _acceso in dbA.authAcceso.Where(c => c.gUsuario == gUsuario && c.sServidor == _srv.sServidor).ToList())
                {
                    List<AutContratos> _contratos = (await db.autContratos((int)_acceso.iPersona)).ToList();
                    resp.AddRange(_contratos
                        .Select(c => new aut_Contratos
                        {
                            sServidor = _srv.sServidor,
                            iContrato = c.iContrato,
                            sEmpresa = c.sEmpresa,
                            sCargo = c.sCargo,
                            xCargo = c.xCargo,
                            xAbreviado = c.xAbreviado,
                            xRazonSocial = c.xRazonSocial,
                            jImagen = c.jImagen
                        }));
                }
            }
            return resp;
        }

        public static async Task<List<R_servidor>> ContratosServidor(string xEmail)
        {
            nominusAppEntities dbA = new nominusAppEntities();
            List<R_servidor> resp = new List<R_servidor>();
            Guid gUsuario = dbA.authUsuario.FirstOrDefault(c => c.email == xEmail).gUsuario;
            List<authServidor> _srvs = await ServersxPersona(gUsuario);
            foreach (authServidor _srv in _srvs)
            {
                R_servidor cServ;
                if (_srv.sServidor != 8)
                {
                    //no es ULADECH
                    cServ = new R_servidor { sServidor = _srv.sServidor, empresas = new List<R_empresa>() };
                    nominaEntities db = new nominaEntities(_srv.xConexion);
                    List<authAcceso> _accesos = dbA.authAcceso.Where(c => c.gUsuario == gUsuario && c.sServidor == _srv.sServidor).ToList();
                    foreach (authAcceso _acceso in _accesos)
                    {
                        List<AutContratos> _contratos = (await db.autContratos((int)_acceso.iPersona)).OrderBy(c => c.sEmpresa).ToList();
                        int numContratos = _contratos.Count();
                        foreach (AutContratos _contrato in _contratos)
                        {
                            if (cServ.empresas.FirstOrDefault(c => c.sEmpresa == _contrato.sEmpresa) == null)
                            {
                                cServ.empresas.Add(new R_empresa
                                {
                                    sEmpresa = _contrato.sEmpresa,
                                    jImagen = _contrato.jImagen,
                                    xAbreviado = _contrato.xAbreviado,
                                    xRazonSocial = _contrato.xRazonSocial,
                                    contratos = new List<R_contrato>()
                                });
                            }
                            int indexEmpresa = cServ.empresas.FindIndex(c => c.sEmpresa == _contrato.sEmpresa);

                            cServ.empresas[indexEmpresa].contratos.Add(new R_contrato
                            {
                                iContrato = _contrato.iContrato,
                                sCargo = _contrato.sCargo,
                                xCargo = _contrato.xCargo,
                                dDesde = _contrato.dDesde,
                                dHasta = _contrato.dHasta
                            });
                        }
                    }
                }
                else
                {
                    // ULADECH
                    cServ = new R_servidor { sServidor = _srv.sServidor, empresas = new List<R_empresa>() };
                    cServ.empresas.Add(new R_empresa()
                    {
                        sEmpresa = 1,
                        xRazonSocial = "ULADECH -CATÓLICA",
                        xAbreviado = "ULADECH",
                        jImagen = "iVBORw0KGgoAAAANSUhEUgAAAJwAAABzCAIAAADiyPxNAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAABinSURBVHhe7Z0LVFdVvsepsAkwQF6i8kf/poAQCFqooRWlo1lhXmyW3tuQ5nDHVi27Vg7pGnw1ll5znGpcdqsxR5urd9JrkpomSU2iaCq+8IWKDyR5+KLEUrveL//fZs9mn8f//N8P+ayW7X3+D8453/177X3O+d+2sld6QBv+xe3s/234EW2i+iFtovohbTHVCu379g4xm9Ho0CuxXUh72giiknuxloWmhoamunrWCQio27kL/16pqvph117a4k7aRP0npF/MfX2DY6KDo6KCIiOCOnRgrzlAY03NjaarDQcPXaqsvLhnrxtkvqVFjRjycHhKSniPe8Li4yN63MO2upgbP/54/mhlQ0VF9foNLhL4lhMVQsb07xeVkhKZ0DPwrrvYVg9x9eLFc+V7Tq4purDpK7bJGdwSosKvxgwa2Ll/P1uFJM95+fTp6z9cuXLu3JXqs9h4/fIlqxp0HPUUNeDM8S8CcGBwUGjnzrRRCdQ9sWHjiSXLrp2pYZscwJ9FhVF2G5ETmZSoczZFLhw7jnznclXVxUNHXJTjkMOPTksNjTep7lV12faKhYsc/NN+KCppGZuRbjXNgSE2nj5Tv2//pYoK5zpAI+j4Dwel9R9RcY7MT48yDcrS1xJCnj98pL58T+0XxU7xdY5zp6lzXO5IqNsxLZVtsnC0aO2RhYvs2El/ELX7f7zQ9ZFsnfSVEs6asu1132xxQ0VhNxiXCeOeNT0wgBsuYu2uBe/UrvyUugbxYVGVp0ACWp7bs7emdOuZvyxlm3wBGG7iC88n5DzB+gEBVZtLdr/4MusYwCdFNY3P6zEiR8s0fVRLCQzZ+6cX8mNE1Ph6XL5BV+xjosLTJj2dqxU1a/fth4898aeFrO/74HhT854hV4TB+k3BFCMJnW+ISh5JKwlC4DnzTWnVJyu9OV7aDZL5vgWTqf4xqKu3i0pydv/lYNXAicry2Join3azRsBJeOijD4zr6r2i6svplCLdh5B0/TJ/gs6xe6moWrETx3Pii2L7qjdfR9RVP2/yOlE7jnqq76SJbXKqgpT40Q/eI9elU+fc8avoWNb0NNjjrP9amJg7sl1QENtkAXIeW7+h7OXJ3/1v0c+N37OttyTXvqu9HnRXp7590O5gNl+orb1y8DC9JOIVlkrhUyy3iTbrVCW7aCXVr0j712c9QhtFPG+ppvF5A2bPik3vzfotIBXaXjijetnyW9w6lVy9fi0++2E04NICTXF1xSW0neNJS4WBZs6bI81iAxQqe9/9s/uXTXyI4aWbKe2AM/t8RK7kyTx2NSEM9LE1qyRFsYvl739YkjPq1lEUmUS/xe8lFRawvjHOle+hBpKm7mN/TW2OB0SFgQ5csTTzlUlSAYp0DoPOnyb59EGej/MwdNmSuP79Ekc+hdPCXjBAfYuooPuwoazVgrtFxZEoDRQBf8u0mUjQb52ECIX4wFnT+XnA+EaqSG0jXLvcyFoBAfDDEUOaQyzHraL2+fMfcSSSgR4tWosUztYlQ1+netVqxBrWsdD9l4NtMlaRbiNyWMuCm0RF5BhSvM78SDbrWyAD3f9aIev7L1ALpsk6FuCTUK2xjgVbjVUkNqNVtusOUZETPfrBe9J1VqhYikc/4/cGCjlT57yOiJPx77+RdEX97SxjhQcWP+hyUXFIUk6EI9kxf8H25yb4fQRFuIGcCTlP0OEnPZ1L2wlHjLVDr0TWaqFDv0zWcqmoGDvI7qR5osaami/zJ/j9YhnRLjhYHM2wJ8lYr1/5gbVaMGisYZbbe0ToAmPCVaJizx766AMpy4XL/Xpc/q2zXlaxcBFrtUDGipMDBza8dHPKmNG0nWPQWCMTE1irheCYaNZy0YwSMuxBc98UBymoWL7i8OtzWce/gEjwfmQrdTt3iYlCv8XvoQxlHQu1+/br3yigOkkkgtOb/fYC1mnhwrHjJTmjqO18UVGJ9p9aIO409rLsjbl+mRPhYLsNHybJhqz+8CeraBYFaf/QZUtou3FQ5ukUBcqBAkRRnex+qaYWFUUQ/aZgiv8pCuukslt5fhE7kevi1KONWFO1WZ5wF8EIgITG02BsVy5+SDhTVCiKg2EdC7RA738TuTizyBikslsiOCqKhDkw9y1JMwJylr//4fqsR2CUxtPguNyROq6bcJqoSkURPIxfqupbZM6bI5XdErBOOEM6dmXpApAzQk4+0W28ZpXqIlWcI6pSUez0ltF5fqko4qiU1SMHXJWcgf+2TJsJ+0MVLl1ootRMSl9xoo6sbhWhYI7K5ZekwgJacdPHCaKqKrr9uQms4+PQWopoNF0efpC1LOBgeVaP1AH2p6zClcaqrFnxJRgQ1MYIQKA9sWQZdQnsQ+JIdturPo6K6seKmsbnZRetpLUUMcKFxcezloWa0q2spYvSWJWOFDkzBdo1fQYg0Ep+7t6CV3WiacPBQ6zloKgYxX6pKEbq8NLNma9M4rey6KSjIZ07sVZr8H6cH3xV6pzXUdgYMVaEWDHQiuCr9PMyEftFRQmMepR1LPi6opABQQtyYqQqQxefXL3e1EQNotP997OWAOqZJzeug5XjqxJynqCH9hgxVi3SJuSzlgb0kB/CTlFx/NKckU8risOhtZSUMaMlOSm8fT4il5faDRUV1CBgzTAj1mlBfKYSR2ms548c1XIAIiiI9ZNtcHH7DtayT1TsB6o0UVHUo+XTZ7GOT0GXCIlrKRwKb5BTCm/V6zewVgtKM2rXPoS1WsONFTaw8ddjYQZS4FRixPFiV8XvsUfUjJnTxIGjfwuA9xPXv5+qnBTecFxkxyN2b6MQ+MOuvSjB6Z0EzgbNHxF4myTDlaoqauDbyt6Y+9nQxyGnkYUN/GkpxqkCi2ctCzbP/SLqiGsLGHcGb5r0WpRTqSg3ydnCjs1Pj0KWRKpDbChN25UzuhjcN5quoiHdDS3OytrKkOJ1Vh0vwBAU0yvbLuZGcpQ5+VXWsbB11uyGtbI78i0uV5/p8S+tgmJ7k6nh6JGM2TP7THwxMjHh9sBA2t4uKOindoEXy7699l1toClOmkD4xd13B0VE4D/Wt4BBv2P2m1dPnGR9W8Boi+7Vi3V02Tm1ULzk3QZLhSsYvOJjMY/wodU0uMTO/fuFmuLQhrO6XFUlPolKaayqIBCK909a/ZQjbgw7LJWLWiAWbBmdxzoWbBBVOgafSHcxEBM1bkGHLy2bMYvOuP4CGbQ5s3Xb0Y/+qoyCiLXKW4AAPoJEF2mRfakGkiOUQ2jAbDD4UE116JUYZjYHR0VJvh1IvhcYFdU0Pg/FOOtYzkjx6Ge8OTkiOXk41IKfEVWzU9WGAi1f70Q3Ydyz9MhKhNWrDedryrZXr1pt98nhlxjg2zYNfpxtFcAbQuLjozPSkY5hD5Ur6oZExQlC0i+enZKXJnltcoSznPLC80bcKcBJobuylcaqHLg4mz3HjKZvVtqHU+CKok2+EDt2//TCy6dPSw/0olGo6i8NJUq935gZ2bMn61jW5U+9/xHreB+Prfs0vFs31hFA7LlUdRIejCc+AO2ItNSTy/+O3Ce0b0ZoXHPQJZAW1VceoxtA4Q8z5/wh+d/G8DeEdzdXLv4rtZ0FjGfAvDk8UuBvIS+rXf1ZY0Nd6nNjTQ8OOvnFpoDAO5Kn/i5iQGaPx4bhPbvmzVdmYdZFxfH0fm4s61gS9215hgK4p7jT3E3KSyFnSd5zVYuXnl2ztmr9550ezUamyl4LCEC+Sjmtahp8e8fo9KkFibkjpbSWZ8Ks7zBQlN/9z4lOSa4/cRxOEf82XbpUs2LVfX+aZ370kZjUe/EqiqVDs/+T3iliffIhOe8Z1rKwa+481vJWlFOsSHq510JDeQj3PDEc/8IJw5vRFgJhEimoMjeh2Qknul9VRQH8MLwxXoWuKDRgYGJYObj0Y9ZqjRVRkViLhwTH6z2hFPuG9I11BCCb/noIDkESD2cToQsN5UWdEjCOHfMXaK2l2IeWogR0xavUvjMslBoAe6J15ZcVUcVlBAxPGAHreA6cApq0gw0hIRfV4iiNVVoPUYoXN7w5RCmNlYOTuGXazJKcUc69El1fUQKvIi1CA38a4Y82apkp0IupzaYwMIt1AgIOLPtbwxebWcdDQM7M309BpOHJTqe+fZSx7efG76XIKoVApEWxQ4eIYRI5FN1nr4yskHnblN8f/eO7qk/NcAQjihJImm5GhIf2Se/28EPoYoQdKNRcQdGzVHF041tckcHbimrdCZOF2KzTglVj1YIbKz6OBq2lKKcdHAcO36CiRMqY0akt+c23M+XjFdEUFWYqzsLoGLs7abQ8xV5JQs4T5KA4+pEVJ1RKf8RFZjhnWkN1kZwA9ajyTkCr0ICu2lyiv1eaolJCSOjEZO8BaaGkq5axJhUW4ITSFgKBSjxAnDLlJUJORJxhsBVkNgfmvsU6GqiLir8qDqJja4pYy9OIl1dBCZ41EJKuqsaKDAt+TDqh7qzT4C2UisJspGPRYteCd6yONnVReworphga3nnnIYbd1+PylbpmF61EAkJdpbFKZxOvokRxW50GJ4EMQNwHyImwjaR60+DHreoKx2vEZaqLKt6tcWLDRtZyGdCAy6DPpcpK1rLQPilBqSuCJRIQ+kKlsYogD0LgdNuQhReR7l3EDjRfyF9X33HUU8j19EOsEcdLqIiKil4cStIlxc4FCQsOpvnu+ZnT2CZdxKeSgJD4eMim1BVnh+uqNFbA01rXBU4R7An8hzgZBLBXdGFX1sJ3Bs6azpfwYLv4j9oiRhwvoSJq56wHWMty8C46bMiJkTt02RK64gsHTHM6+vCLfYjwnj0xxqVrpgiuq2isOI/wYK6rUlRBgoI9Uc41nj9aSedWfIluwlHWGsjGjeeqKqKKvtfg5ed2EGI2SyM3xcAd1JISKFtVbyYkoCt8AM4pjBW+i6qU3S++7DY5Adwe0iJ9vypexkaX/0tjF2/gy7dGkEXFKRB9r3PjDayKZ6cYd5KTMWisoi+Vsh4leEPfgskwiPWWOwZd5HW0aJ7/UjzYjROZ0JMCxI7Jr3FdKb2PGTSQugCRBW9gHWPIosYIo14cQQ6CPH546WayKj4DoHQyRoxVa/6hOel4aRL+46rDOsvf/1D14gFXQ0FU9UoXDsRGNEUDQ23L6LzmieWXJmHk4bN85gjHgnLL1rEoiyo+9kO6FN1uhhSvQx7P56f4dJ3dxiqCw+ZZDyoT/Ld74SIMR5wj566lGAfebvCKj5VBVAneg5MDB4Z289nY9BXa+Cw37rI35tpRbskT+gnjx/Jp7qOfrHLKFHZIci+tuXX+6FrOXZ07nV2zlnXUuLt3qvhtexcvOTBl+rXvalkfply+9/TK1U6ffDcIKtHMya/iGFnfGr+4+26cgdihQyKy+idNyBcfTA43U73YntJDEVOF8WU83RKBqUnWpjpdR+HEuLHi/ThfrCMQbsAg3AP2cOCKpVIlahCcdnPrn6trvo7QXjejkv0SCEisZRiIQVXK/dNbpWoICcrpOn7Hp9XISl/75MZ1OF+Ix9L8g3S3qKeA20SmLd1hbjcIKI5cUN1KVHLuxNXzF1jLAIgi/NG1zV3FjWBKY+V3fOoYK76WRgl9LYCJ8/kHfCGqlNIXJlLXU+AoVJ+OCmBtyH1YxzBQFPkB69hFq5jaPjmJRzgkmYhM1LbKHR3Cej//W9ax0N5kOrn876yjtmp9e2DgbWFhtC6tjKwRaanmMb8Sr94jEG9+/P6H4JjoA8v+Vjbut/i4Z5+wj2E3YN6c2LQ01heANntfmXL1xEkkEPT7FEZwXFGg6X4vnzzFWgZAOY+9YR0LDhorPi4GGA6KtrqduzyV1kqgDM1+e4HWxAK8CxVv2FXp5GjhFEWBpqi2orzqR7oMURlZ4a/ickdS2+oiPFRHlYKi0770zbnAQFGK6JehAOUm3okGpLK6AuMsRYHTRLXPWDu3BEulsXLwtXTFlzfISRFUx0BFMGr7z5hG3ujrcfk6uSeir7MUBZqihnXrylqGMWKsZ7ZuYx0LkQn/vPBfaayQk2YVvEFOYBqfN3jFx8YfqAGQ5/Npo7IZs6QxTaAede7Ng61EvX75EmshJQkOZi3DGDFW8WcagJgxcmOltNbNayn6IBvPLlqZ+cokPi9mHJwEGDcaFzZ9dW5Pq8PBke6Yv8Dp+UErUcUZqdC4LqxlC0pjTZuQT/6HkB5QI7lcGCutpex/rdBL5MTOIyFCWaWauBkExp1UWIDyjNdmAN74m4Iprliil+964z9NBGArdpxZadcBcgR6KASywQyfeu4Sdpj/erRz4eeE9Z2KHFMbz1SzVusFIOPAWKXIgZziyY3rcg+WS4oCpWV7CZCTHqikVLR5LWz+AtXoaBAMZaTxLlIUyKKK5SlPTW0Cxr3fWn1CIEHwEh8rwuVUDZ/wmdADPtPgMUpgKOCoXe2c5FWaG7fdNFtufATtO3Y8XvSZHVM2F8u+jRyUhY+zvgIc28633z216EPW9wIQO5On/u6+mYWmgVk6ayx46WZEeMM/SnGM0o0bVoGJIwGu+e9PWN9lqNxJPmL3Nu5zkLbYdCGFCPILZXmOAzvz9T/Eh2h4HOTnykem64ARSXfkIyVGAsW2WowYkmsFYLjc8umz3HPUKjdIhaSmdGhZKg/rGn92505xtdI4dcUlZ8rKfvrpx5s3b9ZXHKwp217x4V8qZryBYe7ZCVsCknT/zdjMN1+/Z/iw0Lg4SPV/N26IN5lrgfeE9047vXI1f/AOPnts/YbSfx17W0wU3Q4sQm7p4IzZbjtqFUuNaP1jCs1XEbR+ootPAy3jhg8zPfSgOCUE/7GtYCr/uW8jbGl5gBZq0ANz34IJwoHfW/CqNDWBmq30hYludkvqD/JAoS2WZV5eeBhBVUsORD1/+Aj0gAZBkRFGZhjgbMXHfCiLHxjokdWfeuQ5U+qiIszQc3w4PqorDqTLww9GJiXqT9VCIf6jIwCfSpuQb3V2lxIO1TfDve2Y/JqbDZSjLiqQjBV4xJPYAcJHTP9+USkp+r/pw4GZKucB4EulpwypAvGkqx0wPvYvXuLZu480RZUiK+FBl6KPrUKK0MQZn+3iPimp9aM1jQDbPWLvU86ciKaoQLUmARiMJzZs9GxZghjZIb13eM+eUcm9HJmVhZnShcEDVyzlNkdJkOqw1gImu2fefC+ZS9ETFeg/mhRH4uAj2wwCZ0jP5wvu2DEsPj40rout5qgFF1Ucwbw6zz1YTlt0wDfse+8DL1kcJKyICoMwkuhT9njp2PEr1WcdPDz8xRCzOSSuS0hsbHBMdHBUlN0SIgloOHiobueuvpMm6iS05H5hl8rnx4lzC0qk9Mp7sCIqEI/WODih+Pd6U5P+tU4kG7Ud8aIEQv75o5UNFRV1ZdvFZUTlwpEIdrXE8pBl0/i8HiNysM+UuGpFH+C1chLWRQU8ifBypMdgwtQSxj1rtZ4B0myo6jQC4eVyEoZEBcri2lYovbp46MidYaHRGemmBwYY/zZ89ur5C1ZNuWpzSVNtbZjZXFO6FUUFtHly4zr2mjVgr6csP58Y3uOe2Ix0pbv2CTkJo6IC+/wwoawFccaNPEUIp5IeWw8MPlAe8LLE+Ed0gN7H1hR5tvS0Cc0Lz5TAs32Zb/1SR1W2FUyFovCHCG/ZRSth9+geXv4/7GVtxN9xOL52PWtZg181jvSNGvaBwdF8e6SzH13namwQFSBLRAEAL8f6xsBIpwLO/PQoJCzwoojQHUc9de1yI1Ibeo8WdCs7hgL+heuD4Vo2WwHOE34FDek6N4Pgr1QsX0E/NCIGaV/BNlGJ3S++jPLc4PkF/OFHUcm9qAEGzpquev+JCCQnE4lN7w3jRgNRzfKKdejuaZssDH8OpolDg8M//PpcVxffrsMeUQGKURw5kkarpuYIqE/wL5w2tO9qyUWNG2tUSgo1qLjSp3bf/nLLj0XBNL1qGsE+9J4iapW64pKq9Z/fFhYW1jVeZ3kZxeipTZt+bvw+Iqs/Lb/vmL/gSsN5/U+ByjWfNd+bHHhHfeWx+r376D7iO7rEKheildwVFhaSmnLvyxPDu2pelQ4t8Sd2Ti2sWrwUf8gblu6dgg3Zrw5IZbV+KYRAGKYf/0Wou3bhIoVY5UKQBKIa94EUIxHh8LeMrJ9oAddybs/e+n373TC76SmcIyrHND6v65DBqvfewixObSqmIAePmj75Ff1bdPlED78UGXqs6TMADZ25Hi3wbQjtZ7/80hcTH1txsqgEjCkud2R0WioSHLtNimRAQ9QPBVVwRISR78QIaKw+S3O/fhAmbcIloorAbdJKZ6gpTmdW3XG4ilfOnbtUUXErWKQWLhdVBBYsrqAFBgfZN90De73RdLWpoaGprh4SOr405Ge4VVQtSGzW0aZNOYN4hahtOBc7Jx/a8GbaRPVD2kT1OwIC/h8sk9WM72V7pwAAAABJRU5ErkJggg=="
                    });
                }
                resp.Add(cServ);
            }
            return resp;
        }

        public static async Task<List<RspBuscaPersona>> BuscaPersonas(ParBuscaPersona per)
        {
            List<RspBuscaPersona> rsp = new List<RspBuscaPersona>();
            JavaScriptSerializer jss = new JavaScriptSerializer();
            HttpWebRequest rqper = (HttpWebRequest)WebRequest.Create(apiUladech + "per/persona");
            rqper.Method = "POST";
            rqper.ContentType = "application/json;charset=\"utf-8\"";
            rqper.Headers.Add("xTokenMin", Token.getToken(xSujeto, 128, lUTC: true));
            string xPush = jss.Serialize(per);
            rqper.ContentLength =xPush.Length;
            StreamWriter requestWriter = new StreamWriter(rqper.GetRequestStream(), System.Text.Encoding.ASCII);
            requestWriter.Write(xPush);
            requestWriter.Close();
            WebResponse rspPer = rqper.GetResponse();
            Stream streamPer = rspPer.GetResponseStream();
            StreamReader readerPer = new StreamReader(streamPer);
            string jsonPer = readerPer.ReadToEnd();
            rsp = jss.Deserialize<List<RspBuscaPersona>>(jsonPer);
            await Task.Delay(1);
            return rsp;
        }

        public static async Task<string> HorarioDiario(Nullable<DateTime> dHoy)
        {
            nominusAppEntities dbA = new nominusAppEntities();
            //TODO:  Extender control de horario a otros servidores, 8 es uladech
            if (dHoy == null)
            {
                dHoy = DateTime.Now.Date;
            }
            List<string> faltantes = new List<string>();
            authServidor _srv = dbA.authServidor.Where(c => c.sServidor == 8).FirstOrDefault();
            nominaEntities db = new nominaEntities(_srv.xConexion);
            uladechEntities dbU = new uladechEntities();
            JavaScriptSerializer jss = new JavaScriptSerializer();
            string xApi = String.Format("asi/diario?dFecha={0:yyyy-MM-dd}", dHoy);
            HttpWebRequest rqsD = (HttpWebRequest)WebRequest.Create(apiUladech + xApi);
            rqsD.Method = "GET";
            rqsD.ContentType = "application/json:charset=\"utf-8\"";
            string tkn = Token.getToken(xSujeto, 128, lUTC: true);
            rqsD.Headers.Add("xTokenMin", tkn);
            WebResponse rspD = rqsD.GetResponse();
            Stream strD = rspD.GetResponseStream();
            StreamReader strrD = new StreamReader(strD);
            string jsDiario = strrD.ReadToEnd();
            List<RspHorarioDiario> horario = jss.Deserialize<List<RspHorarioDiario>>(jsDiario);
            List<asi_HorarioDiario> todoH = db.asi_HorarioDiario.ToList();
            db.asi_HorarioDiario.RemoveRange(todoH);
            db.asi_HorarioDiario.AddRange(horario.Select(c => new asi_HorarioDiario
            {
                cEmpleado = c.cEmpleado,
                bControl = c.bControl,
                cHora = c.cHora,
                cActividad = c.cActividad,
                cAula = c.cAula
            }).ToList());
            db.SaveChanges();
            await db.asi_CargaDiaria((DateTime)dHoy);
            if (dHoy >= DateTime.Now.Date)
            {
                await db.asi_SincronizarMarcacion();
            }
            await Task.Delay(1);
            return horario.Count.ToString();
        }

        public static List<asi_HorarioDiario> PreDiarioDocente(Nullable<DateTime> dHoy)
        {
            nominusAppEntities dbA = new nominusAppEntities();
            //TODO:  Extender Pre control de horario a otros servidores, 8 es uladech
            if (dHoy == null)
            {
                dHoy = DateTime.Now;
            }
            List<string> faltantes = new List<string>();
            authServidor _srv = dbA.authServidor.Where(c => c.sServidor == 8).FirstOrDefault();
            nominaEntities db = new nominaEntities(_srv.xConexion);
            uladechEntities dbU = new uladechEntities();
            JavaScriptSerializer jss = new JavaScriptSerializer();
            string xApi = String.Format("asi/diario?dFecha={0:yyyy-MM-dd}", dHoy);
            HttpWebRequest rqsD = (HttpWebRequest)WebRequest.Create(apiUladech + xApi);
            rqsD.Method = "GET";
            rqsD.ContentType = "application/json:charset=\"utf-8\"";
            string tkn = Token.getToken(xSujeto, 128, lUTC: true);
            rqsD.Headers.Add("xTokenMin", tkn);
            WebResponse rspD = rqsD.GetResponse();
            Stream strD = rspD.GetResponseStream();
            StreamReader strrD = new StreamReader(strD);
            string jsDiario = strrD.ReadToEnd();
            List<RspHorarioDiario> horario = jss.Deserialize<List<RspHorarioDiario>>(jsDiario);
            List<asi_HorarioDiario> todoH = db.asi_HorarioDiario.ToList();
            db.asi_HorarioDiario.RemoveRange(todoH);
            db.asi_HorarioDiario.AddRange(horario.Where(c => c.cActividad != null).Select(c => new asi_HorarioDiario
            {
                cEmpleado = c.cEmpleado,
                bControl = c.bControl,
                cHora = c.cHora,
                cActividad = c.cActividad,
                cAula = c.cAula
            }).ToList());
            db.SaveChanges();
            return db.asi_HorarioDiario.ToList();
        }

        public static void empleadoUladech(RespFirebase uchFirebase)
        {
            uchFirebase.uchempleado = EmpleadoUladech(uchFirebase.usuario.email);
        }

        public static List<string> IndiceUladech()
        {
            JavaScriptSerializer jss = new JavaScriptSerializer();
            HttpWebRequest rqst = (HttpWebRequest)WebRequest.Create(apiUladech + "per/indice");
            rqst.Method = "GET";
            rqst.ContentType = "application/json;charset=\"utf-8\"";
            rqst.Headers.Add("xTokenMin", Token.getToken(xSujeto, 128, lUTC: true));
            WebResponse webRsp = rqst.GetResponse();
            Stream webStream2 = webRsp.GetResponseStream();
            StreamReader rspReader = new StreamReader(webStream2);
            string rt = rspReader.ReadToEnd();
            List<uIndice> rspi = jss.Deserialize<List<uIndice>>(rt);
            return rspi.Select(c => c.cCodigoEmpleado).ToList();
        }

        public static OuchEmpleado EmpleadoUladech(string cCodigoUsuario)
        {
            OuchEmpleado ouch = new OuchEmpleado();
            JavaScriptSerializer jss = new JavaScriptSerializer();
            HttpWebRequest rqst2 = (HttpWebRequest)WebRequest.Create(apiUladech + "per/persona?cPersona=" + cCodigoUsuario);
            rqst2.Method = "GET";
            string FechaNula = (char)34 + "0000-00-00" + (char)34;
            rqst2.ContentType = "application/json;charset=\"utf-8\"";
            rqst2.Headers.Add("xTokenMin", Token.getToken(xSujeto, 128, lUTC: true));
            WebResponse webRsp2 = rqst2.GetResponse();
            Stream webStream2 = webRsp2.GetResponseStream();
            StreamReader rspReader2 = new StreamReader(webStream2);
            string rt2 = rspReader2.ReadToEnd();
            rt2 = rt2.Replace(FechaNula, "null");
            if (rt2.IndexOf("[") < 4) rt2 = "null";
            ouch = jss.Deserialize<OuchEmpleado>(rt2);
            if (ouch != null)
            {
                uladechEntities dbU = new uladechEntities();
                USERINFO uzk = dbU.USERINFO.FirstOrDefault(c => c.Badgenumber == ouch.cCodigoEmpleado);
                if (uzk != null)
                {
                    ouch.iZkEmpleado = uzk.USERID;
                }
            }
            return ouch;
        }

        public static async Task<OuchEmpleado> EmpleadoUladech(int iEmpleado)
        {
            OuchEmpleado rsp = new OuchEmpleado();
            uladechEntities dbU = new uladechEntities();
            string cCodEmpleado = dbU.USERINFO.Find(iEmpleado).Badgenumber;
            if(cCodEmpleado!=null) rsp = EmpleadoUladech(cCodEmpleado);
            await Task.Delay(1);
            return rsp;
        }

        public static async Task<OuchLogin> LoginUladech(string email)
        {
            nominusAppEntities dbA = new nominusAppEntities();
            OuchLogin ouch = new OuchLogin();
            JavaScriptSerializer jss = new JavaScriptSerializer();
            uladechEntities dbU = new uladechEntities();

            authUsuario _usu = dbA.authUsuario.Where(c => c.email == email).FirstOrDefault();
            if (_usu == null)
            {
                HttpWebRequest requestCodigo = (HttpWebRequest)WebRequest.Create(apiUladech + "aut/accesos?xEmail" + email);
                requestCodigo.Method = "GET";
                requestCodigo.ContentType = "application/json;charset=\"utf-8\"";
                requestCodigo.Headers.Add("xTokenMin", Token.getToken(xSujeto, 128, lUTC: true));
                try
                {
                    WebResponse rspCodigo = requestCodigo.GetResponse();
                    Stream codStream = rspCodigo.GetResponseStream();
                    StreamReader rspReader = new StreamReader(codStream);
                    string codTE = rspReader.ReadToEnd();
                    OuchLogin codigoLogin;
                    if (codTE != "[]")
                    {
                        codigoLogin = jss.Deserialize<OuchLogin>(codTE);
                        authProvider _prv = dbA.authProvider.Where(c => c.providerId == "google.com").FirstOrDefault();
                        _usu = new authUsuario
                        {
                            bProvider = _prv.bProvider,
                            email = email,
                            gUsuario = Guid.NewGuid()
                        };
                        dbA.authUsuario.Add(_usu);
                        dbA.SaveChanges();
                        USERINFO _user = dbU.USERINFO.Where(c => c.Badgenumber == codigoLogin.cCodigoUsuario).FirstOrDefault();
                        authAcceso _acc = new authAcceso
                        {
                            gAcceso = Guid.NewGuid(),
                            gUsuario = _usu.gUsuario,
                            sServidor = 8,
                            iPersona = _user.USERID
                        };
                        dbA.authAcceso.Add(_acc);
                        dbA.SaveChanges();
                    }
                }
                finally { }               
            }
            if (_usu != null)
            {
                authAcceso acceso = dbA.authAcceso.Where(c => c.sServidor == 8 && c.gUsuario == _usu.gUsuario).FirstOrDefault();
                USERINFO user = dbU.USERINFO.Find(acceso.iPersona);
                HttpWebRequest request1 = (HttpWebRequest)WebRequest.Create(apiUladech + "aut/accesos?cCodigoEmpleado=" + user.Badgenumber);
                request1.Method = "GET";
                request1.ContentType = "application/json;charset=\"utf-8\"";
                request1.Headers.Add("xTokenMin", Token.getToken(xSujeto, 128, lUTC: true));
                try
                {
                    WebResponse webResp = request1.GetResponse();
                    Stream webStream = webResp.GetResponseStream();
                    StreamReader rspReader = new StreamReader(webStream);
                    string rte = rspReader.ReadToEnd();
                    ouch = jss.Deserialize<OuchLogin>(rte);
                }
                catch { }
            }
            await Task.Delay(1);
            return ouch;
        }

        public static void EnlazarAppNominus(ParEnlazar parEnlazar)
        {
            new nominusAppEntities().authCrearEnlace(parEnlazar.sServidor, parEnlazar.iUsuario, parEnlazar.xCorreo);
        }

        public static async void CompletarEnlaces(authUsuario usuario)
        {
            nominusAppEntities dbA = new nominusAppEntities();
            List<authServidor> _srvs = dbA.authServidor.ToList();
            foreach (authServidor _srv in _srvs)
            {
                if (_srv.sServidor == 8)
                {
                    //uladech
                    uladechEntities db = new uladechEntities();
                    USERINFO usr = db.USERINFO.FirstOrDefault(c => c.email == usuario.email);
                    if (usr != null)
                    {

                        dbA.authCrearEnlace(_srv.sServidor, usr.USERID, usr.email);
                    }
                }
                else
                {
                    // no uladech
                    nominaEntities db = new nominaEntities(_srv.xConexion);
                    autUsuario usr = db.autUsuario.FirstOrDefault(c => c.xEmail == usuario.email);
                    if (usr != null)
                    {
                        dbA.authCrearEnlace(_srv.sServidor, usr.iUsuario, usr.xEmail);
                    }
                }
            }
            await Task.Delay(1);
        }

        public static async Task<List<int>> Perfiles(string xEmail)
        {
            nominusAppEntities dbA = new nominusAppEntities();
            Guid gUsuario = dbA.authUsuario.FirstOrDefault(c => c.email == xEmail).gUsuario;

            List<authServidor> _srvs = await ServersxPersona(gUsuario);
            List<int> resp = new List<int>();
            List<int> tempLst = new List<int>();

            foreach (authServidor _srv in _srvs)
            {
                if (_srv.sServidor != 8)
                {
                    nominaEntities db = new nominaEntities(_srv.xConexion);
                    List<authAcceso> _accesos = dbA.authAcceso.Where(c => c.gUsuario == gUsuario && c.sServidor == _srv.sServidor).ToList();
                    foreach (authAcceso _acceso in _accesos)
                    {
                        List<AutContratos> _contratos = (await db.autContratos((int)_acceso.iPersona)).OrderBy(c => c.sEmpresa).ToList();
                        if (_contratos.Count > 0)
                        {
                            tempLst.Add(1);  // acceso a boletas en cualquier estado contractual
                            tempLst.Add(2);  // acceso a vacaciones en cualquier estado contractual
                        }
                        foreach (AutContratos _contrato in _contratos)
                        {
                            if (db.traMarcacion.Where(c => c.iContrato == _contrato.iContrato).ToList().Count > 0)
                            {
                                tempLst.Add(3); //acceso a asistencia calificada (que marcan reloj)
                            }
                            if (_contrato.dHasta == null || _contrato.dHasta > DateTime.Now)  //contrato vigente
                            {
                                if (db.webPrestamos(_contrato.iContrato).ToList().Count > 0)
                                {
                                    tempLst.Add(4); //tiene algun prestamo
                                }
                                if (db.traJefeDirecto.Where(c => c.iTrabajador == _acceso.iPersona).ToList().Count > 0)
                                {
                                    tempLst.Add(5); //es jefe de alguien
                                }
                            }
                        }
                    }
                }
            }
            for (int ii = 1; ii < 100; ii++)
            {
                if (tempLst.Contains(ii)) resp.Add(ii);
            }
            return resp;
        }

        public static string EnviarPush(NotiPush notiPush)
        {
            string rsp;
            JavaScriptSerializer jss = new JavaScriptSerializer();
            string xPush = jss.Serialize(notiPush);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://fcm.googleapis.com/fcm/send");
            request.Method = "POST";
            request.ContentType = "application/json;charset=\"utf-8\"";
            request.Headers.Add("Authorization", "key=AAAAsb5bga8:APA91bGQ2z97vHe6xoKeJmpkED8UPRZ_bK-X3FGyyZXFV9DTEzrOOPxgFxA7KUu2-TME47Sz-W3CGSgAxgVLjsfBiIQzdmyjHz-HjXIMqmcXFXs8PjJz27GuN4lwRJ_aviDEtKdREiGvMZ7BMXVtwP0k-Ou6jILb7w");
            request.ContentLength = xPush.Length;
            StreamWriter requestWriter = new StreamWriter(request.GetRequestStream(), System.Text.Encoding.ASCII);
            requestWriter.Write(xPush);
            requestWriter.Close();
            try
            {
                WebResponse webResp = request.GetResponse();
                Stream webStream = webResp.GetResponseStream();
                StreamReader rspReader = new StreamReader(webStream);
                rsp = rspReader.ReadToEnd();
                rspReader.Close();
            }
            catch (Exception e)
            {
                rsp = e.Message;
            }
            return rsp;
        }

        public static async Task<string> ServerConnection(string xSrv)
        {
            nominusAppEntities dbA = new nominusAppEntities();
            string xEntity = "nominaEntities";
            if (xSrv != null && Regex.IsMatch(xSrv, @"^\d+$"))
            {
                authServidor _servidor = dbA.authServidor.Find(int.Parse(xSrv));
                if (_servidor != null)
                {
                    xEntity = _servidor.xConexion;
                }
            }
            await Task.Delay(1);
            return xEntity;
        }

        public static bool RayCasting(GeoPunto punto, List<GeoPunto> poligono)
        {
            int contador = 0;
            for (int ii = 0; ii < poligono.Count; ii++)
            {
                if (aDerecha(poligono[ii], poligono[(ii + 1) % poligono.Count], punto))
                {
                    contador++;
                }
            }
            return (contador % 2 == 1);
        }

        private static bool aDerecha(GeoPunto A, GeoPunto B, GeoPunto x)
        {
            if (A.fLon <= B.fLon)
            {
                if (x.fLon <= A.fLon || x.fLon > B.fLon || x.fLat >= A.fLat && x.fLat >= B.fLat)
                {
                    return false;
                }
                else if (x.fLat < A.fLat && x.fLat < B.fLat)
                {
                    return true;
                }
                else
                {
                    return (x.fLon - A.fLon) / (x.fLat - A.fLat) > (B.fLon - A.fLon) / (B.fLat - A.fLat);
                }
            }
            else {
                return aDerecha(B, A, x);
            }
        }

    }

    public class RspHorarioDiario
    {
        public string cEmpleado { get; set; }
        public short bControl { get; set; }
        public string cHora { get; set; }
        public string cActividad { get; set; }
        public string cAula { get; set; }
    }

    public class NotiNotification
    {
        public string title { get; set; }
        public string body { get; set; }
        public string icon { get; set; }
        public webNotificar data { get; set; }
    }

    public class ParBuscaPersona
    {
        public string xCargoParcial { get; set; }
        public string xNombreParcial { get; set; }
        public string xJefe { get; set; }
        public string xSituacion { get; set; }
        public short bCantidad { get; set; }
    }

    public class RspBuscaPersona
    {
        public string xCargo { get; set; }
        public string xEmail { get; set; }
        public string xNombre { get; set; }
        public string cSituacion { get; set; }
        public string xApellidos { get; set; }
        public string cJefeDirecto { get; set; }
        public string cCodigoEmpleado { get; set; }
        public List<string> cEstablecimiento { get; set; }
    }


    public class NotiPush
    {
        public NotiNotification notification { get; set; }
        public string to { get; set; }
    }

    public class RspGeoUbicacion
    {
        public int iEstablecimiento { get; set; }
        public string cEstablecimiento { get; set; }
        public short sOrtogono { get; set; }
        public bool lMatch { get; set; }
        public List<AgmPunto> puntos { get; set; }
    }

    public class aut_Contratos : AutContratos
    {
        public short sServidor { get; set; }
    }

    public class hdContrato
    {
        public short sServidor { get; set; }
        public int iContrato { get; set; }
    }

    public class BigBelCalculos : belCalculos
    {
        public short sServidor { get; set; }
    }

    public class R_servidor
    {
        public short sServidor { get; set; }
        public List<R_empresa> empresas { get; set; }
    }

    public class R_empresa
    {
        public short sEmpresa { get; set; }
        public string xRazonSocial { get; set; }
        public string xAbreviado { get; set; }
        public string jImagen { get; set; }
        public List<R_contrato> contratos { get; set; }
    }

    public class R_contrato
    {
        public int iContrato { get; set; }
        public short sCargo { get; set; }
        public string xCargo { get; set; }
        public Nullable<DateTime> dDesde { get; set; }
        public Nullable<DateTime> dHasta { get; set; }
    }

    public class OuchEmpleado
    {
        public string xCargo { get; set; }
        public string xEmail { get; set; }
        public string xNombre { get; set; }
        public string cSituacion { get; set; }
        public string xApellidos { get; set; }
        public string cJefeDirecto { get; set; }
        public string cCodigoEmpleado { get; set; }
        public Nullable<int> iZkEmpleado { get; set; }
        public List<string> cEstablecimiento { get; set; }
        public Nullable<DateTime> dDesde { get; set; }
        public Nullable<DateTime> dHasta { get; set; }
        public Nullable<DateTime> dNacimiento { get; set; }
        public string cGenero { get; set; }
    }

    public class OuchLogin
    {
        public string xEmail { get; set; }
        public string cCodigoUsuario { get; set; }
        public List<int> bAccesos { get; set; }
    }

    public class RespFirebase
    {
        public Nullable<Guid> token { get; set; }
        public authUsuario usuario { get; set; }
        public List<R_servidor> contratos { get; set; }
        public List<int> perfiles { get; set; }
        public OuchEmpleado uchempleado { get; set; }
    }

    public class ParEnlazar
    {
        public short sServidor { get; set; }
        public string xCorreo { get; set; }
        public int iUsuario { get; set; }
    }

    public class uIndice
    {
        public string cCodigoEmpleado { get; set; }
    }

    public class GeoPunto
    {
        public double? fLat { get; set; }
        public double? fLon { get; set; }
    }

    public class AgmPunto
    {
        public double lat { get; set; }
        public double lng { get; set; }

    }

    public class ParCaminar
    {
        public List<string> establecimientos { get; set; }
        public GeoPunto punto { get; set; }
    }

    public class ParPoligono
    {
        public string cEstablecimiento { get; set; }
        public short sOrtogono { get; set; }
    }
}
