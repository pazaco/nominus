﻿using System;
using System.Collections.Generic;

namespace api2Nominus.Models.uladech
{
    public partial class acc_levelset_emp
    {
        public int id { get; set; }
        public int acclevelset_id { get; set; }
        public int employee_id { get; set; }
    }
}
