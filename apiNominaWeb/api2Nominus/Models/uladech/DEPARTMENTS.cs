﻿using System;
using System.Collections.Generic;

namespace api2Nominus.Models.uladech
{
    public partial class DEPARTMENTS
    {
        public int DEPTID { get; set; }
        public string DEPTNAME { get; set; }
        public int SUPDEPTID { get; set; }
        public short? InheritParentSch { get; set; }
        public short? InheritDeptSch { get; set; }
        public short? InheritDeptSchClass { get; set; }
        public short? AutoSchPlan { get; set; }
        public short? InLate { get; set; }
        public short? OutEarly { get; set; }
        public short? InheritDeptRule { get; set; }
        public int? MinAutoSchInterval { get; set; }
        public short? RegisterOT { get; set; }
        public int DefaultSchId { get; set; }
        public short? ATT { get; set; }
        public short? Holiday { get; set; }
        public short? OverTime { get; set; }
        public string change_operator { get; set; }
        public DateTime? change_time { get; set; }
        public string create_operator { get; set; }
        public DateTime? create_time { get; set; }
        public string delete_operator { get; set; }
        public DateTime? delete_time { get; set; }
        public int? status { get; set; }
        public string code { get; set; }
        public string type { get; set; }
        public DateTime? invalidate { get; set; }
    }
}
