﻿using System;
using System.Collections.Generic;

namespace api2Nominus.Models.uladech
{
    public partial class acc_levelset
    {
        public int id { get; set; }
        public string change_operator { get; set; }
        public DateTime? change_time { get; set; }
        public string create_operator { get; set; }
        public DateTime? create_time { get; set; }
        public string delete_operator { get; set; }
        public DateTime? delete_time { get; set; }
        public short status { get; set; }
        public string level_name { get; set; }
        public int? level_timeseg_id { get; set; }
    }
}
