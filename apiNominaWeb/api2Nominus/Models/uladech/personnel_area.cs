﻿using System;
using System.Collections.Generic;

namespace api2Nominus.Models.uladech
{
    public partial class personnel_area
    {
        public int id { get; set; }
        public string change_operator { get; set; }
        public DateTime? change_time { get; set; }
        public string create_operator { get; set; }
        public DateTime? create_time { get; set; }
        public string delete_operator { get; set; }
        public DateTime? delete_time { get; set; }
        public short status { get; set; }
        public string areaid { get; set; }
        public string areaname { get; set; }
        public int? parent_id { get; set; }
        public string remark { get; set; }
    }
}
