﻿//using SoundFingerprinting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;
using api2Nominus.Models;
using api2Nominus.Models.nominus;
using api2Nominus.Models.auth;
using Microsoft.EntityFrameworkCore;
//using SoundFingerprinting.InMemory;
//using SoundFingerprinting.Audio;
//using SoundFingerprinting.Data;
//using SoundFingerprinting.Builder;
//using SoundFingerprinting.Query;

namespace api2Nominus.Controllers
{
    [RoutePrefix("voiceprint")]
    public class voiceprintsController : ApiController
    {
        nominusAppEntities dbN = new nominusAppEntities();
        readonly JavaScriptSerializer jss = new JavaScriptSerializer();

        [HttpGet, Route("tipofrase/{sServidor}")]
        public List<vozFrase> tiposFrase([FromUri] int sServidor)
        {
            List<vozFrase> rsp = null;
            string xConn = dbN.authServidor.Find(sServidor).xConexion;
            nominaEntities db = new nominaEntities(xConn);
            rsp = db.vozFrase.ToList();
            return rsp;
        }

        [HttpPost, Route("vozAbierta")]
        public Voces vozAbierta([FromBody] ParPersona persona)
        {
            string xConn = dbN.authServidor.Find(persona.sServidor).xConexion;
            nominaEntities db = new nominaEntities(xConn);
            Voces voz = new Voces();
            vozTrack oTrack = db.vozTrack.FirstOrDefault(c => c.iPersona == persona.iPersona && c.lAbierto == true);
            if (oTrack != null)
            {
                vozFrase cfr = db.vozFrase.Find(oTrack.bFrase);
                List<vozFingerPrint> afp = db.vozFingerPrint.Where(c => c.iTrack == oTrack.iTrack).ToList();
                voz = new Voces
                {
                    iTrack = oTrack.iTrack,
                    iPersona = oTrack.iPersona,
                    bFrase = oTrack.bFrase,
                    lAbierto = oTrack.lAbierto,
                    xFrase = cfr.xFrase,
                    aFingerPrints = afp
                };
            }
            return voz;
        }

        [HttpPost, Route("abrirVoz")]
        public bool? abrirVoz([FromBody] ParFrase frase)
        {
            bool? rsp;
            string xConn = dbN.authServidor.Find(frase.sServidor).xConexion;
            nominaEntities db = new nominaEntities(xConn);
            vozTrack vtr = db.vozTrack.FirstOrDefault(c => c.bFrase == frase.bFrase && c.iPersona == frase.iPersona);
            if (vtr == null)
            {
                vtr = new vozTrack { bFrase = frase.bFrase, iPersona = frase.iPersona, lAbierto = true };
                db.vozTrack.Add(vtr);
                db.SaveChanges();
                rsp = true;
            }
            else
            {
                vtr.lAbierto = !vtr.lAbierto;
                db.Entry(vtr).State = EntityState.Modified;
                db.SaveChanges();
                rsp = vtr.lAbierto;
            }
            return rsp;
        }

        [HttpPost, Route("borrarVoz")]
        public void borrarVoz([FromBody] ParVoz parvoz)
        {
            string xConn = dbN.authServidor.Find(parvoz.sServidor).xConexion;
            nominaEntities db = new nominaEntities(xConn);
            vozFingerPrint borrar = db.vozFingerPrint.Find(parvoz.iFingerPrint);
            var filePath = Path.Combine(HttpContext.Current.Server.MapPath("~/voces"), borrar.xSoporte);
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
            db.vozFingerPrint.Remove(borrar);
            db.SaveChanges();
        }

        [HttpPost, Route("usuario")]
        public List<Voces> VocesUsuario([FromBody] ParPersona parPersona)
        {
            string xConn = dbN.authServidor.Find(parPersona.sServidor).xConexion;
            nominaEntities db = new nominaEntities(xConn);
            List<Voces> voces = new List<Voces>();
            List<vozFrase> frases = db.vozFrase.OrderBy(c => c.bFrase).ToList();
            foreach (vozFrase cfr in frases)
            {
                vozTrack oTrack = db.vozTrack.FirstOrDefault(c => c.bFrase == cfr.bFrase && c.iPersona == parPersona.iPersona);
                if (oTrack == null)
                {
                    oTrack = new vozTrack { iPersona = parPersona.iPersona, bFrase = cfr.bFrase, iTrack = 0, lAbierto = false };
                }
                List<vozFingerPrint> afp = db.vozFingerPrint.Where(c => c.iTrack == oTrack.iTrack).ToList();
                voces.Add(new Voces
                {
                    iTrack = oTrack.iTrack,
                    iPersona = oTrack.iPersona,
                    bFrase = oTrack.bFrase,
                    lAbierto = oTrack.lAbierto,
                    xFrase = cfr.xFrase,
                    aFingerPrints = afp
                });
            }
            return voces;
        }

        [HttpPost, Route("upload")]
        public string UploadFile()
        {
            string rsp = "";
            Guid gSesion;
            string xSesion = HttpContext.Current.Request.Headers["sesionPersona"];
            try
            {
                gSesion = Guid.Parse(xSesion);
            }
            catch
            {
                return "Error! no hay sesion para identificar al empleado.";
            }
            VozTrack voz;
            string xVoz = HttpContext.Current.Request.Headers["idTrack"];
            try
            {
                voz = jss.Deserialize<VozTrack>(xVoz);
            }
            catch
            {
                return "Error! no hay track para identificar";
            }
            Random rnd = new Random((DateTime.Now).Millisecond);
            authSesion oSesion = dbN.authSesion.Find(gSesion);
            authAcceso oAcceso = dbN.authAcceso.Where(c => c.gUsuario == oSesion.gUsuario && c.sServidor == voz.sServidor).FirstOrDefault();
            if (oAcceso == null)
            {
                return "Error!  El usuario no está debidamente autenticado";
            }

            if (HttpContext.Current.Request.Files.AllKeys.Any())
            {
                var httpPostedFile = HttpContext.Current.Request.Files["waves"];
                if (httpPostedFile != null)
                {

                    string xConn = dbN.authServidor.Find(voz.sServidor).xConexion;
                    nominaEntities db = new nominaEntities(xConn);
                    vozTrack oTrack = db.vozTrack.Find(voz.iTrack);
                    if (oTrack != null)
                    {
                        vozFingerPrint nuevoFingerPrint = new vozFingerPrint
                        {
                            iTrack = oTrack.iTrack
                        };
                        db.vozFingerPrint.Add(nuevoFingerPrint);
                        db.SaveChanges();
                        oTrack.lAbierto = false;
                        db.Entry(oTrack).State = EntityState.Modified;
                        db.SaveChanges();
                        nuevoFingerPrint.xSoporte = string.Format("R{1:00}{0:0000000}", nuevoFingerPrint.iFingerPrint, voz.sServidor) + "." + (httpPostedFile.ContentType == "audio/wav" ? "wav" : "ogg");
                        db.Entry(nuevoFingerPrint).State = EntityState.Modified;
                        db.SaveChanges();
                        var fileSavePath = Path.Combine(HttpContext.Current.Server.MapPath("~/voces"), nuevoFingerPrint.xSoporte);
                        httpPostedFile.SaveAs(fileSavePath);
                    }
                }
            }
            return rsp;
        }


        [HttpPost, Route("uploadMarca")]
        public string UploadMarca()
        {
            //            ResultEntry rsp = null;
            string rsp = "Ok";
            Guid gSesion;
            string xSesion = HttpContext.Current.Request.Headers["sesionPersona"];
            try
            {
                gSesion = Guid.Parse(xSesion);
            }
            catch
            {
                return "Error sobre internet, reportelo al administrador";
            }
            VozMarca voz;
            string xVoz = HttpContext.Current.Request.Headers["vozMarca"];
            try
            {
                voz = jss.Deserialize<VozMarca>(xVoz);
            }
            catch
            {
                return "Error en el archivo de voz";
            }
            //            Random rnd = new Random((DateTime.Now).Millisecond);
            authSesion oSesion = dbN.authSesion.Find(gSesion);
            authAcceso oAcceso = dbN.authAcceso.Where(c => c.gUsuario == oSesion.gUsuario && c.sServidor == voz.sServidor).FirstOrDefault();
            if (oAcceso == null)
            {
                return "Error de acceso";
            }

            if (HttpContext.Current.Request.Files.AllKeys.Any())
            {
                var httpPostedFile = HttpContext.Current.Request.Files["waves"];
                if (httpPostedFile != null)
                {
                    string xConn = dbN.authServidor.Find(voz.sServidor).xConexion;
                    nominaEntities db = new nominaEntities(xConn);
                    string xFingerPrint = string.Format("M{2:00}{0:000000}{1:00}", voz.iPersona, voz.bFrase, voz.sServidor) + "." + (httpPostedFile.ContentType == "audio/wav" ? "wav" : "ogg");
                    var fileMarcaPath = Path.Combine(HttpContext.Current.Server.MapPath("~/voces"), xFingerPrint);
                    if (File.Exists(fileMarcaPath))
                    {
                        File.Delete(fileMarcaPath);
                    }
                    httpPostedFile.SaveAs(fileMarcaPath);

                    //IModelService vozSvc = new InMemoryModelService();
                    //IAudioService audSvc = new SoundFingerprintingAudioService();

                    //foreach (vozTrack vlt in db.vozTrack.Where(c => c.iPersona == voz.iPersona).ToList())
                    //{
                    //    foreach (vozFingerPrint vfp in db.vozFingerPrint.Where(c => c.iTrack == vlt.iTrack).ToList())
                    //    {
                    //        var track = new TrackInfo(vfp.xSoporte, $"Print{vfp.iFingerPrint}", $"Frase{vlt.bFrase}", 14d);
                    //        var fileVoz = Path.Combine(HttpContext.Current.Server.MapPath("~/voces"), vfp.xSoporte);
                    //        List<HashedFingerprint> hashedFingerprints = FingerprintCommandBuilder.Instance
                    //            .BuildFingerprintCommand()
                    //            .From(fileVoz)
                    //            .UsingServices(audSvc)
                    //            .Hash()
                    //            .Result;
                    //        vozSvc.Insert(track, hashedFingerprints);
                    //    }
                    //}
                    //var queryResult = QueryCommandBuilder.Instance.BuildQueryCommand()
                    //                 .From(fileMarcaPath, 14, 0)
                    //                 .UsingServices(vozSvc, audSvc)
                    //                 .Query()
                    //                 .Result;
                    //rsp = queryResult.BestMatch;
                    if (db.vozTrack.Where(c => c.iPersona == voz.iPersona).Count() == 0)
                    {
                        rsp = "No hay voces";
                    }
                }
            }
            else
            {
                rsp = "No se detectó ninguna voz";
            }
            return rsp;
        }

        //[HttpGet, Route("probando")]
        //public List<ResultEntry> calificarVoces()
        //{
        //    List<ResultEntry> rsp = null;

        //    IModelService vozSvc = new InMemoryModelService();
        //    IAudioService audSvc = new SoundFingerprintingAudioService();

        //    string xMarca = Path.Combine(HttpContext.Current.Server.MapPath("~/voces"), "M0800743701.wav");
        //    List<string> reclu = new List<string> { "R080000676.wav", "R080000677.wav", "R080000678.wav", "R080000679.wav", "R080000680.wav", "R080000681.wav", "R080000682.wav", "R080000724.wav", "R080000723.wav" };
        //    foreach (string cr in reclu)
        //    {
        //        var track = new TrackInfo(cr, cr, cr, 14d);
        //        var fileVoz = Path.Combine(HttpContext.Current.Server.MapPath("~/voces"), cr);
        //        List<HashedFingerprint> hashedFingerprints = FingerprintCommandBuilder.Instance
        //            .BuildFingerprintCommand()
        //            .From(fileVoz)
        //            .UsingServices(audSvc)
        //            .Hash()
        //            .Result;
        //        vozSvc.Insert(track, hashedFingerprints);
        //    }
        //    var queryResult = QueryCommandBuilder.Instance.BuildQueryCommand()
        //                     .From(xMarca, 10, 0)
        //                     .UsingServices(vozSvc, audSvc)
        //                     .Query()
        //                     .Result;
        //    rsp = queryResult.ResultEntries.ToList();


        //    return rsp;
        //}


        //[HttpGet, Route("verFingerPrint/{xWaveFile}")]
        //public void verFingerPrint([FromUri] string xWaveFile)
        //{
        //    var filePath = Path.Combine(HttpContext.Current.Server.MapPath("~/voces"), xWaveFile);
        //    Voz.StoreAudioFileFingerprintsInStorageForLaterRetrieval(filePath);
        //}

        [HttpPost, Route("upload2")]
        public async Task<HttpResponseMessage> PostFormData()
        {
            // Check if the request contains multipart/form-data.
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            string root = HttpContext.Current.Server.MapPath("~/voces");
            var provider = new MultipartFormDataStreamProvider(root);

            try
            {
                // Read the form data.
                await Request.Content.ReadAsMultipartAsync(provider);

                // This illustrates how to get the file names.
                foreach (MultipartFileData file in provider.FileData)
                {
                    Trace.WriteLine(file.Headers.ContentDisposition.FileName);
                    Trace.WriteLine("Server file path: " + file.LocalFileName);
                }
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (System.Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }
    }

    public class VozTrack
    {
        public int iTrack { get; set; }
        public short sServidor { get; set; }
    }

    public class VozIn : vozFingerPrint
    {
        public byte bFrase { get; set; }
    }

    public class VozMarca : vozMarcando
    {
        public short sServidor { get; set; }
    }

    public class Voces : vozTrack
    {
        public string xFrase { get; set; }
        public List<vozFingerPrint> aFingerPrints { get; set; }
    }

    public class ParPersona
    {
        public int iPersona { get; set; }
        public short sServidor { get; set; }
    }

    public class ParFrase : ParPersona
    {
        public byte bFrase { get; set; }
    }

    public class ParVoz
    {
        public int iFingerPrint { get; set; }
        public short sServidor { get; set; }
    }
}