﻿using System;

namespace apiNominaWeb.Models
{
    public class Token
    {

        public static string getToken(string xSujeto,short sLongitud,bool lPrevio=false,bool lUTC=false)
        {
            string xLpo="ab9cK4fstv5^*dCDSxy7zAB#$%0&=TUV6OPZ1emnE2gQRWXYhjklLMNpqrFG3H8I@)J";
            DateTime ya = DateTime.Now.AddMinutes(lPrevio ? -1 : 0);
            if (lUTC) ya = ya.ToUniversalTime();
            string xPassword = string.Format("{0:mmddMM}Y{0:yy}T{0:mmHH}", ya) ;
            xSujeto += xPassword;
            string xTokenMinuto = "";
            int iLen = 0;
            int iSeed = 0;
            int iPass = 0;
            short sPos = 1;
            string xPwd = "";
            int iChar = 0;
            while (xSujeto.Length < sLongitud)
            {
                xSujeto += xSujeto;   
            }
            while (xPwd.Length< sLongitud)
            {
                xPwd += xPassword;
            }
            foreach(char c in xPassword)
            {
                iLen += (int)c;
            }
            while (sPos <= sLongitud)
            {
                iSeed = xLpo.IndexOf(xSujeto.Substring(sPos - 1, 1)) + 1;
                iPass = xPwd[sPos-1];
                iChar = 1 + ((2 * iSeed + 5 * iPass + 3 * iLen) % 67);
                xTokenMinuto += xLpo.Substring(iChar - 1, 1);
                sPos++;
            }
            return xTokenMinuto;
        }

    }
}