﻿using SoundFingerprinting;
using SoundFingerprinting.Audio;
using SoundFingerprinting.Builder;
using SoundFingerprinting.DAO.Data;
using SoundFingerprinting.Data;
using SoundFingerprinting.InMemory;
using SoundFingerprinting.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace apiNominaWeb.Models
{
    public class Voz
    {

        private static readonly IModelService modelService = new InMemoryModelService(); // store fingerprints in RAM
        private static readonly IAudioService audioService = new SoundFingerprintingAudioService(); // default audio library

        public static void StoreAudioFileFingerprintsInStorageForLaterRetrieval(string pathToAudioFile)
        {
            
            pathToAudioFile = pathToAudioFile + ".wav";
            var track = new TrackInfo(pathToAudioFile,"VOZ","PRUEBAS",14d);

            // create fingerprints
            List<HashedFingerprint> hashedFingerprints = FingerprintCommandBuilder.Instance
                                        .BuildFingerprintCommand()
                                        .From(pathToAudioFile)
                                        .UsingServices(audioService)
                                        .Hash()
                                        .Result;

            // store hashes in the database for later retrieval
            modelService.Insert(track, hashedFingerprints);

            if (modelService.Info.SubFingerprintsCount>0) {
               QueryResult queryResult = GetBestMatchForSong(pathToAudioFile);
                string rsp = queryResult.BestMatch.Track.ISRC;
            }
        }


        public static QueryResult GetBestMatchForSong(string queryAudioFile)
        {
            int secondsToAnalyze = 10; // number of seconds to analyze from query file
            int startAtSecond = 0; // start at the begining

            // query the underlying database for similar audio sub-fingerprints
            var queryResult = QueryCommandBuilder.Instance.BuildQueryCommand()
                                                 .From(queryAudioFile, secondsToAnalyze, startAtSecond)
                                                 .UsingServices(modelService, audioService)
                                                 .Query()
                                                 .Result;

            return queryResult;
        }

        public List<MatchVoz> MatchVozcargarVoces(int iUsuario, short bFrase)
        {
            List<MatchVoz> rsp = new List<MatchVoz>();
            


            return rsp;
        }

    }

    public class MatchVoz
    {
        public int iTrack { get; set; }
        public string xWave { get; set; }
        public double fMatch { get; set; }
    }

}