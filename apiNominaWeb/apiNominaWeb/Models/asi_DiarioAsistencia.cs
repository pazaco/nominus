//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace apiNominaWeb.Models
{
    using System;
    
    public partial class asi_DiarioAsistencia
    {
        public int iAsistencia { get; set; }
        public int iContrato { get; set; }
        public System.DateTime dFecha { get; set; }
        public Nullable<double> fDescuento { get; set; }
        public Nullable<double> fExtra { get; set; }
        public Nullable<int> iCalculoPlanilla { get; set; }
        public Nullable<short> sEstablecimiento { get; set; }
        public string cPlanilla { get; set; }
        public string name { get; set; }
        public string lastname { get; set; }
        public Nullable<int> iEstado { get; set; }
    }
}
