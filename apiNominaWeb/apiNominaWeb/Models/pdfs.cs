﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using apiNominaWeb.Controllers;

namespace apiNominaWeb.Models
{
    public class Pdfs
    {
        public static outBoleta Boleta(DataBoleta dt, ParCabecera parCabecera, string xServer = "nominaEntities", Boolean forzar = false)
        {
            nominaEntities db = new nominaEntities(xServer);
            outBoleta resp = new outBoleta();
            //forzar = true;   //habilitar esta linea sólo en pruebas de desarrollo

            belPDFs iPDF = db.belPDFs.FirstOrDefault(c => c.iCalculoPlanilla == parCabecera.iCalculoPlanilla);
            if (iPDF == null)
            {
                iPDF = new belPDFs
                {
                    cPDF = v16((short)parCabecera.sServidor),
                    iCalculoPlanilla = parCabecera.iCalculoPlanilla,
                    dGeneracion = DateTime.Now
                };
                while (db.belPDFs.Any(c => c.cPDF == iPDF.cPDF))
                {
                    iPDF.cPDF = v16((short)parCabecera.sServidor);
                }
                db.belPDFs.Add(iPDF);
                db.SaveChanges();
            }
            string xPdf = HttpContext.Current.Server.MapPath("/pdfs") + $"\\{iPDF.cPDF}.pdf";
            resp.xUrl = "https://api.nominus.pe/pdfs/" + $"{iPDF.cPDF}.pdf";
            Image imgQR = (new BarcodeQRCode(resp.xUrl, 1, 1, null)).GetImage();
            byte[] rawQR = imgQR.RawData;
            resp.QRbase64 = Convert.ToBase64String(rawQR);
            if (forzar && File.Exists(xPdf))
            {
                File.Delete(xPdf);
            }
            if (!File.Exists(xPdf))
            {
                FileStream fs = new FileStream(xPdf, FileMode.Create);
                Document doc = new Document(PageSize.A5.Rotate(), 50, 30, 40, 20);
                PdfWriter wri = PdfWriter.GetInstance(doc, fs);
                doc.AddAuthor("jla/nominus.pe");
                doc.AddCreator("iTextSharp v.5.5 AGPL license");
                int idx;
                short sEmpresa = dt.cabecera[0].sEmpresa;
                short sLogo = dt.cabecera[0].sEmpresaImagenLogo;
                short sFirma = dt.cabecera[0].sEmpresaImagenFirma;
                string base64Logo = db.orgEmpresaImagen.FirstOrDefault(c => c.sEmpresa == sEmpresa && c.sEmpresaImagen == sLogo).jImagen;
                string base64Firma = db.orgEmpresaImagen.FirstOrDefault(c => c.sEmpresa == sEmpresa && c.sEmpresaImagen == sFirma).jImagen;

                Image imgLogo = Image.GetInstance(Convert.FromBase64String(base64Logo));
                Image imgFirma = Image.GetInstance(Convert.FromBase64String(base64Firma));

                Font fvEmp = FontFactory.GetFont("Verdana", 11, Font.BOLD);
                Font fvNormal = FontFactory.GetFont("Verdana", 9);
                Font fvBold = FontFactory.GetFont("Verdana", 9, Font.BOLD);
                Font fvSmall = FontFactory.GetFont("Verdana", 6);
                Font fxSmall = FontFactory.GetFont("Verdana", 5);
                Font faNormal = FontFactory.GetFont("Arial", 7);
                Font faBold = FontFactory.GetFont("Arial", 7, Font.BOLD);
                Font faSmall = FontFactory.GetFont("Arial", 5);

                float fsb = 7; //espacio entre tablas
                BaseColor bgcolor = new BaseColor(224, 224, 224);
                doc.Open();
                PdfPTable tTitulo = new PdfPTable(new float[] { 75, 35 }) { WidthPercentage = 100f };
                PdfPTable tEmpresa = new PdfPTable(new float[] { 4, 9 });
                float rataImg = imgLogo.Height / imgLogo.Width;
                imgLogo.ScaleAbsolute(50/rataImg,50f);
                PdfPCell cEmpresaLogo = new PdfPCell(imgLogo) { Border = 0, Padding = 5,  };
                tEmpresa.AddCell(cEmpresaLogo);
                PdfPTable tEmpresaDatos = new PdfPTable(1) { WidthPercentage = 90 };
                PdfPCell cEmpresaDatos0 = new PdfPCell(new Phrase(dt.empresa[0], fvEmp)) { Border = 0 };
                PdfPCell cEmpresaDatos1 = new PdfPCell(new Phrase(dt.empresa[1], fvNormal)) { Border = 0 };
                PdfPCell cEmpresaDatos2 = new PdfPCell(new Phrase(dt.empresa[2], fvSmall)) { Border = 0 };
                tEmpresaDatos.AddCell(cEmpresaDatos0);
                tEmpresaDatos.AddCell(cEmpresaDatos1);
                tEmpresaDatos.AddCell(cEmpresaDatos2);
                PdfPCell cEmpresaDatos = new PdfPCell(tEmpresaDatos) { Border = 0 };
                tEmpresa.AddCell(cEmpresaDatos);
                PdfPCell cEmpresa = new PdfPCell(tEmpresa) { Border = 0 };
                tTitulo.AddCell(cEmpresa);
                PdfPTable otBoleta = new PdfPTable(1);
                PdfPCell ocBoleta = new PdfPCell() { Border = 0, CellEvent = new RoundRectangle() };
                PdfPTable itBoleta = new PdfPTable(1) { WidthPercentage = 100 };
                idx = 0;
                foreach (string icab in dt.titulo)
                {
                    PdfPCell iCell = new PdfPCell(new PdfPCell(new Phrase(icab, idx == 0 ? fvBold : idx == 2 ? fvSmall : fvNormal))) { Border = 0, HorizontalAlignment = 1 };
                    idx++;
                    itBoleta.AddCell(iCell);
                }
                ocBoleta.AddElement(itBoleta);
                otBoleta.AddCell(ocBoleta);
                PdfPCell ccBoleta = new PdfPCell(otBoleta) { Border = 0 };
                tTitulo.AddCell(ccBoleta);
                doc.Add(tTitulo);

                PdfPTable tTrab = new PdfPTable(new float[] { 35, 15 }) { WidthPercentage = 100, SpacingBefore = fsb };
                PdfPTable otTrabContrato = new PdfPTable(1);
                PdfPCell ocTrabContrato = new PdfPCell() { Border = 0, CellEvent = new RoundRectangle() };
                PdfPTable itTrabContrato = new PdfPTable(new float[] { 20, 15 }) { WidthPercentage = 100 };
                PdfPTable tPersona = new PdfPTable(new float[] { 1, 2 });
                foreach (remBoletaCabeceraDatos idat in dt.datosCol1)
                {
                    PdfPCell iCel1 = new PdfPCell(new Phrase(idat.xNombre, faNormal)) { Border = 0 };
                    PdfPCell iCel3 = new PdfPCell(new Phrase(idat.xDato, faNormal)) { Border = 0 };
                    tPersona.AddCell(iCel1);
                    tPersona.AddCell(iCel3);
                }
                PdfPCell cPersona = new PdfPCell(tPersona) { Border = 0 };
                itTrabContrato.AddCell(cPersona);
                PdfPTable tContrato = new PdfPTable(new float[] { 1, 2 });
                foreach (remBoletaCabeceraDatos idat in dt.datosCol2)
                {
                    PdfPCell iCel1 = new PdfPCell(new Phrase(idat.xNombre, faNormal)) { Border = 0 };
                    PdfPCell iCel3 = new PdfPCell(new Phrase(idat.xDato, faNormal)) { Border = 0, HorizontalAlignment = 2 };
                    tContrato.AddCell(iCel1);
                    tContrato.AddCell(iCel3);
                }
                PdfPCell cContrato = new PdfPCell(tContrato) { Border = 0 };
                itTrabContrato.AddCell(cContrato);
                ocTrabContrato.AddElement(itTrabContrato);
                otTrabContrato.AddCell(ocTrabContrato);
                PdfPCell ccTrabContrato = new PdfPCell(otTrabContrato) { Border = 0 };
                tTrab.AddCell(ccTrabContrato);

                PdfPTable otAsist = new PdfPTable(1);
                PdfPCell ocAsist = new PdfPCell() { Border = 0, CellEvent = new RoundRectangle() };
                PdfPTable itAsist = new PdfPTable(new float[] { 2, 1, 1 }) { WidthPercentage = 100 };
                foreach (remBoletaConcepto ccn in dt.concepto1.OrderBy(c => c.sOrdenMostrar))
                {
                    PdfPCell iCel1 = new PdfPCell(new Phrase(ccn.Concepto, faNormal)) { Border = 0 };
                    PdfPCell iCel2 = new PdfPCell(new Phrase(ccn.MontoHora, faNormal)) { Border = 0 };
                    PdfPCell iCel3 = new PdfPCell(new Phrase(string.Format("{0:0}", ccn.mMonto), faNormal)) { Border = 0, HorizontalAlignment = 2 };
                    itAsist.AddCell(iCel1);
                    itAsist.AddCell(iCel2);
                    itAsist.AddCell(iCel3);
                }
                ocAsist.AddElement(itAsist);
                otAsist.AddCell(ocAsist);
                PdfPCell ccAsist = new PdfPCell(otAsist) { Border = 0 };
                tTrab.AddCell(ccAsist);
                doc.Add(tTrab);

                float[] fwDetConceptos = { 4, 1, 2 };
                PdfPTable tConceptos = new PdfPTable(new float[] { 6, 5, 5 }) { WidthPercentage = 100, SpacingBefore = fsb };
                PdfPCell cConce2 = new PdfPCell() { Border = 0, CellEvent = new RoundRectangle(), Padding = 0 };
                PdfPTable tConce2 = new PdfPTable(1) { WidthPercentage = 100 };
                tConce2.AddCell(new PdfPCell(new Phrase(dt.cabecera[0].xingresos, faNormal)) { Border = 0, HorizontalAlignment = 1, BackgroundColor = bgcolor });
                PdfPTable itConce2 = new PdfPTable(fwDetConceptos) { WidthPercentage = 100 };
                foreach (remBoletaConcepto cda in dt.concepto2.OrderBy(c => c.sOrdenMostrar))
                {
                    itConce2.AddCell(new PdfPCell(new Phrase(cda.Concepto, faNormal)) { Border = 0 });
                    itConce2.AddCell(new PdfPCell(new Phrase(string.Format("{0:#,##0.00}", cda.MontoInfo), faNormal)) { Border = 0 });
                    itConce2.AddCell(new PdfPCell(new Phrase(string.Format("{0}{1:#,##0.00}", dt.cabecera[0].monedaneto, cda.mMonto), faNormal)) { Border = 0, HorizontalAlignment = 2 });
                }
                PdfPCell icConce2 = new PdfPCell() { Border = 0, MinimumHeight = 80 };
                icConce2.AddElement(itConce2);
                tConce2.AddCell(icConce2);
                PdfPTable tConce2t = new PdfPTable(2) { WidthPercentage = 100 };
                tConce2t.AddCell(new PdfPCell(new Phrase("TOTAL " + dt.cabecera[0].xingresos, faNormal)) { Border = 0 });
                tConce2t.AddCell(new PdfPCell(new Phrase(string.Format("{0}{1:#,##0.00}", dt.cabecera[0].monedaneto, vsuma(dt.concepto2)), faBold)) { Border = 0, HorizontalAlignment = 2 });
                tConce2.AddCell(new PdfPCell(tConce2t) { Border = 0, BackgroundColor = bgcolor });
                cConce2.AddElement(tConce2);
                tConceptos.AddCell(cConce2);
                PdfPCell cConce3 = new PdfPCell() { Border = 0, CellEvent = new RoundRectangle(), Padding = 0 };
                PdfPTable tConce3 = new PdfPTable(1) { WidthPercentage = 100 };
                tConce3.AddCell(new PdfPCell(new Phrase(dt.cabecera[0].xdescuentos, faNormal)) { Border = 0, HorizontalAlignment = 1, BackgroundColor = bgcolor });
                PdfPTable itConce3 = new PdfPTable(fwDetConceptos) { WidthPercentage = 100 };
                foreach (remBoletaConcepto cda in dt.concepto3.OrderBy(c => c.sOrdenMostrar))
                {
                    itConce3.AddCell(new PdfPCell(new Phrase(cda.Concepto, faNormal)) { Border = 0 });
                    itConce3.AddCell(new PdfPCell(new Phrase(string.Format("{0:#,##0.00}", cda.MontoInfo), faNormal)) { Border = 0 });
                    itConce3.AddCell(new PdfPCell(new Phrase(string.Format("{0}{1:#,##0.00}", dt.cabecera[0].monedaneto, cda.mMonto), faNormal)) { Border = 0, HorizontalAlignment = 2 });
                }
                PdfPCell icConce3 = new PdfPCell() { Border = 0, MinimumHeight = 80 };
                icConce3.AddElement(itConce3);
                tConce3.AddCell(icConce3);
                PdfPTable tConce3t = new PdfPTable(2) { WidthPercentage = 100 };
                tConce3t.AddCell(new PdfPCell(new Phrase("TOTAL " + dt.cabecera[0].xdescuentos, faNormal)) { Border = 0 });
                tConce3t.AddCell(new PdfPCell(new Phrase(string.Format("{0}{1:#,##0.00}", dt.cabecera[0].monedaneto, vsuma(dt.concepto3)), faBold)) { Border = 0, HorizontalAlignment = 2 });
                tConce3.AddCell(new PdfPCell(tConce3t) { Border = 0, BackgroundColor = bgcolor });
                cConce3.AddElement(tConce3);
                tConceptos.AddCell(cConce3);
                PdfPCell cConce4 = new PdfPCell() { Border = 0, CellEvent = new RoundRectangle(), Padding = 0 };
                PdfPTable tConce4 = new PdfPTable(1) { WidthPercentage = 100 };
                tConce4.AddCell(new PdfPCell(new Phrase(dt.cabecera[0].xempleador, faNormal)) { Border = 0, HorizontalAlignment = 1, BackgroundColor = bgcolor });
                PdfPTable itConce4 = new PdfPTable(fwDetConceptos) { WidthPercentage = 100 };
                foreach (remBoletaConcepto cda in dt.concepto4.OrderBy(c => c.sOrdenMostrar))
                {
                    itConce4.AddCell(new PdfPCell(new Phrase(cda.Concepto, faNormal)) { Border = 0 });
                    itConce4.AddCell(new PdfPCell(new Phrase(string.Format("{0:#,##0.00}", cda.MontoInfo), faNormal)) { Border = 0 });
                    itConce4.AddCell(new PdfPCell(new Phrase(string.Format("{0}{1:#,##0.00}", dt.cabecera[0].monedaneto, cda.mMonto), faNormal)) { Border = 0, HorizontalAlignment = 2 });
                }
                PdfPCell icConce4 = new PdfPCell() { Border = 0, MinimumHeight = 80 };
                icConce4.AddElement(itConce4);
                tConce4.AddCell(icConce4);
                PdfPTable tConce4t = new PdfPTable(2) { WidthPercentage = 100 };
                tConce4t.AddCell(new PdfPCell(new Phrase("TOTAL " + dt.cabecera[0].xempleador, faNormal)) { Border = 0 });
                tConce4t.AddCell(new PdfPCell(new Phrase(string.Format("{0}{1:#,##0.00}", dt.cabecera[0].monedaneto, vsuma(dt.concepto4)), faBold)) { Border = 0, HorizontalAlignment = 2 });
                tConce4.AddCell(new PdfPCell(tConce4t) { Border = 0, BackgroundColor = bgcolor });
                cConce4.AddElement(tConce4);
                tConceptos.AddCell(cConce4);
                doc.Add(tConceptos);

                PdfPTable tPie = new PdfPTable(new float[] { 4, 1 }) { WidthPercentage = 100, SpacingBefore = fsb };
                PdfPCell ocFirmas = new PdfPCell() { Border = 0, CellEvent = new RoundRectangle() };
                PdfPTable itFirmas = new PdfPTable(new float[] { 2, 2, 1 }) { WidthPercentage = 100 };
                PdfPTable itFirma1 = new PdfPTable(1) { WidthPercentage = 100 };
                itFirma1.AddCell(new PdfPCell(imgFirma, true) { Border = 0, FixedHeight = 40, HorizontalAlignment = 1 });
                itFirma1.AddCell(new PdfPCell(new Phrase("EMPLEADOR", fxSmall)) { Border = 0, HorizontalAlignment = 1, Padding = 0 });
                PdfPCell icFirma1 = new PdfPCell() { Border = 0, Padding = 0 };
                icFirma1.AddElement(itFirma1);
                itFirmas.AddCell(icFirma1);

                PdfPTable itFirma2 = new PdfPTable(1) { WidthPercentage = 100 };
                itFirma2.AddCell(new PdfPCell(new Phrase("")) { Border = 0, MinimumHeight = 40 });
                itFirma2.AddCell(new PdfPCell(new Phrase(dt.datosCol1[1].xDato, fxSmall)) { Border = 0, HorizontalAlignment = 1, Padding = 0 });
                PdfPCell icFirma2 = new PdfPCell() { Border = 0, Padding = 0 };
                icFirma2.AddElement(itFirma2);
                itFirmas.AddCell(icFirma2);

                PdfPTable itQR = new PdfPTable(2) { WidthPercentage = 100 };
                itQR.AddCell(new PdfPCell(imgQR, true) { Border = 0, VerticalAlignment = 1, HorizontalAlignment = 1 });
                itQR.AddCell(new PdfPCell(new Phrase(string.Format("{0:00000000}", parCabecera.iCalculoPlanilla), fxSmall)) { Border = 0, Rotation = 90, VerticalAlignment = 1, HorizontalAlignment = 1 });
                PdfPCell icQR = new PdfPCell() { Border = 0, HorizontalAlignment = 2, PaddingRight = 6 };
                icQR.AddElement(itQR);
                itFirmas.AddCell(icQR);
                ocFirmas.AddElement(itFirmas);
                tPie.AddCell(ocFirmas);
                PdfPCell ocTotal = new PdfPCell() { Border = 0, CellEvent = new RoundRectangle(), Padding = 0 };
                PdfPTable itTotal = new PdfPTable(1) { WidthPercentage = 100 };
                itTotal.AddCell(new PdfPCell(new Phrase("TOTAL A PAGAR", faBold)) { Border = 0, BackgroundColor = bgcolor, HorizontalAlignment = 1, Padding = 6, VerticalAlignment = 1 });
                itTotal.AddCell(new PdfPCell(new Phrase(string.Format("{0}{1:#,##0.00}", dt.cabecera[0].monedaneto, dt.cabecera[0].neto), fvEmp)) { Border = 0, HorizontalAlignment = 1, VerticalAlignment = 1, Padding = 3 });
                ocTotal.AddElement(itTotal);
                tPie.AddCell(ocTotal);
                doc.Add(tPie);
                doc.Close();
                wri.Close();
                fs.Close();
            }
            return resp;
        }

        private class RoundRectangle : IPdfPCellEvent
        {
            public void CellLayout(
              PdfPCell cell, iTextSharp.text.Rectangle rect, PdfContentByte[] canvas
            )
            {
                PdfContentByte cb = canvas[PdfPTable.LINECANVAS];
                cb.RoundRectangle(
                  rect.Left, rect.Bottom, rect.Width, rect.Height, 4
                );
                cb.SetLineWidth(1f);
                cb.SetCMYKColorStrokeF(0f, 0f, 0f, 1f);
                cb.Stroke();
            }
        }

        static decimal vsuma(List<remBoletaConcepto> arr)
        {
            decimal suma = 0;
            foreach (remBoletaConcepto cc in arr)
            {
                suma += cc.mMonto;
            }
            return suma;
        }

        static string v16(short pre = 1)
        {
            string x16 = string.Format("{0:NM00_}", pre);
            string cads = "ACDEFHJKLMNPQRSTVWXY1234567890";
            Random rnd = new Random();
            int ix;
            for (int ii = 0; ii < 11; ii++)
            {
                ix = rnd.Next(0, 29);
                x16 += cads.Substring(ix, 1);
            }
            return x16.Trim();
        }

    }

    public class outBoleta
    {
        public string xUrl { get; set; }
        public string QRbase64 { get; set; }
    }

}


