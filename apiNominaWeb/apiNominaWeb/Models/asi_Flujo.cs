//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace apiNominaWeb.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class asi_Flujo
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public asi_Flujo()
        {
            this.asi_Resolucion = new HashSet<asi_Resolucion>();
        }
    
        public int iAsistencia { get; set; }
        public short bControl { get; set; }
        public System.DateTime dHoraHorario { get; set; }
        public Nullable<System.DateTime> dHoraControl { get; set; }
        public Nullable<int> iMarcacion { get; set; }
        public Nullable<short> sIncidencia { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<asi_Resolucion> asi_Resolucion { get; set; }
    }
}
