//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace apiNominaWeb.Models
{
    using System;
    
    public partial class asi_MarcacionDiarioEmpleado_Result
    {
        public int iMarcacion { get; set; }
        public Nullable<short> sDispositivo { get; set; }
        public Nullable<short> sEstablecimiento { get; set; }
        public Nullable<System.DateTime> dHora { get; set; }
        public Nullable<int> iPersona { get; set; }
        public Nullable<int> zkId { get; set; }
        public string xDispositivo { get; set; }
        public string xEstablecimiento { get; set; }
    }
}
