﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentScheduler;
using apiNominaWeb.Models;

namespace apiNominaWeb.Jobs
{
    public class Agenda : Registry
    {
        nominusApp2Entities dbA = new nominusApp2Entities();

        public Agenda()
        {
            Schedule(
                async () =>                 {
                    //Carga diaria del horario
                    await WareHouse.HorarioDiarioAsync(DateTime.Now.AddDays(1));
                })
                .ToRunEvery(1).Days().At(20, 55);


        }
    }
}