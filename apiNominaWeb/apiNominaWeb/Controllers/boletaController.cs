﻿using apiNominaWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace apiNominaWeb.Controllers
{

    [RoutePrefix("boleta")] public class BoletaController : ApiController
    {
        private static nominaEntities db = new nominaEntities();
        private static nominusApp2Entities dbA = new nominusApp2Entities();

        [Route("cabecera")]
        public List<remBoletaCabecera> PostCabecera([FromBody] ParCabecera parCabecera)
        {
            string SesionPersona = HttpContext.Current.Request.Headers["sesionPersona"];
            string SesionGestor = HttpContext.Current.Request.Headers["sesionGestor"];
            Guid? gSesionPersona = null;
            Guid? gSesionGestor = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            if (SesionGestor != null && SesionGestor != "") gSesionGestor = new Guid(SesionGestor);

            return db.remBoletaCabecera(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero).ToList();
        }

        [Route("data2")]
        public DataBoleta PostDataBoleta2([FromBody] ParCabecera parCabecera)
        {
            string SesionPersona = HttpContext.Current.Request.Headers["sesionPersona"];
            string SesionGestor = HttpContext.Current.Request.Headers["sesionGestor"];
            Guid? gSesionPersona = null;
            Guid? gSesionGestor = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            if (SesionGestor != null && SesionGestor != "") gSesionGestor = new Guid(SesionGestor);
            return remBoleta(parCabecera);
        }

        [Route("data")]
        public DataBoleta PostDataBoleta([FromBody] ParCabecera parCabecera)
        {
            string SesionPersona = HttpContext.Current.Request.Headers["sesionPersona"];
            Guid? gSesionPersona = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            JavaScriptSerializer jSer = new JavaScriptSerializer();
            authServidor _srv = dbA.authServidor.Find(parCabecera.sServidor);
            nominaEntities db_ = new nominaEntities(_srv.xConexion);
            remCalculoPlanilla calculo = db_.remCalculoPlanilla.Find(parCabecera.iCalculoPlanilla);
            traContrato contrato = db_.traContrato.Find(calculo.iContrato);
            DataBoleta datas = new DataBoleta();
            datas.cabecera = db_.remBoletaCabecera(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero).ToList();
            datas.asistencia = db_.remBoletaCabeceraAsistenciaFechas(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero).ToList();
            datas.datosCol1 = db_.remBoletaCabeceraDatos(parCabecera.iCalculoPlanilla, 1).ToList();
            datas.datosCol2 = db_.remBoletaCabeceraDatos(parCabecera.iCalculoPlanilla, 2).ToList();
            datas.empresa = db_.remBoletaCabeceraEmpresa(contrato.sEmpresa).ToList();
            datas.titulo = db_.remBoletaCabeceraTitulo(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero).ToList();
            datas.concepto1 = db_.remBoletaConcepto(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero, 1).ToList();
            datas.concepto2 = db_.remBoletaConcepto(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero, 2).ToList();
            datas.concepto3 = db_.remBoletaConcepto(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero, 3).ToList();
            datas.concepto4 = db_.remBoletaConcepto(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero, 4).ToList();
            datas.pdf = Pdfs.Boleta(datas, parCabecera, _srv.xConexion);
            return datas;
        }




        [Route("cabeceraAsistenciaFechas")]
        public List<remBoletaCabeceraAsistenciaFechas> PostCabeceraAsistFechas([FromBody] ParCabecera parCabecera)
        {
            string SesionPersona = HttpContext.Current.Request.Headers["sesionPersona"];
            string SesionGestor = HttpContext.Current.Request.Headers["sesionGestor"];
            Guid? gSesionPersona = null;
            Guid? gSesionGestor = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            if (SesionGestor != null && SesionGestor != "") gSesionGestor = new Guid(SesionGestor);
            return db.remBoletaCabeceraAsistenciaFechas(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero).ToList();
        }

        [Route("cabeceraEmpresa/{sEmpresa}")]
        public List<string> GetCabeceraEmpresa([FromUri]  short sEmpresa) {
            List<string> xCabecera = db.remBoletaCabeceraEmpresa(sEmpresa).ToList();
            return xCabecera;
        }

        [Route("cabeceraTitulo")]
        public List<string> PostCabeceraTitulo([FromBody] ParCabecera parCabecera)
        {
            List<string> respuesta = new List<string>();   //  []
            string SesionPersona = HttpContext.Current.Request.Headers["sesionPersona"];
            string SesionGestor = HttpContext.Current.Request.Headers["sesionGestor"];
            Guid? gSesionPersona = null;
            Guid? gSesionGestor = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            if (SesionGestor != null && SesionGestor != "") gSesionGestor = new Guid(SesionGestor);
            int iPersona = (int)db.perAccesoData(gSesionPersona, gSesionGestor).FirstOrDefault();
            if (iPersona >= 0)
            {
                respuesta = db.remBoletaCabeceraTitulo(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero).ToList();
            }
            return respuesta;
        }

        [Route("cabeceraDatos")]
        public List<remBoletaCabeceraDatos> PostCabeceraDatos([FromBody] ParCabeceraDatos parCabecera)
        {
            return db.remBoletaCabeceraDatos(parCabecera.iCalculoPlanilla, parCabecera.bColumna).ToList();
        }

        [Route("concepto")]
        public List<remBoletaConcepto> PostConcepto([FromBody] ParBoletaConcepto parBoletaConcepto)
        {
            List<remBoletaConcepto> respuesta = db.remBoletaConcepto(parBoletaConcepto.iCalculoPlanilla, parBoletaConcepto.bMes, parBoletaConcepto.bNumero, parBoletaConcepto.bClaseConcepto).ToList();
            return respuesta;
        }


        [Route("calculos")]
        public List<belCalculosxPeriodo> GetBelCalculosxPeriodos()
        {
            string SesionPersona = HttpContext.Current.Request.Headers["sesionPersona"];
            string SesionGestor = HttpContext.Current.Request.Headers["sesionGestor"];
            Guid? gSesionPersona = null;
            Guid? gSesionGestor = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            if (SesionGestor != null && SesionGestor != "") gSesionGestor = new Guid(SesionGestor);
            return db.belCalculosxPeriodo(gSesionPersona).ToList();
        }

        [Route("indice")]
        public IndiceBoletas getIndice()
        {
            string SesionPersona = HttpContext.Current.Request.Headers["sesionPersona"];
            string SesionGestor = HttpContext.Current.Request.Headers["sesionGestor"];
            Guid? gSesionPersona = null;
            Guid? gSesionGestor = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            if (SesionGestor != null && SesionGestor != "") gSesionGestor = new Guid(SesionGestor);
            int iPersona = db.autSesion.FirstOrDefault(se => se.gSesion == gSesionPersona).iUsuario;
            IndiceBoletas indice = new IndiceBoletas();
            if (iPersona >= 0)
            {
                indice.contratos = db.traContratosxPersona(iPersona).ToList();
                indice.calculos = db.belCalculos(gSesionPersona).ToList();
            }
            return indice;
        }

        private static DataBoleta remBoleta(ParCabecera parCabecera)
        {
            remCalculoPlanilla calculo = db.remCalculoPlanilla.Find(parCabecera.iCalculoPlanilla);
            traContrato contrato = db.traContrato.Find(calculo.iContrato);
            DataBoleta datas = new DataBoleta();
            datas.cabecera = db.remBoletaCabecera(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero).ToList();
            datas.asistencia = db.remBoletaCabeceraAsistenciaFechas(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero).ToList();
            datas.datosCol1 = db.remBoletaCabeceraDatos(parCabecera.iCalculoPlanilla, 1).ToList();
            datas.datosCol2 = db.remBoletaCabeceraDatos(parCabecera.iCalculoPlanilla, 2).ToList();
            datas.empresa = db.remBoletaCabeceraEmpresa(contrato.sEmpresa).ToList();
            datas.titulo = db.remBoletaCabeceraTitulo(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero).ToList();
            datas.concepto1 = db.remBoletaConcepto(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero, 1).ToList();
            datas.concepto2 = db.remBoletaConcepto(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero, 2).ToList();
            datas.concepto3 = db.remBoletaConcepto(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero, 3).ToList();
            datas.concepto4 = db.remBoletaConcepto(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero, 4).ToList();
            datas.pdf = Pdfs.Boleta(datas,parCabecera);
            return datas;
        }

        #region BOLETA DE PAGO (FIN DE MES)
        [HttpPost, Route("comprobanteBoleta")]
        public async Task<RspComprobanteBoleta> ComprobanteBoleta([FromBody] ParComprobanteBoleta parArg)
        {
            RspComprobanteBoleta rsp = RspComprobanteBoleta();
            string xToken = HttpContext.Request.Headers["token"];
            try
            {
                using (PWAEntities db = new PWAEntities())
                {
                    if (await soporte.esTokenValidoAsync(db, xToken))
                    {
                        remCalculoPlanilla _CP = db.remCalculoPlanilla.Find(parArg.iCalculoPlanilla);
                        remPeriodo = _P = db.remPeriodo.Find(_CP.iPeriodo);
                        orgEmpresa = _E = db.orgEmpresa.Find(_P.sEmpresa);
                        orgEmpresaTelefono = _E = db.orgEmpresaTelefono.Where(c => c.sEmpresa == _P.sEmpresa).FirstOrDefault(); //TODO: Cambiar a lista y concatenar los teléfonos.

                        orgEmpresaDireccion = _ED = db.orgEmpresaDireccion.Find(_P.sEmpresa);
                        /*@xTipoVia = TV.xAbreviado, @xNombreVia = ED.xNombreVia
	, @xNroVia = ED.xNroVia, @xDpto = ED.xDpto, @xInterior = ED.xInterior, @xManzana = ED.xManzana
	, @xLote = ED.xLote, @xKilometro = ED.xKilometro, @xBlock = ED.xBlock, @xEtapa = ED.xEtapa
	, @xTipoZona = TZ.xAbreviado, @xNombreZona = ED.xNombreZona, @xReferencia = ED.xReferencia
	FROM orgEmpresaDireccion ED (NoLock) 
	LEFT JOIN isoTipoVia TV (NoLock) ON (TV.bTipoVia = ED.bTipoVia)
	LEFT JOIN isoTipoZona TZ (NoLock) ON (TZ.bTipoZona = ED.bTipoZona)
	WHERE ED.sEmpresa = @sEmpresa
	
	Set @xDireccion = ''
	If Len(@xTipoVia) > 0
		Set @xDireccion = @xDireccion + ' ' + @xTipoVia
	If Len(@xNombreVia) > 0
		Set @xDireccion = @xDireccion + ' ' + @xNombreVia
	If Len(@xNroVia) > 0
		Set @xDireccion = @xDireccion + ' Nro. ' + @xNroVia
	If Len(@xDpto) > 0
		Set @xDireccion = @xDireccion + ' Dpto. ' + @xDpto
	If Len(@xInterior) > 0
		Set @xDireccion = @xDireccion + ' Interior ' + @xInterior
	If Len(@xManzana) > 0
		Set @xDireccion = @xDireccion + ' Mz. ' + @xManzana
	If Len(@xLote) > 0
		Set @xDireccion = @xDireccion + ' Lt. ' + @xLote
	If Len(@xKilometro) > 0
		Set @xDireccion = @xDireccion + ' Km. ' + @xKilometro
	If Len(@xBlock) > 0
		Set @xDireccion = @xDireccion + ' Block ' + @xBlock
	If Len(@xEtapa) > 0
		Set @xDireccion = @xDireccion + ' Etapa ' + @xEtapa
	If Len(@xTipoZona) > 0 AND Len(@xNombreZona) > 0
		Set @xDireccion = @xDireccion + ' ' + @xTipoZona + ' ' + @xNombreZona
	
	Set @xDireccion = Replace(LTrim(RTrim(@xDireccion)), '  ', ' ')*/


                        rsp.Empresa = new dataEmpresa { Ruc = _E };


                    }
                }
            }

            catch (Exception e)
            {
                Console.WriteLine(e);
            };
            return rsp;
        }

        public class ParComprobanteBoleta
        {
            public int iCalculoPlanilla { get; set; }
            public byte bMes { get; set; }
            public byte bNumero { get; set; }
        }
        public class RspComprobanteBoleta
        {
            public dataEmpresa Empresa { get; set; }
            public dataTitulo Titulo { get; set; }
            public List<remBoletaCabeceraDatos> datosCol1 { get; set; }
            public List<remBoletaCabeceraDatos> datosCol2 { get; set; }
            public List<string> empresa { get; set; }
            public List<string> titulo { get; set; }
            public List<remBoletaCabeceraAsistenciaFechas> asistenciaPeriodos { get; set; }
            public List<dataConcepto> conceptoAsistencia { get; set; }
            public List<dataConcepto> conceptoIngresos { get; set; }
            public List<dataConcepto> conceptoDescuentos { get; set; }
            public List<dataConcepto> conceptoEmpleador { get; set; }
            //public outBoleta pdf { get; set; }
        }
        public class dataEmpresa
        {
            public string xRazonSocial { get; set; }      
            public string cRUC { get; set; }
            public string xDireccion { get; set; }      
            public string xTelefono { get; set; }
            public string xTrabajador { get; set; }
            public decimal rNeto { get; set; }
            public decimal rTipoCambio { get; set; }
            public decimal rPagar { get; set; }
        }
        public class dataTitulo
        {
            public byte titulo { get; set; }
            public byte Periodo { get; set; }
            public int Fechas { get; set; }
            public int Ds { get; set; }
        }
        public class dataConcepto
        {
            public byte Concepto { get; set; }
            public byte Info { get; set; }
            public int xMonto { get; set; }
            public int orden { get; set; }
        }
        #endregion
    }

    public class ParCabecera {
        public byte bMes { get; set; }
        public byte bNumero { get; set; }
        public int iCalculoPlanilla { get; set; }
        public Nullable<short> sServidor { get; set; }
    }

    public class ParBoletaConcepto : ParCabecera {
        public byte bClaseConcepto { get; set; }
    }

    public class ParCabeceraDatos
    {
        public byte bColumna { get; set; }
        public int iCalculoPlanilla { get; set; }
    }

    public class IndiceBoletas
    {
        public List<traContratosxPersona> contratos { get; set; }
        public List<belCalculos> calculos { get; set; }
    }

    public class DataBoleta
    {
        public List<remBoletaCabecera> cabecera { get; set; }
        public List<remBoletaCabeceraAsistenciaFechas> asistencia { get; set; }
        public List<remBoletaCabeceraDatos> datosCol1 { get; set; }
        public List<remBoletaCabeceraDatos> datosCol2 { get; set; }
        public List<string> empresa { get; set; }
        public List<string> titulo { get; set; }
        public List<remBoletaConcepto> concepto1 { get; set; }
        public List<remBoletaConcepto> concepto2 { get; set; }
        public List<remBoletaConcepto> concepto3 { get; set; }
        public List<remBoletaConcepto> concepto4 { get; set; }
        public outBoleta pdf { get; set; }
    }

}

