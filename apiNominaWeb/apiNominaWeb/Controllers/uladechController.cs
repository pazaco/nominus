﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using apiNominaWeb.Models;

namespace apiNominaWeb.Controllers
{

    [RoutePrefix("uladech")]
    public class uladechController : ApiController
    {

        private static uladechEntities dbU = new uladechEntities();
        private static nominusApp2Entities dbN = new nominusApp2Entities();
        private static nominaEntities dbA = new nominaEntities("Uladech08");

        private string xSujeto = "asistencia-from-nominus.pe";
        private string apiUladech = "https://api.uladech.edu.pe:8088/";

        [Route("updEstablecimientos")]
        public string GetUpdEstablecimientos()
        {
            string rsp = "";

            JavaScriptSerializer jss = new JavaScriptSerializer();
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(apiUladech + "tip/establecimientos");
            request.Method = "GET";
            request.ContentType = "application/json;charset=\"utf-8\"";
            request.Headers.Add("xTokenMin", Token.getToken(xSujeto, 128, lUTC: true));
            try
            {
                WebResponse webResp = request.GetResponse();
                Stream webStream = webResp.GetResponseStream();
                StreamReader rspReader = new StreamReader(webStream);
                rsp = rspReader.ReadToEnd();
                rspReader.Close();
                List<Establecimiento> lEstab = jss.Deserialize<List<Establecimiento>>(rsp);
                foreach (Establecimiento cEstab in lEstab)
                {
                    DEPARTMENTS dpt = dbU.DEPARTMENTS.FirstOrDefault(c => c.code == cEstab.cEstablecimiento);
                    if (dpt == null)
                    {
                        dpt = new DEPARTMENTS
                        {
                            code = cEstab.cEstablecimiento,
                            DEPTNAME = cEstab.xEstablecimiento.Length>30? cEstab.xEstablecimiento.Substring(0,30):cEstab.xEstablecimiento,
                            SUPDEPTID = 1,
                            DefaultSchId = 1,
                        };
                        dbU.DEPARTMENTS.Add(dpt);
                        dbU.SaveChanges();
                    }
                    else
                    {
                        string xEstab = cEstab.xEstablecimiento.Length > 30 ? cEstab.xEstablecimiento.Substring(0, 30) : cEstab.xEstablecimiento;
                        if (xEstab != dpt.DEPTNAME)
                        {
                            dpt.DEPTNAME = xEstab;
                            dbU.Entry(dpt).State = System.Data.Entity.EntityState.Modified;
                            dbU.SaveChanges();
                        }
                    }
                    personnel_area par = dbU.personnel_area.FirstOrDefault(c => c.areaid == cEstab.cEstablecimiento);
                    if (par == null)
                    {
                        par = new personnel_area { areaid = cEstab.cEstablecimiento, areaname = cEstab.xEstablecimiento };
                        dbU.personnel_area.Add(par);
                        dbU.SaveChanges();
                    }
                    else
                    {
                        string xEstab = cEstab.xEstablecimiento.Length > 50 ? cEstab.xEstablecimiento.Substring(0, 50) : cEstab.xEstablecimiento;
                        if (xEstab != par.areaname)
                        {
                            par.areaname = xEstab;
                            dbU.Entry(par).State = System.Data.Entity.EntityState.Modified;
                            dbU.SaveChanges();
                        }
                    }
                }
                rsp = "Ok";
            }
            catch (Exception e)
            {
                throw e;
            }
            return rsp;
        }

        [Route("oficinas")]
        public List<Oficina> GetOficinas()
        {
            List<Oficina> rsp = new List<Oficina>();

            JavaScriptSerializer jss = new JavaScriptSerializer();
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(apiUladech + "tip/oficinas");
            request.Method = "GET";
            request.ContentType = "application/json;charset=\"utf-8\"";
            request.Headers.Add("xTokenMin", Token.getToken(xSujeto, 128, lUTC: true));
            try
            {
                WebResponse webResp = request.GetResponse();
                Stream webStream = webResp.GetResponseStream();
                StreamReader rspReader = new StreamReader(webStream);
                string xRsp = rspReader.ReadToEnd();
                rspReader.Close();
                rsp = jss.Deserialize<List<Oficina>>(xRsp);

            }
            catch
            {
                rsp = null;
            }
            return rsp;
        }

        [Route("cargos")]
        public List<Cargo> GetCargos()
        {
            List<Cargo> rsp = new List<Cargo>();

            JavaScriptSerializer jss = new JavaScriptSerializer();
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(apiUladech + "tip/cargos");
            request.Method = "GET";
            request.ContentType = "application/json;charset=\"utf-8\"";
            request.Headers.Add("xTokenMin", Token.getToken(xSujeto, 128, lUTC: true));
            try
            {
                WebResponse webResp = request.GetResponse();
                Stream webStream = webResp.GetResponseStream();
                StreamReader rspReader = new StreamReader(webStream);
                string xRsp = rspReader.ReadToEnd();
                rspReader.Close();
                rsp = jss.Deserialize<List<Cargo>>(xRsp);

            }
            catch
            {
                rsp = null;
            }
            return rsp;
        }

        [Route("establecimientos")]
        public List<Establecimiento> GetEstablecimientos()
        {
            List<Establecimiento> rsp = new List<Establecimiento>();

            JavaScriptSerializer jss = new JavaScriptSerializer();
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(apiUladech + "tip/establecimientos");
            request.Method = "GET";
            request.ContentType = "application/json;charset=\"utf-8\"";
            request.Headers.Add("xTokenMin", Token.getToken(xSujeto, 128, lUTC: true));
            try
            {
                WebResponse webResp = request.GetResponse();
                Stream webStream = webResp.GetResponseStream();
                StreamReader rspReader = new StreamReader(webStream);
                string xRsp = rspReader.ReadToEnd();
                rspReader.Close();
                rsp = jss.Deserialize<List<Establecimiento>>(xRsp);
            }
            catch
            {
                rsp = null;
            }
            return rsp;
        }

        [Route("zkEstablecimientos"), HttpGet]
        public List<ZkDepartment> ZkEstablecimientos()
        {
            return dbU.DEPARTMENTS.Select(c => new ZkDepartment { deptid = c.DEPTID, deptname = c.DEPTNAME, code = c.code }).ToList();
        }

        [Route("situaciones")]
        public List<Situacion> GetSituacions()
        {
            List<Situacion> rsp = new List<Situacion>();

            JavaScriptSerializer jss = new JavaScriptSerializer();
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(apiUladech + "tip/situaciones");
            request.Method = "GET";
            request.ContentType = "application/json;charset=\"utf-8\"";
            request.Headers.Add("xTokenMin", Token.getToken(xSujeto, 128, lUTC: true));
            try
            {
                WebResponse webResp = request.GetResponse();
                Stream webStream = webResp.GetResponseStream();
                StreamReader rspReader = new StreamReader(webStream);
                string xRsp = rspReader.ReadToEnd();
                rspReader.Close();
                rsp = jss.Deserialize<List<Situacion>>(xRsp);
            }
            catch
            {
                rsp = null;
            }
            return rsp;
        }

        [HttpPost, Route("persona")]
        public List<RspBuscaPersona> buscaPersonas([FromBody] ParBuscaPersona per)
        {
            return WareHouse.BuscaPersonas(per);
        }

        [HttpPost, Route("reubicacion")]
        public List<string> reubicaciones([FromBody] ParBuscaPersona per)
        {
            List<string> rsp = new List<string>();
            List<RspBuscaPersona> lstPer = WareHouse.BuscaPersonas(per);
            List<DEPARTMENTS> dep = dbU.DEPARTMENTS.ToList();
            foreach (RspBuscaPersona cPer in lstPer)
            {
                USERINFO cem = dbU.USERINFO.FirstOrDefault(c => c.Badgenumber == cPer.cCodigoEmpleado);
                if (cem == null)
                {
                    rsp.Add(cPer.cCodigoEmpleado);
                    RevisionEmpleado rs = GetEmpleado(cPer.cCodigoEmpleado);
                }
                else
                {
                    int iEstable = dep.FirstOrDefault(c => c.code == cPer.cEstablecimiento[0]).DEPTID;
                    USERINFO cje = dbU.USERINFO.FirstOrDefault(c => c.Badgenumber == cPer.cJefeDirecto);
                    if (cem.DEFAULTDEPTID != iEstable)
                    {
                        cem.DEFAULTDEPTID = iEstable;
                        dbU.Entry(cem).State = System.Data.Entity.EntityState.Modified;
                        dbU.SaveChanges();
                    }
                    if (dbU.acc_levelset_emp.FirstOrDefault(c => c.employee_id == cem.USERID) == null)
                    {
                        dbU.acc_levelset_emp.Add(new acc_levelset_emp { employee_id = cem.USERID, acclevelset_id = 1 });
                        dbU.SaveChanges();
                    }
                    ajustarJefe(cem.USERID, cPer.cJefeDirecto);
                }
            }
            return rsp;
        }

        private void ajustarJefe(int iPersona, string cJefe)
        {
            USERINFO cje = dbU.USERINFO.FirstOrDefault(c => c.Badgenumber == cJefe);
            if (cje != null)
            {
                asi_Jefatura jefe = dbA.asi_Jefatura.FirstOrDefault(c => c.iContrato == iPersona);
                if (jefe == null)
                {
                    dbA.asi_Jefatura.Add(new asi_Jefatura
                    {
                        iContrato = iPersona,
                        iContratoJefe = cje.USERID,
                        bTipoJefe = 1
                    });
                    dbA.SaveChanges();
                }
                else
                {
                    if (jefe.iContratoJefe != cje.USERID)
                    {
                        jefe.iContratoJefe = cje.USERID;
                        dbA.Entry(jefe).State = System.Data.Entity.EntityState.Modified;
                        dbA.SaveChanges();
                    }
                }
            }

        }

        [Route("crearPruebaQR/{iEmpleado}"), HttpGet]
        public void CrearPruebaQR([FromUri] int iEmpleado)
        {
            dbA.asi_CrearPruebaQR(iEmpleado);
        }

        [Route("borrarPruebaQR/{iEmpleado}"), HttpGet]
        public void BorrarPruebaQR([FromUri] int iEmpleado)
        {
            dbA.asi_BorrarPruebaQR(iEmpleado);
        }

        [Route("reclutamiento/{iPersona}")]
        public List<Reclutamiento> GetReclutamiento([FromUri] int iPersona)
        {
            List<TEMPLATE> dtem = dbU.TEMPLATE.Where(c => c.USERID == iPersona).ToList();
            List<Reclutamiento> rcl = dtem.GroupBy(info => info.FINGERID)
                .Select(grupo => new Reclutamiento { iFinger = grupo.Key, v10 = grupo.Count() })
                .ToList();
            return rcl;
        }

        [Route("zkRetiros")]
        public void GetRetiros()
        {
            List<USERINFO> _pkUsr = dbU.USERINFO.ToList();
            foreach (USERINFO _pk1 in _pkUsr)
            {
                if (WareHouse.EmpleadoUladech(_pk1.Badgenumber) == null)
                {
                    _pk1.DEFAULTDEPTID = 68;
                    dbU.Entry(_pk1).State = System.Data.Entity.EntityState.Modified;
                    List<acc_levelset_emp> _ale = dbU.acc_levelset_emp.Where(c => c.employee_id == _pk1.USERID).ToList();
                    dbU.acc_levelset_emp.RemoveRange(_ale);
                    dbU.SaveChanges();
                }
            }
        }

        [Route("zkAdiciones/{topeSup}")]
        public void GetZkAdiciones([FromUri] int topeSup)
        {
            if (topeSup < 280800) topeSup = 280800;
            for (int ii = 2797; ii <= topeSup; ii++)
            {
                string cEmpleado = string.Format("{0:000000}", ii);
                RevisionEmpleado rsp = new RevisionEmpleado();
                rsp.ouchEmpleado = WareHouse.EmpleadoUladech(cEmpleado);
                if (rsp.ouchEmpleado != null)
                {
                    USERINFO _pkUsr = dbU.USERINFO.FirstOrDefault(c => c.Badgenumber == cEmpleado);
                    if (_pkUsr == null)
                    {
                        string cEstablec = rsp.ouchEmpleado.cEstablecimiento[0];
                        DEPARTMENTS oDepto = dbU.DEPARTMENTS.FirstOrDefault(c => c.code == cEstablec);
                        string xPnemo = rsp.ouchEmpleado.xEmail.Substring(0, rsp.ouchEmpleado.xEmail.IndexOf("@") + 1);
                        _pkUsr = new USERINFO
                        {
                            Badgenumber = cEmpleado,
                            SSN = "0",
                            Name = xPnemo,
                            Gender = rsp.ouchEmpleado.cGenero,
                            TITLE = (rsp.ouchEmpleado.xCargo.Length > 20) ? rsp.ouchEmpleado.xCargo.Substring(0, 20) : rsp.ouchEmpleado.xCargo,
                            DEFAULTDEPTID = oDepto.DEPTID,
                            HIREDDAY = rsp.ouchEmpleado.dDesde,
                            BIRTHDAY = rsp.ouchEmpleado.dNacimiento,
                            lastname = rsp.ouchEmpleado.xApellidos + " " + rsp.ouchEmpleado.xNombre,
                            email = rsp.ouchEmpleado.xEmail
                        };
                        dbU.USERINFO.Add(_pkUsr);
                        dbU.SaveChanges();
                        dbU.acc_levelset_emp.Add(new acc_levelset_emp
                        {
                            acclevelset_id = 1,
                            employee_id = _pkUsr.USERID
                        });
                        dbU.SaveChanges();
                        WareHouse.EnlazarAppNominus(new ParEnlazar { iUsuario = _pkUsr.USERID, sServidor = 8, xCorreo = rsp.ouchEmpleado.xEmail });
                    }
                    rsp.ouchEmpleado.iZkEmpleado = _pkUsr.USERID;
                }
            }
        }

        [Route("zkSincronizacionCompleta")]
        public void GetZkSincroFull()
        {
            List<string> _indice = WareHouse.IndiceUladech();


            foreach (string cEmpleado in _indice)
            {
                RevisionEmpleado rsp = new RevisionEmpleado();
                rsp.ouchEmpleado = WareHouse.EmpleadoUladech(cEmpleado);
                if (rsp.ouchEmpleado != null)
                {
                    USERINFO _pkUsr = dbU.USERINFO.FirstOrDefault(c => c.Badgenumber == cEmpleado);
                    string xPnemo = rsp.ouchEmpleado.xEmail.IndexOf("@") > 0 ? rsp.ouchEmpleado.xEmail.Substring(0, rsp.ouchEmpleado.xEmail.IndexOf("@")) : rsp.ouchEmpleado.xNombre;
                    if (_pkUsr == null)
                    {
                        string cEstablec = rsp.ouchEmpleado.cEstablecimiento[0];
                        DEPARTMENTS oDepto = dbU.DEPARTMENTS.FirstOrDefault(c => c.code == cEstablec);

                        _pkUsr = new USERINFO
                        {
                            Badgenumber = cEmpleado,
                            SSN = "0",
                            Name = xPnemo,
                            Gender = rsp.ouchEmpleado.cGenero,
                            TITLE = (rsp.ouchEmpleado.xCargo.Length > 20) ? rsp.ouchEmpleado.xCargo.Substring(0, 20) : rsp.ouchEmpleado.xCargo,
                            DEFAULTDEPTID = oDepto.DEPTID,
                            HIREDDAY = rsp.ouchEmpleado.dDesde,
                            BIRTHDAY = rsp.ouchEmpleado.dNacimiento,
                            lastname = rsp.ouchEmpleado.xApellidos + " " + rsp.ouchEmpleado.xNombre,
                            email = rsp.ouchEmpleado.xEmail
                        };
                        dbU.USERINFO.Add(_pkUsr);
                        dbU.SaveChanges();
                        dbU.acc_levelset_emp.Add(new acc_levelset_emp
                        {
                            acclevelset_id = 1,
                            employee_id = _pkUsr.USERID
                        });
                        dbU.SaveChanges();
                        ajustarJefe(_pkUsr.USERID, rsp.ouchEmpleado.cJefeDirecto);
                        WareHouse.EnlazarAppNominus(new ParEnlazar { iUsuario = _pkUsr.USERID, sServidor = 8, xCorreo = rsp.ouchEmpleado.xEmail });
                    }
                    else
                    {
                        _pkUsr.SSN = "0";
                        _pkUsr.Name = xPnemo;
                        _pkUsr.Gender = rsp.ouchEmpleado.cGenero;
                        _pkUsr.TITLE = (rsp.ouchEmpleado.xCargo.Length > 20) ? rsp.ouchEmpleado.xCargo.Substring(0, 20) : rsp.ouchEmpleado.xCargo;
                        _pkUsr.HIREDDAY = rsp.ouchEmpleado.dDesde;
                        _pkUsr.BIRTHDAY = rsp.ouchEmpleado.dNacimiento;
                        _pkUsr.lastname = rsp.ouchEmpleado.xApellidos + " " + rsp.ouchEmpleado.xNombre;
                        _pkUsr.email = rsp.ouchEmpleado.xEmail;
                        dbU.Entry(_pkUsr).State = System.Data.Entity.EntityState.Modified;
                        dbU.SaveChanges();
                        ajustarJefe(_pkUsr.USERID, rsp.ouchEmpleado.cJefeDirecto);
                    }
                    rsp.ouchEmpleado.iZkEmpleado = _pkUsr.USERID;
                }
            }
        }

        [Route("empleado/{cEmpleado}")]
        public RevisionEmpleado GetEmpleado([FromUri] string cEmpleado)
        {
            RevisionEmpleado rsp = new RevisionEmpleado();
            rsp.ouchEmpleado = WareHouse.EmpleadoUladech(cEmpleado);
            if (rsp.ouchEmpleado != null)
            {
                USERINFO _pkUsr = dbU.USERINFO.FirstOrDefault(c => c.Badgenumber == cEmpleado);
                string xPnemo = rsp.ouchEmpleado.xEmail.Substring(0, rsp.ouchEmpleado.xEmail.IndexOf("@") + 1);
                if (_pkUsr == null)
                {
                    string cEstablec = rsp.ouchEmpleado.cEstablecimiento[0];
                    DEPARTMENTS oDepto = dbU.DEPARTMENTS.FirstOrDefault(c => c.code == cEstablec);
                    _pkUsr = new USERINFO
                    {
                        Badgenumber = cEmpleado,
                        SSN = "0",
                        Name = xPnemo,
                        Gender = rsp.ouchEmpleado.cGenero,
                        TITLE = (rsp.ouchEmpleado.xCargo.Length > 20) ? rsp.ouchEmpleado.xCargo.Substring(0, 20) : rsp.ouchEmpleado.xCargo,
                        DEFAULTDEPTID = oDepto.DEPTID,
                        HIREDDAY = rsp.ouchEmpleado.dDesde,
                        BIRTHDAY = rsp.ouchEmpleado.dNacimiento,
                        lastname = rsp.ouchEmpleado.xApellidos + " " + rsp.ouchEmpleado.xNombre,
                        email = rsp.ouchEmpleado.xEmail
                    };
                    dbU.USERINFO.Add(_pkUsr);
                    dbU.SaveChanges();
                    dbU.acc_levelset_emp.Add(new acc_levelset_emp
                    {
                        acclevelset_id = 1,
                        employee_id = _pkUsr.USERID
                    });
                    dbU.SaveChanges();
                    ajustarJefe(_pkUsr.USERID, rsp.ouchEmpleado.cJefeDirecto);
                    WareHouse.EnlazarAppNominus(new ParEnlazar { iUsuario = _pkUsr.USERID, sServidor = 8, xCorreo = rsp.ouchEmpleado.xEmail });
                }
                rsp.ouchEmpleado.iZkEmpleado = _pkUsr.USERID;
            }
            return rsp;
        }

        [Route("empleados")]
        public List<rowEmpleado> PostEmpleado([FromBody] ParEmpleados par)
        {
            List<rowEmpleado> rsp = new List<rowEmpleado>();
            JavaScriptSerializer jss = new JavaScriptSerializer();
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(apiUladech + "per/personas");
            request.Method = "POST";
            request.ContentType = "application/json;charset=\"utf-8\"";
            request.Headers.Add("xTokenMin", Token.getToken(xSujeto, 128, lUTC: true));
            string xPush = jss.Serialize(par);
            request.ContentLength = xPush.Length;
            StreamWriter requestWriter = new StreamWriter(request.GetRequestStream(), System.Text.Encoding.ASCII);
            requestWriter.Write(xPush);
            requestWriter.Close();
            try
            {
                WebResponse webResp = request.GetResponse();
                Stream webStream = webResp.GetResponseStream();
                StreamReader rspReader = new StreamReader(webStream);
                string xRsp = rspReader.ReadToEnd();
                rspReader.Close();
                rsp = jss.Deserialize<List<rowEmpleado>>(xRsp);
            }
            catch
            {

                rsp = null;
            }
            if (par.bCantidad != null && par.bCantidad > 0 && rsp != null)
            {
                return rsp.Take((int)par.bCantidad).ToList();
            }
            else
                return rsp;
        }

        [Route("diario/{dFecha}"), HttpGet]
        public async System.Threading.Tasks.Task<string> HorarioDiarioAsync([FromUri] DateTime dFecha)
        {
            return await WareHouse.HorarioDiarioAsync(dFecha);
        }

        [Route("sincronizarMarcas"), HttpGet]
        public void SincronizarMarcas()
        {
            dbA.asi_SincronizarMarcacion();
        }

        [Route("preDiarioDocente/{dFecha}"), HttpGet]
        public List<asi_HorarioDiario> PreHorarioDiario([FromUri] DateTime dFecha)
        {
            return WareHouse.PreDiarioDocente(dFecha);
        }

        [Route("cuentasEmpleado/{cEmpleado}"), HttpGet]
        public List<string> verCuentasxEmpleado([FromUri] string cEmpleado)
        {
            List<string> rsp = null;
            while (cEmpleado.Count() < 6)
            {
                cEmpleado = "0" + cEmpleado;
            }

            USERINFO _userinfo = dbU.USERINFO.Where(c => c.Badgenumber == cEmpleado).FirstOrDefault();
            if (_userinfo != null)
            {
                List<authAcceso> _acc = dbN.authAcceso.Where(c => c.sServidor == 8 && c.iPersona == _userinfo.USERID).ToList();
                rsp = dbN.authUsuario
                    .ToList()
                    .Join(_acc, kp => kp.gUsuario, ks => ks.gUsuario, (kp, ks) => new { kp, ks })
                    .Select(s => s.kp.email).ToList();
            }
            return rsp;
        }

        [Route("delCuentaEmpleado"), HttpPost]
        public void delCuentaEmpleado([FromBody] string xCuenta)
        {
            authUsuario _usr = dbN.authUsuario.Where(c => c.email == xCuenta).FirstOrDefault();
            if (_usr != null)
            {
                authAcceso _acc = dbN.authAcceso.Where(c => c.gUsuario == _usr.gUsuario && c.sServidor == 8).FirstOrDefault();
                if (_acc != null)
                {
                    dbN.authAcceso.Remove(_acc);
                    dbN.SaveChanges();
                }

            }
        }

        [Route("autorizarCuentaGoogle"), HttpPost]
        public string autorizarCtaGoogle([FromBody] ParCuentaGoogle cta)
        {
            string rsp = "Permiso concedido a " + cta.email + ".";
            authUsuario _usu = dbN.authUsuario.Where(c => c.email == cta.email).FirstOrDefault();
            if (_usu == null)
            {
                authProvider _prv = dbN.authProvider.Where(c => c.providerId == "google.com").FirstOrDefault();
                _usu = new authUsuario
                {
                    gUsuario = Guid.NewGuid(),
                    bProvider = _prv.bProvider,
                    email = cta.email
                };
                dbN.authUsuario.Add(_usu);
                dbN.SaveChanges();
            }
            authAcceso _acc = dbN.authAcceso.Where(c => c.gUsuario == _usu.gUsuario && c.sServidor == 8).FirstOrDefault();
            if (_acc == null)
            {
                _acc = new authAcceso
                {
                    gAcceso = Guid.NewGuid(),
                    gUsuario = _usu.gUsuario,
                    iPersona = cta.iPersona,
                    sServidor = 8
                };
                dbN.authAcceso.Add(_acc);
                dbN.SaveChanges();
            }
            else
            {
                if (cta.iPersona != _acc.iPersona)
                {
                    rsp = "No es posible matricular la cuenta " + cta.email + ".";
                }
            }
            return rsp;
        }

    }

    public class ParCuentaGoogle
    {
        public string email { get; set; }
        public int iPersona { get; set; }
    }

    public class ZkDepartment
    {
        public int deptid { get; set; }
        public string deptname { get; set; }
        public string code { get; set; }
    }
    
    public class Establecimiento
    {
        public string cEstablecimiento { get; set; }
        public string xEstablecimiento { get; set; }
    }

    public class Situacion
    {
        public string cSituacion { get; set; }
        public string xSituacion { get; set; }
    }

    public class Oficina
    {
        public string cOficina { get; set; }
        public string xEstablecimiento { get; set; }
    }

    public class Cargo
    {
        public string cCargo { get; set; }
        public string xCargo { get; set; }
    }

    public class RevisionEmpleado
    {
        public OuchEmpleado ouchEmpleado { get; set; }
    }

    public class ParEmpleados
    {
        public string xCargoParcial { get; set; }
        public string xNombreParcial { get; set; }
        public string xJefe { get; set; }
        public string xSituacion { get; set; }
        public Nullable<int> bCantidad { get; set; }
    }

    public class rowEmpleado
    {
        public string xCargo { get; set; }
        public string xEmail { get; set; }
        public string xNombre { get; set; }
        public string cSituacion { get; set; }
        public string xApellidos { get; set; }
        public string cJefeDirecto { get; set; }
        public string cCodigoEmpleado { get; set; }
        public DateTime? dDesde { get; set; }
        public DateTime? dHasta { get; set; }
        public DateTime? dNacimiento { get; set; }
        public string cGenero { get; set; }
        public List<string> cEstablecimiento { get; set; }
    }

    public class Reclutamiento
    {
        public int iFinger { get; set; }
        public int v10 { get; set; }
    }

}

