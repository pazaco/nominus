﻿using apiNominaWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Http;

namespace apiNominaWeb.Controllers
{

    [RoutePrefix("tipologia")]
    public class TipologiaController : ApiController
    {
        public static nominusApp2Entities dbA = new nominusApp2Entities();

        [Route("tiposDocIdentidad")]
        public List<isoTipoDocIdentidad> getTiposDocIdentidad()
        {
            nominaEntities db = new nominaEntities(WareHouse.ServerConnection(HttpContext.Current.Request.Headers["servidor"]));
            return db.isoTipoDocIdentidad.ToList();
        }

        [Route("ipAddress")]
        public string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            return "No network adapters with an IPv4 address in the system!";
        }

        [Route("monedas")]
        public List<isoMoneda> getMonedas()
        {
            nominaEntities db = new nominaEntities(WareHouse.ServerConnection(HttpContext.Current.Request.Headers["servidor"]));
            return db.isoMoneda.ToList();
        }

    }

}
