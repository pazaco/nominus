﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web.Http;
using System.Web.Script.Serialization;
using apiNominaWeb.Models;
using Newtonsoft.Json;

namespace apiNominaWeb.Controllers
{
    [RoutePrefix("asi")]
    public class asistenciaController : ApiController
    {
        private static nominusApp2Entities dbA = new nominusApp2Entities();
        private static uladechEntities dbU = new uladechEntities();

        [Route("addPoligono"), HttpPost]
        public void addPoligono([FromBody] RspGeoUbicacion poli)
        {
            authServidor _srv = dbA.authServidor.Find(8);
            nominaEntities db = new nominaEntities(_srv.xConexion);
            short sPosicion = 0;
            foreach (AgmPunto pto in poli.puntos)
            {
                db.orgGeoEstablecimiento.Add(new orgGeoEstablecimiento
                {
                    cEstablecimiento = poli.cEstablecimiento,
                    sOrtogono = poli.sOrtogono,
                    sPosicion = sPosicion++,
                    fLat = pto.lat,
                    fLon = pto.lng
                });
            }
            db.SaveChanges();
        }

        [Route("delPoligono"), HttpPost]
        public void delPoligono([FromBody] ParPoligono poli)
        {
            authServidor _srv = dbA.authServidor.Find(8);
            nominaEntities db = new nominaEntities(_srv.xConexion);
            List<orgGeoEstablecimiento> borrar = db.orgGeoEstablecimiento
                .Where(c => c.cEstablecimiento == poli.cEstablecimiento && c.sOrtogono == poli.sOrtogono)
                .ToList();
            db.orgGeoEstablecimiento.RemoveRange(borrar);
            db.SaveChanges();
        }

        [Route("geoUbicacion"), HttpPost]
        public List<RspGeoUbicacion> GeoUBicacion([FromBody] ParCaminar caminar)
        {
            List<RspGeoUbicacion> rsp = null;
            if (caminar != null)
            {
                authServidor _srv = dbA.authServidor.Find(8);
                nominaEntities db = new nominaEntities(_srv.xConexion);
                rsp = db.orgGeoEstablecimiento
                    .Join(caminar.establecimientos, c1 => c1.cEstablecimiento, c2 => c2, (c1, c2) => new { c1, c2 })
                    .Select(s => new RspGeoUbicacion { cEstablecimiento = s.c1.cEstablecimiento, sOrtogono = s.c1.sOrtogono, lMatch = false })
                    .Distinct().ToList();
                foreach (RspGeoUbicacion poligono in rsp)
                {
                    List<GeoPunto> ortogono = db.orgGeoEstablecimiento
                        .Where(c => c.cEstablecimiento == poligono.cEstablecimiento && c.sOrtogono == poligono.sOrtogono)
                        .OrderBy(o => o.sPosicion)
                        .Select(s => new GeoPunto { fLat = s.fLat, fLon = s.fLon })
                        .ToList();
                    poligono.lMatch = WareHouse.RayCasting(caminar.punto, ortogono);
                    poligono.puntos = ortogono.Select(cc => new AgmPunto { lat = (double)cc.fLat, lng = (double)cc.fLon }).ToList();
                }
            }
            return rsp;
        }

        [Route("poligono"), HttpPost]
        public List<GeoPunto> Poligono([FromBody] ParPoligono poli)
        {
            authServidor _srv = dbA.authServidor.Find(8);
            nominaEntities db = new nominaEntities(_srv.xConexion);
            return db.orgGeoEstablecimiento
                .Where(c => c.cEstablecimiento == poli.cEstablecimiento && c.sOrtogono == poli.sOrtogono)
                .OrderBy(c => c.sPosicion)
                .Select(c => new GeoPunto { fLat = c.fLat, fLon = c.fLon })
                .ToList();
        }

        [Route("diarioEmpleado"), HttpPost]
        public RspDiarioEmpleado asiDiarioEmpleado([FromBody] parDiaEmpleado diaempleado)
        {
            authServidor _srv = dbA.authServidor.Find(8);
            nominaEntities db = new nominaEntities(_srv.xConexion);
            RspDiarioEmpleado rsp = new RspDiarioEmpleado();
            diaempleado.dDia = diaempleado.dDia.Date;
            asi_Asistencia _asi = db.asi_Asistencia.FirstOrDefault(c => c.iContrato == diaempleado.iEmpleado && c.dFecha == diaempleado.dDia);
            if (_asi != null)
            {
                rsp.oAsistencia = new RspDiarioAsistencia();
                rsp.oAsistencia.iAsistencia = _asi.iAsistencia;
                rsp.oAsistencia.dFecha = _asi.dFecha;
                rsp.oAsistencia.iContrato = _asi.iContrato;
                rsp.oAsistencia.fDescuento = _asi.fDescuento;
                rsp.oAsistencia.fExtra = _asi.fExtra;
                rsp.oAsistencia.cPlanilla = _asi.cPlanilla;
                rsp.oAsistencia.iCalculoPlanilla = _asi.iCalculoPlanilla;
                rsp.oAsistencia.sEstablecimiento = _asi.sEstablecimiento;
                rsp.oAsistencia.aFlujo = db.asi_FlujoDiarioEmpleado(_asi.iAsistencia).ToList();
                rsp.empleado = WareHouse.EmpleadoUladech(diaempleado.iEmpleado);
            }
            List<asi_Dispositivo> _dsp = db.asi_Dispositivo.ToList();
            rsp.aMarcacion = db.asi_MarcacionDiarioEmpleado(diaempleado.iEmpleado, diaempleado.dDia).ToList();
            return rsp;
        }

        [Route("mensualEmpleado"), HttpPost]
        public List<asi_CalificacionMensual> calificacionMensual([FromBody] parDiaEmpleado mesEmpleado)
        {
            authServidor _srv = dbA.authServidor.Find(8);
            nominaEntities db = new nominaEntities(_srv.xConexion);
            return db.asi_CalificacionMensual(mesEmpleado.iEmpleado, mesEmpleado.dDia).ToList();
        }

        [Route("grafControlxEstablecimiento/{dDia}"), HttpGet]
        public List<asi_aggControlxEstablecimiento> grafCtlEstablecimiento([FromUri] DateTime dDia)
        {
            authServidor _srv = dbA.authServidor.Find(8);
            nominaEntities db = new nominaEntities(_srv.xConexion);
            return db.asi_aggControlxEstablecimiento(dDia).ToList();
        }

        [Route("grafMarcacion/{dDia}"), HttpGet]
        public RspGrafMarcacion grafMarcacion(DateTime dDia)
        {
           RspGrafMarcacion rsp = new RspGrafMarcacion();
            authServidor _srv = dbA.authServidor.Find(8);
            nominaEntities db = new nominaEntities(_srv.xConexion);
            rsp.establecimientos = dbU.DEPARTMENTS.ToList();
            rsp.grafMarcados = db.asi_grafMarcado(dDia).ToList();
            return rsp;
        }

        [Route("grafReclutamiento"), HttpGet]
        public asi_Reclutamiento grafReclutamiento()
        {
            authServidor _srv = dbA.authServidor.Find(8);
            nominaEntities db = new nominaEntities(_srv.xConexion);
            return db.asi_aggReclutamientoGlobal().FirstOrDefault();
        }

        [Route("revaluarDia/{dDia}"), HttpGet]
        public void revaluarDia([FromUri] DateTime dDia)
        {
            authServidor _srv = dbA.authServidor.Find(8);
            nominaEntities db = new nominaEntities(_srv.xConexion);
            db.asi_RevaluarDia(dDia);
        }

        [Route("revaluar/{iAsistencia}"), HttpGet]
        public ObjectResult<string> RevaluarAsistencia([FromUri] int iAsistencia)
        {
            authServidor _srv = dbA.authServidor.Find(8);
            nominaEntities db = new nominaEntities(_srv.xConexion);
            return db.asi_Revaluar(iAsistencia);
        }

        [Route("agregarMarcacion"), HttpPost]
        public int AgregarMarca([FromBody] parMarca diaEmpleado)
        {
            if (diaEmpleado.cEstablecimiento == null) { diaEmpleado.cEstablecimiento = "0000"; }
            if (diaEmpleado.sDispositivo == null) { diaEmpleado.sDispositivo = 4; }
            authServidor _srv = dbA.authServidor.Find(8);
            nominaEntities db = new nominaEntities(_srv.xConexion);
            short sEstablecimiento = (short)dbU.DEPARTMENTS.FirstOrDefault(c => c.code == diaEmpleado.cEstablecimiento).DEPTID;

            asi_Marcacion _nvaMarca = new asi_Marcacion
            {
                iPersona = diaEmpleado.iEmpleado,
                dHora = diaEmpleado.dDia,
                sDispositivo = diaEmpleado.sDispositivo,
                sEstablecimiento = sEstablecimiento
            };
            db.asi_Marcacion.Add(_nvaMarca);
            db.SaveChanges();
            return _nvaMarca.iMarcacion;
        }

        [Route("agregarMarcaQR"), HttpPost]
        public string AgregarMarcaQR([FromBody] parMarcaQR marcaQR)
        {
            authServidor _srv = dbA.authServidor.Find(8);
            nominaEntities db = new nominaEntities(_srv.xConexion);
            string hashLocalUtc = Token.getToken(marcaQR.aula + "&&uladech-asistencia", 128, lUTC: true);
            string hashLocal = Token.getToken(marcaQR.aula + "&&uladech-asistencia", 128, lUTC: false);
            string rsp = "Intente de nuevo por favor...";
            //if (marcaQR.hash == hashLocal || marcaQR.hash == hashLocalUtc || true)
            //{
                rsp = db.asi_agregarMarcaQR(marcaQR.aula, marcaQR.hora, marcaQR.iAsistencia).FirstOrDefault().ToString();
            //}
            return rsp;
        }


        [Route("agregaMarca"), HttpPost]
        public RspDiarioEmpleado AgregaMarca([FromBody] parMarca diaEmpleado)
        {
            if (diaEmpleado.cEstablecimiento == null) { diaEmpleado.cEstablecimiento = "0000"; }
            if (diaEmpleado.sDispositivo == null) { diaEmpleado.sDispositivo = 4; }
            authServidor _srv = dbA.authServidor.Find(8);
            nominaEntities db = new nominaEntities(_srv.xConexion);

            short sEstablecimiento = (short)dbU.DEPARTMENTS.FirstOrDefault(c => c.code == diaEmpleado.cEstablecimiento).DEPTID;

            asi_Marcacion _nvaMarca = new asi_Marcacion
            {
                iPersona = diaEmpleado.iEmpleado,
                dHora = diaEmpleado.dDia,
                sDispositivo = diaEmpleado.sDispositivo,
                sEstablecimiento = sEstablecimiento
            };
            db.asi_Marcacion.Add(_nvaMarca);
            db.SaveChanges();
            parDiaEmpleado pDE = new parDiaEmpleado { iEmpleado = diaEmpleado.iEmpleado, dDia = diaEmpleado.dDia };
            RspDiarioEmpleado de = asiDiarioEmpleado(pDE);
            string dd = db.asi_Revaluar(de.oAsistencia.iAsistencia).ToString();
            return de;
        }

        [Route("papeleta"),HttpPost]
        public NuevaPapeleta CrearPapeleta([FromBody] NuevaPapeleta creaPap)
        {
            authServidor _srv = dbA.authServidor.Find(8);
            nominusEntities db = new nominusEntities();
            int? proxPapeleta = db.asi_Papeleta.Where(c => c.bTipoPapeleta == creaPap.papeleta.bTipoPapeleta).Max(c => c.iPapeleta);
            if (proxPapeleta == null) proxPapeleta = 1; else proxPapeleta++;
            creaPap.papeleta.iPapeleta = (int)proxPapeleta;
            creaPap.resolucion.ForEach(c => c.iPapeleta = (int)proxPapeleta);
            creaPap.flujo.iPapeleta = (int)proxPapeleta;
            db.asi_Papeleta.Add(creaPap.papeleta);
            db.SaveChanges();
            creaPap.resolucion.ForEach(c => {
                db.asi_Resolucion.Add(c);
            });
            db.asi_FlujoPapeleta.Add(creaPap.flujo);
            db.SaveChanges();
            db.asi_CompletarPapeleta(creaPap.papeleta.bTipoPapeleta, creaPap.papeleta.iPapeleta);
            return creaPap;
        }


        [Route("diarioIncidencias"), HttpPost]
        public ObjectResult<asi_DiarioIncidencias> DiarioIncidencias([FromBody] ParDiarioIncidencias parDiarioIncidencias)
        {
            authServidor _srv = dbA.authServidor.Find(8);
            nominaEntities db = new nominaEntities(_srv.xConexion);
            return db.asi_DiarioIncidencias(parDiarioIncidencias.dFecha, parDiarioIncidencias.sEstablecimiento, parDiarioIncidencias.xTitulo, parDiarioIncidencias.cGenero);
        }

        [Route("diarioEstablecimientos/{dia}"), HttpGet]
        public ObjectResult<asi_DiarioEstablecimientos> DiarioEstablecimientos([FromUri] DateTime dia)
        {
            authServidor _srv = dbA.authServidor.Find(8);
            nominaEntities db = new nominaEntities(_srv.xConexion);
            return db.asi_DiarioEstablecimientos(dia);
        }

        [Route("diarioAsistencia"), HttpPost]
        public rspDiarioAsistencia DiarioAsistencia([FromBody] parDiarioEstablecimiento parDiario)
        {
            rspDiarioAsistencia rsp = new rspDiarioAsistencia();
            authServidor _srv = dbA.authServidor.Find(8);
            nominaEntities db = new nominaEntities(_srv.xConexion);
            rsp.controles = db.asi_DiarioControles(parDiario.dDia, parDiario.sEstablecimiento);
            rsp.asistencia = db.asi_DiarioAsistencia(parDiario.dDia, parDiario.sEstablecimiento);
            rsp.flujo = db.asi_DiarioFlujo(parDiario.dDia, parDiario.sEstablecimiento);
            return rsp;            
        }


        [Route("indicePapeletas/{iPersona}"), HttpGet]
        public List<asi_IndicePapeletas> IndicePapeletas([FromUri] int iPersona)
        {
            authServidor _srv = dbA.authServidor.Find(8);
            nominaEntities db = new nominaEntities(_srv.xConexion);
            return db.asi_IndicePapeletas(iPersona).OrderByDescending(c=>c.dCreacion).ToList();
        }

        [Route("flujoPapeleta"), HttpPost]
        public ObjectResult<asi_detFlujoPapeleta> FlujoPapeleta([FromBody] ParPapeleta papeleta)
        {
            authServidor _srv = dbA.authServidor.Find(8);
            nominaEntities db = new nominaEntities(_srv.xConexion);
            return db.asi_detFlujoPapeleta(papeleta.bTipoPapeleta, papeleta.iPapeleta);
        }

        [Route("resolucion"), HttpPost]
        public ObjectResult<asi_detResolucion> Resolucion([FromBody] ParPapeleta papeleta)
        {
            authServidor _srv = dbA.authServidor.Find(8);
            nominaEntities db = new nominaEntities(_srv.xConexion);
            return db.asi_detResolucion(papeleta.bTipoPapeleta,papeleta.iPapeleta);
        }
    }

    public class NuevaPapeleta
    {
        public List<asi_Resolucion> resolucion { get; set; }
        public asi_Papeleta papeleta { get; set; }
        public asi_FlujoPapeleta flujo { get; set; }
    }

    public class parMarcaQR
    {
        public int iAsistencia { get; set; }
        public string aula { get; set; }
        public DateTime hora { get; set; }
        public string hash { get; set; }
    }

    public class RspGrafMarcacion
    {
        public List<DEPARTMENTS> establecimientos { get; set; }
        public List<asi_grafMarcado> grafMarcados { get; set; }
    }

    public class ParPapeleta {
        public byte bTipoPapeleta { get; set; }
        public int iPapeleta { get; set; }
    }

    public class ParDiarioIncidencias
    {
        public DateTime dFecha { get; set; }
        public short? sEstablecimiento { get; set; }
        public string xTitulo { get; set; }
        public string cGenero { get; set; }
    }

    public class RspDiarioAsistencia : asi_Asistencia
    {
        public List<asi_FlujoDiarioEmpleado> aFlujo { get; set; }
    }

    public class RspDiarioEmpleado
    {
        public RspDiarioAsistencia oAsistencia { get; set; }
        public List<asi_MarcacionDiarioEmpleado> aMarcacion { get; set; }
        public OuchEmpleado empleado {get; set;}
    }


    public class TuplaControl
    {
        public int? iAsistencia { get; set; }
        public short? bControl { get; set; }
    }

    public class parMarca : parDiaEmpleado
    {
        public short? sDispositivo { get; set; }
        public string cEstablecimiento { get; set; }
    }

    public class parDiaEmpleado
    {
        public int iEmpleado { get; set; }
        public DateTime dDia { get; set; }
    }

    public class parDiarioEstablecimiento
    {
        public short sEstablecimiento { get; set; }
        public DateTime dDia { get; set; }
    }

    public class rspDiarioAsistencia
    {
        public ObjectResult<asi_DiarioControles> controles { get; set; }
        public ObjectResult<asi_DiarioAsistencia> asistencia { get; set; }
        public ObjectResult<asi_DiarioFlujo> flujo { get; set; }
    }
}

