﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using apiNominaWeb.Models;

namespace apiNominaWeb.Controllers
{
    [RoutePrefix("warehouse")]
    public class WarehouseController : ApiController
    {
        [Route("servidores/{iPersona}")]
        public List<authServidor> GetServidoresPersona([FromUri] int iPersona) => WareHouse.ServerxPersona(iPersona);

        [Route("servidores")]
        public List<authServidor> PostServidoresEmail([FromBody] string xEmail) => WareHouse.ServerxEmail(xEmail);

        [Route("contratos")]
        public List<aut_Contratos> PosContratos([FromBody] string xEmail) => WareHouse.Contratos(xEmail);
    }
}
