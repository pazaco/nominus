﻿using apiNominaWeb.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Http;

namespace apiNominaWeb.Controllers
{
    [RoutePrefix("auth")]
    public class LoginController : ApiController
    {
        private static nominusApp2Entities dbA = new nominusApp2Entities();


        [HttpGet, Route("testTM")]
        public rspTM testTokens()
        {
            string xNav = HttpContext.Current.Request.Headers["User-Agent"];
            string xSeed = HttpContext.Current.Request.Headers["User-Seed"] ?? "";
            string xMnto = HttpContext.Current.Request.Headers["minuto"] ?? "";
            rspTM rsp = new rspTM();
            rsp.cliente = new parTM { plantilla = xMnto, xHash = xSeed };
            rsp.alminuto = new parTM
            {
                dHora = DateTime.Now.ToUniversalTime(),
                plantilla = string.Format("{0:mmddMM}Y{0:yy}T{0:mmHH}", DateTime.Now.ToUniversalTime()),
                xHash = Token.getToken("app.nominus.pe$$", 128)
            };
            rsp.previo = new parTM
            {
                dHora = DateTime.Now.AddMinutes(-1).ToUniversalTime(),
                plantilla = string.Format("{0:mmddMM}Y{0:yy}T{0:mmHH}", DateTime.Now.AddMinutes(-1).ToUniversalTime()),
                xHash = Token.getToken("app.nominus.pe$$", 128, lPrevio: true)
            };
            return rsp;
        }

        [Route("firebase")]
        public RespFirebase postFireBase([FromBody] ParLoginFirebase email)
        {
            RespFirebase resp = new RespFirebase();
            string xNav = HttpContext.Current.Request.Headers["User-Agent"];
            string xSeed = HttpContext.Current.Request.Headers["User-Seed"] ?? "";
            string xUTC = HttpContext.Current.Request.Headers["token-utc"] ?? "";
            bool lUTC = (xUTC == "true");
            string xMin = Token.getToken("app.nominus.pe$$", 128, lUTC: lUTC);
            if (xMin != xSeed)
            {
                xMin = Token.getToken("app.nominus.pe$$", 128, lPrevio: true, lUTC: lUTC);
                if (xMin != xSeed && xSeed == "q6e98B@NZ=cN6VeeJvJPEXlZXYHTHXHY&BJNO=aNcB6IgLqTcJvzSHXDHHJHlaVNZ=)NaBcNe^643HnE2XSTXYJvqCp6a=aN)BaNt9&V1eTZKJ2CSHl8qT3X$f6NaB")
                {
                    xMin = xSeed;
                }
            }

            if (xMin == xSeed || true)
            {
                authProvider proveedor = dbA.authProvider.FirstOrDefault(c => c.providerId == email.providerId);
                if (proveedor == null)
                {
                    byte bProvider = 1;
                    if (dbA.authProvider.Count() > 0) { bProvider = dbA.authProvider.Max(c => c.bProvider); bProvider++; }
                    proveedor = new authProvider { bProvider = bProvider, providerId = email.providerId };
                    dbA.authProvider.Add(proveedor);
                    dbA.SaveChanges();
                }
                authNavegador navegador = dbA.authNavegador.FirstOrDefault(c => c.xNavegador == xNav);
                short sNavegador = -1;
                if (navegador == null)
                {
                    try
                    {
                        sNavegador = dbA.authNavegador.Max(c => c.sNavegador);
                    }
                    catch
                    {
                        sNavegador = -1;
                    }
                    sNavegador++;

                    navegador = new authNavegador
                    {
                        sNavegador = sNavegador,
                        xNavegador = xNav,
                        bClaseNavegador = 0
                    };
                    dbA.authNavegador.Add(navegador);
                    dbA.SaveChanges();
                    //TODO: Notificar al sysadmin para que clasifique nuevo navegador
                }
                authUsuario usuario = dbA.authUsuario.FirstOrDefault(c => c.email == email.xEmail);
                if (usuario == null)
                {
                    usuario = new authUsuario
                    {
                        gUsuario = Guid.NewGuid(),
                        email = email.xEmail,
                        bProvider = proveedor.bProvider
                    };
                    dbA.authUsuario.Add(usuario);
                    dbA.SaveChanges();
                    WareHouse.CompletarEnlaces(usuario);
                }
                authSesion sesion = dbA.authSesion.FirstOrDefault(c => c.gUsuario == usuario.gUsuario && c.sNavegador == navegador.sNavegador && c.dFin == null);
                if (sesion == null)
                {
                    sesion = new authSesion
                    {
                        dInicio = DateTime.Now,
                        gToken = Guid.NewGuid(),
                        gUsuario = usuario.gUsuario,
                        sNavegador = navegador.sNavegador
                    };
                    dbA.authSesion.Add(sesion);
                    dbA.SaveChanges();
                }
                sesion.xTokenMsg = email.tokenMsg;
                dbA.Entry(sesion).State = EntityState.Modified;
                dbA.SaveChanges();
                resp.token = sesion.gToken;
                resp.usuario = usuario;
                resp.contratos = WareHouse.ContratosServidor(usuario.email);
                resp.perfiles = WareHouse.Perfiles(usuario.email);
                if (resp.contratos.Exists(c => c.sServidor == 8))
                {
                    R_servidor _serv = resp.contratos.Find(c => c.sServidor == 8);
                    R_empresa _empr = _serv.empresas[0];
                    OuchLogin _logn = WareHouse.LoginUladech(email.xEmail);
                    if (_logn != null)
                    {
                        if (_logn.bAccesos != null && _logn.bAccesos.Count > 0)
                        {
                            foreach (int __logn in _logn.bAccesos)
                            {
                                if (resp.perfiles.IndexOf(__logn) == -1) resp.perfiles.Add(__logn);
                            }
                        }
                        resp.uchempleado = WareHouse.EmpleadoUladech(_logn.cCodigoUsuario);
                        foreach (R_servidor sUla in resp.contratos)
                        {
                            if (sUla.sServidor == 8)
                            {
                                R_contrato ctUla = new R_contrato {
                                    iContrato = resp.uchempleado.iZkEmpleado != null ? (int)resp.uchempleado.iZkEmpleado : 0,
                                    sCargo = 0,
                                    xCargo = resp.uchempleado.xCargo,
                                    dDesde = resp.uchempleado.dDesde != null ? resp.uchempleado.dDesde : DateTime.Parse("01-01-" + DateTime.Now.Year),
                                    dHasta = resp.uchempleado.dHasta
                                };
                                sUla.empresas[0].contratos = new List<R_contrato>();
                                sUla.empresas[0].contratos.Add(ctUla);
                            }
                        }
                    }
                }
                if (usuario.bProvider != proveedor.bProvider)
                {
                    usuario.bProvider = proveedor.bProvider;
                    dbA.Entry(usuario).State = EntityState.Modified;
                    dbA.SaveChanges();
                }
            } 
            return resp;
        }

        [Route("asociarPush")]
        public void getAsociarPush([FromUri] string tokenPush)
        {
            string SesionPersona = HttpContext.Current.Request.Headers["sesionPersona"];
            Guid? gSesionPersona = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            authSesion _sesion = dbA.authSesion.Find(gSesionPersona);
            if (_sesion != null)
            {
                _sesion.xTokenMsg = tokenPush;
                dbA.Entry(_sesion).State = EntityState.Modified;
                dbA.SaveChanges();
            }
        }

        [Route("consultaPersona/{iPersona}")]
        public List<QryAcceso> GetListaAccesos([FromUri] int iPersona)
        {
            List<QryAcceso> lista = new List<QryAcceso>();
            List<authServidor> _servidores = dbA.authServidor.ToList();
            List<authAcceso> _accesos = dbA.authAcceso.Where(c => c.iPersona == iPersona).ToList();
            List<authUsuario> _usuarios = dbA.authUsuario.ToList();
            List<authServidor> servidores = _servidores
                .Join(_accesos, srv => srv.sServidor, acc => acc.sServidor, (srv, acc) => new { srv, acc })
                .Select(s => new authServidor
                {
                    sServidor = s.srv.sServidor,
                    xConexion = s.srv.xConexion
                }).ToList();

            foreach (authServidor cSer in servidores)
            {
                nominaEntities db = new nominaEntities(cSer.xConexion);
                List<traContrato> _contratos = db.traContrato.Where(c => c.iTrabajador == iPersona).ToList();
                List<QryAcceso> parcial = _usuarios
                    .Join(_accesos, usu => usu.gUsuario, acc => acc.gUsuario, (usu, acc) => new { usu, acc })
                    .Join(_contratos, uac => uac.acc.iPersona, ctt => ctt.iTrabajador, (uac, ctt) => new { uac, ctt })
                    .Select(s => new QryAcceso
                    {
                        sServidor = s.uac.acc.sServidor,
                        iPersona = s.uac.acc.iPersona,
                        xEmail = s.uac.usu.email,
                        iContrato = s.ctt.iContrato,
                        sEmpresa = s.ctt.sEmpresa
                    }).ToList();
                lista.AddRange(parcial);
            }
            return lista;
        }

        [Route("asignarEmail")]
        public List<ResponseEmails> PostAsignarEmail([FromBody] List<RequestEmails> lista)
        {
            TextInfo iTxt = new CultureInfo("es-pe", false).TextInfo;

            List<ResponseEmails> resp = new List<ResponseEmails>();
            foreach (RequestEmails cemail in lista)
            {
                authServidor _servidor = dbA.authServidor.Find(cemail.sServidor);
                nominaEntities db = new nominaEntities(_servidor.xConexion);
                perPersona _persona = db.perPersona.Find(cemail.iPersona);
                autAvatar _avatar = db.autAvatar(cemail.iPersona).FirstOrDefault();

                ResponseEmails crsp = new ResponseEmails
                {
                    iPersona = cemail.iPersona,
                    sServidor = cemail.sServidor,
                    xEmail = cemail.xEmail,
                    xRespuesta = ""
                };

                authUsuario __usuario = dbA.authUsuario.FirstOrDefault(c => c.email == cemail.xEmail);
                if (__usuario == null)
                {
                    __usuario = new authUsuario
                    {
                        gUsuario = Guid.NewGuid(),
                        email = cemail.xEmail,
                        displayName = iTxt.ToTitleCase(_persona.xNombres + " " + _persona.xApellidoPaterno + " " + _persona.xApellidoMaterno ?? ""),
                    };
                    crsp.xRespuesta = "Agregado " + __usuario.displayName; ;
                    if (_avatar.jFoto != null && _avatar.jFoto != "")
                    {
                        __usuario.photo = "data:image/png;base64, " + _avatar.jFoto;
                    }
                    dbA.authUsuario.Add(__usuario);
                    dbA.SaveChanges();
                }
                else
                {
                    crsp.xRespuesta = "Ya estaba registrado: " + __usuario.displayName + ".";
                }
                authAcceso __acceso = dbA.authAcceso.FirstOrDefault(c => c.gUsuario == __usuario.gUsuario && c.sServidor == cemail.sServidor);
                if (__acceso == null)
                {
                    __acceso = new authAcceso
                    {
                        gAcceso = Guid.NewGuid(),
                        gUsuario = __usuario.gUsuario,
                        sServidor = cemail.sServidor,
                        iPersona = cemail.iPersona
                    };
                    dbA.authAcceso.Add(__acceso);
                    dbA.SaveChanges();
                    crsp.xRespuesta += " Se adicionó nuevo acceso.";
                }
                if (crsp.xRespuesta == "")
                {
                    crsp.xRespuesta = "Ya existe registro y acceso.";
                }
                resp.Add(crsp);
            }
            return resp;
        }

        [Route("consultaEmail")]
        public List<QryAcceso> PostListaAccesos([FromBody] ParLoginFirebase mail)
        {
            List<QryAcceso> lista = new List<QryAcceso>();
            List<authServidor> _servidores = dbA.authServidor.ToList();
            List<authAcceso> _accesos = dbA.authAcceso.ToList();
            List<authUsuario> _usuarios = dbA.authUsuario.Where(c => c.email == mail.xEmail).ToList();
            List<authServidor> servidores = _servidores
                .Join(_accesos, srv => srv.sServidor, acc => acc.sServidor, (srv, acc) => new { srv, acc })
                .Join(_usuarios, sac => sac.acc.gUsuario, usu => usu.gUsuario, (sac, usu) => new { sac, usu })
                .Select(s => new authServidor
                {
                    sServidor = s.sac.srv.sServidor,
                    xConexion = s.sac.srv.xConexion
                }).ToList();

            foreach (authServidor cSer in servidores)
            {
                nominaEntities db = new nominaEntities(cSer.xConexion);
                List<traContrato> _contratos = db.traContrato.ToList();
                List<QryAcceso> parcial = _usuarios
                    .Join(_accesos, usu => usu.gUsuario, acc => acc.gUsuario, (usu, acc) => new { usu, acc })
                    .Join(_contratos, uac => uac.acc.iPersona, ctt => ctt.iTrabajador, (uac, ctt) => new { uac, ctt })
                    .Select(s => new QryAcceso
                    {
                        sServidor = s.uac.acc.sServidor,
                        iPersona = s.uac.acc.iPersona,
                        xEmail = s.uac.usu.email,
                        iContrato = s.ctt.iContrato,
                        sEmpresa = s.ctt.sEmpresa
                    }).ToList();
                lista.AddRange(parcial);
            }
            return lista;
        }

        [Route("login")]
        public RespLogin PostLogin([FromBody] ParLogin parLogin)
        {
            nominaEntities db = new nominaEntities();
            RespLogin resp = new RespLogin();
            string xNav = HttpContext.Current.Request.Headers["User-Agent"];
            resp.respuesta = "NoValido";
            autLoginSemilla semilla = null;
            string xPatronEmail = @"^[\w!#$%&'*+\-/=?\^_`{|}~]+(\.[\w!#$%&'*+\-/=?\^_`{|}~]+)*"
                       + "@" + @"((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))$";
            if (Regex.IsMatch(parLogin.xEmail, xPatronEmail))
            {
                semilla = db.autLoginSemilla(parLogin.xEmail).FirstOrDefault();
            }
            else
            {
                resp.respuesta = "ErrorFmto";
            }
            if (semilla != null) // email encontrado en el sistema 
            {
                autNavegador navegador = db.autNavegador.FirstOrDefault(c => c.xNavegador == xNav);
                short sNavegador;
                string cClaimAuth = "";
                if (navegador == null)
                {
                    try
                    {
                        sNavegador = db.autNavegador.Max(c => c.sNavegador);
                    }
                    catch
                    {
                        sNavegador = -1;
                    }
                    sNavegador++;
                    db.autNavegador.Add(new autNavegador
                    {
                        sNavegador = sNavegador,
                        xNavegador = xNav,
                        bClaseNavegador = 0
                    });
                    db.SaveChanges();
                    //TODO: Notificar al sysadmin para que clasifique nuevo navegador
                }
                else
                {
                    sNavegador = navegador.sNavegador;
                }
                autUsuario usr = db.autUsuario.Find(semilla.iUsuario);
                if (semilla.hSemilla != null) //email es operativo
                {
                    if (parLogin.escenario == 1)
                    {
                        resp.respuesta = "YaRegistrado";
                    }
                    else
                    {
                        if (parLogin.escenario == 0)
                        {
                            byte[] hPass = Hash(parLogin.password, semilla.hSemilla);
                            if (!Iguales(usr.hPassword, hPass))
                            {
                                usr = null;
                            }
                            if (usr != null)
                            {
                                List<autUsuarioClaim> claims = db.autUsuarioClaim
                                     .Join(db.autClaim.Where(fc => fc.bGrupo == 1 && fc.cClaim != "cambiaPwd"), uc => uc.cClaim, cl => cl.cClaim, (uc, cl) => new { uc, cl })
                                     .Select(o => o.uc).Where(cla => cla.iUsuario == semilla.iUsuario).ToList();
                                if (claims.Count > 0)
                                {
                                    cClaimAuth = claims.OrderByDescending(c => c.xValor).FirstOrDefault().cClaim;

                                    autSesion sesion = db.autSesion.FirstOrDefault(c => c.iUsuario == usr.iUsuario && c.sNavegador == sNavegador && c.cClaim == cClaimAuth && (c.dVigencia == null || c.dVigencia >= DateTime.Now));
                                    if (sesion == null)
                                    {
                                        autSesion nuevaSesion = new autSesion
                                        {
                                            gSesion = Guid.NewGuid(),
                                            iUsuario = usr.iUsuario,
                                            cClaim = cClaimAuth,
                                            sNavegador = sNavegador
                                        };
                                        if (navegador.iLatencia != null)
                                        {
                                            nuevaSesion.dVigencia = DateTime.Now.AddMinutes((int)navegador.iLatencia);
                                        }
                                        db.autSesion.Add(nuevaSesion);
                                        db.SaveChanges();
                                        sesion = db.autSesion.FirstOrDefault(c => c.iUsuario == usr.iUsuario && c.sNavegador == sNavegador && c.cClaim == cClaimAuth && (c.dVigencia == null || c.dVigencia >= DateTime.Now));
                                    }
                                    resp.token = sesion.gSesion; //login ok
                                    resp.avatar = db.autAvatar(usr.iUsuario).FirstOrDefault();
                                    if (resp.avatar.jFoto == null || resp.avatar.jFoto == "")
                                    {
                                        if (resp.avatar.cGenero == "F")
                                        {
                                            resp.avatar.jFoto = "iVBORw0KGgoAAAANSUhEUgAAAE0AAABSCAIAAACe686pAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAACZmSURBVHhe5XsJtB5Vneete2uv+vbvrXlL3ksIWQgJi2GVAI3g0jIKOIwj0kdtGNtRenRmXE7LTGv3QQdHWwVUXNHWCIKIYBBBXEACYQkEQkJWkrzk7e99+1d71fxu3ccz0pCH0XM8Z/p/KvVu3a/q3vu7/+33r++LlCQJ+XcgdO7v/+/y7wXnsdttFEWMsTAMPc+TZVnTNHQ2m03btuM4xqeKoqAHbQhNBQ1MJ6WCBp6FBEGAZyHoTAfmI2NMwzBwP9rowUd4XHyKSUXjj5I/yT8dx8GsqqqKNlaj63qlUsGa0BDIBZ55kFi3gIqtgQhs+KjRaAAwBJ9mUhG38WnSG+YB4ynR+UfJn4QT0/u+jxGw97hEA8vNZrPoBzboE8tqt9vodF13ZmYGl1ytlGJrgKRYLGI7xsbG0BCbMi/QJ24WI+ByHiee/QvoEwI8WAFWA8FQWBAaWHSr1Xrqqaduv/32n/3sZ/v37xc34yOhQ5yBs6OjI5/Pv+9971u/fv3Q0FA9FWA2TRP2L0AeKWIE0fijBYs7NoGNwVZFG/DGx8dhsWhjiTfccMOyZcvE+KVSqVAoCF8VyhT9EKwYkET7zDPPvPPOOzEORsDepaPyBqaAzPcIsz8GOXacsEbRqFarYnrgvPvuu9esWdPX1wcALzMwy7Jg0hAY+TxacU9vby8+ReOEE06ACWCnxJZBMDJACvw4zwP+Y+XYcQplwj8PHDgAK0X7hz/8IRaarp8LAm+5XM7lckKZ8/Iy2xNtmPHw8LDAf8UVV8CZYcPC+SFAC48VUerY5NhxQrDrCDNoQLdf+tKXBMiXoRJiGJqsShSRkhKJcTUyhtuASrbMHJUQsdGmEqO6aVgZ07SN7du3T05OiokAElPMwz4G+ZNwQoQmH3jggdT1Xj0YQmeMFHoNnGWDqjpAIiKZjFgSsRSaUZWMrJhM02VDoQYlCsE9P/rRj8Qs09PTonHMcuw4scc4A2etVhscHIT5iUT6yiIRs0SJShjcEIALOU0xLb1QzPWWMossrYMSBCSFSJQwieoyM5RMLosYhh2cnZ3FRDBdNISjHoMcO05MLJT5yU9+UmAZGBgQjVcQiVAdKiRd/aVM3mBM0hQVqpUJ9kZjRJGIAuuVFUNSVN4NwKkLL1myZNu2bcI7EJ+O2XSPHSe2FrM++uijWA2yH1Ii8qEIJK8gEsmWMhRcgJJ8wQaE/p5OS2Urly6FDaiEmqqWsbKmkaUMN0Gx3JURtBHGzjjjDIT0+aQlZv9j5U/SJyz2nHPOAYru7m5ARQMxNoX1CmLZWUF6MrZuyKRosZ6iDpBFg9gKMSmBlhkPSArFvXbZtrO4WeThj33sY5hRGPCxyZ8Uh2699VYsAl6EM8gAzkiPOL+SUFimLOnDA4M6I90FjvhN567pyhGLEbRxGBKxVVMlCE4ZXS6qyhyFWLp0Kc67d+/+y+BEyr7wwguR9Oc5DXLgy2jqvMA9FWKZNLOkdxB3wFXf/qZ1z22+a89z9/7Ht65duyxTUEhWJmXTyin4ayMOQ7ciRcHtMQW4ISb9C9gt8ht0KBg8Ii0cKUX0yiIROauWVGJohJZ0uTdLNj/4g6l9v5rcez+Omz77/nUri9gt4NeIrJOMIeX5I9ksoAqfx0SbNm0SQf4Y5FXCxhECFoLR0UAwwBmxR1yCjqINVtTV1YUGfBWwU6ojHZlFEZ+wCxJJYt8r2RlGYpaEl1/2H9adtc5r1ynx4aMXv+WiDf/67XPPWo7HGAnzli7T2FRVUCLMDqgoazAmGD+GwqIRfjEyJkWMQEOcjy4L48Q0GBENYZ8Is5gM0yOzIa+gR3wKeCK5ARjOWBZuwxnmjRsUCghhu1ntyFhLhvs+c92nn3t806K+jtBr0sTv6+sYHzv4r9/7xhmnHmepUhw0g6jpBy5GwKQYGWUdLHZ0dBThHZfQsJhIzHjktr6aLIwTIhQIwGJczLRv376HH34YnZgMKxANQEJD0G7sMXrmV8BkKSauocYk8W/+ypeTwFl1/JKpw4dzthWH7oE9u8568wWzU4fvvuu2005dRRNHxn4mnLVjKGwTZsRoUO+Pf/xjjCYQAq2YEYJVicaryWvCKQSDYl9hQmg/+OCDmAk+A/DQLXoAjOtNUeanRL9YH4RKkakQxw+vuebqE1YtbVSnaRxZBkJv4jSbA/09E89tXdzfXa2M3Xzzv5TLugWCKHHbwRbjjGoGlSpGvuuuuxB1BU4xqZjrz4ATIwqLBQy0sXSMvnHjRvQIs0RDTCx8CW2csdm4xJ3YCwRh3w+9gKw/Y9nfXX3l5OjBjG1MT02YlpmEUWdff7NSM3TZ95qaHLUak9/73k1th8AlMTKGwhnjIBCgsXfv3i1btqAhgOGM9aCxoCyME2PNZw6hTAQkMRniAcDAiwBGREURD9E5v9/wYUicEFMnN930+ShoF4s2iYNysVgZn1AxYBCGgW/pmmkoYdAe6Cv39RXf/1/e7AdcV9hKbBxAYhAsA7Dvu+++ee2JSYXhiJ5Xk9ekT5yxYgyKKdEeHx+H8YiPhAcCHj4VqhaLwGVPTw/qT/F4sWh86IPv6hvo0Q0G5q93lDzH4TkpjNxKFQpnmlqdmbZsRevrPHRw9zXXvH9wsEuMjGEFHqQuGPD999+PMdGJs+iHN/0ZcIqli1AOQQWMshCLB+fWdFvXzQDx3fdZHBYMFRwgp0o4d2bU1csGlg8tgltmDXbiiiX/8PEPv7DtCVmOMrbZOHRI46QCxYus2zmzWGrMVmRGWZJMbnv6pJNXx179/LNeV7ARfBG6uPFjqVhItVoHrU+imEpJFPqMLz/2An8B73wtOGGrsBlwHaFMXP7qV79CsRyGLJbwkW/rNlaBUKq6XhHpx406KbF996brPlFQ3S6LUD/62pc+06qNDw90hy72K8mAHkZEUnQUo0TSiQfSC8phyiQp6IY3O1Uy5fe/6+1x08eWdRUyTgtP0cnJGRiywtRfP3A/wpgiU1VVojjI5HKwmaNDXRgnRBinMEigPXToEMJPJp9P2WzsuU1LZyce133e6ctPXVUc7EAf+R8fvHyoO2tKDovIP197VW9nliUB2IJEQkpi/EHC5UVJWpckYAew9ARKAnOKEdPkJCxmtOVDIPzEc5qgbbxS496gRGGAaISHSRQQPICRGEZYQI4F544dO2BLjer4zORBRY7OO+e0G7782e9//+sbfvitW2/99s1f++xV7z7l8svfGga1rk573breSy99U2XmsCTWlDrSnDtxqL/3K3TO9aeCRLJu3TrbRLSbC0g4C5t68skn+YPcpPmYlO/UAvKacAp3F2egffHFF4kU50o2kULTSNasOe7cc9fJkndw//PTE/tPOGvN3/7tfw792Wrl8OvPOunD11zdWTYLBYsrM4VxJBguAt5LnekVF4TWtWvXahoNY+juD7jkM888g7DBW2lA4n9fisCvJgvjFGqECJwgJcgrisykhNM9hIAwaJQLRi6ndHdnB5b2TLzwZHeHpVC3XFBPOWn5CSsWV6ZHDBUgf69Pjmr+mJcU3lw7zRaDg4OijanFRzzsEzoyMtKs16FirI1ye/79Il9NFsaJCTCKCOW4xBy4lGhSr7UVmYCF739x1+TkiO9UvXZlYv/OcsFEI1cwmBwVc3qrMaPLpFqZhFvOwTjyLBrimLuaE4QAmC4ysVgiz09pLoHpIi5iGUAv4HHvXUgWxgkROEV7z549mGmOCCC7MAwRl/K5UiGXyWWQ65lCvVa9MTNZnxo3VaWQsayOYr6IUuYP9SnOL8HjksKba6cOidgOTomSNoojqBeFD1YiFsN9J23jTjBhYWtHkdeKU5yxxyAJwJnEpKsjl4SkXiVOy42CeGpisjlbMTV9+tBobnBQY7KpaDIUH9Oxbc8TL5Lma8Ajsb26CAoJNwQ75tdJIugkTBdXExMTXMXp7mNhC8JYGCeAga9gSmwwKO7hw4cbjQZ4Sa3KmQOWTIkxOTnb0b9EV43QT8rlbjJbU2WNSQoif+hFXeW+yOFcV8h8nJyTedipooTgNqzetm0/5DHW0Oe+j8MnMeH1Aw+5jM2z3/T5o8nCOLu7u7F5gsRjO5E8wb9aLUfi73S4LVZmmzI1SIBSGlUmBoQh83IaCqSJxGKUZEhw3D3FgNw65o/0Ou1+uWBGTlw5i+bvMtEj8DCJoR/hkD/4UgrgDxxVFsYJATyQdTTgME8//TS8SFV0RASF8bd7ExMznMHBY0GBJZUAkjiAMMGB7A+oPO/Ny++hCt3OH0cIwIC+g9mh0MH94LjwGlBdmJXreeCe/P7UFv48OLFzhUIBY2E7MTeKeuQVlA6wLCrrlCjVmqMqGm4Ay4leFvpwhd6X3Iff85Lwi5dAzl2mIu7EVmJzeQLjhsmFB4W0BgRO3ARv4nv0mmVhnHBFIASPhzP89re/hTlhEdVaNYwjEC5sL+onPwwwN6w2jkNQCFBufuaHMO3fL2gOTeqK82chc3e8JJgOsWAepwjF87ehnIAT821KzU10HkUWvgP7BzUCLea44YYbms0mCq7UYBB0kzAJqUwmpidDEkjcSIEQXgS0MCrACxMBmDOWuSVyTKnwgHMEToi4QQgUuHPnTvRhJnFn2s3NCn94TYtNTV9NcVWnnx1FFsYJQbzdt2/fjTfeiNoPl6j3xFtp/r1rHIKQHBzZ5/sumBmlQBUSCgPDMY8ZJs5XOYdmIeFTpti2b98OIIbOIxBE4uSWl7hq+i4KN4gXV4hMfwbehxGh0m9961vXXHMN2lAmimyQP8wYEg/DI46OT8z4ARDKhL0Ub4B27gDaEJ5LScigfL6geR0ixwRSErD0UymJUypB4Q8xUdyI7T881Q6IrOpgAkkMeCB5SZBEMGk+YiKF6TsHbtDxa8ApdkUYBqIOvIJ/kr4fQcT7zne+c/HFF3/uc58TnVNTU5jGsg2qwWz4e02Y757dI6GvUMmKAjXkpsQIOCGLQuTNEPeE/BvB2Fdin8Q+uA1Pg1KU1miR25qVaCiFDo08pmltJ5iaacdq8XAt2jFScQmZrrumZcEHPbelqQhCcd1xip1d+0FISmVAcNothS1APmitVkOMgbnDCZEbgRZW+sgjj2zYsOETn/hEqVT6yEc+snHjRnyKfmRO4a71Wi32faZSw1IDnzy2+elSR18cKqpipd+VYPt40MfioKUIZL/VhC8TlKBIPtznoFJ+AKoJiowiNfSqs9NOrZbL5ju6+0CfRsZnA6LyHJzmGCgCRxCkr1EYc1zfMJHVMJikMDCmo8PEMlLLhimC5Xz0ox8FJKTKSqWCTiQPGCr8AdjES2cEWzgn4ptm6G4YAKrKpKKuZ+Rk5zOPVsb2FAvMq47IFHkPXoqRec0cY7kJUXQdETmmeki1SNZ4pYUFw5IjD2ixKYnnt7xQ0a2IamP1+PaNj/7T9TcCIQTYMDsWk66Zxwv0wO6KxSI+RRwS0fgoQmG0CKGIK729vbfddtu99977jne8o7+/H5qE0WIU4AQwaDKTASXngikFWqaoYZgEYVxvuJNTNTuT96EIqsaJHIQIHnA3mcqIGijjwB8QnsHm+Bla5WdxSInbaCKCSenXFs22g3iD9YDZCYSYCMrApJC5RacCVoi1AfyCICEScMJch4eHcQHrRV4aGBiYnp7eu3fvPffcg3QMwOh84YUXnn/+eXgsbuP7x2jgh509PW696TVr4Eo3feZ/vfuKtzWnX7QUJwkbUdSClTGqQvfp+xGUJhFKrJgaEVUTxvUJYwNRQtYNGzWMiRTTaIfZUpdk5Q+M1d7w9vfuPTApcAJwykz4r06wABgXtIIUAJDoAWA0MAIH9CoiASQoDnBCRYsWLULX5s2boVK0Ozs7t27deuDAATEZBgLvQxsWDjJEQPsYiz0/rxuS3+7vyH/h/1x75utWaHJbiptYO9wSwY0kTCLpL7sU5FLQXiOkKqE6HI8BK4nDdkNF6gijeqOl2nk93/HYE1u/veGnG376WzdkwAaEiCDzTgTBvBdeeCFMD/jFFgAtjFlAekVhn//85wVTx/agqEMcQjj9wQ9+sHv3bnQi/OJ5TIB+TCAaAKzphmnZTqMFNZVzRU3RxmemJ0YOaqrc21uGTaKYgnD/R1oBVOiOUwhQfdgYLrBxEuP1VBy4bYXROIwUTVf1zHPbX/jqN2+57c7H3Yiouo4ZBQxsFNo4AyRWcskll1xwwQWYQvQALZY9h+mVhP+MEvdls1no86STToI9AMbQ0BB6YK4YGl6KezAilIkzpuQ9UVSvVFVN6+roGZuZnq01DM18+Knt1376c1OzzXrLD5ERmUaZCs+EJUh8EdwzoWEoFQcBbUT4TSKjXIg8p1mrIqkgzv3u4ceeevJZsWaxdGGQwIZlwFHFJZwLbW4lR4U3L9z08adcLq9fv37Lli14EkPcdNNNCE7YSEwsxoUPYCMAG20ECTRA23zUDVPTMAr0Njz+sni2Sd5z1d/97tEnlGwBq5iYmOSTyLJXryHbwiB48gz8KHChxzDw48AjbhsWjiDn1Or1ZmvZipWTU8RFjZLGG8yL7cYYwMmHSkkVzrBbfIq2yIjQgfj01YTrE9EFDoBbEalxhq0ef/zxd9xxByIQBoICAVhMgwY2AlkHeSxNaEJA6sAwkQTB9MjOg9GGO36yc8euRJKL5U7sVNhqafzne4LxQpmcIaQkIYRWSQStJrBmiTAqyQjisGvcp2kUK8F2w9wQ5GFf8CCsFqhWrFiRvjrmAjUIPYvLVxOK2jyfzwMAQuuVV175xS9+EbrF0KtWrbr++uvPPvts3CSIBBifKFbQg3Nqfhid0yj8iXiRMgf1meem2n4AO1MsCzdGvkdCH2UohXty0svjLlIL5wyAGgRxyG0KdzImK4oquCMWBgBoQG+YF2sQ1oROKBN6Rhu7L8CLVR1F6C233PLEE0/s2rWro6MD10uWLIG6YKKLFy++/PLLH3roIeQY3HPdddddffXVmBKDiqibPs71k0JFqgQvBX8jmQxCLMmWSrO1ug9GZmqabQQeCFxauOBIgtRLOU6QJWwBqBxXbFobKarOf/lHiNPGI9xFoSvoECABSZT755xzjnBL4XSQhXFeccUVq1evxgS//OUvv/GNb5x//vnABpwYfceOHXge1vuWt7zltNNOwzTiF9D4dG6nOUguc/pESJVI2yctj7Sw/44D+sRjj8IUXeZmC5LA8aCq5jwBzIGH4zhJ6TtXFDSqKJoiK1i153O+DhFzweKwAKznlFNOQbwEYKxw3mkXFO7KsAqo8aKLLrrqqqu2bdsGfYIhgCcgIGG1P/nJT+C36Ln22mvHxsbEY32Lel6qdLhn8IMbsYSzHxMQT8fzi+WSaVtuqxk26kRFspzbFHBbgOIMl4PkyQZmgn7AAXBoFPwJiRVlLUwX0QE4EaWgCSwMl+985ztFngdmiGgsCJgimuE+GIDgOrDeSy+9FJsHp33ssceg2/e+973nnXce/BZj9fX1iQBw+PAc4FRe0ipPF0h65NR1KxCTsbgwLV5iMPg5u+UJMwUKAshJHxqwirkXl3Bo7BV/lwa+ym0YShMAoFWQEzROPvnkyy67DDvEH+B7zBu4U+j8KEJ/cd+9ge8qCoNNoggYGh62M5nTzzgLOeasM8584xsu2LNj22BvF7yhq5SfmpicnJweGFxiZVBnY3U4UChpCTUSasc0H7JsxSerT3pdMZ9zG5WZqXHdNtRS2UFMRW2WfvmARYHhxxL1JNkjOpHNhCrcJHjVTkNqNtXOllxwQ7XJv2TQQJ5i1SCS0tnb/4H/+veDg4MIw77rxX5AEZkQ7ZBFefiDr+JArOKYuYkdcbCvf+3GrVueWtTTw1TN8cNao60b2sTkdKvZPG5oUKKxqclvPPsMWQqffuKJth/qRrbhJ45HNMmEZcUECE0i54ndRcqDpNxdKOY+9L4rB7OEtaaKtuagakx0NVeOkwgIpdAjyKKJnChWaHYkRoExLf3NccwSltg9f/M/r3/e6Uwyw0QzQPMINtTIuLMNmi9f+Z73XnHFlTnbsAyTeAGMUBUkAQV/jDznIt7DMGEtlCouakHXRxEBkBD29x/8wN7dO1etWul6LtKUpmtT05X+vh5Vs35x//2rVp7gN9ulgcEzT3/94uNWVZxk14tjkSdL+UWhK4c0kxglUuhThk4on3jmwOnnrll3hteonLxs0do+m7lTMphsQjzZjqCxyGdxQMGBJBimFqtWpOQkxQRLblZntcifrTkHauTnTx82Vl6UXXaabBdDNRtFFpEskutZefLpb7vsXSefPDTb4A6i4jnEXmxQHISe63qOYluorSrVGpBqBooquIfk++CDPE2x973nb5qt5vdu+fZfrT+Thh5j1AUnNk0rY43PtMCkS72dHspI3T7+lLXrL/5P+aHTd9WTZjUh+cHs4jWL15513Gnn9a0+y1y0JNSybuDZzFf9yvoT+yVnSkGdjEoMIGHikQ/nZEgq4KRMAVSi8F9vSL4HnJadS2T7oedHnxpx1MHTAn3Rot7VXYtO6lxyasfQyVbHcCsynt05ct9Dzw2dcErIbZm4Epnxo1nPIbapWPlnX9jV2Tmom3nXRxFV13XVwBbShNdWiFXbnnlq1Ymrn3ns4ee3PIqo24rVcv/SbOdiLZtlOvn5A8+ddfZqZIvZGtlzoPmbx7be88uHpyfr2b7hocXLmaxj2zyJNonixImXRFY8U2rv1Ecfvuuf323Xtlu6D7rjsmwi6ySKWeyrKGXgL0wFqkDNY72B67i1Rs7OhGrXf/vCbVvbnbVF505F3cy3VKYrcoxDZaFC3Disxn51144nbT1ZMtR5ztmnnL/+9OM6uFPW6+FwVkb8btTb+Yxqyah/2pHjZO08iVAUyaBcwejBkd6+zl9s+OZFb35jqBSmA3nbgeoLhyv9x61lNrv5m/ccHJ3ZsfcAzKF7yYpcR3+AzVQyXqTHUE0S+EnkJDRmYOxSXpq1Z56uPr5h080fWhS+qAZTJHJCasBKAQ5ViRyjagtQvMSSFsg2zjSRwliuVt221nPJh7+YO/WysdwprrWcJtk0i5J2qyZLfsbkX8SFPqqEiWJOlak3emhP/dBeJW+e8/ozzn/dqWt6SqsXE4O/jgx01sojaSMgIcBD+9iof/zHf6w3mlnbLGeMTY9t7l66WssVG3Jh+6hz3Ve+/9Xrv7vb06TcYL5/pdF1nFIe9vVyLTIqOJpSk2ZCzZKMfGxkfGbCvKPQzTC/NfrCpeeu6bET6iJXcWKANCLBWVGhcW6A6UEVYlQCyCXoMHOlmsd2z8QbnxrNrzzvoF9ssOLsaK0e6Q6jEZwZNF0zXaY0Y5bvGWonWj1Q5WxvcWit2XHcganod5t33Hnrz/O5JYPLbGx4ELQl4spJyCuMGMFcZv9w7bX5fKHdbGW6ug6MTWvlwSf2VD7+uW//9rmD2cG1/Wf/dWT3FgdWSZmeemTUA6XqIx9YKOS0LoPozMNlww1aAWdyCKtwFb/mT+87e3nHYF5OWtOyCXIjARGPGTwR4T7OFdDJUIWCKoZEtjrqUu6uR3YdirvcwoqDvu0FSnFFWStQLBJxJoRxY7NQ4umZugsqYoUkG0p5Xyr6STlRFxlmFwmkX9x336bHt61YuWyoIx9FzcmRg8VCieOMZfa/P/Xpw4fGCx0dkqSWBo+/7qu3fu22B9pGX8voaamdu8dbkt1V9aSJStuNVbPYIeumF6lhTIIqZzDwOzMjU1tJFLB04ruOGta9yd0n9OhLOnTJnTVyqJhoDL2JaIRgC6D4hzOHKSeS0ZYyI23tM9/6mV9aNUN7wtxQksu1ZusIoxEIE/9PAoiZUoRJI4l/18jfrWkhjlgNEjUkqkSMwd4+09LGJg/u3LU1m1OWDwx0F0o0BLsEJZbpbLWZKxSRfQ475FBA7npk5wztsIdOjXPDdVY2upc5LOtQm1kl2ci0nQjMN/11UvqjYIiTBPWAvw8C4YGRJFTVM4Xuxfc++KhZ6iVGHttMJCwLHIcwbq/8jT3oEGd8YOHIgbninpGpSO+acOHfg4FWajYRu0CCZJ6UEJ8jHy4NhiFRFAgJzFJmnCnOHemOoVTaO1mrRPrxp/5VLcz80//9RpNolYAG1CAhz6Bs1dpTcfvO/TNSufyBa78rFYaV7uNHHc3XO1oulexcGGEvGVeB0AGWymks//kIuLkWO0qChCFIOmMkUOO26k2R6T2XvelM5lUUyUdgj+MQa0uZCRgRiCa4O0ZAU3UTM7T6bn/ohe2zml9Y3lQ7Aq2AyIl7+IsV/gIX2SF98YI+bKQoVvE8zJ4PxXvBcFH4y5pqmEoLSS90xg8dvuCMkw+PTBfzBdxH3/zXb122fOWKNcvv/s3oIw88SwpLY6u/TfNOApZj87onJRQIWxQOwr8jCHCWk0BNfIDU4pYWu7wR+YjrnBWEiaLnZlp+wwHZNdoBFssS7FLC375j/WlZoyQE6lJiSa07oV5edNt9D2f6VrSZDSaowNBCwkJVDmx+hKYcGnLE5Ii/DmbEY1JLkeqKNMvoFD/YJGHVWKPEzo/XE08qm+XVt93z5K5JouZ7PJknHpqxFU3HeshXv/vj7nPfNlZLDk072WJvGFD+DY7LuSIIN0ACKn+hzM8wPX7GJePg0zPqSexIQoMwoZqNZHJ4pkoVy4WHcG4OnB7PKIh/EoskOaIIvzIaADZR83aPNki2CwwpICo8EAbEyxs+oNhoKJK7KOeuvExHZgLfcCTJkWhbkloxcyI1DlR9sha4cqeRX2kUV//3a7+u5wiMrZkE9J6Nv2o2yeanZycOV9e87vUNR4rg3yGsROHfp8IfkgjaS8Ggl08DxPwrTuRA7G9q0sIaYVwRMPAvCww48+59I0RWYE8cGjYILJTnGP5sKClhqsyQ6YlqbX52t90z3EBCVjJxwsKWb2uc3IGfHHFgCpgLDj4cNgHjcJflDsRvQKXXgs/Lli8VqkGhY/C0R54efXIPWC8JVUbDJBk5PHbH7XcNrzll3/4JO1Pu6lo0OzFrqCxqe7oB30hBpmkQB4eHZEAZ1hpIWCt0AmwADMYMm1QI070oKXb0PrNtWxpReSyhXJMBYg8qKVgs7scRMjBk1Ze032x+unf4hFaIHp3Xnb6HEMszPCMhmA3O/EgXQbGVeFaLJC1JjIgYUWLHsY021oZajxa6ApafqMo1N3viujdd/+U7JlzioR5cs2bNpk2bfvLTexYPDB3aP2IZBuM0B5sAOwR9iZR0fB5leN2IxMeVlsDBqBRQxZcMLBTwYqKInaCK4YVSZ3fvju07oT2EWVTB3GKx2Qgvc3qA8aXfu0hK04+e3bFPtguhbLhhglAmK5T/1lVuE7VNlDaREeL9hAUR+BSFwyCjWGGSC5NCGJf4EXWESRGFG1yeaXwjUCE2XLPYc/yTz+97ZMv+0coU7ekv6+WBvmVrZ9vELHY7fjI+MdnVVfScdsYwAsfltTHiOU8KSID8pxV8qyHYC0p8WASPUdhpbELaKbN2JGHMvZMtDwUNtSgKByQSPJ7AF5D7PDgCnggoa8vGtM8OVh2UmjL/WU6gUpLRDN9pz319CoOfa6SDY0p4ElpITOmBbcOJ8w5OPFCJ8MXqJaJ2FJ7ZP1YaWL5py66RwzU6GZFbfvZEZvi06diMrTLoG9Vzlaqra1b6AwQ9jtU4xllPIp2EOgtklX8xy+eFowXMQ30WMDeibSRTkjjY9RaRd84mTmbp7imi5wfbNcSabBJnidGjUBZVRq2kbunJVKs5y8yv3P3gyjdc7GuZhuNiF6gPA09oKMuBRnyN4Bwo3HwxYwhbApOOcfB4IICnoqBCaDXkaoWFPur5GiMVjbCeLqWr/66Njw4vPp5+Z8NT+8ZmA2ZHqlX1wlqz7fNcrGKD+G+VgBRpkr/IQCkpy/x7Fs5rBIXjvkRVRCzcA/tUaCxTomiqbGYjI8dyPfsnmz41ZZRgIQliJXb9uNnMGFBs4LVbWqbUpuazIzOj9ThUTN3OGfAaHpqRa1WWpP9xEtSUn9HN+LdRPF9yD+KBjcd8BHfRiDKqbqu6rEihHwYN32m1miSmJoqX0mObDtGli/vzpj47M+YHrmZope5CZ7eJlTu1WadeCSIf5BIPemHkh5EXxn4Q4xxg1xHI+CFHjhx6OGAzmCPx+HvWOAhC3TQe37q1FYQ+U1AiKTrMKsYwpABfUh2PmLmO0Yn65HS72UZUVpFp3Chp+kkL8URWPNwb8lcpLzvjiP/NGfzQiUgdpafjcrZSUJUOS9M0z3NyucyNN36Z9Rx/GnaKWR2TnuYzHSECpDLwHEllpm1ZGTXh33/yV1XI9vwHAthe3kbU5WajIVkicICnQqVc2URhsSa5OcXJ0kY4sfPic08q6D6NXcpkSZF9t63k8kHDayYayQ7+8Oebx7yM2rlcyvR5BAEMDIdpmqTpnAczGSZEwfKOPCsKk0H9/vCsKSgi+FetkWrGCqdnMWC0J2lr4vyTlzzzu5/TckZ79vFHVOIO9BaKWSVwplvje/32TNaUdDmoTo3GXi32cfAaN/Yr4ki8OkMgbBHaDiWnLTkOcduJ25bcJvXaDPTUd1RZevHA/prrtRO5EZI2uBDKVWy2FzcDlOilejPZtGlrPtulMi1ou06jmri1JGr4fqPdroZONWrP/tsjcXHby4/Irdcr42HQRI1K/CapTJLWRMGkxw11//rB+3p7SuzWn27Yf3B2y/MvVD2i6npX0czason6PWojgaqJY6mxyQKDBfxMQ5OmbZoYkmTRxKaeSX2LBpocaizC1qiSqyZ1W6pb0ez0vi2XXHSmzfw4cLguGP//kRI1UPoY+YGRSvKDu35dHFwTsELIv1yTTUPN6DCQduLXMKYu+a90iAD1hwcsCNktboPZGmqcz+sFM2Htifb4rvrBbVdecqH07Eirp8/82Oc33rkJFXUBUiwWUXC1Wi1V4S/g6/V6GtJEkSLOSJQo6iwkGJqyVv7WGaEALSTKsC2FtTyr2/7o+DMbv/+Fjy/JgtzXPLfutvnXh4jhTdTARvembROf+srt/ae8uaF112jBoRpVNZhlFMKtWhrVOdN5jSJF5YJeazerCKNMZYoWe43W+H5vau/3/+VT5yxX/x96GCUhQRgEqAAAAABJRU5ErkJggg==";
                                        }
                                        else
                                        {
                                            resp.avatar.jFoto = "iVBORw0KGgoAAAANSUhEUgAAAFEAAABUCAIAAAB1IEzSAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAACI0SURBVHhe5Xx5mF5Vmee55+7fvtWakEoqiwQSCAQSBBlowEFQadCmWdXucdDW7mm7ZWyUHkUc5VF7mhYXGHsEldGIiAzQQAsjAkJCyAZZIKSyL5VKLd9+v7sv/TvnflVZNalUfPqPfp+TW3c599zze/f33PtFiKKI/Acj2v77H4n+AJihN3H7/XSC3f4AdKoxcxi+RZyWF7iEhCT027BarZbneXGHyCeBE6Fb6BLP8WFfYRg6juP7ftwZ+ziJ/hOmZxgGTsb7U6RTbc8AbIeSSImEsYnRtGq1CqFRKpXKZrO4LkRCGBISRAJIZH0Ycc4DkmmakiTpuo4t9tkVSrEPilngui6GYr2nQH8AHxYRy7Q9z0kmk6IkxaiCIIAMARMYRFEU8NwwdF2c88ERgMT5uFvcB7PCDruT8wJbVVWxBWZFUfjpk6dTjjlsGnVFkVVFw4HjBBCX70OypKOjiB3TNMAOHArABUaEYVdnzxEmhlsajUYulwO8mBeQMNgRCzzuMxU65ZhhkJ4T2FbLFqiSTGREKoc+YFjf/e53BwcHN295e/uOLeXKiOfZFOIWBLMV9fb0Llmy5OKLLz7vvPPmzJnT0dEhy3Kz2QRUwI6V2bZtKAUOQfGTTp6A+ZSS1zAPBFEjjEwvtDa9/dY3/+Gfzlm0BLop0gQhMoGhQ2dh7ypJ5YR8h5rO6LHegvL5/DXXXPPoo48CcDwcXBfXFB9yhp+LT06RTrGc4cJC4gXEG9i87dGf/7+fL3t8YOt2gUiFbNGyLFmVFZVZq+MbLbMOL84oJJIoQXpAFZtuOp2ePn36/ffff/rpp3d3d0PCOI+TsZ6fAoqhnyoKI8Sg1mNPPfLeKy/jViontWJO7xFJBk0iaUpiaVNIW04KqYKmJdpChq3CmUGr4eTAAk3T7r333nhYmDdgxzvxmanQlDBDAoi62IEqDg8PQwOhfI89+ctkNgNUZ56xSKPAmZyWm5UipQQpFNRuHCblvCwlMtk8YGdLuXa4OhbNmDHjjjvuqFQq0BE8JYY9dTp5zEALA4O9QSdxeODAAWwffvhhCGxW/7xioRuC1EiyqHZoREfry8/EVidJmWjdHdMh7XPOXaLpySOc9gQVi0VsS6USnB9Ghv9DoDolJn3ymCHVeAfSxhZCGBgYOPPMhUSAX1UBuCvXPX/GvC4tC1jTdYheWDi9P0nE/q6+hJhEh87OGW09PxbBn8U7M2fOfPrpp2POjoyM8GdOiaak23FuCPBjY2PYufXWWwGgt6ePeWkiZRUdQaZASLdMkIKVCPnMrdd3yQIMuqCn+npnpvQC73xszLBq+K3e3l7sL1iwIGbx6Ogoe/DUaEq6DYKQgRxCfvnll5FFSJJCiZJN5NKSDPHCrIF2bp586k+WbnjhR4Prf/WtL306TUhvNqkDE5Eg7d+FOU4/ABvODDv33XdfbE3tx0+BTj5WAS1mg3kgr0gkEldcccXy5cvhjtOJbLlyAKl0KStIJOqbnrj5xmuuu/YDCMKeT8pV5977vv/4U6/YHtGSOQSpkcpYhHj1uwlKjmcher3++utI2k9BxIqhnxzFXIfT3rt3L4ZCgg0frMssUcJehpJLF6We+9kd0di/7F//7X1vfMve+2htx+ODbz155UWzU9AHKPB4lXE0ARvYFG8Rw7BdtmxZ7MCnSMfnGdRgXBMgjfEG0YRk+/ad/Cp98cUXoaq+5ygSCT23f3pOl8hFS7sfvP/eP7rwvM3rVpZSakYRBNeoHtilCPY9d/3drOlqLkHSahv20ciLhZzrOJl0UhYly3KoqN391X+QuZ5PkY6j27gWJ0ti5ItRQCKuhIxTIhGlSCBjlVY2m7z2mg+ufn15rVzNaKQzK44MBx96/4KvfeXOrE6rY4OFrI6KGfZZqzccn6byHaGgvf7Ghod+/LN/fWGXrBLLJ07A9VvkVRe8TOjBmsOAyKLgBJGsZH1BpUp6955tnb8vop8QHUfOE5Jt/wVmVv6yE5hQEJF8Iel44YYNG6rlqq6gLiQAfMMHT/+vH7leRWloGXnkIKEg+VHQsgsdXT3Folkpu2b1gkVnfOJj13/8lvMEj4ghyeikkFMlIfRZCeVN6y6QgHQVdU2MMpqqKRKU3GkaW7ft5pOZEp2UP+AWAf1wXXYEex7ctw87CV1CvpxPkFtuunHpJRc7luE5NgRltZroDTMPGw3TaOiaktRUVaGLzz37bz/z6e9967OXXtTvWsRsOrP7cnNmZhIyGR6sQOeXnDPnoncvQLrdNMq5rEREZ8WK37BHTo2Or9vxeozMdNtjQmbdURvJAaG2H6Ea3LF96+JFZ2oiKotQCsnn//qaP7/5Q6Vi9sC+PbmkKhHPMuqZQo54fstshVTS05lQVFqOR0U5mS1ERBwu17fu2vv62jeff+GljW+NIH2ZP2/6lVdcfuEFF8iSdtun/+atbXWiKGq647Irrvz5/31wiro9KcywuRizSATJDQRRFcrV1lub1v/JH1/tGvXQI1dePPeBf7xLiVAbRywvEXziOxIMVBPtSlXrKBFRMco1XxCyhU5k5wdGRnL5jEAlDAYdF9hagxoFoedYxUKhUauJov7EMy/c+eVv768SqtEPXnv9L5Y98oe155hgpZg2gcsCYcNbzCxUh47Zsk3U9wT2fMvN1+czydCzNEnQVNlq1sLARfjyGnUtqRPbipoNmUaaRD3b8O1WWqX1kT2hOUK9emSNRtaY6NWIXbYq+0Z2v1U7sHt4z8DFFyxC7sIUxgmj0BFi9zIFOj5mAI7/trcwZt4kiZ1RVVmSqeuEqkwWntGzdPE5wAnBImZFngXOsCm6duC5RFWMWq1eK6sJRUsorXoVOp/Lp6Z35jOaqBBHiRzRM4nZ0ASnM6clxGDGaV2zpnd2F7NXXLLwtB5RRHXutPg0pkQnJOdxQme2nEMEdheVSLVqQgESmorjGb2Z+XNnq1IIT5ZKKK7TCn03VczLqhy6rpZO+/U6DnNdXaHr+GYrX4CNp+16JXRst9kQXDeb0LO6FFgNq1YRHDtBaWXPLsF37GbtozffZDYCRSTz5sxth5Ep0PExiwJrFDjRkPfFTSSjI418PtGo1jo7ihilVmnccuOHaOgHrhWFrqyIYE5km3ABUAkEOUmToZ3EtdjKpij4ru17liJCWwRF0RUKMfuRG+qSkpJUMYg8s1VIp2rDFd9wLli8dOH8WcQnfdN/Z01y4jSp+8cVm1M+n/F95Jt6JpVMJgjm3tNZQukgsLwlorB24RCBMFfAucaIncdA3EMg/wBHkIDgRkojKobYMoOSieRbTi6by6TS1dGxT932F7j56ve9n48wJTohzId1as+bUJmMjlYSCS2Xy5x71rumdemlXBKZJyU+BapxwAyWAA+Iu5hnaDsHBjtk3RghEssRkRhMxlC4fMYQKoiSpBFFFYLQsdxFC89aeMbs/tnQ7anScTBDPIf3OHgEQUaIXoQouvqxj9x06X+6SFNEaHV8NSYux1hsQgjkAuWtfRXEXJxAQ6S2LBmjrE/8CMYeH5knMVuObXaUCrt27frc7Z8NkPSMs+2k6TiYY2KB6lDi7hjmWiqVmFuOwptvuuGq9/1nSFhPJCBAJsR2Ax7eAIxhg/TRJk6igxAI1KeSJ1DsADBORnEvEgXNKvFRsSKFo75nX/exm8VTsKR/YpiPpHEdVRShASqPCtl0/8w+17KYZMaJa3Xc2njabfylDAhXAwGwoegUuh7QEIBR1BMkMxndMhsE1WRSHR7ad9ppPYQESMc5M6dEJ4WZU/wKEU4YRBwX5ZDnO8g6+EWmw3zwtni5AOMGZW7zIiYOFcjDkKPFlgHm22Rah4MgjoH9dEonRs3z7fZtU6ATwtyeH7bjE8Vfbs9s7SZdKAatJtyupqgRX6c/iKd9Y3yCyYerL7wUuoMBzFcj+4VRMDWPO2DLfZhfLguFgm0gkal2ze63XIOkNTWhHWVpk6bjYMbw7blg1szGOHEEMuI0LrkO8VxRkiFtwzAEGJ+u8aWsMAx9IfREJkVXQI7FMnY0+CvosURDRQhVMZTUKFIiX0HPMBSDgEUsls/LkpqJynUtlc1lC5XBfZlcFv6MPXXKdPwhYuNt85ap3PgB3Fj7LPZB3JJ5Et4WL+cVboEkuTARtNktrOE0i2BIUrEN4e5Zg3Zzi+eqj4lRQWArhMzV8wcwt8nHnyJNlm1tqYPYBLhxYm4EiRXzTBgt3k6C2vw5mmJ44AnfEZEPIN2d9ISPQSc1BJ8mw9imOCFFUQ1ZchEx+8XIRw1+pCmOG8uxCXkbdIfyt9dUkGQi4vDfQc5tYhLmOszqachZlERRRshlU/qdgjsOHZzKxAicr5AznkERmsHWfw/dPoq44RFBQt3P4jCXCWsTomYdDmEDc4S8MbPGDjCw7ZEawO7l3MSYCBBUpJJCJBme4eSZOk4Y+jgUP+To52CSrMXXKFs5QYsipo3tHofR0QOAYsBHEOcUKGYWC9noBxfJjBn4p07Hx3wUQURHTJQZHquIWJ0gMjbEwp8Q9RFPmRB1m7iQ2ycnenIvDXvmKTqzGv6VEU91pkonOQRmh/LCsC1BlC3bgZzTuWK50rAcD5UQbBsjQzSg+FULgnV8I5dtTBzkkexj1O4BzLJsuy58mGFaSibbMgxJVE96zhM0+fuZUNhEoXGSLMO3IGNk06SSnkxJssaYAbQUlyjUMn6HyiPZBMWWjG7HAMyJVdrMc0G+kkwlUVF1sDUSWO01wbOTphPCfPA53NXgH6bjB4EiSlxYSBSRl+mZbDEMaMRtji2XUQniPRwz7x7TQQ0/5OQhBDUJQrgx5q5lTed6LsZvO6ZIx8d8FGPxVHYKMDh4mCMGEYiiJVPZhtEKoIuQPGVvHlhv/h6PwgMfpPYInOCU23ucYAPsLnZOpD6vYzw/kGWZQJ3Yx3QnJKTfTyc9BIo/lmx6YcCslycMip6o1pvs+852FEVyBnuWDlfsCWL92rtMgQ7uc0KSIwMtGOr6gSQrge8jL2E+csqwT/j+I6bE74Tluq4Lq2b+BmLQkwJ8LNPnCKUukMOHod7Atn3P0XS4SR82G9TVARjKvKCuc90WgfmoeUyeJsMzFoEOI8ACQhGBCsK1HJJIFYodGBM66XseLlFgZn7u6KcA6nEtE6OGkqIqWkLN5sE8jHPce06EYK1wFG09ixukN7HFMwK2lMEsFA3qyheuIB1moIACvcWOF/gtxyWinO7o9UjKilJmqNlU8WUlVFAvoj4OQgHGyUoStsTHbhVDAeOIochWSEK2soBn+ig8+VIJJkBdohI1LelZksgQSYNuQ/LoNNEwQ04xBzF+3OLDuB2D8KgQzhD3OyR0+Nsp9/CtxZvZbtQiEsAFTJvZ98opTQ2igEIayazTcoV0j5g/M3fW1ZtH/S2jrboo1ohnUIvqgOsKbEIs0xAAhqiBoDiC3AjDMRTGmkjTou2Ufb9BNWp4zsbtezLT5pCO/paQJUIa/Y1aQxcVj08MW4zFtqHne3bgtvjRRIt5AnEeA7bgRx4CwjhPjhH9YmnH2skkzbcQTOCzr7UE5sNEFSbNCMou22VXS2sjQ28HwujrK59avKivIyVVBnd3J7MUk3H58g8KY8Q5BSZKzdDSVckz64FjZWC3VBotN0dqrpI7bdaZF9QdpTht3p69Q5TKxUKHgPgvQdHYfJiq8JUlgS2/+lwKoPgKgDBlxA7+xRcmiGHmPTDfCWhtilPCeDkk3rYvsFEYc+BRYLlxNMKW7fskly5WDowVpumv/uYXCxd0b17/SnVo+1XvvZzUmgRm7zOtxa0BGl/0M3232De9umOrY7W6Z88d2rJ9x76xd1/2flcqPPPiqg/f+pd2CJUGW/V46sw3MjsLxYitHbKZ8KyBEwMJ6wsISxzY9JE38O2hBNNhmLEXsvcPh6LiW5ZOMCXhRzG1+eLDQtlbGBnKisfiDEbwvMh3qKZKzz69TJWNXNpfMK9XDYw1v31p3mkzdXhgWUdGySJO5AWh60aB3tG5ac2q/ll9iVx+/cpVuWJ33zkX7d0zVnGVx59/7fNf+ka5bhdyJYw/NDyc0rVSWhcixmhkeQwzxIuHI0fgKwocMMvPYzCYKzAfETYAahwSostBhnEuMfJJaHPb4EyIVzPjJIRQL6LMAYq8kGSPiQLmRIQf/fAHqmxveOO337j79uqB7eU9O87s79u3bUCjVEPGrCqyElGFpZJw+qMVI5cr1KvlkbFK77SZhRlz9+6vrVi/Y/n6nYsvvWrn/tqf3fYpDKuoskqFjAhPAPNwubDRYmHwuUkaphRwzJgUZowGtPAcx8Ic38kcGfvDt+MUATNcGC5xnG3MYiBIVFQ9uNYg9GGfyE9wGnoWkRWvrB7YvOG2P//TbW+vve+ev//833xyRjHrNasSZONZnmN6rhERW5R9VMRUlhwfVYqnq1ou3+FG4p7h5pbB+v5GcOX1H+/onfvUSyvVVOb8887AvI2mlxajrgRVYl84TjAsbAWWsTAbjjGj4ewx5YyTnOJe6NbuzqJHuyRkGS+0EU0hohpJeiCyVnUiI6CuKAWy6Iqk6pEdY+HmvY3Hnvv/H/vkx51QXLRo6d/dcfcD3/txvebX6wg3vXqmR8l0SOkC0ZKeKDth0HK8lu12T+sXtUzDimxffXPL7n3l1nW3fKKzd+4bu0fedc4F9/3gp9v2O2M2iTRZTSjYqTiiESo2URHMfKJ6AnYUSBiA2YoKBwlFj9s4woMELwo5Y4fHrDZxCwFgxkpEUJyPWSbFmsP0l5AW9yS4VrHIth3VVeveWLV67c4dA9M7Mg98656+rBqajZwihLXR+77+tb6erqUXLIFKK6qgqBG8dRA0Q7+JvJKKWSrompZ4c+PbK1ZvXHj+JZdf91Gb5EYCAlaOmuS6Gz+dz2dt0/iji989vaN47ZVX6KKgqlRH8I5nyoREEqhBuPQxUdg5hMamz4hL7hAaxyyE9XI5yz4fps0GdE9KZxMAWmuYiq4ia4bHcgBXQSrI0FYNoqfJxoHWyytWr3pj/eD+UTeMRDWhyaHiV//+bz9x9Xmnh6Yd1UY7OgvEc598+KFde3Zd9YGrit1ZKLZtV2QK/9VEySmp+UbdajQaD/1w2d3/85uukMxPm98gcpMQtAcfWfXI40+oIE0sDw8VspmxoeE5/f3nLz538aIFs/tOK+WFtEp0rsO2HUSumdJkXUZ550psQRZocYVVeBM1D8fMiEnSthw/olpSxUG5HgiymEiQFhw/NwjI1grJSI1s3T2yd//ww488ZnqR5cOYUeAmREVjiUpouZUdl503/+tf+KuiQESjlU1hPl5kNN7eNvDgT354+VVXzDl9lmNXjcbojO6C53m1urd2zfqBd9752le/LlKdqrmaK1mCakoEanznPT/dOThsWCY8n++ayAUEQXYdz7Et4jv5THJu37Szz5g3e3rnGbNnTCulO5MMYhR4odMSAhdBNJlh6TCbP/9RB0q02G8zgtf2QgEJSoRIz3MZoK1ZJJ0idYfs2+9u3bFz2469Azv3DOzYte/AaNf0mW4kedBvURUkBbYERQ2dZl9J0L3aD775lXl5orAkroW5ZruKY83GnvLw9x/+4UWXvKent9RVytrNWqNaWbVmQ09n943XfLhSGe0u9FabrUor6ujuqBKybgv5i8/9j9KMWUMjZUmR9YRimLYXymxJgqmmS3xXEfyMIiTlyKmPnT2v793nzF90+uwz5/VnVfihsFoZTeoZ4ESpA8yWZaXTaWCOcxLaMB0xoYIBzKuGTHvf2jq2ccvA6jc3Ni27XkdpbPlwZXpSVpAMqaKWMl3fgCfGPVAb1IzsIwpLj2qSMfK5/3Lrn119dsIjSYm4ThiqFJnn26OjZde644tfzKaSsiTatUrg+cVS50P/9B0UYoJvIQykE1nY4e5RXy1K/7xs7X0P/bRj5rsM1/cFEbVGy8bTFEnh3wVCT10rcG0xsAE5MusacVKS35VLLpo/+z1LFkP5e3K6b0IjvFQqhTugVrGc3TiOQ3UbIXEoy67XbGw89dyLmwa2+4JUaRgoY9kiK/v0AW6MVQA+pOrBoylUVkVVFyVEWwrNCf2W6FTyon1md+H79/xlxmefBcJdNCOyeWSsq6v0x399uycqM2f2G9Xm7ne2XrD0/LHKsCYK/+vLn+/kHiiJdDogI5XQpvT2u+7fNtyo+jRZ6jG8kCXJsoR5M60M4XR95BMSjRTEfBqmJBQpJnVbvlmzG5VpnflrP3D1DR+4bFYa0daCUcQ/zGL16Ze//EUe0wQD/0SCVP3VdSOPPfvCy6s37K+aDpFduGuqUEVXkjlZS0LSVNYkHOpJVHkyHCgyZ+SUAV8GCv1UJpFO6tX9g++9+LJCihhGSDUBGaebStx0+5201L2v4dQcakdaqjB9zYYBK5KIoq17Y+NlF56HtKhWMdJJJZsUXl257Ymnnyt2zxgca2ZKPaYvuG4oqUnfd1GSa6BkUtNTVFYgAPC/Uq7VbU9OZjOl3kjWhxv2mOHWqvXF82fndBlCcxwHQj6IGRl6GZxUxTe3NX/6y2dfWbtJTBfTpWmGG6SyRXTw4BH9wLSdFuKpD7UQHY/9HMo1Ta9lBrYFYcqKnExozcqoa9uCZeV09cJzZiR1AYo0cGD0tju+oHZ172k6xRmz9+0ctoRkZ1d/x7SZue6uffuHEpr65FPPXrzkgmmFROiRap385Ge/3LH7gJwqWIFEtUwT/kiAs9TAERp6ptlyGobTMl1MTlYkLZHr7IFPNRtm2Q6ciCJ6Vy1/eHBfp+zPn9kDIddqtfjXp1BU1yMyqjNkmGu2G48++9ILr68fQ8jPlpAfNgxb0RhvZFFSdQ0pteXYyHMxBAp6pNuoo1mBEaB8QALMHAsVg6QQdElhwqkve+DOUoKseHPggZ/8qKEp+2zPTeZaoZJOd7kWseu24DmCV+7vzbeGBt934dJNr7767a9/oyNHduzxP/mZLzSJbitZIVlqCWrDQgICtaKR1aB4ChWR6iPthWp40DEecdkSBYt+MHVUO1AIW7dGz86a3/vKf5/dmxsaGpnZ04l6Fv5PbFhOK2Sl8m/Xbn5h5caqRRPZTlFKCiEcDcK+RCUVNg+XaSOWCRIRRZf9kg/ZNaIgMkq27sBW33mTqGwYJgxhqGat3Wq+trX148efuf3Ou/L5ouR7qmcRs+I3Duhiy7aGe0ri2b1q9a3l71961g1XXX3/d77x1W/+86oB8vyaHVW11NQLTVUbC10jBGrYRyOwa8g3kITg0Z7jupYZODYNPPgzeGnshK7DvsVs4ZqHdMpXs+uG/f/9qzWDhKR7OuumAXchfumuu01UdLKwq0J+9fK6TdsGxUReVFKmCVyRriUgQF6tsSQn3h7SDlL7QIicRq1Y6tj5zmZZVRAqDMu69IrL5vRl5p51/u7BvZVms1DIDw+PIsudO2fO0M53xPrg5UsW3XTdn/Z1aIZNLrpk8Udu++LO0XrFI7akuFRGyRnx99Qic6TMl06sUvFJsJO0fSluh1xExiDJdcOY3X96b0nJwfjNGrVcF14JWrli1cYt27eblsN+T0EF6DA8pKoiwk+OpvXPrTdbWeRfxY5Nbw/M6p8z/4wOBPxpKXrth2+AR9i6fXexs7M6Wt6ydaDQ2TXnjAWf+6v/liqk9jaYsJavGnGCoNqApwcw1rDDvxhj+3xdaRKEezU9uWXr1md+9WvMAbHJYR4P7l4khkNWvLZ6eKQiyHJERc9liQr8Oyw5vvlEKaIon9xGU02mYNn1ljVcrrUcMuqS3TZ55bXXJTWVL3Tu27SZ7N0rqprpemYowJuUW+3vAf/xO9/t65/bMCy+Wsa+uYklxiXNXl5NCPkECWEFSfFvl782VCENn+iZAvvpPIS8Z3/znW07gD+VySIaQ8iqgrxfsZgBTIIwpeG9e3veNW+s2nCCSNKSDz68LBTZZ85PPP3Kr19eGYkKlI2VaMWuVq0ZCtKBSuOxFetpkmQT5Dv/5zeCom/ZtUfWU0h5majbIFlugMa/NpkcOX6QyXdUW84zL7wSSBC1RIlIweZ16zfVWjZVNDWZgSd0PR+hDIRo1L71hEkrFBHp09lcvWVnCh2WGz37/MAbW72Va9c7IW254VjV6Fm0uLRgIanWWMDXMmveGvBFst0gjzzxL4Ke0jM5LZOGUCFq/pKEQY0bM8LJEEZA7gR1zha6nnjm+bpNyi5XnkqLLF+9FkllKMgefKIIz4wshS1xHWtp+jgkyfLI1m1Q7Jbl1E27c/qMH/3kFw/++BE1kS8PjQmS3j9v/tCBEdeP+t7znn1DB/ZX6vDwm/eQp57f2DtzTrnRShc6YBETn9EB84So2f4kCdEb0cYT5KGq8dKqLQhPwEZqZrh5+04lmXEiYiHGS5i2CgfmeS6Sc15ynSiBg0atnp3Zj4J0xuw5LceHVHOdvSOVxr6R8txzlzRtf//ImJpMN8pjyOCzpU4pkRssN379yspHfvlkxbBR1m3bsb3ntBkB7HncBYOAnoUNyHmS9syqAyqO1pqF7hn/+sJyGQ7a9MjQyBiyy7qJMklFFWE7HgoGECs1J6tMIFmsN+qSro2MVZAbhlQerdTrhpNIF+uGq+lptq4kK5nuHjAVzwpE1Ynkl1asoWoK0kBtp+dLB8ZGDwk7PNs5WYIbNhuGns46kfjauk1v7wcHZPLaujfrpuOzH4LI/OtL9mMx9jw4TmxO5nngFv/DxCOGBMNKDisSkEDJSOuRVbCchl2kfoAMR/JCEd6FvdZgS2uIq2wRmz2ZzwFbtsTZTg8moXegwGe/EoCzbNqBmMg+99IKptvLX1sTUgU8RhWFa2x1l33OENPkHsCpvX7KrJGNw8YMBBFc9Xywki0hIiEJIwTLEOEQ+ZLrUx9BDjxggEXKFhg5sxngQ1r7cHIEbUomk3guKl8llUP1RPcO+3tHRpK5IgBjZqy2YnONGcxYO0mKAY9rIxMl90CRhGzEY596UQHemFEgIEWnmFPge0hjYaUI0HCaLCRx34nOsREzIbMxWU4xWWJpaojklGJQKRSVwZEKXbl6NUtMVR2AWS7NyoZ295Mgdmv73UA80Zhwmkbsv1Vh/98GO0YMYpxhP9jyPRQIAqsR2Pq8CNjgSTzCeMNh233xw0kQWA4vbLUMYJaRAlmulswB8xp4LMN2uNaxyXHHiEnxp0Da7M9kiH3geShgRmyIUABkwIaIAQ6PgnpD8dj/7sHUHMJvu6xYDdgOx8ze7E0oeZsdkyBNknzXkQS2IIjSQ0km6fDoiCgpls3qUPaUNnJMEzuTG50T/56L3wgec5cDwoaZMSI+w8zeXlAJj4LOOTYDxQECDirbmJg6TOCcaO0zkyBwLQp9iX2NKnhhAFEjRWFBCQWnTAIldGhok8ASAksObTW05MgTI/6bEgHb4ES2kAkGF4hDqBWKVoQttUNqEcFmr5Mjl/quGDhKhEKOI4Sc2SfP7IeJzFyZ6fr81QoezZSF8k9+2RygD+xbdrTjzOGwLfED19FlpLshqohiSvNNQzj/o58dMyNXTIQyUly2rAX2sOfhYbhBoBZFQce8C2Pz8bYRgCmYFltXhNwgahgpBoQgYbtQXtRwekJFuQZhWq2W1fKol6CRHBtQ2/MxYtEuVvaJS3wg5l9x6ZhPP3oLCIJjh66b0DTbaiZlyWvV/g25B4dFtE/WlAAAAABJRU5ErkJggg==";
                                        }
                                    }
                                    resp.respuesta = "Ok";
                                }
                                else
                                {
                                    resp.respuesta = "NoPermisos";
                                    //TODO: Notificar al admin que el usr no tiene permisos de autenticación
                                }
                                resp.claims = db.autUsuarioClaim.Where(c => c.iUsuario == usr.iUsuario).ToList();
                            }
                        }
                        else
                        {
                            autUsuarioClaim tokenRegistro = db.autUsuarioClaim.FirstOrDefault(c => c.iUsuario == usr.iUsuario && c.cClaim == "cambiaPwd");
                            if (tokenRegistro != null)
                            {
                                tokenRegistro.xValor = v20();
                                db.Entry(tokenRegistro).State = EntityState.Modified;
                                db.SaveChanges();
                                autSesion sesionRegistro = db.autSesion.FirstOrDefault(c => c.iUsuario == tokenRegistro.iUsuario && c.cClaim == "cambiaPwd");
                                sesionRegistro.dVigencia = DateTime.Now.AddMinutes(20);
                                db.Entry(sesionRegistro).State = EntityState.Modified;
                                db.SaveChanges();
                                perPersona persona = db.perPersona.Find(usr.iUsuario);
                                resp.respuesta = "TokenOlvido";
                                string mensaje = "";
                                mensaje += "<br/><br/>Proceda a grabar una nueva contraseña haciendo click al siguiente vínculo:<br/><br/>";
                                mensaje += @"<div>https://app.nominus.pe/registro/" + tokenRegistro.xValor + @"</div>";
                                mensaje += "<br/><br/>";
                                mensaje += "Por seguridad, este permiso debe procesarse en 30 minutos.";
                                mensaje += "<br/><br/>";
                                mensaje += "<br/><br/><br/><br/>";
                                enviarEmail(persona.xConcatenado, usr.xEmail, "Solicitud de cambio de contraseña por olvido.", mensaje);
                            }
                            else
                            {
                                resp.respuesta = "NoClaims";
                                //TODO: Notificar al admin que el usr no tiene claim de Cambio de Contraseña
                            }
                        }
                    }
                }
                else  // email válido pero no operativo
                {
                    if (parLogin.escenario == 0)
                    {
                        resp.respuesta = "Registrable";
                    }
                    else
                    {
                        if (parLogin.escenario == 3)
                        {
                            resp.respuesta = "NoAutentico";
                        }
                        else
                        {
                            autUsuarioClaim tokenRegistro = db.autUsuarioClaim.FirstOrDefault(c => c.iUsuario == usr.iUsuario && c.cClaim == "cambiaPwd");
                            if (tokenRegistro == null)
                            {
                                tokenRegistro = new autUsuarioClaim
                                {
                                    iUsuario = usr.iUsuario,
                                    cClaim = "cambiaPwd",
                                    xValor = v20()
                                };
                                db.autUsuarioClaim.Add(tokenRegistro);
                            }
                            else
                            {
                                tokenRegistro.xValor = v20();
                                db.Entry(tokenRegistro).State = EntityState.Modified;
                            }
                            db.SaveChanges();
                            autSesion sesionRegistro = db.autSesion.FirstOrDefault(c => c.iUsuario == tokenRegistro.iUsuario && c.cClaim == "cambiaPwd" && c.dVigencia > DateTime.Now);
                            if (sesionRegistro == null)
                            {
                                sesionRegistro = new autSesion
                                {
                                    gSesion = Guid.NewGuid(),
                                    iUsuario = tokenRegistro.iUsuario,
                                    cClaim = "cambiaPwd",
                                    sNavegador = sNavegador,
                                    dVigencia = DateTime.Now.AddMinutes(20)
                                };
                                db.autSesion.Add(sesionRegistro);
                                db.SaveChanges();
                                sesionRegistro = db.autSesion.FirstOrDefault(c => c.iUsuario == tokenRegistro.iUsuario && c.cClaim == "cambiaPwd" && c.dVigencia > DateTime.Now);
                            }
                            perPersona persona = db.perPersona.Where(c => c.iPersona == usr.iUsuario && c.bTipoDocIdentidad == parLogin.bTipoDocIdentidad && c.xDocIdentidad == parLogin.xDocIdentidad).FirstOrDefault();
                            if (persona != null)
                            {
                                resp.respuesta = "TokenInicio";
                                string mensaje = "";
                                mensaje += @"<br/><br/>Recibe este correo porque ha solicitado registrar una contraseña para acceder vía web a su planilla.  Si es cierto,  proceda a grabar su contraseña haciendo click al siguiente vínculo:<br/><br/>";
                                mensaje += @"<div>https://app.nominus.pe/registro/" + tokenRegistro.xValor + @"</div>";
                                mensaje += @"<br/><br/>";
                                mensaje += @"Por seguridad, este permiso debe procesarse en 30 minutos.  Genere un nuevo registro inicial si este tiempo no fué suficiente para terminar el registro.";
                                mensaje += @"<br/><br/>";
                                mensaje += "<br/><br/><br/><br/>";
                                enviarEmail(persona.xConcatenado, usr.xEmail, "Registro inicial a la APP Boleta Electrónica", mensaje);
                            }
                            else
                            {
                                resp.respuesta = "NoAutentico";
                            }
                        }
                    }
                }
            }
            return resp;
        }

        [Route("registro")]
        public string PostRegistro([FromBody] ParRegistro registro)
        {
            string xExitoso = "SinPermiso";
            nominaEntities db = new nominaEntities();

            autUsuarioClaim claimRegistro = db.autUsuarioClaim.FirstOrDefault(c => c.xValor == registro.xValor);
            if (claimRegistro != null)
            {
                autSesion sesionRegistro = db.autSesion.FirstOrDefault(c => c.iUsuario == claimRegistro.iUsuario && c.cClaim == "cambiaPwd" && c.dVigencia > DateTime.Now);
                if (sesionRegistro != null)
                {
                    perPersona persona = db.perPersona.FirstOrDefault(c => c.iPersona == claimRegistro.iUsuario && c.bTipoDocIdentidad == registro.bTipoDocIdentidad && c.xDocIdentidad == registro.xDocIdentidad);
                    if (persona != null)
                    {
                        claimRegistro.xValor = $"{sesionRegistro.gSesion} ({DateTime.Now.ToShortDateString()})";
                        sesionRegistro.dVigencia = DateTime.Now;
                        autUsuario usr = db.autUsuario.Find(claimRegistro.iUsuario);
                        usr.hSemilla = GenerarSemilla();
                        usr.hPassword = Hash(registro.password, usr.hSemilla);
                        db.Entry(usr).State = EntityState.Modified;
                        db.Entry(claimRegistro).State = EntityState.Modified;
                        db.Entry(sesionRegistro).State = EntityState.Modified;
                        db.SaveChanges();
                        xExitoso = "Ok";
                    }
                    else
                    {
                        xExitoso = "NoValida";
                    }
                }
            }
            return xExitoso;
        }

        [Route("avatarRegistro/{registro}")]
        public autAvatarRegistro getAvatarRegistro(string registro)
        {
            nominaEntities db = new nominaEntities();
            return db.autAvatarRegistro(registro).FirstOrDefault();
        }

        [Route("cambiaPassword")]
        public void PostCambiaPassword([FromBody]ParPassword pwd)
        {
            nominaEntities db = new nominaEntities();
            autUsuarioClaim claim = db.autUsuarioClaim.FirstOrDefault(c => c.xValor == pwd.registro);
            if (claim != null)
            {
                autUsuario usr = db.autUsuario.Find(claim.iUsuario);
                if (usr != null)
                {
                    byte[] hSemilla = GenerarSemilla();
                    byte[] hPassword = Hash(pwd.password, hSemilla);
                    usr.hSemilla = hSemilla;
                    usr.hPassword = hPassword;
                    usr.dUltimoLogin = DateTime.Now;
                    db.Entry(usr).State = EntityState.Modified;
                    db.SaveChanges();
                    autSesion ses = db.autSesion.FirstOrDefault(c => c.iUsuario == usr.iUsuario && c.cClaim == "cambiaPwd");
                    ses.dVigencia = DateTime.Now;
                    db.Entry(ses).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
        }

        #region métodos estáticos 

        public static byte[] Hash(string plaintext, byte[] salt)
        {
            SHA512Cng hashFunc = new SHA512Cng();
            byte[] plainBytes = System.Text.Encoding.ASCII.GetBytes(plaintext);
            byte[] toHash = new byte[plainBytes.Length + salt.Length];
            plainBytes.CopyTo(toHash, 0);
            salt.CopyTo(toHash, plainBytes.Length);
            return hashFunc.ComputeHash(toHash);
        }

        public static byte[] GenerarSemilla()
        {
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] salt = new byte[64];
            rng.GetBytes(salt);
            return salt;
        }

        public static bool Iguales(byte[] a, byte[] b)
        {
            int diff = a.Length ^ b.Length;
            for (int i = 0; i < a.Length && i < b.Length; i++)
            {
                diff |= a[i] ^ b[i];
            }
            return diff == 0;
        }

        public static string v20()
        {
            string x20 = "";
            string cads = "ACDEFHJKLMNPQRSTVWXY1234567890";
            Random rnd = new Random();
            int ix;
            for (int ii = 0; ii < 18; ii++)
            {
                ix = rnd.Next(0, 29);
                x20 += cads.Substring(ix, 1);
            }
            return x20;
        }

        #endregion

        #region Envio de Correos

        public static void enviarEmail(string usuario, string correo, string asunto, string contenido)
        {
            string body = "<div>Remunerarte></div><hr noshade='noshade'><div>{0}</div><hr noshade='noshade'><span class='font-size:7pt'>Envío automático de correo, por favor NO responda este mensaje.</span>";
            MailMessage message = new MailMessage();
            message.To.Add(new MailAddress(correo));
            message.From = new MailAddress("soporte@nominus.pe", "webNomina");
            message.Subject = asunto;
            message.Body = string.Format(body, contenido);
            message.IsBodyHtml = true;

            using (var smtp = new SmtpClient())
            {
                var credential = new NetworkCredential
                {
                    UserName = "soporte@nominus.pe",
                    Password = "777$Minita"
                };
                smtp.Credentials = credential;
                smtp.Host = "mail.nominus.pe";
                smtp.Port = 587;
                smtp.Send(message);
            }
        }

        #endregion

    }

    #region clases data

    public class ParLogin
    {
        public string xEmail { get; set; }
        public string password { get; set; }
        public byte escenario { get; set; }
        public byte bTipoDocIdentidad { get; set; }
        public string xDocIdentidad { get; set; }
    }

    public class RespLogin
    {
        public Guid token { get; set; }
        public string respuesta { get; set; }
        public List<autUsuarioClaim> claims { get; set; }
        public autAvatar avatar { get; set; }
    }

    public class ParRegistro : ParLogin
    {
        public string xValor { get; set; }
    }

    public class ParPassword
    {
        public string registro { get; set; }
        public string password { get; set; }
    }

    public class RespFirebase_
    {
        public Nullable<Guid> token { get; set; }
        public authUsuario usuario { get; set; }
        public List<aut_Contratos> contratos { get; set; }
        public List<int> perfiles { get; set; }
    }


    public class TuplaServer
    {
        public short sServidor { get; set; }
        public int iPersona { get; set; }
    }

    public class ParLoginFirebase
    {
        public string xEmail { get; set; }
        public string providerId { get; set; }
        public string tokenMsg { get; set; }
    }

    public class QryAcceso
    {
        public short? sServidor { get; set; }
        public int? iPersona { get; set; }
        public string xEmail { get; set; }
        public int? iContrato { get; set; }
        public short? sEmpresa { get; set; }
    }

    public class ResponseEmails : RequestEmails
    {
        public string xRespuesta { get; set; }
    }

    public class RequestEmails
    {
        public short sServidor { get; set; }
        public int iPersona { get; set; }
        public string xEmail { get; set; }
    }

    public class parTM
    {
        public Nullable<DateTime> dHora { get; set; }
        public string plantilla { get; set; }
        public string xHash { get; set; }
    }

    public class rspTM {
        public parTM cliente { get; set; }
        public parTM alminuto { get; set; }
        public parTM previo { get; set; }
    }

    #endregion

}
