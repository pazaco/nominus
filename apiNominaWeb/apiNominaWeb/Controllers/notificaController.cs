﻿using apiNominaWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace apiNominaWeb.Controllers
{

    [RoutePrefix("notificaciones")]
    public class notificaController : ApiController
    {

        private static nominaEntities db = new nominaEntities();
        private static nominusApp2Entities dbA = new nominusApp2Entities();

        [Route("calculos")]
        public List<belCalculosxPeriodo> GetBelCalculosxPeriodos()
        {
            string SesionPersona = HttpContext.Current.Request.Headers["sesionPersona"];
            string SesionGestor = HttpContext.Current.Request.Headers["sesionGestor"];
            Guid? gSesionPersona = null;
            Guid? gSesionGestor = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            if (SesionGestor != null && SesionGestor != "") gSesionGestor = new Guid(SesionGestor);
            return db.belCalculosxPeriodo(gSesionPersona).ToList();
        }

        [Route("notificar")]
        public List<BigWebNotificar> PostNotificar()
        {
            List<BigWebNotificar> rsp = new List<BigWebNotificar>();
            string SesionPersona = HttpContext.Current.Request.Headers["sesionPersona"];
            Guid? gSesionPersona = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            List<authServidoresUsuario> _servidoresUsr = dbA.authServidoresUsuario(gSesionPersona).ToList();
            foreach (authServidoresUsuario _srv in _servidoresUsr)
            {
                nominaEntities dbD = new nominaEntities(_srv.xConexion);

                List<webNotificar> _notifs = dbD.webNotificar(_srv.iPersona).ToList();

                rsp.AddRange(_notifs.Select(s => new BigWebNotificar
                {
                    sServidor = _srv.sServidor,
                    iNotificacion = s.iNotificacion,
                    dEnvio = s.dEnvio,
                    bTipoNotificacion = s.bTipoNotificacion,
                    iReferencia = s.iReferencia,
                    iContrato = s.iContrato,
                    xAsunto = s.xAsunto,
                    xFaIcono = s.xFaIcono,
                    dLeido = s.dLeido,
                    dFirmado = s.dFirmado,
                    xUrl = s.xUrl
                }));
            }
            return rsp;
        }

    }

    public class BigWebNotificar : webNotificar {
        public short sServidor { get; set; }
    }
}
