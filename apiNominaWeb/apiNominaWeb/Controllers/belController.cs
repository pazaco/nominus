﻿using apiNominaWeb.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace apiNominaWeb.Controllers
{
    [RoutePrefix("bel")]
    public class belController : ApiController
    {

        private static nominusApp2Entities dbA = new nominusApp2Entities();

        [Route("indice")]
        public List<BigBelCalculos> getIndiceCalculos()
        {
            List<BigBelCalculos> rsp = new List<BigBelCalculos>();
            string SesionPersona = HttpContext.Current.Request.Headers["sesionPersona"];
            Guid? gSesionPersona = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            List<authServidoresUsuario> _servidoresUsr =  dbA.authServidoresUsuario(gSesionPersona).ToList();
            foreach (authServidoresUsuario _srv in _servidoresUsr)
            {
                nominaEntities dbD = new nominaEntities(_srv.xConexion);

                List<belCalculos> calculos = dbD.belCalculosPersona(_srv.iPersona).ToList();

                rsp.AddRange(calculos.Select(s => new BigBelCalculos
                {
                    sServidor = _srv.sServidor,
                    iCalculoPlanilla = s.iCalculoPlanilla,
                    iContrato = s.iContrato,
                    iPeriodo = s.iPeriodo,
                    iAnoMes = s.iAnoMes,
                    bMes = s.bMes,
                    bNumero = s.bNumero,
                    bBEL = s.bBEL
                }));
            }
            return rsp;
        }

        [Route("data")]
        public DataBoleta PostDataBoleta([FromBody] ParCabecera parCabecera)
        {
            string SesionPersona = HttpContext.Current.Request.Headers["sesionPersona"];
            Guid? gSesionPersona = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            try
            {
                authServidor _srv = dbA.authServidor.Find(parCabecera.sServidor);
                nominaEntities db = new nominaEntities(_srv.xConexion);
                remCalculoPlanilla calculo = db.remCalculoPlanilla.Find(parCabecera.iCalculoPlanilla);
                traContrato contrato = db.traContrato.Find(calculo.iContrato);
                DataBoleta datas = new DataBoleta();
                webNotificacion wNot = db.webNotificacion.FirstOrDefault(c => c.iReferencia == parCabecera.iCalculoPlanilla && c.bTipoNotificacion == 1);
                if (wNot != null)
                {
                    webFirma wFir = db.webFirma.FirstOrDefault(c => c.iNotificacion == wNot.iNotificacion && c.bRol == 1);
                    if (wFir != null && wFir.dLeido == null)
                    {
                        wFir.dLeido = DateTime.Now;
                        db.Entry(wFir).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                }
                datas.cabecera = db.remBoletaCabecera(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero).ToList();
                datas.asistencia = db.remBoletaCabeceraAsistenciaFechas(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero).ToList();
                datas.datosCol1 = db.remBoletaCabeceraDatos(parCabecera.iCalculoPlanilla, 1).ToList();
                datas.datosCol2 = db.remBoletaCabeceraDatos(parCabecera.iCalculoPlanilla, 2).ToList();
                datas.empresa = db.remBoletaCabeceraEmpresa(contrato.sEmpresa).ToList();
                datas.titulo = db.remBoletaCabeceraTitulo(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero).ToList();
                datas.concepto1 = db.remBoletaConcepto(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero, 1).ToList();
                datas.concepto2 = db.remBoletaConcepto(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero, 2).ToList();
                datas.concepto3 = db.remBoletaConcepto(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero, 3).ToList();
                datas.concepto4 = db.remBoletaConcepto(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero, 4).ToList();
                datas.pdf = Pdfs.Boleta(datas, parCabecera, _srv.xConexion);
                return datas;
            } catch (Exception ex)
            {
                throw ex;
            }
        }

    }

    public class TuplaContrato
    {
        public int iContrato { get; set; }
        public short sServidor { get; set; }
    }

}

