// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCKETJWCElRDxdo-JvY-Cp8pTV6m2V4_RY",
    authDomain: "asistencia-uladech.firebaseapp.com",
    databaseURL: "https://asistencia-uladech.firebaseio.com",
    projectId: "asistencia-uladech",
    storageBucket: "asistencia-uladech.appspot.com",
    messagingSenderId: "1095708802748"
  },
  actionCodeSettings: {
    url: 'http://localhost:4200/registro',
    handleCodeInApp: true
  },
  vapidKey:
  {
    "publicKey": "BCbMw-UiT-KIotQVYIOePbNIJnq9SrEn2pRmBDu8uMTEVj6uPIqA4FWgeqiZIq4zYyAN0M6SXd9_TvES9tO856k",
    "privateKey": "f8W5XiE6F5Sa_3xHHyJ4XN-U3aH8sPsys6QnYbSSV3A"
  },
  urlAPI: "https://api.nominus.pe/"
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
