import { Component, OnInit } from '@angular/core';
import { AuthService } from '../servicios/auth.service';

@Component({
  selector: 'app-prestamos',
  templateUrl: './prestamos.component.html',
  styleUrls: ['./prestamos.component.scss']
})
export class PrestamosComponent implements OnInit {
  sesion;

  constructor(public auth: AuthService) { }

  ngOnInit() {
    this.auth.Sesion().subscribe(rSes => {
      if (rSes) {
        this.sesion = rSes;
      }
    });
  }

}
