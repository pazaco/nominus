import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegistroComponent } from './registro/registro.component';
import { LaboratorioComponent } from './laboratorio/laboratorio.component';
import { BoletasComponent } from './boletas/boletas.component';
import { AsistenciaComponent } from './asistencia/asistencia.component';
import { VacacionesComponent } from './vacaciones/vacaciones.component';
import { PrestamosComponent } from './prestamos/prestamos.component';
import { UsuarioComponent } from './usuario/usuario.component';
import { JefaturaComponent } from './jefatura/jefatura.component';
import { LeerQRComponent } from './asistencia/leer-qr.component';

const routes: Routes = [
  { path: "", redirectTo: "home", pathMatch: "full" },
  { path: "home", component: HomeComponent },
  { path: 'login', component: LoginComponent, },
  { path: 'registro', component: RegistroComponent },
  { path: 'registro/:registro', component: RegistroComponent },
  { path: "laboratorio", component: LaboratorioComponent },
  { path: "boletas", component: BoletasComponent },
  { path: "asistencia", component: AsistenciaComponent },
  { path: "vacaciones", component: VacacionesComponent },
  { path: "prestamos", component: PrestamosComponent },
  { path: "usuario", component: UsuarioComponent },
  { path: "jefatura", component: JefaturaComponent },
  { path: "leerQR", component: LeerQRComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
