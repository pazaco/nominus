import { DataService } from "./../servicios/data.service";
import { AuthService } from "./../servicios/auth.service";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-laboratorio",
  templateUrl: "./laboratorio.component.html",
  styleUrls: ["./laboratorio.component.scss"]
})
export class LaboratorioComponent implements OnInit {
  tokensmin;

  constructor(private auth: AuthService, private data: DataService) {}

  ngOnInit() {
  }
}
