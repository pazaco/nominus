
import { LoginComponent } from "./login/login.component";
import { AngularFireMessagingModule } from "@angular/fire/messaging";
import { AngularFireAuthModule } from "@angular/fire/auth";
import { BrowserModule, DomSanitizer } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { ServiceWorkerModule } from "@angular/service-worker";
import { environment } from "../environments/environment";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import {
  MAT_DATE_LOCALE,
  MatButtonModule,
  MatIconRegistry,
  MatToolbarModule,
  MatIconModule,
  MatSidenavModule,
  MatFormFieldModule,
  MatTabsModule,
  MatInputModule,
  MatProgressBarModule,
  MatTooltipModule,
  MatExpansionModule,
  MAT_DATE_FORMATS,
  MatDatepickerModule
} from "@angular/material";
import { FlexLayoutModule } from "@angular/flex-layout";
import { HomeComponent } from "./home/home.component";
import { HttpClientModule } from "@angular/common/http";
import { ServiciosModule } from "./servicios/servicios.module";
import { ToastrModule } from "ngx-toastr";
import { AngularFireModule } from "@angular/fire";
import { AngularFireDatabaseModule } from "@angular/fire/database";
import { RegistroComponent } from "./registro/registro.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LaboratorioComponent } from './laboratorio/laboratorio.component';
import { BoletasComponent } from './boletas/boletas.component';
import { AsistenciaComponent } from './asistencia/asistencia.component';
import { VacacionesComponent } from './vacaciones/vacaciones.component';
import { PrestamosComponent } from './prestamos/prestamos.component';
import { UsuarioComponent } from './usuario/usuario.component';
import { JefaturaComponent } from './jefatura/jefatura.component';
import { NgQrScannerModule } from 'angular2-qrscanner';
import { LeerQRComponent } from './asistencia/leer-qr.component';
import { OrderByPipe, ZerosPipe, HorasPipe } from "./pipes/pipes";
import { MatMomentDateModule } from '@angular/material-moment-adapter';

@NgModule({
  declarations: [
    OrderByPipe,
    ZerosPipe,
    HorasPipe,
    AppComponent,
    HomeComponent,
    LoginComponent,
    RegistroComponent,
    LaboratorioComponent,
    BoletasComponent,
    AsistenciaComponent,
    VacacionesComponent,
    PrestamosComponent,
    UsuarioComponent,
    JefaturaComponent,
    LeerQRComponent
  ],
  imports: [
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    AngularFireMessagingModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    FlexLayoutModule,
    FormsModule,
    HttpClientModule,
    MatButtonModule,
    MatDatepickerModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatMomentDateModule,
    MatProgressBarModule,
    MatSidenavModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    NgQrScannerModule,
    FormsModule,
    ReactiveFormsModule,
    ServiceWorkerModule.register("ngsw-worker.js", {
      enabled: environment.production
    }),
    ServiciosModule,
    ToastrModule.forRoot({
      positionClass: "toast-bottom-center",
      preventDuplicates: true
    })
  ],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: "es-PE" },
    {
      provide: MAT_DATE_FORMATS,
      useValue: {
        parse: {
          dateInput: 'L'
        },
        display: {
          dateInput: 'L',
          monthYearLabel: 'MMM YYYY',
          dateA11yLabel: 'LL',
          monthYearA11Label: 'MMMM YYYY'
        }
      }
    }],
  bootstrap: [AppComponent],
})
export class AppModule {
  constructor(matIconRegistry: MatIconRegistry, domSanitizer: DomSanitizer) {
    matIconRegistry.addSvgIconSet(
      domSanitizer.bypassSecurityTrustResourceUrl(
        "./assets/icons/iconografia.svg"
      )
    );
  }
}
