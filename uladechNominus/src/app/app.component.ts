import { Router } from "@angular/router";
import { NavService } from "./servicios/nav.service";
import { AuthService } from "./servicios/auth.service";
import { Component, ViewChild, OnInit } from "@angular/core";
import { ToastContainerDirective, ToastrService } from "ngx-toastr";

declare var require: any;

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent implements OnInit {
  @ViewChild(ToastContainerDirective) toastContainer: ToastContainerDirective;
  public soloIconos = true;
  public Menu = [];
  public appVersion = require("../../package.json").version;

  constructor(
    public toastr: ToastrService,
    public auth: AuthService,
    public navi: NavService,
    public router: Router
  ) {}

  ngOnInit() {
    this.toastr.overlayContainer = this.toastContainer;
    this.auth.setBrowser(navigator.userAgent);
    this.auth.user.subscribe(rUser => {
      if (rUser) {
        this.auth.cambiarSesion(rUser);
        this.navi.Menu().subscribe(rMenu => (this.Menu = rMenu));
      }
    });
  }

  aLog() {
    this.router.navigate(["/login"]);
  }
}
