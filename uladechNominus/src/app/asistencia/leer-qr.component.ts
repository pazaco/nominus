import { Component, OnInit, ViewEncapsulation, ViewChild, Output, EventEmitter } from '@angular/core';
import { QrScannerComponent } from "angular2-qrscanner";
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'nom-qr',
  templateUrl: './leer-qr.component.html',
  styleUrls: ['./leer-qr.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LeerQRComponent implements OnInit {
  @ViewChild(QrScannerComponent) qrScannerComponent: QrScannerComponent;
  @Output() QRLeido = new EventEmitter<any>();
  @Output() devs = new EventEmitter<any>();

  constructor(private toast: ToastrService) { }

  ngOnInit() {
    this.qrScannerComponent.getMediaDevices().then(devices => {
      const videoDevices: MediaDeviceInfo[] = [];
      const videoNames = [];
      for (const device of devices) {
        if (device.kind.toString() === 'videoinput') {
          videoDevices.push(device);
          videoNames.push(device.label);
        }
      }
      if (videoDevices.length > 0) {
        let choosenDev;
        for (const dev of videoDevices) {
          if (dev.label.includes('back')) {
            choosenDev = dev;
            break;
          }
        }
        if (!choosenDev) {
          choosenDev = videoDevices[0];
        }
        if (choosenDev) {
          this.qrScannerComponent.chooseCamera.next(choosenDev);
        } else {
          this.qrScannerComponent.chooseCamera.next(videoDevices[0]);
        }
      }
    });
    this.qrScannerComponent.capturedQr.subscribe(result => {
      this.QRLeido.emit(result);
    });
  }
}
