import { AuthService } from "./../servicios/auth.service";
import { DataService } from "./../servicios/data.service";
import { Component, OnInit } from "@angular/core";
import * as RecordRTC from "recordrtc";
import { DomSanitizer } from "@angular/platform-browser";
import { HttpEventType, HttpResponse } from "@angular/common/http";
import { ToastrService } from "ngx-toastr";
import { interval, Subscription } from "rxjs";
import { FormControl } from "@angular/forms";
import * as moment from "moment";

@Component({
  selector: "app-asistencia",
  templateUrl: "./asistencia.component.html",
  styleUrls: ["./asistencia.component.scss"]
})
export class AsistenciaComponent implements OnInit {

  private record;
  private vozMarca;
  private selContrato;
  public selTab = new FormControl(0);
  public recording = false;
  public marcando = false;
  public QRon = true;
  public hoy = moment();
  public selDia = new FormControl(moment());
  public esperandoQR = false;
  public url;
  public geoEstablecimiento;
  public error;
  public sesion;
  public ubicado;
  public horario = [];
  public _reclutado = false;
  public vozActiva;
  public marcaPendiente = false;
  public pristine = true;
  public mpristine = true;
  public _vozActiva = interval(1000);
  public lVideos;
  iAsistencia: number;
  marcaPendienteDocente: boolean;

  constructor(
    private domSanitizer: DomSanitizer,
    private data: DataService,
    public auth: AuthService,
    public toast: ToastrService
  ) { }

  ngOnInit() {
    this.auth.Sesion().subscribe(rSes => {
      if (rSes) {
        this.sesion = rSes;
        // TODO : opcion de elegir contrato, por ahora se elige ULADECH (8)
        this.selContrato = this.sesion.contratos.filter(
          cco => cco.sServidor === 8
        )[0];
        this.chHorario();
      }
    });
    this.selDia.valueChanges.subscribe(xCad => this.chHorario());
  }

  videos(lista) {
    this.lVideos = JSON.stringify(lista);
  }

  qrLeido(valor) {
    this.esperandoQR = false;
    const jQR = JSON.parse(valor);
    jQR["iAsistencia"] = this.iAsistencia;
    this.data.agregarMarcaQR(this.sesion.token, jQR)
      .subscribe(rsp => {
        this.toast.info(rsp);
        this.chHorario();
      }, err => {
        this.toast.warning(JSON.stringify(err));
      });

  }

  reclutado() {
    this._reclutado = true;
    let nReclutado = 0;
    const mientrasAutorizan = this._vozActiva.subscribe(csg => {
      nReclutado += 1000;
      if (nReclutado > 20000) {
        mientrasAutorizan.unsubscribe();
        this._reclutado = false;
      }
      this.data
        .vozAbierta(this.sesion.token, {
          iPersona: this.sesion.uchempleado.iZkEmpleado,
          sServidor: this.selContrato.sServidor
        })
        .subscribe(rvz => {
          if (rvz["bFrase"]) {
            this.vozActiva = rvz;
          } else {
            this.vozActiva = null;
            mientrasAutorizan.unsubscribe();
            this._reclutado = false;
          }
        });
    });
  }

  sanitize(url: string) {
    return this.domSanitizer.bypassSecurityTrustUrl(url);
  }

  // iniciarMarca() {
  //     this.marcando = true;
  //     const mediaConstraints = {
  //       video: false,
  //       audio: true
  //     };
  //     navigator.mediaDevices
  //       .getUserMedia(mediaConstraints)
  //       .then(this.okVozMarca.bind(this), this.errorCallback.bind(this));
  // }

  initiateRecording() {
    this.recording = true;
    const mediaConstraints = {
      video: false,
      audio: true
    };
    navigator.mediaDevices
      .getUserMedia(mediaConstraints)
      .then(this.successCallback.bind(this), this.errorCallback.bind(this));
  }

  successCallback(stream) {
    const options = {
      mimeType: "audio/wav",
      bufferSize: 16384
    };
    this.pristine = false;
    const StereoAudioRecorder = RecordRTC.StereoAudioRecorder;
    this.record = new StereoAudioRecorder(stream, options);
    this.record.record();
  }

  okVozMarca(stream) {
    const options = {
      mimeType: "audio/wav",
      bufferSize: 16384
    };
    this.mpristine = false;
    const StereoAudioRecorder = RecordRTC.StereoAudioRecorder;
    this.vozMarca = new StereoAudioRecorder(stream, options);
    this.vozMarca.record();
  }

  terminarMarca() {
    this.marcando = false;
    this.vozMarca.stop(this.procesarMarca.bind(this));
  }

  stopRecording() {
    this.recording = false;
    this.record.stop(this.processRecording.bind(this));
  }

  processRecording(_blob) {
    _blob.filename = "waves";
    _blob.lastModifiedDate = new Date();
    const voz = {
      idTrack: { iTrack: this.vozActiva.iTrack, sServidor: 8 },
      data: _blob
    };
    this.vozActiva = null;
    this.data.subirVoz(voz).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {
        const porc = Math.round((100 * event.loaded) / event.total);
      } else if (event instanceof HttpResponse) {
        this.toast.success(
          "Voz registrada! Espere indicaciones del personal de RRHH."
        );
      }
    });
  }

  chHorario() {
    this.marcaPendiente = false;
    const ddd = moment(this.selDia.value, 'L');
    const parEmpleado = {
      iEmpleado: this.sesion.uchempleado.iZkEmpleado,
      dDia: ddd.format('YYYY-MM-DD')
    };
    this.data.diarioEmpleado(parEmpleado).subscribe(rhor => {
      const tHor = rhor;
      this.horario = [];
      if (tHor.oAsistencia) {
        this.horario = tHor.oAsistencia.aFlujo;
        this.iAsistencia = tHor.oAsistencia.iAsistencia;
        this.horario.filter(cflu => {
          if (cflu.iMarcacion) {
            cflu.oMarcacion = tHor.aMarcacion.filter(cma => cma.iMarcacion === cflu.iMarcacion)[0];
          }
        });
        this.marcaPendiente = moment().isSame(parEmpleado.dDia, "date") && this.horario.filter(cho => cho.dHoraControl === null && cho.bControl < 100).length > 0;
        this.marcaPendienteDocente = moment().isSame(parEmpleado.dDia, "date") && this.horario.filter(cho => cho.dHoraControl === null && cho.bControl > 100).length > 0;
      }
    });
  }

  habilitarCam(minutos) {
    this.esperandoQR = true;
    const reloj = interval(minutos * 60000);
    let tiempo: Subscription;
    tiempo = reloj.subscribe(tiem => {
      this.esperandoQR = false;
      tiempo.unsubscribe();
    });
  }

  procesarMarca(_blob) {
    _blob.filename = "waves";
    _blob.lastModifiedDate = new Date();
    const marca = {
      id: {
        iPersona: this.sesion.uchempleado.iZkEmpleado,
        bFrase: 1,
        sServidor: 8
      },
      data: _blob
    };
    this.data.subirMarca(marca).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {
        const porc = Math.round((100 * event.loaded) / event.total);
      } else if (event instanceof HttpResponse) {
        if (event.body === "Ok") {
          const parMarcacion = {
            iEmpleado: this.sesion.uchempleado.iZkEmpleado,
            dDia: moment().format('YYYY-MM-DD HH:mm:ss'),
            sDispositivo: 2,
            cEstablecimiento: this.geoEstablecimiento
          };
          this.data.agregarMarcacion(parMarcacion).subscribe(rsp => {
            const parEmpleado = {
              iEmpleado: this.sesion.uchempleado.iZkEmpleado,
              dDia: moment().format('YYYY-MM-DD')
            };
            this.data.diarioEmpleado(parEmpleado).subscribe(rhor => {
              const tHor = rhor;
              this.horario = [];
              if (tHor.oAsistencia) {
                this.horario = tHor.oAsistencia.aFlujo;
                this.iAsistencia = tHor.oAsistencia.iAsistencia;
                let hayMarcacion = false;
                this.horario.filter(cflu => {
                  if (cflu.iMarcacion) {
                    cflu.oMarcacion = tHor.aMarcacion.filter(cma => cma.iMarcacion === cflu.iMarcacion)[0];
                    if (cflu.iMarcacion === rsp) {
                      hayMarcacion = true;
                      this.toast.success(cflu.xControl + " a las " + cflu.oMarcacion.dHora, "TESTIGO DE MARCACIÓN");
                    }
                  }
                });
                if (!hayMarcacion) {
                  this.toast.warning("No es coherente marcar a esta hora.", "Revise su horario!");
                }
                this.marcaPendiente = moment().isSame(parMarcacion.dDia, "date") && this.horario.filter(cho => cho.dHoraControl === null).length > 0;
              }
            });
          });
        }
      }
    });
  }

  geoUbicado() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(pos => {
        const parGeo = {
          establecimientos: this.sesion.uchempleado.cEstablecimiento,
          punto: { fLat: pos.coords.latitude, fLon: pos.coords.longitude }
        };
        console.log(parGeo);
        this.data.geoUbicacion(parGeo)
          .subscribe(rspG => {
            if (rspG.filter((cEst: any) => cEst.lMatch).length === 0) {
              this.toast.warning("Debe estar ubicado dentro de la universidad.");
              return false;
            } else {
              this.geoEstablecimiento = rspG.filter(cEst => cEst.lMatch)[0].cEstablecimiento;
              this.toast.info("Diga su nombre y termine con el botón stop");
              this.marcando = true;
              const mediaConstraints = {
                video: false,
                audio: true
              };
              navigator.mediaDevices
                .getUserMedia(mediaConstraints)
                .then(this.okVozMarca.bind(this), this.errorCallback.bind(this));
            }
          });
      });
    } else {
      this.toast.error("Este procedimento requiere GPS");
    }
  }


  errorCallback(error) {
    this.error = "Su aplicativo no permite sonidos";
  }
}
