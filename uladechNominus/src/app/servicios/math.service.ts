import { Injectable } from "@angular/core";

@Injectable()
export class MathService {
  constructor() {}

  Interseccion(arr1: any[], arr2: any[]): any[] {
    const intersec: any[] = [];
    if (arr1.length > 0 && arr2.length > 0) {
      arr1.filter(itm => {
        if (arr2.indexOf(itm) >= 0) {
          intersec.push(itm);
        }
      });
    }
    return intersec;
  }
}
