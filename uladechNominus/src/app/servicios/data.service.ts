import { Injectable } from "@angular/core";
import {
  HttpClient,
  HttpHeaders,
  HttpEvent,
  HttpRequest
} from "@angular/common/http";
import { Observable } from "rxjs";
import * as moment from "moment";

@Injectable()
export class DataService {

  private api = "https://api.nominus.pe/";
  // private api = "http://localhost:5700/";
  private iCalculoPlanilla = 0;
  private sCiclo = 0;
  private xUrl = "";
  public token: string;
  public dataInit = {
    prestamos: false,
    vacaciones: false,
    indiceBoletas: false
  };
  private monedas = [];

  constructor(private http: HttpClient) { }

  subirVoz(voz): Observable<HttpEvent<any>> {
    const hdrVoz = this.optUpload({ nombre: "idTrack", valor: voz.idTrack });
    const file = new File([voz.data], voz.name);
    const fd = new FormData();
    fd.set("waves", voz.data, voz.name);
    console.log("FormData", fd);
    const req = new HttpRequest(
      "POST",
      this.api + "voiceprint/upload",
      fd,
      hdrVoz
    );
    return this.http.request(req);
  }

  subirMarca(marca): Observable<HttpEvent<any>> {
    const hdrVoz = this.optUpload({ nombre: "vozMarca", valor: marca.id });
    const file = new File([marca.data], marca.name);
    const fd = new FormData();
    fd.set("waves", marca.data, marca.name);
    console.log("FormData", fd);
    const req = new HttpRequest(
      "POST",
      this.api + "voiceprint/uploadMarca",
      fd,
      hdrVoz
    );
    return this.http.request(req);
  }




  vozAbierta(token, usuario): Observable<any> {
    return this.http.post(this.api + "voiceprint/vozAbierta", usuario, this.optTkn(token));
  }

  voces(token, usuario): Observable<any> {
    return this.http.post(this.api + "voiceprint/usuario", usuario, this.optTkn(token));
  }

  getCalculoPlanilla(): number {
    return this.iCalculoPlanilla;
  }

  setCalculoPlanilla(numCP: number) {
    this.iCalculoPlanilla = numCP;
  }

  getUrl(): string {
    return this.xUrl;
  }
  setUrl(url: string) {
    this.xUrl = url;
  }

  boletasIndice(token): Observable<any> {
    return this.http.get(this.api + "bel/indice", this.optTkn(token));
  }

  frases(token, servidor): Observable<any> {
    return this.http.get(this.api + "voiceprint/tipofrase/" + servidor, this.optTkn(token));
  }

  vacaciones(token, tuplaContrato): Observable<any> {
    return this.http.post(
      this.api + "webapp/vacaciones",
      tuplaContrato,
      this.optTkn(token)
    );
  }

  agregarMarcaQR(token, qr): Observable<any> {
    console.log(token, JSON.stringify(qr));
    return this.http.post(
      this.api + "asi/agregarMarcaQR",
      qr,
      this.optTkn(token)
    );
  }

  prestamos(token, tuplaContrato): Observable<any> {
    return this.http.post(
      this.api + "webapp/prestamos",
      tuplaContrato,
      this.optTkn(token)
    );
  }

  notificaciones(token): Observable<any> {
    return this.http.post(
      this.api + "notificaciones/notificar",
      null,
      this.optTkn(token)
    );
  }

  asociarPush(token, tokenPush) {
    this.http
      .get(
        this.api + "auth/asociarPush?tokenPush=" + tokenPush,
        this.optTkn(token)
      )
      .subscribe(rsp => { });
  }

  solicitudVacaciones(token, parSolicitud): Observable<any> {
    return this.http.post(
      this.api + "webapp/solicitarVacaciones",
      parSolicitud,
      this.optTkn(token)
    );
  }

  agregarMarcacion(tDiaEmpleado): Observable<any> {
    return this.http.post(
      this.api + "asi/agregarMarcacion",
      tDiaEmpleado,
      this.optTkn(this.token)
    );
  }

  printBoleta(token, oCalculo): Observable<any> {
    return this.http.post(this.api + "bel/data", oCalculo, this.optTkn(token));
  }

  testTM(): Observable<any> {
    const osd = this.optSeed();
    return this.http.get(this.api + "auth/testTM", osd);
  }

  loginFirebase(parLogin: any): Observable<any> {
    const osd = this.optSeed();
    return this.http.post(this.api + "auth/firebase", parLogin, osd);
  }

  diarioEmpleado(tDiaEmpleado): Observable<any> {
    return this.http.post(
      this.api + "asi/diarioEmpleado",
      tDiaEmpleado,
      this.optTkn(this.token)
    );
  }

  geoUbicacion(geoRef): Observable<any> {
    return this.http.post(this.api + "asi/geoUbicacion", geoRef, this.optTkn(this.token));
  }

  private optSeed(): any {
    return {
      headers: new HttpHeaders()
        .set("content-type", "application/json;charset=utf-8")
        .set("User-Seed", this.minutero("app.nominus.pe$$"))
        .set("token-utc", "true")
    };
  }

  private optMinuto(): any {
    return {
      headers: new HttpHeaders()
        .set("content-type", "application/json;charset=utf-8")
        .set("xTokenMin", this.minutero("app-from-nominus.pe"))
        .set("token-utc", "true")
    };
  }



  private optPlus(token, dPlus): any {
    const sHeaders = new HttpHeaders()
      .set("content-type", "application/json;charset=utf-8")
      .set("sesionPersona", token)
      .set(dPlus.nombre, JSON.stringify(dPlus.valor));
    return { headers: sHeaders };
  }

  private optUpload(dPlus): any {
    const sHeaders = new HttpHeaders()
      .set("sesionPersona", this.token)
      .set(dPlus.nombre, JSON.stringify(dPlus.valor));
    return { headers: sHeaders };
  }

  private optTkn(token, contrato = null): any {
    let sHeaders = new HttpHeaders().set(
      "content-type",
      "application/json;charset=utf-8"
    );
    sHeaders = sHeaders.set("sesionPersona", token);
    if (contrato) {
      sHeaders = sHeaders.set("contrato", JSON.stringify(contrato));
    }
    return { headers: sHeaders };
  }

  moneda(token, sMoneda): any {
    if (this.monedas.length > 0) {
      const moneda = this.monedas.filter(cmo => cmo.sMoneda === sMoneda);
      if (moneda.length === 0) {
        return {
          sMoneda: 604,
          xMoneda: "Sol",
          xMonedaPlural: "Soles",
          xSimbolo: "S/ ",
          xCodigo: "PEN",
          rDecimales: 2
        };
      } else {
        return moneda[0];
      }
    } else {
      this.http
        .get(this.api + "tipologia/monedas", this.optTkn(token))
        .subscribe(rsp => {
          this.monedas = JSON.parse(JSON.stringify(rsp));
          console.log(this.monedas);
        });
    }
  }

  public minutero(sujeto): string {
    let hashPassword = "";
    const passwd = moment.utc().format("mmDDMM[Y]YY[T]mmHH");
    let infoId = sujeto + passwd;
    const lpo =
      "ab9cK4fstv5^*dCDSxy7zAB#$%0&=TUV6OPZ1emnE2gQRWXYhjklLMNpqrFG3H8I@)J";
    let nLen = 0;
    let nSeed: number;
    let nPass: number;
    let pos = 1;
    let hPwd = "";
    let pChar: number;
    while (infoId.length < 128) {
      infoId = infoId + infoId;
    }
    while (hPwd.length < 128) {
      hPwd = hPwd + passwd;
    }
    while (pos <= passwd.length) {
      nLen += passwd.substr(pos - 1, 1).charCodeAt(0);
      pos++;
    }
    pos = 1;
    while (pos <= 128) {
      nSeed = lpo.indexOf(infoId.substr(pos - 1, 1)) + 1;
      nPass = hPwd.substr(pos - 1, 1).charCodeAt(0);
      pChar = 1 + ((2 * nSeed + 5 * nPass + 3 * nLen) % 67);
      hashPassword += lpo.substr(pChar - 1, 1);
      pos++;
    }
    return hashPassword;
  }
}
