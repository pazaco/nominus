import { Injectable } from "@angular/core";

@Injectable()
export class MathService {
  constructor() { }

  Interseccion(arr1: any[], arr2: any[]): any[] {
    let intersec: any[] = [];
    if (arr1.length > 0 && arr2.length > 0) {
      arr1.filter(itm => {
        if (arr2.indexOf(itm) >= 0) {
          intersec.push(itm);
        }
      });
    }
    return intersec;
  }

  Union(arr1: any[], arr2: any[]): any[] {
    if (arr1 !== null && arr2 !== null) {
      let uni = arr1;
      arr2.filter(itm => {
        if (uni.indexOf(itm) === -1) {
          uni.push(itm);
        }
      });
      return uni;
    } else {
      return null;
    }
  }

  BuscarTupla(aConjunto: any[], oTupla: any): any {
    let aMatch = aConjunto;
    if (JSON.stringify(oTupla) !== "{}") {
      Object.keys(oTupla).forEach(ck => {
        aMatch = aMatch.filter(cobj => (oTupla[ck] === cobj[ck]));
      });
      return aMatch[0] || {};
    } else {
      return {};
    }
  }

}
