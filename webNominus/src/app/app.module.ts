import { BrowserModule, DomSanitizer } from '@angular/platform-browser';
import { NgModule, Injector } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatIconRegistry, MatIconModule } from '@angular/material';
import { HttpClientModule } from '@angular/common/http';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MenuComponent } from './menu/menu.component';
import { createCustomElement } from '@angular/elements';
import { AsyncPipe } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { FocusElementDirective, AutoFocusDirective } from './recursos/directives.directive';
import { OrderByPipe } from './recursos/pipes.pipe';
import { StorageModule } from '@ngx-pwa/local-storage';
import { DataService } from './services/data.service';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { ToastrModule } from 'ngx-toastr';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    HomeComponent,
    FocusElementDirective,
    AutoFocusDirective,
    OrderByPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    BrowserAnimationsModule,
    MatIconModule,
    HttpClientModule,
    FlexLayoutModule,
    // StorageModule.forRoot({
    //   IDBNoWrap: true
    // }),
    ToastrModule.forRoot({
      positionClass: 'toast-bottom-center',
      preventDuplicates: true,
    }),
  ],
  entryComponents: [
    MenuComponent
  ],
  providers: [AsyncPipe, DataService],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(matIconRegistry: MatIconRegistry, domSanitizer: DomSanitizer, injector: Injector) {
    const menuMain = createCustomElement(MenuComponent, { injector });
    customElements.define('menu-main', menuMain);
    matIconRegistry.addSvgIconSet(domSanitizer.bypassSecurityTrustResourceUrl('./assets/mdi-nominus.svg'));
  }
}
