import { Directive, Input, ElementRef, AfterContentInit, OnInit, Renderer2 } from '@angular/core';

@Directive({
  selector: '[autoFocus]'
})
export class AutoFocusDirective implements AfterContentInit {
  @Input() public autoFocus: boolean;

  constructor(private el: ElementRef) { }

  ngAfterContentInit(): void {
    setTimeout(() => {
      this.el.nativeElement.focus();
    }, 300);
  }
}

@Directive({ selector: '[autoFocusElement]' })
export class FocusElementDirective implements OnInit {

  @Input() eleFocus: boolean;

  constructor(private hostElement: ElementRef) { }

  ngOnInit() {
    if (this.eleFocus) {
      this.hostElement.nativeElement.focus()
//      this.renderer.invokeElementMethod(this.hostElement.nativeElement, 'focus');
    }
  }
}
