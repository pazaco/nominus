import { Component, OnInit } from '@angular/core';
import { DataService } from './services/data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  openNav: false;
  openNotif: false;
  constructor(private sdata: DataService) { }

  async ngOnInit() {
    const zLogin = {
      xRestful: "auth/firebase",
      oBody: {
        xEmail: "xEmail",
        providerId: "providerId"
      },
      data:
      {
        xEmail: "jorge@loaizas.com",
        providerId: "google.com"
      },
      lCache: false,
      sDuracion: 200
    };
    console.log(await this.sdata.httpS(zLogin));

  }

}
