import { Component, OnInit } from '@angular/core';
import { AuthService } from '../recursos/auth.service';
import { DataService } from '../recursos/data.service';

@Component({
  selector: 'app-jefatura',
  templateUrl: './jefatura.component.html',
  styleUrls: ['./jefatura.component.scss']
})
export class JefaturaComponent implements OnInit {
  _jefatura: any[];

  constructor(public authService: AuthService, public data: DataService) { }

  ngOnInit() {
  }

}
