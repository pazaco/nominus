import { Component, OnInit, HostBinding, OnDestroy } from "@angular/core";
import { AuthService } from "../recursos/auth.service";
import { Subscription } from "rxjs";
import { DataService } from "../recursos/data.service";
import { Router } from "@angular/router";
import { MessagingService } from "../recursos/messaging.service";
import { Observable } from 'rxjs';

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"]
})
export class HomeComponent implements OnInit, OnDestroy {
  // private sesion_: Subscription;
  _excepcional;

  constructor(public authService: AuthService, public data: DataService, private router: Router, private mensaje: MessagingService) { }

  ngOnInit() {
    this.authService.sesion().subscribe(ses => {
      if (ses) {
        if (ses.perfiles.length === 0) {
          this._excepcional = "Su correo no se encuentra asociado a ningún perfil de ninguna de las empresas registradas en nuestro sistema.  De acuerdo a Ley, usted debe registrar su correo " + ses.usuario.email + " en la empresa donde labora.";
        }
      }
    });
  }

  ngOnDestroy() {
  }

  lanzarUrl(oj) {
    if (oj.bTipoNotificacion === 1) {
      this.data.setCalculoPlanilla(oj.iReferencia * 100 + oj.sServidor);
      this.router.navigateByUrl("/boletas");
    }
  }
}
