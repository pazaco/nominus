import { animate, AnimationTriggerMetadata, state, style, transition, trigger, keyframes } from '@angular/animations';

export const aniSlideInDown: AnimationTriggerMetadata =
    trigger('routeAnimation', [
        state('*',
            style({
                opacity: 1,
                transform: 'translateX(0)'
            })
        ),
        transition(':enter', [
            style({
                opacity: 0,
                transform: 'translateX(-100%)'
            }),
            animate('0.4s ease-in')
        ]),
        transition(':leave', [
            animate('0.8s ease-out', style({
                opacity: 0,
                transform: 'translateY(100%)'
            }))
        ])
    ]);

    export const aniMenuMax: AnimationTriggerMetadata =
    trigger('menuMax', [
        state('maxi', style({
            width: '176px'
        })),
        state('mini,void', style({
            width: '0px'
        })),
        transition('* => *', animate('300ms'))
    ]);

    export const aniOpacoMax: AnimationTriggerMetadata =
    trigger('opacoMax', [
        state('maxi', style({
            opacity: '0.5'
        })),
        state('mini,void', style({
            opacity: '0'
        })),
        transition('* => *', animate('300ms'))
    ]);

export const aniBotonMax: AnimationTriggerMetadata =
    trigger('botonMax', [
        state('maxi,void', style({
            left: '182px'
        })),
        state('mini', style({
            left: '56px'
        })),
        state('nada', style({
            left: '4px'
        })),
        transition('mini => maxi', animate('120ms')),
        transition('* => *', animate('300ms'))
    ]);
    export const aniLogoMax: AnimationTriggerMetadata =
    trigger('logoMax', [
        state('maxi', style({
            width: '100px'
        })),
        state('mini,nada,void', style({
            width: '0px'
        })),
        transition('* => *', [style({opacity:'0'}),animate('2ms')]),
    ]);
    export const aniLogoMin: AnimationTriggerMetadata =
    trigger('logoMin', [
        state('maxi,nada,void', style({
            width: '0px'
        })),
        state('mini', style({
            width: '30px',
        })),
        transition('* => *', [style({opacity:'0'}),animate('2ms')]),
    ]);
export const aniTextMax: AnimationTriggerMetadata =
    trigger('textMax', [
        state('maxi', style({
            width: '54px',
            height: '60px'
        })),
        state('mini,nada', style({
            width: '0px',
            height: '60px'
        })),
        transition('* => *', animate('140ms'))
    ]);
export const aniSubMnu: AnimationTriggerMetadata =
    trigger('subMnu', [
        state('si', style({
            maxHeight: '35px'
        })),
        state('no', style({
            maxHeight: '0'
        })),
        transition('* => *', animate('300ms'))
    ]);
export const aniMiniMnu: AnimationTriggerMetadata =
    trigger('miniMnu', [
        state('no', style({
            maxWidth: '0'
        })),
        state('si', style({
            maxWidth: '200px'
        })),
        transition('* => *', animate('300ms'))
    ]);
