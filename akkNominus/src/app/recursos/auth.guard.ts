import { Injectable } from "@angular/core";
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from "@angular/router";
import { Observable } from "rxjs";
import { map, take, tap } from "rxjs/operators";
import { AuthService } from "./auth.service";
import { ToastrService } from "ngx-toastr";

@Injectable({
  providedIn: "root"
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router, private authService: AuthService, private toast: ToastrService) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    return this.authService.sesion().pipe(
      map(sesion => {
        if (sesion) {
          if (state.url === "/boletas" || state.url === "/usuario" || state.url === "/vacaciones") {
            if (sesion.perfiles.indexOf(1) === -1) {
              this.toast.warning("No existe ningún contrato asociado con su cuenta.");
              return false;
            } else {
              return true;
            }
          } else {
            if (state.url === "/asistencia") {
              if (sesion.perfiles.indexOf(3) === -1) {
                this.toast.warning("No hay controles de asistencia en ninguno de sus contratos.");
                return false;
              } else {
                return true;
              }
            } else {
              if (state.url === "/prestamos") {
                if (sesion.perfiles.indexOf(4) === -1) {
                  this.toast.warning("No se conocen préstamos a su nombre.");
                  return false;
                } else {
                  return true;
                }
              } else {
                if (state.url === "/jefatura") {
                  if (sesion.perfiles.indexOf(5) === -1) {
                    this.toast.warning("No tienes funciones de jefatura.");
                    return false;
                  } else {
                    return true;
                  }
                } else {
                  return true;
                }
              }
            }
          }
        } else {
          this.toast.warning("Debes iniciar sesión.");
          return false;
        }
      }),
      tap(puedeAcceder => {
        if (!puedeAcceder) {
          this.router.navigate(["/login"]);
        }
      })
    );
  }
}
