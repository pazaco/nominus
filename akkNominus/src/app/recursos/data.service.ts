import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable, BehaviorSubject, of } from "rxjs";
import { map, tap } from "rxjs/operators";
import * as moment from "moment";

@Injectable()
export class DataService {
  private api = "https://api.nominus.pe/";
  // private api = "http://localhost:64000/";
  private iCalculoPlanilla = 0;
  private sCiclo = 0;
  private xUrl = "";
  public dataInit = { prestamos: false, vacaciones: false, indiceBoletas: false };
  private monedas = [];

  constructor(private http: HttpClient) { }

  getCalculoPlanilla(): number {
    return this.iCalculoPlanilla;
  }

  setCalculoPlanilla(numCP: number) {
    this.iCalculoPlanilla = numCP;
  }

  getUrl(): string {
    return this.xUrl;
  }
  setUrl(url: string) {
    this.xUrl = url;
  }

  boletasIndice(token): Observable<any> {
    return this.http.get(this.api + "bel/indice", this.optTkn(token));
  }

  vacaciones(token, tuplaContrato): Observable<any> {
    return this.http.post(this.api + 'webapp/vacaciones', tuplaContrato, this.optTkn(token));
  }

  prestamos(token, tuplaContrato): Observable<any> {
    return this.http.post(this.api + 'webapp/prestamos', tuplaContrato, this.optTkn(token));
  }

  notificaciones(token): Observable<any> {
    return this.http.post(this.api + 'notificaciones/notificar', null, this.optTkn(token));
  }

  asociarPush(token, tokenPush) {
    this.http.get(this.api + 'auth/asociarPush?tokenPush=' + tokenPush, this.optTkn(token)).subscribe(rsp => { });
  }

  solicitudVacaciones(token, parSolicitud): Observable<any> {
    return this.http.post(this.api + 'webapp/solicitarVacaciones', parSolicitud, this.optTkn(token));
  }

  printBoleta(token, oCalculo): Observable<any> {
    return this.http.post(
      this.api + "bel/data",
      oCalculo,
      this.optTkn(token)
    );
  }

  loginFirebase(parLogin: any): Observable<any> {
    return this.http.post(this.api + "auth/firebase", parLogin, this.optSeed());
  }

  private optSeed(): any {
    return {
      headers: new HttpHeaders()
        .set("content-type", "application/json;charset=utf-8")
        .set("User-Seed", this.minutero())
        .set("token-utc", "true")
      };
  }

  private optTkn(token, contrato = null): any {
    let sHeaders = new HttpHeaders().set(
      "content-type",
      "application/json;charset=utf-8"
    );
    sHeaders = sHeaders.set("sesionPersona", token);
    if (contrato) {
      sHeaders = sHeaders.set("contrato", JSON.stringify(contrato));
    }
    return { headers: sHeaders };
  }

  moneda(token, sMoneda): any {
    if (this.monedas.length > 0) {
      const moneda = this.monedas.filter(cmo => cmo.sMoneda === sMoneda);
      if (moneda.length === 0) {
        return { sMoneda: 604, xMoneda: "Sol", xMonedaPlural: "Soles", xSimbolo: "S/ ", xCodigo: "PEN", rDecimales: 2 };
      } else {
        return moneda[0];
      }
    } else {
      this.http.get(this.api + 'tipologia/monedas', this.optTkn(token)).subscribe( rsp => {
        this.monedas = JSON.parse(JSON.stringify(rsp));
        console.log(this.monedas);
      });
    }
  }

  public minutero(): string {
    let hashPassword = "";
    const passwd = moment.utc().format("mmDDMM[Y]YY[T]mmHH");
    let hashSemilla = "app.nominus.pe$$" + passwd;
    const lpo =
      "ab9cK4fstv5^*dCDSxy7zAB#$%0&=TUV6OPZ1emnE2gQRWXYhjklLMNpqrFG3H8I@)J";
    let nLen = 0;
    let nSeed: number;
    let nPass: number;
    let pos = 1;
    let hPwd = "";
    let pChar: number;
    while (hashSemilla.length < 128) {
      hashSemilla = hashSemilla + hashSemilla;
    }
    while (hPwd.length < 128) {
      hPwd = hPwd + passwd;
    }
    while (pos <= passwd.length) {
      nLen += passwd.substr(pos - 1, 1).charCodeAt(0);
      pos++;
    }
    pos = 1;
    while (pos <= 128) {
      nSeed = lpo.indexOf(hashSemilla.substr(pos - 1, 1)) + 1;
      nPass = hPwd.substr(pos - 1, 1).charCodeAt(0);
      pChar = 1 + ((2 * nSeed + 5 * nPass + 3 * nLen) % 67);
      hashPassword += lpo.substr(pChar - 1, 1);
      pos++;
    }
    return hashPassword;
  }
}
