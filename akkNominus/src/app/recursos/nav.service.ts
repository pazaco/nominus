import { BehaviorSubject, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { MathService } from './math.service';
import { Router } from '@angular/router';
import { AuthService } from './auth.service';

export const MENU = [
  { id: 'hom', menu: 'Notificaciones', icono: {base: 'bell'}, link: '/home'},
  { id: 'bol', menu: 'Boletas', icono: {base: 'cash'}, link: '/boletas'},
  { id: 'asi', menu: 'Asistencia', icono: { base: 'clock' }, link: '/asistencia'},
  { id: 'vac', menu: 'Vacaciones', icono: { base: 'beach' }, link: '/vacaciones'},
  { id: 'prs', menu: 'Préstamos', icono: {base: 'cash-usd'}, link: '/prestamos'},
  { id: 'usr', menu: 'Contratos', icono: { base: 'account-box-multiple' }, link: '/usuario'},
  {id: 'jfe', menu: 'Jefatura', icono: {base: 'account-supervisor' }, link: '/jefatura' },
  // {id: 'hlp', menu: 'Ayuda', icono: {base: 'help-box' }, link: '/ayuda' }
];

@Injectable()
export class NavService {
  private mn = [];
  private perfiles = [];
  private tamanho = 'mini';

  private menu: any[] = this._defineMenu();
  private __menu = new BehaviorSubject<any[]>(this.menu);
  private __tamanho = new BehaviorSubject<string>('mini');

  constructor(private math: MathService, private router: Router) { }

  Menu(): Observable<any[]> {
    return this.__menu.asObservable();
  }

  setPerfiles(perfiles) {
    this.perfiles = perfiles;
    this.menu = this._defineMenu();
    this.__menu.next(this.menu);
  }

  Tamanho(): Observable<string> {
    return this.__tamanho.asObservable();
  }

  // defineMenu(perfil: any[]) {
  //   this.menu = this._defineMenu(perfil);
  //   this.__menu.next(this.menu);
  // }

  crumbs() {
    const routeActivo = this.router.routerState.snapshot.url;
    const oRsp = { menu: null, opcion: null, icono: null };
    let menuAct = this.menu.filter(cmn => cmn.link === routeActivo);
    if (menuAct.length > 0) {
      oRsp.menu = "";
      oRsp.opcion = "";
      oRsp.icono = menuAct[0].icono;
    } else {
      menuAct = this.menu.filter(cmn => cmn.id === routeActivo.substring(1, 4));
      if (menuAct.length > 0) {
      const subMenu = menuAct[0].submenu.filter(csm => csm.link === routeActivo);
      if (subMenu.length > 0) {
        oRsp.menu = menuAct[0].menu;
        oRsp.opcion = subMenu[0].opcion;
        oRsp.icono = subMenu[0].icono;
      }}
    }
    return oRsp;
  }

  crumb() {
    const routeActivo = this.router.routerState.snapshot.url;
    const oRsp = { menu: null, icono: null };
    const menuAct = this.mn.filter(cmn => cmn.link === routeActivo)[0];
    return {menu: menuAct.menu, icono: menuAct.icono};
  }

  colapsarMinis() {
    this.menu.filter(csm => csm.miniEx = 'no');
    this.__menu.next(this.menu);
  }

  private _defineMenu(): any[] {
    this.mn = [];
    MENU.filter(opcMenu => {
    if (opcMenu.link === "/boletas" || opcMenu.link === "/usuario" || opcMenu.link === "/vacaciones") {
      if (this.perfiles.indexOf(1) === -1) {
        return false;
      } else {
        this.mn.push(opcMenu);
      }
    } else {
      if (opcMenu.link === "/asistencia") {
        if (this.perfiles.indexOf(3) === -1) {
          return false;
        } else {
          this.mn.push(opcMenu);
        }
      } else {
        if (opcMenu.link === "/prestamos") {
          if (this.perfiles.indexOf(4) === -1) {
            return false;
          } else {
            this.mn.push(opcMenu);
          }
        } else {
          if (opcMenu.link === "/jefatura") {
            if (this.perfiles.indexOf(5) === -1) {
              return false;
            } else {
              this.mn.push(opcMenu);
            }
          } else {
            this.mn.push(opcMenu);
          }
        }
      }
    }
  });
  return this.mn;
  }

  toggle() {
    switch (this.tamanho) {
      case 'maxi': this.tamanho = 'nada'; break;
      case 'nada': this.tamanho = 'mini'; break;
      case 'mini': this.tamanho = 'maxi'; break;
    }
    this.__tamanho.next(this.tamanho);
  }

  swipeLeft() {
    if (this.tamanho === 'mini') {
      this.tamanho = 'nada';
    }
    if (this.tamanho === 'maxi') {
      this.tamanho = 'mini';
    }
    this.__tamanho.next(this.tamanho);
  }

  swipeRight() {
    if (this.tamanho === 'mini') {
      this.tamanho = 'maxi';
    }
    if (this.tamanho === 'nada') {
      this.tamanho = 'mini';
    }
    this.__tamanho.next(this.tamanho);
  }

  toggleVisible() {
    this.tamanho = this.tamanho !== 'maxi' ? 'maxi' : 'mini';
    this.__tamanho.next(this.tamanho);
  }
}

