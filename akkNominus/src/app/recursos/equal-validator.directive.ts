import { Directive, Attribute, forwardRef } from "@angular/core";
import {
  Validator,
  NG_VALIDATORS,
  AbstractControl,
  FormGroup
} from "@angular/forms";

@Directive({
  selector:
    // tslint:disable-next-line:directive-selector
    "[validateEqual][formControlName],[validateEqual][formControl],[validateEqual][ngModel]",
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => EqualValidatorDirective),
      multi: true
    }
  ]
})
export class EqualValidatorDirective implements Validator {
  constructor(
    @Attribute("validateEqual") public validateEqual: string,
    @Attribute("reverse") public reverse: string
  ) {}
  private get isReverse() {
    if (!this.reverse) {
      return false;
    }
    return this.reverse === "true" ? true : false;
  }

  validate(c: AbstractControl): { [key: string]: any } {
    // self value
    const v = c.value;

    // control value
    const e = c.root.get(this.validateEqual);

    // value not equal
    if (e && v !== e.value && !this.isReverse) {
      return {
        validateEqual: false
      };
    }

    // value equal and reverse
    if (e && v === e.value && this.isReverse) {
      delete e.errors["validateEqual"];
      if (!Object.keys(e.errors).length) {
        e.setErrors(null);
      }
    }

    // value not equal and reverse
    if (e && v !== e.value && this.isReverse) {
      e.setErrors({ validateEqual: false });
    }

    return null;
  }
}

export class RegistrationValidator {
  static validate(registrationFormGroup: FormGroup): RspValidador {
    const password = registrationFormGroup.controls.password.value;
    const repeatPassword = registrationFormGroup.controls.verificar.value;

    if (repeatPassword.length <= 0) {
      return null;
    }

    if (repeatPassword !== password) {
      return {
        passwordsDiferentes: true
      };
    }
    return null;
  }
}

export interface RspValidador {
  passwordsDiferentes: boolean;
}
