import { Component, OnInit, OnDestroy } from "@angular/core";
import { DataService } from "../recursos/data.service";
import { AuthService } from "../recursos/auth.service";
import { Subscription } from "rxjs";
import { OrderByPipe } from "../recursos/pipes.pipe";
import { DatePipe } from '@angular/common';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';

import * as _moment from 'moment';
import { ToastrService } from "ngx-toastr";

_moment.locale("es");

export interface Ciclo {
  sCiclo: number;
  dHasta: string;
  dDesde: string;
  cStatus: string;
  sGeneradas: number;
  sGozadas: number;
  sVendidas: number;
  sAcordadas: number;
  sSaldo: number;
  zContrato: number;
  lCerrado: boolean;
  sTope: number;
}

@Component({
  selector: "app-vacaciones",
  templateUrl: "./vacaciones.component.html",
  styleUrls: ["./vacaciones.component.css"],
  providers: [
    DatePipe,
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})
export class VacacionesComponent implements OnInit, OnDestroy {
  vacaciones: Ciclo[] = [];
  novedades: any[];
  registros: string;
  _novedades: Subscription;
  token;
  totales = { sVencidas: 0, sDisponibles: 0, sSaldo: 0, sCicloActual: 0, sTruncas: 0, lPuedeSolicitar: true };
  iniciando: boolean;
  contratos: any[] = [];
  disponibilidad = { dias: 0, tipo: '' };
  selContrato: number;
  selCiclo: number;
  solicitud = { dias: 7, desde: _moment().add(1, 'days'), hasta: _moment().add(7, 'days'), absDesde: _moment().add(1, 'days'), xAsunto: "" };
  colsCiclo = ['sCiclo', 'dHasta', 'sGeneradas', 'sGozadas', 'sVendidas', 'sAcordadas', 'sSaldo'];
  colsMovi = ['dDesde', 'dHasta', 'iDias', 'cTipoNovedad', 'xDescripcion'];
  private _sesion: Subscription;
  orderBy = new OrderByPipe();

  constructor(public authService: AuthService, public data: DataService, private datePipe: DatePipe, private toast: ToastrService) { }

  ngOnInit() {
    this.iniciando = true;
    this._sesion = this.authService.sesion().subscribe(ses => {
      if (ses) {
        this.token = ses.token;
        if (ses.contratos.length > 0) {
          ses.contratos.filter(cse => {
            cse.empresas.filter(cem => {
              cem.contratos.filter(cct => {
                let fSuperior = new Date();
                fSuperior.setHours(0);
                fSuperior.setMinutes(0);
                fSuperior.setSeconds(0);
                fSuperior.setMilliseconds(0);
                const tCtt = {
                  iContrato: cct.iContrato,
                  xContrato: cct.xCargo + " /" + cem.xAbreviado,
                  sServidor: cse.sServidor,
                  zContrato: cct.iContrato * 100 + cse.sServidor,
                  lCerrado: (cct.dHasta !== null && new Date(cct.dHasta) < fSuperior)
                };
                this.contratos.push(tCtt);
                const fHoy = fSuperior;
                let fD = new Date(cct.dDesde);
                if (cct.lCerrado) {
                  fSuperior = new Date(cct.dHasta);
                }
                while (fD <= fSuperior) {
                  const fV = new Date(
                    fHoy.getFullYear() - 1,
                    fD.getMonth(),
                    fD.getDate()
                  );
                  const fH = new Date(
                    fD.getFullYear() + 1,
                    fD.getMonth(),
                    fD.getDate() - 1
                  );
                  const sGen: number =
                    fH <= fHoy
                      ? 30
                      : Math.floor(30 * (fHoy.getFullYear() - fD.getFullYear()) + 2.5 * (fHoy.getMonth() - fD.getMonth()) + Math.floor(fHoy.getDate() - fD.getDate() - 1) / 12);
                  const sTope = this.dateDiff(fHoy, fH) - sGen;
                  const cStatus = sGen < 30 ? "T"
                    : sTope < 366 ? 'D' : 'V';
                  const sSaldo: number =
                    cct.dHasta !== null && new Date(cct.dHasta) <= fHoy
                      ? 0
                      : sGen;
                  this.vacaciones.push({
                    zContrato: cct.iContrato * 100 + cse.sServidor,
                    dDesde: this.datePipe.transform(fD, 'd/M/yy'),
                    dHasta: this.datePipe.transform(fH, 'd/M/yy'),
                    sCiclo: 1 + fD.getFullYear(),
                    cStatus: cStatus,
                    sGeneradas: sGen,
                    sGozadas: 0,
                    sVendidas: 0,
                    sAcordadas: 0,
                    sSaldo: sSaldo,
                    lCerrado: (cct.dHasta !== null && new Date(cct.dHasta) < fSuperior),
                    sTope: sTope
                  });
                  fD = new Date(
                    fD.getFullYear() + 1,
                    fD.getMonth(),
                    fD.getDate()
                  );
                }
                this.iniciando = false;
              });
            });
          });
          if (this.vacaciones.length === 0) {
            this.registros =
              "No hay datos contractuales para calcular vacaciones.";
          }
          this.selContrato = this.contratos[0].zContrato;
          this.chContrato();
        }
      }
      this.registros = "Debe iniciar sesión";
    });
  }

  ngOnDestroy() {
    if (this._novedades) {
      this._novedades.unsubscribe();
      this._sesion.unsubscribe();
    }
  }

  filtroInicioVacaciones = (d: _moment.Moment): boolean => d >= this.solicitud.absDesde;

  enviarSolicitud() {
    const sCiclo = this.vacaciones.filter(sci => sci.cStatus === 'D')[0].sCiclo;
    const parSolicitar = {
      sServidor: this.selContrato % 100,
      iContrato: Math.floor(this.selContrato / 100),
      dDesde: this.solicitud.desde.toDate(),
      dHasta: this.solicitud.hasta.toDate(),
      iDias: this.solicitud.dias,
      sCiclo: sCiclo,
      xAsunto: this.solicitud.xAsunto
    };
    this.data.solicitudVacaciones(this.token, parSolicitar).subscribe(rsp => {
      this.novedades.push(rsp);
      this.totales.lPuedeSolicitar = false;
      this.selCiclo = rsp.sCiclo;
      this.toast.info("Se radicó su solicitud, ahora debe esperar la programación por parte de RRHH.");
    });
  }

  chContrato() {
    const tuplaContrato = { iContrato: Math.floor(this.selContrato / 100), sServidor: this.selContrato % 100 };
    this.novedades = [];
    this._novedades = this.data.vacaciones(this.token, tuplaContrato).subscribe(rsp => {
      this.novedades = rsp;
      this.totales = { sVencidas: 0, sDisponibles: 0, sSaldo: 0, sCicloActual: 0, sTruncas: 0, lPuedeSolicitar: true };
      this.vacaciones.filter(cci => {
        this.novedades.filter(cnov => {
          if (cnov.sCiclo === cci.sCiclo) {
            if (cnov.cTipoNovedad === 'G') {
              cci.sGozadas += cnov.iDias;
              cci.sSaldo -= cci.lCerrado ? 0 : cnov.iDias;
            } else {
              if (cnov.cTipoNovedad === 'V') {
                cci.sVendidas += cnov.iDias;
                cci.sSaldo -= cci.lCerrado ? 0 : cnov.iDias;
              } else {
                if (cnov.cTipoNovedad === 'A') {
                  cci.sAcordadas += cnov.iDias;
                  cci.sSaldo -= cci.lCerrado ? 0 : cnov.iDias;
                } else {
                  if (cnov.cTipoNovedad === 'P' || cnov.cTipoNovedad === 'S') {
                    this.totales.lPuedeSolicitar = false;
                  }
                }
              }
            }
          }
        });
      });
      this.vacaciones.filter(cvac => {
        if (cvac.zContrato === this.selContrato) {
          this.totales.sSaldo += cvac.sSaldo;
          this.totales.sVencidas += cvac.cStatus === "V" ? cvac.sSaldo : 0;
          this.totales.sDisponibles += cvac.cStatus === "D" ? cvac.sSaldo : 0;
          this.totales.sTruncas += cvac.cStatus === 'T' ? cvac.sSaldo : 0;
        }
        if (cvac.cStatus === "D") { this.totales.sCicloActual = cvac.sCiclo; }
      });
    });
  }

  chVacSlider($event) {
    this.solicitud.dias = $event.value;
    this.solicitud.hasta = _moment(this.solicitud.desde).add(this.solicitud.dias - 1, 'days');
  }

  chVacInicio($event) {
    this.solicitud.desde = $event.value;
    this.solicitud.hasta = _moment(this.solicitud.desde).add(this.solicitud.dias - 1, 'days');
  }

  vacxContrato(zContrato): Ciclo[] {
    const salida: Ciclo[] = this.orderBy.transform(
      this.vacaciones.filter(cvac => cvac.zContrato === zContrato),
      "sCiclo",
      true
    );
    return salida;
  }

  novxCiclo(sCiclo): any[] {
    return this.orderBy.transform(this.novedades.filter(cnov => cnov.sCiclo === sCiclo), "dDesde", true);
  }

  private dateDiff(fecha1, fecha2) {
    return Math.floor((fecha1.getTime() - fecha2.getTime()) / (24 * 3600 * 1000));
  }

}
