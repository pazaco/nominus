import { BrowserModule, DomSanitizer } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { NgBusyModule } from 'ng-busy';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { MatSidenavModule, MatIconRegistry, MatIconModule, MatInputModule } from '@angular/material';
import { RecursosModule } from './recursos/recursos.module';
import { Height100Directive } from './recursos/height100.directive';
import { HttpClientModule } from '@angular/common/http';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { ToastrModule } from 'ngx-toastr';
import { HomeComponent } from './home/home.component';
import { MensajesComponent } from './mensajes/mensajes.component';
import { HelpComponent } from './help/help.component';
import { LoginComponent } from './login/login.component';
import { RegistroComponent } from './registro/registro.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IncidenciasComponent } from './incidencias/incidencias.component';
import { EmpleadoComponent } from './empleado/empleado.component';
import { ReglasComponent } from './reglas/reglas.component';
import { PapeletasComponent } from './papeletas/papeletas.component';
import { JefaturaComponent } from './jefatura/jefatura.component';

@NgModule({
  declarations: [
    AppComponent,
    Height100Directive,
    HomeComponent,
    MensajesComponent,
    HelpComponent,
    JefaturaComponent,
    LoginComponent,
    RegistroComponent,
    IncidenciasComponent,
    EmpleadoComponent,
    ReglasComponent,
    PapeletasComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFireMessagingModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    ToastrModule,
    ToastrModule.forRoot({
      positionClass: 'toast-bottom-center',
      preventDuplicates: true,
    }),
    MatSidenavModule, MatIconModule, MatInputModule,
    RecursosModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {   constructor(matIconRegistry: MatIconRegistry, domSanitizer: DomSanitizer) {
  matIconRegistry.addSvgIconSet(domSanitizer.bypassSecurityTrustResourceUrl('./assets/mdi.svg'));
}
}
