import { BehaviorSubject, Observable } from "rxjs";
import { Injectable } from "@angular/core";
import { MathService } from "./math.service";
import { Router } from "@angular/router";

export const MENU = [
  {
    id: "hom", menu: "Tablero de Control", icono: { base: "view-dashboard" }, link: "/home"},
  { id: "inc", menu: "Incidencias", icono: { base: "clock" }, link: "/incidencias" },
  { id: "nti", menu: "Notificaciones", icono: { base: "bell" }, link: "/mensajes" },
  { id: "emp", menu: "Empleado", icono: { base: "account-alert" }, link: "/empleado" },
  { id: "reg", menu: "Reglas", icono: { base: "message-processing" }, link: "/reglas" },
  { id: "pap", menu: "Papeletas", icono: { base: "file-document" }, link: "/papeletas" },
  { id: "jef", menu: "Jefatura", icono: { base: "account-child" }, link: "/jefatura" }
];

@Injectable()
export class NavService {
  private mn = [];
  private perfiles = [];
  private tamanho = "mini";

  private menu: any[] = this._defineMenu();
  private __menu = new BehaviorSubject<any[]>(this.menu);
  private __tamanho = new BehaviorSubject<string>("mini");

  constructor(private math: MathService, private router: Router) {}

  Menu(): Observable<any[]> {
    return this.__menu.asObservable();
  }

  setPerfiles(perfiles) {
    this.perfiles = perfiles;
    this.menu = this._defineMenu();
    this.__menu.next(this.menu);
  }

  Tamanho(): Observable<string> {
    return this.__tamanho.asObservable();
  }

  // defineMenu(perfil: any[]) {
  //   this.menu = this._defineMenu(perfil);
  //   this.__menu.next(this.menu);
  // }

  private _defineMenu(): any[] {
    this.mn = MENU;
    // MENU.filter(opcMenu => {
    //   return true;
    // });
    return this.mn;
  }

  // toggle() {
  //   switch (this.tamanho) {
  //     case 'maxi': this.tamanho = 'nada'; break;
  //     case 'nada': this.tamanho = 'mini'; break;
  //     case 'mini': this.tamanho = 'maxi'; break;
  //   }
  //   this.__tamanho.next(this.tamanho);
  // }

  // swipeLeft() {
  //   if (this.tamanho === 'mini') {
  //     this.tamanho = 'nada';
  //   }
  //   if (this.tamanho === 'maxi') {
  //     this.tamanho = 'mini';
  //   }
  //   this.__tamanho.next(this.tamanho);
  // }

  // swipeRight() {
  //   if (this.tamanho === 'mini') {
  //     this.tamanho = 'maxi';
  //   }
  //   if (this.tamanho === 'nada') {
  //     this.tamanho = 'mini';
  //   }
  //   this.__tamanho.next(this.tamanho);
  // }

  // toggleVisible() {
  //   this.tamanho = this.tamanho !== 'maxi' ? 'maxi' : 'mini';
  //   this.__tamanho.next(this.tamanho);
  // }
}
