import { Injectable } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";
import * as firebase from "firebase";
import { Observable, BehaviorSubject, of } from "rxjs";
import { DataService } from "./data.service";
import { LocalStorage } from "@ngx-pwa/local-storage";
import { environment } from "../../environments/environment";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { NavService } from "./nav.service";
import { MessagingService } from "./messaging.service";
import * as _moment from 'moment';

@Injectable({
  providedIn: "root"
})
export class AuthService {
  user: Observable<firebase.User | null>;
  public hayLog = new BehaviorSubject<boolean>(false);
  public haySes = new BehaviorSubject<any>(null);
  public returnUrl = "/";
  private browserAgent = "";
  private __loading = new BehaviorSubject<boolean>(false);
  public loading = this.__loading.asObservable();
  private __dropContratos = new BehaviorSubject<any>([]);
  public dropContratos = this.__dropContratos.asObservable();

  constructor(
    private afAuth: AngularFireAuth,
    private data: DataService,
    protected localStorage: LocalStorage,
    private router: Router,
    public toast: ToastrService,
    private navi: NavService,
    private mensajes: MessagingService
  ) {
    this.user = afAuth.authState;
  }

  loginGoogle() {
    return this.oAuthLogin(new firebase.auth.GoogleAuthProvider()).catch(err => {
      if (err.code === "auth/account-exists-with-different-credential") {
        this.toast.warning("el correo " + err.email + " está asociado con otro tipo de credencial.  Pruebe con Facebook ó password.");
      }
    });
  }

  loginFacebook() {
    return this.oAuthLogin(new firebase.auth.FacebookAuthProvider()).catch(err => {
      if (err.code === "auth/account-exists-with-different-credential") {
        this.toast.warning("el correo " + err.email + " está asociado con otro tipo de credencial.  Pruebe con Google+ ó password.");
      }
    });
  }

  loginEmail(email: string, pass: string) {
    this.afAuth.auth.signInWithEmailAndPassword(email, pass).catch(err => {
      if (err.code === "auth/user-not-found") {
        this.toast.warning("Correo no registrado, intente como usuario nuevo.");
      }
      if (err.code === "auth/wrong-password") {
        this.toast.error("contraseña incorrecta.");
      }
      if (err.code === "auth/account-exists-with-different-credential") {
        this.toast.warning("el correo " + err.email + " está asociado con otro tipo de credencial.  Pruebe con Google+ ó Facebook.");
      }
    });
  }

  hayUser(): boolean {
    return this.hayLog.getValue();
  }

  haySesion(): any {
    return this.haySes.getValue();
  }

  setBrowser(agente: string) {
    if (agente.match(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini|Mobile|mobile|CriOS/)) {
      this.browserAgent = "mobile";
    } else {
      if (agente.match(/Chrome/)) {
        this.browserAgent = "chrome";
      } else { this.browserAgent = "other"; }
    }
  }

  getBrowser() {
    return this.browserAgent;
  }

  sesion(): Observable<any> {
    if (!this.haySesion()) {
      this.cambiarSesion();
    }
    return this.haySes.asObservable();
  }

  private oAuthLogin(provider) {
    return this.afAuth.auth.signInWithPopup(provider);
  }

  cambiarSesion() {
    this.__loading.next(true);
    this.afAuth.user.subscribe(currUser => {
      if (currUser) {
        let permitido = currUser.emailVerified;
        if (currUser.providerData) {
          permitido = permitido || currUser.providerData[0].providerId === "facebook.com";
        }
        if (permitido) {
          this.hayLog.next(true);
          this.haySes.next(null);
          const usrNotif = currUser.email.replace("@", "|").replace(".", "-");
          this.mensajes.requestPermission(usrNotif);
          this.mensajes.receiveMessage();
          this.data.loginFirebase({ xEmail: currUser.email, provider: currUser.providerData[0].providerId }).subscribe(sesion => {
            if (sesion) {
              const tSes = sesion;
              this.mensajes.contratos = tSes.contratos;
              this.mensajes.tokenSes = tSes.token;
              this.data.asociarPush(tSes.token, this.mensajes.token);
              this.mensajes.inicioNotificar();
              tSes.uid = currUser.uid;
              tSes.iContratos = [];
              tSes.contratos.filter(cse => {
                cse.empresas.filter(cem => {
                  cem.contratos.filter(cct => {
                    tSes.iContratos.push({
                      iContrato: cct.iContrato,
                      sServidor: cse.sServidor,
                      zContrato: cct.iContrato * 100 + cse.sServidor,
                      lVigente: (cct.dHasta === null || _moment(cct.dHasta).isSameOrAfter(_moment())),
                      xLabel: cct.xCargo + ' /' + cem.xAbreviado
                    });
                  });
                });
              });
              this.__dropContratos.next(tSes.iContratos);
              this.navi.setPerfiles(tSes.perfiles);
              this.haySes.next(tSes);
              this.__loading.next(false);
            }
          },
            () => {
              this.__loading.next(false);
            });
        }
      }
    },
      () => {
        this.__loading.next(false);
      });
  }

  logout() {
    this.afAuth.auth.signOut().then(() => {
      this.router.navigate(["/login"]);
      this.hayLog.next(false);
      this.haySes.next(false);
      this.navi.setPerfiles([]);
    });
  }

  async solicitarEmail(email) {
    try {
      await this.afAuth.auth.sendSignInLinkToEmail(
        email,
        environment.actionCodeSettings
      );
      this.toast.info("Se ha enviado un vínculo al correo " + email + " para que continúe el proceso de registro.", null, { closeButton: true, tapToDismiss: false, timeOut: 12000 });
      this.localStorage.setItem("emailxVerificar", email).subscribe(() => { });
    } catch (err) {
      console.log("nvoSSO", err);
    }
  }

  // private v20(): string {
  //   let cv20 = "";
  //   let ii: number;
  //   const ccrs = "123456789aAbBcCdDeEfFgHhijJKkLmMnNoPpqQrRsStTuUwWxXyYzZ";
  //   for (ii = 0; ii < 20; ii++) {
  //     const r = Math.floor(Math.random() * ccrs.length);
  //     cv20 += ccrs[r];
  //   }
  //   return cv20;
  // }
}
