import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { NavService } from "./nav.service";
import { LoginService } from "./login.service";
import { MathService } from "./math.service";
import { TipologiaService } from "./tipologia.service";
import { MessagingService } from "./messaging.service";
import { DataService } from "./data.service";
import { AuthService } from "./auth.service";
import { AuthGuard } from "./auth.guard";
import { OrderByPipe } from "./pipes.pipe";
import { MayusculaDirective, NumericoDirective } from "./teclado.directive";
import { EqualValidatorDirective } from "./equal-validator.directive";
import { PrestamosService } from "./prestamos.service";

@NgModule({
  imports: [CommonModule],
  declarations: [OrderByPipe, MayusculaDirective, NumericoDirective, EqualValidatorDirective],
  providers: [
    NavService,
    LoginService,
    MathService,
    MessagingService,
    TipologiaService,
    DataService,
    PrestamosService,
    AuthService,
    AuthGuard
  ]
})
export class RecursosModule {}
