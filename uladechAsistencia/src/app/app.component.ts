import { Component, ViewChild, OnInit } from '@angular/core';
import { ToastrService, ToastContainerDirective } from 'ngx-toastr';
import { NavService } from './recursos/nav.service';
import { AuthService } from './recursos/auth.service';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  @ViewChild(ToastContainerDirective) toastContainer: ToastContainerDirective;
  siMenu: any[];
  appVersion: string;

  constructor(public navi: NavService,
    public authService: AuthService,
    private router: Router,
    private http: HttpClient,
    public toast: ToastrService ) {
      http.get("../../package.json").subscribe(rsp => this.appVersion = rsp["version"]);
    }

  ngOnInit() {
    this.navi.Menu().subscribe(rsp => { this.siMenu = rsp; });
    this.toast.overlayContainer = this.toastContainer;
    this.authService.setBrowser(navigator.userAgent);
  }
  aLog() {
    this.router.navigate(['/login']);
  }

}
