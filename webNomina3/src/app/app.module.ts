import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { LoadingModule, ANIMATION_TYPES } from 'ngx-loading';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
// import { AngularFirestoreModule } from 'angularfire2/firestore';
// import { AngularFireAuthModule } from 'angularfire2/auth';
import { HttpClientModule } from '@angular/common/http';
import { SnotifyModule, SnotifyService, ToastDefaults } from 'ng-snotify';
import { QRCodeModule } from 'angular2-qrcode';

import { AppRoutingModule } from './app-routing.module';
import { RecursosModule } from './recursos/recursos.module';
import { Height100Directive } from './recursos/height100.directive';
import { MayusculaDirective, NumericoDirective } from './recursos/teclado.directive';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { BoletasComponent } from './boletas/boletas.component';
import { AsistenciaComponent } from './asistencia/asistencia.component';
import { VacacionesComponent } from './vacaciones/vacaciones.component';
import { LoginComponent } from './login/login.component';
import { UsuarioComponent } from './usuario/usuario.component';
import { EqualValidatorDirective } from './recursos/equal-validator.directive';
import { RegistroComponent } from './registro/registro.component';
import { PrestamosComponent } from './prestamos/prestamos.component';
import { OrderByPipe } from './recursos/pipes.pipe';

@NgModule({
  declarations: [
    AppComponent,
    AsistenciaComponent,
    BoletasComponent,
    HomeComponent,
    VacacionesComponent,
    Height100Directive,
    MayusculaDirective,
    EqualValidatorDirective,
    NumericoDirective,
    OrderByPipe,
    HomeComponent,
    BoletasComponent,
    AsistenciaComponent,
    VacacionesComponent,
    LoginComponent,
    UsuarioComponent,
    EqualValidatorDirective,
    RegistroComponent,
    PrestamosComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    LoadingModule.forRoot({
      animationType: ANIMATION_TYPES.chasingDots,
      backdropBackgroundColour: 'rgba(0,0,0,0.1)',
      backdropBorderRadius: '5px',
      primaryColour: '#3b748f',
      secondaryColour: '#75cc33',
      tertiaryColour: '#004455',
    }),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    HttpClientModule,
    SnotifyModule,
    QRCodeModule,
    RecursosModule,
    AppRoutingModule,
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [
    { provide: 'SnotifyToastConfig', useValue: ToastDefaults },
    SnotifyService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
