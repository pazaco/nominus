import {
  HostListener,
  Directive,
  ElementRef,
  Input,
  AfterViewInit
} from "@angular/core";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: "[height-100]"
})
export class Height100Directive implements AfterViewInit {
  @Input()
  footerElement = null;
  constructor(private el: ElementRef) {}

  ngAfterViewInit(): void {
    this.calculateAndSetElementHeight();
  }

  @HostListener("window:resize", ["$event"])
  onresize() {
    this.calculateAndSetElementHeight();
  }

  private calculateAndSetElementHeight() {
    this.el.nativeElement.style.overflow = "auto";
    const windowHeight = window.innerHeight;
    const elementOffsetTop = this.getElementOffsetTop();
    const elementMarginBottom = this.el.nativeElement.style.marginBottom;
    const footerElementMargin = this.getfooterElementMargin();

    this.el.nativeElement.style.height =
      windowHeight - footerElementMargin - elementOffsetTop + "px";
  }

  private getElementOffsetTop() {
    return this.el.nativeElement.getBoundingClientRect().top;
  }

  private getfooterElementMargin() {
    /*
                if (!this.footerElement) { return 0; }
                const footerStyle = window.getComputedStyle(this.footerElement);
                return parseInt(footerStyle.height, 10);
            }
        */
    return 0;
  }
}
