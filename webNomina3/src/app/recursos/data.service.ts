import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable, BehaviorSubject, of } from "rxjs";
import * as moment from "moment";

@Injectable()
export class DataService {
  private api = "https://api.remunerarte.com/";

  constructor(private http: HttpClient) {}

  boletasIndice(token): Observable<any> {
    return this.http.get(this.api + "bel/indice", this.optTkn(token));
  }

  public printBoleta(token, oCalculo): Observable<any> {
    return this.http.post(
      this.api + "boleta/data",
      oCalculo,
      this.optTkn(token)
    );
  }

  loginFirebase(parLogin: any): Observable<any> {
    return this.http.post(this.api + "auth/firebase", parLogin, this.optSeed());
  }

  private optSeed(): any {
    return {
      headers: new HttpHeaders()
        .set("content-type", "application/json;charset=utf-8")
        .set("User-Seed", this.minutero())
    };
  }

  private optTkn(token, contrato = null): any {
    let sHeaders = new HttpHeaders().set(
      "content-type",
      "application/json;charset=utf-8"
    );
    sHeaders = sHeaders.set("sesionPersona", token);
    if (contrato) {
      sHeaders = sHeaders.set("contrato", JSON.stringify(contrato));
    }
    return { headers: sHeaders };
  }

  public minutero(): string {
    let hashPassword = "";
    const passwd = moment().format("mmDDMM[Y]YY[T]mmHH");
    let hashSemilla = "app.nominus.pe$$" + passwd;
    const lpo =
      "ab9cK4fstv5^*dCDSxy7zAB#$%0&=TUV6OPZ1emnE2gQRWXYhjklLMNpqrFG3H8I@)J";
    let nLen = 0;
    let nSeed: number;
    let nPass: number;
    let pos = 1;
    let hPwd = "";
    let pChar: number;
    while (hashSemilla.length < 128) {
      hashSemilla = hashSemilla + hashSemilla;
    }
    while (hPwd.length < 128) {
      hPwd = hPwd + passwd;
    }
    while (pos <= passwd.length) {
      nLen += passwd.substr(pos - 1, 1).charCodeAt(0);
      pos++;
    }
    pos = 1;
    while (pos <= 128) {
      nSeed = lpo.indexOf(hashSemilla.substr(pos - 1, 1)) + 1;
      nPass = hPwd.substr(pos - 1, 1).charCodeAt(0);
      pChar = 1 + ((2 * nSeed + 5 * nPass + 3 * nLen) % 67);
      hashPassword += lpo.substr(pChar - 1, 1);
      pos++;
    }
    return hashPassword;
  }
}
