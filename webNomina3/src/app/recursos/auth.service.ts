import { Injectable } from "@angular/core";
import { AngularFireAuth } from "angularfire2/auth";
import * as firebase from "firebase";
import { Observable, BehaviorSubject, of } from "rxjs";
import { DataService } from "./data.service";
import { LocalStorage } from "@ngx-pwa/local-storage";
import { environment } from "../../environments/environment";
import { Router } from "@angular/router";
import { SnotifyService } from "ng-snotify";

@Injectable({
  providedIn: "root"
})
export class AuthService {
  user: Observable<firebase.User | null>;
  public hayLog = new BehaviorSubject<boolean>(false);
  private haySes = new BehaviorSubject<any>(null);
  public returnUrl = "/";

  constructor(
    private afAuth: AngularFireAuth,
    private data: DataService,
    protected localStorage: LocalStorage,
    private router: Router,
    private snotify: SnotifyService
  ) {
    this.user = afAuth.authState;
  }

  loginGoogle() {
    return this.oAuthLogin(new firebase.auth.GoogleAuthProvider());
  }

  loginFacebook() {
    return this.oAuthLogin(new firebase.auth.FacebookAuthProvider());
  }

  hayUser(): boolean {
    return this.hayLog.getValue();
  }

  haySesion(): any {
    return this.haySes.getValue();
  }

  sesion(): Observable<any> {
    if (!this.haySesion()) {
      this.cambiarSesion();
    }
    return this.haySes.asObservable();
  }

  private oAuthLogin(provider) {
    return this.afAuth.auth.signInWithPopup(provider);
  }

  loginEmail(email: string, pass: string) {
    this.afAuth.auth.signInWithEmailAndPassword(email, pass).catch(err => {
      if (err.code === "auth/wrong-password") {
        this.snotify.warning("Usuario o contraseña incorrecta.");
      } else {
        this.snotify.error(err.code);
      }
    });
  }

  cambiarSesion() {
    this.afAuth.user.subscribe(currUser => {
      if (currUser) {
        if (currUser.emailVerified) {
          this.hayLog.next(true);
          this.haySes.next(null);
          this.data
            .loginFirebase({
              xEmail: currUser.email,
              provider: currUser.providerData[0].providerId
            })
            .subscribe(sesion => {
              if (sesion) {
                const tSes = sesion;
                tSes.uid = currUser.uid;
                tSes.iContratos = [];
                tSes.contratos.filter(cse => {
                  cse.empresas.filter(cem => {
                    cem.contratos.filter(cct => {
                      tSes.iContratos.push({
                        iContrato: cct.iContrato,
                        sServidor: cse.sServidor
                      });
                    });
                  });
                });
                this.haySes.next(tSes);
              }
            });
        }
      }
    });
  }

  logout() {
    this.afAuth.auth.signOut().then(() => {
      this.router.navigate(["/login"]);
      this.hayLog.next(false);
      this.haySes.next(false);
    });
  }

  async solicitarEmail(email) {
    try {
      await this.afAuth.auth.sendSignInLinkToEmail(
        email,
        environment.actionCodeSettings
      );
      this.localStorage.setItem("emailxVerificar", email).subscribe(() => {});
    } catch (err) {
      console.log("no SSO | noLocalStorage", err);
    }
  }

  private v20(): string {
    let cv20 = "";
    let ii: number;
    const ccrs = "123456789aAbBcCdDeEfFgHhijJKkLmMnNoPpqQrRsStTuUwWxXyYzZ";
    for (ii = 0; ii < 20; ii++) {
      const r = Math.floor(Math.random() * ccrs.length);
      cv20 += ccrs[r];
    }
    return cv20;
  }
}
