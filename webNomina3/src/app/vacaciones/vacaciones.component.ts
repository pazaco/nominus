import { Component, OnInit } from "@angular/core";
import { DataService } from "../recursos/data.service";
import { AuthService } from "../recursos/auth.service";
import { Subscription } from "rxjs";
import { OrderByPipe } from "../recursos/pipes.pipe";

@Component({
  selector: "app-vacaciones",
  templateUrl: "./vacaciones.component.html",
  styleUrls: ["./vacaciones.component.css"]
})
export class VacacionesComponent implements OnInit {
  vacaciones: any[] = [];
  novedades: any[];
  token;
  contratos: any[] = [];
  selContrato: number;
  selCiclo: number;
  private _sesion: Subscription;
  cBusy = false;
  dBusy = false;
  orderBy = new OrderByPipe();

  constructor(
    public authService: AuthService,
    public data: DataService
  ) {}

  ngOnInit() {
    this.cBusy = true;
    this._sesion = this.authService.sesion().subscribe(ses => {
      if (ses) {
        if (ses.contratos.length > 0) {
          ses.contratos.filter(cse => {
            cse.empresas.filter(cem => {
              cem.contratos.filter(cct => {
                const tCtt = {
                  iContrato: cct.iContrato,
                  xContrato: cct.xCargo + " /" + cem.xAbreviado,
                  sServidor: cse.sServidor,
                  zContrato: cct.iContrato * 100 + cse.sServidor
                };
                this.contratos.push(tCtt);
                let fSuperior = new Date();
                fSuperior.setHours(0);
                fSuperior.setMinutes(0);
                fSuperior.setSeconds(0);
                fSuperior.setMilliseconds(0);
                const fHoy = fSuperior;
                let fD = new Date(cct.dDesde);
                if (cct.dHasta != null && new Date(cct.dHasta) < fSuperior) {
                  fSuperior = new Date(cct.dHasta);
                }
                while (fD <= fSuperior) {
                  const fV = new Date(
                    fHoy.getFullYear() - 1,
                    fD.getMonth(),
                    fD.getDate()
                  );
                  const fH = new Date(
                    fD.getFullYear() + 1,
                    fD.getMonth(),
                    fD.getDate() - 1
                  );
                  const sGen: number =
                    fH <= fHoy
                      ? 30
                      : 12 * (fHoy.getFullYear() - fD.getFullYear()) +
                        fHoy.getMonth() -
                        fD.getMonth() +
                        Math.floor((fHoy.getDate() - fD.getDate() - 1) / 30);
                  const cStatus = sGen < 30 ? "T" : fH <= fV ? "V" : "D";
                  const sSaldo: number =
                    cct.dHasta != null && new Date(cct.dHasta) <= fHoy
                      ? 0
                      : sGen;
                  this.vacaciones.push({
                    zContrato: cct.iContrato * 100 + cse.sServidor,
                    dDesde: fD,
                    dHasta: fH,
                    sCiclo: 1 + fD.getFullYear(),
                    cStatus: cStatus,
                    sGeneradas: sGen,
                    sGozadas: 0,
                    sVendidas: 0,
                    sAcordadas: 0,
                    sSaldo: sSaldo
                  });
                  fD = new Date(
                    fD.getFullYear() + 1,
                    fD.getMonth(),
                    fD.getDate()
                  );
                }
                // TODO: Implementar lectura de novedades (this.data)
                // TODO: recalcular tabla de vacaciones basado en la lectura de novedades
                // TODO: revaluar saldos por ciclos.
                this.novedades = [];
              });
            });
          });
          this.selContrato = this.contratos[0].zContrato;
          this.token = ses.token;
          this.cBusy = false;
        }
      }
    });
  }

  chContrato() {}

  totalDias(zContrato): any {
    let sVencidas = 0;
    let sDisponibles = 0;
    this.vacaciones.filter(cvac => {
      if (cvac.zContrato === zContrato) {
        sVencidas += cvac.cStatus === "V" ? cvac.sSaldo : 0;
        sDisponibles += cvac.cStatus === "D" ? cvac.sSaldo : 0;
      }
    });
    return { sVencidas: sVencidas, sDisponibles: sDisponibles };
  }

  vacxContrato(zContrato): any[] {
    //    return this.vacaciones.filter(cvac => cvac.zContrato == zContrato);
    return this.orderBy.transform(
      this.vacaciones.filter(cvac => cvac.zContrato === zContrato),
      "sCiclo",
      true
    );
  }
}
