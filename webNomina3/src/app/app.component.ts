import { Component, OnInit } from '@angular/core';
import { NavService } from './recursos/nav.service';
import { aniMenuMax, aniOpacoMax } from './recursos/animar';
import { SnotifyService } from 'ng-snotify';
import { AuthService } from './recursos/auth.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [aniMenuMax, aniOpacoMax]
})
export class AppComponent implements OnInit {
  tamanho = "mini";
  siMenu: any[];

  constructor(public navi: NavService, public snotify: SnotifyService, public authService: AuthService, private router: Router) { }

  ngOnInit() {
    this.siMenu = this.navi.mn;
  }

  toggleMenu() {
    if (this.tamanho === 'mini') {
      this.tamanho = 'maxi';
    } else {
      this.tamanho = 'mini';
    }
  }

  aLog() {
    this.router.navigate(['/login']);
  }
}
