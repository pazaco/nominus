import { Component, OnInit } from "@angular/core";
import { AuthService } from "../recursos/auth.service";
import {
  FormGroup,
  FormControl,
  Validators,
  FormBuilder
} from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { LocalStorage } from "@ngx-pwa/local-storage";
import { AngularFireAuth } from "angularfire2/auth";
import { tap } from "rxjs/operators";

@Component({
  selector: "app-registro",
  templateUrl: "./registro.component.html"
})
export class RegistroComponent implements OnInit {
  valMail = false;
  valForm: FormGroup;
  passGroup: FormGroup;
  public verifStatus: any = { mode: "signNo" };

  constructor(
    public authService: AuthService,
    private route: ActivatedRoute,
    private router: Router,
    private localStorage: LocalStorage,
    private afAuth: AngularFireAuth
  ) {
    const regxEmail = /^[\w!#$%&'*+\-/=?\^_`{|}~]+(\.[\w!#$%&'*+\-/=?\^_`{|}~]+)*@((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))$/;
    const regxPass1 = /^(?=\w*\d)(?=\w*[aA-zZ])\S{6,}$/;
    const regxPass2 = /^(?=\w*\d)(?=\w*[aA-zZ])(?=.*[!@#\$%\^&\*])\S{6,}$/;
    const regxPass3 = /^(?=\w*\d)(?=\w*[a-z])(?=\w*[A-Z])(?=.*[!@#\$%\^&\*])\S{6,}$/;
    route.queryParamMap.subscribe(
      params => {
        if (params.get("mode")) {
          this.verifStatus.oobCode = params.get("oobCode");
          this.verifStatus.mode = params.get("mode");
        }
        if (this.verifStatus.mode === "signIn") {
          this.valForm = new FormGroup({
            password: new FormControl("", [
              Validators.minLength(6),
              Validators.required
            ]),
            verificar: new FormControl("", [
              Validators.required,
              validateEquals
            ]),
            displayName: new FormControl("", [
              Validators.required,
              Validators.minLength(5),
              Validators.maxLength(60)
            ])
          });
          localStorage.getItem("emailxVerificar").subscribe(valor => {
            if (valor != null) {
              if (this.afAuth.auth.isSignInWithEmailLink(this.router.url)) {
                this.verifStatus.mode = "signIn2";
                this.valForm.addControl(
                  "email",
                  new FormControl({ value: valor, disabled: true }, [
                    Validators.required,
                    Validators.pattern(regxEmail)
                  ])
                );
              } else {
                this.verifStatus.mode = "signNo";
              }
            } else {
              this.verifStatus.mode = "signLess";
              this.valForm.addControl(
                "email",
                new FormControl("", [
                  Validators.required,
                  Validators.pattern(regxEmail)
                ])
              );
            }
          });
        }
      },
      errParams => {
        console.log("errParams", errParams);
      }
    );
  }

  ngOnInit() {}

  onCompletarValidacion() {
    const email = this.valForm.controls.email.value;
    const password = this.valForm.controls.password.value;
    const displayName = this.valForm.controls.displayName.value;
    try {
      if (this.afAuth.auth.isSignInWithEmailLink(this.router.url)) {
        this.afAuth.auth
          .signInWithEmailLink(email, this.router.url)
          .then(rSso => {
            this.afAuth.auth.currentUser.updatePassword(password);
            this.afAuth.auth.currentUser.updateProfile({
              displayName: displayName,
              photoURL: null
            });
            this.localStorage.removeItem("emailxVerificar").subscribe(() => {});
            // TODO: this.authService.completarSesion(rSso);
            this.router.navigate(["/home"]);
          })
          .catch(eSso => {
            this.verifStatus.mode = "SignNo";
          });
      }
    } catch (err) {
      console.log("error Completando validación", err);
    }
  }
}

function validateEquals(c: FormControl) {
  if (c.parent) {
    const v1 = c.parent.controls["password"].value;
    const v2 = c.parent.controls["verificar"].value;
    if (v1 !== v2) {
      return { validateEquals: { valid: false } };
    }
  }
  return null;
}
