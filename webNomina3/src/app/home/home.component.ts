import { Component, OnInit, HostBinding, OnDestroy } from "@angular/core";
import { AuthService } from "../recursos/auth.service";
import { Subscription } from "rxjs";
import { DataService } from "../recursos/data.service";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"]
})
export class HomeComponent implements OnInit, OnDestroy {
  // private sesion_: Subscription;
  private notifica: any[];
  private _sesion: Subscription;
  public sesion;

  constructor(public authService: AuthService, public data: DataService) {}

  ngOnInit() {
    this._sesion = this.authService.sesion().subscribe(rsp => {
      this.sesion = rsp;
    });
  }

  ngOnDestroy() {
    this._sesion.unsubscribe();
  }
}
