import { Component, ViewChild, ElementRef, OnInit, Renderer2 } from '@angular/core';
import { NgElement, WithProperties } from '@angular/elements';
import { MenuComponent } from './menu/menu.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  @ViewChild('fullDisplay', { static: true }) eFullDisplay: ElementRef;
  @ViewChild('menu', { static: true }) eMenu: ElementRef;
  private menuMain;


  constructor(private rend: Renderer2) { }

  ngOnInit() {
    this.menuMain = document.createElement('menu-main') as NgElement & WithProperties<MenuComponent>;
    this.menuMain.addEventListener('elegido', (event: any) => {
      console.log(event);
    });
    this.menuMain.addEventListener('closed', () => {
      this.rend.removeChild(this.eMenu.nativeElement, this.menuMain);
    });
    this.rend.appendChild(this.eMenu.nativeElement, this.menuMain);
  }

  openMain() {
    console.log(this.menuMain);
    this.menuMain.cState = this.menuMain.cState === 'opened' ? 'closed' : 'opened';
  }

}
