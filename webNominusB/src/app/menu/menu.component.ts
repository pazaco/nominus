import { Component, Input, Output, ViewChild, ElementRef, HostListener, ViewEncapsulation } from '@angular/core';
import { trigger, style, state, transition, animate } from '@angular/animations';
import { Router } from '@angular/router';

export const MENU = [
  {
    iMenu: 0,
    xMenu: 'Inicio',
    xIcono: 'home',
    xLink: '/home',
    iPadre: 0
  },
  {
    iMenu: 1,
    xMenu: 'Asistencia',
    xIcono: 'mn-asistencia',
    xLink: '/asistencia',
    iPadre: 0
  },
  {
    iMenu: 2,
    xMenu: 'Marcación',
    xIcono: 'mn-asi-marcar',
    xLink: '/asiMarcacion',
    iPadre: 1
  },
  {
    iMenu: 3,
    xMenu: 'Incidencias',
    xIcono: 'mn-asi-incidencias',
    xLink: '/asiIncidencias',
    iPadre: 1
  },
  {
    iMenu: 4,
    xMenu: 'Reclutamiento',
    xIcono: 'mn-asi-reclutar',
    xLink: '/asiReclutamiento',
    iPadre: 1
  }
];

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  encapsulation: ViewEncapsulation.ShadowDom,
  host: { '[@state]': 'state' },
  animations: [
    trigger('state', [
      state('opened', style({ width: 230, opacity: 1, zIndex: 20 })),
      state('closed', style({ width: 0, zIndex: -50, opacity: 0 })),
      transition('* => *', animate('300ms ease-in'))
    ])
  ]
})
export class MenuComponent {
  @Input()
  set cState(State: string) {
    this.state = State;
    this.lFocus = false;
  }
  get cState(): string {
    return this.state;
  }
  @ViewChild('interiorMenu', { static: true }) eInteriorMenu: ElementRef;
  public state = 'closed';
  private lFocus = false;
  public menu: any[];
  public selMenu: number[] = [0];

  @HostListener('document:click', ['$event.target'])
  clickOut(event) {
    if (this.state === 'opened') {
      if (this.lFocus && !this.eInteriorMenu.nativeElement.contains(event)) {
        this.state = 'closed';
      }
      this.lFocus = true;
    }
  }

  constructor(private router: Router) {
  }


  selected = (opc: any): boolean => {
    let rsp: boolean;
    if (this.selMenu && this.selMenu.length > 0) {
      rsp = this.selMenu.indexOf(opc) > -1;
    } else {
      rsp = true;
    }
    return rsp;
  }

  subMenu = (iMenu?: number) => MENU.filter(item =>
    (!iMenu && (item.iPadre === null || item.iPadre === 0))
    || (iMenu && item.iPadre === iMenu))

  selOpcion(valor) {
    this.state = 'closed';
    console.log(valor.xLink);
    this.router.navigateByUrl(valor.xLink);
  }

}
