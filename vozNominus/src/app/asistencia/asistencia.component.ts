import { AuthService } from "./../servicios/auth.service";
import { DataService } from "./../servicios/data.service";
import { Component, OnInit, OnDestroy } from "@angular/core";
import * as RecordRTC from "recordrtc";
import { DomSanitizer } from "@angular/platform-browser";
import { HttpEventType, HttpResponse } from "@angular/common/http";
import { ToastrService } from "ngx-toastr";
import { Subscription } from "rxjs";
import { debounceTime } from "rxjs/operators";

@Component({
  selector: "app-asistencia",
  templateUrl: "./asistencia.component.html",
  styleUrls: ["./asistencia.component.scss"]
})
export class AsistenciaComponent implements OnInit, OnDestroy {
  private record;
  private selContrato;
  public recording = false;
  public url;
  public error;
  public sesion;
  public vozActiva;
  public pristine = true;
  public _vozActiva: Subscription;

  constructor(
    private domSanitizer: DomSanitizer,
    private data: DataService,
    public auth: AuthService,
    public toast: ToastrService
  ) {}

  ngOnInit() {
    this.auth.Sesion().subscribe(rSes => {
      if (rSes) {
        this.sesion = rSes;
        // TODO : opcion de elegir contrato, por ahora se elige ULADECH (8)
        this.selContrato = this.sesion.contratos.filter(
          cco => cco.sServidor === 8
        )[0];
        this._vozActiva = this.data
          .vozAbierta(this.sesion.token, {
            iPersona: this.sesion.uchempleado.iZkEmpleado,
            sServidor: this.selContrato.sServidor
          })
          .pipe(debounceTime(1000))
          .subscribe(rvz => {
            if (rvz["bFrase"]) {
              this.vozActiva = rvz;
              this._vozActiva.unsubscribe();
            } else {
              this.vozActiva = null;
            }
          });
      }
    });
  }

  ngOnDestroy() {
    if (!this._vozActiva.closed) {
      this._vozActiva.unsubscribe();
    }
  }

  sanitize(url: string) {
    return this.domSanitizer.bypassSecurityTrustUrl(url);
  }

  initiateRecording() {
    this.recording = true;
    const mediaConstraints = {
      video: false,
      audio: true
    };
    navigator.mediaDevices
      .getUserMedia(mediaConstraints)
      .then(this.successCallback.bind(this), this.errorCallback.bind(this));
  }

  successCallback(stream) {
    const options = {
      mimeType: "audio/wav",
      numberOfAudioChannels: 1
    };
    this.pristine = false;
    const StereoAudioRecorder = RecordRTC.StereoAudioRecorder;
    this.record = new StereoAudioRecorder(stream, options);
    this.record.record();
  }

  stopRecording() {
    this.recording = false;
    this.record.stop(this.processRecording.bind(this));
  }

  processRecording(_blob) {
    _blob.filename = "waves";
    _blob.lastModifiedDate = new Date();
    const voz = {
      idTrack: { iTrack: this.vozActiva.iTrack, sServidor: 8 },
      data: _blob
    };
    this.vozActiva = null;
    this.data.subirVoz(voz).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {
        const porc = Math.round((100 * event.loaded) / event.total);
      } else if (event instanceof HttpResponse) {
        this.toast.success(
          "grabación recibida. En espera de la autorización por parte del personal de RRHH."
        );
      }
    });
  }

  errorCallback(error) {
    this.error = "Can not play audio in your browser";
  }
}
