import { MathService } from './math.service';
import { MessagingService } from "./messaging.service";
import { NavService } from "./nav.service";
import { DataService } from "./data.service";
import { AuthService } from "./auth.service";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

@NgModule({
  declarations: [],
  imports: [CommonModule],
  providers: [AuthService, DataService, NavService, MessagingService, MathService ]
})
export class ServiciosModule {}
