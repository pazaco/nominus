import { BehaviorSubject, Observable } from "rxjs";
import { Injectable } from "@angular/core";
import { MathService } from "./math.service";

export const MENU = [
  {
    id: "lab",
    menu: "Laboratorio",
    icono: { base: "flask-outline" },
    link: "/laboratorio",
    acceso: [20]
  },
  {
    id: "nti",
    menu: "Notificaciones",
    icono: { base: "bell" },
    link: "/home",
    acceso: [-2]
  },
  {
    id: "bol",
    menu: "Boletas",
    icono: { base: "cash" },
    link: "/boletas",
    acceso: [1, 99]
  },
  {
    id: "asi",
    menu: "Asistencia",
    icono: { base: "clock" },
    link: "/asistencia",
    acceso: [2, 99]
  },
  {
    id: "vac",
    menu: "Vacaciones",
    icono: { base: "beach" },
    link: "/vacaciones",
    acceso: [3, 99]
  },
  {
    id: "prs",
    menu: "Préstamos",
    icono: { base: "cash-usd" },
    link: "/prestamos",
    acceso: [4, 99]
  },
  {
    id: "usr",
    menu: "Contratos",
    icono: { base: "account-box-multiple" },
    link: "/usuario",
    acceso: [99]
  },
  {
    id: "jfe",
    menu: "Jefatura",
    icono: { base: "account-supervisor" },
    link: "/jefatura",
    acceso: [5, 99]
  },
  {
    id: "hlp",
    menu: "Ayuda",
    icono: { base: "help-box" },
    link: "/ayuda",
    acceso: [55]
  }
];

@Injectable()
export class NavService {
  private _perfiles;
  private __menu = new BehaviorSubject<any[]>(this.calcMenu());
  constructor(private math: MathService) {}

  Menu(): Observable<any[]> {
    return this.__menu.asObservable();
  }

  calcMenu(): any[] {
    const _mnu = MENU.filter(
      cmn =>
        this._perfiles &&
        cmn.acceso &&
        (cmn.acceso.indexOf(-1) >= 0 ||
          (cmn.acceso.indexOf(-2) >= 0 && this._perfiles.length > 0) ||
          this.math.Interseccion(this._perfiles, cmn.acceso).length > 0)
    );
    return _mnu;
  }

  Perfiles(perfiles?) {
    if (perfiles) {
      this._perfiles = perfiles;
    }
    this.__menu.next(this.calcMenu());
  }
}
