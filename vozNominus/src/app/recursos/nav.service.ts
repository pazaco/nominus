import { BehaviorSubject, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { MathService } from './math.service';
import { Router } from '@angular/router';
import { AuthService } from './auth.service';

export const MENU = [
  { id: 'asi', menu: 'Asistencia', icono: { base: 'clock' }, link: '/asistencia', submenu:[
    {id:'asi1', menu:'Incidencias', icono: {base:''},link:'/incidencias'},
    {id:'asi2', menu:'Empleado', icono: {base:''},link:'/inciempleado'},
    {id:'asi3', menu:'', icono: {base:''},link:'/incidencias'},
    {id:'asi4', menu:'Incidencias', icono: {base:''},link:'/incidencias'},
    {id:'asi5', menu:'Incidencias', icono: {base:''},link:'/incidencias'},
  ]},
  { id: 'vac', menu: 'Vacaciones', icono: { base: 'beach' }, link: '/vacaciones' },
  { id: 'prs', menu: 'Préstamos', icono: { base: 'cash-usd' }, link: '/prestamos' },
  { id: 'hom', menu: 'Notificaciones', icono: { base: 'bell' }, link: '/home' },
  { id: 'usr', menu: 'Contratos', icono: { base: 'account-box-multiple' }, link: '/usuario' },
  { id: 'jfe', menu: 'Jefatura', icono: { base: 'account-supervisor' }, link: '/jefatura' },
  // {id: 'hlp', menu: 'Ayuda', icono: {base: 'help-box' }, link: '/ayuda' }
];

@Injectable()
export class NavService {
  private mn = [];
  private perfiles = [];
  private tamanho = { layout: "", orientation: "", width: 0, height: 0 };
  private menu: any[] = this._defineMenu();
  private __menu = new BehaviorSubject<any[]>(this.menu);
  private __tamanho = new BehaviorSubject<any>(this.tamanho);

  constructor(private math: MathService, private router: Router) { }

  Menu(): Observable<any[]> {
    return this.__menu.asObservable();
  }

  setPerfiles(perfiles) {
    this.perfiles = perfiles;
    this.menu = this._defineMenu();
    this.__menu.next(this.menu);
  }

  getTamanho(): Observable<string> {
    return this.__tamanho.asObservable();
  }

  setTamanho(ancho, alto) {
    this.tamanho.orientation = ancho >= alto ? "horizontal" : "vertical";
    this.tamanho.layout =
      ancho > 1200 ? "xl" :
        ancho > 992 ? "lg" :
          ancho > 768 ? "md" :
            ancho > 576 ? "sm" : "xs";
    this.tamanho.width = ancho;
    this.tamanho.height = alto;
    this.__tamanho.next(this.tamanho);
  }

  crumb() {
    const routeActivo = this.router.routerState.snapshot.url;
    const oRsp = { menu: null, icono: null };
    const menuAct = this.mn.filter(cmn => cmn.link === routeActivo)[0];
    return { menu: menuAct.menu, icono: menuAct.icono };
  }


  private _defineMenu(): any[] {
    this.mn = [];
    MENU.filter(opcMenu => {
      if (opcMenu.link === "/boletas" || opcMenu.link === "/usuario" || opcMenu.link === "/vacaciones") {
        if (this.perfiles.indexOf(1) === -1) {
          return false;
        } else {
          this.mn.push(opcMenu);
        }
      } else {
        if (opcMenu.link === "/asistencia") {
          if (this.perfiles.indexOf(3) === -1) {
            return false;
          } else {
            this.mn.push(opcMenu);
          }
        } else {
          if (opcMenu.link === "/prestamos") {
            if (this.perfiles.indexOf(4) === -1) {
              return false;
            } else {
              this.mn.push(opcMenu);
            }
          } else {
            if (opcMenu.link === "/jefatura") {
              if (this.perfiles.indexOf(5) === -1) {
                return false;
              } else {
                this.mn.push(opcMenu);
              }
            } else {
              this.mn.push(opcMenu);
            }
          }
        }
      }
    });
    return this.mn;
  }

}

