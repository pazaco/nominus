import { LoginComponent } from "./login/login.component";
import { AngularFireMessagingModule } from "@angular/fire/messaging";
import { AngularFireAuthModule } from "@angular/fire/auth";
import { BrowserModule, DomSanitizer } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { ServiceWorkerModule } from "@angular/service-worker";
import { environment } from "../environments/environment";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import {
  MAT_DATE_LOCALE,
  MatButtonModule,
  MatIconRegistry,
  MatToolbarModule,
  MatIconModule,
  MatSidenavModule,
  MatFormFieldModule,
  MatTabsModule,
  MatInputModule
} from "@angular/material";
import { FlexLayoutModule } from "@angular/flex-layout";
import { HomeComponent } from "./home/home.component";
import { HttpClientModule } from "@angular/common/http";
import { ServiciosModule } from "./servicios/servicios.module";
import { ToastrModule } from "ngx-toastr";
import { AngularFireModule } from "@angular/fire";
import { AngularFireDatabaseModule } from "@angular/fire/database";
import { RegistroComponent } from "./registro/registro.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LaboratorioComponent } from './laboratorio/laboratorio.component';
import { BoletasComponent } from './boletas/boletas.component';
import { AsistenciaComponent } from './asistencia/asistencia.component';
import { VacacionesComponent } from './vacaciones/vacaciones.component';
import { PrestamosComponent } from './prestamos/prestamos.component';
import { UsuarioComponent } from './usuario/usuario.component';
import { JefaturaComponent } from './jefatura/jefatura.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    RegistroComponent,
    LaboratorioComponent,
    BoletasComponent,
    AsistenciaComponent,
    VacacionesComponent,
    PrestamosComponent,
    UsuarioComponent,
    JefaturaComponent
  ],
  imports: [
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    AngularFireMessagingModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    FlexLayoutModule,
    FormsModule,
    HttpClientModule,
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatSidenavModule,
    MatTabsModule,
    MatToolbarModule,
    ReactiveFormsModule,
    ServiceWorkerModule.register("ngsw-worker.js", {
      enabled: environment.production
    }),
    ServiciosModule,
    ToastrModule.forRoot({
      positionClass: "toast-bottom-center",
      preventDuplicates: true
    })
  ],
  providers: [{ provide: MAT_DATE_LOCALE, useValue: "es-PE" }],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(matIconRegistry: MatIconRegistry, domSanitizer: DomSanitizer) {
    matIconRegistry.addSvgIconSet(
      domSanitizer.bypassSecurityTrustResourceUrl(
        "./assets/icons/iconografia.svg"
      )
    );
  }
}
