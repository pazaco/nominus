export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyB0WrKAai8wun_4NUuhVXb-tUJlc6b30as",
    authDomain: "nominus-209402.firebaseapp.com",
    databaseURL: "https://nominus-209402.firebaseio.com",
    projectId: "nominus-209402",
    storageBucket: "nominus-209402.appspot.com",
    messagingSenderId: "763402879407"
  },
  actionCodeSettings: {
    url: 'https://lab.nominus.pe/registro',
    handleCodeInApp: true
  },
  vapidKey:
  {
    "publicKey": "BCbMw-UiT-KIotQVYIOePbNIJnq9SrEn2pRmBDu8uMTEVj6uPIqA4FWgeqiZIq4zYyAN0M6SXd9_TvES9tO856k",
    "privateKey": "f8W5XiE6F5Sa_3xHHyJ4XN-U3aH8sPsys6QnYbSSV3A"
  },
  urlAPI: "https://api.nominus.pe/"
};
