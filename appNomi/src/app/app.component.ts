import { Component, ViewChild, ElementRef, AfterViewInit, HostListener } from '@angular/core';
import { AuthService } from './servicios/auth.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  @HostListener('window:resize', ['$event'])
  onresize(event) {
    const w = event.srcElement.innerWidth;
    const h = event.srcElement.innerHeight;
    this.auth.ancho = w;
    this.auth.direccion = w > h ? "horizontal" : "vertical";
    this.auth.tamanno = w < 320 ? 'xxs' : w < 600 ? 'xs' : w < 960 ? 'sm' : w < 1280 ? 'md' : w < 1920 ? 'lg' : 'xl';
  }

  constructor(private auth: AuthService, toast: ToastrService) {

  }

}
