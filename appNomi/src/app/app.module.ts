import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule, DomSanitizer } from '@angular/platform-browser';
import { environment } from '../environments/environment';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Height100Directive } from './general/height100.directive';
import { MatIconRegistry, MAT_DATE_LOCALE, MAT_DATE_FORMATS, MatFormFieldModule, MatInputModule, MatExpansionModule, MatIconModule, MatButtonModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { PasswordComponent } from './auth/password/password.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { ToastrModule } from 'ngx-toastr';

import { HomeComponent, PanelHomeComponent } from './home/home.component';
import { AltaComponent } from './auth/alta/alta.component';
import { LoginComponent } from './auth/login/login.component';
import { LogoutComponent } from './auth/logout/logout.component';
import { VerificacionComponent } from './auth/verificacion/verificacion.component';
import { HttpClientModule } from '@angular/common/http';
import { AuthService } from './servicios/auth.service';
import { DataService } from './servicios/data.service';

@NgModule({
  declarations: [
    AppComponent,
    Height100Directive,
    HomeComponent,
    PanelHomeComponent,
    LoginComponent,
    LogoutComponent,
    AltaComponent,
    PasswordComponent,
    VerificacionComponent
  ],
  imports: [
    AngularFireAuthModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    FlexLayoutModule,
    FormsModule,
    HttpClientModule,
    MatButtonModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    ReactiveFormsModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    ToastrModule.forRoot({ positionClass: "toast-bottom-right", timeOut: 10000, preventDuplicates: true })
  ],
  providers: [
    AuthService,
    DataService,
    { provide: MAT_DATE_LOCALE, useValue: 'es-PE' },
    {
      provide: MAT_DATE_FORMATS,
      useValue: {
        parse: {
          dateInput: 'L',
        },
        display: {
          dateInput: 'L',
          monthYearLabel: 'MMM YYYY',
          dateA11yLabel: 'LL',
          monthYearA11yLabel: 'MMMM YYYY',
        },
      }
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(matIconRegistry: MatIconRegistry, domSanitizer: DomSanitizer) {
    matIconRegistry.addSvgIconSet(domSanitizer.bypassSecurityTrustResourceUrl('./assets/mdi.svg'));
  }
}
