import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  panel = { nti: true, bel: true, prs: true, asi: true, vac: true, acc: true };

  constructor() { }

  ngOnInit() {
  }

  toggle(obj) {
    this.panel[obj] = !this.panel[obj];
  }
}

@Component({
  selector: 'app-panel-home',
  templateUrl: './panel.home.html',
  styleUrls: ['./home.component.scss']
})
export class PanelHomeComponent {
}
