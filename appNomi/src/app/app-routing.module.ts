import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { AltaComponent } from './auth/alta/alta.component';
import { PasswordComponent } from './auth/password/password.component';
import { HomeComponent } from './home/home.component';
import { VerificacionComponent } from './auth/verificacion/verificacion.component';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'registro', component: AltaComponent },
  { path: 'password', component: PasswordComponent },
  { path: 'home', component: HomeComponent },
  { path: 'verificacion', component: VerificacionComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
