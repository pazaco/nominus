import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/servicios/auth.service';
import { FormControl, Validators, FormGroupDirective, NgForm, FormGroup } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material';
import { ToastrService } from 'ngx-toastr';

export class ErrStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  userEmail = new FormControl('', [Validators.required, Validators.email]);
  userPassword = new FormControl('', [Validators.required]);
  matcher = new ErrStateMatcher();

  constructor(public auth: AuthService, private toast: ToastrService) { }

  ngOnInit() {
  }

  msgHint(campo) {
    if (campo.valid || campo.pristine || campo.untouched) {
      return '';
    } else {
      if (campo.errors) {
        if (campo.errors.required) {
          return 'requerido!';
        } else {
          if (campo.errors.email) {
            return 'correo inválido';
          } else {
            console.log(campo.errors);
          }
        }
      }
    }
  }

}
