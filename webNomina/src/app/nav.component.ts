import { Component, OnInit, OnDestroy } from '@angular/core';
import { NavService } from './recursos/nav.service';
import { Subscription } from 'rxjs';
import { aniMenuMax, aniBotonMax, aniLogoMax, aniLogoMin, aniMiniMnu, aniSubMnu, aniTextMax } from './recursos/animar';
import { LoginService } from './recursos/login.service';
import { AppComponent } from './app.component';

@Component({
    selector: 'nav-menu',
    templateUrl: './nav.component.html',
    styleUrls: ['./nav.component.css'],
    animations: [aniBotonMax, aniMenuMax, aniLogoMax, aniLogoMin, aniMiniMnu, aniSubMnu, aniTextMax]
})
export class NavComponent implements OnInit, OnDestroy {
    public siMenu: any[];
    public tamanho;
    private menu_: Subscription;
    private tamanho_: Subscription;

    constructor(public navi: NavService, public login: LoginService, public app: AppComponent) { };

    ngOnInit() {
        this.menu_ = this.navi.Menu()
            .subscribe(
                data => { this.siMenu = data; }
            );
        this.tamanho_ = this.navi.Tamanho().subscribe(
            data => { this.tamanho = data; }
        );
    };

    ngOnDestroy() {
        this.menu_.unsubscribe();
        this.tamanho_.unsubscribe();
    };

    toggSubmenu(oj) {
        if (this.tamanho == 'maxi') {
            oj.expand = oj.expand == 'no' ? 'si' : 'no';
        } else {
            this.siMenu.filter(cop => {
                cop.miniEx = cop.id == oj.id ? (cop.miniEx == 'si' ? 'no' : 'si') : 'no';
            });
        }
    };
}
