import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { BoletasComponent } from './boletas/boletas.component';
import { AsistenciaComponent } from './asistencia/asistencia.component';
import { VacacionesComponent } from './vacaciones/vacaciones.component';
import { PersonalComponent } from './datos/personal/personal.component';
import { ContratosComponent } from './datos/contratos/contratos.component';
import { LoginComponent } from './login/login.component';
import { RegistroComponent } from './login/registro/registro.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: "login", component: LoginComponent },
  { path: "registro", component: RegistroComponent},
  { path: "registro/:registro", component: RegistroComponent},
  { path: "bolBoletas", component: BoletasComponent },
  { path: "bolBoletas/:calculo", component: BoletasComponent },
  { path: "asiAsistencia", component: AsistenciaComponent },
  { path: "vacVacaciones", component: VacacionesComponent },
  { path: "datPersonal", component: PersonalComponent },
  { path: "datContratos", component: ContratosComponent },
  { path: 'home', component: HomeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
