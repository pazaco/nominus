import { Component, OnInit, HostBinding } from '@angular/core';
import { NavService } from '../recursos/nav.service';
import { ActivatedRoute } from '@angular/router';
import { DataService } from '../recursos/data.service';
import { QRCodeComponent } from 'angular2-qrcode';

@Component({
  selector: 'app-boletas',
  templateUrl: './boletas.component.html'
})
export class BoletasComponent implements OnInit {
  public nCalculo: number;
  public xPeriodo: string;
  public calculos: any[];
  public calculo: any;
  public boleta: any;
  public periodos: string[];
  public contratos: any[];
  public parciales: number[];
  public contrato: any;
  public busy: boolean = false;
  public busyBol: boolean = false;
   constructor(private navi: NavService, private route: ActivatedRoute, private data: DataService) {
  }

  ngOnInit() {
    this.navi.colapsarMinis();
    this.busy = true;
    this.data.boletasIndice().subscribe(rsp => {
      this.contratos = rsp.contratos;
      this.calculos = rsp.calculos;
      this.nCalculo = eval(this.route.snapshot.paramMap.get('calculo')) || this.calculos[0].iCalculoPlanilla;
      this.calculos.filter(c => {
        if (c.iCalculoPlanilla == this.nCalculo) {
          this.contrato = c.iContrato;
          this.calculo = c;
          this.xPeriodo = c.iAnoMes;
        }
      });
      this.chContrato();
    },
      ex => {

      },
      () => {
        this.busy = false;
      });
  }

  chContrato() {
    this.periodos = [];
    this.calculos.filter(c => {
      if (c.iContrato == this.contrato && this.periodos.indexOf(c.iAnoMes) == -1) {
        this.periodos.push(c.iAnoMes);
      }
    });
    if (this.periodos.indexOf(this.xPeriodo) == -1) {
      this.xPeriodo = this.periodos[0];
    }
    this.chPeriodo();
  }

  chPeriodo() {
    this.parciales = [];
    this.calculos.filter(c => {
      if (c.iContrato == this.contrato && c.iAnoMes == this.xPeriodo && this.parciales.indexOf(c.iCalculoPlanilla) == -1) {
        this.parciales.push(c.iCalculoPlanilla);
      }
    });
    if (this.nCalculo == null || this.parciales.indexOf(this.nCalculo) == -1) {
      this.nCalculo = this.parciales[0];
    }
    this.chParcial();
  }

  chParcial() {
    this.calculo = this.calculos.filter(c => c.iCalculoPlanilla == this.nCalculo)[0];
    this.busyBol = true;
    this.data.printBoleta(this.calculo).subscribe(
      rsp => {
        this.boleta = rsp;
      },
      ex => { },
      () => {
        this.busyBol = false;
      });
  }

  toggle(cpar) {
      if(document.getElementById(cpar+'Cn').classList.contains("show")){
        document.getElementById(cpar+'Ico').classList.remove("fa-rotate-180")
      } else {
        document.getElementById(cpar+'Ico').classList.add("fa-rotate-180")
      }
  }

  sumaMonto(e){
    let suma=0;
    e.filter(cada=>{suma+=cada.mMonto;});
    return suma;
  }
}


