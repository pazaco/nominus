import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { LoadingModule,ANIMATION_TYPES } from 'ngx-loading';
import { HttpClientModule } from '@angular/common/http';
import { SnotifyModule, SnotifyService, ToastDefaults } from "ng-snotify";
import { QRCodeModule } from 'angular2-qrcode';
import { RecursosModule } from './recursos/recursos.module';
import { AppRoutingModule } from './app-routing.module';
import { Height100Directive } from './recursos/height100.directive';
import { MayusculaDirective, NumericoDirective } from './recursos/teclado.directive';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { EqualValidator }  from './recursos/login.service';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { NavComponent } from './nav.component';

import { BoletasComponent } from './boletas/boletas.component';
import { AsistenciaComponent } from './asistencia/asistencia.component';
import { VacacionesComponent } from './vacaciones/vacaciones.component';
import { PersonalComponent } from './datos/personal/personal.component';
import { ContratosComponent } from './datos/contratos/contratos.component';
import { LoginComponent } from './login/login.component';
import { RegistroComponent } from './login/registro/registro.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavComponent,
    Height100Directive,
    MayusculaDirective,
    NumericoDirective,
    BoletasComponent,
    AsistenciaComponent,
    VacacionesComponent,
    PersonalComponent,
    ContratosComponent,
    LoginComponent,
    RegistroComponent,
    EqualValidator
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RecursosModule,
    AppRoutingModule,
    SnotifyModule,
    QRCodeModule,
    FormsModule,
    HttpClientModule,
    LoadingModule.forRoot({
      animationType: ANIMATION_TYPES.chasingDots,
      backdropBackgroundColour: 'rgba(0,0,0,0.1)',
      backdropBorderRadius: '5px',
      primaryColour: '#3b748f',
      secondaryColour: '#75cc33',
      tertiaryColour: '#004455',
    }),
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production }),    

  ],
  providers: [
    { provide: "SnotifyToastConfig", useValue: ToastDefaults },
    SnotifyService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
