import { BehaviorSubject, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { MathService } from './math.service';
import { Router } from '@angular/router';

export const MENU = [
  { id: 'hom', menu: 'Notificaciones', icono: {base:'fas fa-bell'}, link: '/home', submenu: [], expand: 'no', miniEx: 'no' },
  { id: 'bol', menu: 'Boletas', icono: {base:'fas fa-money-bill-wave'}, link: '/bolBoletas', submenu: [], expand: 'no', miniEx: 'no' },
  { id: 'asi', menu: 'Asistencia', icono: { base: 'fas fa-clock' }, link: '/asiAsistencia', submenu: [], expand: 'no', miniEx: 'no' },
  { id: 'vac', menu: 'Vacaciones', icono: { base: 'fas fa-plane' }, link: '/vacVacaciones', submenu: [], expand: 'no', miniEx: 'no' },
  { id: 'inf', menu: 'Mis datos', icono: { base: 'fas fa-user' }, link: '/datPersonal', submenu: [], expand: 'no', miniEx: 'no' },
];
export const SUBMENU = [
  // { menuId: 'dat', opcion: 'Info Personal', link: '/datPersonal', icono: { base: 'fas fa-user' }, acceso: null },
  // { menuId: 'dat', opcion: 'Contratos', link: '/datContratos', icono: { base: 'fas fa-address-card' }, acceso: null }
];

@Injectable()
export class NavService {
  private mn = MENU;
  private sm = SUBMENU;
  private tamanho: string = 'mini';

  private menu: any[] = this._defineMenu([]);
  private __menu = new BehaviorSubject<any[]>(this.menu);
  private __tamanho = new BehaviorSubject<string>('mini');

  constructor(private math: MathService, private router: Router) { }

  Menu(): Observable<any[]> {
    return this.__menu.asObservable();
  }

  Tamanho(): Observable<string> {
    return this.__tamanho.asObservable();
  }

  defineMenu(perfil: any[]) {
    this.menu = this._defineMenu(perfil);
    this.__menu.next(this.menu);
  }

  crumbs() {
    let routeActivo = this.router.routerState.snapshot.url;
    let oRsp = { menu: null, opcion: null, icono: null };
    let menuAct = this.menu.filter(cmn => cmn.link == routeActivo);
    if (menuAct.length > 0) {
      oRsp.menu = "";
      oRsp.opcion = "";
      oRsp.icono = menuAct[0].icono;
    } else {
      menuAct = this.menu.filter(cmn => cmn.id == routeActivo.substring(1, 4));
      if(menuAct.length>0){
      let subMenu = menuAct[0].submenu.filter(csm => csm.link == routeActivo);
      if (subMenu.length > 0) {
        oRsp.menu = menuAct[0].menu;
        oRsp.opcion = subMenu[0].opcion;
        oRsp.icono = subMenu[0].icono;
      }}
    }
    return oRsp;
  }

  crumb() {
    let routeActivo = this.router.routerState.snapshot.url;
    let oRsp = { menu: null, icono: null };
    let menuAct = this.mn.filter(cmn => cmn.link == routeActivo)[0];
    return {menu: menuAct.menu,icono:menuAct.icono};
  }

  colapsarMinis() {
    this.menu.filter(csm => csm.miniEx = 'no');
    this.__menu.next(this.menu);
  }

  private _defineMenu(perfil: any[]): any[] {
    return this.mn.filter(cmn => {
      let msubm = this.sm.filter(csm => {
        if (csm.menuId == cmn.id) {
          if (csm.acceso == null) {
            return true;
          } else {
            if (perfil.indexOf(99999) == -1) {
              return (this.math.Interseccion(csm.acceso, perfil).length > 0);
            } else {
              return true;
            }
          }
        } else return false;
      });
      if (cmn.link != '' && msubm.length == 0) {
        return true;
      } else {
        if (msubm.length > 0) {
          cmn.submenu = msubm;
          return true;
        } else {
          return false;
        }
      }
    });
  }

  toggle() {
    switch (this.tamanho) {
      case 'maxi': this.tamanho = 'nada'; break;
      case 'nada': this.tamanho = 'mini'; break;
      case 'mini': this.tamanho = 'maxi'; break;
    }
    this.__tamanho.next(this.tamanho);
  }

  swipeLeft() {
    if (this.tamanho == 'mini') this.tamanho = 'nada';
    if (this.tamanho == 'maxi') this.tamanho = 'mini';
    this.__tamanho.next(this.tamanho);
  }

  swipeRight() {
    if (this.tamanho == 'mini') this.tamanho = 'maxi';
    if (this.tamanho == 'nada') this.tamanho = 'mini';
    this.__tamanho.next(this.tamanho);
  }

  toggleVisible() {
    this.tamanho = this.tamanho != 'maxi' ? 'maxi' : 'mini';
    this.__tamanho.next(this.tamanho);
  }
}