import { Injectable, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { LoginService } from "./login.service";
import { Observable } from "rxjs";
import { tap } from 'rxjs/operators';

@Injectable()
export class DataService implements OnInit {
  constructor(private http: HttpClient, private login: LoginService) { }
  ngOnInit() {
  }

  public Notificaciones(): Observable<any> {
    return  this.http.get(this.login.getApi("general") + 'notificaciones/calculos', this.login.getHeaders());
  }

  public boletasIndice():Observable<any>{
    return this.http.get(this.login.getApi("general") +"boleta/indice", this.login.getHeaders());
  }

  public printBoleta(oCalculo:any):Observable<any>{
    return this.http.post(this.login.getApi("general") +"boleta/data", oCalculo,this.login.getHeaders());
  }
}