import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavService } from './nav.service';
import { LoginService } from './login.service';
import { MathService } from './math.service';
import { TipologiaService } from './tipologia.service';
import { DataService } from './data.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers: [NavService, LoginService, MathService, TipologiaService, DataService]
})
export class RecursosModule { }
