import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { LoginService } from './login.service';
import { Observable } from 'rxjs';

@Injectable()
export class TipologiaService {
    constructor(private http: HttpClient, private login: LoginService) { }

    public getTiposDocumento(): Observable<any> {
        return this.http.get(this.login.getApi("general") + '/tipologia/tiposDocIdentidad', this.login.getHeaders())
    }

}
