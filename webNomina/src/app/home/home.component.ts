import { Component, OnInit, HostBinding, OnDestroy } from '@angular/core';
import { NavService } from '../recursos/nav.service';
import { aniSlideInDown } from '../recursos/animar';
import { Router } from '@angular/router';
import { LoginService } from '../recursos/login.service';
import { DataService } from '../recursos/data.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {
  public notifs;
  public busy:boolean=false;

  constructor(private navi: NavService, private router: Router, public login: LoginService, public data: DataService) { 
  }

  ngOnInit() {
    this.navi.colapsarMinis();
    this.busy=true;
    if (this.login.sesion == null) {
      this.router.navigateByUrl("/login");
    } else {
      this.data.Notificaciones().subscribe(nots=> {this.notifs=nots;},err=>{},()=>{this.busy=false;});
    }
  }

  public procesar(tproc){
    if(tproc.bTipoNotificacion==1){
      this.router.navigateByUrl("/bolBoletas/"+tproc.iCalculoPlanilla);
    }
    else {
      window.alert(JSON.stringify(tproc));
    }
  }
}
