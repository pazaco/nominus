import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoginService } from '../../recursos/login.service';
import { SnotifyService } from 'ng-snotify';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html'
})
export class RegistroComponent implements OnInit {
  public parPassword: any = { registro: '', password: '',confirma: '' };
  public avatar: any;
  public busy:boolean=false;

  constructor(private route: ActivatedRoute, private router: Router, private login: LoginService,private snotify: SnotifyService) { }

  ngOnInit() {
    this.parPassword.registro = this.route.snapshot.paramMap.get('registro');
    this.login.getAvatarRegistro(this.parPassword.registro)
      .subscribe(rsp => {
        this.avatar = rsp;
      });
  }

  callCambiarPwd(){
    this.busy= true;
    this.login.cambiarPassword(this.parPassword)
    .subscribe(rsp=>{
      this.snotify.confirm("Se registró su nueva contraseña.  Presiona Ok para ir a la página de inicio de sesión.", null, {
        closeOnClick: false,
        buttons: [
          {
            text: "Ok", action: (toast) => {
              this.busy=false;
              this.router.navigateByUrl("/home");
              this.snotify.remove(toast.id);
            }
          }
        ]
      });
    })
  }
}
