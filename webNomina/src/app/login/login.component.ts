import { Component, OnInit } from '@angular/core';
import { LoginService } from '../recursos/login.service';
import { SnotifyService } from 'ng-snotify';
import { TipologiaService } from '../recursos/tipologia.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {
  public parLogin: ParLogin = { local: false, escenario: 0, bTipoDocIdentidad: 1, xDocIdentidad: '' };
  public busy: boolean = false;
  public tiposDoc: any[]

  constructor(private login: LoginService, public snotify: SnotifyService, private tipos: TipologiaService, private router: Router) {
    tipos.getTiposDocumento().subscribe(rsp => this.tiposDoc = rsp);
  }

  ngOnInit() {
    if (this.login.sesion != null) {
      this.router.navigateByUrl("/home");
    }
  }

  callRegistro(scena: number) {
    if (scena != 1 && scena != 2) scena = 1;
    this.busy = true;
    this.parLogin.escenario = scena;
    this.login.putLogin(this.parLogin).subscribe(
      resp => {
        switch (resp) {
          case "ErrorFmto":
            this.snotify.error("Correo electrónico no tiene el formato correcto.");
            break;
          case "NoAutentico":
            this.snotify.warning("El documento de identidad no coincide.");
            break;
          case "YaRegistrado":
            this.snotify.warning("El correo " + this.parLogin.xEmail + " ya se encuentra operativo.");
            break;
          case "NoClaims":
            this.snotify.warning("No tiene permisos para cambiar contraseña.");
            break;
          case "TokenInicio":
            this.snotify.info("Revise su correo, se ha enviado un mensaje con un vínculo para iniciar su registro.");
            this.parLogin = { local: false, escenario: 0, bTipoDocIdentidad: 1, xDocIdentidad: '' };
            break;
          case "TokenOlvido":
            this.snotify.info("Revise su correo, se ha enviado un mensaje con un vínculo para registrar una nueva contraseña.");
            this.parLogin = { local: false, escenario: 0, bTipoDocIdentidad: 1, xDocIdentidad: '' };
            break;
          default:
            this.snotify.warning("Correo no válido.");
            break;
        };
      },
      ex => {
        this.snotify.confirm(JSON.stringify(ex), null, { closeOnClick: false, buttons: [{ text: "Ok", action: (toast) => { this.snotify.remove(toast.id) } }] });
      },
      () => {
        this.busy = false;
      });
  }

  callLogin() {
    this.busy = true;
    this.parLogin.escenario = 0;
    this.login.putLogin(this.parLogin)
      .subscribe(
        resp => {
          if (resp.respuesta == 'Ok') {
            this.login.registrarSesion(resp, this.parLogin.local);
            this.router.navigateByUrl("/home");
          } else {
            if (resp.respuesta == 'NoValido') {
              this.snotify.warning("Correo o contraseña incorrecta.");
            } else {
              if (resp.respuesta == 'ErrorFmto') {
                this.snotify.error("Correo electrónico no tiene el formato correcto.");
              } else {
                this.snotify.confirm("Correo no validado, seleccione la opción 'Ingresar por primera vez", null, { closeOnClick: false, buttons: [{ text: "Ok", action: (toast) => { this.snotify.remove(toast.id) } }] });
              }
            }
          }
        },
        ex => {
          this.snotify.confirm(JSON.stringify(ex), null, { closeOnClick: false, buttons: [{ text: "Ok", action: (toast) => { this.snotify.remove(toast.id) } }] });
        },
        () => {
          this.busy = false;
        });
  }
}

export interface ParLogin {
  xEmail?: string;
  password?: string;
  local: boolean;
  escenario: number;
  bTipoDocIdentidad: number;
  xDocIdentidad: string
}

