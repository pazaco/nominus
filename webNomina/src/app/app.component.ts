import { Component, OnInit, OnDestroy } from '@angular/core';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';
import { LoginService } from './recursos/login.service';
import { NavService } from './recursos/nav.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  public sesion: any = null;
  private _sesion: Subscription;
  constructor(public snotify: SnotifyService, public navi: NavService, public login: LoginService, public router: Router, public route: ActivatedRoute) {
  }

  ngOnInit() {
    this.navi.colapsarMinis();
    this.login.storage();
    this._sesion = this.login.Sesion().subscribe(
      rsp => {
        this.sesion = rsp;
        if(this.sesion==null){
          this.router.navigateByUrl("/login");
        } else {
          this.router.navigateByUrl("/home");
        }
      }
    );
  }

  ngOnDestroy() {
    this._sesion.unsubscribe();
  }
}
