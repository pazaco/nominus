import { Component, OnInit, OnDestroy } from '@angular/core';
import { aniMenuMax, aniOpacoMax } from './recursos/animar';
import { SnotifyService } from '../../node_modules/ng-snotify';
import { NavService } from './recursos/nav.service';
import { LoginService } from './recursos/login.service';
import { Router } from '../../node_modules/@angular/router';
import { Subscription } from '../../node_modules/rxjs';
import { AuthService } from './recursos/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [aniMenuMax, aniOpacoMax]
})
export class AppComponent implements OnInit, OnDestroy {
  public siMenu: any[];
  public tamanho = 'mini';
  public _menu: Subscription;

  constructor(public snotify: SnotifyService, public navi: NavService, public login: LoginService, public router: Router, public auth:AuthService) { }

  ngOnInit() {
    this._menu = this.navi.Menu().subscribe(
      data => {
      this.siMenu = data;
      }
    );
  }

  ngOnDestroy() {
    this._menu.unsubscribe();
  }

  toggleMenu() {
    if (this.tamanho == 'mini') {
      this.tamanho = 'maxi';
    } else {
      this.tamanho = 'mini';
    }
  }

  logout() {
    this.auth.logout();
  }
}

