import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { LoadingModule,ANIMATION_TYPES } from 'ngx-loading';
import { AngularFireModule } from 'angularfire2';
// import { AngularFirestoreModule } from 'angularfire2/firestore';
// import { AngularFireAuthModule } from 'angularfire2/auth';
import { HttpClientModule } from '@angular/common/http';
import { SnotifyModule, SnotifyService, ToastDefaults } from "ng-snotify";
import { QRCodeModule } from 'angular2-qrcode';
import { RecursosModule } from './recursos/recursos.module';
import { AppRoutingModule } from './app-routing.module';
import { Height100Directive } from './recursos/height100.directive';
import { MayusculaDirective, NumericoDirective } from './recursos/teclado.directive';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { EqualValidator }  from './recursos/login.service';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';

import { BoletasComponent } from './boletas/boletas.component';
import { AsistenciaComponent } from './asistencia/asistencia.component';
import { VacacionesComponent } from './vacaciones/vacaciones.component';
import { PersonalComponent } from './datos/personal/personal.component';
import { ContratosComponent } from './datos/contratos/contratos.component';

@NgModule({
  declarations: [
    AppComponent,
    Height100Directive,
    MayusculaDirective,
    NumericoDirective,
    EqualValidator,
    HomeComponent,
    BoletasComponent,
    AsistenciaComponent,
    VacacionesComponent,
    PersonalComponent,
    ContratosComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RecursosModule,
    AppRoutingModule,
    SnotifyModule,
    QRCodeModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    AngularFireModule.initializeApp(environment.firebase),
    // AngularFirestoreModule,
    // AngularFireAuthModule,
    LoadingModule.forRoot({
      animationType: ANIMATION_TYPES.chasingDots,
      backdropBackgroundColour: 'rgba(0,0,0,0.1)',
      backdropBorderRadius: '5px',
      primaryColour: '#3b748f',
      secondaryColour: '#75cc33',
      tertiaryColour: '#004455',
    }),
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production }),    
  ],
  providers: [
    { provide: "SnotifyToastConfig", useValue: ToastDefaults },
    SnotifyService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
