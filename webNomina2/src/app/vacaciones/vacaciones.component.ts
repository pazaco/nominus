import { Component, OnInit, HostBinding } from '@angular/core';
import { NavService } from '../recursos/nav.service';
import { aniSlideInDown } from '../recursos/animar';

@Component({
  selector: 'app-vacaciones',
  templateUrl: './vacaciones.component.html',
  styleUrls: ['./vacaciones.component.css'],
  animations: [aniSlideInDown]
})
export class VacacionesComponent implements OnInit {
  @HostBinding("@routeAnimation") routeAnimation = true;
  @HostBinding("style.display") display = 'block';
  @HostBinding("style.position") position = "absolute";


  constructor(private navi: NavService) { }

  ngOnInit() {
    this.navi.colapsarMinis();
  }

}
