import { Directive, EventEmitter, Output, HostListener, ElementRef } from '@angular/core';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@Directive({selector: '[ngModel][mayuscula]'})
export class MayusculaDirective {
  @Output() ngModelChange : EventEmitter<any> = new EventEmitter();
  loDigitado:string;

  constructor() { }

  @HostListener('input',['$event'] )
  onInputChange($event){
    this.loDigitado = $event.target.value.toUpperCase();
    this.ngModelChange.emit(this.loDigitado);
  }
}

@Directive({selector: '[ngModel][numerico]'})
export class NumericoDirective {
 // Allow decimal numbers and negative values
 private regex: RegExp = new RegExp(/^-?[0-9]+(\.[0-9]*){0,1}$/g);
 // Allow key codes for special events. Reflect :
 // Backspace, tab, end, home
 private specialKeys: Array<string> = [ 'Backspace', 'Tab', 'End', 'Home', '-' ];

constructor(private el: ElementRef) {
 }
 @HostListener('keydown', [ '$event' ])
 onKeyDown(event: KeyboardEvent) {
 // Allow Backspace, tab, end, and home keys
 if (this.specialKeys.indexOf(event.key) !== -1) {
 return;
 }
 let current: string = this.el.nativeElement.value;
 let next: string = current.concat(event.key);
 if (next && !String(next).match(this.regex)) {
 event.preventDefault();
 }
 }
}
