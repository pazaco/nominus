import { interval } from 'rxjs';
import { Injectable } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';

@Injectable()
export class CheckForUpdateService {

  constructor(updates: SwUpdate) {
    interval(6 * 60 * 60).subscribe(() => updates.checkForUpdate());
  }
}

@Injectable()
export class LogUpdateService {

  constructor(updates: SwUpdate) {
    updates.available.subscribe(event => {
      console.log('versión actual: ', event.current);
      console.log('versión disponible: ', event.available);
    });
    updates.activated.subscribe(event => {
      console.log('versión anterior: ', event.previous);
      console.log('versión nueva: ', event.current);
    });
  }
}

@Injectable()
export class PromptUpdateService {

  constructor(updates: SwUpdate) {
    updates.available.subscribe(event => {
      if (event) {
        updates.activateUpdate().then(() => document.location.reload());
      }
    });
  }
}