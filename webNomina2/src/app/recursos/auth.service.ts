import { Injectable } from '@angular/core';

import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';

import { Observable } from 'rxjs';

@Injectable()
export class AuthService {
  user: Observable<firebase.User>;

  constructor(private firebaseAuth: AngularFireAuth) {
    this.user = firebaseAuth.authState;
  }

  signup(email: string, password: string) {
    this.firebaseAuth
      .auth
      .createUserWithEmailAndPassword(email, password)
      .then(value => {
        console.log('Usuario creado satisfactoriamente!', value);
      })
      .catch(err => {
        console.log('Error creando usuario: ', err.message);
      });
  }

  login(email: string, password: string) {
    this.firebaseAuth
      .auth
      .signInWithEmailAndPassword(email, password)
      .then(value => {
        console.log('Bien! usuario sesionando');
      })
      .catch(err => {
        console.log('Error creando sesión: ', err.message);
      });
  }

  logout() {
    this.firebaseAuth
      .auth
      .signOut();
  }

  unaSemilla(): string {
    let numSemillas = semillas.length;
    let elegido = Math.floor(Math.random() * numSemillas);
    return semillas[elegido];
  }
}



export const semillas =
  [ 
    "8f5a5c53-9a8f-4a60-9d28-01a3d73e93b3",
    "9e115f45-6457-42ed-b87c-020aca33e8d9",
    "a735090c-3b5b-4c6f-a87d-04e7351b9d6d",
    "4a998b49-a89e-4072-933f-06a0d79d5380",
    "80e53701-f60b-499a-a79b-0b91d05cc51e",
    "4291850f-6b68-48a9-b35e-0c70f3ddb08b",
    "f9dd0524-cff5-4927-be50-10f0a77cf0dc",
    "6817a111-74a0-4c6f-92fc-129ac3fabcc3",
    "ac2e266c-91a8-4923-a952-168c2590b47a",
    "477cf58d-ae9e-46b5-a24a-17b7080aed1e",
    "d1569db8-44b9-402d-aa69-17ff731e2a93",
    "384855b6-0916-4b5a-b5d4-184afeb3abad",
    "a18eebd8-cf53-4dc9-82b3-1a22dd7da246",
    "27e26fe9-629e-4ba2-9b4c-1b424df1a232",
    "786c69dc-2a20-4358-ab9c-1b5f00dcf8f8",
    "4e1995b3-f04f-4f29-96e8-1e0938ba576c",
    "016a5f19-1c93-4902-bcbe-1f3cb2a0ed91",
    "02042c29-dfed-4146-acec-26457be74f5c",
    "e0dba1f9-b7f0-4d44-9fad-26b73f9c9bbc",
    "321fcf3f-ca62-4fb1-9f36-2774dc9bd08f",
    "2c2f779f-cb3f-414c-85d3-2b15af2b0c37",
    "d1b5ea71-fcd2-4203-b8a0-2e54c5a7a4d2",
    "f4c51e02-0b75-4959-9ed5-32240e61cbd5",
    "9515a11a-ece2-4eb7-a686-32486ba72e13",
    "f16c7214-c6df-4ee9-806e-329f312c5bcf",
    "45cdc178-38c1-4b1a-83be-34db933c0af0",
    "fa6e9e66-96fc-4a80-8bc9-353866b46481",
    "e1e98e56-e485-4e2a-95f9-3a3998fb0c97",
    "75d1ee1d-15c3-488c-ad1c-3beaeea01f3a",
    "2cfcbe27-a964-4dff-82dd-3c13199f0229",
    "7355f726-b4af-43ea-bb7b-3f508320acc6",
    "fca072c8-f6fa-4816-8fb8-3f58da48a48d",
    "2a24427e-93cf-462f-a468-47c52da1e042",
    "72745a03-a5a8-4353-870c-4b47fa43f7eb",
    "f565a8c7-b622-4f2b-9689-4caa87f599c8",
    "212c968c-7ae0-4e70-8845-4d12524f5f98",
    "3f3f86c9-96a7-4f3b-a270-585646e1bd4b",
    "25827738-cfb4-49a6-924b-592378643a37",
    "665e54e1-84e5-47b7-b9b2-595d7b5061b1",
    "48d68f44-866a-4c67-be1f-59d2f9ff2b14",
    "2fd66dd4-2f00-4fe4-9997-5e6f431523e4",
    "de86f2ac-5ba5-4a89-9705-6716b43aca61",
    "dc97abf5-ceb6-45d6-bf52-6ff08116ed88",
    "013060c1-f4c0-4760-90b8-7023fbe2a6a9",
    "8310b932-e53d-411c-95de-73abcecb9419",
    "543b20b6-90c7-480d-b3f1-7e2ff40f9abe",
    "f26bcc26-f0e6-4a34-96b9-7e3312d67156",
    "32d432da-9a02-49ae-a03c-7e450b1540e5",
    "6a0d4ba9-1046-4864-a1ee-804c7fa74ea2",
    "3d50ecdd-43c4-4c2b-be24-83aa74729b17",
    "41b9a35d-20d1-4147-8f98-84b6e87ffce4",
    "14006772-afe2-4ed0-a97a-85315aaea8c0",
    "f166ac89-7e9e-41ad-8800-86452b583efc",
    "6eeba1b1-282c-4ed3-882c-8b224fb37332",
    "ee987b9d-764b-4ce4-a41e-92bf63bcc10d",
    "7fa84140-023e-4420-8d3d-93e5c8d7c554",
    "15c85a12-5399-445a-83a2-94d5b4726cf5",
    "fbf7d4cf-458d-4eab-96ba-9525837436c2",
    "5dedf21b-61a6-4e8d-aed0-960def2d9caf",
    "f8a8f4a4-1c54-4e1d-96ed-97b3eff4a0eb",
    "fcdcacd4-5973-4f5e-aec7-99aaec862b2c",
    "a5b2f572-f44a-4e67-b281-9b3b3a9188b3",
    "0b690331-8384-4cdd-aa54-9bfc5f5cee2e",
    "6a5b5648-5074-4e0b-b685-9df4e86cd330",
    "58347790-9b07-4478-9693-9e2049f8feb0",
    "3ebe00cb-5d41-43fe-9eb2-a0265f50fa2c",
    "f872d943-22ea-47f1-a7c8-a130838c45f1",
    "43640a26-df4a-4fc5-aa45-a14b71f08934",
    "2f33da24-ffaa-427d-bc76-a188681d6c70",
    "fade65cc-5efe-4645-b634-a23636b41db3",
    "1a1a13d0-eb12-41d6-be7d-a3bf3cb94f0e",
    "ff697915-36d8-4a9f-8753-a45a0f4b0ffb",
    "622558af-32da-43f5-a005-a4958ddff430",
    "90fdccc7-47d1-4865-98f4-a587a9075b06",
    "a8cd698a-a02d-447c-9bb8-a8bad2e45f95",
    "fd068efe-a9ac-4aab-a28c-a8bed247dd36",
    "de3920e1-43c4-4c62-8447-a8da06e985d8",
    "fabdff57-1caf-48ff-a858-a96ff61c38ac",
    "bc2dacd6-b73d-4add-b89c-aa29ce93a295",
    "a791eb26-c3c5-4b11-a0da-ab06d8d4102c",
    "f72c4ae4-7836-48fe-afac-ab2fa392c5d2",
    "4de21181-1ff5-4657-aacd-ad3512b1a425",
    "99dc32c1-b98d-4621-8fbf-ae086266cf9d",
    "50af0d2e-7d7b-46ca-b800-b2a0e39dccf0",
    "1d8103bb-a68e-43ac-bf4b-b2bee183e7c6",
    "d87a6480-ef79-4703-ac92-b45563728ed4",
    "f6d2053c-c928-48e3-ba7e-b5fce20713ad",
    "0e87ccdb-0485-4eff-8635-b685ee191666",
    "08b5c20f-0f89-4be5-ba80-b7cc7779730c",
    "16569bec-ddad-430f-a9ea-b992da660cdf",
    "7f734c3a-6ffd-4f83-b7a6-bba7eef9e4bc",
    "7374123b-c58d-4f51-8b52-bec5a55d4029",
    "58fe7968-bbe6-4820-a873-bfcf8fa8a6a7",
    "de640127-309e-4fbb-bb85-c08b8360235d",
    "8d526190-5b75-4d4e-aaab-c1a0afb3f04f",
    "10f65536-098e-446e-a9ea-c9c49c17665d",
    "ac1f8ecf-8402-4058-9856-caf9fb76be35",
    "b63dc5c5-6814-45a5-ae18-cba8d407e087",
    "af554b7d-dd4a-4aa4-90da-d34ab8e0067a",
    "7e9a5447-7f88-4eae-abc7-d3ebc643789a",
    "f6a6ee81-0969-4348-976d-d764e83a2773",
    "92d522bb-0beb-4d90-aa3d-d91247c60b12",
    "f338d6c4-5793-4bc6-bfd9-daaf6e31e29d",
    "8bf71eb5-2bc5-4786-9b00-db40f57e4b43",
    "062bfcf7-b202-4a0c-9fa8-db8c4cac7c1a",
    "dd0fe958-5cc8-44a3-ad7f-dd52c490ee79",
    "28a68163-884c-44a8-b91a-dec6eb5e953a",
    "2a80873d-8c18-4b1f-8928-e2b5e2980dc2",
    "9059a725-598b-48ea-8e74-e4f1bf943e5f",
    "ede66978-a460-43f5-b917-e6090e5aaacf",
    "8869e54b-c949-42f4-8611-e9dd6cc285ea",
    "ae29c8ad-363f-4f47-9243-ec2f4867b364",
    "2f972598-ca21-4f48-b471-eccc6df4ba1d",
    "97429d67-7833-41a4-a636-ee7e167e21b7",
    "a48dea19-4a44-43ca-9e71-eec4b49cc4c6",
    "4e0c7765-adaa-41b6-8f44-f005a848ec1f",
    "5d316fd2-8fe4-4e95-9220-f34dfd0cfdb3",
    "1522dc2c-1996-463c-b103-f44bf098484b",
    "45270fb4-c591-46f8-a86b-f8532de93eb4",
    "ce2a1dc1-7161-4d62-a6a3-fb0726ac9ff3",
    "46f3ffce-a547-4063-829b-fb471bf18174",
    "f89491a2-5c0e-4887-9b1e-ffbfd911dc4b"
  ]
