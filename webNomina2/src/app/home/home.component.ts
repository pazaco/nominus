import { Component, OnInit, HostBinding, OnDestroy } from '@angular/core';
import { NavService } from '../recursos/nav.service';
import { aniSlideInDown } from '../recursos/animar';
import { Router } from '@angular/router';
import { LoginService } from '../recursos/login.service';
import { DataService } from '../recursos/data.service';
import { Subscription } from 'rxjs';
import { AuthService } from '../recursos/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  animations:[aniSlideInDown]
})
export class HomeComponent implements OnInit {
  @HostBinding("@routeAnimation") routeAnimation = true;
  @HostBinding("style.display") display = 'block';
  @HostBinding("style.position") position = 'absolute';
  notifs;
  busy:boolean=false;
  email:string;
  password:string;

  constructor(private navi: NavService, private router: Router,public data: DataService,public auth:AuthService) { 
  }

  ngOnInit() {
  }

  public procesar(tproc){
    if(tproc.bTipoNotificacion==1){
      this.router.navigateByUrl("/boletas/"+tproc.iCalculoPlanilla);
    }
    else {
      window.alert(JSON.stringify(tproc));
    }
  }

  signup() {
    this.auth.signup(this.email, this.password);
    this.email = this.password = '';
  }

  login() {
    this.auth.login(this.email, this.password);
    this.email = this.password = '';    
  }

}
