import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { BoletasComponent } from './boletas/boletas.component';
import { AsistenciaComponent } from './asistencia/asistencia.component';
import { VacacionesComponent } from './vacaciones/vacaciones.component';
import { PersonalComponent } from './datos/personal/personal.component';
import { ContratosComponent } from './datos/contratos/contratos.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: "boletas", component: BoletasComponent },
  { path: "boletas/:calculo", component: BoletasComponent },
  { path: "asistencia", component: AsistenciaComponent },
  { path: "vacaciones", component: VacacionesComponent },
  { path: "datPersonal", component: PersonalComponent },
  { path: "datContratos", component: ContratosComponent },
  { path: 'home', component: HomeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
