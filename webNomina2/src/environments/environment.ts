// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyB0WrKAai8wun_4NUuhVXb-tUJlc6b30as",
    authDomain: "nominus-209402.firebaseapp.com",
    databaseURL: "https://nominus-209402.firebaseio.com",
    projectId: "nominus-209402",
    storageBucket: "nominus-209402.appspot.com",
    messagingSenderId: "763402879407"
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
