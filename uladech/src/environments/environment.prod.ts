export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyCKETJWCElRDxdo-JvY-Cp8pTV6m2V4_RY",
    authDomain: "asistencia-uladech.firebaseapp.com",
    databaseURL: "https://asistencia-uladech.firebaseio.com",
    projectId: "asistencia-uladech",
    storageBucket: "asistencia-uladech.appspot.com",
    messagingSenderId: "1095708802748"
  },
  actionCodeSettings: {
    url: 'https://asistencia.uladech.edu.pe/registro',
    handleCodeInApp: true
  },
  vapidKey:
  {
    "publicKey": "BCbMw-UiT-KIotQVYIOePbNIJnq9SrEn2pRmBDu8uMTEVj6uPIqA4FWgeqiZIq4zYyAN0M6SXd9_TvES9tO856k",
    "privateKey": "f8W5XiE6F5Sa_3xHHyJ4XN-U3aH8sPsys6QnYbSSV3A"
  },
  urlAPI: "https://api.remunerarte.com/",
  urlAPIuladech: "https://erp.uladech.edu.pe/api/asistencia/"
};
