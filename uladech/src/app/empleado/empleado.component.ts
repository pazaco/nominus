import { MathService } from "./../recursos/math.service";
import {
  Component,
  OnInit,
  OnDestroy,
  ElementRef,
  Renderer2
} from "@angular/core";
import { AuthService } from "../recursos/auth.service";
import { DataService } from "../recursos/data.service";
import { interval } from "rxjs";
import { FormControl } from "@angular/forms";
import { ToastrService } from "ngx-toastr";
import * as moment from "moment";
import { MatCalendarCellCssClasses, MatDialog } from "@angular/material";
import { HorasPipe } from "../recursos/pipes.pipe";
import { debounceTime, timeout } from "rxjs/operators";
import { DialAsistenciaComponent } from "../controles/asistencia/asistencia.component";

@Component({
  selector: "app-empleado",
  templateUrl: "./empleado.component.html",
  styleUrls: ["./empleado.component.scss"],
  providers: [HorasPipe]
})
export class EmpleadoComponent implements OnInit, OnDestroy {
  private elRef: ElementRef;
  private renderer: Renderer2;
  nvaMarca = new FormControl('');
  listando = false;
  buscando = false;
  sesion;
  hoy = moment();
  reclutado;
  empleados;
  empleado;
  lstCuentas;
  selDia = moment();
  cuentaGoogle = new FormControl();
  parEmpleados = {
    xCargoParcial: null,
    xJefe: null,
    xSituacion: null
  };
  colsE = ["cEmpleado", "xNombreCompleto"];
  pb = {
    empleados: false,
    empleado: false,
    historial: false,
    papeletas: false
  };
  _liberado = interval(1000);
  situaciones;
  selSituacion;
  sServidor = 8;
  iPersona;
  perfilOk;
  sesionOk;
  diario;
  mensual = [];
  mesCarga = [];
  public voces;
  public huellas;
  indicePapeletas;
  selPapeleta;
  flujoPapeleta;
  resolucion;

  constructor(
    public auth: AuthService,
    private data: DataService,
    private toastr: ToastrService,
    private math: MathService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.auth.sesion.subscribe(ses => {
      if (ses) {
        this.sesion = ses;
        this.rxRecluta();
        this.sesionOk = true;
        this.perfilOk =
          this.math.Interseccion(ses["perfiles"], [5, 6, 99]).length > 0;
        this.nvaMarca.valueChanges
          .pipe(debounceTime(700))
          .subscribe(async rsp => {
            if (rsp.length === 6) {
              const nueva = `${this.selDia.format('YYYY-MM-DD')}T${rsp.substr(0, 2)}:${rsp.substr(2, 2)}:${rsp.substr(4, 2)}Z`;
              const parDiaUsr = { iEmpleado: this.empleado.iZkEmpleado, dDia: nueva, bDispositivo: 4, cEstablecimiento: "0000" };
              await this.data.agregarMarcacion(parDiaUsr).toPromise().then(() => {
                this.chDiaMes(this.selDia);
              });
              this.nvaMarca.setValue('');
            }
          });
      }
    });
  }

  ngOnDestroy() { }

  async addCuenta() {
    const mail = this.cuentaGoogle.value + "@gmail.com";
    await this.data
      .autorizarCuentaGoogle({
        email: mail,
        iPersona: this.empleado.iZkEmpleado
      })
      .toPromise().then(rsp => {
        this.toastr.show(rsp);
        if (rsp.includes("concedido")) {
          this.lstCuentas.add(mail);
        }
      });
  }

  delCuenta(mail) {
    this.data.delCuentaEmpleado(mail).subscribe(rsp => {
      this.lstCuentas = this.lstCuentas.filter(ccu => ccu !== mail);
    });
  }

  libreDeGoogles(): boolean {
    return (
      this.lstCuentas.filter(cct => cct.includes("@gmail.com")).length === 0
    );
  }

  async rxRecluta() {
    if (this.empleado) {
      this.buscando = true;
      await this.data
        .reclutamiento(this.empleado.iZkEmpleado).toPromise().then(async recl => {
          this.huellas = recl;
          await this.data
            .voces({
              iPersona: this.empleado.iZkEmpleado,
              sServidor: this.sServidor
            }).toPromise().then(async rVoz => {
              this.voces = rVoz;
              this.buscando = false;
              await this.data
                .cuentasEmpleado(this.empleado.cCodigoEmpleado).toPromise().then(async ctas => {
                  this.lstCuentas = ctas;
                  await this.data
                    .indicePapeletas(this.empleado.iZkEmpleado).toPromise().then(ipap => {
                      this.indicePapeletas = ipap;
                      this.mesCarga = [];
                      this.mensual = [];
                      this.selPapeleta = null;
                    });
                });
            });
        });
    } else {
      this.voces = null;
      this.huellas = null;
      this.selDia = null;
      this.mensual = [];
      this.mesCarga = [];
    }
    this.chDiaMes(moment());
  }

  async chPapeleta(pasa) {
    this.selPapeleta = { bTipoPapeleta: pasa.bTipoPapeleta, iPapeleta: pasa.iPapeleta };
    await this.data.flujoPapeleta(this.selPapeleta)
      .toPromise().then(async rfl => {
        this.flujoPapeleta = rfl;
        await this.data.resolucion(this.selPapeleta)
          .toPromise().then(rrs => {
            this.resolucion = rrs;
            console.log(rrs);
          });
      });
  }

  async chDiaMes(nuevoDia: moment.Moment) {
    this.selDia = nuevoDia.clone();
    if (this.empleado) {
      this.agregaMes(this.selDia);
      const mesAnterior = nuevoDia.clone();
      mesAnterior.subtract(1, "month");
      this.agregaMes(mesAnterior);
      const parEmpleado = {
        iEmpleado: this.empleado.iZkEmpleado,
        dDia: nuevoDia.format("YYYY-MM-DD")
      };
      await this.data.diarioEmpleado(parEmpleado)
      .toPromise().then(edia => {
        this.diario = edia;
      });
    }
  }

  sonar(frase, pista) {
    this.voces.filter(cvoz => {
      if (cvoz.bFrase === frase) {
        cvoz.aFingerPrints.filter(pst => {
          if (pst.iFingerPrint === pista) {
            const aud = new Audio(this.data.api + "voces/" + pst.xSoporte);
            aud.play();
          }
        });
      }
    });
  }

  liberar(frase) {
    this.data
      .abrirVoz({
        iPersona: this.empleado.iZkEmpleado,
        sServidor: this.sServidor,
        bFrase: frase
      })
      .subscribe(rsp => {
        this.voces.filter(c => c.bFrase === frase)[0].lAbierto = true;
        this.esperar(frase);
      });
  }

  esperar(frase) {
    const esperando = this._liberado.subscribe(async csg => {
      await this.data
        .voces({
          iPersona: this.empleado.iZkEmpleado,
          sServidor: this.sServidor
        })
        .toPromise().then(rVoz => {
          this.voces = rVoz;
          if (!this.voces.filter(c => c.bFrase === frase)[0].lAbierto) {
            esperando.unsubscribe();
          }
        });
    });
  }

  async borrar(frase, voz) {
    await this.data
      .borrarVoz({ sServidor: this.sServidor, iFingerPrint: voz })
      .toPromise().then(rsp => {
        this.voces.filter(c => {
          if (c.bFrase === frase) {
            const aFngers = c.aFingerPrints.filter(f => f.iFingerPrint !== voz);
            c.aFingerPrints = aFngers;
          }
        });
      });
  }

  revaluar() {
    const dialogAsist = this.dialog.open(DialAsistenciaComponent, { data: this.diario, width: '660px', height: '400px' });
    dialogAsist.afterClosed()
      .subscribe(rAsist => {
        this.chDiaMes(this.selDia);
      });
  }

async revaluar2() {
    await this.data
      .revaluar(this.diario.oAsistencia.iAsistencia)
      .toPromise().then(rsp => {
        console.log(rsp);
        this.actualizarMes(this.selDia);
        this.toastr.success(rsp);
        this.chDiaMes(this.selDia);
      });
  }

  async onSelect($event) {
    if ($event) {
      this.listando = true;
      await this.data.persona($event).toPromise().then(rsEm => {
        this.empleado = rsEm["ouchEmpleado"];
        this.rxRecluta();
        this.listando = false;
      });
    } else {
      this.empleado = null;
      this.rxRecluta();
    }
  }

  fingerprint(dedo): boolean {
    return this.huellas.filter(chu => chu.iFinger === dedo).length > 0;
  }

  marcaMes = (d: moment.Moment): MatCalendarCellCssClasses => {
    const mesAnterior = d.clone();
    mesAnterior.subtract(1, "month");
    if (
      this.mesCarga.filter((cval: moment.Moment) =>
        cval.isSame(mesAnterior, "month")
      ).length === 0
    ) {
      this.agregaMes(mesAnterior);
    }
    const cld = this.mensual.filter(dia => d.isSame(dia.dFecha, "day"))[0];
    if (cld) {
      return "marca" + cld.bCalificacion;
    } else {
      return null;
    }
  }

  async actualizarMes(d: moment.Moment): Promise<void> {
    const modMes = d.clone();
    this.mensual = this.mensual.filter(cdi => !modMes.isSame(cdi.dFecha, "month"));
    await this.data
      .mensualEmpleado({
        iEmpleado: this.empleado.iZkEmpleado,
        dDia: modMes
      })
      .toPromise().then(rms => {
        this.mensual = this.math.Union(this.mensual, rms);
      });
  }

  async agregaMes(d: moment.Moment): Promise<any> {
    if (this.empleado) {
      if (this.mesCarga.filter(cdi => d.isSame(cdi, "month")).length === 0) {
        const addMes = d.clone();
        this.mesCarga.push(addMes);
        await this.data
          .mensualEmpleado({
            iEmpleado: this.empleado.iZkEmpleado,
            dDia: addMes
          })
          .toPromise().then(rms => {
            this.mensual = this.math.Union(this.mensual, rms);
          });
      }
    }
  }
}
