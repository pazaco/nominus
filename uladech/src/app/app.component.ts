import { Component, ViewChild, OnInit } from "@angular/core";
import { AuthService } from "./recursos/auth.service";
import { NavService } from "./recursos/nav.service";
import { ToastrService, ToastContainerDirective } from "ngx-toastr";
import { Router } from "@angular/router";
import { OrderByPipe } from "./recursos/pipes.pipe";

declare var require: any;

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
  providers: [OrderByPipe]
})
export class AppComponent implements OnInit {
  @ViewChild(ToastContainerDirective)
  toastContainer: ToastContainerDirective;

  public appVersion: string = require("../../package.json").version;
  public Menu;
  public genMenu;
  public selMenu;
  public soloIconos = true;

  constructor(
    public authService: AuthService,
    public navi: NavService,
    private router: Router,
    public toast: ToastrService,
    public orderBy: OrderByPipe
  ) {}

  ngOnInit() {
    this.toast.overlayContainer = this.toastContainer;
    this.authService.setBrowser(navigator.userAgent);
    this.authService.Sesion().subscribe(rspS => {
      if (rspS) {
        this.navi.perfiles = rspS["perfiles"];
        this.navi.calcMenu();
        this.navi.Menu().subscribe(rspM => {
          this.Menu = this.orderBy.transform(rspM["menu"], "orden", false);
          this.selMenu = rspM["selMenu"];
          this.genMenu = rspM["menuGeneral"];
        });
      }
    });
  }

  aLog() {
    this.router.navigate(["/login"]);
  }

  chGenMenu(idGM) {
    if (this.selMenu.id !== idGM) {
      this.navi.calcMenu(idGM);
      console.log(this.Menu);
    }
  }
}
