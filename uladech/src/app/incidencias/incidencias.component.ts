import { Component, OnInit } from "@angular/core";
import { AuthService } from "../recursos/auth.service";
import { MathService } from "../recursos/math.service";
import { DataService } from "../recursos/data.service";
import * as moment from "moment";
import { FormControl } from "@angular/forms";
import { MatDialog } from "@angular/material";
import { DialAsistenciaComponent } from "../controles/asistencia/asistencia.component";

@Component({
  selector: "app-incidencias",
  templateUrl: "./incidencias.component.html",
  styleUrls: ["./incidencias.component.scss"]
})
export class IncidenciasComponent implements OnInit {
  sesion;
  perfilOk;
  tipo = 0;
  hoy = moment();
  selDia = moment();
  establecimientos;
  selEstablecimiento;
  estados = [
    { estado: -2, estilo: '', descripcion: 'todos', marcado: true },
    { estado: 0, estilo: 'marca1', descripcion: 'asistiendo', marcado: false },
    { estado: 1, estilo: 'marca2', descripcion: 'cumplido', marcado: false },
    { estado: 2, estilo: 'marca3', descripcion: 'retraso leve', marcado: false },
    { estado: 3, estilo: 'marca4', descripcion: 'con incidencia resuelta', marcado: false },
    { estado: 4, estilo: 'marca5', descripcion: 'con incidencia no resuelta', marcado: false },
    { estado: 5, estilo: 'marca6', descripcion: 'requiere intervención de RRHH', marcado: false },
  ];
  selEstado = -2;
  asistencias;
  cargandoAsistencia=true;
  controles;

  constructor(
    private auth: AuthService,
    private math: MathService,
    private data: DataService,
    private dialog: MatDialog
  ) { }

  ngOnInit() {
    this.auth.sesion.subscribe(ses => {
      if (ses) {
        this.sesion = ses;
        this.perfilOk =
          this.math.Interseccion(ses["perfiles"], [5, 6, 99]).length > 0;
        if (this.perfilOk) {
          this.chDiaMes(this.selDia);
        }
      }
    });
  }

  chDiaMes(nuevoDia: moment.Moment) {
    this.selDia = nuevoDia;
    this.cargandoAsistencia = true;
    this.data.diarioEstablecimientos(this.selDia.format('YYYY-MM-DD')).subscribe(rEst => {
      this.establecimientos = rEst || [];
      this.selEstablecimiento = (this.establecimientos[0] || null);
      this.chEstablecimiento();
      this.cargandoAsistencia=false;
    });
  }

  chEstablecimiento() {
    if (this.establecimientos && this.establecimientos.length > 0) {
      const parD = {
        dDia: this.selDia.format('YYYY-MM-DD'),
        sEstablecimiento: this.selEstablecimiento.deptid
      };
      this.cargandoAsistencia = true;
      this.data.diarioAsistencia(parD).subscribe(rAs => {
        if (rAs) {
          this.asistencias = rAs.asistencia;
          this.estados.filter(ces => ces.marcado = (ces.estado === -2));
          this.asistencias.filter(cAs => {
            cAs.flujo = rAs['flujo'].filter(cFl => cFl.iAsistencia === cAs.iAsistencia);
            this.estados.find(ces => ces.estado === cAs.iEstado).marcado = true;
          });
          this.controles = rAs.controles;
        }
        this.cargandoAsistencia = false;
      });
    } else {
      this.asistencias = [];
      this.cargandoAsistencia = false;
    }
  }

  tabularFlujo(flujo, bControl, ttab) {
    const ubicontrol = flujo.find(bct => bct.bControl === bControl);
    if (ubicontrol) {
      let rHora: moment.Moment;
      rHora = ttab === 'control' ? ubicontrol.dHoraControl : ubicontrol.dHoraHorario;
      return rHora;
    } else {
      return null;
    }
  }

  asistenciaxEstado() {
    return this.asistencias ? this.asistencias
      .filter(cas => (cas.iEstado === this.selEstado || this.selEstado === -2) &&
        cas.flujo.filter(cco => (cco.bControl <= 100 && this.tipo === 0) || (cco.bControl > 100 && this.tipo === 1)).length > 0)
      : [];
  }

  estadosValidos() {
    return this.estados.filter(ces => ces.marcado);
  }

  diarioEmpleado(asist) {
    const dD: moment.Moment = moment(asist.dFecha);
    const pde = { iEmpleado: asist.iContrato, dDia: dD.format("YYYY-MM-DD") };
    const dialogAsist = this.dialog.open(DialAsistenciaComponent, { data: { parDia: pde }, width: '660px', height: '400px' });
    dialogAsist.afterClosed()
      .subscribe(rAsist => {
        this.chDiaMes(this.selDia);
      });
  }

  iControles() {
    return this.controles ? this.controles.filter(cco => (cco.bControl <= 100 && this.tipo === 0) || (cco.bControl > 100 && this.tipo === 1)) : [];
  }

}


