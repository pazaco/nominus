import { DataService } from "./../recursos/data.service";
import { MathService } from "./../recursos/math.service";
import { AuthService } from "./../recursos/auth.service";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-mensajes",
  templateUrl: "./mensajes.component.html",
  styleUrls: ["./mensajes.component.scss"]
})
export class MensajesComponent implements OnInit {
  sesion;
  perfilOk;

  constructor(
    private auth: AuthService,
    private math: MathService,
    private data: DataService
  ) {}

  ngOnInit() {
    this.auth.sesion.subscribe(ses => {
      if (ses) {
        this.sesion = ses;
        this.perfilOk =
          this.math.Interseccion(ses["perfiles"], [5, 6, 99]).length > 0;
      }
    });
  }
}
