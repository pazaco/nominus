import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { DataService } from "src/app/recursos/data.service";
import { FormControl } from "@angular/forms";
import { Observable } from "rxjs";
import { debounceTime } from "rxjs/operators";

@Component({
  selector: "nom-select",
  templateUrl: "./select.component.html",
  styleUrls: ["./select.component.scss"]
})
export class SelectComponent implements OnInit {
  @Input("lista") xLista: string;
  @Input("token") gToken;
  @Input("mostrar") xMostrar: string[];
  @Input("cantidad") iCantidad: number;
  @Input("anexos") oAnexos: any;
  @Input("clave") xClave;
  @Output("seleccion") evtSeleccion: EventEmitter<any> = new EventEmitter();

  selClick;
  ctrl = new FormControl();
  items;

  constructor(private data: DataService) { }

  ngOnInit() {
    this.ctrl.valueChanges
      .pipe(debounceTime(600))
      .subscribe(xCad => {
        if (this.iCantidad || this.iCantidad === 0) {
          this.iCantidad = 12;
        }
        let oCad: any;
        if (this.oAnexos) {
          oCad = this.oAnexos;
        } else {
          oCad = {
            idle: null
          };
        }
        this.selClick = null;
        let api: Observable<any>;
        switch (this.xLista) {
          case "personas": {
            oCad.xNombreParcial = xCad;
            oCad.bCantidad = this.iCantidad;
            api = this.data.personas(oCad);
            break;
          }
        }
        api.subscribe(lst => {
          this.items = lst;
          this.items.filter(cit => {
            cit.mostrar = "";
            cit.derecha = "";
            cit.abajo = "";
            this.xMostrar.forEach(cti => {
              if (cti.substring(0, 1) === "-") {
                cit.derecha = cit[cti.substring(1)];
              } else {
                cit.mostrar += cit[cti] + " ";
              }
            });
            cit.mostrar = cit.mostrar.trim();
            if (this.xClave) {
              const vClave =
                this.xClave.substring(0, 1) === "#"
                  ? this.xClave.substring(1)
                  : this.xClave;
              cit.clave = cit[vClave];
            }
          });
          if (this.items.length === 1) {
            this.evtSeleccion.emit(this.items[0][this.xClave]);
          } else {
            this.evtSeleccion.emit(null);
          }
        });
      });
  }

  borrarSelector() {
    this.ctrl.setValue("");
  }

  selDesdeLista(selItem) {
    if (this.xClave.substring(0, 1) === "#") {
      this.selClick = selItem.clave || null;
    }
  }
}
