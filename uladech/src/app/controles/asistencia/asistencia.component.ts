import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { DataService } from 'src/app/recursos/data.service';
import * as moment from "moment";
import { ToastrService } from 'ngx-toastr';
import { FormControl } from '@angular/forms';
import { debounceTime, filter } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
import { AuthService } from 'src/app/recursos/auth.service';

export interface AsistenciaData {
  parDia?: any;
  aMarcacion?: any[];
  oAsistencia?: any;
}

@Component({
  selector: 'dial-asistencia',
  templateUrl: './asistencia.component.html',
  styleUrls: ['./asistencia.component.scss']
})
export class DialAsistenciaComponent implements OnInit {
  nvaMarca = new FormControl('');
  _diario = new BehaviorSubject<any>(this.diario);
  diario$ = this._diario.asObservable();
  tiposIncidencia = [
    { bTipoIncidencia: 0, xTipoIncidencia: "Seleccione el tipo de incidencia...", xDescripcion: "" },
    { bTipoIncidencia: 1, xTipoIncidencia: "Ausencia injustificada", xDescripcion: "Marque las entradas correspondientes a la(s) jornada(s) inasistidas" },
    { bTipoIncidencia: 2, xTipoIncidencia: "Horas Extras", xDescripcion: "Marque la hora de salida adecuada frente a cada jornada, que refleje con exactitud la cantidad de horas extras autorizadas." }
  ];
  selTipoIncidencia = 0;
  usuario;
  cargando = false;

  constructor(
    public dialogRef: MatDialogRef<DialAsistenciaComponent>,
    @Inject(MAT_DIALOG_DATA) public diario: AsistenciaData,
    private data: DataService,
    private auth: AuthService,
    private toastr: ToastrService) { }

  async ngOnInit() {
    this.auth.sesion.subscribe(rs => { this.usuario = rs; });
    if (this.diario.parDia) {
      this.cargando = true;
      await this.data.diarioEmpleado(this.diario.parDia).toPromise().then(rsp => {
        this.diario = rsp;
        this.prepararDiario();
      });
    }
    this.nvaMarca.valueChanges
      .pipe(debounceTime(700))
      .subscribe(rsp => {
        if (rsp.length === 6) {
          const nueva = `${this.diario.oAsistencia.dFecha.substring(0, 10)}T${rsp.substr(0, 2)}:${rsp.substr(2, 2)}:${rsp.substr(4, 2)}Z`;
          const parDiaUsr = { iEmpleado: this.diario.oAsistencia.iContrato, dDia: nueva, bDispositivo: 4, cEstablecimiento: "0000" };
          console.log(parDiaUsr);
          this.data.agregaMarca(parDiaUsr).subscribe((rDi: any) => {
            this.diario = rDi;
            console.log(this.diario);
            this._diario.next(this.diario);
          });
          this.nvaMarca.setValue('');
        }
      });
  }

  prepararDiario() {
    this.diario.oAsistencia.aFlujo.filter(cfl => {
      cfl.marcaAusente = false;
    });
    this._diario.next(this.diario);
    this.cargando = false;
  }

  revaluar() {
    this.data
      .revaluar(this.diario.oAsistencia.iAsistencia)
      .subscribe(rsp => {
        this.data.diarioEmpleado(this.diario.parDia).subscribe(rsp2 => {
          this.diario = rsp2;
        });
        this.toastr.success(rsp);
      });
  }

  desmarcar() {
  }

  calcAusencias(): number {
    let nAusencias = 0;
    let idx = 0;
    if (this.diario.oAsistencia) {
      this.diario.oAsistencia.aFlujo.filter(cfl => {
        if (idx % 2 === 0) {
          if (!cfl.dHoraControl) {
            nAusencias++;
          }
        }
        idx++;
      });
    }
    return nAusencias;
  }

  calcAusenciasMarcadas(): number {
    let nMarcas = 0;
    if (this.diario.oAsistencia) {
      this.diario.oAsistencia.aFlujo.filter(cfl => {
        if (cfl.marcaAusente === true) {
          nMarcas++;
        }
      });
    }
    return nMarcas;
  }

  calcExtras() {
    let nExtras: number;
    nExtras = 0;
    return nExtras;
  }

  calcExtrasMarcadas(): number {
    let nMarcas: number;
    nMarcas = 1;
    // TODO:  Falta algoritmo de extras marcadas
    return nMarcas;
  }

  crearAusencia() {
    let resolucion;
    resolucion = [];
    let sConsec = 0;
    this.diario.oAsistencia.aFlujo.forEach((cfl: any) => {
      if (cfl.marcaAusente === true) {
        resolucion.push({
          bTipoPapeleta: 102,
          iPapeleta: 0,
          sConsecutivo: sConsec++,
          dFecha: moment().format("YYYY-MM-DDTHH:MM:SS"),
          iAsistencia: cfl.iAsistencia,
          bControl: cfl.bControl,
          dHoraHorario: cfl.dHoraHorario,
          dResolucion: cfl.dHoraHorario
        });
      }
    });
    const parCrea = {
      resolucion: resolucion,
      papeleta: {
        bTipoPapeleta: 102,
        iPapeleta: 0,
        sIncidencia: 4,
        dCreacion: moment().format("YYYY-MM-DDTHH:MM:SS"),
        iEmpleado: this.diario.oAsistencia.iContrato,
        iUsuario: this.usuario.uchempleado.iZkEmpleado
      },
      flujo: {
        bTipoPapeleta: 102,
        iPapeleta: 0,
        iConsecutivo: 0,
        cTipoFlujo: "G",
        iEmpleado: this.usuario.uchempleado.iZkEmpleado
      }
    };
    const parDia = {
      iEmpleado: this.diario['empleado'].iZkEmpleado,
      dDia: this.diario.oAsistencia.dFecha
    };
    this.data.papeleta(parCrea).subscribe(rsp => {
      this.data.diarioEmpleado(parDia).subscribe((rDia: any) => {
        this.diario = rDia;
        this.prepararDiario();
      });
    });
  }
}
