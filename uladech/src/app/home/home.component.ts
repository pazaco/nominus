import { DataService } from "src/app/recursos/data.service";
import { Component, OnInit, ViewChild, OnDestroy } from "@angular/core";
import { AuthService } from "../recursos/auth.service";
import { MathService } from "../recursos/math.service";
import * as moment from "moment";
import { FormControl } from "@angular/forms";
import { Subscription, interval } from "rxjs";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"]
})
export class HomeComponent implements OnInit, OnDestroy {
  sesion;
  lApiGrafico = false;
  dDiaGraf1 = new FormControl(moment());
  sesionOk = false;
  perfilOk = false;
  hoy = moment();
  gRecluta;
  gMarca = [];
  selLocal;
  selMarcado = { sEstablecimiento: null, xEstab: null, maxi: 0, turnos: [] };
  intervaloGraf = interval(20000);
  ruletee: Subscription;

  constructor(
    public auth: AuthService,
    private math: MathService,
    private data: DataService
  ) { }

  ngOnInit() {
    this.auth.sesion.subscribe(ses => {
      if (ses) {
        this.sesionOk = true;
        this.perfilOk = this.math.Interseccion(ses["perfiles"], [5, 6, 99]).length > 0;
        this.lApiGrafico = true;
        this.data.grafReclutamiento().subscribe(rec => {
          this.gRecluta = rec;
          this.calcularGrafico();
        });
        this.dDiaGraf1.valueChanges.subscribe(vchDia => {
          this.calcularGrafico();
        });
      }
    });
  }

  calcularGrafico() {
    const vchDia = this.dDiaGraf1.value;
    this.data.grafMarcacion(vchDia).subscribe(gma => {
      this.gMarca = [];
      this.selMarcado = { xEstab: null, sEstablecimiento: null, turnos: [], maxi: 0 };
      if (gma.grafMarcados && gma.grafMarcados.length > 0) {
        gma.grafMarcados.forEach(gm => {
          let marca0 = this.gMarca.find(c0 => c0.sEstablecimiento === 0);
          if (typeof marca0 === "undefined") {
            this.gMarca.push({
              sEstablecimiento: 0,
              xEstab: 'ULADECH',
              turnos: [
                {
                  cJornada: gm["cTurno"],
                  "E": { esperado: 0, marcado: 0 },
                  "S": { esperado: 0, marcado: 0 }
                }]
            });
          }
          marca0 = this.gMarca.find(c0 => c0.sEstablecimiento === 0);
          let turno0 = marca0.turnos.find(t0 => t0.cJornada === gm["cTurno"]);
          if (typeof turno0 === 'undefined') {
            marca0.turnos.push({
              cJornada: gm["cTurno"],
              "E": { esperado: 0, marcado: 0 },
              "S": { esperado: 0, marcado: 0 }
            });
            turno0 = marca0.turnos.find(t0 => t0.cJornada === gm["cTurno"]);
          }
          if (gm["cTipoControl"] === "E") {
            turno0.E.esperado += gm["esperado"] || 0;
            turno0.E.marcado += gm["marcado"] || 0;
          } else {
            turno0.S.esperado += gm["esperado"] || 0;
            turno0.S.marcado += gm["marcado"] || 0;
          }
          let marcaE = this.gMarca.find(cE => cE.sEstablecimiento === gm["sEstablecimiento"]);
          if (typeof marcaE === "undefined") {
            const kEst = gma.establecimientos.find(estab => estab.DEPTID === gm["sEstablecimiento"]);
            let xEsta: string;
            if (!kEst) {
              xEsta = '...';
            } else {
              xEsta = kEst.DEPTNAME;
            }
            this.gMarca.push({
              sEstablecimiento: gm["sEstablecimiento"],
              xEstab: xEsta,
              turnos: [
                {
                  cJornada: gm["cTurno"],
                  "E": { esperado: 0, marcado: 0 },
                  "S": { esperado: 0, marcado: 0 }
                }]
            });
          }
          marcaE = this.gMarca.find(c0 => c0.sEstablecimiento === gm["sEstablecimiento"]);
          let turnoE = marcaE.turnos.find(t0 => t0.cJornada === gm["cTurno"]);
          if (typeof turnoE === 'undefined') {
            marcaE.turnos.push({
              cJornada: gm["cTurno"],
              "E": { esperado: 0, marcado: 0 },
              "S": { esperado: 0, marcado: 0 }
            });
            turnoE = marcaE.turnos.find(t0 => t0.cJornada === gm["cTurno"]);
          }
          if (gm["cTipoControl"] === "E") {
            turnoE.E.esperado += gm["esperado"] || 0;
            turnoE.E.marcado += gm["marcado"] || 0;
          } else {
            turnoE.S.esperado += gm["esperado"] || 0;
            turnoE.S.marcado += gm["marcado"] || 0;
          }
        });
        this.selLocal = 0;
        this.ejesGrafico();
        this.ruletee = this.intervaloGraf.subscribe(ct => this.avanzar(1));
        this.lApiGrafico = false;
      }
    });

  }

  ngOnDestroy(): void {
    if (this.ruletee && !this.ruletee.closed) {
      this.ruletee.unsubscribe();
    }
  }

  avanzar(vr) {
    if (this.gMarca.length > 0) {
      this.ruletee.unsubscribe();
      if (this.selLocal + vr < 0) {
        vr = 0;
      }
      this.selLocal = (this.selLocal + vr) % this.gMarca.length;
      this.ejesGrafico();
      this.ruletee = this.intervaloGraf.subscribe(ti => this.avanzar(vr));
    }
  }

  ejesGrafico() {
    if (this.gMarca.length > 0) {
      this.selMarcado = this.gMarca[this.selLocal];
      this.selMarcado.maxi = 0;
      const ngr = this.selMarcado.turnos.length;
      this.selMarcado.turnos.forEach(ctu => {
        ctu.punto = this.selMarcado.turnos.indexOf(ctu);
        ctu.centro = (1 + ctu.punto) * 600 / (ngr + 1);
        ctu.ancho = [0.7, 0.75, 0.8, 0.85, 0.9, 0.9, 0.95][ngr] * 600 / (ngr + 1);
        ctu.color = ['#0bb', '#b0b', '#bb0', '#883'][ctu.punto];
        if ((ctu.maxi || 0) < ctu.E.esperado) { ctu.maxi = ctu.E.esperado; }
        if ((ctu.maxi) < ctu.S.esperado) { ctu.maxi = ctu.S.esperado; }
        if ((this.selMarcado.maxi) < ctu.maxi) { this.selMarcado.maxi = ctu.maxi; }
      });
    }

  }


}
