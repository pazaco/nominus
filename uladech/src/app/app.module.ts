import { BrowserModule, DomSanitizer } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { FlexLayoutModule } from "@angular/flex-layout";
import { ServiceWorkerModule } from "@angular/service-worker";
import { environment } from "../environments/environment";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import {
  MatSidenavModule,
  MatIconRegistry,
  MatIconModule,
  MatInputModule,
  MatTabsModule,
  MatButtonModule,
  MatProgressBarModule,
  MatTableModule,
  MAT_DATE_LOCALE,
  MatSelectModule,
  MatAutocompleteModule,
  MatExpansionModule,
  MatButtonToggleModule,
  MatProgressSpinnerModule,
  MatDatepickerModule,
  MAT_DATE_FORMATS,
  MatBadgeModule,
  MatTooltipModule,
  MatRippleModule,
  MatDialogModule
} from "@angular/material";
import { Height100Directive } from "./recursos/height100.directive";
import { RecursosModule } from "./recursos/recursos.module";
import { HttpClientModule } from "@angular/common/http";
import { RegistroComponent } from "./registro/registro.component";
import { HomeComponent } from "./home/home.component";
import { MensajesComponent } from "./mensajes/mensajes.component";
import { ToastrModule } from "ngx-toastr";
import { LoginComponent } from "./login/login.component";
import { AngularFireModule } from "@angular/fire";
import { AngularFireAuthModule } from "@angular/fire/auth";
import { AngularFireDatabaseModule } from "@angular/fire/database";
import { AngularFireMessagingModule } from "@angular/fire/messaging";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { IncidenciasComponent } from "./incidencias/incidencias.component";
import { EmpleadoComponent } from "./empleado/empleado.component";
import { ReglasComponent } from "./reglas/reglas.component";
import { PapeletasComponent } from "./papeletas/papeletas.component";
import { HelpComponent } from "./help/help.component";
import { JefaturaComponent } from "./jefatura/jefatura.component";
import { GeoreferenciaComponent } from "./georeferencia/georeferencia.component";
import { SistemasComponent } from "./sistemas/sistemas.component";
import { UsuariosComponent } from "./usuarios/usuarios.component";
import { CdkTableModule } from "@angular/cdk/table";
import { SelectComponent } from "./controles/select/select.component";
import { AgmCoreModule } from "@agm/core";
import { MatMomentDateModule } from "@angular/material-moment-adapter";
import { OrderByPipe, ZerosPipe, HorasPipe } from "./recursos/pipes.pipe";
import { NgxMaskModule} from "ngx-mask";
import { ScrollDispatchModule } from "@angular/cdk/scrolling";
import { AsyncPipe } from "@angular/common";
import { DialAsistenciaComponent } from './controles/asistencia/asistencia.component';

@NgModule({
  declarations: [
    OrderByPipe,
    ZerosPipe,
    HorasPipe,
    SelectComponent,
    AppComponent,
    Height100Directive,
    LoginComponent,
    HomeComponent,
    MensajesComponent,
    RegistroComponent,
    IncidenciasComponent,
    EmpleadoComponent,
    ReglasComponent,
    PapeletasComponent,
    HelpComponent,
    JefaturaComponent,
    GeoreferenciaComponent,
    SistemasComponent,
    UsuariosComponent,
    DialAsistenciaComponent
  ],
  entryComponents: [
    DialAsistenciaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ServiceWorkerModule.register("ngsw-worker.js", {
      enabled: environment.production
    }),
    AgmCoreModule.forRoot({
      apiKey: "AIzaSyB0WrKAai8wun_4NUuhVXb-tUJlc6b30as",
      libraries: ["places", "drawing", "geometry"]
    }),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    AngularFireMessagingModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    RecursosModule,
    ToastrModule.forRoot({
      positionClass: "toast-bottom-center",
      preventDuplicates: true
    }),
    FlexLayoutModule,
    MatExpansionModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatDatepickerModule,
    MatDialogModule,
    MatSidenavModule,
    MatIconModule,
    MatInputModule,
    MatTabsModule,
    MatTooltipModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatMomentDateModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatTableModule,
    MatRippleModule,
    CdkTableModule,
    MatSelectModule,
    NgxMaskModule.forRoot()
  ],
  providers: [
    OrderByPipe,
    AsyncPipe,
    { provide: MAT_DATE_LOCALE, useValue: "es-PE" },
    {
      provide: MAT_DATE_FORMATS,
      useValue: {
        parse: {
          dateInput: "L"
        },
        display: {
          dateInput: "L",
          monthYearLabel: "MMM YYYY",
          dateA11yLabel: "LL",
          monthYearA11yLabel: "MMMM YYYY"
        }
      }
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(matIconRegistry: MatIconRegistry, domSanitizer: DomSanitizer) {
    matIconRegistry.addSvgIconSet(
      domSanitizer.bypassSecurityTrustResourceUrl("./assets/mdi.svg")
    );
  }
}
