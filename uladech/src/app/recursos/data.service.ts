import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import * as moment from "moment";
import { map } from "rxjs/operators";

@Injectable()
export class DataService {
  public token;
   public api = "http://localhost:5700/";
   //public api = "https://api.nominus.pe/";

  constructor(private http: HttpClient) { }

  login(parLogin: any): Observable<any> {
    return this.http.post(this.api + "auth/firebase", parLogin, this.optSeed());
  }

  establecimientos(): Observable<any> {
    return this.http.get(this.api + "uladech/establecimientos", this.optTkn());
  }

  personas(parPersonas: any): Observable<any> {
    return this.http.post(
      this.api + "uladech/persona",
      parPersonas,
      this.optTknMin()
    );
  }

  geoEstablecimientos(geoRef): Observable<any> {
    return this.http.post(this.api + "asi/geoUbicacion", geoRef, this.optTkn());
  }

  cuentasEmpleado(cEmpleado): Observable<any> {
    return this.http.get(
      this.api + "uladech/cuentasEmpleado/" + cEmpleado,
      this.optTkn()
    );
  }

  autorizarCuentaGoogle(cuenta): Observable<any> {
    return this.http.post(
      this.api + "uladech/autorizarCuentaGoogle",
      cuenta,
      this.optTkn()
    );
  }

  delCuentaEmpleado(email): Observable<any> {
    return this.http.post(
      this.api + "uladech/delCuentaEmpleado",
      email,
      this.optTkn()
    );
  }

  geoPoligono(geoEstablecimiento): Observable<any> {
    return this.http.post(
      this.api + "asi/poligono",
      geoEstablecimiento,
      this.optTkn()
    );
  }

  addPoligono(poligono): Observable<any> {
    return this.http.post(
      this.api + "asi/addPoligono",
      poligono,
      this.optTkn()
    );
  }

  delPoligono(poligono): Observable<any> {
    return this.http.post(
      this.api + "asi/delPoligono",
      poligono,
      this.optTkn()
    );
  }

  voces(usuario): Observable<any> {
    return this.http.post(
      this.api + "voiceprint/usuario",
      usuario,
      this.optTkn()
    );
  }

  abrirVoz(frase): Observable<any> {
    return this.http.post(
      this.api + "voiceprint/abrirVoz",
      frase,
      this.optTkn()
    );
  }

  borrarVoz(voz): Observable<any> {
    return this.http.post(
      this.api + "voiceprint/borrarVoz",
      voz,
      this.optTkn()
    );
  }

  persona(cPersona): Observable<any> {
    return this.http.get(
      this.api + "uladech/empleado/" + cPersona,
      this.optTkn()
    );
  }

  reclutamiento(iPersona): Observable<any> {
    return this.http.get(
      this.api + "uladech/reclutamiento/" + iPersona,
      this.optTkn()
    );
  }

  diarioEmpleado(tDiaEmpleado): Observable<any> {
    return this.http.post(
      this.api + "asi/diarioEmpleado",
      tDiaEmpleado,
      this.optTkn()
    );
  }

  diarioEstablecimientos(tDia): Observable<any> {
    return this.http.get(
      this.api + "asi/diarioEstablecimientos/" + tDia,
      this.optTkn()
    );
  }


  diarioAsistencia(tDiaAsistencia): Observable<any> {
    return this.http.post(
      this.api + "asi/diarioAsistencia",
      tDiaAsistencia,
      this.optTkn()
    );
  }


  mensualEmpleado(tDiaEmpleado): Observable<any> {
    return this.http.post(
      this.api + "asi/mensualEmpleado",
      tDiaEmpleado,
      this.optTkn()
    );
  }

  revaluar(iAsistencia: number): Observable<any> {
    return this.http.get(
      this.api + "asi/revaluar/" + iAsistencia,
      this.optTkn()
    );
  }

  revaluarDia(dDia: moment.Moment): Observable<any> {
    return this.http.get(
      this.api + "asi/revaluar/" + dDia.format("YYYY-MM-DD"),
      this.optTkn()
    );
  }

  grafControlxEstablecimiento(dDia: moment.Moment): Observable<any> {
    return this.http.get(
      this.api + "asi/grafControlxEstablecimiento/" + dDia.format("YYYY-MM-DD"),
      this.optTkn()
    );
  }

  grafMarcacion(dDia: moment.Moment): Observable<any> {
    return this.http.get(
      this.api + "asi/grafMarcacion/" + dDia.format("YYYY-MM-DD"),
      this.optTkn()
    );
  }

  grafReclutamiento(): Observable<any> {
    return this.http.get(this.api + "asi/grafReclutamiento", this.optTkn());
  }

  agregarMarcacion(tDiaEmpleado): Observable<any> {
    return this.http.post(
      this.api + "asi/agregarMarcacion",
      tDiaEmpleado,
      this.optTkn()
    );
  }

  agregaMarca(tDiaEmpleado): Observable<any> {
    return this.http.post(
      this.api + "asi/agregaMarca",
      tDiaEmpleado,
      this.optTkn()
    );
  }

  papeleta(nvaPapeleta): Observable<any> {
    return this.http.post(
      this.api + "asi/papeleta",
      nvaPapeleta,
      this.optTkn()
    );
  }

  zkEstablecimientos(): Observable<any> {
    return this.http.get(this.api + "uladech/zkEstablecimientos", this.optTkn());
  }

  diarioIncidencias(tDiaIncidencias): Observable<any> {
    return this.http.post(
      this.api + "asi/diarioIncidencias",
      tDiaIncidencias,
      this.optTkn()
    );
  }

  indicePapeletas(iPersona): Observable<any> {
    return this.http.get(
      this.api + "asi/indicePapeletas/" + iPersona,
      this.optTkn()
    );
  }

  flujoPapeleta(parPapeleta): Observable<any> {
    return this.http.post(
      this.api + "asi/flujoPapeleta",
      parPapeleta,
      this.optTkn()
    );
  }

  resolucion(parPapeleta): Observable<any> {
    return this.http.post(
      this.api + "asi/resolucion",
      parPapeleta,
      this.optTkn()
    );
  }

  horarioDiario(tDia): Observable<any> {
    return this.http.get(
      this.api + "uladech/diario/" + tDia,
      this.optTkn()
    );
  }

  sincronizarMarcas(): Observable<any> {
    return this.http.get(
      this.api + "uladech/sincronizarMarcas",
      this.optTkn()
    );
  }

  postFile(fileToUpload: File): Observable<boolean> {
    const endpoint = 'your-destination-url';
    const formData: FormData = new FormData();
    formData.append('fileKey', fileToUpload, fileToUpload.name);
    return this.http
      .post(endpoint, formData, { headers: this.optTkn() })
      .pipe(map(r => true));
  }


  private optSeed(): any {
    return {
      headers: new HttpHeaders()
        .set("content-type", "application/json;charset=utf-8")
        .set("User-Seed", this.minutero("app.nominus.pe$$"))
        .set("token-utc", "true")
    };
  }

  private optTknMin(): any {
    return {
      headers: new HttpHeaders()
        .set("content-type", "application/json;charset=utf-8")
        .set("xTokenMin", this.minutero("asistencia-from-nominus.pe"))
        .set("token-utc", "true")
    };
  }

  private optClear(): any {
    return {
      headers: new HttpHeaders().set(
        "Content-Type",
        "application/json;charset=utf-8"
      )
    };
  }

  private optTkn(): any {
    const sHeaders = new HttpHeaders()
      .set("Content-Type", "application/json;charset=utf-8")
      .set("token", this.token);
    return { headers: sHeaders };
  }

  public minutero(sujeto): string {
    let hashPassword = "";
    const passwd = moment.utc().format("mmDDMM[Y]YY[T]mmHH");
    let hashSemilla = sujeto + passwd;
    const lpo =
      "ab9cK4fstv5^*dCDSxy7zAB#$%0&=TUV6OPZ1emnE2gQRWXYhjklLMNpqrFG3H8I@)J";
    let nLen = 0;
    let nSeed: number;
    let nPass: number;
    let pos = 1;
    let hPwd = "";
    let pChar: number;
    while (hashSemilla.length < 128) {
      hashSemilla = hashSemilla + hashSemilla;
    }
    while (hPwd.length < 128) {
      hPwd = hPwd + passwd;
    }
    while (pos <= passwd.length) {
      nLen += passwd.substr(pos - 1, 1).charCodeAt(0);
      pos++;
    }
    pos = 1;
    while (pos <= 128) {
      nSeed = lpo.indexOf(hashSemilla.substr(pos - 1, 1)) + 1;
      nPass = hPwd.substr(pos - 1, 1).charCodeAt(0);
      pChar = 1 + ((2 * nSeed + 5 * nPass + 3 * nLen) % 67);
      hashPassword += lpo.substr(pChar - 1, 1);
      pos++;
    }
    return hashPassword;
  }
}

