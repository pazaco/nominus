import { BehaviorSubject, Observable } from "rxjs";
import { Injectable } from "@angular/core";
import { MathService } from "./math.service";
import { OrderByPipe } from "./pipes.pipe";

declare var require: any;

@Injectable()
export class NavService {
  private __me = { selMenu: null, menuGeneral: [], menu: [] };
  private _me = new BehaviorSubject<any>(this.__me);
  private _m = require("../../../menubase.json").consola;
  private _fa = require("../../../menubase.json").filtro;
  public perfiles;

  constructor(private math: MathService, private orderBy: OrderByPipe) { }

  Menu(): Observable<any> {
    return this._me.asObservable();
  }

  calcMenu(seleccion?) {
    if (this.perfiles && this.perfiles.length > 0) {
      let _perfiles = [];
      this.perfiles.filter(cp => {
        if (this._fa.filter(c => c.perfil === cp).length > 0) {
          const _accp = this._fa.filter(c => c.perfil === cp)[0].accesos;
          if (_accp && _accp.length > 0) {
            _perfiles = this.math.Union(_perfiles, _accp);
          }
        }
      });
      const _mp = [];
      if (_perfiles) {
        _perfiles.filter(_p => {
          const hayM = this._m.filter(_cm => _cm.id === _p);
          if (hayM.length > 0) {
            if (_mp.filter(cmp => cmp.id === hayM[0]["id"]).length === 0) {
              _mp.push(hayM[0]);
              let _pa = hayM[0]["padre"];
              while (_pa !== null) {
                const hayPa = this._m.filter(_cm => _cm.id === _pa);
                if (hayPa.length > 0) {
                  if (_mp.filter(cmp => cmp.id === _pa).length === 0) {
                    _mp.push(hayPa[0]);
                    _pa = hayPa[0]["padre"];
                  } else {
                    _pa = null;
                  }
                } else {
                  _pa = null;
                }
              }
            }
          }
        });
      }
      this.__me.menuGeneral = this.orderBy.transform(
        _mp.filter(cmp => cmp.padre === 1),
        "id",
        false
      );
      if (seleccion) {
        this.__me.selMenu = this.__me.menuGeneral.filter(
          cmg => cmg.id === seleccion
        )[0];
      } else {
        if (
          this.__me.menuGeneral === null ||
          this.__me.menuGeneral.length === 0
        ) {
          this.__me.selMenu = null;
        } else {
          this.__me.selMenu = this.__me.menuGeneral[0];
        }
      }
      if (this.__me.selMenu) {
        this.__me.menu = _mp.filter(cmp => cmp.padre === this.__me.selMenu.id);
        this.__me.menu.filter(cme => {
          const sme = _mp.filter(cmp => cmp.padre === cme.id);
          if (sme.length > 0) {
            cme.submenu = sme;
          }
        });
      }
      this._me.next(this.__me);
    }
  }
}
