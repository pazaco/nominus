import {
  HostListener,
  Directive,
  ElementRef,
  Input,
  AfterViewInit
} from "@angular/core";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: "[height-100]"
})
export class Height100Directive implements AfterViewInit {
  @Input() footerElement = null;
  @Input() headerElement = null;
  constructor(private el: ElementRef) {}

  ngAfterViewInit(): void {
    this.calculateAndSetElementHeight();
  }

  @HostListener("window:resize", ["$event"])
  onresize() {
    this.calculateAndSetElementHeight();
  }

  private calculateAndSetElementHeight() {
    const windowHeight = window.innerHeight;
    const hTop = (this.headerElement) ? parseInt(window.getComputedStyle(this.headerElement).height, 10) : 0;
    const pTop = parseInt(window.getComputedStyle(this.el.nativeElement).paddingTop, 10);
    const mTop = parseInt(window.getComputedStyle(this.el.nativeElement).marginTop, 10);
    const hBottom = (this.footerElement) ? parseInt(window.getComputedStyle(this.footerElement).height, 10) : 0;
    const pBottom = parseInt(window.getComputedStyle(this.el.nativeElement).paddingBottom, 10);
    const mBottom = parseInt(window.getComputedStyle(this.el.nativeElement).marginBottom, 10);
    this.el.nativeElement.style.height = (windowHeight - hTop - pTop - mTop - hBottom - pBottom - mBottom) + "px";
    this.el.nativeElement.style.overflowY = 'auto';
  }

}


