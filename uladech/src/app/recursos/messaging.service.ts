import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { take } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
import { DataService } from './data.service';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../environments/environment';

@Injectable()
export class MessagingService {
  public _notificar: any[] = [];
  private __notificar = new BehaviorSubject<any[]>([]);
  private __loading = new BehaviorSubject<boolean>(false);

  contratos: any[];
  tokenSes: string;
  token: string;
  notificar = this.__notificar.asObservable();
  loading = this.__loading.asObservable();

  currentMessage = new BehaviorSubject(null);

  constructor(
    private afDB: AngularFireDatabase,
    private afAuth: AngularFireAuth,
    private afMessaging: AngularFireMessaging,
    private data: DataService,
    private toast: ToastrService) {
    this.afMessaging.messaging.subscribe(_messaging => {
      _messaging.onMessage = _messaging.onMessage.bind(_messaging);
      _messaging.onTokenRefresh = _messaging.onTokenRefresh.bind(_messaging);
    });
  }

  /**
   * update token in firebase database
   *
   * @param userId userId as a key
   * @param token token as a value
   */
  updateToken(userId, token) {
    this.afAuth.authState.pipe(take(1)).subscribe(
      () => {
        const data = {};
        data[userId] = token;
        this.afDB.object('fcmTokens/').update(data);
      });
  }

  /**
   * leer las notificaciones de los ultimos dos meses
   *
   * @param token token que representa la sesión del usuario
   */
  inicioNotificar() {
    this.__loading.next(true);
    // this.data.notificaciones(this.tokenSes).subscribe(rsp => {
    //   this._notificar = [];
    //   rsp.filter(cno => {
    //     this._notificar.push(this.completarNotif(cno));
    //   });
    //   this.__notificar.next(this._notificar);
    //   this.__loading.next(false);
    // });
  }

  /**
   * request permission for notification from firebase cloud messaging
   *
   * @param userId userId
   */
  requestPermission(userId) {
    this.afMessaging.requestToken.subscribe(
      token => {
        this.token = token;
        this.updateToken(userId, token);
      },
      err => {
        console.log("No se obtuvo permisos para notificación", err);
      }
    );
  }

  /**
   * hook method when new notification received
   */
  receiveMessage() {
    this.afMessaging.messages.subscribe(
      payload => {
        const nuevaNota = this.completarNotif(JSON.parse(payload['data']['gcm.notification.data']));
        if (environment.firebase.messagingSenderId === payload['from'] && this.esContrato(nuevaNota.sServidor, nuevaNota.iContrato)) {
          this._notificar.push(nuevaNota);
          this.__notificar.next(this._notificar);
          this.currentMessage.next(payload);
          this.toast.info(nuevaNota.xAsunto, payload["notification"]["title"]);
          this.currentMessage.next(payload);
        }
      });
  }

  private completarNotif(cnot) {
    const iconos = ['cash', 'beach', 'clock', 'bulletin-board'];
    const links = ['/boletas', '/vacaciones', '/asistencia', '/board'];
    cnot.xIcono = iconos[cnot.bTipoNotificacion - 1];
    cnot.xLink = links[cnot.bTipoNotificacion - 1];
    this.contratos.filter(c1 => {
      if (c1.sServidor === cnot.sServidor) {
        c1.empresas.filter(c2 => {
          c2.contratos.filter(c3 => {
            if (c3.iContrato === cnot.iContrato) {
              cnot.sEmpresa = c2.sEmpresa;
              cnot.jImagen = "data:image/png;base64," + c2.jImagen;
            }
          });
        });
      }
    });
    return cnot;
  }

  private esContrato(sServidor, iContrato): boolean {
    let esCtt = false;
    this.contratos.filter(c1 => {
      if (c1.sServidor === sServidor) {
        c1.empresas.filter(c2 => {
          c2.contratos.filter(c3 => {
            if (c3.iContrato === iContrato) {
              esCtt = true;
            }
          });
        });
      }
    });
    return esCtt;
  }
}
