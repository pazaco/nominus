import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { NavService } from "./nav.service";
import { MathService } from "./math.service";
import { MessagingService } from "./messaging.service";
import { DataService } from "./data.service";
import { AuthService } from "./auth.service";
import { AuthGuard } from "./auth.guard";
import { OrderByPipe, ZerosPipe, HorasPipe } from "./pipes.pipe";
import { MayusculaDirective, NumericoDirective } from "./teclado.directive";

@NgModule({
  imports: [CommonModule],
  declarations: [

    MayusculaDirective,
    NumericoDirective
  ],
  providers: [
    NavService,
    MathService,
    MessagingService,
    DataService,
    AuthService,
    AuthGuard
  ]
})
export class RecursosModule {}
