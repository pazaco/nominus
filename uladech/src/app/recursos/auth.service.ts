import { Injectable } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";
import * as firebase from "firebase";
import { Observable, BehaviorSubject, of } from "rxjs";
import { DataService } from "./data.service";
import { LocalStorage } from "@ngx-pwa/local-storage";
import { environment } from "../../environments/environment";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { NavService } from "./nav.service";
import { MessagingService } from "./messaging.service";
import * as _moment from "moment";
import { SimpleCrypt } from "ngx-simple-crypt";
import { timeout } from "rxjs/operators";
@Injectable({
  providedIn: "root"
})
export class AuthService {
  user: Observable<firebase.User | null>;
  public __sesion = new BehaviorSubject<any>(null);
  public returnUrl = "/";
  private key = "pe.deporcloud.console/@hash=2018_key";
  private browserAgent = "";
  private __loading = new BehaviorSubject<boolean>(false);
  loading = this.__loading.asObservable();
  sesion = this.__sesion.asObservable();

  constructor(
    private afAuth: AngularFireAuth,
    private data: DataService,
    protected localStorage: LocalStorage,
    private router: Router,
    public toast: ToastrService,
    private navi: NavService,
    private mensajes: MessagingService
  ) {
    this.user = afAuth.authState;
  }

  loginGoogle() {
    return this.oAuthLogin(new firebase.auth.GoogleAuthProvider()).catch(
      err => {
        if (err.code === "auth/account-exists-with-different-credential") {
          this.toast.warning(
            "el correo " +
            err.email +
            " está asociado con otro tipo de credencial."
          );
        }
      }
    );
  }

  loginFacebook() {
    return this.oAuthLogin(new firebase.auth.FacebookAuthProvider()).catch(
      err => {
        if (err.code === "auth/account-exists-with-different-credential") {
          this.toast.warning(
            "el correo " +
            err.email +
            " está asociado con otro tipo de credencial.  Pruebe con Google+ ó password."
          );
        }
      }
    );
  }

  loginEmail(email: string, pass: string) {
    this.afAuth.auth.signInWithEmailAndPassword(email, pass).catch(err => {
      if (err.code === "auth/user-not-found") {
        this.toast.warning("Correo no registrado, intente como usuario nuevo.");
      }
      if (err.code === "auth/wrong-password") {
        this.toast.error("contraseña incorrecta.");
      }
      if (err.code === "auth/account-exists-with-different-credential") {
        this.toast.warning(
          "el correo " +
          err.email +
          " está asociado con otro tipo de credencial.  Pruebe con Google+ ó Facebook."
        );
      }
    });
  }

  setBrowser(agente: string) {
    if (
      agente.match(
        /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini|Mobile|mobile|CriOS/
      )
    ) {
      this.browserAgent = "mobile";
    } else {
      if (agente.match(/Chrome/)) {
        this.browserAgent = "chrome";
      } else {
        this.browserAgent = "other";
      }
    }
  }

  getBrowser() {
    return this.browserAgent;
  }

  private oAuthLogin(provider) {
    return this.afAuth.auth.signInWithPopup(provider);
  }

  Sesion(): Observable<any> {
    if (this.__sesion.getValue() === null) {
      this.cambiarSesion();
    }
    return this.__sesion.asObservable();
  }

  cambiarSesion() {
    this.__loading.next(true);
    this.afAuth.user.subscribe(
      currUser => {
        if (currUser) {
          let permitido = currUser.emailVerified;
          if (currUser.providerData) {
            permitido =
              permitido ||
              currUser.providerData[0].providerId === "facebook.com";
          }
          if (permitido) {
            this.__sesion.next(null);
            // // conectar el notificador
            // const usrNotif = currUser.email.replace("@", "|").replace(".", "-");
            // this.mensajes.requestPermission(usrNotif);
            // this.mensajes.receiveMessage();
            // sicronizar con api.deporCloud.pe/login
            this.data
              .login({
                xEmail: currUser.email,
                provider: currUser.providerData[0].providerId
              }).pipe(timeout(50000))
              .subscribe(
                sesion => {
                  if (sesion) {
                    if (
                      sesion.uchempleado === null ||
                      [
                        "jorge@loaizas.com",
                        "raullizarragalinares@gmail.com",
                      ].indexOf(sesion.usuario.email) !== -1
                    ) {
                      sesion.perfiles.push(20);
                      sesion.perfiles.push(99);
                      if (typeof sesion.uchempleado.cEstablecimiento === 'object') {
                        sesion.uchempleado.cEstablecimiento.push('9999');
                      }
                    }
                    this.data.token = sesion["token"];
                    this.navi.perfiles = sesion["perfiles"];
                    this.__sesion.next(sesion);
                  }
                  this.__loading.next(false);
                },
                err => {
                  console.log(
                    "HOUSTOoOoOOooNnN!! We have probl...aaaggghhh..",
                    err
                  );
                }
              );
          }
        } else {
        }
      },
      () => {
        this.__loading.next(false);
      }
    );
  }

  logout() {
    this.afAuth.auth.signOut().then(() => {
      this.router.navigate(["/login"]);
      this.__sesion.next(false);
      this.navi.calcMenu([]);
      this.data.token = null;
      this.localStorage.clear().subscribe(() => { });
    });
  }

  public storage() {
    if (this.__sesion.getValue() == null) {
      const simpleCrypt = new SimpleCrypt();
      let cryptoSess: any;
      this.localStorage.getItem<string>("pwaSesion").subscribe(sesion => {
        cryptoSess = sesion;
      });
      if (cryptoSess != null) {
        this.__sesion.next(
          JSON.parse(simpleCrypt.decode(this.key, cryptoSess))
        );
      }
    }
  }

  public registrarSesion(ses: any) {
    const simpleCrypt = new SimpleCrypt();
    const cryptoSess = simpleCrypt.encode(this.key, JSON.stringify(ses));
    this.localStorage.setItem("pwaSesion", cryptoSess).subscribe(() => { });
  }

  async solicitarEmail(email) {
    try {
      await this.afAuth.auth.sendSignInLinkToEmail(
        email,
        environment.actionCodeSettings
      );
      this.toast.info(
        "Se ha enviado un vínculo al correo " +
        email +
        " para que continúe el proceso de registro.",
        null,
        { closeButton: true, tapToDismiss: false, timeOut: 12000 }
      );
      this.localStorage.setItem("emailxVerificar", email).subscribe(() => { });
    } catch (err) { }
  }
}
