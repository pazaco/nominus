import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../recursos/data.service';
import { AuthService } from '../recursos/auth.service';
import { interval, Subscription } from 'rxjs';

@Component({
  selector: 'app-georeferencia',
  templateUrl: './georeferencia.component.html',
  styleUrls: ['./georeferencia.component.scss']
})
export class GeoreferenciaComponent implements OnInit {
  gmap = { lat: -9.077390, lng: -78.589350 };
  mapZoom = 18;
  caminar;
  ajustar;
  areando = false;
  clicar;
  arear;
  areas;
  selArea;
  establecimientos;
  selEstablecimiento;
  relojUbicacion = interval(5000);
  _relojUbicacion: Subscription;

  constructor(public auth: AuthService, private data: DataService) { }

  ngOnInit() {
    this.auth.sesion.subscribe(ses => {
      if (ses) {
        this.data.establecimientos()
          .subscribe(rspEs => {
            this.establecimientos = rspEs;
            if (this.establecimientos && this.establecimientos.length > 0) {
              this.selEstablecimiento = this.establecimientos[0];
            } else {
              this.selEstablecimiento = null;
            }
            this.chEstablecimiento();
          });
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(pos => {
            this.gmap = { lat: pos.coords.latitude, lng: pos.coords.longitude };
          });
        }
      }
    });
  }

  borrarArea(poligono) {
    console.log(poligono);
    this.data.delPoligono(poligono).subscribe();
    this.areas = this.areas.filter(car => car.cEstablecimiento !== poligono.cEstablecimiento || car.sOrtogono !== poligono.sOrtogono);
  }

  caminando(poligono) {
    let camina = false;
    if (this.caminar && this.caminar.geos) {
      camina = (this.caminar.geos.filter(rsG => rsG.lMatch && poligono.sOrtogono === rsG.sOrtogono).length > 0);
    }
    return camina;
  }

  chEstablecimiento() {
    const puntoRef = this.caminar || this.clicar || this.gmap;
    const parEst = {
      establecimientos: [this.selEstablecimiento.cEstablecimiento],
      punto: { fLat: puntoRef.lat, fLon: puntoRef.lng }
    };
    this.data.geoEstablecimientos(parEst)
      .subscribe(rspE => {
        this.areas = rspE;
        this.areas.filter(poli => poli.stroke = ["#00f", "#0f0", "#f00", "#0bb", "#b0b", "#bb0"][poli.sOrtogono % 6]);
        if (this.areas && this.areas.length > 0) {
          this.selArea = this.areas[0];
        } else {
          this.selArea = null;
        }
        this.mapZoom = 18;
        this.chArea(null);
      });
  }

  cancelArear() {
    this.areando = false;
    this.arear = null;
  }
  addArea() {
    if (this.areando) {
      if (this.arear.length > 2) {
        const nPoli = {
          cEstablecimiento: this.selEstablecimiento.cEstablecimiento,
          sOrtogono: 0,
          stroke: null,
          puntos: this.arear
        };
        this.areas.filter(care => {
          if (care.sOrtogono >= nPoli.sOrtogono) {
            nPoli.sOrtogono = care.sOrtogono + 1;
          }
        });
        nPoli.stroke = ["#00f", "#0f0", "#f00", "#0bb", "#b0b", "#bb0"][nPoli.sOrtogono % 6];
        this.areas.push(nPoli);
        this.arear = null;
        this.areando = false;
        this.data.addPoligono(nPoli).subscribe();
      }
    } else {
      this.areando = true;
    }
  }

  chArea(evento) {
    if (evento) {
      this.selArea = evento;
    }
    let sLat = 0;
    let sLng = 0;
    if (this.selArea) {
      this.selArea.puntos.filter(cpt => {
        sLat += cpt.lat;
        sLng += cpt.lng;
      });
      this.gmap = { lat: sLat / this.selArea.puntos.length, lng: sLng / this.selArea.puntos.length };
    }
  }


  clickMapa($event) {
    if (this.areando) {
      if (!this.arear) {
        this.arear = [];
      }
      this.arear.push($event.coords);
    } else {
      this.clicar = $event.coords;
    }
  }


  toggleCaminar() {
    if (navigator.geolocation) {
      if (this.caminar) {
        this.caminar = null;
        this._relojUbicacion.unsubscribe();
      } else {
        this._relojUbicacion = this.relojUbicacion.subscribe(cpr => {
          navigator.geolocation.getCurrentPosition(pos => {
            this.caminar = { lat: pos.coords.latitude, lng: pos.coords.longitude };
            const parGeo = {
              establecimientos: [this.selEstablecimiento.cEstablecimiento],
              punto: { fLat: pos.coords.latitude, fLon: pos.coords.longitude }
            };
            this.data.geoEstablecimientos(parGeo)
              .subscribe(rspG => this.caminar.geos = rspG);
          });
        });
      }
    }
  }
}
