import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HomeComponent } from "./home/home.component";
import { MensajesComponent } from "./mensajes/mensajes.component";
import { HelpComponent } from "./help/help.component";
import { LoginComponent } from "./login/login.component";
import { IncidenciasComponent } from "./incidencias/incidencias.component";
import { EmpleadoComponent } from "./empleado/empleado.component";
import { ReglasComponent } from "./reglas/reglas.component";
import { PapeletasComponent } from "./papeletas/papeletas.component";
import { JefaturaComponent } from "./jefatura/jefatura.component";
import { RegistroComponent } from "./registro/registro.component";
import { GeoreferenciaComponent } from "./georeferencia/georeferencia.component";
import { SistemasComponent } from "./sistemas/sistemas.component";
import { UsuariosComponent } from "./usuarios/usuarios.component";

const routes: Routes = [
  { path: "", redirectTo: "home", pathMatch: "full" },
  { path: "home", component: HomeComponent },
  { path: "incidencias", component: IncidenciasComponent },
  { path: "empleado", component: EmpleadoComponent },
  { path: "reglas", component: ReglasComponent },
  { path: "papeletas", component: PapeletasComponent },
  { path: "mensajes", component: MensajesComponent },
  { path: "help", component: HelpComponent },
  { path: "login", component: LoginComponent },
  { path: "registro", component: RegistroComponent },
  { path: "registro/:registro", component: RegistroComponent },
  { path: "jefatura", component: JefaturaComponent },
  { path: "georeferencia", component: GeoreferenciaComponent },
  { path: "sistemas", component: SistemasComponent },
  { path: "usuarios", component: UsuariosComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
