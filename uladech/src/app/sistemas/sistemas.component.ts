import { Component, OnInit } from '@angular/core';
import { AuthService } from '../recursos/auth.service';
import { MathService } from '../recursos/math.service';
import { DataService } from '../recursos/data.service';
import { FormControl } from '@angular/forms';

import * as moment from "moment";
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-sistemas',
  templateUrl: './sistemas.component.html',
  styleUrls: ['./sistemas.component.scss']
})
export class SistemasComponent implements OnInit {
  sesion;
  dDia = moment();
  sesionOk = false;
  perfilOk = false;
  lCargaDiaria = false;


  constructor(
    public auth: AuthService,
    private math: MathService,
    private data: DataService,
    private toast: ToastrService
  ) { }

  ngOnInit() {
    this.auth.sesion.subscribe(ses => {
      if (ses) {
        this.sesionOk = true;
        this.perfilOk = this.math.Interseccion(ses["perfiles"], [99]).length > 0;
      }
    });
  }

  cargaDiario() {
    this.lCargaDiaria = true;
    this.data.horarioDiario(this.dDia.format("YYYY-MM-DD"))
      .subscribe(
        rst => {
          this.toast.info(`Horario cargado (${rst})`);
        },
        err => {
          this.toast.warning(err, "Problemas para cargar horario!");
        },
        () => {
          this.lCargaDiaria = false;
        });



  }

}
