import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { User } from 'firebase';
import { ToastrService } from 'ngx-toastr';
import { DataService } from './data.service';
import * as moment from 'moment';
import * as firebase from 'firebase/app';
import 'firebase/auth';
import { NgxIndexedDB } from 'ngx-indexed-db';
import { BehaviorSubject } from 'rxjs';
import { OrderByPipe } from '../recursos/pipes.pipe';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private usuarioSubject = new BehaviorSubject<any>(null);
  private perfilSubject = new BehaviorSubject<any>(null);
  private contratoSubject = new BehaviorSubject<any>(null);
  private tokenSubject = new BehaviorSubject<any>(null);
  usuario$ = this.usuarioSubject.asObservable();
  perfil$ = this.perfilSubject.asObservable();
  token$ = this.tokenSubject.asObservable();
  contrato$ = this.contratoSubject.asObservable();
  user: User;
  private orderBy = new OrderByPipe();

  private db = new NgxIndexedDB('nominus-session', 1);

  constructor(
    public af: AngularFireAuth,
    public router: Router,
    private toast: ToastrService,
    private data: DataService) {
    this.db.openDatabase(1, evt => {
      const objectStore = evt.currentTarget.result.createObjectStore('auth', { keyPath: 'clave' });
      objectStore.createIndex('clave', 'clave', { unique: true });
    });
    if (af) {
      af.authState.subscribe(user => {
        this.revivirUsr(user);
      });
    }
  }

  revivirUsr(user) {
    this.db.getByIndex('auth', 'clave', 'firebase:usr')
      .then(oUsr => {
        // hay buffer de usuario
        if (user) {
          // hay usuario logueado
          const uBf = JSON.parse(oUsr.data) as User;
          if (uBf.email === user.email) {
            // usuario logueado es el mismo en buffer, nada qué hacer (sesion normal)
          } else {
            // usuario logueado no concuerda con buffer, debe cargarse buffer con nueva data (sesión vencida y relogueada)
            this.borrarBufferAuth(true);
            this.cargarBufferAuth(user);
          }
          this.user = user;
        } else {
          // no hay usuario firebase en memoria,  reconstruir memoria según buffer.  (sesión vencida que despertó)
          this.user = JSON.parse(oUsr.data) as User;
          this.revivirDeBufferAuth();
        }
      })
      .catch(() => {
        // no hay buffer de usuario.
        if (user) {
          // cargar buffer para el usuario en memoria. (sesión cerrada, relogueada)
          this.db.add('auth', { clave: 'firebase:usr', data: JSON.stringify(user) }).then(() => { });
          this.cargarBufferAuth(user);
          this.user = user;
        } else {
          // (no hay sesión, llamada inválida, asegurarse de borrar el buffer.)
          this.db.count('auth').then(nregs => {
            if (nregs > 0) {
              this.borrarBufferAuth();
            }
          });
        }
      });
  }

  cargarBufferAuth(user) {
    const parLogin = {
      xRestful: 'auth/firebase',
      oBody: { xEmail: "xEmail", providerId: "providerId" },
      data: {
        xEmail: user.email,
        providerId: "google.com"
      }
    };
    this.data.httpAsync(parLogin)
      .then(fire => {
        const moYa = moment().clone();
        if (fire) {
          if (fire.token) {
            this.tokenSubject.next(fire.token);
            this.db.add('auth', { clave: 'token', valor: fire.token }).then(() => { });
          }
          if (fire.usuario) {
            this.usuarioSubject.next(fire.usuario);
            this.db.add('auth', { clave: 'usuario', valor: fire.usuario }).then(() => { });
          }
          if (fire.contratos) {
            const lstContratos = [];
            fire.contratos.forEach(oServidor => {
              oServidor.empresas.forEach(oEmpresa => {
                oEmpresa.contratos.forEach(oContrato => {
                  lstContratos.push({
                    sServidor: oServidor.sServidor,
                    sEmpresa: oEmpresa.sEmpresa,
                    iContrato: oContrato.iContrato,
                    xCargo: oContrato.xCargo,
                    dDesde: oContrato.dDesde,
                    dHasta: oContrato.dHasta,
                    cTipo: oContrato.dHasta === null ? 'Término Indefinido' : moYa.isSameOrBefore(oContrato.dHasta) ? 'Término Fijo' : 'Vencido'
                  });
                });
              });
            });
            const ordContratos = lstContratos.sort((ct1: any, ct2: any) => {
              if (ct1.cTipo >= ct1.cTipo && ct1.dDesde >= ct1.dHasta) { return 1; } else { return -1; }
            });

            const ctS = {
              contratos: ordContratos,
              default: ordContratos.length > 0 ? ordContratos[0] : null
            };
            this.contratoSubject.next(ctS);
            this.db.add('auth', { clave: 'contrato', valor: ctS }).then(() => { });
          }
          if (fire.perfiles) {
            this.db.add('auth', { clave: 'perfiles', valor: fire.perfiles }).then(() => { });
          }

        }

      });
  }

  revivirDeBufferAuth() {
    this.db.getByIndex('auth', 'clave', 'usuario')
      .then(oUsr => this.usuarioSubject.next(oUsr.data))
      .catch(() => this.usuarioSubject.next(null));
    this.db.getByIndex('auth', 'clave', 'perfil')
      .then(oUsr => this.perfilSubject.next(oUsr.data))
      .catch(() => this.perfilSubject.next(null));
    this.db.getByIndex('auth', 'clave', 'token')
      .then(oUsr => this.tokenSubject.next(oUsr.data))
      .catch(() => this.tokenSubject.next(null));
    this.db.getByIndex('auth', 'clave', 'contrato')
      .then(oUsr => this.contratoSubject.next(oUsr.data))
      .catch(() => this.contratoSubject.next(null));
  }

  borrarBufferAuth(conservarFirebase = false) {
    this.usuarioSubject.next(null);
    this.perfilSubject.next(null);
    this.contratoSubject.next(null);
    this.tokenSubject.next(null);
    if (conservarFirebase) {
      this.db.getByIndex('auth', 'clave', 'firebase:usr').then(firebaseusr => {
        this.db.clear('auth').then(() => {
          this.db.add('auth', firebaseusr).then(() => { });
        });
      });
    } else {
      this.db.clear('auth').then(() => { });
    }
  }

  async login(provider) {
    let providerAuth;
    switch (provider) {
      case 'google.com':
      case 'Google': {
        providerAuth = new firebase.auth.GoogleAuthProvider();
        break;
      }
      case 'facebook.com':
      case 'Facebook': {
        providerAuth = new firebase.auth.FacebookAuthProvider();
        break;
      }
      default: {
        providerAuth = new firebase.auth.OAuthProvider(provider);
      }
    }
    try {
      await this.af.auth
        .signInWithPopup(providerAuth)
        .then(async (rsp) => {
          this.db.add('auth', { clave: 'firebase:usr', data: JSON.stringify(rsp.user) })
            .then(() => this.revivirUsr(rsp.user))
            .catch(() => {
              this.db.update('auth', { clave: 'firebase:usr', data: JSON.stringify(rsp.user) }).then(() => { });
              this.revivirUsr(rsp.user);
            });
        })
        .catch(err => {
          if (err.code === 'auth/account-exists-with-different-credential') {
            this.toast.info('cuenta con otro tipo de credencial, no Google');
          } else if (err.code === 'auth/operation-not-allowed') {
            this.toast.warning('Autenticación con ' + provider + ' temporalmente inactiva.');
          } else {
            console.log(err);
          }
        });
    } catch (e) {
      console.log("Error!" + e.message);
    }
  }


  // async completarPerfil(user) {
  //   const oLogin = {
  //     xRestful: 'auth/firebase',
  //     iDuracion: 525600,
  //     oBody: { xEmail: 'xEmail', providerId: 'providerId' }
  //     data: { xEmail: user.xEmail, providerId: user.providerId };
  //   };
  //   await this.data.httpAsync(oLogin)
  //     .toPromise()
  //     .then(rsp => {
  //       if (rsp) {
  //         this.perfil = rsp;
  //         if ('jorge@loaizas.com pablo.neyra.n@gmail.com'.indexOf(user.email) > -1) {
  //           this.perfil['perfiles']['accesos'].push(99);
  //         }
  //       }
  //     });
  // }

  // async logout() {
  //   await this.af.auth.signOut();
  //   this.user = null;
  //   this.data.token = null;
  //   this.info = {};
  //   this.perfil = null;
  // }

  // get usrLogged(): User {
  //   return this.user;
  // }

  // get usrInfo() {
  //   return this.info;
  // }

}
