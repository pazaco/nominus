import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import * as moment from "moment";
import { SimpleCrypt } from 'ngx-simple-crypt';
import { NgxIndexedDB } from 'ngx-indexed-db';
import { _getOptionScrollPosition } from '@angular/material';
import { AuthService } from './auth.service';


@Injectable()
export class DataService {
  public api = "http://api.nominus.pe/";
  public token = null;
  private cryptSeed = 'nominus.seed';
  private simpleCrypt = new SimpleCrypt();
  private db = new NgxIndexedDB('nominus-data', 1);

  constructor(private http: HttpClient, private auth: AuthService) {
    this.db.openDatabase(1, evt => {
      const objectStore = evt.currentTarget.result.createObjectStore('data', { keyPath: 'clave' });
      objectStore.createIndex('clave', 'clave', { unique: true });
    });
    auth.token$.subscribe(nTok => this.token = nTok);
  }

  async httpAsync(conjunto: any): Promise<any> {
    if (!conjunto.iDuracion) {
      conjunto.iDuracion = 525600;
    }
    let metodo = 'GET';
    let callback: string;
    let xParams = "";
    const kBuff = {
      clave: "",
      vence: "",
      data: null
    };
    if (conjunto.xRestful) {
      if (conjunto.oBody) {
        metodo = 'POST';
        callback = conjunto.xRestful;
        const xtPar = {};
        Object.keys(conjunto.oBody).forEach(ckb => {
          xtPar[ckb] = conjunto.data[conjunto.oBody[ckb]] || null;
        });
        xParams = JSON.stringify(xtPar, Object.keys(xtPar).sort());
      } else {
        metodo = 'GET';
        let xCall = conjunto.xRestful;
        const iLlave = xCall.indexOf("{");
        if (iLlave > -1) {
          if (conjunto.data) {
            const fLlave = xCall.indexOf("}");
            const nCampo = xCall.substring(iLlave + 1, fLlave);
            if (conjunto.data && typeof conjunto.data[nCampo] !== 'undefined') {
              xCall = xCall.replace("{" + nCampo + "}", conjunto.data[nCampo]);
            } else {
              xCall = xCall.replace("{" + nCampo + "}", "");
            }
          }
        }
        callback = xCall;
      }
      const dvnc = moment().clone().add(conjunto.iDuracion, "minutes");
      kBuff.vence = dvnc.format();
      let header: any;
      if (callback.indexOf('auth/') > -1) {
        header = this.optSeed();
      } else {
        header = this.optTkn();
      }
      kBuff.clave = callback + xParams;
      let rValor;
      if (conjunto.iDuracion > 0) {
        await this.db.keys('data').then(async rs=>{
          const uRg = await this.db.getByIndex('data','clave',rs.clave).then(rs=>rs);
          if 
        });

        rValor = await this.selBuffer(kBuff.clave);
      }
      if (rValor === null) {
        {
          if (metodo === 'POST') {
            rValor = await this.http
              .post(this.api + callback, JSON.parse(xParams), header)
              .toPromise().then(hrsp => hrsp);
          } else {
            rValor = await this.http
              .get(this.api + callback, header)
              .toPromise().then(hrsp => hrsp);
          }
          if (rValor && rValor !== null && conjunto.iDuracion > 0) {
            kBuff.data = rValor;
            await this.db.add('data', kBuff).then(() => { });
          }
        }
      }
      return rValor;
    } else {
      return null;
    }
  }




  private optSeed(): any {
    return {
      headers: new HttpHeaders()
        .set("content-type", "application/json;charset=utf-8")
        .set("User-Seed", this.minutero())
    };
  }

  private optTkn(): any {
    const sHeaders = new HttpHeaders()
      .set("Content-Type", "application/json;charset=utf-8")
      .set("token", this.token);
    return { headers: sHeaders };
  }


  public minutero(): string {
    let hashPassword = "";
    const passwd = moment.utc().format("mmDDMM[Y]YY[T]mmHH");
    let hashSemilla = "app.nominus.pe$$" + passwd;
    const lpo =
      "ab9cK4fstv5^*dCDSxy7zAB#$%0&=TUV6OPZ1emnE2gQRWXYhjklLMNpqrFG3H8I@)J";
    let nLen = 0;
    let nSeed: number;
    let nPass: number;
    let pos = 1;
    let hPwd = "";
    let pChar: number;
    while (hashSemilla.length < 128) {
      hashSemilla = hashSemilla + hashSemilla;
    }
    while (hPwd.length < 128) {
      hPwd = hPwd + passwd;
    }
    while (pos <= passwd.length) {
      nLen += passwd.substr(pos - 1, 1).charCodeAt(0);
      pos++;
    }
    pos = 1;
    while (pos <= 128) {
      nSeed = lpo.indexOf(hashSemilla.substr(pos - 1, 1)) + 1;
      nPass = hPwd.substr(pos - 1, 1).charCodeAt(0);
      pChar = 1 + ((2 * nSeed + 5 * nPass + 3 * nLen) % 67);
      hashPassword += lpo.substr(pChar - 1, 1);
      pos++;
    }
    return hashPassword;
  }

}
