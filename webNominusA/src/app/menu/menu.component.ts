import { Component, Input, Output, ViewChild, ElementRef, HostListener, ViewEncapsulation, HostBinding } from '@angular/core';
import { trigger, style, state, transition, animate } from '@angular/animations';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

export const NOMINUS_MENU = [
  {
    iMenu: 1,
    xMenu: 'Principal',
    xIcono: 'home',
    xLink: '/',
    aAccesos: [0],
    selected: false
  },
  {
    iMenu: 2,
    xMenu: 'Asistencia',
    xIcono: 'nm-asistencia',
    items: [
      {
        iMenu: 201,
        xMenu: 'Control de Asistencia',
        xLink: '/asiControl',
        aAccesos: [1]
      },
      {
        iMenu: 202,
        xMenu: 'Periódico',
        xLink: '/asiPeriódico',
        aAccesos: [1]
      },
      {
        iMenu: 203,
        xMenu: 'Solicitudes',
        xLink: '/asiSolicitudes',
        aAccesos: [1]
      },
      {
        iMenu: 202,
        xMenu: 'Reclutamiento',
        xLink: '/asiReclutamiento',
        aAccesos: [1]
      }
    ]
  },
  {
    iMenu: 3,
    xMenu: 'Documentos',
    xIcono: 'nm-documentos',
    items: [
      {
        iMenu: 301,
        xMenu: "Boleta de haberes",
        xLink: '/docBoletas',
        aAccesos: [2]
      },
      {
        iMenu: 302,
        xMenu: "Boleta de Gratificación",
        xLink: '/docBoletas',
        aAccesos: [2]
      },
      {
        iMenu: 303,
        xMenu: "Boleta de CTS",
        xLink: '/docBoletas',
        aAccesos: [2]
      }
    ]
  },
  {
    iMenu: 4,
    xMenu: 'Vacaciones',
    xIcono: 'nm-vacaciones',
    xLink: '/mscVacaciones',
    aAccesos: [3]
  },
  {
    iMenu: 5,
    xMenu: 'Préstamos',
    xIcono: 'nm-prestamos',
    xLink: '/mscPrestamos',
    aAccesos: [4]
  },
  {
    iMenu: 6,
    xMenu: 'Jefatura',
    xIcono: 'nm-jefatura',
    items: [
      {
        iMenu: 601,
        xMenu: 'Horario Periódico',
        xIcono: 'nm-horperiodico',
        xLink: 'asiHorarioPeriodico',
        aAccesos: [5]
      },
      {
        iMenu: 602,
        xMenu: 'Autorizaciones',
        xIcono: 'nm-autorizacion',
        xLink: 'asiAutorizaciones',
        aAccesos: [5]
      }
    ]
  },
  {
    iMenu: 7,
    xMenu: 'Personas/Contratos',
    xIcono: 'nm-personas',
    xLink: '/perGrupo',
    aAccesos: [50, 98, 99]
  },
  {
    iMenu: 8,
    xMenu: 'Horarios',
    xIcono: 'nm-horarios',
    xLink: '/cttHorario',
    aAccesos: [50, 98, 99]
  }
];

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  animations: [trigger('trgItem',
    [
      state('open',
        style({ height: "24px"})),
      state('closed',
        style({ height: 0, overflow: 'hidden'})),
      transition('open => closed', [animate('300ms ease-in')]),
      transition('closed => open', [animate('350ms ease-out')])
    ]
  )]
})
export class MenuComponent {
  constructor(private router: Router, private auth: AuthService) {
  }

  lstMenu() {
    const mnu = NOMINUS_MENU;
    mnu.forEach(cOp => {
      if (typeof cOp['selected'] === 'undefined') {
        cOp.selected = false;
      }
    });
    return mnu;
  }

  selOpcion(evt) {
    if (evt.items) {
      evt.selected = !evt.selected;
    } else {
      console.log(evt);
    }

  }

}
