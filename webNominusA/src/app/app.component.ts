import { Component, HostListener, ViewChild, ElementRef } from '@angular/core';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  openNav = 0;
  openNotif = 0;

  @ViewChild("areaMenu", { static: false }) eMenu: ElementRef;
  @ViewChild("areaNotif", { static: false }) eNotif: ElementRef;

  @HostListener('click', ['$event'])
  onclick(evt: any) {
    if (this.openNav === 1) {
      this.openNav = 2;
    } else {
      if (!this.eMenu.nativeElement.contains(evt.target) && this.openNav === 2) {
        this.openNav = 0;
      }
    }
    if (this.openNotif === 1) {
      this.openNotif = 2;
    } else {
      if (!this.eNotif.nativeElement.contains(evt.target) && this.openNotif === 2) {
        this.openNotif = 0;
      }
    }
  }

  constructor(public auth: AuthService) { }

  clickNav() {
    this.openNav = this.openNav !== 0 ? 0 : 1;
    if (this.openNav !== 0 && this.openNotif !== 0) {
      this.openNotif = 0;
    }
  }

  clickNotif() {
    this.openNotif = this.openNotif !== 0 ? 0 : 1;
    if (this.openNotif !== 0 && this.openNav !== 0) {
      this.openNav = 0;
    }
  }

}
