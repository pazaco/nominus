﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using apiCoreNominus.Model;
using apiCoreNominus.Controllers;
using iTextSharp.text.pdf;
using iTextSharp.text;
using System.Security.Cryptography.Xml;
using Microsoft.EntityFrameworkCore.Metadata.Conventions;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace apiCoreNominus.Helpers
{
    public partial class Pdfs
    {

        public static outPdf diarioEvalRiesgo(short sServidor, short sEmpresa, string webPath, DateTime dFecha, bool isUrl = true)
        {
            string xPdf;
            string cPdf = String.Format("CV{0:00}{1:00}_{2:yyMMdd}.pdf", sServidor, sEmpresa, dFecha);
            xPdf = webPath + $"\\pdfs\\{cPdf}";
            string hPdf = $"/pdfs/{cPdf}";
            string rsPdf = "https://api.nominus.pe/pdfs/" + $"{cPdf}";
            outPdf oPdf = new outPdf { xUrl = rsPdf, xUNC = xPdf };
            using (NominusContext dbN = new NominusContext(sServidor))
            {
                if (File.Exists(xPdf))
                {
                    File.Delete(xPdf);
                }
                FileStream fs = new FileStream(xPdf, FileMode.Create);
                Document doc = new Document(PageSize.A4, 50, 30, 40, 20);
                PdfWriter wri = PdfWriter.GetInstance(doc, fs);
                doc.AddAuthor("jla/nominus.pe");
                doc.AddCreator("iTextSharp v.5.5 AGPL license");
                doc.AddTitle("Evaluación de riesgo personal");
                doc.AddTitle("Reporte diario");
                orgEmpresa _empresa = dbN.orgEmpresa.Find(sEmpresa);
                string base64Logo = dbN.orgEmpresaImagen.FirstOrDefault(c => c.sEmpresa == sEmpresa && c.bTipoImagen == 1).jImagen;

                Image imgLogo = Image.GetInstance(Convert.FromBase64String(base64Logo));

                Font fvEmp = FontFactory.GetFont("Verdana", 11, Font.BOLD);
                Font fvNormal = FontFactory.GetFont("Verdana", 9);
                Font fvBold = FontFactory.GetFont("Verdana", 9, Font.BOLD);
                Font fvSmall = FontFactory.GetFont("Verdana", 6);
                Font fxSmall = FontFactory.GetFont("Verdana", 5);
                Font faNormal = FontFactory.GetFont("Arial", 7);
                Font faBold = FontFactory.GetFont("Arial", 7, Font.BOLD);
                Font faSmall = FontFactory.GetFont("Arial", 5);

                var bgColorEval = new BaseColor[4];
                bgColorEval[1] = new BaseColor(255, 255, 255);
                bgColorEval[2] = new BaseColor(224, 244, 0);
                bgColorEval[3] = new BaseColor(255, 28, 28);
                var bgLight = new BaseColor(192, 192, 192);

                doc.Open();
                PdfPTable tTitulo = new PdfPTable(new float[] { 40, 60 }) { WidthPercentage = 100f };
                float rataImg = imgLogo.Height / imgLogo.Width;
                imgLogo.ScaleAbsolute(40 / rataImg, 40f);
                PdfPCell cEmpresaLogo = new PdfPCell(imgLogo) { Padding = 5, Border = 0 };
                tTitulo.AddCell(cEmpresaLogo);
                PdfPTable tSubs = new PdfPTable(1) { WidthPercentage = 100f };
                tSubs.AddCell(new PdfPCell(new Phrase("Evaluación de Riesgo Personal", (Font)fvEmp)) { Border = 0, HorizontalAlignment = 2 });
                tSubs.AddCell(new PdfPCell(new Phrase(String.Format("Informe del día {0:dd/MM/yyyy}", dFecha), (Font)fvNormal)) { Border = 0, HorizontalAlignment = 2 });
                tTitulo.AddCell(new PdfPCell(tSubs) { Border = 0 });
                doc.Add(tTitulo);

                IEnumerable<asiAsistencia> dtA = dbN.asiAsistencia.Where(a => a.dFecha.Date == dFecha);
                IEnumerable<qryAsiRiesgoCOVID> dt = dbN.asiRiesgoCOVID
                    .Join(dtA, q => q.iAsistencia, a => a.iAsistencia, (q, a) => new { q, a })
                    .Join(dbN.traContrato, q => q.a.iContrato, c => c.iContrato, (q, c) => new { q, c })
                    .Join(dbN.perPersona, q => q.c.iTrabajador, t => t.iPersona, (q, t) => new { q, t })
                    .Select(s => new qryAsiRiesgoCOVID
                    {
                        iAsistencia = s.q.q.q.iAsistencia,
                        fTemperatura = s.q.q.q.fTemperatura,
                        aSintomas = s.q.q.q.aSintomas,
                        bEvaluacion = s.q.q.q.bEvaluacion,
                        lContacto = s.q.q.q.lContacto,
                        lLavadoManos = s.q.q.q.lLavadoManos,
                        lTapabocas = s.q.q.q.lTapabocas,
                        lRopas = s.q.q.q.lRopas,
                        xDocIdentidad = s.t.xDocIdentidad,
                        xNombre = (s.t.xApellidoPaterno ?? "") + " " + (s.t.xApellidoMaterno ?? "") + " " + s.t.xNombres,
                        general = dbN.marRiesgoCOVID.FirstOrDefault(f => f.iContrato == s.q.c.iContrato && f.dFecha.Date <= s.q.q.a.dFecha)
                    }).Where(f => f.bEvaluacion > 0).OrderBy(o => o.xNombre);

                if (dt.Count() > 0)
                {
                    PdfPTable tDetalle = new PdfPTable(new float[] { 30f, 120f, 16f, 16f, 64f, 18f, 64f, 13f, 13f, 13f, 13f }) { WidthPercentage = 100f, SpacingBefore = 10f };
                    tDetalle.AddCell(new PdfPCell(new Phrase("Documento", (Font)fvSmall)) { Border = 0, BorderWidthBottom = 0.5f, HorizontalAlignment = 1, VerticalAlignment = Element.ALIGN_BOTTOM }); ;
                    tDetalle.AddCell(new PdfPCell(new Phrase("Nombre del trabajador", (Font)fvSmall)) { Border = 0, BorderWidthBottom = 0.5f, HorizontalAlignment = 1, VerticalAlignment = Element.ALIGN_BOTTOM });
                    tDetalle.AddCell(new PdfPCell(new Phrase("Talla", (Font)fvSmall)) { Border = 0, BorderWidthBottom = 0.5f, HorizontalAlignment = 1, VerticalAlignment = Element.ALIGN_BOTTOM });
                    tDetalle.AddCell(new PdfPCell(new Phrase("Peso", (Font)fvSmall)) { Border = 0, BorderWidthBottom = 0.5f, HorizontalAlignment = 1, VerticalAlignment = Element.ALIGN_BOTTOM });
                    tDetalle.AddCell(new PdfPCell(new Phrase("Preexistencias", (Font)fvSmall)) { Border = 0, BorderWidthBottom = 0.5f, HorizontalAlignment = 1, VerticalAlignment = Element.ALIGN_BOTTOM });
                    tDetalle.AddCell(new PdfPCell(new Phrase("Temperatura", (Font)fvSmall)) { Border = 0, Rotation = 90, VerticalAlignment = Element.ALIGN_MIDDLE, BorderWidthBottom = 0.5f });
                    tDetalle.AddCell(new PdfPCell(new Phrase("Síntomas", (Font)fvSmall)) { Border = 0, BorderWidthBottom = 0.5f, HorizontalAlignment = 1, VerticalAlignment = Element.ALIGN_BOTTOM });
                    tDetalle.AddCell(new PdfPCell(new Phrase("Contacto de Riesgo", (Font)fvSmall)) { Border = 0, Rotation = 90, VerticalAlignment = Element.ALIGN_MIDDLE, BorderWidthBottom = 0.5f });
                    tDetalle.AddCell(new PdfPCell(new Phrase("Uso de tapabocas", (Font)fvSmall)) { Border = 0, Rotation = 90, VerticalAlignment = Element.ALIGN_MIDDLE, BorderWidthBottom = 0.5f });
                    tDetalle.AddCell(new PdfPCell(new Phrase("Lavado de Manos", (Font)fvSmall)) { Border = 0, Rotation = 90, VerticalAlignment = Element.ALIGN_MIDDLE, BorderWidthBottom = 0.5f });
                    tDetalle.AddCell(new PdfPCell(new Phrase("Ropa adecuada", (Font)fvSmall)) { Border = 0, Rotation = 90, VerticalAlignment = Element.ALIGN_MIDDLE, BorderWidthBottom = 0.5f });
                    foreach (qryAsiRiesgoCOVID udt in dt)
                    {
                        tDetalle.AddCell(new PdfPCell(new Phrase(udt.xDocIdentidad, (Font)faNormal)) { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_MIDDLE, BorderColorBottom = bgLight, BorderWidthBottom = 0.5f });
                        tDetalle.AddCell(new PdfPCell(new Phrase(udt.xNombre, (Font)faNormal)) { Border = 0, BackgroundColor = bgColorEval[udt.bEvaluacion], VerticalAlignment = Element.ALIGN_MIDDLE, BorderColorBottom = bgLight, BorderWidthBottom = 0.5f });
                        tDetalle.AddCell(new PdfPCell(new Phrase(udt.general.bTalla.ToString(), (Font)faNormal)) { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_MIDDLE, BorderColorBottom = bgLight, BorderWidthBottom = 0.5f });
                        tDetalle.AddCell(new PdfPCell(new Phrase(String.Format("{0:###.0}", udt.general.fPeso), (Font)faNormal)) { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_MIDDLE, BorderColorBottom = bgLight, BorderWidthBottom = 0.5f });
                        tDetalle.AddCell(new PdfPCell(new Phrase(preExist(udt.general.aEnfermedades), (Font)faNormal)) { Border = 0, VerticalAlignment = Element.ALIGN_MIDDLE, BorderColorBottom = bgLight, BorderWidthBottom = 0.5f });
                        tDetalle.AddCell(new PdfPCell(new Phrase(String.Format("{0:##.0}", udt.fTemperatura), (Font)faNormal)) { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_MIDDLE, BackgroundColor = bgColorEval[udt.fTemperatura > 37 ? udt.bEvaluacion : 0], BorderColorBottom = bgLight, BorderWidthBottom = 0.5f });
                        tDetalle.AddCell(new PdfPCell(new Phrase(Sintomas(udt.aSintomas), (Font)faNormal)) { Border = 0, VerticalAlignment = Element.ALIGN_MIDDLE, BackgroundColor = bgColorEval[udt.aSintomas.IndexOf("-") == -1 ? udt.bEvaluacion : 0], BorderColorBottom = bgLight, BorderWidthBottom = 0.5f });
                        tDetalle.AddCell(new PdfPCell(new Phrase(udt.lContacto == null ? "*" : udt.lContacto == true ? "S" : "N", (Font)faNormal)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_MIDDLE, BackgroundColor = bgColorEval[udt.lContacto == true ? udt.bEvaluacion : 0], BorderColorBottom = bgLight, BorderWidthBottom = 0.5f });
                        tDetalle.AddCell(new PdfPCell(new Phrase(udt.lTapabocas == null ? "*" : udt.lTapabocas == true ? "S" : "N", (Font)faNormal)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_MIDDLE, BackgroundColor = bgColorEval[udt.lTapabocas == false ? udt.bEvaluacion : 0], BorderColorBottom = bgLight, BorderWidthBottom = 0.5f });
                        tDetalle.AddCell(new PdfPCell(new Phrase(udt.lLavadoManos == null ? "*" : udt.lLavadoManos == true ? "S" : "N", (Font)faNormal)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_MIDDLE, BackgroundColor = bgColorEval[udt.lLavadoManos == false ? udt.bEvaluacion : 0], BorderColorBottom = bgLight, BorderWidthBottom = 0.5f });
                        tDetalle.AddCell(new PdfPCell(new Phrase(udt.lRopas == null ? "*" : udt.lRopas == true ? "S" : "N", (Font)faNormal)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_MIDDLE, BackgroundColor = bgColorEval[udt.lRopas == false ? udt.bEvaluacion : 0], BorderColorBottom = bgLight, BorderWidthBottom = 0.5f });
                    }
                    doc.Add(tDetalle);
                }
                else
                {
                    PdfPTable tNoData = new PdfPTable(1) { WidthPercentage = 100f, SpacingBefore = 10f };
                    tNoData.AddCell(new PdfPCell(new Phrase("No hay evaluaciones en este dia", (Font)fvSmall)) { Border = 0, BorderWidthBottom = 0.5f, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_CENTER, FixedHeight = 20f });
                    doc.Add(tNoData);
                }
                doc.Close();
                wri.Close();
                fs.Close();                
            }
            return oPdf;
        }
        public class outPdf
        {
            public string xUrl { get; set; }
            public string xUNC { get; set; }
        }

        private static string preExist(String arr)
        {
            string rsp = "";
            if (arr == null || arr == "")
            {
                return "n/r";
            }
            else
            {
                if (arr.IndexOf("-") > -1)
                {
                    return "Ninguna.";
                }
                else
                {
                    rsp += arr.IndexOf("O") > -1 ? "Obesidad" : "";
                    rsp += arr.IndexOf("P") > -1 ? (rsp.Length > 0 ? ", e" : "E") + "nfermedad pulmonar crónica" : "";
                    rsp += arr.IndexOf("A") > -1 ? (rsp.Length > 0 ? ", a" : "A") + "sma" : "";
                    rsp += arr.IndexOf("D") > -1 ? (rsp.Length > 0 ? ", d" : "D") + "iabetes" : "";
                    rsp += arr.IndexOf("H") > -1 ? (rsp.Length > 0 ? ", h" : "H") + "ipertensión arterial" : "";
                    rsp += arr.IndexOf("I") > -1 ? (rsp.Length > 0 ? ", e" : "E") + "nfermedad o tratamiento inmunosupresor" : "";
                    rsp += arr.IndexOf("C") > -1 ? (rsp.Length > 0 ? ", e" : "E") + "nfermedad cardiovascular" : "";
                    rsp += arr.IndexOf("R") > -1 ? (rsp.Length > 0 ? ", i" : "I") + "nsuficiencia renal crónica" : "";
                    rsp += arr.IndexOf("N") > -1 ? (rsp.Length > 0 ? ", c" : "C") + "áncer" : "";
                }
            }
            return rsp;
        }
        private static string Sintomas(String arr)
        {
            string rsp = "";
            if (arr == null || arr == "")
            {
                return "n/r";
            }
            else
            {
                if (arr.IndexOf("-") > -1)
                {
                    return "Ningún sintoma.";
                }
                else
                {
                    rsp += arr.IndexOf("F") > -1 ? "Fiebre" : "";
                    rsp += arr.IndexOf("T") > -1 ? (rsp.Length > 0 ? ", t" : "T") + "os seca" : "";
                    rsp += arr.IndexOf("R") > -1 ? (rsp.Length > 0 ? ", d" : "D") + "ificultad para respirar" : "";
                    rsp += arr.IndexOf("N") > -1 ? (rsp.Length > 0 ? ", f" : "F") + "lujo nasal notorio" : "";
                    rsp += arr.IndexOf("G") > -1 ? (rsp.Length > 0 ? ", d" : "D") + "olor de garganta" : "";
                    rsp += arr.IndexOf("I") > -1 ? (rsp.Length > 0 ? ", d" : "D") + "olor al inhalar/exhalar" : "";
                }
            }
            return rsp;
        }

        public class qryAsiRiesgoCOVID : asiRiesgoCOVID
        {
            public string xDocIdentidad { get; set; }
            public string xNombre { get; set; }
            public marRiesgoCOVID general { get; set; }
        }
    }
}


