﻿using apiCoreNominus.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace apiCoreNominus.Helpers
{
    public class Strings
    {
        static string key { get; set; } = "->nominu$$______";

        public static string ConnectionString(string xServerName, string xDb, string xUserId, string xPasswd)
        {
            string plantilla = AppSettingConexion("plantilla");
            return string.Format(plantilla, xServerName, xDb, xUserId, xPasswd);
        }

        private static string AppSettingConexion(string clave)
        {
            // DbContextOptionsBuilder optionsBuilder = new DbContextOptionsBuilder();

            IConfigurationRoot configuration = new ConfigurationBuilder()
              .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
              .AddJsonFile("appsettings.json")
              .Build();
            return configuration.GetConnectionString(clave);
        }

        public static string GetToken(string xSujeto, short sLongitud, bool lPrevio = false, bool lUTC = false)
        {
            string xLpo = "ab9cK4fstv5^*dCDSxy7zAB#$%0&=TUV6OPZ1emnE2gQRWXYhjklLMNpqrFG3H8I@)J";
            DateTime ya = DateTime.Now.AddMinutes(lPrevio ? -1 : 0);
            if (lUTC) ya = ya.ToUniversalTime();
            string xPassword = string.Format("{0:mmddMM}Y{0:yy}T{0:mmHH}", ya);
            xSujeto += xPassword;
            string xTokenMinuto = "";
            int iLen = 0;
            int iSeed;
            int iPass;
            short sPos = 1;
            string xPwd = "";
            int iChar;
            while (xSujeto.Length < sLongitud)
            {
                xSujeto += xSujeto;
            }
            while (xPwd.Length < sLongitud)
            {
                xPwd += xPassword;
            }
            foreach (char c in xPassword)
            {
                iLen += (int)c;
            }
            while (sPos <= sLongitud)
            {
                iSeed = xLpo.IndexOf(xSujeto.Substring(sPos - 1, 1)) + 1;
                iPass = xPwd[sPos - 1];
                iChar = 1 + ((2 * iSeed + 5 * iPass + 3 * iLen) % 67);
                xTokenMinuto += xLpo.Substring(iChar - 1, 1);
                sPos++;
            }
            return xTokenMinuto;
        }


        public static string  Encrypt128(string text,string _key = null)
        {
            if (_key == null) _key = key;

            byte[] iv = new byte[16];
            byte[] array;

            using (Aes aes = Aes.Create())
            {
                aes.Key = Encoding.UTF8.GetBytes(_key);
                aes.IV = iv;
                aes.Padding = PaddingMode.PKCS7;
                aes.Mode = CipherMode.CBC;

                ICryptoTransform encryptor = aes.CreateEncryptor(aes.Key, aes.IV);

                using MemoryStream memoryStream = new MemoryStream();
                using CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, encryptor, CryptoStreamMode.Write);
                using (StreamWriter streamWriter = new StreamWriter((Stream)cryptoStream))
                {
                    streamWriter.Write(text);
                }

                array = memoryStream.ToArray();
            }
            return Convert.ToBase64String(array);
        }

        public static string EncryptMD5(string text)
        {
            using var md5 = new MD5CryptoServiceProvider();
            using var tdes = new TripleDESCryptoServiceProvider
            {
                Key = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key)),
                Mode = CipherMode.ECB,
                Padding = PaddingMode.PKCS7
            };

            using var transform = tdes.CreateEncryptor();
            byte[] textBytes = UTF8Encoding.UTF8.GetBytes(text);
            byte[] bytes = transform.TransformFinalBlock(textBytes, 0, textBytes.Length);
            return Convert.ToBase64String(bytes, 0, bytes.Length);
        }

        public static string EncryptXOR(string text, string _key = null)
        {
            byte[] bText = Encoding.Default.GetBytes(text);
            text = Encoding.UTF8.GetString(bText);
            if(_key == null)
            {
                _key = key;
            }
            var result = new StringBuilder();

            for (int c = 0; c < text.Length; c++)
                result.Append((char)((uint)text[c] ^ (uint)_key[c % _key.Length]));

            byte[] bResult = Encoding.Default.GetBytes(result.ToString());
            return Encoding.UTF8.GetString(bResult);
        }

        public static string EncryptAES(string data)
        {
            string encData = null;
            byte[][] keys = GetHashKeys(key);

            try
            {
                encData = EncryptStringToBytes_Aes(data, keys[0], keys[1]);
            }
            catch (CryptographicException) { }
            catch (ArgumentNullException) { }

            return encData;
        }

        private static byte[][] GetHashKeys(string __key)
        {
            byte[][] result = new byte[2][];
            Encoding enc = Encoding.UTF8;

            SHA256 sha2 = new SHA256CryptoServiceProvider();

            byte[] rawKey = enc.GetBytes(__key);
            byte[] rawIV = enc.GetBytes(__key);

            byte[] hashKey = sha2.ComputeHash(rawKey);
            byte[] hashIV = sha2.ComputeHash(rawIV);

            Array.Resize(ref hashIV, 16);

            result[0] = hashKey;
            result[1] = hashIV;

            return result;
        }

        private static string EncryptStringToBytes_Aes(string plainText, byte[] Key, byte[] IV)
        {
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");
            byte[] encrypted;

            using (AesManaged aesAlg = new AesManaged())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                using MemoryStream msEncrypt = new MemoryStream();
                using CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write);
                using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                {
                    swEncrypt.Write(plainText);
                }
                encrypted = msEncrypt.ToArray();
            }
            return Convert.ToBase64String(encrypted);
        }
    }

}
