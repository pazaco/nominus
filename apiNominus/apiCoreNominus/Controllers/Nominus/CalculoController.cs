﻿using apiCoreNominus.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace apiCoreNominus.Controllers
{
    [Route("calculo")]
    [ApiController]
    public class CalculoController : ControllerBase
    {

        [HttpPost("tipos")]
        public tipologia PostTvCalculo([FromBody] ParTvCalculo parCalculo)
        {
            tipologia rsp = new tipologia();
            string SesionPersona = HttpContext.Request.Headers["sesionPersona"];
            Guid? gSesionPersona = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            nomPersonalPerfilCliente perfil = Procs.PerfilSesion(gSesionPersona);
            if (perfil != null && perfil.bPerfil <= 4)
            {
                using (NominusContext dbN = new NominusContext(perfil.sCliente))
                {
                    rsp.treeviewCalculos = Procs.treeviewCalculos(dbN, parCalculo.bDeclarar, parCalculo.bTodas);
                    rsp.planillas = dbN.remPlanilla.ToList();
                    rsp.planillas.Add(new remPlanilla { bTipoPlanilla = 0, xPlanilla = "(todas)", bDeclarar = 0, bPlanilla = 0 });
                }
            }
            return rsp;
        }
        public class tipologia
        {
            public List<RemTvCalculo> treeviewCalculos { get; set; }
            public List<remPlanilla> planillas { get; set; }
        }

        [HttpPost("comboAnho")]
        public List<RemCboAno> PostComboAnho([FromBody] ParCboAno comboAnho)
        {
            List<RemCboAno> rsp = null;
            string SesionPersona = HttpContext.Request.Headers["sesionPersona"];
            Guid? gSesionPersona = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            nomPersonalPerfilCliente perfil = Procs.PerfilSesion(gSesionPersona);
            if (perfil != null && perfil.bPerfil <= 4)
            {
                using (NominusContext dbN = new NominusContext(perfil.sCliente))
                {
                    rsp = Procs.comboAnho(dbN, comboAnho);
                }
            }
            return rsp;
        }

        [HttpPost("comboMes")]
        public List<RemCboMes> PostComboMes([FromBody] ParCboMes comboMes)
        {
            List<RemCboMes> rsp = null;
            string SesionPersona = HttpContext.Request.Headers["sesionPersona"];
            Guid? gSesionPersona = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            nomPersonalPerfilCliente perfil = Procs.PerfilSesion(gSesionPersona);
            if (perfil != null && perfil.bPerfil <= 4)
            {
                using (NominusContext dbN = new NominusContext(perfil.sCliente))
                {
                    rsp = Procs.comboMes(dbN, comboMes);
                }
            }
            return rsp;
        }

        [HttpPost("comboNumero")]
        public List<RemCboNumero> PostComboNumero([FromBody] ParCboNumero comboNumero)
        {
            List<RemCboNumero> rsp = null;
            string SesionPersona = HttpContext.Request.Headers["sesionPersona"];
            Guid? gSesionPersona = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            nomPersonalPerfilCliente perfil = Procs.PerfilSesion(gSesionPersona);
            if (perfil != null && perfil.bPerfil <= 4)
            {
                using (NominusContext dbN = new NominusContext(perfil.sCliente))
                {
                    rsp = Procs.comboNumero(dbN, comboNumero);
                }
            }
            return rsp;
        }

    }
}
