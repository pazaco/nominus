﻿using apiCoreNominus.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace apiCoreNominus.Controllers
{
    [Route("notificaciones")]
    [ApiController]
    public class NotificarController : ControllerBase
    {

        [Route("notificar")]
        public List<BigWebNotificar> PostNotificar()
        {
            List<BigWebNotificar> rsp = new List<BigWebNotificar>();
            string sesionpersona = HttpContext.Request.Headers["sesionpersona"];
            Guid? gsesionpersona = null;
            if (sesionpersona != null && sesionpersona != "")
            {
                gsesionpersona = new Guid(sesionpersona);
                using (AuthContext dbA = new AuthContext())
                {
                    List<AuthServidorUsuario> _servidoresUsr = new List<AuthServidorUsuario>();
                    foreach (AuthServidorUsuario _srv in _servidoresUsr)
                    {
                        NominusContext dbD = new NominusContext(_srv.sServidor);

                        List<webNotificar> _notifs = Procs.webNotificar(_srv.iPersona,_srv.sServidor).ToList();

                        rsp.AddRange(_notifs.Select(s => new BigWebNotificar
                        {
                            sServidor = _srv.sServidor,
                            iNotificacion = s.iNotificacion,
                            dEnvio = s.dEnvio,
                            bTipoNotificacion = s.bTipoNotificacion,
                            iReferencia = s.iReferencia,
                            iContrato = s.iContrato,
                            xAsunto = s.xAsunto,
                            xFaIcono = s.xFaIcono,
                            dLeido = s.dLeido,
                            dFirmado = s.dFirmado,
                            xUrl = s.xUrl
                        }));
                    }
                }
            }
            return rsp;
        }

    }

}
