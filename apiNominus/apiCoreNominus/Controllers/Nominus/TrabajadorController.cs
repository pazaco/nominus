﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using apiCoreNominus.Model;
using System.Security.Policy;
using Microsoft.CodeAnalysis.FlowAnalysis;

namespace apiCoreNominus.Controllers
{
    [Route("tra")]
    [ApiController]
    public class TrabajadorController : ControllerBase
    {
        [HttpGet("trabajadores")]
        public List<TraTrabajador> getTrabajadores()
        {
            List<TraTrabajador> rsp = null;
            string SesionPersona = HttpContext.Request.Headers["sesionPersona"];
            Guid? gSesionPersona = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            short? sServidor = Procs.HoldingSesion(gSesionPersona);
            if (sServidor != null)
            {
                using (NominusContext dbN = new NominusContext((short)sServidor))
                {
                    rsp = Procs.GetTrabajadores(dbN);
                }
            }
            return rsp;
        }

        [HttpGet("contratos")]
        public List<TraContrato> getContratos()
        {
            List<TraContrato> rsp = null;
            string SesionPersona = HttpContext.Request.Headers["sesionPersona"];
            Guid? gSesionPersona = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            short? sServidor = Procs.HoldingSesion(gSesionPersona);
            if (sServidor != null)
            {
                using (NominusContext dbN = new NominusContext((short)sServidor))
                {
                    rsp = Procs.GetContratos(dbN);
                }
            }
            return rsp;
        }

        [HttpGet("trabajadoresVigentes/{sEmpresa}")]
        public List<TraTrabajador> getTrabajadoresVigentes(short sEmpresa)
        {
            List<TraTrabajador> rsp = null;
            string SesionPersona = HttpContext.Request.Headers["sesionPersona"];
            Guid? gSesionPersona = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            nomPersonalPerfilCliente perfil = Procs.PerfilSesion(gSesionPersona);
            if (perfil != null)
            {
                //TODO: Habilitar el filtro de iJefe (el que tiene sesionPersona)
                int? iJefe = null;

                using (NominusContext dbN = new NominusContext(perfil.sCliente))
                {
                    rsp = Procs.lstTrabajadoresVigentes(dbN,sEmpresa, iJefe);
                }
            }
            return rsp;
        }

        [HttpGet("trabajadoresSinContrato")]
        public List<TraTrabajador> getTrabajadoresSinContrato()
        {
            List<TraTrabajador> rsp = null;
            string SesionPersona = HttpContext.Request.Headers["sesionPersona"];
            Guid? gSesionPersona = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            short? sServidor = Procs.HoldingSesion(gSesionPersona);
            if (sServidor != null)
            {
                using (NominusContext dbN = new NominusContext((short)sServidor))
                {
                    rsp = Procs.GetTrabajadores(dbN).Where(f => f.contratos == null || (f.contratos!=null && !f.contratos.Where(c=>c.vigente!=null).Any() )).ToList();
                }
            }
            return rsp;
        }

        [HttpGet("trabajadoresVigentesSinJefe/{sEmpresa}")]
        public List<TraTrabajador> getTrabajadoresVigentesSinJefe(short sEmpresa)
        {
            List<TraTrabajador> rsp = null;
            string SesionPersona = HttpContext.Request.Headers["sesionPersona"];
            Guid? gSesionPersona = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            short? sServidor = Procs.HoldingSesion(gSesionPersona);
            if (sServidor != null)
            {
                using (NominusContext dbN = new NominusContext((short)sServidor))
                {
                    rsp = Procs.lstTrabajadoresVigentes(dbN,sEmpresa).Where(t => t.contratos != null && t.contratos.Where(c => c.iJefe == null).Any()).ToList();
                }
            }
            return rsp;
        }

        [HttpGet("trabajadoresPorRenovar")]
        public List<TraTrabajador> getTrabajadoresPorRenovar()
        {
            List<TraTrabajador> rsp = null;
            string SesionPersona = HttpContext.Request.Headers["sesionPersona"];
            Guid? gSesionPersona = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            short? sServidor = Procs.HoldingSesion(gSesionPersona);
            if (sServidor != null)
            {
                using (NominusContext dbN = new NominusContext((short)sServidor))
                {
                    rsp = Procs.lstTrabajadoresPorRenovar(dbN);
                }
            }
            return rsp;
        }

        [HttpPost("infoTrabajador")]
        public ParTrabajador PostInfoTrabajador(ParTrabajador pars)
        {
            ParTrabajador rsp = pars;
            string SesionPersona = HttpContext.Request.Headers["sesionPersona"];
            Guid? gSesionPersona = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            nomPersonalPerfilCliente perfil = Procs.PerfilSesion(gSesionPersona);
            if (perfil != null)
            {
                using (NominusContext dbN = new NominusContext(perfil.sCliente))
                {
                    if (pars.nuevo)
                    {
                        perPersona per = dbN.perPersona.FirstOrDefault(p => p.bTipoDocIdentidad == pars.trabajador.persona.bTipoDocIdentidad && p.xDocIdentidad.Trim() == pars.trabajador.persona.xDocIdentidad.Trim());
                        if (per == null)
                        {
                            pars.trabajador.persona.dFechaReg = DateTime.Now;
                            pars.trabajador.persona.iUsuarioReg = 0;
                            dbN.perPersona.Add(pars.trabajador.persona);
                            dbN.SaveChanges();
                            pars.trabajador.iTrabajador = pars.trabajador.persona.iPersona;
                            pars.trabajador.auth.iUsuario = pars.trabajador.persona.iPersona;
                            dbN.traTrabajador.Add(pars.trabajador);
                            dbN.autUsuario.Add(pars.trabajador.auth);
                            dbN.SaveChanges();
                            rsp.estado = "nuevo";
                        }
                        else
                        {
                            if (dbN.traTrabajador.Any(t => t.iTrabajador == per.iPersona))
                            {
                                rsp.estado = "Ya Existe un trabajador con " + pars.trabajador.xTipoDocIdentidad + " " + per.xDocIdentidad + " (" + per.xApellidoPaterno + " " + per.xApellidoPaterno ?? "" + " " + per.xNombres + ")";
                            }
                            else
                            {
                                per.xApellidoMaterno = pars.trabajador.persona.xApellidoMaterno;
                                per.xApellidoPaterno = pars.trabajador.persona.xApellidoMaterno;
                                per.xNombres = pars.trabajador.persona.xNombres;
                                per.bGenero = pars.trabajador.persona.bGenero;
                                per.dNacimiento = pars.trabajador.persona.dNacimiento;
                                dbN.Entry(per).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                                pars.trabajador.iTrabajador = per.iPersona;
                                dbN.traTrabajador.Add(pars.trabajador);
                                dbN.SaveChanges();
                                pars.trabajador.auth.iUsuario = per.iPersona;
                                if (dbN.autUsuario.Any(ss => ss.iUsuario == per.iPersona))
                                {
                                    dbN.Entry(pars.trabajador.auth).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                                }
                                else
                                {
                                    dbN.autUsuario.Add(pars.trabajador.auth);
                                }
                                dbN.SaveChanges();
                                rsp.estado = "nuevo";
                            }
                        }
                    }
                    else
                    {
                        dbN.Entry(pars.trabajador.persona).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                        traTrabajador tra = pars.trabajador;
                        dbN.Entry(tra).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                        pars.trabajador.auth.iUsuario = tra.iTrabajador;
                        if (dbN.autUsuario.Any(ss => ss.iUsuario == tra.iTrabajador))
                        {
                            dbN.Entry(pars.trabajador.auth).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                        }
                        else
                        {
                            dbN.autUsuario.Add(pars.trabajador.auth);
                        }
                        dbN.SaveChanges();
                        rsp.estado = "modificado";
                    }
                }
            }
            return rsp;
        }
        
        [HttpGet("contratosTrabajador/{iTrabajador}")]
        public dynamic GetContratosTrabajador(int iTrabajador)
        {
            dynamic rsp = null;
            string SesionPersona = HttpContext.Request.Headers["sesionPersona"];
            Guid? gSesionPersona = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            nomPersonalPerfilCliente perfil = Procs.PerfilSesion(gSesionPersona);
            if (perfil != null)
            {
                using (NominusContext dbN = new NominusContext(perfil.sCliente))
                {

                    rsp = Procs.GetContratosTrabajador(dbN, iTrabajador);
                }
            }
            return rsp;
        }
        public class ParTrabajador
        {
            public TraTrabajador trabajador { get; set; }
            public bool nuevo { get; set; }
            public string estado { get; set; }
        }

        [HttpGet("tipologiaRHuContratados/{sEmpresa}")]
        public RhuContratados getTiposRhuContratados(short sEmpresa)
        {
            RhuContratados rsp = new RhuContratados();
            string SesionPersona = HttpContext.Request.Headers["sesionPersona"];
            Guid? gSesionPersona = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            short? sServidor = Procs.HoldingSesion(gSesionPersona);
            if (sServidor != null)
            {
                using (NominusContext dbN = new NominusContext((short)sServidor))
                {
                    List<TraContratoPeriodo> _periodos = new List<TraContratoPeriodo>();
                    _periodos.Add(new TraContratoPeriodo
                    {
                        iContrato = 9999999,
                        iContratoPeriodo = 9999999,
                        dDesde = DateTime.Now.Date,
                        bFormato = 1,
                        formato = new TraContratoFormato { bFormato = 1, bTipoContrato = 1, xFormato = "Indefinido" },
                        xTipoContrato = "Indefinido"

                    });
                    rsp.tipoDocIdentidad = dbN.isoTipoDocIdentidad.ToList();
                    rsp.cargos = dbN.orgCargo.OrderBy(o=>o.sCargo).ToList();
                    rsp.motivosCese = dbN.isoMotivoCese.OrderBy(o=>o.xMotivoCese).ToList();
                    rsp.sedes = dbN.orgSede.OrderBy(o=>o.xSede).ToList();
                    rsp.jefes = Procs.lstTrabajadoresVigentes(dbN,sEmpresa)
                        .Select(s => new TraJefe
                        {
                            iJefe = s.iTrabajador,
                            xJefe = s.persona.xApellidoPaterno + " " + (s.persona.xApellidoMaterno ?? "") +"  " + s.persona.xNombres
                        }).OrderBy(o=>o.xJefe).ToList();
                    rsp.formatos = dbN.traContratoFormato
                        .Select(cf => new TraContratoFormato
                        {
                            bFormato = cf.bFormato,
                            xFormato = cf.xFormato,
                            bTipoContrato = cf.bTipoContrato
                        }).OrderBy(o=>o.xFormato).ToList();
                    rsp.contratoBase = new TraContrato
                    {
                        bCategoria = 1,
                        bCategoriaOcupacional = 3,
                        bHorarioNoc = false,
                        bJornadaTrabMax = false,
                        bPeriodicidadRem = 1,
                        bPlanilla = 1,
                        bRegAtipicoDeJornadaTrab = false,
                        bRentas5taExoneradas = false,
                        bSindicalizado = false,
                        bSituacion = 11,
                        bSituacionEspecial = 0,
                        bTipoPago = 2,
                        bTipoTrabajador = 21,
                        periodos = _periodos,
                        sEmpresa = sEmpresa,
                        vigente = _periodos.First(),
                        iContrato = 9999999,                        
                    };
                }
            }
            return rsp;


        }
        public class RhuContratados
        {
            public List<isoTipoDocIdentidad> tipoDocIdentidad { get; set; }
            public TraContrato contratoBase { get; set; }
            public List<TraContratoFormato> formatos { get; set; }
            public List<orgCargo> cargos { get; set; }
            public List<orgSede> sedes { get; set; }
            public List<isoMotivoCese> motivosCese { get; set; }
            public List<TraJefe> jefes { get; set; }
        }
        public class TraJefe
        {
            public int iJefe { get; set; }
            public string xJefe { get; set; }
        }
    }
}
