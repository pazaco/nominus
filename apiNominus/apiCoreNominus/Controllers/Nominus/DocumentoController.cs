﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using apiCoreNominus.Helpers;
using apiCoreNominus.Model;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace apiCoreNominus.Controllers
{ 
   
    [ApiController, Route("docs")]
    public class DocumentosController : ControllerBase
    {

        private IWebHostEnvironment webHost;

        public DocumentosController(IWebHostEnvironment environment)
        {
            webHost = environment;
        }

        [HttpGet("indice")]
        public async Task<List<BigBelCalculos>> IndiceBoletas()
        {
            AuthContext dbA = new AuthContext();
            List<BigBelCalculos> rsp = new List<BigBelCalculos>();
            string SesionPersona = HttpContext.Request.Headers["sesionPersona"];
            Guid? gSesionPersona = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            authSesion _ses = dbA.authSesion.Find(gSesionPersona);
            if (_ses != null)
            {
                short _sServ = _ses.sServidor ?? 0;
                if (_sServ != 0)
                {
                    NominusContext dbD = new NominusContext(_sServ);
                    traContratoPeriodo _tcp = dbD.traContratoPeriodo.Find(_ses.iContrato);
                    traContrato _tcc = dbD.traContrato.Find(_tcp.iContrato);
                    var calculos = Procs.belCalculosPersona(_tcc.iTrabajador,_sServ).ToList();
                    rsp.AddRange(calculos.Select(s => new BigBelCalculos
                    {
                        sServidor = _sServ,
                        iCalculoPlanilla = s.iCalculoPlanilla,
                        iContrato = s.iContrato,
                        iPeriodo = s.iPeriodo,
                        iAnoMes = s.iAnoMes,
                        bMes = s.bMes,
                        bNumero = s.bNumero,
                        bBEL = s.bBEL,
                        xCalculo = s.xCalculo
                    }));
                }
            }
            await Task.Delay(1);
            return rsp;
        }

        [HttpPost("boleta")]
        public async Task<DataBoleta> PostDataBoletaAsync([FromBody] ParCabecera parCabecera)
        {
            AuthContext dbA = new AuthContext();
            string SesionPersona = HttpContext.Request.Headers["sesionPersona"];
            Guid? gSesionPersona = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            authServidor _srv = dbA.authServidor.Find(parCabecera.sServidor);
            NominusContext db = new NominusContext(_srv.sServidor);
            remCalculoPlanilla calculo = db.remCalculoPlanilla.Find(parCabecera.iCalculoPlanilla);
            traContrato contrato = db.traContrato.Find(calculo.iContrato);
            DataBoleta datas = new DataBoleta();
            webNotificacion wNot = db.webNotificacion.FirstOrDefault(c => c.iReferencia == parCabecera.iCalculoPlanilla && c.bTipoNotificacion == 1);
            if (wNot != null)
            {
                webFirma wFir = db.webFirma.FirstOrDefault(c => c.iNotificacion == wNot.iNotificacion && c.bRol == 1);
                if (wFir != null && wFir.dLeido == null)
                {
                    wFir.dLeido = DateTime.Now;
                    db.Entry(wFir).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
            datas.cabecera = Procs.remBoletaCabecera(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero, (short)parCabecera.sServidor);
            datas.asistencia = Procs.remBoletaCabeceraAsistenciaFechas(parCabecera.bMes, parCabecera.bNumero, parCabecera.iCalculoPlanilla, (short)parCabecera.sServidor);
            datas.datosCol1 = Procs.remBoletaCabeceraDatos(1, parCabecera.iCalculoPlanilla, (short)parCabecera.sServidor);
            datas.datosCol2 = Procs.remBoletaCabeceraDatos(2, parCabecera.iCalculoPlanilla, (short)parCabecera.sServidor);
            datas.empresa = Procs.remBoletaCabeceraEmpresa(contrato.sEmpresa, (short)parCabecera.sServidor);
            datas.titulo = Procs.remBoletaCabeceraTitulo(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero, (short)parCabecera.sServidor);
            datas.concepto1 = Procs.remBoletaConcepto(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero, 1, (short)parCabecera.sServidor);
            datas.concepto2 = Procs.remBoletaConcepto(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero, 2, (short)parCabecera.sServidor);
            datas.concepto3 = Procs.remBoletaConcepto(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero, 3, (short)parCabecera.sServidor);
            datas.concepto4 = Procs.remBoletaConcepto(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero, 4, (short)parCabecera.sServidor);
            datas.pdf = Pdfs.Boleta(datas, parCabecera, _srv.sServidor, webHost.ContentRootPath, true);
            await Task.Delay(1);
            return datas;
        }


    }
}