﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using apiCoreNominus.Model;
using Org.BouncyCastle.Asn1.Pkcs;
using Namotion.Reflection;

namespace apiCoreNominus.Controllers
{

    [Route("per")]
    [ApiController]
    public class PersonaController : ControllerBase
    {

        [HttpPost, Route("buscaPersona")]
        public List<perPersona> buscaPersona([FromBody] ParBuscaPersona parBusca)
        {
            List<perPersona> rsp = null;
            string sesionpersona = HttpContext.Request.Headers["sesionpersona"];
            if (sesionpersona != null && sesionpersona != "")
            {
                PerNavegante perfil = Procs.PerPerfil(new Guid(sesionpersona));
                if (perfil.esAdmin || perfil.esJefe || perfil.esSuper)
                {
                    int _iUsr = perfil.iUsuario ?? 0;
                    if (perfil.esSuper) { _iUsr = 999999999; }
                    rsp = Procs.PerBuscarPersonas(parBusca, _iUsr);
                }
            }
            return rsp;
        }

        [HttpPost, Route("personasUsuario")]
        public List<PerPersonaUsuario> personasUsuario([FromBody] ParBuscaPersona parBusca)
        {
            List<PerPersonaUsuario> rsp = null;
            string sesionpersona = HttpContext.Request.Headers["sesionpersona"];
            if (sesionpersona != null && sesionpersona != "")
            {
                PerNavegante perfil = Procs.PerPerfilServidor(new Guid(sesionpersona), parBusca.sServidor);
                if (perfil.esAdmin || perfil.esJefe || perfil.esSuper)
                {
                    int _iUsr = perfil.iUsuario ?? 0;
                    if (perfil.esSuper) { _iUsr = 999999999; }
                    List<perPersona> _pers = Procs.PerBuscarPersonas(parBusca, _iUsr);
                    if (_pers != null)
                    {
                        List<USERINFO> _usri = new List<USERINFO>();
                        using (MarcaZkContext dbZ = new MarcaZkContext(parBusca.sServidor))
                        {
                            try
                            {
                                _usri = dbZ.USERINFO.ToList();
                            }
                            catch
                            {
                                _usri = new List<USERINFO>();
                            }
                        }

                        List<authAcceso> _aacc = new List<authAcceso>();
                        using (AuthContext dbA = new AuthContext())
                        {
                            try
                            {
                                _aacc = dbA.authAcceso.Where(c => c.sServidor == parBusca.sServidor).ToList();
                            }
                            catch
                            {
                                // nada
                            }
                        }
                        List<autUsuario> _ausu = new List<autUsuario>();
                        using (NominusContext dbN = new NominusContext(parBusca.sServidor))
                        {
                            _ausu = dbN.autUsuario.ToList();
                        }
                        rsp = new List<PerPersonaUsuario>();
                        foreach (perPersona _per in _pers)
                        {
                            PerPersonaUsuario _pus = new PerPersonaUsuario
                            {
                                iPersona = _per.iPersona,
                                bTipoDocIdentidad = _per.bTipoDocIdentidad,
                                xDocIdentidad = _per.xDocIdentidad,
                                xApellidoPaterno = _per.xApellidoPaterno,
                                xApellidoMaterno = _per.xApellidoMaterno,
                                xNombres = _per.xNombres,
                                bGenero = _per.bGenero,
                                dNacimiento = _per.dNacimiento,
                                xConcatenado = _per.xConcatenado,
                            };
                            autUsuario _usua = _ausu.FirstOrDefault(c => c.iUsuario == _per.iPersona);
                            if (_usua != null)
                            {
                                _pus.xEmail = _usua.xEmail;
                            }
                            authAcceso _acce = _aacc.FirstOrDefault(c => c.iPersona == _per.iPersona && c.sServidor == parBusca.sServidor);
                            if (_acce != null)
                            {
                                _pus.gUsuario = _acce.gUsuario;
                            }
                            else
                            {
                                if (_usua != null)
                                {
                                    _pus.gUsuario = Procs.crearAuth(_usua.xEmail, _per.iPersona, parBusca.sServidor);
                                }
                            }
                            USERINFO _uzk = _usri.FirstOrDefault(c => c.Badgenumber == _per.iPersona.ToString() || c.Badgenumber == _per.xDocIdentidad);
                            if (_uzk != null)
                            {
                                _pus.zkUser = _uzk.USERID;
                            }

                            rsp.Add(_pus);
                        }
                    }
                }
            }
            return rsp;
        }

    }
    

}

