﻿using apiCoreNominus.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace apiCoreNominus.Controllers.Nominus
{
    [Route("buscar")]
    [ApiController]
    public class BuscadorController : ControllerBase
    {
        [HttpPost("trabajador")]
        public dynamic findTrabajadores(ParBuscar buscar)
        {
            dynamic rsp = null;
            string SesionPersona = HttpContext.Request.Headers["sesionPersona"];
            Guid? gSesionPersona = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            short? sServidor = Procs.HoldingSesion(gSesionPersona);
            if (sServidor != null)
            {
                using (NominusContext dbN = new NominusContext((short)sServidor))
                {
                    IEnumerable<string> lstBuscar = buscar.xBuscar
                        .ToLower()
                        .Replace("á", "a").Replace("é", "e")
                        .Replace("í", "i").Replace("ó", "o")
                        .Replace("ú", "u").Replace("ü", "u")
                        .Replace("ñ", "n").Replace("ç", "c").Split(" ").AsEnumerable();
                    rsp = Procs.GetTrabajadores(dbN)
                        .Select(s => new PerPatron
                        {
                            iCodigo = s.iTrabajador,
                            xDisplay = s.persona.xApellidoPaterno.Trim() + " " + (s.persona.xApellidoMaterno != null ? s.persona.xApellidoMaterno.Trim() + " " : "") + s.persona.xNombres.Trim(),
                            xGris = s.contratos != null ? s.contratos.OrderBy(o => o.iContrato).Last().xCargo : null,
                            iMatch = lstBuscar.Where(c => (
                                        s.persona.xApellidoPaterno
                                            + " " + (s.persona.xApellidoMaterno != null ? s.persona.xApellidoMaterno + " " : "")
                                            + s.persona.xNombres
                                            + " " + (s.auth != null ? s.auth.xEmail : ""
                                        //                                        + " " + (s.contratos != null ? s.contratos.OrderBy(o => o.iContrato).Last().xCargo + " " + s.contratos.OrderBy(o => o.iContrato).Last().xSede + " " + s.contratos.OrderBy(o => o.iContrato).Last().xEmpresa : "")
                                        //                    + " " + s.contratos.First(c => c.vigente != null).xSede + " " + s.contratos.First(c => c.vigente != null).xEmpresa : null)                                            
                                        )).ToLower()
                                    .Replace("á", "a").Replace("é", "e")
                                    .Replace("í", "i").Replace("ó", "o")
                                    .Replace("ú", "u").Replace("ü", "u")
                                    .Replace("ñ", "n").Replace("ç", "c").Contains(c) || c == "*").Count(),
                        }).Where(s => s.iMatch == lstBuscar.Count() || lstBuscar.Any(a => a == "*")).OrderByDescending(o => o.iMatch).ThenBy(n => n.xDisplay);
                }
            }
            return rsp;
        }
    }

    public class PerPatron
    {
        public int iCodigo { get; set; }
        public string xDisplay { get; set; }
        public string xGris { get; set; }
        public int iMatch { get; set; }
    }

    public class ParBuscar
    {
        public short? sEmpresa { get; set; }
        public string xBuscar { get; set; }
    }
}
