﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using apiCoreNominus.Model;

namespace apiCoreNominus.Controllers.Nominus
{
    [Route("org")]
    [ApiController]
    public class OrganigramaController : ControllerBase
    {
        [HttpPost("empresa/appConfig")]
        public void appConfig([FromBody] ParAppConfig config)
        {
            string SesionPersona = HttpContext.Request.Headers["sesionPersona"];
            Guid? gSesionPersona = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            short? sServidor = Procs.HoldingSesion(gSesionPersona);
            if (sServidor != null)
            {
                using (NominusContext dbN = new NominusContext((short)sServidor))
                {
                    orgEmpresa _emp = dbN.orgEmpresa.Find(config.sEmpresa);
                    _emp.jAppConfig = config.jAppConfig;
                    dbN.Entry(_emp).State = EntityState.Modified;
                    dbN.SaveChanges();
                }
            }
        }
    }

    public class ParAppConfig
    {
        public short sEmpresa { get; set; }
        public string jAppConfig { get; set; }
    }
}
