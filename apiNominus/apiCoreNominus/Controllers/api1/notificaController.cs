﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using apiCoreNominus.Model;
using Microsoft.AspNetCore.Mvc;

namespace apiCoreNominus.Controllers
{

    [Route("notificaciones")]
    [ApiController]
    public class notificaController : ControllerBase
    {
        //[Route("calculos")]
        //public async Task<List<belCalculosxPeriodo>> GetBelCalculosxPeriodos()
        //{

        //    NominusContext db = new NominusContext(_srv.sServidor);

        //    string SesionPersona = HttpContext.Request.Headers["sesionPersona"];
        //    string SesionGestor = HttpContext.Request.Headers["sesionGestor"];
        //    Guid? gSesionPersona = null;
        //    Guid? gSesionGestor = null;
        //    if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
        //    if (SesionGestor != null && SesionGestor != "") gSesionGestor = new Guid(SesionGestor);
        //    return Procs.belCalculosxPeriodo((Guid)gSesionPersona)).ToList();
        //}

        [HttpGet("notificar2")]
        public async Task<List<BigWebNotificar>> PostNotificar()
        {
            AuthContext dbA = new AuthContext();
            List<BigWebNotificar> rsp = new List<BigWebNotificar>();
            string SesionPersona = HttpContext.Request.Headers["sesionPersona"];
            Guid? gSesionPersona = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            List<AuthServidorUsuario> _servidoresUsr = Procs.authServidoresUsuario((Guid)gSesionPersona);
            foreach (AuthServidorUsuario _srv in _servidoresUsr)
            {
                NominusContext dbD = new NominusContext(_srv.sServidor);

                List<webNotificar> _notifs = Procs.webNotificar(_srv.iPersona,_srv.sServidor);

                rsp.AddRange(_notifs.Select(s => new BigWebNotificar
                {
                    sServidor = _srv.sServidor,
                    iNotificacion = s.iNotificacion,
                    dEnvio = s.dEnvio,
                    bTipoNotificacion = s.bTipoNotificacion,
                    iReferencia = s.iReferencia,
                    iContrato = s.iContrato,
                    xAsunto = s.xAsunto,
                    xFaIcono = s.xFaIcono,
                    dLeido = s.dLeido,
                    dFirmado = s.dFirmado,
                    xUrl = s.xUrl
                }));
            }
            await Task.Delay(1);
            return rsp;
        }

    }

    public class BigWebNotificar : webNotificar
    {
        public short sServidor { get; set; }
    }

}
