﻿using System.Collections.Generic;
using System.Threading.Tasks;
using apiCoreNominus.Model;
using Microsoft.AspNetCore.Mvc;
using apiCoreNominus.Helpers;

namespace apiCoreNominus.Controllers
{
    [Route("warehouse")]
    [ApiController]
    public class WarehouseController : ControllerBase
    {
        [HttpGet("servidores/{iPersona}")]
        public async Task<List<authServidor>> GetServidoresPersona(int iPersona) => await WareHouse.ServerxPersona(iPersona);

        [HttpPost("servidores")]
        public async Task<List<authServidor>> PostServidoresEmail([FromBody] string xEmail) => await WareHouse.ServerxEmail(xEmail);

        [HttpPost("contratos")]
        public async Task<List<aut_Contratos>> PosContratos([FromBody] string xEmail) => await WareHouse.Contratos(xEmail);
    }
}
