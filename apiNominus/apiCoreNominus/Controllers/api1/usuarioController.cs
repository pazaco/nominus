﻿using apiCoreNominus.Model;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace apiCoreNominus.Controllers
{
    [Route("usuarios")]
    [ApiController]
    public class usuarioController : ControllerBase
    {
        [HttpGet("buscarUsuario/{cadena}")]
        public async Task<List<perPersona>> BuscarUsuarioAsync(string cadena)
        {
            List<perPersona> rsp = null;
            AuthContext dbA = new AuthContext();
            string SesionPersona = HttpContext.Request.Headers["sesionPersona"];
            string xServidor = HttpContext.Request.Headers["selServidor"];
            short sServidor;
            if (short.TryParse(xServidor, out sServidor))
            {
                        rsp = Procs.buscarPersonas(cadena,sServidor).ToList();
            }
            await Task.Delay(1);
            return rsp;
        }

    }
}
