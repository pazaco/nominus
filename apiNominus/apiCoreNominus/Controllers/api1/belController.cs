﻿using apiCoreNominus.Helpers;
using apiCoreNominus.Model;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace apiCoreNominus.Controllers
{
    [Route("bel")]
    [ApiController]
    public class BelController : ControllerBase
    {

        private IWebHostEnvironment webHost;

        public BelController(IWebHostEnvironment environment)
        {
            webHost = environment;
        }

        [HttpGet("indice")]
        public async Task<List<BigBelCalculos>> getIndiceCalculosAsync()
        {
            AuthContext dbA = new AuthContext();
            List<BigBelCalculos> rsp = new List<BigBelCalculos>();
            string SesionPersona = HttpContext.Request.Headers["sesionPersona"];
            Guid? gSesionPersona = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            List<AuthServidorUsuario> _servidoresUsr = Procs.authServidoresUsuario((Guid)gSesionPersona);
            foreach (AuthServidorUsuario _srv in _servidoresUsr)
            {
                NominusContext dbD = new NominusContext(_srv.sServidor);

                var calculos = Procs.belCalculosPersona(_srv.iPersona, _srv.sServidor).ToList();

                rsp.AddRange(calculos.Select(s => new BigBelCalculos
                {
                    sServidor = _srv.sServidor,
                    iCalculoPlanilla = s.iCalculoPlanilla,
                    iContrato = s.iContrato,
                    iPeriodo = s.iPeriodo,
                    iAnoMes = s.iAnoMes,
                    bMes = s.bMes,
                    bNumero = s.bNumero,
                    bBEL = s.bBEL,
                    xCalculo = s.xCalculo
                }));
            }
            await Task.Delay(1);
            return rsp;
        }

        [HttpPost("data")]
        public async Task<DataBoleta> PostDataBoletaAsync([FromBody] ParCabecera parCabecera)
        {
            AuthContext dbA = new AuthContext();
            string SesionPersona = HttpContext.Request.Headers["sesionPersona"];
            Guid? gSesionPersona = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            authServidor _srv = dbA.authServidor.Find(parCabecera.sServidor);
            NominusContext db = new NominusContext(_srv.sServidor);
            remCalculoPlanilla calculo = db.remCalculoPlanilla.Find(parCabecera.iCalculoPlanilla);
            traContrato contrato = db.traContrato.Find(calculo.iContrato);
            DataBoleta datas = new DataBoleta();
            webNotificacion wNot = db.webNotificacion.FirstOrDefault(c => c.iReferencia == parCabecera.iCalculoPlanilla && c.bTipoNotificacion == 1);
            if (wNot != null)
            {
                webFirma wFir = db.webFirma.FirstOrDefault(c => c.iNotificacion == wNot.iNotificacion && c.bRol == 1);
                if (wFir != null && wFir.dLeido == null)
                {
                    wFir.dLeido = DateTime.Now;
                    db.Entry(wFir).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
            datas.cabecera = Procs.remBoletaCabecera(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero, (short)parCabecera.sServidor);
            datas.asistencia = Procs.remBoletaCabeceraAsistenciaFechas(parCabecera.bMes, parCabecera.bNumero, parCabecera.iCalculoPlanilla, (short)parCabecera.sServidor);
            datas.datosCol1 = Procs.remBoletaCabeceraDatos(1, parCabecera.iCalculoPlanilla, (short)parCabecera.sServidor);
            datas.datosCol2 = Procs.remBoletaCabeceraDatos(2, parCabecera.iCalculoPlanilla, (short)parCabecera.sServidor);
            datas.empresa = Procs.remBoletaCabeceraEmpresa(contrato.sEmpresa, (short)parCabecera.sServidor);
            datas.titulo = Procs.remBoletaCabeceraTitulo(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero, (short)parCabecera.sServidor);
            datas.concepto1 = Procs.remBoletaConcepto(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero, 1, (short)parCabecera.sServidor);
            datas.concepto2 = Procs.remBoletaConcepto(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero, 2, (short)parCabecera.sServidor);
            datas.concepto3 = Procs.remBoletaConcepto(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero, 3, (short)parCabecera.sServidor);
            datas.concepto4 = Procs.remBoletaConcepto(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero, 4, (short)parCabecera.sServidor);
            datas.pdf = Pdfs.Boleta(datas, parCabecera, _srv.sServidor, webHost.ContentRootPath, true);
            await Task.Delay(1);
            return datas;
        }


        [HttpGet("boletas/{bMes}")]
        public void dataBoletas(byte bMes)
        {
            string SesionPersona = HttpContext.Request.Headers["sesionPersona"];
            Guid? gSesionPersona = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            short? sServidor = Procs.HoldingSesion(gSesionPersona);
            if (sServidor != null)
            {
                using (NominusContext db = new NominusContext((short)sServidor))
                {
                    List<ParCabecera> bels = db.remCalculoPlanilla.AsEnumerable()
                        .Join(db.remPeriodo.Where(f => f.bMes == bMes && f.sAno==2020 && f.bCalculo==2), pl => pl.iPeriodo, rp => rp.iPeriodo, (pl, pr) => new ParCabecera
                        {
                            sServidor = sServidor,
                            bMes = bMes,
                            bNumero = pr.bNumero,
                            iCalculoPlanilla = pl.iCalculoPlanilla                            
                        }).ToList();
                    foreach (ParCabecera parCabecera in bels)
                    {
                        if (!db.belPDFs.Any(ps => ps.iCalculoPlanilla == parCabecera.iCalculoPlanilla))
                        {
                            remCalculoPlanilla calculo = db.remCalculoPlanilla.Find(parCabecera.iCalculoPlanilla);
                            traContrato contrato = db.traContrato.Find(calculo.iContrato);
                            DataBoleta datas = new DataBoleta();
                            webNotificacion wNot = db.webNotificacion.FirstOrDefault(c => c.iReferencia == parCabecera.iCalculoPlanilla && c.bTipoNotificacion == 1);
                            if (wNot != null)
                            {
                                webFirma wFir = db.webFirma.FirstOrDefault(c => c.iNotificacion == wNot.iNotificacion && c.bRol == 1);
                                if (wFir != null && wFir.dLeido == null)
                                {
                                    wFir.dLeido = DateTime.Now;
                                    db.Entry(wFir).State = EntityState.Modified;
                                    db.SaveChanges();
                                }
                            }
                            datas.cabecera = Procs.remBoletaCabecera(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero, (short)parCabecera.sServidor);
                            datas.asistencia = Procs.remBoletaCabeceraAsistenciaFechas(parCabecera.bMes, parCabecera.bNumero, parCabecera.iCalculoPlanilla, (short)parCabecera.sServidor);
                            datas.datosCol1 = Procs.remBoletaCabeceraDatos(1, parCabecera.iCalculoPlanilla, (short)parCabecera.sServidor);
                            datas.datosCol2 = Procs.remBoletaCabeceraDatos(2, parCabecera.iCalculoPlanilla, (short)parCabecera.sServidor);
                            datas.empresa = Procs.remBoletaCabeceraEmpresa(contrato.sEmpresa, (short)parCabecera.sServidor);
                            datas.titulo = Procs.remBoletaCabeceraTitulo(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero, (short)parCabecera.sServidor);
                            datas.concepto1 = Procs.remBoletaConcepto(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero, 1, (short)parCabecera.sServidor);
                            datas.concepto2 = Procs.remBoletaConcepto(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero, 2, (short)parCabecera.sServidor);
                            datas.concepto3 = Procs.remBoletaConcepto(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero, 3, (short)parCabecera.sServidor);
                            datas.concepto4 = Procs.remBoletaConcepto(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero, 4, (short)parCabecera.sServidor);
                            datas.pdf = Pdfs.Boleta(datas, parCabecera, (short)parCabecera.sServidor, webHost.ContentRootPath, true);
                        }
                    }
                }
            }
        }


        //        AuthContext dbA = new AuthContext();
        //string SesionPersona = HttpContext.Request.Headers["sesionPersona"];
        //Guid? gSesionPersona = null;
        //if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
        //authServidor _srv = dbA.authServidor.Find(parCabecera.sServidor);
        //NominusContext db = new NominusContext(_srv.sServidor);
        //remCalculoPlanilla calculo = db.remCalculoPlanilla.Find(parCabecera.iCalculoPlanilla);
        //traContrato contrato = db.traContrato.Find(calculo.iContrato);
        //DataBoleta datas = new DataBoleta();
        //webNotificacion wNot = db.webNotificacion.FirstOrDefault(c => c.iReferencia == parCabecera.iCalculoPlanilla && c.bTipoNotificacion == 1);
        //if (wNot != null)
        //{
        //    webFirma wFir = db.webFirma.FirstOrDefault(c => c.iNotificacion == wNot.iNotificacion && c.bRol == 1);
        //    if (wFir != null && wFir.dLeido == null)
        //    {
        //        wFir.dLeido = DateTime.Now;
        //        db.Entry(wFir).State = EntityState.Modified;
        //        db.SaveChanges();
        //    }
        //}
        //datas.cabecera = Procs.remBoletaCabecera(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero, (short)parCabecera.sServidor);
        //datas.asistencia = Procs.remBoletaCabeceraAsistenciaFechas(parCabecera.bMes, parCabecera.bNumero, parCabecera.iCalculoPlanilla, (short)parCabecera.sServidor);
        //datas.datosCol1 = Procs.remBoletaCabeceraDatos(1, parCabecera.iCalculoPlanilla, (short)parCabecera.sServidor);
        //datas.datosCol2 = Procs.remBoletaCabeceraDatos(2, parCabecera.iCalculoPlanilla, (short)parCabecera.sServidor);
        //datas.empresa = Procs.remBoletaCabeceraEmpresa(contrato.sEmpresa, (short)parCabecera.sServidor);
        //datas.titulo = Procs.remBoletaCabeceraTitulo(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero, (short)parCabecera.sServidor);
        //datas.concepto1 = Procs.remBoletaConcepto(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero, 1, (short)parCabecera.sServidor);
        //datas.concepto2 = Procs.remBoletaConcepto(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero, 2, (short)parCabecera.sServidor);
        //datas.concepto3 = Procs.remBoletaConcepto(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero, 3, (short)parCabecera.sServidor);
        //datas.concepto4 = Procs.remBoletaConcepto(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero, 4, (short)parCabecera.sServidor);
        //datas.pdf = Pdfs.Boleta(datas, parCabecera, _srv.sServidor, webHost.ContentRootPath, true);
    }


    public class TuplaContrato
    {
        public int iContratoPeriodo { get; set; }
        public short sServidor { get; set; }
    }
    public class ParCabecera
    {
        public byte bMes { get; set; }
        public byte bNumero { get; set; }
        public int iCalculoPlanilla { get; set; }
        public Nullable<short> sServidor { get; set; }
    }

    public class ParBoletaConcepto : ParCabecera
    {
        public byte bClaseConcepto { get; set; }
    }

    public class ParCabeceraDatos
    {
        public byte bColumna { get; set; }
        public int iCalculoPlanilla { get; set; }
    }

    public class IndiceBoletas
    {
        public List<traContratosxPersona> contratos { get; set; }
        public List<belCalculos> calculos { get; set; }
    }

    public class DataBoleta
    {
        public List<remBoletaCabecera> cabecera { get; set; }
        public List<remBoletaCabeceraAsistenciaFechas> asistencia { get; set; }
        public List<remBoletaCabeceraDatos> datosCol1 { get; set; }
        public List<remBoletaCabeceraDatos> datosCol2 { get; set; }
        public List<remBoletaCabeceraEmpresa> empresa { get; set; }
        public List<remBoletaCabeceraTitulo> titulo { get; set; }
        public List<remBoletaConcepto> concepto1 { get; set; }
        public List<remBoletaConcepto> concepto2 { get; set; }
        public List<remBoletaConcepto> concepto3 { get; set; }
        public List<remBoletaConcepto> concepto4 { get; set; }
        public outBoleta pdf { get; set; }
    }

}

