﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using apiCoreNominus.Model;

namespace apiCoreNominus.Controllers
{
    [Route("zkMaestros")]
    [ApiController]
    public class MarcadoresController : ControllerBase
    {

        [HttpGet("departamentos/{sServidor}")]
        public List<DEPARTMENTS> GetZkDepartamentos(int sServidor)
        {
            List<DEPARTMENTS> rsp = null;
            using (AuthContext dbA = new AuthContext())
            {
                authServidor _serv = dbA.authServidor.Where(c => c.sServidor == sServidor).FirstOrDefault();
                if (_serv != null)
                {
                    using (MarcaZkContext dbZ = new MarcaZkContext(_serv.sServidor))
                    {
                        rsp = dbZ.DEPARTMENTS.ToList();
                    }
                }
                return rsp;
            }
        }

        [HttpGet("dispositivos")]
        public List<Machines> GetZkMachines()
        {
            List<Machines> rsp = null;
            string SesionPersona = HttpContext.Request.Headers["sesionPersona"];
            Guid? gSesionPersona = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            short? sServidor = Procs.HoldingSesion(gSesionPersona);
            if (sServidor != null)
            {
                {
                    using (MarcaZkContext dbZ = new MarcaZkContext((short)sServidor))
                    {
                        rsp = dbZ.Machines.ToList();
                    }
                }
            }
            return rsp;
        }
    }

    [Route("rptMarcacion")]
    [ApiController]
    public class RptMarcacionController : ControllerBase
    {
        [HttpPost("listaMarcaciones")]
        public List<acc_monitor_log> ListaMarcaciones([FromBody] ParPeriodoPersona ppp)
        {
            List<acc_monitor_log> rsp = null;
            string SesionPersona = HttpContext.Request.Headers["sesionPersona"];
            Guid? gSesionPersona = null;
            if (SesionPersona != null && SesionPersona != "")
            {
                gSesionPersona = new Guid(SesionPersona);
            }
            using (AuthContext dbA = new AuthContext())
            {
                //TODO preguntar si el gSesionPersona puede consultar datos de la ppp.iPersona
                authServidor _srv = dbA.authServidor.Where(c => c.sServidor == ppp.sServidor).FirstOrDefault();
                if (_srv != null)
                {
                    using (NominusContext dbN = new NominusContext(_srv.sServidor))
                    {
                        using (MarcaZkContext dbZ = new MarcaZkContext(_srv.sServidor))
                        {
                            // aqui va todo lo que quieras hacer contra la instancia de nominus que ya tu creaste, teniendo acceso a zk
                            rsp = dbZ.acc_monitor_log.Where(c => c.pin == ppp.iPersona.ToString() && (c.time >= ppp.rango.dDesde || ppp.rango.dDesde == null) && (c.time <= ppp.rango.dHasta || ppp.rango.dHasta == null)).ToList();
                        }
                    }
                }
            }
            return rsp;
        }

        [HttpPost("misMarcaciones")]
        public List<acc_monitor_log> MisMarcaciones([FromBody]  ParPeriodoPersona ppp)
        {
            List<acc_monitor_log> rsp = null;
            string SesionPersona = HttpContext.Request.Headers["sesionPersona"];
            Guid? gSesionPersona = null;
            if (SesionPersona != null && SesionPersona != "")
            {
                gSesionPersona = new Guid(SesionPersona);
            }
            using (AuthContext dbA = new AuthContext())
            {
                authSesion _ses = dbA.authSesion.Where(c => c.gToken == gSesionPersona).FirstOrDefault();
                authAcceso _acc = dbA.authAcceso.Where(c => c.gUsuario == _ses.gUsuario).FirstOrDefault();
                authServidor _srv = dbA.authServidor.Where(c => c.sServidor == _acc.sServidor).FirstOrDefault();
                if (_ses != null && _srv != null)
                {
                    using (NominusContext dbN = new NominusContext(_srv.sServidor))
                    {
                        using (MarcaZkContext dbZ = new MarcaZkContext(_srv.sServidor))
                        {
                            // aqui va todo lo que quieras hacer contra la instancia de nominus que ya tu creaste, teniendo acceso a zk
                            rsp = dbZ.acc_monitor_log.Where(c => c.pin == _acc.iPersona.ToString() && (c.time >= ppp.rango.dDesde || ppp.rango.dDesde == null) && (c.time <= ppp.rango.dHasta || ppp.rango.dHasta == null)).ToList();
                        }
                    }
                }
            }


            return rsp;
        }

    }

    [Route("clocks")]
    [ApiController]
    public class ZkClocks : ControllerBase
    {
        [HttpGet("usuarios")]
        public void GetUsuarios() {

        }
    }


    public class ParPeriodoPersona
    {
        public short? sServidor { get; set; }
        public int? iPersona { get; set; }
        public RangoFechas rango { get; set; }
    }

}
