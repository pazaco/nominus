﻿using apiCoreNominus.Helpers;
using apiCoreNominus.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace apiCoreNominus.Controllers
{
    [Route("general")]
    [ApiController]
    public class ConexionController : ControllerBase
    {
        // GET: general/conexiones/1
        [HttpGet("conexiones/{sCliente}")]
        
        public async Task<List<GenBaseConexion>> Conexiones(byte sCliente)
        {
            string xSeed = HttpContext.Request.Headers["User-Seed"];
            string xUTC = HttpContext.Request.Headers["token-utc"];
            bool lUTC = (xUTC == "true");
            string xMin = Strings.GetToken("app.nominus.pe$$", 128, lUTC: lUTC);
            if (xMin != xSeed)
            {
                xMin = Strings.GetToken("app.nominus.pe$$", 128, lPrevio: true, lUTC: lUTC);
                if (xMin != xSeed && xSeed == "q6e98B@NZ=cN6VeeJvJPEXlZXYHTHXHY&BJNO=aNcB6IgLqTcJvzSHXDHHJHlaVNZ=)NaBcNe^643HnE2XSTXYJvqCp6a=aN)BaNt9&V1eTZKJ2CSHl8qT3X$f6NaB")
                {
                    xMin = xSeed;
                }
            }
            List<GenBaseConexion> rsp = null;
            if (xMin == xSeed)
            {
                using (GeneralContext dg = new GeneralContext())
                {
                    List<nomConexion> _cnn = await dg.nomConexion.Where(c => c.sCliente == sCliente).ToListAsync();
                    List<nomTipoDb> _tdb = await dg.nomTipoDb.ToListAsync();
                    List<nomCredencial> _crd = await dg.nomCredencial.ToListAsync();
                    rsp = _cnn
                        .Join(_tdb, cn => cn.bTipoDb, cd => cd.bTipoDb, (cn, cd) => new { cn, cd })
                        .Join(_crd, j1 => j1.cn.sCredencial, cc => cc.sCredencial, (j1, cc) => new GenBaseConexion
                        {
                            bTipoDb = j1.cd.bTipoDb,
                            xTipoDb = j1.cd.xTipoDb,
                            xServerName = cc.xServerName,
                            xDb = cc.xPrefijo + "_" + j1.cn.xNombreDb,
                            xUserId = cc.xPrefijo + "_" + cc.xUserId,
                            bModeloSeguro = cc.bModeloSeguro ?? 0
                        }).ToList();
                }
            }
            return rsp;
        }

        [HttpGet("ping")]
        public DateTime Ping()
        {
            return DateTime.UtcNow;
        }

        [HttpPost, Route("conexion")]
        public GenStringConexion Conexion([FromBody] TuplaClienteTipoDb clienteTipoDb)
        {
            string xSeed = HttpContext.Request.Headers["User-Seed"];
            string xUTC = HttpContext.Request.Headers["token-utc"];
            bool lUTC = (xUTC == "true");
            string xMin = Strings.GetToken("app.nominus.pe$$", 128, lUTC: lUTC);
            if (xMin != xSeed)
            {
                xMin = Strings.GetToken("app.nominus.pe$$", 128, lPrevio: true, lUTC: lUTC);
                if (xMin != xSeed && xSeed == "q6e98B@NZ=cN6VeeJvJPEXlZXYHTHXHY&BJNO=aNcB6IgLqTcJvzSHXDHHJHlaVNZ=)NaBcNe^643HnE2XSTXYJvqCp6a=aN)BaNt9&V1eTZKJ2CSHl8qT3X$f6NaB")
                {
                    xMin = xSeed;
                }
            }
            if (xMin == xSeed)
            {
                return Procs.Conexion(clienteTipoDb);
            }
            else return null;
        }

    }
}
