﻿using apiCoreNominus.Helpers;
using apiCoreNominus.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;


namespace apiCoreNominus.Controllers.General
{
    [Route("general")]
    [ApiController]
    public class GeneralController : ControllerBase
    { 

        [HttpGet("primaAfp")]
        public string getPrimaAFP()
        {
            return WareHouse.GetPrimasAFP();
        }



    }
}