﻿using apiCoreNominus.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;


namespace apiCoreNominus.Controllers.General
{
    [Route("builder")]
    [ApiController]
    public class BuilderController : ControllerBase
    {
        [HttpGet("{cBuilder}")]
        public dynamic Builder(string cBuilder)
        {
            string xSesionpersona = HttpContext.Request.Headers["sesionpersona"];
            string xCliente = HttpContext.Request.Headers["cliente"];
            Guid? gsesionpersona = null;
            short sCliente = 0;
            if (xSesionpersona != null && xSesionpersona != "") gsesionpersona = new Guid(xSesionpersona);
            if (gsesionpersona != null)
            {
                bool lHayCliente = short.TryParse(xCliente, out sCliente);
                using (GeneralContext dg = new GeneralContext())
                {
                    string jBuild = null;
                    nomBuilder oBuilder = dg.nomBuilder.FirstOrDefault(c => c.cBuilder == cBuilder);
                    if (oBuilder != null)
                    {
                        if (sCliente > 0 && dg.nomBuilderCliente.Any(c => c.iBuilder == oBuilder.iBuilder && c.sCliente == sCliente))
                        {
                            jBuild = dg.nomBuilderCliente.FirstOrDefault(c => c.iBuilder == oBuilder.iBuilder && c.sCliente == sCliente).jBuilder;
                        }
                        else
                        {
                            jBuild = oBuilder.jBuilder;
                        }
                        return JsonConvert.DeserializeObject(jBuild);
                    }
                    else return null;
                }
            }
            else return null;
        }

        [HttpGet("excepciones/{cBuilder}")]
        public List<nomCliente> BuilderExc(string cBuilder)
        {
            List<nomCliente> resp = new List<nomCliente>();
            string xSesionpersona = HttpContext.Request.Headers["sesionpersona"];
            Guid? gsesionpersona = null;
            if (xSesionpersona != null && xSesionpersona != "") gsesionpersona = new Guid(xSesionpersona);
            if (gsesionpersona != null)
            {
                using (GeneralContext dg = new GeneralContext())
                {
                    nomBuilder oBuilder = dg.nomBuilder.FirstOrDefault(c => c.cBuilder == cBuilder);
                    resp= dg.nomBuilderCliente.Where(c => c.iBuilder == oBuilder.iBuilder).ToList()
                        .Join(dg.nomCliente.ToList(), bl => bl.sCliente, cl => cl.sCliente, (bl, cl) => cl).ToList();
                }
            }
            return resp;
        }

        [HttpGet("indice")]
        public List<selBuilder> LstBuilder()
        {
            List<selBuilder> resp = new List<selBuilder>();
            string xSesionpersona = HttpContext.Request.Headers["sesionpersona"];
            Guid? gsesionpersona = null;
            if (xSesionpersona != null && xSesionpersona != "") gsesionpersona = new Guid(xSesionpersona);
            if (gsesionpersona != null)
            {
                using (GeneralContext dg = new GeneralContext())
                {
                    resp = dg.nomBuilder.Select(c => new selBuilder { 
                        iBuilder = c.iBuilder, 
                        cBuilder = c.cBuilder, 
                        xTitulo = extraerTitulo(c.jBuilder) 
                    }).ToList();
                }
            }
            return resp;
        }

        public static string extraerTitulo(string jBuilder)
        {
            var defineTitulo = new { xTitulo = "" };
            var jsonB = JsonConvert.DeserializeAnonymousType(jBuilder, defineTitulo);
            return jsonB.xTitulo;
        }

        public class selBuilder
        {
            public int iBuilder { get; set; }
            public string cBuilder { get; set; }
            public string xTitulo { get; set; }
        }
    }
}