﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using apiCoreNominus.Helpers;
using apiCoreNominus.Model;
using Microsoft.EntityFrameworkCore;

namespace apiCoreNominus.Controllers
{
    [Route("auth")]
    [ApiController]
    public class LoginController : ControllerBase
    {

        [HttpPost, Route("firebase")]
        public AutFirebase postFireBase([FromBody] TuplaLoginFirebase email)
        {
            try
            {
                AutFirebase resp = new AutFirebase();
                AuthContext dbA = new AuthContext();
                string xNav = HttpContext.Request.Headers["User-Agent"];
                string xSeed = HttpContext.Request.Headers["User-Seed"];
                string xUTC = HttpContext.Request.Headers["token-utc"];
                bool lUTC = (xUTC == "true");
                string xMin = Strings.GetToken("app.nominus.pe$$", 128, lUTC: lUTC);
                if (xMin != xSeed)
                {
                    xMin = Strings.GetToken("app.nominus.pe$$", 128, lPrevio: true, lUTC: lUTC);
                    if (xMin != xSeed && xSeed == "q6e98B@NZ=cN6VeeJvJPEXlZXYHTHXHY&BJNO=aNcB6IgLqTcJvzSHXDHHJHlaVNZ=)NaBcNe^643HnE2XSTXYJvqCp6a=aN)BaNt9&V1eTZKJ2CSHl8qT3X$f6NaB")
                    {
                        xMin = xSeed;
                    }
                }
                if (xMin == xSeed || true)
                {
                    authProvider proveedor = dbA.authProvider.FirstOrDefault(c => c.providerId == email.providerId);
                    if (proveedor == null)
                    {
                        byte bProvider = 1;
                        if (dbA.authProvider.Count() > 0) { bProvider = dbA.authProvider.Max(c => c.bProvider); bProvider++; }
                        proveedor = new authProvider { bProvider = bProvider, providerId = email.providerId };
                        dbA.authProvider.Add(proveedor);
                        dbA.SaveChanges();
                    }
                    authNavegador navegador = dbA.authNavegador.FirstOrDefault(c => c.xNavegador == xNav);
                    short sNavegador = -1;
                    if (navegador == null)
                    {
                        try
                        {
                            sNavegador = dbA.authNavegador.Max(c => c.sNavegador);
                        }
                        catch
                        {
                            sNavegador = -1;
                        }
                        sNavegador++;

                        navegador = new authNavegador
                        {
                            sNavegador = sNavegador,
                            xNavegador = xNav,
                            bClaseNavegador = 0
                        };
                        dbA.authNavegador.Add(navegador);
                        dbA.SaveChanges();
                        //TODO: Notificar al sysadmin para que clasifique nuevo navegador
                    }
                    authUsuario usuario = dbA.authUsuario.FirstOrDefault(c => c.email == email.xEmail);
                    if (usuario == null)
                    {
                        usuario = new authUsuario
                        {
                            gUsuario = Guid.NewGuid(),
                            email = email.xEmail,
                            bProvider = proveedor.bProvider
                        };
                        dbA.authUsuario.Add(usuario);
                        dbA.SaveChanges();
                        Procs.CompletarEnlaces(usuario);
                    }
                    authSesion sesion = dbA.authSesion.FirstOrDefault(c => c.gUsuario == usuario.gUsuario && c.sNavegador == navegador.sNavegador && c.dFin == null);
                    if (sesion == null)
                    {
                        sesion = new authSesion
                        {
                            dInicio = DateTime.Now,
                            gToken = Guid.NewGuid(),
                            gUsuario = usuario.gUsuario,
                            sNavegador = navegador.sNavegador
                        };
                        dbA.authSesion.Add(sesion);
                        dbA.SaveChanges();
                    }
                    sesion.xTokenMsg = email.tokenMsg;
                    dbA.Entry(sesion).State = EntityState.Modified;
                    dbA.SaveChanges();
                    resp.token = sesion.gToken;
                    resp.usuario = usuario;
                    resp.contratos = Procs.ContratosServidor(usuario.email);
                    resp.indiceContratos = Procs.IndiceContratos(sesion.gToken);
                    resp.indiceAcceso = Procs.IndiceAccesos(sesion.gToken);

                    if (sesion.sServidor == null)
                    {
                        sesion.sServidor = resp.contratos[0].sServidor;
                        using (NominusContext db = new NominusContext(sesion.sServidor ?? 0))
                        {
                            traStatusContrato _iSetContrato = db.traStatusContrato.FirstOrDefault(c => c.iTrabajador == resp.contratos[0].iPersona);
                            if (_iSetContrato != null)
                            {
                                sesion.iContrato = _iSetContrato.iContrato;
                            }
                        }
                        dbA.Entry(sesion).State = EntityState.Modified;
                        dbA.SaveChanges();
                    }

                    if (usuario.bProvider != proveedor.bProvider)
                    {
                        usuario.bProvider = proveedor.bProvider;
                        dbA.Entry(usuario).State = EntityState.Modified;
                        dbA.SaveChanges();
                    }
                }
                return resp;
            } catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }

        }

        [HttpPost("firebase2")]
        public IActionResult firebase2([FromBody] TuplaLoginFirebase email)
        {
            try
            {
                AutFirebase resp = new AutFirebase();
                AuthContext dbA = new AuthContext();
                string xNav = HttpContext.Request.Headers["User-Agent"];
                string xSeed = HttpContext.Request.Headers["User-Seed"];
                string xUTC = HttpContext.Request.Headers["token-utc"];
                bool lUTC = (xUTC == "true");
                string xMin = Strings.GetToken("app.nominus.pe$$", 128, lUTC: lUTC);
                if (xMin != xSeed)
                {
                    xMin = Strings.GetToken("app.nominus.pe$$", 128, lPrevio: true, lUTC: lUTC);
                    if (xMin != xSeed && xSeed == "q6e98B@NZ=cN6VeeJvJPEXlZXYHTHXHY&BJNO=aNcB6IgLqTcJvzSHXDHHJHlaVNZ=)NaBcNe^643HnE2XSTXYJvqCp6a=aN)BaNt9&V1eTZKJ2CSHl8qT3X$f6NaB")
                    {
                        xMin = xSeed;
                    }
                }
                if (xMin == xSeed || true)
                {
                    authProvider proveedor = dbA.authProvider.FirstOrDefault(c => c.providerId == email.providerId);
                    if (proveedor == null)
                    {
                        byte bProvider = 1;
                        if (dbA.authProvider.Count() > 0) { bProvider = dbA.authProvider.Max(c => c.bProvider); bProvider++; }
                        proveedor = new authProvider { bProvider = bProvider, providerId = email.providerId };
                        dbA.authProvider.Add(proveedor);
                        dbA.SaveChanges();
                    }
                    authNavegador navegador = dbA.authNavegador.FirstOrDefault(c => c.xNavegador == xNav);
                    short sNavegador = -1;
                    if (navegador == null)
                    {
                        try
                        {
                            sNavegador = dbA.authNavegador.Max(c => c.sNavegador);
                        }
                        catch
                        {
                            sNavegador = -1;
                        }
                        sNavegador++;

                        navegador = new authNavegador
                        {
                            sNavegador = sNavegador,
                            xNavegador = xNav,
                            bClaseNavegador = 0
                        };
                        dbA.authNavegador.Add(navegador);
                        dbA.SaveChanges();
                        //TODO: Notificar al sysadmin para que clasifique nuevo navegador
                    }
                    authUsuario usuario = dbA.authUsuario.FirstOrDefault(c => c.email == email.xEmail);
                    if (usuario == null)
                    {
                        usuario = new authUsuario
                        {
                            gUsuario = Guid.NewGuid(),
                            email = email.xEmail,
                            bProvider = proveedor.bProvider
                        };
                        dbA.authUsuario.Add(usuario);
                        dbA.SaveChanges();
                        Procs.CompletarEnlaces(usuario);
                    }
                    authSesion sesion = dbA.authSesion.FirstOrDefault(c => c.gUsuario == usuario.gUsuario && c.sNavegador == navegador.sNavegador && c.dFin == null);
                    if (sesion == null)
                    {
                        sesion = new authSesion
                        {
                            dInicio = DateTime.Now,
                            gToken = Guid.NewGuid(),
                            gUsuario = usuario.gUsuario,
                            sNavegador = navegador.sNavegador
                        };
                        dbA.authSesion.Add(sesion);
                        dbA.SaveChanges();
                    }
                    sesion.xTokenMsg = email.tokenMsg;
                    dbA.Entry(sesion).State = EntityState.Modified;
                    dbA.SaveChanges();
                    resp.token = sesion.gToken;
                    resp.usuario = usuario;
                    resp.contratos = Procs.ContratosServidor(usuario.email);
                    resp.indiceContratos = Procs.IndiceContratos(sesion.gToken);
                    resp.indiceAcceso = Procs.IndiceAccesos(sesion.gToken);

                    if (sesion.sServidor == null)
                    {
                        sesion.sServidor = resp.contratos[0].sServidor;
                        using (NominusContext db = new NominusContext(sesion.sServidor ?? 0))
                        {
                            traStatusContrato _iSetContrato = db.traStatusContrato.FirstOrDefault(c => c.iTrabajador == resp.contratos[0].iPersona);
                            if (_iSetContrato != null)
                            {
                                sesion.iContrato = _iSetContrato.iContrato;
                            }
                        }
                        dbA.Entry(sesion).State = EntityState.Modified;
                        dbA.SaveChanges();
                    }

                    if (usuario.bProvider != proveedor.bProvider)
                    {
                        usuario.bProvider = proveedor.bProvider;
                        dbA.Entry(usuario).State = EntityState.Modified;
                        dbA.SaveChanges();
                    }
                }
                return Ok(resp);
            } catch (Exception ex)
            {
                return StatusCode(500, ex.ToString());
            }

        }

        [HttpGet("asociarPush")]
        public void getAsociarPush(string tokenPush)
        {
            AuthContext dbA = new AuthContext();
            string SesionPersona = HttpContext.Request.Headers["sesionPersona"];
            Guid? gSesionPersona = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            authSesion _sesion = dbA.authSesion.Find(gSesionPersona);
            if (_sesion != null)
            {
                _sesion.xTokenMsg = tokenPush;
                dbA.Entry(_sesion).State = EntityState.Modified;
                dbA.SaveChanges();
            }
        }

        [HttpGet("consola/{email}")]
        public AutConsola GetAuthConsola(string email)
        {
            string xUserAgent = HttpContext.Request.Headers["user-agent"];
            AutConsola rsp = new AutConsola();
            using (GeneralContext dbG = new GeneralContext())
            {
                rsp.persona = dbG.nomPersonal.FirstOrDefault(c => c.xCorreo == email);
                if (rsp.persona != null)
                {
                    isoUserAgent _ua = dbG.isoUserAgent.FirstOrDefault(c => c.xUserAgent == xUserAgent);
                    if (_ua == null)
                    {
                        _ua = new isoUserAgent { xUserAgent = xUserAgent };
                        dbG.Add(_ua);
                        dbG.SaveChanges();
                    }
                    nomSesion _ses = dbG.nomSesion.FirstOrDefault(c=>c.iPersonaNominus==rsp.persona.iPersonaNominus && c.iUserAgent==_ua.iUserAgent);
                    if (_ses == null)
                    {
                        _ses = new nomSesion { 
                            gSesion = Guid.NewGuid(), 
                            iPersonaNominus = rsp.persona.iPersonaNominus,
                            iUserAgent = _ua.iUserAgent,
                            dInicio = DateTime.Now
                        };
                        dbG.nomSesion.Add(_ses);
                        dbG.SaveChanges();
                    }
                    rsp.gSesion = _ses.gSesion;
                    nomPersonalPerfilCliente cliDef = null;
                    List<nomPersonalPerfilCliente> _nppc = dbG.nomPersonalPerfilCliente
                        .Where(c => c.iPersonaNominus == rsp.persona.iPersonaNominus).ToList();
                    if (_nppc.Count > 0)
                    {
                        cliDef = _nppc.FirstOrDefault(c => c.lDefault);
                        if (cliDef == null)
                        {
                            cliDef = _nppc[0];
                            cliDef.lDefault = true;
                            dbG.Entry(cliDef).State = EntityState.Modified;
                            dbG.SaveChanges();
                        }
                    }
                    rsp.perfiles = _nppc
                        .Join(dbG.nomCliente.ToList(),
                            per => per.sCliente,
                            cli => cli.sCliente,
                            (per, cli) => new { per, cli })
                        .Join(dbG.nomPersonalPerfil.ToList(),
                            per => per.per.bPerfil,
                            lpe => lpe.bPerfil,
                            (per, lpe) => new { per, lpe })
                        .Select(s => new AutPersonalPerfilCliente
                        {
                            iPersonaNominus = s.per.per.iPersonaNominus,
                            sCliente = s.per.per.sCliente,
                            bPerfil = s.per.per.bPerfil,
                            dVigencia = s.per.per.dVigencia,
                            lDefault = s.per.per.lDefault,
                            xCliente = s.per.cli.xCliente,
                            xPerfil = s.lpe.xPerfil
                        }).ToList();
                    if (cliDef != null)
                    {
                        AutConsolaCliente _ce = Procs.PerListaContractual(rsp.persona.xCorreo, cliDef.sCliente, cliDef.bPerfil);
                        rsp.empresas = _ce.empresas;
                        rsp.colaboradores = _ce.colaboradores;
                        rsp.trabajador = _ce.trabajador;
                    }
                }
            }
            return rsp;
        }

        [HttpPost("cambiarCliente")]
        public AutConsolaCliente CambiarCliente([FromBody] nomPersonalPerfilCliente nppc)
        {
            AutConsolaCliente rsp = new AutConsolaCliente();
            using(GeneralContext dbG = new GeneralContext())
            {
                foreach(nomPersonalPerfilCliente _nppc in dbG.nomPersonalPerfilCliente.Where(n=>n.iPersonaNominus==nppc.iPersonaNominus).ToList())
                {
                    if(_nppc.lDefault != (_nppc.sCliente == nppc.sCliente)) {
                        _nppc.lDefault = (_nppc.sCliente == nppc.sCliente);
                        dbG.Entry(_nppc).State = EntityState.Modified;
                        dbG.SaveChanges();
                    }
                }
                nomPersonal np = dbG.nomPersonal.Find(nppc.iPersonaNominus);
                rsp = Procs.PerListaContractual(np.xCorreo, nppc.sCliente, nppc.bPerfil);
            }
            return rsp;
        }

        [HttpPost, Route("fire")]
        public AutFire postFire([FromBody] TuplaLoginFirebase email)
        {
            AutFire resp = new AutFire();
            using (AuthContext dbA = new AuthContext())
            {
                string xNav = HttpContext.Request.Headers["User-Agent"];
                string xSeed = HttpContext.Request.Headers["User-Seed"];
                string xUTC = HttpContext.Request.Headers["token-utc"];
                bool lUTC = (xUTC == "true");
                string xMin = Strings.GetToken("app.nominus.pe$$", 128, lUTC: lUTC);
                if (xMin != xSeed)
                {
                    xMin = Strings.GetToken("app.nominus.pe$$", 128, lPrevio: true, lUTC: lUTC);
                    if (xMin != xSeed && xSeed == "q6e98B@NZ=cN6VeeJvJPEXlZXYHTHXHY&BJNO=aNcB6IgLqTcJvzSHXDHHJHlaVNZ=)NaBcNe^643HnE2XSTXYJvqCp6a=aN)BaNt9&V1eTZKJ2CSHl8qT3X$f6NaB")
                    {
                        xMin = xSeed;
                    }
                }
                if (xMin == xSeed || true)
                {
                    authProvider proveedor = dbA.authProvider.FirstOrDefault(c => c.providerId == email.providerId);
                    if (proveedor == null)
                    {
                        byte bProvider = 1;
                        if (dbA.authProvider.Count() > 0) { 
                            bProvider = dbA.authProvider.Max(c => c.bProvider); bProvider++; 
                        }
                        proveedor = new authProvider { bProvider = bProvider, providerId = email.providerId };
                        dbA.authProvider.Add(proveedor);
                        dbA.SaveChanges();
                    }
                    authNavegador navegador = dbA.authNavegador.FirstOrDefault(c => c.xNavegador == xNav);
                    short sNavegador = -1;
                    if (navegador == null)
                    {
                        try
                        {
                            sNavegador = dbA.authNavegador.Max(c => c.sNavegador);
                        }
                        catch
                        {
                            sNavegador = -1;
                        }
                        sNavegador++;

                        navegador = new authNavegador
                        {
                            sNavegador = sNavegador,
                            xNavegador = xNav,
                            bClaseNavegador = 0
                        };
                        dbA.authNavegador.Add(navegador);
                        dbA.SaveChanges();
                        //TODO: Notificar al sysadmin para que clasifique nuevo navegador
                    }
                    authUsuario usuario = dbA.authUsuario.FirstOrDefault(c => c.email == email.xEmail);
                    if (usuario == null)
                    {
                        usuario = new authUsuario
                        {
                            gUsuario = Guid.NewGuid(),
                            email = email.xEmail,
                            bProvider = proveedor.bProvider
                        };
                        dbA.authUsuario.Add(usuario);
                        dbA.SaveChanges();

                        //using (GeneralContext dbG = new GeneralContext())
                        //{
                        //    foreach (nomConexion _srv in dbG.nomConexion.Where(c=>c.bTipoDb==3))
                        //    {
                        //        using (NominusContext db = new NominusContext(_srv.sCliente))
                        //        {
                        //            autUsuario usr = db.autUsuario.FirstOrDefault(c => c.xEmail == usuario.email);
                        //            if (usr != null)
                        //            {
                        //                dbA.authAcceso.Add(new authAcceso
                        //                {
                        //                    gAcceso = Guid.NewGuid(),
                        //                    sServidor = _srv.sCliente,
                        //                    gUsuario = usuario.gUsuario,
                        //                    iPersona = usr.iUsuario
                        //                });
                        //                dbA.SaveChanges();
                        //            }
                        //        }
                        //    }
                        //}
                    }
                    authSesion sesion = dbA.authSesion.FirstOrDefault(c => c.gUsuario == usuario.gUsuario && c.sNavegador == navegador.sNavegador && c.dFin == null);
                    if (sesion == null)
                    {
                        using (GeneralContext dbG = new GeneralContext())
                        {
                            foreach (nomConexion _srv in dbG.nomConexion.Where(c => c.bTipoDb == 3))
                            {
                                using (NominusContext db = new NominusContext(_srv.sCliente))
                                {
                                    autUsuario usr = db.autUsuario.FirstOrDefault(c => c.xEmail == usuario.email);
                                    if (usr != null)
                                    {
                                        dbA.authAcceso.Add(new authAcceso
                                        {
                                            gAcceso = Guid.NewGuid(),
                                            sServidor = _srv.sCliente,
                                            gUsuario = usuario.gUsuario,
                                            iPersona = usr.iUsuario
                                        });
                                        dbA.SaveChanges();
                                    }
                                }
                            }
                        }

                        sesion = new authSesion
                        {
                            dInicio = DateTime.Now,
                            gToken = Guid.NewGuid(),
                            gUsuario = usuario.gUsuario,
                            sNavegador = navegador.sNavegador
                        };
                        dbA.authSesion.Add(sesion);
                        dbA.SaveChanges();
                    }
                    sesion.xTokenMsg = email.tokenMsg;
                    dbA.Entry(sesion).State = EntityState.Modified;
                    dbA.SaveChanges();
                    resp.token = sesion.gToken;
                    resp.usuario = usuario;
                    resp.indiceAcceso = Procs.IndiceAccesos(sesion.gToken);
                    resp.indiceContratos = Procs.IndiceContratos(sesion.gToken);
                    AutIndiceContrato contratoSel = resp.indiceContratos.FirstOrDefault(c => c.lSeleccionado == true);
                    if (contratoSel != null)
                    {
                        resp.contrato = Procs.ContratoSel(contratoSel);
                        if (sesion.iContrato == null || sesion.iContrato != resp.contrato.iContratoPeriodo)
                        {
                            sesion.iContrato = resp.contrato.iContratoPeriodo;
                            dbA.Entry(sesion).State = EntityState.Modified;
                            dbA.SaveChanges();
                        }
                    }
                    if (usuario.bProvider != proveedor.bProvider)
                    {
                        usuario.bProvider = proveedor.bProvider;
                        dbA.Entry(usuario).State = EntityState.Modified;
                        dbA.SaveChanges();
                    }
                }
            }
            return resp;
        }

        [HttpGet, Route("fijarServidor/{sServidor}")]
        public void FijarServidor(short sServidor)
        {
            string sesionpersona = HttpContext.Request.Headers["sesionpersona"];
            if (sesionpersona != null && sesionpersona != "")
            {
                using (AuthContext dbA = new AuthContext())
                {
                    authSesion _ses = dbA.authSesion.Find(new Guid(sesionpersona));
                    _ses.sServidor = sServidor;
                    dbA.Entry(_ses).State = EntityState.Modified;
                    dbA.SaveChanges();
                }
            }
        }

        [HttpPost, Route("selContrato")]
        public AutContratoSeleccionado SeleccionarContrato([FromBody] TuplaContrato contrato)
        {
            AutContratoSeleccionado resp = null;
            string sesionpersona = HttpContext.Request.Headers["sesionpersona"];
            if (sesionpersona != null && sesionpersona != "")
            {
                PerNavegante perfil = Procs.PerPerfilContrato(new Guid(sesionpersona),contrato.sServidor,contrato.iContratoPeriodo);
                if (perfil.tieneContrato)
                {
                    using (AuthContext dbA = new AuthContext())
                    {
                        using (NominusContext db = new NominusContext(contrato.sServidor))
                        {
                            traContratoPeriodo _contratoPeriodo = db.traContratoPeriodo.Find(contrato.iContratoPeriodo);
                            traContrato _contrato = db.traContrato.Find(_contratoPeriodo.iContrato);
                            USERINFO _uzk = null;
                            using (MarcaZkContext dbZ = new MarcaZkContext(contrato.sServidor))
                            {
                                if (dbZ.isConnected())
                                {
                                    _uzk = dbZ.USERINFO.FirstOrDefault(c => c.Badgenumber == _contrato.iTrabajador.ToString());
                                }
                            }
                            orgEmpresa _emp = db.orgEmpresa.Find(_contrato.sEmpresa);
                            perPersona _per = db.perPersona.Find(_contrato.iTrabajador);
                            perFoto _pfo = db.perFoto.FirstOrDefault(c => c.iPersona == _contrato.iTrabajador && c.bTipoFoto == 1);

                            resp = new AutContratoSeleccionado
                            {
                                iContrato = _contrato.iContrato,
                                iContratoPeriodo = contrato.iContratoPeriodo,
                                sServidor = contrato.sServidor,
                                sEmpresa = _contrato.sEmpresa,
                                sCargo = _contrato.sCargo,
                                dDesde = _contratoPeriodo.dDesde,
                                dHasta = _contratoPeriodo.dHasta,
                                iPersona = _contrato.iTrabajador,
                                iZkUserId = _uzk != null ? _uzk.USERID : 0,
                                cTipo = _contratoPeriodo.dHasta == null ? "A" : _contratoPeriodo.dHasta > DateTime.Now ? "F" : "V",
                                xDescripcion = dbA.authServidor.Find(contrato.sServidor).xDescripcion,
                                jImagen = db.orgEmpresaImagen.FirstOrDefault(c => c.sEmpresa == _contrato.sEmpresa && c.bTipoImagen == 1).jImagen,
                                xCargo = db.orgCargo.Find(_contrato.sCargo).xCargo,
                                xAbreviado = _emp.xAbreviado,
                                xRazonSocial = _emp.xRazonSocial,
                                jFoto = _pfo != null ? _pfo.jFoto : null,
                                xNombres = _per.xNombres,
                                xApellidos = _per.xApellidoPaterno + (_per.xApellidoMaterno != null ? " " + _per.xApellidoMaterno : ""),
                                jAppConfig = _emp.jAppConfig
                            };
                        }
                        authSesion _ses = dbA.authSesion.Find(new Guid(sesionpersona));
                        if (_ses != null)
                        {
                            _ses.iContrato = contrato.iContratoPeriodo;
                            dbA.Entry(_ses).State = EntityState.Modified;
                            dbA.SaveChanges();
                        }
                    }
                }
            }
            return resp;
        }
    }
}
