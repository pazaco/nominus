﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using apiCoreNominus.Helpers;
using apiCoreNominus.Model;
using Microsoft.EntityFrameworkCore;

namespace apiCoreNominus.Controllers
{
    [Route("auth/v2")]
    [ApiController]
    public class Login2Controller : ControllerBase
    {
        [HttpGet("consola/{email}")]
        public Jash GetAuthConsola(string email)
        {
            string xUserAgent = HttpContext.Request.Headers["user-agent"];
            AutConsola rsp = new AutConsola();
            using (GeneralContext dbG = new GeneralContext())
            {
                rsp.persona = dbG.nomPersonal.FirstOrDefault(c => c.xCorreo == email);
                if (rsp.persona != null)
                {
                    isoUserAgent _ua = dbG.isoUserAgent.FirstOrDefault(c => c.xUserAgent == xUserAgent);
                    if (_ua == null)
                    {
                        _ua = new isoUserAgent { xUserAgent = xUserAgent };
                        dbG.Add(_ua);
                        dbG.SaveChanges();
                    }
                    nomSesion _ses = dbG.nomSesion.FirstOrDefault(c => c.iPersonaNominus == rsp.persona.iPersonaNominus && c.iUserAgent == _ua.iUserAgent);
                    if (_ses == null)
                    {
                        _ses = new nomSesion
                        {
                            gSesion = Guid.NewGuid(),
                            iPersonaNominus = rsp.persona.iPersonaNominus,
                            iUserAgent = _ua.iUserAgent,
                            dInicio = DateTime.Now
                        };
                        dbG.nomSesion.Add(_ses);
                        dbG.SaveChanges();
                    }
                    rsp.gSesion = _ses.gSesion;
                    nomPersonalPerfilCliente cliDef = null;
                    List<nomPersonalPerfilCliente> _nppc = dbG.nomPersonalPerfilCliente
                        .Where(c => c.iPersonaNominus == rsp.persona.iPersonaNominus).ToList();
                    if (_nppc.Count > 0)
                    {
                        cliDef = _nppc.FirstOrDefault(c => c.lDefault);
                        if (cliDef == null)
                        {
                            cliDef = _nppc[0];
                            cliDef.lDefault = true;
                            dbG.Entry(cliDef).State = EntityState.Modified;
                            dbG.SaveChanges();
                        }
                    }
                    rsp.perfiles = _nppc
                        .Join(dbG.nomCliente.ToList(),
                            per => per.sCliente,
                            cli => cli.sCliente,
                            (per, cli) => new { per, cli })
                        .Join(dbG.nomPersonalPerfil.ToList(),
                            per => per.per.bPerfil,
                            lpe => lpe.bPerfil,
                            (per, lpe) => new { per, lpe })
                        .Select(s => new AutPersonalPerfilCliente
                        {
                            iPersonaNominus = s.per.per.iPersonaNominus,
                            sCliente = s.per.per.sCliente,
                            bPerfil = s.per.per.bPerfil,
                            dVigencia = s.per.per.dVigencia,
                            lDefault = s.per.per.lDefault,
                            xCliente = s.per.cli.xCliente,
                            xPerfil = s.lpe.xPerfil
                        }).ToList();
                    if (cliDef != null)
                    {
                        AutConsolaCliente _ce = Procs.PerListaContractual(rsp.persona.xCorreo, cliDef.sCliente, cliDef.bPerfil);
                        rsp.empresas = _ce.empresas;
                        rsp.colaboradores = _ce.colaboradores;
                        rsp.trabajador = _ce.trabajador;
                    }
                }
            }
            return new Jash { hash = Helpers.Strings.Encrypt128("Hola mundo, yo soy un dato importante, por eso estoy encriptado"), ex = null };
            
        }
    }
}
