﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using apiCoreNominus.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace apiCoreNominus.Controllers.Asistencia
{
    [Route("zk")]
    [ApiController]
    public class ZkAccessController : ControllerBase
    {

        [HttpGet("machines")]
        public List<Machines> lstMachines()
        {
            List<Machines> rsp = null;
            string SesionPersona = HttpContext.Request.Headers["sesionPersona"];
            Guid? gSesionPersona = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            short? sServidor = Procs.HoldingSesion(gSesionPersona);
            if (sServidor != null)
            {
                using (MarcaZkContext  dbZ = new MarcaZkContext((short)sServidor))
                {
                    if (dbZ.isConnected())
                    {
                        rsp = dbZ.Machines.ToList();
                    }
                }
            }
            return rsp;
        }

    }
}
