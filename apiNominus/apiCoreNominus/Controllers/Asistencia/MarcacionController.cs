﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection.Metadata.Ecma335;
using System.Threading.Tasks;
using apiCoreNominus.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace apiCoreNominus.Controllers.Asistencia
{
    [Route("marcacion")]
    [ApiController]
    public class MarcacionController : ControllerBase
    {
        [HttpPost("marcaQRi")]
        public MarOutMarca marcaQRinterno([FromBody] MarInMarca marca)
        {
            MarOutMarca oResp = new MarOutMarca();
            try
            {
                using (NominusContext db = new NominusContext(marca.sServidor))
                {
                    marDispositivo _disp = db.marDispositivo.FirstOrDefault(c => c.cLlave == marca.cPuerta);
                    if (_disp == null)
                    {
                        oResp.bRespuesta = 1; //  Dispositivo o puerta no existe
                    }
                    else
                    {
                        asiControl _control = db.asiControl.Find(marca.iControl);

                        if (_control == null)
                        {
                            oResp.bRespuesta = 2; // Control no existe
                        }
                        else
                        {
                            asiAsistencia _asistencia = db.asiAsistencia.FirstOrDefault(a => a.iAsistencia == _control.iAsistencia);
                            traContrato _contrato = db.traContrato.FirstOrDefault(ct => ct.iContrato == _asistencia.iContrato);
                            marMarcacion _ultima = db.marMarcacion.OrderByDescending(c => c.dHora).FirstOrDefault(c => c.iPersona == _contrato.iTrabajador);
                            double difMinutos = 99999;
                            if (_ultima != null)
                            {
                                TimeSpan diferencia = marca.dHora.Subtract((DateTime)_ultima.dHora);
                                difMinutos = diferencia.TotalMinutes;
                            }
                            if (difMinutos < 30)
                            {
                                oResp.bRespuesta = 3; // Debe existir al menos 30 minutos de diferencia entre marcas;
                                oResp.aRespuestaData = new string[] { string.Format("{0:yyyy-MM-ddTHH:mm:ss}", _ultima.dHora) };
                            }
                            else
                            {
                                int _suba = 0;
                                int _subas = db.marMarcacion.Count();
                                if (_subas > 0)
                                {
                                    _suba = db.marMarcacion.Max(c => c.iMarca);
                                }

                                marMarcacion _nueva = new marMarcacion { dHora = marca.dHora, sDispositivo = _disp.sDispositivo, iPersona = _contrato.iTrabajador, iMarca = _suba + 1, iControl = marca.iControl };
                                _control.dHoraControl = marca.dHora;

                                // TODO aqui va la calificacion de la marcación
                                db.Entry(_control).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                                db.marMarcacion.Add(_nueva);
                                db.SaveChanges();
                                oResp.bRespuesta = 0; // marcación Ok.
                                oResp.aRespuestaData = new string[] { _suba.ToString() };
                            }
                        }
                    }
                }
            }
            catch (SocketException ex)
            {
                String exp = ex.Message;
                throw new WebException();
            }
            return oResp;
        }

        [HttpGet("dispositivoTeletrabajo")]
        public MarDispositivoTeletrabajo GetDispositivoTeletrabajo()
        {
            string SesionPersona = HttpContext.Request.Headers["sesionPersona"];
            Guid? gSesionPersona = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            using (AuthContext dbA = new AuthContext())
            {
                authSesion _sesion = dbA.authSesion.Find(gSesionPersona);
                if (_sesion != null && _sesion.sServidor != null)
                {
                    using (NominusContext dbN = new NominusContext((short)_sesion.sServidor))
                    {
                        traContratoPeriodo ctp = dbN.traContratoPeriodo.Find((int)_sesion.iContrato);
                        traContrato ctt = dbN.traContrato.Find((int)ctp.iContrato);
                        return Procs.GetDispositivo(dbN, ctt.iTrabajador);
                    }
                }
                else return null;
            }
        }

        [HttpGet("solicitudDispositivo")]
        public string GetSolicitudDispositivo()
        {
            string SesionPersona = HttpContext.Request.Headers["sesionPersona"];
            string xNav = HttpContext.Request.Headers["User-Agent"];
            Guid? gSesionPersona = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            using (AuthContext dbA = new AuthContext())
            {
                authSesion _sesion = dbA.authSesion.Find(gSesionPersona);
                if (_sesion != null && _sesion.sServidor != null)
                {
                    using (NominusContext dbN = new NominusContext((short)_sesion.sServidor))
                    {
                        traContratoPeriodo ctp = dbN.traContratoPeriodo.Find((int)_sesion.iContrato);
                        traContrato ctt = dbN.traContrato.Find((int)ctp.iContrato);
                        IEnumerable<marEmparejar> delPares = dbN.marEmparejar.Where(e => e.iPersona == ctt.iTrabajador || e.dVencimiento < DateTime.Now);
                        if (delPares.Count() > 0)
                        {
                            dbN.marEmparejar.RemoveRange(delPares);
                            dbN.SaveChanges();
                        }
                        int _pi = xNav.IndexOf("(");
                        int _pf = xNav.IndexOf(")");
                        bool isMobile = xNav.ToLower().IndexOf("mobile") > 0;
                        string xDis = xNav.ToLower().Substring(_pi + 1, _pf - _pi - 1);
                        if (isMobile)
                        {
                            marEmparejar nem = new marEmparejar
                            {
                                iPersona = ctt.iTrabajador,
                                dVencimiento = DateTime.Now.AddMinutes(5),
                                xRespuesta = xDis
                            };
                            dbN.marEmparejar.Add(nem);
                            dbN.SaveChanges();
                            return "Ok";
                        }
                        else return "No mobile";
                    }
                }
                else return "No sesion";
            }
        }

        [HttpGet("buscarEmparejo/{iTrabajador}")]
        public marEmparejar GetBuscarEmparejo(int iTrabajador)
        {
            marEmparejar rsp = null;
            string SesionPersona = HttpContext.Request.Headers["sesionPersona"];
            Guid? gSesionPersona = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            nomPersonalPerfilCliente perfil = Procs.PerfilSesion(gSesionPersona);
            if (perfil != null && perfil.bPerfil <= 4)
            {
                using (NominusContext dbN = new NominusContext(perfil.sCliente))
                {
                    rsp = dbN.marEmparejar.FirstOrDefault(c => c.iPersona == iTrabajador);
                }
            }
            return rsp;
        }

        [HttpGet("dispositivo/{iTrabajador}")]
        public marDispositivo GetDispositivo(int iTrabajador)
        {
            marDispositivo dsp = null;
            string SesionPersona = HttpContext.Request.Headers["sesionPersona"];
            Guid? gSesionPersona = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            nomPersonalPerfilCliente perfil = Procs.PerfilSesion(gSesionPersona);
            if (perfil != null && perfil.bPerfil <= 4)
            {
                using (NominusContext dbN = new NominusContext(perfil.sCliente))
                {
                        dsp = dbN.marDispositivo.FirstOrDefault(d => d.iDispositivoInterno == iTrabajador && d.bPlataforma == 3);
                }
            }
            return dsp;
        }

        [HttpGet("autorizarDispositivo/{iTrabajador}")]
        public marDispositivo AutorizarDispositivo(int iTrabajador)
        {
            marDispositivo dsp = null;
            string SesionPersona = HttpContext.Request.Headers["sesionPersona"];
            Guid? gSesionPersona = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            nomPersonalPerfilCliente perfil = Procs.PerfilSesion(gSesionPersona);
            if (perfil != null && perfil.bPerfil <= 4)
            {
                using (NominusContext dbN = new NominusContext(perfil.sCliente))
                {
                    marEmparejar solic = dbN.marEmparejar.FirstOrDefault(c => c.iPersona == iTrabajador);
                    if (solic != null)
                    {
                        dsp = dbN.marDispositivo.FirstOrDefault(d => d.iDispositivoInterno == iTrabajador && d.bPlataforma == 3);
                        if (dsp != null)
                        {
                            dsp.xDescripcion = solic.xRespuesta;
                            dbN.Entry(dsp).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                            dbN.SaveChanges();
                        }
                        else
                        {
                            short sDispositivo = 1;
                            perPersona per = dbN.perPersona.First(p => p.iPersona == iTrabajador);
                            string cLlave = (per.xNombres.Trim() + per.xApellidoPaterno.Trim()).Replace(" ", "");
                            if (dbN.marDispositivo.Count() > 0)
                            {
                                sDispositivo = dbN.marDispositivo.Max(c => c.sDispositivo);
                                sDispositivo++;
                            }
                            dsp = new marDispositivo
                            {
                                sDispositivo = sDispositivo,
                                xDescripcion = solic.xRespuesta,
                                cLlave = cLlave,
                                bPlataforma = 3,
                                iDispositivoInterno = iTrabajador,
                                sSede = 1
                            };
                            dbN.marDispositivo.Add(dsp);
                            dbN.SaveChanges();
                        }
                        dbN.marEmparejar.Remove(solic);
                        dbN.SaveChanges();
                    }
                }
            }
            return dsp;
        }

    }
}
