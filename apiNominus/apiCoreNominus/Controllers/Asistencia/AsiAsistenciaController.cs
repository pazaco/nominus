﻿using apiCoreNominus.Helpers;
using apiCoreNominus.Model;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;


namespace apiCoreNominus.Controllers.Asistencia
{
    [Route("asistencia")]
    [ApiController]
    public class AsiAsistenciaController : ControllerBase
    {
        private IWebHostEnvironment webHost;

        public AsiAsistenciaController(IWebHostEnvironment environment)
        {
            webHost = environment;
        }

        [HttpPost("diarioEvaluacionRiesgo")]
        public Pdfs.outPdf diarioEvalRiesgo([FromBody] ParEmpresaFecha empresaFecha)
        {
            Pdfs.outPdf rsp = null;
            string SesionPersona = HttpContext.Request.Headers["sesionPersona"];
            Guid? gSesionPersona = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            short? sServidor = Procs.HoldingSesion(gSesionPersona);
            if (sServidor != null)
            {
                rsp = Pdfs.diarioEvalRiesgo((short)sServidor, empresaFecha.sEmpresa, webHost.ContentRootPath, empresaFecha.dFecha.Date);
            }
            return rsp;
        }

        [HttpPost("PdfDiarioEvaluacionRiesgo")]
        public PhysicalFileResult PdfDiarioEvalRiesgo([FromBody] ParEmpresaFecha empresaFecha)
        {
            string rsp = "";
            string SesionPersona = HttpContext.Request.Headers["sesionPersona"];
            Guid? gSesionPersona = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            short? sServidor = Procs.HoldingSesion(gSesionPersona);
            if (sServidor != null)
            {
                var outP = Pdfs.diarioEvalRiesgo((short)sServidor, empresaFecha.sEmpresa, webHost.ContentRootPath, empresaFecha.dFecha.Date);
                rsp = outP.xUNC;
            }
            return new PhysicalFileResult(rsp, "application/pdf");
        }
        public class outUrl
        {
            public string xUrl { get; set; }
        }

        [HttpGet("calendarioEvaluacionRiesgo/{sEmpresa}")]
        public List<DateTime> GetCalendarioEvaluacionRiesgo(short sEmpresa)
        {
            List<DateTime> rsp = new List<DateTime>();
            string SesionPersona = HttpContext.Request.Headers["sesionPersona"];
            Guid? gSesionPersona = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            nomPersonalPerfilCliente sesion = Procs.PerfilSesion(gSesionPersona);
            if (sesion != null)
            {
                using (NominusContext dbN = new NominusContext(sesion.sCliente))
                {
                    rsp = dbN.asiRiesgoCOVID.AsEnumerable()
                    .Join(dbN.asiAsistencia, r => r.iAsistencia, a => a.iAsistencia, (r, a) => a)
                    .Select(f => f.dFecha).Distinct().ToList();
                }
            }
            return rsp;
        }
        public class ParEmpresaFecha
        {
            public short sEmpresa { get; set; }
            public DateTime dFecha { get; set; }
        }



    }




}
