﻿using apiCoreNominus.Helpers;
using apiCoreNominus.Model;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;


namespace apiCoreNominus.Controllers.Asistencia
{
    [Route("asistencia")]
    [ApiController]
    public class AppAsistenciaController : ControllerBase
    {
        private IWebHostEnvironment webHost;

        public AppAsistenciaController(IWebHostEnvironment environment)
        {
            webHost = environment;
        }

        [HttpGet("hoy/{iContrato}")]
        public AsiAsistencia getAsistenciaHoy(int iContrato)
        {
            AsiAsistencia rsp = null;
            AuthContext dbA = new AuthContext();
            string SesionPersona = HttpContext.Request.Headers["sesionPersona"];
            Guid? gSesionPersona = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            DateTime hoy = DateTime.Now.Date;
            authSesion _sesion = dbA.authSesion.Find(gSesionPersona);
            if (_sesion != null && _sesion.sServidor != null)
            {
                using (NominusContext dbN = new NominusContext((short)_sesion.sServidor))
                {
                    asiAsistencia _asi = dbN.asiAsistencia.FirstOrDefault(a => a.iContrato == iContrato && a.dFecha == hoy);
                    traContrato _ctt = dbN.traContrato.Find(iContrato);
                    if (_asi == null)
                    {
                        horProgramacion _prg = dbN.horProgramacion.FirstOrDefault(c => c.iContrato == iContrato && (c.dDesde == null || c.dDesde <= hoy) && (c.dHasta == null || c.dHasta >= hoy));
                        horHorario _hor = null;
                        if (_prg == null)
                        {
                            _hor = dbN.horHorario.FirstOrDefault(h => h.sEmpresa == _ctt.sEmpresa && h.bPrioridad == 255);
                            if (_hor == null)
                            {
                                _hor = dbN.horHorario.FirstOrDefault(h => h.sEmpresa == _ctt.sEmpresa);
                            }
                            if (_hor != null)
                            {
                                int _nProg = 1;
                                if (dbN.horProgramacion.Count() > 0)
                                {
                                    _nProg += dbN.horProgramacion.Max(c => c.iProgramacion);
                                }
                                _prg = new horProgramacion { iProgramacion = _nProg, iContrato = iContrato, sRotacion = 0 };
                                dbN.horProgramacion.Add(_prg);
                                dbN.SaveChanges();
                            }
                        }
                        else
                        {
                            if (_prg.sRotacion == null)
                            {
                                _prg.sRotacion = 0;
                                dbN.Entry(_prg).State = EntityState.Modified;
                                dbN.SaveChanges();
                            }
                            horTipoRotacion _rot = dbN.horTipoRotacion.Find((short)_prg.sRotacion);
                            _hor = dbN.horHorario.Find(_rot.sHorario);
                        }
                        int _nAsist = 1;
                        if (dbN.asiAsistencia.Count() > 0)
                        {
                            _nAsist += dbN.asiAsistencia.Max(c => c.iAsistencia);
                        }
                        _asi = new asiAsistencia
                        {
                            iAsistencia = _nAsist,
                            iContrato = _prg.iContrato,
                            dFecha = hoy
                        };
                        dbN.asiAsistencia.Add(_asi);
                        DayOfWeek dw = hoy.DayOfWeek;
                        string _dw = dw == DayOfWeek.Monday ? "L" : dw == DayOfWeek.Tuesday ? "M" : dw == DayOfWeek.Wednesday ? "W" : dw == DayOfWeek.Thursday ? "J" : dw == DayOfWeek.Friday ? "V" : dw == DayOfWeek.Saturday ? "S" : "D";
                        horJornada _jor = dbN.horJornada.FirstOrDefault(j => j.sHorario == _hor.sHorario && j.cDias.Contains(_dw));
                        List<horControl> _hct = dbN.horControl.Where(c => c.sJornada == _jor.sJornada).ToList();
                        int _nCtl = 1;
                        if (dbN.asiControl.Count() > 0)
                        {
                            _nCtl += dbN.asiControl.Max(c => c.iControl);
                        }
                        List<asiControl> _ctl = _hct.Select(hc => new asiControl
                        {
                            iControl = _nCtl++,
                            iAsistencia = _asi.iAsistencia,
                            sControl = hc.sControl,
                            dHoraHorario = hoy.AddDays((double)hc.sDiasPrevio) + hc.dHora
                        }).ToList();
                        List<AsiControl> _Hct = _ctl.AsEnumerable()
                            .Join(dbN.horControl, h => h.sControl, hc => hc.sControl, (h, hc) => new AsiControl
                            {
                                iControl = h.iControl,
                                iAsistencia = h.iAsistencia,
                                sControl = h.sControl,
                                dHoraHorario = h.dHoraHorario,
                                cTipoControl = hc.cTipoControl
                            }).ToList();
                        if (_Hct.Any(ct => ct.cTipoControl == "T"))
                        {
                            AsiControl _entrada = _Hct.First(ctl => ctl.cTipoControl == "E");
                            AsiControl _salida = _Hct.First(ctl => ctl.cTipoControl == "S");
                            AsiControl _teletrabajo = _Hct.First(ctl => ctl.cTipoControl == "T");
                            TimeSpan timeSpan = (DateTime)_teletrabajo.dHoraHorario - (DateTime)_entrada.dHoraHorario;
                            DateTime proximoTele = (DateTime)_teletrabajo.dHoraHorario + timeSpan;
                            while (proximoTele < (DateTime)_salida.dHoraHorario)
                            {
                                _ctl.Add(new asiControl
                                {
                                    iControl = _nCtl++,
                                    iAsistencia = _asi.iAsistencia,
                                    sControl = _teletrabajo.sControl,
                                    dHoraHorario = proximoTele
                                });
                                proximoTele += timeSpan;
                            }
                        }
                        dbN.asiControl.AddRange(_ctl);
                        dbN.SaveChanges();
                    }
                    rsp = new AsiAsistencia();
                    rsp.iContrato = iContrato;
                    rsp.iAsistencia = _asi.iAsistencia;
                    rsp.dFecha = _asi.dFecha;
                    rsp.controles = Procs.GetControlesAsistencia(dbN, _asi.iAsistencia);
                    marRiesgoCOVID _mrc = dbN.marRiesgoCOVID.OrderByDescending(od => od.dFecha).FirstOrDefault(r => r.iContrato == iContrato);
                    if (_mrc == null)
                    {
                        _mrc = new marRiesgoCOVID { iContrato = iContrato, dFecha = hoy };
                        dbN.marRiesgoCOVID.Add(_mrc);
                        dbN.SaveChanges();
                    }

                    asiRiesgoCOVID _arc = dbN.asiRiesgoCOVID.Find(_asi.iAsistencia);
                    if (_arc == null)
                    {
                        _arc = new asiRiesgoCOVID { iAsistencia = _asi.iAsistencia };
                        dbN.asiRiesgoCOVID.Add(_arc);
                        dbN.SaveChanges();
                    }
                    rsp.evaluacionCOVID = new AsiEvaluacionCOVID
                    {
                        iAsistencia = _arc.iAsistencia,
                        fTemperatura = _arc.fTemperatura,
                        aSintomas = _arc.aSintomas,
                        bEvaluacion = _arc.bEvaluacion,
                        lContacto = _arc.lContacto,
                        lLavadoManos = _arc.lLavadoManos,
                        lTapabocas = _arc.lTapabocas,
                        lRopas = _arc.lRopas,
                        general = _mrc
                    };
                    rsp.gps = Procs.GetDispositivo(dbN, _ctt.iTrabajador);
                }
            }
            return rsp;
        }

        [HttpPost("horario")]
        public AsiAsistencia getAsistenciaHorario([FromBody] ParAhor ahor)
        {
            AsiAsistencia rsp = null;
            AuthContext dbA = new AuthContext();
            string SesionPersona = HttpContext.Request.Headers["sesionPersona"];
            Guid? gSesionPersona = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            DateTime hoy = ahor.dFecha;
            authSesion _sesion = dbA.authSesion.Find(gSesionPersona);
            if (_sesion != null && _sesion.sServidor != null)
            {
                using (NominusContext dbN = new NominusContext((short)_sesion.sServidor))
                {
                    asiAsistencia _asi = dbN.asiAsistencia.FirstOrDefault(a => a.iContrato == ahor.iContrato && a.dFecha == hoy);
                    traContrato _ctt = dbN.traContrato.Find(ahor.iContrato);
                    if (_asi == null)
                    {
                        horProgramacion _prg = dbN.horProgramacion
                            .FirstOrDefault(c => c.iContrato == ahor.iContrato && (c.dDesde == null || c.dDesde <= ahor.dFecha) && (c.dHasta == null || c.dHasta >= ahor.dFecha));
                        horHorario _hor = null;
                        if (_prg == null)
                        {
                            _hor = dbN.horHorario.FirstOrDefault(h => h.sEmpresa == _ctt.sEmpresa && h.bPrioridad == 255);
                            if (_hor == null)
                            {
                                _hor = dbN.horHorario.OrderBy(o=>o.bPrioridad).FirstOrDefault(h => h.sEmpresa == _ctt.sEmpresa);
                            }
                            if (_hor != null)
                            {
                                int _nProg = 1;
                                if (dbN.horProgramacion.Count() > 0)
                                {
                                    _nProg += dbN.horProgramacion.Max(c => c.iProgramacion);
                                }
                                _prg = new horProgramacion { iProgramacion = _nProg, iContrato = ahor.iContrato, bPrioridad = 255 };
                                dbN.horProgramacion.Add(_prg);
                                dbN.SaveChanges();
                            }
                        }
                        else
                        {
                            horTipoRotacion _rot = dbN.horTipoRotacion.Find((short)_prg.sRotacion);
                            _hor = dbN.horHorario.Find(_rot.sHorario);
                        }
                        int _nAsist = 1;
                        if (dbN.asiAsistencia.Count() > 0)
                        {
                            _nAsist += dbN.asiAsistencia.Max(c => c.iAsistencia);
                        }
                        _asi = new asiAsistencia
                        {
                            iAsistencia = _nAsist,
                            iContrato = _prg.iContrato,
                            dFecha = hoy
                        };
                        dbN.asiAsistencia.Add(_asi);
                        DayOfWeek dw = hoy.DayOfWeek;
                        string _dw = dw == DayOfWeek.Monday ? "L" : dw == DayOfWeek.Tuesday ? "M" : dw == DayOfWeek.Wednesday ? "W" : dw == DayOfWeek.Thursday ? "J" : dw == DayOfWeek.Friday ? "V" : dw == DayOfWeek.Saturday ? "S" : "D";
                        horJornada _jor = dbN.horJornada.FirstOrDefault(j => j.sHorario == _hor.sHorario && j.cDias.Contains(_dw));
                        List<horControl> _hct = dbN.horControl.Where(c => c.sJornada == _jor.sJornada).ToList();
                        int _nCtl = 1;
                        if (dbN.asiControl.Count() > 0)
                        {
                            _nCtl += dbN.asiControl.Max(c => c.iControl);
                        }
                        List<asiControl> _ctl = _hct.Select(hc => new asiControl
                        {
                            iControl = _nCtl++,
                            iAsistencia = _asi.iAsistencia,
                            sControl = hc.sControl,
                            dHoraHorario = hoy.AddDays((double)hc.sDiasPrevio) + hc.dHora
                        }).ToList();
                        List<AsiControl> _Hct = _ctl.AsEnumerable()
                            .Join(dbN.horControl, h => h.sControl, hc => hc.sControl, (h, hc) => new AsiControl
                            {
                                iControl = h.iControl,
                                iAsistencia = h.iAsistencia,
                                sControl = h.sControl,
                                dHoraHorario = h.dHoraHorario,
                                cTipoControl = hc.cTipoControl
                            }).ToList();
                        if (_Hct.Any(ct => ct.cTipoControl == "T"))
                        {
                            AsiControl _entrada = _Hct.First(ctl => ctl.cTipoControl == "E");
                            AsiControl _salida = _Hct.First(ctl => ctl.cTipoControl == "S");
                            AsiControl _teletrabajo = _Hct.First(ctl => ctl.cTipoControl == "T");
                            TimeSpan timeSpan = (DateTime)_teletrabajo.dHoraHorario - (DateTime)_entrada.dHoraHorario;
                            DateTime proximoTele = (DateTime)_teletrabajo.dHoraHorario + timeSpan;
                            while (proximoTele < (DateTime)_salida.dHoraHorario)
                            {
                                _ctl.Add(new asiControl
                                {
                                    iControl = _nCtl++,
                                    iAsistencia = _asi.iAsistencia,
                                    sControl = _teletrabajo.sControl,
                                    dHoraHorario = proximoTele
                                });
                                proximoTele += timeSpan;
                            }
                        }
                        dbN.asiControl.AddRange(_ctl);
                        dbN.SaveChanges();
                    }
                    rsp = new AsiAsistencia();
                    rsp.iContrato = ahor.iContrato;
                    rsp.iAsistencia = _asi.iAsistencia;
                    rsp.dFecha = _asi.dFecha;
                    rsp.controles = Procs.GetControlesAsistencia(dbN, _asi.iAsistencia);
                    marRiesgoCOVID _mrc = dbN.marRiesgoCOVID.OrderByDescending(od => od.dFecha).FirstOrDefault(r => r.iContrato == ahor.iContrato);
                    if (_mrc == null)
                    {
                        _mrc = new marRiesgoCOVID { iContrato = ahor.iContrato, dFecha = hoy };
                        dbN.marRiesgoCOVID.Add(_mrc);
                        dbN.SaveChanges();
                    }

                    asiRiesgoCOVID _arc = dbN.asiRiesgoCOVID.Find(_asi.iAsistencia);
                    if (_arc == null)
                    {
                        _arc = new asiRiesgoCOVID { iAsistencia = _asi.iAsistencia };
                        dbN.asiRiesgoCOVID.Add(_arc);
                        dbN.SaveChanges();
                    }
                    rsp.evaluacionCOVID = new AsiEvaluacionCOVID
                    {
                        iAsistencia = _arc.iAsistencia,
                        fTemperatura = _arc.fTemperatura,
                        aSintomas = _arc.aSintomas,
                        bEvaluacion = _arc.bEvaluacion,
                        lContacto = _arc.lContacto,
                        lLavadoManos = _arc.lLavadoManos,
                        lTapabocas = _arc.lTapabocas,
                        lRopas = _arc.lRopas,
                        general = _mrc
                    };
                    rsp.gps = Procs.GetDispositivo(dbN, _ctt.iTrabajador);
                }
            }
            return rsp;
        }
        public class ParAhor
        {
            public int iContrato { get; set; }
            public DateTime dFecha { get; set; }
        }

        [HttpPost("evaluarRiesgo")]
        public void evaluarRiesgo([FromBody] AsiEvaluacionCOVID eval)
        {
            AuthContext dbA = new AuthContext();
            string SesionPersona = HttpContext.Request.Headers["sesionPersona"];
            Guid? gSesionPersona = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            DateTime hoy = DateTime.Now.Date;
            authSesion _sesion = dbA.authSesion.Find(gSesionPersona);
            if (_sesion != null && _sesion.sServidor != null)
            {
                using (NominusContext dbN = new NominusContext((short)_sesion.sServidor))
                {
                    asiRiesgoCOVID _asr = (asiRiesgoCOVID)eval;
                    marRiesgoCOVID _mar = (marRiesgoCOVID)eval.general;
                    marRiesgoCOVID _umr = dbN.marRiesgoCOVID.OrderByDescending(o => o.dFecha).FirstOrDefault(u => u.iContrato == _mar.iContrato);
                    dbN.Entry(_asr).State = EntityState.Modified;
                    dbN.SaveChanges();
                    if (_umr.fPeso != _mar.fPeso || _umr.bTalla != _mar.bTalla || _umr.aEnfermedades != _mar.aEnfermedades)
                    {
                        if (_umr.dFecha.Date == _mar.dFecha.Date)
                        {
                            _umr.bTalla = _mar.bTalla;
                            _umr.fPeso = _mar.fPeso;
                            _umr.aEnfermedades = _mar.aEnfermedades;
                            dbN.Entry(_umr).State = EntityState.Modified;
                        }
                        else
                        {
                            dbN.marRiesgoCOVID.Add(_mar);
                        }
                    }
                    dbN.SaveChanges();
                }
            }
        }

    }




}
