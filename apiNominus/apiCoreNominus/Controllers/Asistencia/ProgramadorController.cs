﻿using apiCoreNominus.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Namotion.Reflection;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Policy;

namespace apiCoreNominus.Controllers.Asistencia
{
    [Route("programador")]
    [ApiController]
    public class ProgramadorController : ControllerBase
    {
        [HttpGet("jefes/{sEmpresa}")]
        public List<PerJefe> lstJefes(short sEmpresa)
        {
            List<PerJefe> rsp = new List<PerJefe>();
            string SesionPersona = HttpContext.Request.Headers["sesionPersona"];
            Guid? gSesionPersona = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            short? sServidor = Procs.HoldingSesion(gSesionPersona);
            if (sServidor != null)
            {
                using (NominusContext dbN = new NominusContext((short)sServidor))
                {
                    IEnumerable<traEstadoContrato> _ctt = Procs.estadoContratos(dbN,sEmpresa).Where(f => f.sEmpresa == sEmpresa && f.bEstadoTrabajador != 2);
                        
                    foreach (int? iJefe in _ctt              
                        .Select(c => c.iJefe).Distinct().ToList())
                    {
                        if (iJefe != null)
                        {
                            perPersona _per = dbN.perPersona.Find((int)iJefe);
                            //var ds = _ctt
                            //    .Join(dbN.horProgramacion, c => c.iContrato, p => p.iContrato, (c, p) => p)
                            //    .Join(dbN.horTipoRotacion, p => p.sRotacion, r => r.sRotacion, (s, r) => r);

                            List<horTipoRotacion> _trt = _ctt
                                .Join(dbN.horProgramacion, c => c.iContrato, p => p.iContrato, (c, p) => p)
                                .Join(dbN.horTipoRotacion, p => p.sRotacion, r => r.sRotacion, (s, r) => r)
                                .Distinct().ToList();
                            rsp.Add(new PerJefe
                            {
                                iPersona = (int)iJefe,
                                xNombre = _per.xApellidoPaterno + " " + (_per.xApellidoMaterno ?? "") + " " + (_per.xNombres ?? ""),
                                tiposRotacion = _trt
                            });
                        }
                    }
                }
            }
            return rsp.OrderBy(c => c.xNombre).ToList();
        }

        [HttpPost("colaboradores2")]
        public List<PerColaborador> lstColaboradores2([FromBody] parJefeEmpresa pars)
        {
            List<PerColaborador> rsp = new List<PerColaborador>();
            string SesionPersona = HttpContext.Request.Headers["sesionPersona"];
            Guid? gSesionPersona = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            short? sServidor = Procs.HoldingSesion(gSesionPersona);
            if (sServidor != null)
            {
                using (NominusContext dbN = new NominusContext((short)sServidor))
                {
                    IEnumerable<traEstadoContrato> _ctt = Procs.estadoContratos(dbN, pars.sEmpresa).Where(f => f.iJefe == pars.iJefe && f.bEstadoTrabajador !=2);
                    rsp = _ctt.Join(dbN.orgSede, cs => cs.sSede, os => os.sSede, (cs, os) => new { cs, os })
                        .Join(dbN.orgCargo, cc => cc.cs.sCargo, oc => oc.sCargo, (cc, oc) => new { cc, oc })
                        .Join(dbN.perPersona, cp => cp.cc.cs.iTrabajador, op => op.iPersona, (cp, op) => new { cp, op })
                        .GroupJoin(dbN.horProgramacionActual.AsEnumerable()
                            .GroupJoin(dbN.horDetProgramacion, d => d.iProgramacion, dd => dd.iProgramacion, (d, dd) => new { d, detalle = dd.Any() ? dd.AsEnumerable() : null }),
                                ca => ca.cp.cc.cs.iContrato,
                                oa => oa.d.iContrato,
                                (ca, oa) => new { ca, os = oa.Any() ? oa.FirstOrDefault() : null })
                        .Select(s => new PerColaborador
                        {
                            iContrato = s.ca.cp.cc.cs.iContrato,
                            iContratoPeriodo = s.ca.cp.cc.cs.iContratoPeriodo,
                            iTrabajador = s.ca.cp.cc.cs.sEmpresa,
                            sSede = s.ca.cp.cc.cs.sSede,
                            sCargo = s.ca.cp.cc.cs.sCargo,
                            cTipo = "AVF".Substring(s.ca.cp.cc.cs.bEstadoTrabajador - 1, 1),
                            dDesde = s.ca.cp.cc.cs.dDesde,
                            dHasta = s.ca.cp.cc.cs.dHasta,
                            sEmpresa = s.ca.cp.cc.cs.sEmpresa,
                            bFormato = s.ca.cp.cc.cs.bFormato,
                            xColaborador = s.ca.op.xApellidoPaterno + " " + (s.ca.op.xApellidoMaterno ?? "") + " " + (s.ca.op.xNombres ?? ""),
                            xSede = s.ca.cp.cc.os.xSede,
                            xCargo = s.ca.cp.oc.xCargo
                        }).ToList();
                }
            }
            return rsp.OrderBy(o => o.xColaborador).ToList();
        }

        [HttpPost("colaboradores")]
        public List<PerColaborador> lstColaboradores([FromBody] parJefeEmpresa pars)
        {
            List<PerColaborador> rsp = new List<PerColaborador>();
            string SesionPersona = HttpContext.Request.Headers["sesionPersona"];
            Guid? gSesionPersona = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            nomPersonalPerfilCliente perfil = Procs.PerfilSesion(gSesionPersona);
            if (perfil != null)
            {
                using (NominusContext dbN = new NominusContext(perfil.sCliente))
                {
                    rsp = Procs.GetColaboradores(dbN, pars.sEmpresa,pars.iJefe);
                    //return Procs.GetTrabajadores(dbN)
                    //            .Where(tr => tr.contratos != null
                    //                        && tr.contratos.Where(c => c.vigente != null
                    //                                && c.vigente.cese == null
                    //                                && c.iJefe == pars.iJefe
                    //                                && c.sEmpresa == pars.sEmpresa
                    //                            ).Any())
                    //    .GroupJoin(dbN.horProgramacion.AsEnumerable()
                    //        .GroupJoin(dbN.horDetProgramacion, kp => kp.iProgramacion, d => d.iProgramacion, (p, s) => new HorProgramacion { 
                    //            iProgramacion = p.iProgramacion,
                    //            iContrato = p.iContrato,
                    //            sRotacion = p.sRotacion,
                    //            dDesde = p.dDesde,
                    //            dHasta = p.dHasta,
                    //            bPrioridad = p.bPrioridad,
                    //            fHorasInicial = p.fHorasInicial,
                    //            fHorasRedimidas = p.fHorasRedimidas,
                    //            fHorasTrabajadas = p.fHorasTrabajadas,
                    //            detalle = s.Any() ? s.AsEnumerable() : null 
                    //        })
                    //    , t => t.contratos.Last().iContrato, p => p.iContrato, (t, p) => new { t, programacion = p.Any() ? p.First() : null })
                    //    .Select(s => new PerColaborador
                    //    {
                    //        iContrato = s.t.contratos.Last().iContrato,
                    //        iContratoPeriodo = s.t.contratos.Last().vigente.iContratoPeriodo,
                    //        iTrabajador = s.t.iTrabajador,
                    //        sSede = s.t.contratos.Last().sSede,
                    //        sCargo = s.t.contratos.Last().sCargo,
                    //        cTipo = s.t.contratos.Select(sv => new { cTipo = sv.vigente.dHasta != null ? "A" : sv.vigente.dHasta >= DateTime.Now.Date ? "F" : "V" }).Last().cTipo,
                    //        dDesde = s.t.contratos.First().vigente.dDesde,
                    //        dHasta = s.t.contratos.Last().vigente.dHasta,
                    //        sEmpresa = s.t.contratos.Last().sEmpresa,
                    //        bFormato = s.t.contratos.Last().vigente.bFormato,
                    //        xColaborador = s.t.persona.xApellidoPaterno + " " + (s.t.persona.xApellidoMaterno ?? "") + " " + (s.t.persona.xNombres ?? ""),
                    //        xSede = s.t.contratos.Last().xSede,
                    //        xCargo = s.t.contratos.Last().xCargo,
                    //        programacion = s.programacion
                    //    }).OrderBy(o=>o.xColaborador).ToList();
                }
            }
            return rsp.OrderBy(o => o.xColaborador).ToList();
        }

        [HttpPost("asignar")]
        public HorProgramacion asignarProgramacion([FromBody] parContratoRotacion par)
        {
            int rsp = 1;
            HorProgramacion _prog = null;
            DateTime dHoy = par.dFecha == null ? DateTime.Now.Date : ((DateTime)par.dFecha).Date;
            string SesionPersona = HttpContext.Request.Headers["sesionPersona"];
            Guid? gSesionPersona = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            short? sServidor = Procs.HoldingSesion(gSesionPersona);
            if (sServidor != null)
            {
                using (NominusContext dbN = new NominusContext((short)sServidor))
                {
                    horTipoRotacion _rot = dbN.horTipoRotacion.Find(par.sRotacion);
                    if (_rot != null)
                    {
                        if (dbN.horProgramacion.Count() > 0)
                        {
                            rsp = dbN.horProgramacion.Max(c => c.iProgramacion);
                            rsp++;
                        }
                        traStatusContrato _ctt = dbN.traStatusContrato.FirstOrDefault(c => c.iContrato == par.iContrato);
                        _prog = dbN.horProgramacion.Where(p => p.iContrato == par.iContrato).Select(s=> new HorProgramacion
                        {
                            iProgramacion = s.iProgramacion,
                            iContrato = s.iContrato,
                            dDesde = s.dDesde,
                            dHasta = s.dHasta,
                            bPrioridad = s.bPrioridad,
                            sRotacion = s.sRotacion
                        }).FirstOrDefault();
                        if (_prog == null)
                        {
                            _prog = new HorProgramacion
                            {
                                iProgramacion = rsp,
                                iContrato = par.iContrato,
                                bPrioridad = 1
                            };
                        }
                        _prog.sRotacion = par.sRotacion;
                        if (_rot.cFactor == "M")
                        {
                            _prog.dDesde = (new DateTime(dHoy.Year, dHoy.Month, 1)).AddDays((double)_rot.bCoeficiente);
                            _prog.dHasta = ((DateTime)_prog.dDesde).AddMonths(1).AddDays(-1);
                        }
                        else if (_rot.cFactor == "X")
                        {
                            _prog.dDesde = _ctt.dDesde;
                            _prog.dHasta = _ctt.dHasta;
                        }
                        if (_prog.iProgramacion == rsp)
                        {
                            dbN.horProgramacion.Add(_prog);
                        }
                        else
                        {
                            dbN.Entry(_prog).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                        }
                        dbN.SaveChanges();
                    }
                }
            }
            return _prog;
        }

        [HttpGet("prueba")]
        public dynamic GetTrabajadores()
        {
             dynamic rsp = null;
            string SesionPersona = HttpContext.Request.Headers["sesionPersona"];
            Guid? gSesionPersona = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            short? sServidor = Procs.HoldingSesion(gSesionPersona);
            if (sServidor != null)
            {
                using (NominusContext dbN = new NominusContext((short)sServidor))
                {
                    rsp = Procs.GetTrabajadores(dbN);
                }
            }
            return rsp;
        }

        [HttpGet("tipologia")]
        public TipProgramacion tipologiaProgramacion()
        {
            TipProgramacion rsp = new TipProgramacion();
            string SesionPersona = HttpContext.Request.Headers["sesionPersona"];
            Guid? gSesionPersona = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            short? sServidor = Procs.HoldingSesion(gSesionPersona);
            if (sServidor != null)
            {
                using (NominusContext dbN = new NominusContext((short)sServidor))
                {
                    rsp.tipoRotacion = dbN.horTipoRotacion.ToList();
                    rsp.mesesProgramados = dbN.horProgramacion
                        .Select(s => new HorMesProgramado
                        {
                            sRotacion = (short)s.sRotacion,
                            dDesde = (DateTime)s.dDesde
                        }).Distinct().ToList();
                }
            }
            return rsp;
        }

    }

    public class PerJefe
    {
        [Key] public int iPersona { get; set; }
        public string xNombre { get; set; }
        public List<horTipoRotacion> tiposRotacion { get; set; }
    }

    public class parJefeEmpresa
    {
        public short sEmpresa { get; set; }
        public int iJefe { get; set; }
    }

    public class parContratoRotacion
    {
        public int iContrato { get; set; }
        public short sRotacion { get; set; }
        public DateTime? dFecha { get; set; }
    }

    public class PerColaborador:  traStatusContrato
    {
        public string xColaborador { get; set; }
        public string xSede { get; set; }
        public string xCargo { get; set; }
        public int? iJefe { get; set; }
        public HorProgramacion programacion { get; set; }
    }

    public class HorProgramacion: horProgramacion
    {
        public IEnumerable<horDetProgramacion> detalle { get; set; }
    }

    public class HorMesProgramado
    {
        public short sRotacion { get; set; }
        public DateTime dDesde { get; set; }
    }

    public class TipProgramacion
    {
        public List<horTipoRotacion> tipoRotacion { get; set; }
        public List<HorMesProgramado> mesesProgramados { get; set; }
    }
}
