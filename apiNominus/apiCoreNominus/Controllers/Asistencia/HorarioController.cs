﻿using apiCoreNominus.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace apiCoreNominus.Controllers.Asistencia
{
    [Route("horario")]
    [ApiController]
    public class HorarioController : ControllerBase
    {

        [HttpGet, Route("log/{sServidor}")]
        public List<acc_monitor_log> GetLogMarcaciones(short sServidor)
        {
            List<acc_monitor_log> rsp = null;
            using (MarcaZkContext dbZ = new MarcaZkContext(sServidor))
            {
                rsp = dbZ.acc_monitor_log.Where(c => c.pin != "").ToList();
            }
            return rsp;
        }

        [HttpGet("lista/{sEmpresa}")]
        public List<horHorario_> listaHorario(int sEmpresa)
        {
            List<horHorario_> rsp = new List<horHorario_>();
            string SesionPersona = HttpContext.Request.Headers["sesionPersona"];
            Guid? gSesionPersona = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            short? sServidor = Procs.HoldingSesion(gSesionPersona);
            if (sServidor != null)
            {
                using (NominusContext dbN = new NominusContext((short)sServidor))
                {
                    if (dbN.horHorario.Any(f => f.sEmpresa == sEmpresa))
                    {
                        rsp = dbN.horHorario.Where(f => f.sEmpresa == sEmpresa)
                            .Select(s => new horHorario_
                            {
                                sEmpresa = s.sEmpresa,
                                sHorario = s.sHorario,
                                xHorario = s.xHorario,
                                lBorrable = !dbN.horJornada.Any(j => j.sHorario == s.sHorario)
                            })
                            .ToList();
                    }
                }
            }
            return rsp;
        }

        [HttpGet("tipologia/{sServidor}")]
        public tipHorario tipologiaHorario(short sServidor)
        {
            tipHorario rsp = new tipHorario();
            using (NominusContext dbN = new NominusContext(sServidor))
            {
                rsp.margenes = dbN.horMargen.ToList();
                rsp.requerimientosMarca = dbN.horRequerimientoMarca.ToList();
            }
            return rsp;
        }

        [HttpPost("agregar")]
        public horHorario addHorario(parNuevoHorario pHorario)
        {
            horHorario nvo = null;
            using (NominusContext dbN = new NominusContext(pHorario.sServidor))
            {
                nvo = new horHorario { sEmpresa = pHorario.sEmpresa, xHorario = pHorario.xHorario };
                dbN.horHorario.Add(nvo);
                dbN.SaveChanges();
                short trt = 0;
                if (dbN.horTipoRotacion.Count() > 0)
                {
                    trt = dbN.horTipoRotacion.Max(tr => tr.sRotacion);
                    trt++;
                }
                dbN.horTipoRotacion.Add(new horTipoRotacion
                {
                    sRotacion=trt,
                    cFactor ="X",
                    bCoeficiente=0,
                    bFactor=0,
                    sHorario = nvo.sHorario,
                    cDescriptor = nvo.xHorario
                });
                dbN.SaveChanges();                
            }
            return nvo;
        }

        [HttpPost("modificar")]
        public horHorario modHorario(parModifHorario pHorario)
        {
            horHorario modh = new horHorario { sEmpresa = pHorario.sEmpresa, sHorario = pHorario.sHorario, xHorario = pHorario.xHorario };
            using (NominusContext dbN = new NominusContext(pHorario.sServidor))
            {
                dbN.Entry(modh).State = EntityState.Modified;
                dbN.SaveChanges();
            }
            return modh;
        }

        [HttpPost("eliminar")]
        public bool delHorario(parModifHorario pHorario)
        {
            horHorario modh = new horHorario { sEmpresa = pHorario.sEmpresa, sHorario = pHorario.sHorario, xHorario = pHorario.xHorario };
            using (NominusContext dbN = new NominusContext(pHorario.sServidor))
            {
                dbN.horHorario.Remove(modh);
                dbN.SaveChanges();
            }
            return true;
        }

        [HttpPost("jornadas")]
        public List<horJornada_> listaHorario(parHorario horario)
        {
            return Procs.asiListaHorario(horario);
        }

        [HttpPost("actualizarJornadas")]
        public List<horJornada_> actualizarJornadas(parActualizarJornadas horario)
        {
            using (NominusContext dbN = new NominusContext(horario.sServidor))
            {
                foreach (horJornada_ jornada in horario.jornadas)
                {
                    if (jornada.sJornada >= 16000)
                    {
                        //int cHj = dbN.horJornada.Count();
                        //if (cHj > 0)
                        //{
                            jornada.sJornada = dbN.horJornada.Max(c => c.sJornada);
                            jornada.sJornada++;
                        //}
                        //else
                        //{
                        //    jornada.sJornada = (short)1;
                        //}
                        dbN.horJornada.Add(jornada);
                        dbN.SaveChanges();
                        foreach (horControl_ control in jornada.controles)
                        {
                            control.sJornada = jornada.sJornada;
                        }
                    }
                    else
                    {
                        dbN.Entry(jornada).State = EntityState.Modified;
                    }
                    foreach (horControl_ control in jornada.controles)
                    {
                        if (control.sControl >= 16000)
                        {
                            if (dbN.horControl.Count() > 0)
                            {
                                control.sControl = dbN.horControl.Max(c => c.sControl);
                                control.sControl++;
                            }
                            else
                            {
                                control.sControl = 1;
                            }
                            dbN.horControl.Add(control);
                            dbN.SaveChanges();
                        }
                        else
                        {
                            dbN.Entry(control).State = EntityState.Modified;
                        }
                    }
                }
                dbN.SaveChanges();
            }

            return Procs.asiListaHorario(horario);
        }

        [HttpPost("eventos")]
        public List<horMargen_> listaEventos(parEvento evento)
        {
            List<horMargen_> rsp = new List<horMargen_>();
            using (NominusContext dbN = new NominusContext(evento.sServidor))
            {
                rsp = dbN.horMargen.Where(h => h.cTipoControl == evento.cTipoControl).Select(c => new horMargen_
                {
                    sMargen = c.sMargen,
                    cTipoControl = c.cTipoControl,
                    xMargen = c.xMargen,
                    lBorrable = !dbN.horControl.Any(d => d.sMargen == c.sMargen) && !dbN.horMargenIncidencia.Any(d => d.sMargen == c.sMargen)
                }).ToList();
            }
            return rsp;
        }

        [HttpPost("evento/agregar")]
        public horMargen addMargen(parNuevoEvento pEvento)
        {
            horMargen nvo = null;
            using (NominusContext dbN = new NominusContext(pEvento.sServidor))
            {
                short sigMargen = dbN.horMargen.Max(c => c.sMargen);
                sigMargen++;
                nvo = new horMargen
                {
                    xMargen = pEvento.xMargen,
                    cTipoControl = pEvento.cTipoControl,
                    sMargen = sigMargen
                };
                dbN.Add(nvo);
                dbN.SaveChanges();
            }
            return nvo;
        }

        [HttpPost("evento/modificar")]
        public horMargen modMargen(parModifEvento pEvento)
        {
            horMargen modm = new horMargen { sMargen = pEvento.sMargen, xMargen = pEvento.xMargen, cTipoControl = pEvento.cTipoControl };
            using (NominusContext dbN = new NominusContext(pEvento.sServidor))
            {
                dbN.Entry(modm).State = EntityState.Modified;
                dbN.SaveChanges();
            }
            return modm;
        }
        [HttpPost("evento/eliminar")]
        public bool delMargen(parModifEvento pEvento)
        {
            horMargen modm = new horMargen { sMargen = pEvento.sMargen, xMargen = pEvento.xMargen, cTipoControl = pEvento.cTipoControl };
            using (NominusContext dbN = new NominusContext(pEvento.sServidor))
            {
                dbN.horMargen.Remove(modm);
                dbN.SaveChanges();
            }
            return true;
        }

        [HttpGet("incidencias/{sServidor}")]
        public HorIncidencias lstIncidencias(short sServidor)
        {
            HorIncidencias rsp = null;
            using (NominusContext dbN = new NominusContext(sServidor))
            {

                rsp = new HorIncidencias();
                rsp.incidencias = dbN.horIncidencia.ToList();
                rsp.tiposPapeleta = dbN.asiTipoPapeleta.ToList();

            }
            return rsp;
        }
    }

}
