﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class auth_user
    {
        public int id { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public int? Status { get; set; }
        public DateTime? last_login { get; set; }
        public int? RoleID { get; set; }
        public string Remark { get; set; }
    }
}
