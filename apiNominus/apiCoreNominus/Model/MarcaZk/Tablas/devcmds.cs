﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class devcmds
    {
        public int id { get; set; }
        public string change_operator { get; set; }
        public DateTime? change_time { get; set; }
        public string create_operator { get; set; }
        public DateTime? create_time { get; set; }
        public string delete_operator { get; set; }
        public DateTime? delete_time { get; set; }
        public int status { get; set; }
        public int? SN_id { get; set; }
        public int? CmdOperate_id { get; set; }
        public string CmdContent { get; set; }
        public DateTime CmdCommitTime { get; set; }
        public DateTime? CmdTransTime { get; set; }
        public DateTime? CmdOverTime { get; set; }
        public int? CmdReturn { get; set; }
        public string CmdReturnContent { get; set; }
        public bool? CmdImmediately { get; set; }
    }
}
