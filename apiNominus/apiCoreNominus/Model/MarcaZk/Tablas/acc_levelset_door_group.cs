﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class acc_levelset_door_group
    {
        public int id { get; set; }
        public int acclevelset_id { get; set; }
        public int accdoor_id { get; set; }
        public int? accdoor_no_exp { get; set; }
        public int? accdoor_device_id { get; set; }
        public int? level_timeseg_id { get; set; }
    }
}
