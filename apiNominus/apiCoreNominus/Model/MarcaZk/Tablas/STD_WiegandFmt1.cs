﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class STD_WiegandFmt1
    {
        public int id { get; set; }
        public int? DoorId { get; set; }
        public string OutWiegandFmt { get; set; }
        public int? OutFailureId { get; set; }
        public int? OutAreaCode { get; set; }
        public int? OutPulseWidth { get; set; }
        public int? OutPulseInterval { get; set; }
        public int? OutContent { get; set; }
        public string InWiegandFmt { get; set; }
        public int? InBitCount { get; set; }
        public int? InPulseWidth { get; set; }
        public int? InPulseInterval { get; set; }
        public int? InContent { get; set; }
    }
}
