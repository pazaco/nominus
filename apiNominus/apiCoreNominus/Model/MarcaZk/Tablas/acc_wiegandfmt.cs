﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class acc_wiegandfmt
    {
        public int id { get; set; }
        public string change_operator { get; set; }
        public DateTime? change_time { get; set; }
        public string create_operator { get; set; }
        public DateTime? create_time { get; set; }
        public string delete_operator { get; set; }
        public DateTime? delete_time { get; set; }
        public int? status { get; set; }
        public string wiegand_name { get; set; }
        public int? wiegand_count { get; set; }
        public int? odd_start { get; set; }
        public int? odd_count { get; set; }
        public int? even_start { get; set; }
        public int? even_count { get; set; }
        public int? cid_start { get; set; }
        public int? cid_count { get; set; }
        public int? comp_start { get; set; }
        public int? comp_count { get; set; }
    }
}
