﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class base_basecode
    {
        public int id { get; set; }
        public string change_operator { get; set; }
        public DateTime? change_time { get; set; }
        public string create_operator { get; set; }
        public DateTime? create_time { get; set; }
        public string delete_operator { get; set; }
        public DateTime? delete_time { get; set; }
        public int? status { get; set; }
        public string content { get; set; }
        public int? content_class { get; set; }
        public string display { get; set; }
        public string value { get; set; }
        public string remark { get; set; }
        public string is_add { get; set; }
    }
}
