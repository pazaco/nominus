﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class att_attreport
    {
        public string ItemName { get; set; }
        public string ItemType { get; set; }
        public int? Author_id { get; set; }
        public string ItemValue { get; set; }
        public int? Published { get; set; }
    }
}
