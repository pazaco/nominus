﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class TmpPermitUsers
    {
        public int id { get; set; }
        public int? GroupId { get; set; }
        public string CertifiNo { get; set; }
        public string UserName { get; set; }
        public string Gender { get; set; }
        public string CardNo { get; set; }
        public DateTime? OfflineBeginDate { get; set; }
        public DateTime? OfflineEndDate { get; set; }
    }
}
