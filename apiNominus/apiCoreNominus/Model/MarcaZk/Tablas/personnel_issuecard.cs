﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class personnel_issuecard
    {
        public int id { get; set; }
        public string change_operator { get; set; }
        public DateTime? change_time { get; set; }
        public string create_operator { get; set; }
        public DateTime? create_time { get; set; }
        public string delete_operator { get; set; }
        public DateTime? delete_time { get; set; }
        public int? status { get; set; }
        public int? UserID_id { get; set; }
        public string cardno { get; set; }
        public DateTime? effectivenessdate { get; set; }
        public bool? isvalid { get; set; }
        public string cardpwd { get; set; }
        public DateTime? failuredate { get; set; }
        public string cardstatus { get; set; }
        public DateTime? issuedate { get; set; }
    }
}
