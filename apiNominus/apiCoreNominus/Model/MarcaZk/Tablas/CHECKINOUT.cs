﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class CHECKINOUT
    {
        public int USERID { get; set; }
        public DateTime CHECKTIME { get; set; }
        public string CHECKTYPE { get; set; }
        public int? VERIFYCODE { get; set; }
        public string SENSORID { get; set; }
        public int LOGID { get; set; }
        public int? MachineId { get; set; }
        public int? UserExtFmt { get; set; }
        public int? WorkCode { get; set; }
        public string Memoinfo { get; set; }
        public string sn { get; set; }
    }
}
