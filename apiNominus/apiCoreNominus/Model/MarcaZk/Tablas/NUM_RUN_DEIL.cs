﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class NUM_RUN_DEIL
    {
        public short NUM_RUNID { get; set; }
        public DateTime STARTTIME { get; set; }
        public DateTime? ENDTIME { get; set; }
        public short SDAYS { get; set; }
        public short? EDAYS { get; set; }
        public int? SCHCLASSID { get; set; }
        public short? OverTime { get; set; }
    }
}
