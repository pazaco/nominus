﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class OfflinePermitDoors
    {
        public int id { get; set; }
        public int? GroupId { get; set; }
        public int? DeviceId { get; set; }
        public int? DoorNo { get; set; }
    }
}
