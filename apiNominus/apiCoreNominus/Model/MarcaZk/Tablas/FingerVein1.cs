﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class FingerVein1
    {
        public int FVID { get; set; }
        public int? UserID { get; set; }
        public int? FingerID { get; set; }
        public byte[] Template { get; set; }
        public int? Size { get; set; }
        public int? DuressFlag { get; set; }
        public string UserCode { get; set; }
        public int? Fv_ID_Index { get; set; }
    }
}
