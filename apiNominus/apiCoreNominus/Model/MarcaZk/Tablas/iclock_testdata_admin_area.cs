﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class iclock_testdata_admin_area
    {
        public int id { get; set; }
        public int? testdata_id { get; set; }
        public int? area_id { get; set; }
    }
}
