﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class ServerLog
    {
        public int ID { get; set; }
        public string EVENT { get; set; }
        public int USERID { get; set; }
        public string EnrollNumber { get; set; }
        public short? parameter { get; set; }
        public DateTime EVENTTIME { get; set; }
        public string SENSORID { get; set; }
        public string OPERATOR { get; set; }
    }
}
