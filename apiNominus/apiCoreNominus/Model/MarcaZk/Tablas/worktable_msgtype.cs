﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class worktable_msgtype
    {
        public int id { get; set; }
        public string change_operator { get; set; }
        public DateTime? change_time { get; set; }
        public string create_operator { get; set; }
        public DateTime? create_time { get; set; }
        public string delete_operator { get; set; }
        public DateTime? delete_time { get; set; }
        public int? status { get; set; }
        public string msgtype_name { get; set; }
        public string msgtype_value { get; set; }
        public int? msg_keep_time { get; set; }
        public int? msg_ahead_remind { get; set; }
        public string type { get; set; }
    }
}
