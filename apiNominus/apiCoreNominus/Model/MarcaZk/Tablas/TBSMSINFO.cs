﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class TBSMSINFO
    {
        public int REFERENCE { get; set; }
        public string SMSID { get; set; }
        public int SMSINDEX { get; set; }
        public int? SMSTYPE { get; set; }
        public string SMSCONTENT { get; set; }
        public string SMSSTARTTM { get; set; }
        public int? SMSTMLENG { get; set; }
        public string GENTM { get; set; }
    }
}
