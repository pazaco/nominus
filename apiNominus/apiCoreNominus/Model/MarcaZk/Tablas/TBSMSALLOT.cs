﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class TBSMSALLOT
    {
        public int REFERENCE { get; set; }
        public int SMSREF { get; set; }
        public int USERREF { get; set; }
        public string GENTM { get; set; }
    }
}
