﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class acc_holidays
    {
        public int id { get; set; }
        public string change_operator { get; set; }
        public DateTime? change_time { get; set; }
        public string create_operator { get; set; }
        public DateTime? create_time { get; set; }
        public string delete_operator { get; set; }
        public DateTime? delete_time { get; set; }
        public int status { get; set; }
        public string holiday_name { get; set; }
        public int? holiday_type { get; set; }
        public DateTime start_date { get; set; }
        public DateTime end_date { get; set; }
        public int? loop_by_year { get; set; }
        public string memo { get; set; }
        public int? HolidayNo { get; set; }
        public int? HolidayTZ { get; set; }
    }
}
