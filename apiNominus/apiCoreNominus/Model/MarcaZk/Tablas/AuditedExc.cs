﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class AuditedExc
    {
        public int AEID { get; set; }
        public int? UserId { get; set; }
        public DateTime CheckTime { get; set; }
        public int? NewExcID { get; set; }
        public short? IsLeave { get; set; }
        public string UName { get; set; }
        public DateTime UTime { get; set; }
    }
}
