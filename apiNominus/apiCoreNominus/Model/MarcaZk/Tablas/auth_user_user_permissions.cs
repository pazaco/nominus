﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class auth_user_user_permissions
    {
        public int id { get; set; }
        public int? user_id { get; set; }
        public int? permission_id { get; set; }
    }
}
