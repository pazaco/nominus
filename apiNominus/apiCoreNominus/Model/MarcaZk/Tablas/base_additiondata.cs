﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class base_additiondata
    {
        public int id { get; set; }
        public DateTime? create_time { get; set; }
        public int? user_id { get; set; }
        public int? content_type_id { get; set; }
        public string object_id { get; set; }
        public string key { get; set; }
        public string value { get; set; }
        public string data { get; set; }
        public DateTime? delete_time { get; set; }
    }
}
