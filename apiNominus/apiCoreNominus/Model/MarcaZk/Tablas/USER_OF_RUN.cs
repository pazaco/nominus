﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class USER_OF_RUN
    {
        public int USERID { get; set; }
        public int NUM_OF_RUN_ID { get; set; }
        public DateTime STARTDATE { get; set; }
        public DateTime ENDDATE { get; set; }
        public int? ISNOTOF_RUN { get; set; }
        public int? ORDER_RUN { get; set; }
    }
}
