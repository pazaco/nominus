﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class OfflinePermitUsers
    {
        public int id { get; set; }
        public int? GroupId { get; set; }
        public string Pin { get; set; }
        public bool? ChkPwd { get; set; }
        public bool? ChkFp { get; set; }
    }
}
