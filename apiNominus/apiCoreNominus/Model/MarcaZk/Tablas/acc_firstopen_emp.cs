﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class acc_firstopen_emp
    {
        public int id { get; set; }
        public int? accfirstopen_id { get; set; }
        public int? employee_id { get; set; }
    }
}
