﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class auth_message
    {
        public int id { get; set; }
        public int? user_id { get; set; }
        public string message { get; set; }
    }
}
