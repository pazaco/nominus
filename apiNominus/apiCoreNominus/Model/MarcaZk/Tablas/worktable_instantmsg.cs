﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class worktable_instantmsg
    {
        public int id { get; set; }
        public string change_operator { get; set; }
        public DateTime? change_time { get; set; }
        public string create_operator { get; set; }
        public DateTime? create_time { get; set; }
        public string delete_operator { get; set; }
        public DateTime? delete_time { get; set; }
        public int? status { get; set; }
        public DateTime? monitor_last_time { get; set; }
        public int? msgtype_id { get; set; }
        public string content { get; set; }
    }
}
