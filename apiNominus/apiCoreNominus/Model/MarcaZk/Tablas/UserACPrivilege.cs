﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class UserACPrivilege
    {
        public int UserID { get; set; }
        public short ACGroupID { get; set; }
        public bool IsUseGroup { get; set; }
        public short? TimeZone1 { get; set; }
        public short? TimeZone2 { get; set; }
        public short? TimeZone3 { get; set; }
    }
}
