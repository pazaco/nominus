﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class HOLIDAYS
    {
        public int HOLIDAYID { get; set; }
        public string HOLIDAYNAME { get; set; }
        public short? HOLIDAYYEAR { get; set; }
        public short? HOLIDAYMONTH { get; set; }
        public short? HOLIDAYDAY { get; set; }
        public DateTime? STARTTIME { get; set; }
        public short? DURATION { get; set; }
        public short? HOLIDAYTYPE { get; set; }
        public string XINBIE { get; set; }
        public string MINZU { get; set; }
        public short? DeptID { get; set; }
    }
}
