﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class acc_morecardempgroup
    {
        public int id { get; set; }
        public string change_operator { get; set; }
        public DateTime? change_time { get; set; }
        public string create_operator { get; set; }
        public DateTime? create_time { get; set; }
        public string delete_operator { get; set; }
        public DateTime? delete_time { get; set; }
        public int status { get; set; }
        public string group_name { get; set; }
        public string memo { get; set; }
        public int? StdGroupNo { get; set; }
        public int? StdGroupTz { get; set; }
        public int? StdGroupVT { get; set; }
        public bool? StdValidOnHoliday { get; set; }
    }
}
