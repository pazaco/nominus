﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class ParamSet
    {
        public string ParamName { get; set; }
        public string ParamValue { get; set; }
    }
}
