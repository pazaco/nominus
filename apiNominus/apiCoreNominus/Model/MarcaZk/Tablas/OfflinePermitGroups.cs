﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class OfflinePermitGroups
    {
        public int id { get; set; }
        public string GroupName { get; set; }
        public DateTime? BeginDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
