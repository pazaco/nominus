﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class TEMPLATEEx
    {
        public int TEMPLATEID { get; set; }
        public int USERID { get; set; }
        public int FINGERID { get; set; }
        public byte[] TEMPLATE { get; set; }
        public byte[] TEMPLATE2 { get; set; }
        public byte[] BITMAPPICTURE { get; set; }
        public byte[] BITMAPPICTURE2 { get; set; }
        public byte[] BITMAPPICTURE3 { get; set; }
        public byte[] BITMAPPICTURE4 { get; set; }
        public short? USETYPE { get; set; }
        public string EMACHINENUM { get; set; }
        public byte[] TEMPLATE1 { get; set; }
        public short? Flag { get; set; }
        public short? DivisionFP { get; set; }
        public byte[] TEMPLATE4 { get; set; }
        public byte[] TEMPLATE3 { get; set; }
        public string change_operator { get; set; }
        public DateTime? change_time { get; set; }
        public string create_operator { get; set; }
        public DateTime? create_time { get; set; }
        public string delete_operator { get; set; }
        public DateTime? delete_time { get; set; }
        public int? status { get; set; }
        public int? Valid { get; set; }
        public string Fpversion { get; set; }
        public int? bio_type { get; set; }
        public int? SN { get; set; }
        public DateTime? UTime { get; set; }
        public string StateMigrationFlag { get; set; }
    }
}
