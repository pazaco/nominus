﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class personnel_empchange
    {
        public string change_operator { get; set; }
        public DateTime? change_time { get; set; }
        public string create_operator { get; set; }
        public DateTime? create_time { get; set; }
        public string delete_operator { get; set; }
        public DateTime? delete_time { get; set; }
        public int? status { get; set; }
        public int changeno { get; set; }
        public int? UserID_id { get; set; }
        public DateTime? changedate { get; set; }
        public int? changepostion { get; set; }
        public string oldvalue { get; set; }
        public string newvalue { get; set; }
        public string changereason { get; set; }
        public bool? isvalid { get; set; }
        public int? approvalstatus { get; set; }
        public string remark { get; set; }
    }
}
