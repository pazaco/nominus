﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class SECURITYDETAILS
    {
        public int SECURITYDETAILID { get; set; }
        public short? USERID { get; set; }
        public short? DEPTID { get; set; }
        public short? SCHEDULE { get; set; }
        public short? USERINFO { get; set; }
        public short? ENROLLFINGERS { get; set; }
        public short? REPORTVIEW { get; set; }
        public string REPORT { get; set; }
    }
}
