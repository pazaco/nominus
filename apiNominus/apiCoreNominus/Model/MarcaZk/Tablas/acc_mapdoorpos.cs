﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class acc_mapdoorpos
    {
        public int id { get; set; }
        public string change_operator { get; set; }
        public DateTime? change_time { get; set; }
        public string create_operator { get; set; }
        public DateTime? create_time { get; set; }
        public string delete_operator { get; set; }
        public DateTime? delete_time { get; set; }
        public int status { get; set; }
        public int? map_door_id { get; set; }
        public int? map_id { get; set; }
        public int? width { get; set; }
        public int? left { get; set; }
        public int? top { get; set; }
    }
}
