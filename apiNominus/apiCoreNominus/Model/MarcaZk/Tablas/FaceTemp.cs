﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class FaceTemp
    {
        public int TEMPLATEID { get; set; }
        public string USERNO { get; set; }
        public int? SIZE { get; set; }
        public int? pin { get; set; }
        public int? FACEID { get; set; }
        public int? VALID { get; set; }
        public int? RESERVE { get; set; }
        public int? ACTIVETIME { get; set; }
        public int? VFCOUNT { get; set; }
        public byte[] TEMPLATE { get; set; }
        public byte? FaceType { get; set; }
        public string StateMigrationFlag { get; set; }
    }
}
