﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class CustomReport1
    {
        public int id { get; set; }
        public string ReportName { get; set; }
        public string Memo { get; set; }
    }
}
