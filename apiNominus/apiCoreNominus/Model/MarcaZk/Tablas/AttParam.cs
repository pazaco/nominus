﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class AttParam
    {
        public string PARANAME { get; set; }
        public string PARATYPE { get; set; }
        public string PARAVALUE { get; set; }
    }
}
