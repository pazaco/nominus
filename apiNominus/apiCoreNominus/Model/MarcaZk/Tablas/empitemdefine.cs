﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class empitemdefine
    {
        public string ItemName { get; set; }
        public string ItemType { get; set; }
        public int? Author_id { get; set; }
        public string ItemValue { get; set; }
        public short? Published { get; set; }
    }
}
