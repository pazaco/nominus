﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class iclock_testdata_admin_dept
    {
        public int id { get; set; }
        public int? testdata_id { get; set; }
        public int? department_id { get; set; }
    }
}
