﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class BioTemplate
    {
        public int UserID { get; set; }
        public string BadgeNumber { get; set; }
        public string CreateOperator { get; set; }
        public DateTime? CreateTime { get; set; }
        public string ValidFlag { get; set; }
        public string IsDuress { get; set; }
        public int BioType { get; set; }
        public string Version { get; set; }
        public int? DataFormat { get; set; }
        public int TemplateNO { get; set; }
        public int TemplateNOIndex { get; set; }
        public int? nOldType { get; set; }
        public string TemplateData { get; set; }
        public int ID { get; set; }
    }
}
