﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class action_log
    {
        public int id { get; set; }
        public DateTime? action_time { get; set; }
        public int? user_id { get; set; }
        public int? content_type_id { get; set; }
        public int? object_id { get; set; }
        public string object_repr { get; set; }
        public int? action_flag { get; set; }
        public string change_message { get; set; }
    }
}
