﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class devlog
    {
        public int id { get; set; }
        public int? SN_id { get; set; }
        public string OP { get; set; }
        public string Object { get; set; }
        public int Cnt { get; set; }
        public int ECnt { get; set; }
        public DateTime OpTime { get; set; }
    }
}
