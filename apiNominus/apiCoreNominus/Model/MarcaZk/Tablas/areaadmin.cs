﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class areaadmin
    {
        public int id { get; set; }
        public int? user_id { get; set; }
        public int? area_id { get; set; }
    }
}
