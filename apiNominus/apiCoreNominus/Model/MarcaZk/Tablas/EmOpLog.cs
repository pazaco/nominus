﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class EmOpLog
    {
        public int ID { get; set; }
        public int USERID { get; set; }
        public DateTime OperateTime { get; set; }
        public int? manipulationID { get; set; }
        public int? Params1 { get; set; }
        public int? Params2 { get; set; }
        public int? Params3 { get; set; }
        public int? Params4 { get; set; }
        public string SensorId { get; set; }
    }
}
