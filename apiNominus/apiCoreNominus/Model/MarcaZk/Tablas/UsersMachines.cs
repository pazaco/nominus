﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class UsersMachines
    {
        public int ID { get; set; }
        public int USERID { get; set; }
        public int DEVICEID { get; set; }
    }
}
