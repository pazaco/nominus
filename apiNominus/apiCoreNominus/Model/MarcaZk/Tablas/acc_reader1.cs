﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class acc_reader1
    {
        public long id { get; set; }
        public int? door_id { get; set; }
        public int? reader_no { get; set; }
        public string reader_name { get; set; }
        public int? reader_state { get; set; }
    }
}
