﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class CHECKEXACT
    {
        public int EXACTID { get; set; }
        public int? USERID { get; set; }
        public DateTime? CHECKTIME { get; set; }
        public string CHECKTYPE { get; set; }
        public short? ISADD { get; set; }
        public string YUYIN { get; set; }
        public short? ISMODIFY { get; set; }
        public short? ISDELETE { get; set; }
        public short? INCOUNT { get; set; }
        public short? ISCOUNT { get; set; }
        public string MODIFYBY { get; set; }
        public DateTime? DATE { get; set; }
    }
}
