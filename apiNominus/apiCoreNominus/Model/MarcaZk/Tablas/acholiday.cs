﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class acholiday
    {
        public int primaryid { get; set; }
        public int? holidayid { get; set; }
        public DateTime? begindate { get; set; }
        public DateTime? enddate { get; set; }
        public int? timezone { get; set; }
    }
}
