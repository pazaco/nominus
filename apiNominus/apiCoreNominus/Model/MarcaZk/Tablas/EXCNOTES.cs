﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class EXCNOTES
    {
        public int? USERID { get; set; }
        public DateTime? ATTDATE { get; set; }
        public string NOTES { get; set; }
    }
}
