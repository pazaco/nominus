﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class DeptUsedSchs
    {
        public int DeptId { get; set; }
        public int SchId { get; set; }
    }
}
