﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class ReaderProperty
    {
        public int id { get; set; }
        public int? devid { get; set; }
        public int? doorid { get; set; }
        public int? type { get; set; }
        public int? inout { get; set; }
        public int? address { get; set; }
        public string ipaddress { get; set; }
        public int? port { get; set; }
        public string mac { get; set; }
        public int? disable { get; set; }
        public int? verifytype { get; set; }
        public string multicast { get; set; }
        public int? offlinerefuse { get; set; }
        public string create_operator { get; set; }
    }
}
