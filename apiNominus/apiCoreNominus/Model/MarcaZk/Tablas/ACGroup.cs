﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class ACGroup
    {
        public short GroupID { get; set; }
        public string Name { get; set; }
        public short? TimeZone1 { get; set; }
        public short? TimeZone2 { get; set; }
        public short? TimeZone3 { get; set; }
        public bool holidayvaild { get; set; }
        public int? verifystyle { get; set; }
    }
}
