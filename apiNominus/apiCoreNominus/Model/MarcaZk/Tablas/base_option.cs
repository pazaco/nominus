﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class base_option
    {
        public int id { get; set; }
        public string change_operator { get; set; }
        public DateTime? change_time { get; set; }
        public string create_operator { get; set; }
        public DateTime? create_time { get; set; }
        public string delete_operator { get; set; }
        public DateTime? delete_time { get; set; }
        public int? status { get; set; }
        public string name { get; set; }
        public string app_label { get; set; }
        public string catalog { get; set; }
        public string group { get; set; }
        public string widget { get; set; }
        public bool? required { get; set; }
        public string validator { get; set; }
        public string verbose_name { get; set; }
        public string help_text { get; set; }
        public bool? visible { get; set; }
        public string _default { get; set; }
        public bool? read_only { get; set; }
        public bool? for_personal { get; set; }
        public string type { get; set; }
    }
}
