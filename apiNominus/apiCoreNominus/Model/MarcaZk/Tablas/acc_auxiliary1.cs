﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class acc_auxiliary1
    {
        public long id { get; set; }
        public int? device_id { get; set; }
        public int? aux_no { get; set; }
        public string printer_number { get; set; }
        public string aux_name { get; set; }
        public int? aux_state { get; set; }
    }
}
