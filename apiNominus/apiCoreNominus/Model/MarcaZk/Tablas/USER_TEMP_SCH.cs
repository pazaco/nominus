﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class USER_TEMP_SCH
    {
        public int USERID { get; set; }
        public DateTime COMETIME { get; set; }
        public DateTime LEAVETIME { get; set; }
        public int OVERTIME { get; set; }
        public short? TYPE { get; set; }
        public short? FLAG { get; set; }
        public int? SCHCLASSID { get; set; }
    }
}
