﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class USER_SPEDAY
    {
        public int USERID { get; set; }
        public DateTime STARTSPECDAY { get; set; }
        public DateTime? ENDSPECDAY { get; set; }
        public short DATEID { get; set; }
        public string YUANYING { get; set; }
        public DateTime? DATE { get; set; }
    }
}
