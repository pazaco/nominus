﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class AUTHDEVICE
    {
        public int ID { get; set; }
        public int USERID { get; set; }
        public int MachineID { get; set; }
    }
}
