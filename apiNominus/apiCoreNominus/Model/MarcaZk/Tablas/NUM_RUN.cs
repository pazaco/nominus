﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class NUM_RUN
    {
        public int NUM_RUNID { get; set; }
        public int? OLDID { get; set; }
        public string NAME { get; set; }
        public DateTime? STARTDATE { get; set; }
        public DateTime? ENDDATE { get; set; }
        public short? CYLE { get; set; }
        public short? UNITS { get; set; }
    }
}
