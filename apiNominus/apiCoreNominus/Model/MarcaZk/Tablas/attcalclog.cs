﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class attcalclog
    {
        public int id { get; set; }
        public int? DeptID { get; set; }
        public int? UserId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? OperTime { get; set; }
        public int? Type { get; set; }
    }
}
