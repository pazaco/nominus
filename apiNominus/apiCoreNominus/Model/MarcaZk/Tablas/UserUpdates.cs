﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class UserUpdates
    {
        public int UpdateId { get; set; }
        public string BadgeNumber { get; set; }
    }
}
