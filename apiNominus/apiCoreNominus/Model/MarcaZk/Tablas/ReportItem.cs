﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class ReportItem
    {
        public int RIID { get; set; }
        public int? RIIndex { get; set; }
        public short? ShowIt { get; set; }
        public string RIName { get; set; }
        public string UnitName { get; set; }
        public byte[] Formula { get; set; }
        public short? CalcBySchClass { get; set; }
        public short? StatisticMethod { get; set; }
        public short? CalcLast { get; set; }
        public byte[] Notes { get; set; }
    }
}
