﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class userinfo_attarea
    {
        public int id { get; set; }
        public int employee_id { get; set; }
        public int area_id { get; set; }
    }
}
