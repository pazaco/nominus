﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class deptadmin
    {
        public int id { get; set; }
        public int? user_id { get; set; }
        public int? dept_id { get; set; }
    }
}
