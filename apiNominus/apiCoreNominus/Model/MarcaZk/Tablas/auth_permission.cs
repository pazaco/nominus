﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class auth_permission
    {
        public int id { get; set; }
        public string name { get; set; }
        public int? content_type_id { get; set; }
        public string codename { get; set; }
    }
}
