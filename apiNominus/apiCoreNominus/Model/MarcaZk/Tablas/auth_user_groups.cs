﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class auth_user_groups
    {
        public int id { get; set; }
        public int? user_id { get; set; }
        public int? group_id { get; set; }
    }
}
