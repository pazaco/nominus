﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class LossCard
    {
        public int id { get; set; }
        public string Pin { get; set; }
        public string CardNo { get; set; }
        public DateTime? LossDate { get; set; }
    }
}
