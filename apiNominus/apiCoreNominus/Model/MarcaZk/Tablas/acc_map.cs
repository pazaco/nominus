﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class acc_map
    {
        public int id { get; set; }
        public string change_operator { get; set; }
        public DateTime? change_time { get; set; }
        public string create_operator { get; set; }
        public DateTime? create_time { get; set; }
        public string delete_operator { get; set; }
        public DateTime? delete_time { get; set; }
        public int status { get; set; }
        public string map_name { get; set; }
        public string map_path { get; set; }
        public int? area_id { get; set; }
        public int? width { get; set; }
        public int? height { get; set; }
    }
}
