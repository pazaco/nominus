﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class django_session
    {
        public string session_key { get; set; }
        public string session_data { get; set; }
        public DateTime expire_date { get; set; }
    }
}
