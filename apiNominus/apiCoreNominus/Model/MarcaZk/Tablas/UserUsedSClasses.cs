﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class UserUsedSClasses
    {
        public int id { get; set; }
        public int UserId { get; set; }
        public int? SchId { get; set; }
    }
}
