﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class ACUnlockComb
    {
        public short UnlockCombID { get; set; }
        public string Name { get; set; }
        public short? Group01 { get; set; }
        public short? Group02 { get; set; }
        public short? Group03 { get; set; }
        public short? Group04 { get; set; }
        public short? Group05 { get; set; }
    }
}
