﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class django_content_type
    {
        public int id { get; set; }
        public string name { get; set; }
        public string app_label { get; set; }
        public string model { get; set; }
    }
}
