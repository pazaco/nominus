﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class auth_group
    {
        public int id { get; set; }
        public string name { get; set; }
        public string Permission { get; set; }
        public string Remark { get; set; }
    }
}
