﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class ReportField1
    {
        public int CRId { get; set; }
        public string TableName { get; set; }
        public string FieldName { get; set; }
        public int ShowIndex { get; set; }
    }
}
