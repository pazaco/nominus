﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace apiCoreNominus.Model
{
    public partial class Procs
    {
        public static void MarSincronizarZk(List<short> servidores)
        {
            foreach (short sServidor in servidores) {
                using (MarcaZkContext dbz = new MarcaZkContext(sServidor)) {
                    if (dbz.isConnected())
                    {
                        using(NominusContext dbn = new NominusContext(sServidor)){
                            marPlataforma plataforma = dbn.marPlataforma.FirstOrDefault(c => c.bModo == 1);
                            List<ZkUnimarca> marcaslog = dbz.acc_monitor_log
                                .Where(c => c.time >= plataforma.dUltimaSincronizacion || plataforma.dUltimaSincronizacion == null)
                                .Select(ca => new ZkUnimarca
                                {
                                    CHECKTIME = (DateTime)ca.time,
                                    device_id = (int)ca.device_id,
                                    BadgeNumber = ca.pin
                                }).ToList();
                            List<ZkUnimarca> marcasCheck = dbz.CHECKINOUT
                                .Where(c => c.CHECKTIME >= plataforma.dUltimaSincronizacion || plataforma.dUltimaSincronizacion == null)
                                .Join(dbz.Machines, j1=>j1.sn, j2=>j2.sn,(j1,j2)=> new { chk = j1 , mch = j2 })
                                .Select(ca => new ZkUnimarca {
                                    CHECKTIME =  ca.chk.CHECKTIME,
                                    device_id = ca.mch.ID,
                                    BadgeNumber = ca.chk.sn
                                }).ToList();

                        }
                    }
                }
            }
        }
    }

    #region Outputs

    #endregion

    #region Inputs

    public class ZkUnimarca
    {
        public string BadgeNumber { get; set; }
        public DateTime CHECKTIME { get; set; }
        public int device_id { get; set; }
    }



    #endregion
}
