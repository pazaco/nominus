﻿using Microsoft.EntityFrameworkCore;

namespace apiCoreNominus.Model
{
    public partial class MarcaZkContext : DbContext
    {
        private short sServidor;
        private bool _Connected = false;
        public MarcaZkContext(short sServidor)
        {
            this.sServidor = sServidor;
        }

        public bool isConnected()
        {
            return _Connected;
        }

        public MarcaZkContext(DbContextOptions<MarcaZkContext> options)
            : base(options)
        {
        }
        public virtual DbSet<ACGroup> ACGroup { get; set; }
        public virtual DbSet<ACTimeZones> ACTimeZones { get; set; }
        public virtual DbSet<ACUnlockComb> ACUnlockComb { get; set; }
        public virtual DbSet<AUTHDEVICE> AUTHDEVICE { get; set; }
        public virtual DbSet<AlarmLog> AlarmLog { get; set; }
        public virtual DbSet<AttParam> AttParam { get; set; }
        public virtual DbSet<AuditedExc> AuditedExc { get; set; }
        public virtual DbSet<BioTemplate> BioTemplate { get; set; }
        public virtual DbSet<CHECKEXACT> CHECKEXACT { get; set; }
        public virtual DbSet<CHECKINOUT> CHECKINOUT { get; set; }
        public virtual DbSet<CustomReport> CustomReport { get; set; }
        public virtual DbSet<CustomReport1> CustomReport1 { get; set; }
        public virtual DbSet<DEPARTMENTS> DEPARTMENTS { get; set; }
        public virtual DbSet<DeptUsedSchs> DeptUsedSchs { get; set; }
        public virtual DbSet<EXCNOTES> EXCNOTES { get; set; }
        public virtual DbSet<EmOpLog> EmOpLog { get; set; }
        public virtual DbSet<FaceTemp> FaceTemp { get; set; }
        public virtual DbSet<FaceTempEx> FaceTempEx { get; set; }
        public virtual DbSet<FingerVein> FingerVein { get; set; }
        public virtual DbSet<FingerVein1> FingerVein1 { get; set; }
        public virtual DbSet<FingerVeinEx> FingerVeinEx { get; set; }
        public virtual DbSet<HOLIDAYS> HOLIDAYS { get; set; }
        public virtual DbSet<LeaveClass> LeaveClass { get; set; }
        public virtual DbSet<LeaveClass1> LeaveClass1 { get; set; }
        public virtual DbSet<LossCard> LossCard { get; set; }
        public virtual DbSet<Machines> Machines { get; set; }
        public virtual DbSet<NUM_RUN> NUM_RUN { get; set; }
        public virtual DbSet<NUM_RUN_DEIL> NUM_RUN_DEIL { get; set; }
        public virtual DbSet<OfflinePermitDoors> OfflinePermitDoors { get; set; }
        public virtual DbSet<OfflinePermitGroups> OfflinePermitGroups { get; set; }
        public virtual DbSet<OfflinePermitUsers> OfflinePermitUsers { get; set; }
        public virtual DbSet<ParamSet> ParamSet { get; set; }
        public virtual DbSet<ReaderProperty> ReaderProperty { get; set; }
        public virtual DbSet<ReportField> ReportField { get; set; }
        public virtual DbSet<ReportField1> ReportField1 { get; set; }
        public virtual DbSet<ReportItem> ReportItem { get; set; }
        public virtual DbSet<SECURITYDETAILS> SECURITYDETAILS { get; set; }
        public virtual DbSet<SHIFT> SHIFT { get; set; }
        public virtual DbSet<STD_WiegandFmt> STD_WiegandFmt { get; set; }
        public virtual DbSet<STD_WiegandFmt1> STD_WiegandFmt1 { get; set; }
        public virtual DbSet<SchClass> SchClass { get; set; }
        public virtual DbSet<ServerLog> ServerLog { get; set; }
        public virtual DbSet<SystemLog> SystemLog { get; set; }
        public virtual DbSet<TBKEY> TBKEY { get; set; }
        public virtual DbSet<TBSMSALLOT> TBSMSALLOT { get; set; }
        public virtual DbSet<TBSMSINFO> TBSMSINFO { get; set; }
        public virtual DbSet<TEMPLATE> TEMPLATE { get; set; }
        public virtual DbSet<TEMPLATEEx> TEMPLATEEx { get; set; }
        public virtual DbSet<TmpPermitDoors> TmpPermitDoors { get; set; }
        public virtual DbSet<TmpPermitGroups> TmpPermitGroups { get; set; }
        public virtual DbSet<TmpPermitUsers> TmpPermitUsers { get; set; }
        public virtual DbSet<USERINFO> USERINFO { get; set; }
        public virtual DbSet<USER_OF_RUN> USER_OF_RUN { get; set; }
        public virtual DbSet<USER_SPEDAY> USER_SPEDAY { get; set; }
        public virtual DbSet<USER_TEMP_SCH> USER_TEMP_SCH { get; set; }
        public virtual DbSet<UserACMachines> UserACMachines { get; set; }
        public virtual DbSet<UserACPrivilege> UserACPrivilege { get; set; }
        public virtual DbSet<UserUpdates> UserUpdates { get; set; }
        public virtual DbSet<UserUsedSClasses> UserUsedSClasses { get; set; }
        public virtual DbSet<UsersMachines> UsersMachines { get; set; }
        public virtual DbSet<ZKAttendanceMonthStatistics> ZKAttendanceMonthStatistics { get; set; }
        public virtual DbSet<acc_antiback> acc_antiback { get; set; }
        public virtual DbSet<acc_auxiliary> acc_auxiliary { get; set; }
        public virtual DbSet<acc_auxiliary1> acc_auxiliary1 { get; set; }
        public virtual DbSet<acc_door> acc_door { get; set; }
        public virtual DbSet<acc_firstopen> acc_firstopen { get; set; }
        public virtual DbSet<acc_firstopen_emp> acc_firstopen_emp { get; set; }
        public virtual DbSet<acc_holidays> acc_holidays { get; set; }
        public virtual DbSet<acc_interlock> acc_interlock { get; set; }
        public virtual DbSet<acc_levelset> acc_levelset { get; set; }
        public virtual DbSet<acc_levelset_door_group> acc_levelset_door_group { get; set; }
        public virtual DbSet<acc_levelset_emp> acc_levelset_emp { get; set; }
        public virtual DbSet<acc_linkageio> acc_linkageio { get; set; }
        public virtual DbSet<acc_map> acc_map { get; set; }
        public virtual DbSet<acc_mapdoorpos> acc_mapdoorpos { get; set; }
        public virtual DbSet<acc_monitor_log> acc_monitor_log { get; set; }
        public virtual DbSet<acc_morecardempgroup> acc_morecardempgroup { get; set; }
        public virtual DbSet<acc_morecardgroup> acc_morecardgroup { get; set; }
        public virtual DbSet<acc_morecardset> acc_morecardset { get; set; }
        public virtual DbSet<acc_reader> acc_reader { get; set; }
        public virtual DbSet<acc_reader1> acc_reader1 { get; set; }
        public virtual DbSet<acc_timeseg> acc_timeseg { get; set; }
        public virtual DbSet<acc_wiegandfmt> acc_wiegandfmt { get; set; }
        public virtual DbSet<acholiday> acholiday { get; set; }
        public virtual DbSet<action_log> action_log { get; set; }
        public virtual DbSet<areaadmin> areaadmin { get; set; }
        public virtual DbSet<att_attreport> att_attreport { get; set; }
        public virtual DbSet<att_waitforprocessdata> att_waitforprocessdata { get; set; }
        public virtual DbSet<attcalclog> attcalclog { get; set; }
        public virtual DbSet<attexception> attexception { get; set; }
        public virtual DbSet<auth_group> auth_group { get; set; }
        public virtual DbSet<auth_group_permissions> auth_group_permissions { get; set; }
        public virtual DbSet<auth_message> auth_message { get; set; }
        public virtual DbSet<auth_permission> auth_permission { get; set; }
        public virtual DbSet<auth_user> auth_user { get; set; }
        public virtual DbSet<auth_user_groups> auth_user_groups { get; set; }
        public virtual DbSet<auth_user_user_permissions> auth_user_user_permissions { get; set; }
        public virtual DbSet<base_additiondata> base_additiondata { get; set; }
        public virtual DbSet<base_appoption> base_appoption { get; set; }
        public virtual DbSet<base_basecode> base_basecode { get; set; }
        public virtual DbSet<base_datatranslation> base_datatranslation { get; set; }
        public virtual DbSet<base_operatortemplate> base_operatortemplate { get; set; }
        public virtual DbSet<base_option> base_option { get; set; }
        public virtual DbSet<base_personaloption> base_personaloption { get; set; }
        public virtual DbSet<base_strresource> base_strresource { get; set; }
        public virtual DbSet<base_strtranslation> base_strtranslation { get; set; }
        public virtual DbSet<base_systemoption> base_systemoption { get; set; }
        public virtual DbSet<dbapp_viewmodel> dbapp_viewmodel { get; set; }
        public virtual DbSet<dbbackuplog> dbbackuplog { get; set; }
        public virtual DbSet<deptadmin> deptadmin { get; set; }
        public virtual DbSet<devcmds> devcmds { get; set; }
        public virtual DbSet<devcmds_bak> devcmds_bak { get; set; }
        public virtual DbSet<devlog> devlog { get; set; }
        public virtual DbSet<django_content_type> django_content_type { get; set; }
        public virtual DbSet<django_session> django_session { get; set; }
        public virtual DbSet<empitemdefine> empitemdefine { get; set; }
        public virtual DbSet<iclock_dstime> iclock_dstime { get; set; }
        public virtual DbSet<iclock_oplog> iclock_oplog { get; set; }
        public virtual DbSet<iclock_testdata> iclock_testdata { get; set; }
        public virtual DbSet<iclock_testdata_admin_area> iclock_testdata_admin_area { get; set; }
        public virtual DbSet<iclock_testdata_admin_dept> iclock_testdata_admin_dept { get; set; }
        public virtual DbSet<operatecmds> operatecmds { get; set; }
        public virtual DbSet<personnel_area> personnel_area { get; set; }
        public virtual DbSet<personnel_cardtype> personnel_cardtype { get; set; }
        public virtual DbSet<personnel_empchange> personnel_empchange { get; set; }
        public virtual DbSet<personnel_issuecard> personnel_issuecard { get; set; }
        public virtual DbSet<personnel_leavelog> personnel_leavelog { get; set; }
        public virtual DbSet<userinfo_attarea> userinfo_attarea { get; set; }
        public virtual DbSet<worktable_groupmsg> worktable_groupmsg { get; set; }
        public virtual DbSet<worktable_instantmsg> worktable_instantmsg { get; set; }
        public virtual DbSet<worktable_msgtype> worktable_msgtype { get; set; }
        public virtual DbSet<worktable_usrmsg> worktable_usrmsg { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                GenStringConexion strConn = Procs.Conexion(new TuplaClienteTipoDb { bTipoDb = 5, sCliente = this.sServidor });
                if (strConn == null)
                {
                    _Connected = false;
                }
                else
                {
                    _Connected = true;
                    optionsBuilder.UseSqlServer(strConn.xConexion);
                }
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ACGroup>(entity =>
            {
                entity.HasKey(e => e.GroupID)
                    .HasName("PK__ACGroup__149AF30A8B555CC6");

                entity.Property(e => e.GroupID).ValueGeneratedNever();

                entity.Property(e => e.Name).HasMaxLength(30);
            });

            modelBuilder.Entity<ACTimeZones>(entity =>
            {
                entity.HasKey(e => e.TimeZoneID)
                    .HasName("PK__ACTimeZo__78D387CF8767CADA");

                entity.Property(e => e.TimeZoneID).ValueGeneratedNever();

                entity.Property(e => e.FriEnd).HasColumnType("datetime");

                entity.Property(e => e.FriStart).HasColumnType("datetime");

                entity.Property(e => e.MonEnd).HasColumnType("datetime");

                entity.Property(e => e.MonStart).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(30);

                entity.Property(e => e.SatEnd).HasColumnType("datetime");

                entity.Property(e => e.SatStart).HasColumnType("datetime");

                entity.Property(e => e.SunEnd).HasColumnType("datetime");

                entity.Property(e => e.SunStart).HasColumnType("datetime");

                entity.Property(e => e.ThursEnd).HasColumnType("datetime");

                entity.Property(e => e.ThursStart).HasColumnType("datetime");

                entity.Property(e => e.TuesEnd).HasColumnType("datetime");

                entity.Property(e => e.TuesStart).HasColumnType("datetime");

                entity.Property(e => e.WedEnd).HasColumnType("datetime");

                entity.Property(e => e.WedStart).HasColumnType("datetime");
            });

            modelBuilder.Entity<ACUnlockComb>(entity =>
            {
                entity.HasKey(e => e.UnlockCombID)
                    .HasName("PK__ACUnlock__1F6C4570B8DD45FB");

                entity.Property(e => e.UnlockCombID).ValueGeneratedNever();

                entity.Property(e => e.Name).HasMaxLength(30);
            });

            modelBuilder.Entity<AUTHDEVICE>(entity =>
            {
                entity.HasKey(e => new { e.USERID, e.MachineID })
                    .HasName("USERCHECKTIME");

                entity.Property(e => e.ID).ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<AlarmLog>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.EnrollNumber).HasMaxLength(30);

                entity.Property(e => e.ID).ValueGeneratedOnAdd();

                entity.Property(e => e.LogTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.MachineAlias).HasMaxLength(20);

                entity.Property(e => e.Operator).HasMaxLength(20);
            });

            modelBuilder.Entity<AttParam>(entity =>
            {
                entity.HasKey(e => e.PARANAME)
                    .HasName("PK__AttParam__889D235F7484F6BC");

                entity.Property(e => e.PARANAME).HasMaxLength(20);

                entity.Property(e => e.PARATYPE).HasMaxLength(2);

                entity.Property(e => e.PARAVALUE)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<AuditedExc>(entity =>
            {
                entity.HasKey(e => e.AEID)
                    .HasName("PK__AuditedE__40737CF38A2B4297");

                entity.Property(e => e.CheckTime).HasColumnType("datetime");

                entity.Property(e => e.UName).HasMaxLength(20);

                entity.Property(e => e.UTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<BioTemplate>(entity =>
            {
                entity.HasIndex(e => new { e.BadgeNumber, e.TemplateNOIndex, e.TemplateNO, e.BioType, e.Version })
                    .HasName("IX_BioTemplate")
                    .IsUnique();

                entity.Property(e => e.BadgeNumber)
                    .IsRequired()
                    .HasMaxLength(24);

                entity.Property(e => e.CreateOperator).HasMaxLength(30);

                entity.Property(e => e.CreateTime).HasColumnType("datetime");

                entity.Property(e => e.IsDuress)
                    .IsRequired()
                    .HasMaxLength(1);

                entity.Property(e => e.TemplateData).HasMaxLength(3000);

                entity.Property(e => e.ValidFlag)
                    .IsRequired()
                    .HasMaxLength(1);

                entity.Property(e => e.Version)
                    .IsRequired()
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<CHECKEXACT>(entity =>
            {
                entity.HasKey(e => e.EXACTID)
                    .HasName("EXACTID");

                entity.Property(e => e.CHECKTIME)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.CHECKTYPE)
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.DATE).HasColumnType("datetime");

                entity.Property(e => e.INCOUNT).HasDefaultValueSql("((0))");

                entity.Property(e => e.ISADD).HasDefaultValueSql("((0))");

                entity.Property(e => e.ISCOUNT).HasDefaultValueSql("((0))");

                entity.Property(e => e.ISDELETE).HasDefaultValueSql("((0))");

                entity.Property(e => e.ISMODIFY).HasDefaultValueSql("((0))");

                entity.Property(e => e.MODIFYBY)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.USERID).HasDefaultValueSql("((0))");

                entity.Property(e => e.YUYIN)
                    .HasMaxLength(25)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<CHECKINOUT>(entity =>
            {
                entity.HasKey(e => new { e.USERID, e.CHECKTIME })
                    .HasName("USERCHECKTIME1");

                entity.Property(e => e.CHECKTIME)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CHECKTYPE)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('I')");

                entity.Property(e => e.LOGID).ValueGeneratedOnAdd();

                entity.Property(e => e.Memoinfo)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.SENSORID)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.VERIFYCODE).HasDefaultValueSql("((0))");

                entity.Property(e => e.sn)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<CustomReport>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.Memo).HasMaxLength(50);

                entity.Property(e => e.ReportName).HasMaxLength(50);

                entity.Property(e => e.id).ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<CustomReport1>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("CustomReport", "jorgeLoa_usrals");

                entity.Property(e => e.Memo).HasMaxLength(50);

                entity.Property(e => e.ReportName).HasMaxLength(50);

                entity.Property(e => e.id).ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<DEPARTMENTS>(entity =>
            {
                entity.HasKey(e => e.DEPTID)
                    .HasName("DEPTID");

                entity.Property(e => e.ATT).HasDefaultValueSql("((1))");

                entity.Property(e => e.AutoSchPlan).HasDefaultValueSql("((1))");

                entity.Property(e => e.DEPTNAME).HasMaxLength(30);

                entity.Property(e => e.DefaultSchId).HasDefaultValueSql("((1))");

                entity.Property(e => e.Holiday).HasDefaultValueSql("((1))");

                entity.Property(e => e.InLate).HasDefaultValueSql("((1))");

                entity.Property(e => e.InheritDeptRule).HasDefaultValueSql("((1))");

                entity.Property(e => e.InheritDeptSch).HasDefaultValueSql("((1))");

                entity.Property(e => e.InheritDeptSchClass).HasDefaultValueSql("((1))");

                entity.Property(e => e.InheritParentSch).HasDefaultValueSql("((1))");

                entity.Property(e => e.MinAutoSchInterval).HasDefaultValueSql("((24))");

                entity.Property(e => e.OutEarly).HasDefaultValueSql("((1))");

                entity.Property(e => e.OverTime).HasDefaultValueSql("((1))");

                entity.Property(e => e.RegisterOT).HasDefaultValueSql("((1))");

                entity.Property(e => e.SUPDEPTID).HasDefaultValueSql("((1))");

                entity.Property(e => e.change_operator).HasMaxLength(50);

                entity.Property(e => e.change_time).HasColumnType("datetime");

                entity.Property(e => e.code).HasMaxLength(50);

                entity.Property(e => e.create_operator).HasMaxLength(50);

                entity.Property(e => e.create_time).HasColumnType("datetime");

                entity.Property(e => e.delete_operator).HasMaxLength(50);

                entity.Property(e => e.delete_time).HasColumnType("datetime");

                entity.Property(e => e.invalidate).HasColumnType("datetime");

                entity.Property(e => e.type).HasMaxLength(50);
            });

            modelBuilder.Entity<DeptUsedSchs>(entity =>
            {
                entity.HasKey(e => new { e.DeptId, e.SchId })
                    .HasName("DeptUsedSchs1");
            });

            modelBuilder.Entity<EXCNOTES>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.ATTDATE).HasColumnType("datetime");

                entity.Property(e => e.NOTES).HasMaxLength(200);
            });

            modelBuilder.Entity<EmOpLog>(entity =>
            {
                entity.Property(e => e.OperateTime).HasColumnType("datetime");

                entity.Property(e => e.SensorId).HasMaxLength(5);
            });

            modelBuilder.Entity<FaceTemp>(entity =>
            {
                entity.HasKey(e => e.TEMPLATEID)
                    .HasName("PK__FaceTemp__9EE4AD53C352EE68");

                entity.Property(e => e.ACTIVETIME).HasDefaultValueSql("((0))");

                entity.Property(e => e.FACEID).HasDefaultValueSql("((0))");

                entity.Property(e => e.RESERVE).HasDefaultValueSql("((0))");

                entity.Property(e => e.SIZE).HasDefaultValueSql("((0))");

                entity.Property(e => e.StateMigrationFlag).HasMaxLength(6);

                entity.Property(e => e.TEMPLATE).HasColumnType("image");

                entity.Property(e => e.USERNO).HasMaxLength(24);

                entity.Property(e => e.VALID).HasDefaultValueSql("((0))");

                entity.Property(e => e.VFCOUNT).HasDefaultValueSql("((0))");

                entity.Property(e => e.pin).HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<FaceTempEx>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.StateMigrationFlag).HasMaxLength(6);

                entity.Property(e => e.TEMPLATE).HasColumnType("image");

                entity.Property(e => e.TEMPLATEID).ValueGeneratedOnAdd();

                entity.Property(e => e.USERNO).HasMaxLength(24);
            });

            modelBuilder.Entity<FingerVein>(entity =>
            {
                entity.HasKey(e => e.FVID)
                    .HasName("PK__FingerVe__39769C6208FEE793");

                entity.Property(e => e.Size).HasDefaultValueSql("((0))");

                entity.Property(e => e.StateMigrationFlag).HasMaxLength(6);

                entity.Property(e => e.Template).HasColumnType("image");

                entity.Property(e => e.UserCode).HasMaxLength(50);

                entity.Property(e => e.UserID).HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<FingerVein1>(entity =>
            {
                entity.HasKey(e => e.FVID)
                    .HasName("PK__FingerVe__39769C621EFE3FF8");

                entity.ToTable("FingerVein", "jorgeLoa_usrals");

                entity.Property(e => e.Size).HasDefaultValueSql("((0))");

                entity.Property(e => e.Template).HasColumnType("image");

                entity.Property(e => e.UserCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UserID).HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<FingerVeinEx>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.FVID).ValueGeneratedOnAdd();

                entity.Property(e => e.StateMigrationFlag).HasMaxLength(6);

                entity.Property(e => e.Template).HasColumnType("image");

                entity.Property(e => e.UserCode).HasMaxLength(50);
            });

            modelBuilder.Entity<HOLIDAYS>(entity =>
            {
                entity.HasKey(e => e.HOLIDAYID)
                    .HasName("PK__HOLIDAYS__5D6453952BC40BE8");

                entity.Property(e => e.HOLIDAYID).ValueGeneratedNever();

                entity.Property(e => e.HOLIDAYNAME).HasMaxLength(20);

                entity.Property(e => e.HOLIDAYTYPE).HasDefaultValueSql("((1))");

                entity.Property(e => e.MINZU).HasMaxLength(50);

                entity.Property(e => e.STARTTIME).HasColumnType("datetime");

                entity.Property(e => e.XINBIE).HasMaxLength(4);
            });

            modelBuilder.Entity<LeaveClass>(entity =>
            {
                entity.HasKey(e => e.LeaveId)
                    .HasName("PK__LeaveCla__796DB95901E6A0DC");

                entity.Property(e => e.LeaveName)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.MinUnit).HasDefaultValueSql("((1))");

                entity.Property(e => e.RemaindCount).HasDefaultValueSql("((1))");

                entity.Property(e => e.RemaindProc).HasDefaultValueSql("((1))");

                entity.Property(e => e.ReportSymbol)
                    .IsRequired()
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('-')");

                entity.Property(e => e.Unit).HasDefaultValueSql("((1))");
            });

            modelBuilder.Entity<LeaveClass1>(entity =>
            {
                entity.HasKey(e => e.LeaveId)
                    .HasName("PK__LeaveCla__796DB959928CDAC4");

                entity.Property(e => e.Code).HasMaxLength(16);

                entity.Property(e => e.LeaveName)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.MinUnit).HasDefaultValueSql("((1))");

                entity.Property(e => e.RemaindCount).HasDefaultValueSql("((1))");

                entity.Property(e => e.RemaindProc).HasDefaultValueSql("((2))");

                entity.Property(e => e.ReportSymbol)
                    .IsRequired()
                    .HasMaxLength(4)
                    .HasDefaultValueSql("('_')");
            });

            modelBuilder.Entity<LossCard>(entity =>
            {
                entity.ToTable("LossCard", "jorgeLoa_usrals");

                entity.Property(e => e.CardNo).HasMaxLength(50);

                entity.Property(e => e.LossDate).HasColumnType("datetime");

                entity.Property(e => e.Pin).HasMaxLength(20);
            });

            modelBuilder.Entity<Machines>(entity =>
            {
                entity.Property(e => e.AccFun).HasDefaultValueSql("('0')");

                entity.Property(e => e.BiometricMaxCount).HasMaxLength(100);

                entity.Property(e => e.BiometricType).HasMaxLength(100);

                entity.Property(e => e.BiometricUsedCount).HasMaxLength(100);

                entity.Property(e => e.BiometricVersion).HasMaxLength(100);

                entity.Property(e => e.CommPassword).HasMaxLength(12);

                entity.Property(e => e.CompatOldFirmware).HasMaxLength(5);

                entity.Property(e => e.DateFormat).HasDefaultValueSql("((-1))");

                entity.Property(e => e.Enabled)
                    .IsRequired()
                    .HasDefaultValueSql("('True')");

                entity.Property(e => e.FingerFunOn).HasMaxLength(5);

                entity.Property(e => e.FirmwareVersion).HasMaxLength(50);

                entity.Property(e => e.FvFunOn).HasDefaultValueSql("((0))");

                entity.Property(e => e.IP).HasMaxLength(20);

                entity.Property(e => e.Idle).HasDefaultValueSql("((-1))");

                entity.Property(e => e.InOutRecordWarn).HasDefaultValueSql("((-1))");

                entity.Property(e => e.IsWireless).HasDefaultValueSql("('False')");

                entity.Property(e => e.LockControl).HasDefaultValueSql("((-1))");

                entity.Property(e => e.MachineAlias).HasMaxLength(50);

                entity.Property(e => e.MachineNumber).HasDefaultValueSql("((1))");

                entity.Property(e => e.ParamValues).HasColumnType("image");

                entity.Property(e => e.PhotoStamp).HasMaxLength(20);

                entity.Property(e => e.Port).HasDefaultValueSql("('4370')");

                entity.Property(e => e.ProductType).HasMaxLength(20);

                entity.Property(e => e.SecretCount).HasDefaultValueSql("((-1))");

                entity.Property(e => e.SerialPort).HasDefaultValueSql("((1))");

                entity.Property(e => e.SimpleEventType).HasDefaultValueSql("((0))");

                entity.Property(e => e.TZAdj).HasDefaultValueSql("('8')");

                entity.Property(e => e.UILanguage).HasDefaultValueSql("((-1))");

                entity.Property(e => e.UTableDesc).HasColumnType("image");

                entity.Property(e => e.UpdateDB)
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('1111101000')");

                entity.Property(e => e.Voice).HasDefaultValueSql("((-1))");

                entity.Property(e => e.WirelessAddr).HasMaxLength(50);

                entity.Property(e => e.WirelessGateWay).HasMaxLength(50);

                entity.Property(e => e.WirelessKey).HasMaxLength(50);

                entity.Property(e => e.WirelessMask).HasMaxLength(50);

                entity.Property(e => e.WirelessSSID).HasMaxLength(50);

                entity.Property(e => e.acpanel_type).HasDefaultValueSql("('2')");

                entity.Property(e => e.agent_ipaddress).HasMaxLength(50);

                entity.Property(e => e.alg_ver).HasMaxLength(50);

                entity.Property(e => e.alias).HasMaxLength(50);

                entity.Property(e => e.brightness).HasMaxLength(50);

                entity.Property(e => e.change_operator).HasMaxLength(50);

                entity.Property(e => e.change_time).HasColumnType("datetime");

                entity.Property(e => e.city).HasMaxLength(50);

                entity.Property(e => e.com_address).HasDefaultValueSql("('1')");

                entity.Property(e => e.create_operator).HasMaxLength(50);

                entity.Property(e => e.create_time).HasColumnType("datetime");

                entity.Property(e => e.delay).HasDefaultValueSql("('10')");

                entity.Property(e => e.delete_operator).HasMaxLength(50);

                entity.Property(e => e.delete_time).HasColumnType("datetime");

                entity.Property(e => e.deviceOption).HasColumnType("image");

                entity.Property(e => e.device_name).HasMaxLength(50);

                entity.Property(e => e.encrypt)
                    .IsRequired()
                    .HasDefaultValueSql("('False')");

                entity.Property(e => e.fingercount).HasDefaultValueSql("((-1))");

                entity.Property(e => e.flash_size).HasMaxLength(50);

                entity.Property(e => e.four_to_two)
                    .IsRequired()
                    .HasDefaultValueSql("('False')");

                entity.Property(e => e.free_flash_size).HasMaxLength(50);

                entity.Property(e => e.fvcount).HasDefaultValueSql("((0))");

                entity.Property(e => e.gateway).HasMaxLength(50);

                entity.Property(e => e.ipaddress).HasMaxLength(50);

                entity.Property(e => e.is_tft).HasMaxLength(50);

                entity.Property(e => e.language).HasMaxLength(50);

                entity.Property(e => e.last_activity).HasColumnType("datetime");

                entity.Property(e => e.lng_encode)
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('gb2312')");

                entity.Property(e => e.log_stamp).HasMaxLength(50);

                entity.Property(e => e.main_time).HasMaxLength(50);

                entity.Property(e => e.managercount).HasDefaultValueSql("((-1))");

                entity.Property(e => e.max_comm_count).HasDefaultValueSql("('20')");

                entity.Property(e => e.max_comm_size).HasDefaultValueSql("('40')");

                entity.Property(e => e.oem_vendor).HasMaxLength(50);

                entity.Property(e => e.oplog_stamp).HasColumnType("image");

                entity.Property(e => e.photo_stamp).HasColumnType("image");

                entity.Property(e => e.platform).HasMaxLength(50);

                entity.Property(e => e.realtime)
                    .IsRequired()
                    .HasDefaultValueSql("('True')");

                entity.Property(e => e.sn).HasMaxLength(20);

                entity.Property(e => e.status).HasDefaultValueSql("((0))");

                entity.Property(e => e.subnet_mask).HasMaxLength(50);

                entity.Property(e => e.sync_time)
                    .IsRequired()
                    .HasDefaultValueSql("('True')");

                entity.Property(e => e.trans_times).HasMaxLength(50);

                entity.Property(e => e.usercount).HasDefaultValueSql("((-1))");

                entity.Property(e => e.video_login).HasMaxLength(50);

                entity.Property(e => e.volume).HasMaxLength(50);
            });

            modelBuilder.Entity<NUM_RUN>(entity =>
            {
                entity.Property(e => e.CYLE).HasDefaultValueSql("((1))");

                entity.Property(e => e.ENDDATE)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('2099-12-31')");

                entity.Property(e => e.NAME)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.OLDID).HasDefaultValueSql("((-1))");

                entity.Property(e => e.STARTDATE)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('2000-1-1')");

                entity.Property(e => e.UNITS).HasDefaultValueSql("((1))");
            });

            modelBuilder.Entity<NUM_RUN_DEIL>(entity =>
            {
                entity.HasKey(e => new { e.NUM_RUNID, e.SDAYS, e.STARTTIME })
                    .HasName("NUMID2");

                entity.Property(e => e.STARTTIME).HasColumnType("datetime");

                entity.Property(e => e.ENDTIME).HasColumnType("datetime");

                entity.Property(e => e.SCHCLASSID).HasDefaultValueSql("((-1))");
            });

            modelBuilder.Entity<OfflinePermitDoors>(entity =>
            {
                entity.ToTable("OfflinePermitDoors", "jorgeLoa_usrals");
            });

            modelBuilder.Entity<OfflinePermitGroups>(entity =>
            {
                entity.ToTable("OfflinePermitGroups", "jorgeLoa_usrals");

                entity.Property(e => e.BeginDate).HasColumnType("datetime");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.GroupName).HasMaxLength(24);
            });

            modelBuilder.Entity<OfflinePermitUsers>(entity =>
            {
                entity.ToTable("OfflinePermitUsers", "jorgeLoa_usrals");

                entity.Property(e => e.Pin).HasMaxLength(24);
            });

            modelBuilder.Entity<ParamSet>(entity =>
            {
                entity.HasKey(e => e.ParamName)
                    .HasName("PK__ParamSet__8E33E55162C287B3");

                entity.ToTable("ParamSet", "jorgeLoa_usrals");

                entity.Property(e => e.ParamName).HasMaxLength(20);

                entity.Property(e => e.ParamValue).HasMaxLength(100);
            });

            modelBuilder.Entity<ReaderProperty>(entity =>
            {
                entity.Property(e => e.create_operator).HasMaxLength(100);

                entity.Property(e => e.ipaddress).HasMaxLength(100);

                entity.Property(e => e.mac).HasMaxLength(100);

                entity.Property(e => e.multicast).HasMaxLength(100);
            });

            modelBuilder.Entity<ReportField>(entity =>
            {
                entity.HasKey(e => new { e.CRId, e.TableName, e.FieldName });

                entity.Property(e => e.TableName).HasMaxLength(50);

                entity.Property(e => e.FieldName).HasMaxLength(50);
            });

            modelBuilder.Entity<ReportField1>(entity =>
            {
                entity.HasKey(e => new { e.CRId, e.TableName, e.FieldName });

                entity.ToTable("ReportField", "jorgeLoa_usrals");

                entity.Property(e => e.TableName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FieldName)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ReportItem>(entity =>
            {
                entity.HasKey(e => e.RIID)
                    .HasName("PK__ReportIt__464E992EC9DC8F3C");

                entity.Property(e => e.Formula)
                    .IsRequired()
                    .HasColumnType("image");

                entity.Property(e => e.Notes).HasColumnType("image");

                entity.Property(e => e.RIName)
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.UnitName)
                    .HasMaxLength(6)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SECURITYDETAILS>(entity =>
            {
                entity.HasKey(e => e.SECURITYDETAILID)
                    .HasName("NAMEID2");

                entity.Property(e => e.REPORT)
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SHIFT>(entity =>
            {
                entity.Property(e => e.CYCLE).HasDefaultValueSql("((0))");

                entity.Property(e => e.ENDDATE)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('1900-12-31')");

                entity.Property(e => e.NAME)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.RUNNUM).HasDefaultValueSql("((0))");

                entity.Property(e => e.SCH1).HasDefaultValueSql("((0))");

                entity.Property(e => e.SCH10).HasDefaultValueSql("((0))");

                entity.Property(e => e.SCH11).HasDefaultValueSql("((0))");

                entity.Property(e => e.SCH12).HasDefaultValueSql("((0))");

                entity.Property(e => e.SCH2).HasDefaultValueSql("((0))");

                entity.Property(e => e.SCH3).HasDefaultValueSql("((0))");

                entity.Property(e => e.SCH4).HasDefaultValueSql("((0))");

                entity.Property(e => e.SCH5).HasDefaultValueSql("((0))");

                entity.Property(e => e.SCH6).HasDefaultValueSql("((0))");

                entity.Property(e => e.SCH7).HasDefaultValueSql("((0))");

                entity.Property(e => e.SCH8).HasDefaultValueSql("((0))");

                entity.Property(e => e.SCH9).HasDefaultValueSql("((0))");

                entity.Property(e => e.STARTDATE)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('1900-1-1')");

                entity.Property(e => e.UNITS).HasDefaultValueSql("((0))");

                entity.Property(e => e.USHIFTID).HasDefaultValueSql("((-1))");
            });

            modelBuilder.Entity<STD_WiegandFmt>(entity =>
            {
                entity.Property(e => e.InWiegandFmt).HasMaxLength(100);

                entity.Property(e => e.OutWiegandFmt).HasMaxLength(100);
            });

            modelBuilder.Entity<STD_WiegandFmt1>(entity =>
            {
                entity.ToTable("STD_WiegandFmt", "jorgeLoa_usrals");

                entity.Property(e => e.InWiegandFmt).HasMaxLength(100);

                entity.Property(e => e.OutWiegandFmt).HasMaxLength(100);
            });

            modelBuilder.Entity<SchClass>(entity =>
            {
                entity.Property(e => e.AutoBind).HasDefaultValueSql("((1))");

                entity.Property(e => e.CheckIn).HasDefaultValueSql("((1))");

                entity.Property(e => e.CheckInTime1).HasColumnType("datetime");

                entity.Property(e => e.CheckInTime2).HasColumnType("datetime");

                entity.Property(e => e.CheckOut).HasDefaultValueSql("((1))");

                entity.Property(e => e.CheckOutTime1).HasColumnType("datetime");

                entity.Property(e => e.CheckOutTime2).HasColumnType("datetime");

                entity.Property(e => e.Color).HasDefaultValueSql("((16715535))");

                entity.Property(e => e.EndTime).HasColumnType("datetime");

                entity.Property(e => e.SensorID)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StartTime).HasColumnType("datetime");

                entity.Property(e => e.WorkDay).HasDefaultValueSql("((0))");

                entity.Property(e => e.WorkMins).HasDefaultValueSql("((0))");

                entity.Property(e => e.schName)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ServerLog>(entity =>
            {
                entity.Property(e => e.EVENT)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.EVENTTIME)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EnrollNumber)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.OPERATOR)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.SENSORID)
                    .HasMaxLength(5)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SystemLog>(entity =>
            {
                entity.Property(e => e.LogDescr)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LogTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.MachineAlias)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Operator)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TBKEY>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.PreName).HasMaxLength(12);
            });

            modelBuilder.Entity<TBSMSALLOT>(entity =>
            {
                entity.HasKey(e => new { e.SMSREF, e.USERREF })
                    .HasName("pk_tbsmsallog");

                entity.Property(e => e.GENTM)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TBSMSINFO>(entity =>
            {
                entity.HasKey(e => e.REFERENCE)
                    .HasName("pk_TBSMSINFO");

                entity.Property(e => e.REFERENCE).ValueGeneratedNever();

                entity.Property(e => e.GENTM)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.SMSCONTENT)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.SMSID)
                    .IsRequired()
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.SMSSTARTTM)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TEMPLATE>(entity =>
            {
                entity.Property(e => e.BITMAPPICTURE).HasColumnType("image");

                entity.Property(e => e.BITMAPPICTURE2).HasColumnType("image");

                entity.Property(e => e.BITMAPPICTURE3).HasColumnType("image");

                entity.Property(e => e.BITMAPPICTURE4).HasColumnType("image");

                entity.Property(e => e.EMACHINENUM).HasMaxLength(3);

                entity.Property(e => e.FINGERID).HasDefaultValueSql("('0')");

                entity.Property(e => e.Fpversion).HasMaxLength(50);

                entity.Property(e => e.StateMigrationFlag).HasMaxLength(6);

                entity.Property(e => e.TEMPLATE1)
                    .HasColumnName("TEMPLATE")
                    .HasColumnType("image");

                entity.Property(e => e.TEMPLATE11)
                    .HasColumnName("TEMPLATE1")
                    .HasColumnType("image");

                entity.Property(e => e.TEMPLATE2).HasColumnType("image");

                entity.Property(e => e.TEMPLATE3).HasColumnType("image");

                entity.Property(e => e.TEMPLATE4).HasColumnType("image");

                entity.Property(e => e.UTime).HasColumnType("datetime");

                entity.Property(e => e.Valid).HasDefaultValueSql("('1')");

                entity.Property(e => e.bio_type).HasDefaultValueSql("('0')");

                entity.Property(e => e.change_operator).HasMaxLength(50);

                entity.Property(e => e.change_time).HasColumnType("datetime");

                entity.Property(e => e.create_operator).HasMaxLength(50);

                entity.Property(e => e.create_time).HasColumnType("datetime");

                entity.Property(e => e.delete_operator).HasMaxLength(50);

                entity.Property(e => e.delete_time).HasColumnType("datetime");
            });

            modelBuilder.Entity<TEMPLATEEx>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.BITMAPPICTURE).HasColumnType("image");

                entity.Property(e => e.BITMAPPICTURE2).HasColumnType("image");

                entity.Property(e => e.BITMAPPICTURE3).HasColumnType("image");

                entity.Property(e => e.BITMAPPICTURE4).HasColumnType("image");

                entity.Property(e => e.EMACHINENUM).HasMaxLength(3);

                entity.Property(e => e.Fpversion).HasMaxLength(50);

                entity.Property(e => e.StateMigrationFlag).HasMaxLength(6);

                entity.Property(e => e.TEMPLATE).HasColumnType("image");

                entity.Property(e => e.TEMPLATE1).HasColumnType("image");

                entity.Property(e => e.TEMPLATE2).HasColumnType("image");

                entity.Property(e => e.TEMPLATE3).HasColumnType("image");

                entity.Property(e => e.TEMPLATE4).HasColumnType("image");

                entity.Property(e => e.TEMPLATEID).ValueGeneratedOnAdd();

                entity.Property(e => e.UTime).HasColumnType("datetime");

                entity.Property(e => e.change_operator).HasMaxLength(50);

                entity.Property(e => e.change_time).HasColumnType("datetime");

                entity.Property(e => e.create_operator).HasMaxLength(50);

                entity.Property(e => e.create_time).HasColumnType("datetime");

                entity.Property(e => e.delete_operator).HasMaxLength(50);

                entity.Property(e => e.delete_time).HasColumnType("datetime");
            });

            modelBuilder.Entity<TmpPermitDoors>(entity =>
            {
                entity.ToTable("TmpPermitDoors", "jorgeLoa_usrals");
            });

            modelBuilder.Entity<TmpPermitGroups>(entity =>
            {
                entity.ToTable("TmpPermitGroups", "jorgeLoa_usrals");

                entity.Property(e => e.BeginDate).HasColumnType("datetime");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.GroupName).HasMaxLength(24);
            });

            modelBuilder.Entity<TmpPermitUsers>(entity =>
            {
                entity.ToTable("TmpPermitUsers", "jorgeLoa_usrals");

                entity.Property(e => e.CardNo).HasMaxLength(20);

                entity.Property(e => e.CertifiNo).HasMaxLength(50);

                entity.Property(e => e.Gender).HasMaxLength(2);

                entity.Property(e => e.OfflineBeginDate).HasColumnType("datetime");

                entity.Property(e => e.OfflineEndDate).HasColumnType("datetime");

                entity.Property(e => e.UserName).HasMaxLength(24);
            });

            modelBuilder.Entity<USERINFO>(entity =>
            {
                entity.HasKey(e => e.USERID)
                    .HasName("PK__USERINFO__7B9E7F352CBF0E8D");

                entity.Property(e => e.ATT).HasDefaultValueSql("((1))");

                entity.Property(e => e.AutoSchPlan).HasDefaultValueSql("((1))");

                entity.Property(e => e.BIRTHDAY).HasColumnType("datetime");

                entity.Property(e => e.Badgenumber).HasMaxLength(50);

                entity.Property(e => e.CITY).HasMaxLength(50);

                entity.Property(e => e.CardNo).HasMaxLength(20);

                entity.Property(e => e.Cuser1).HasMaxLength(50);

                entity.Property(e => e.Cuser2).HasMaxLength(50);

                entity.Property(e => e.Cuser3).HasMaxLength(50);

                entity.Property(e => e.Cuser4).HasMaxLength(50);

                entity.Property(e => e.Cuser5).HasMaxLength(50);

                entity.Property(e => e.DEFAULTDEPTID).HasDefaultValueSql("((1))");

                entity.Property(e => e.Duser1).HasColumnType("datetime");

                entity.Property(e => e.Duser2).HasColumnType("datetime");

                entity.Property(e => e.Duser3).HasColumnType("datetime");

                entity.Property(e => e.Duser4).HasColumnType("datetime");

                entity.Property(e => e.Duser5).HasColumnType("datetime");

                entity.Property(e => e.Education).HasMaxLength(50);

                entity.Property(e => e.FPHONE).HasMaxLength(20);

                entity.Property(e => e.Gender).HasMaxLength(8);

                entity.Property(e => e.HIREDDAY).HasColumnType("datetime");

                entity.Property(e => e.HOLIDAY).HasDefaultValueSql("((1))");

                entity.Property(e => e.INLATE).HasDefaultValueSql("((1))");

                entity.Property(e => e.MINZU).HasMaxLength(8);

                entity.Property(e => e.MinAutoSchInterval).HasDefaultValueSql("((24))");

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.Notes).HasColumnType("image");

                entity.Property(e => e.OPHONE).HasMaxLength(20);

                entity.Property(e => e.OUTEARLY).HasDefaultValueSql("((1))");

                entity.Property(e => e.OVERTIME).HasDefaultValueSql("((1))");

                entity.Property(e => e.OfflineBeginDate).HasColumnType("datetime");

                entity.Property(e => e.OfflineEndDate).HasColumnType("datetime");

                entity.Property(e => e.PAGER).HasMaxLength(20);

                entity.Property(e => e.PASSWORD).HasMaxLength(20);

                entity.Property(e => e.PHOTO).HasColumnType("image");

                entity.Property(e => e.Political).HasMaxLength(50);

                entity.Property(e => e.RegisterOT).HasDefaultValueSql("((1))");

                entity.Property(e => e.SEP).HasDefaultValueSql("((1))");

                entity.Property(e => e.SSN).HasMaxLength(20);

                entity.Property(e => e.STATE).HasMaxLength(50);

                entity.Property(e => e.TITLE).HasMaxLength(20);

                entity.Property(e => e.TimeZones).HasMaxLength(50);

                entity.Property(e => e.UTime).HasColumnType("datetime");

                entity.Property(e => e.ZIP).HasMaxLength(50);

                entity.Property(e => e.acc_enddate).HasColumnType("datetime");

                entity.Property(e => e.acc_startdate).HasColumnType("datetime");

                entity.Property(e => e.bankcode1).HasMaxLength(50);

                entity.Property(e => e.bankcode2).HasMaxLength(50);

                entity.Property(e => e.birthplace).HasMaxLength(50);

                entity.Property(e => e.carBrand).HasMaxLength(50);

                entity.Property(e => e.carColor).HasMaxLength(50);

                entity.Property(e => e.carNo).HasMaxLength(50);

                entity.Property(e => e.carType).HasMaxLength(50);

                entity.Property(e => e.change_operator).HasMaxLength(50);

                entity.Property(e => e.change_time).HasColumnType("datetime");

                entity.Property(e => e.contry).HasMaxLength(50);

                entity.Property(e => e.create_operator).HasMaxLength(50);

                entity.Property(e => e.create_time).HasColumnType("datetime");

                entity.Property(e => e.delete_operator).HasMaxLength(50);

                entity.Property(e => e.delete_time).HasColumnType("datetime");

                entity.Property(e => e.email).HasMaxLength(50);

                entity.Property(e => e.firedate).HasColumnType("datetime");

                entity.Property(e => e.homeaddress).HasMaxLength(50);

                entity.Property(e => e.identitycard).HasMaxLength(50);

                entity.Property(e => e.lastname).HasMaxLength(50);

                entity.Property(e => e.mverifypass).HasMaxLength(10);

                entity.Property(e => e.privilege).HasDefaultValueSql("((0))");

                entity.Property(e => e.status).HasDefaultValueSql("('0')");

                entity.Property(e => e.street).HasMaxLength(80);
            });

            modelBuilder.Entity<USER_OF_RUN>(entity =>
            {
                entity.HasKey(e => new { e.USERID, e.NUM_OF_RUN_ID, e.STARTDATE, e.ENDDATE })
                    .HasName("USER_ST_NUM");

                entity.Property(e => e.STARTDATE)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('1900-1-1')");

                entity.Property(e => e.ENDDATE)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('2099-12-31')");

                entity.Property(e => e.ISNOTOF_RUN).HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<USER_SPEDAY>(entity =>
            {
                entity.HasKey(e => new { e.USERID, e.STARTSPECDAY, e.DATEID })
                    .HasName("USER_SEP");

                entity.Property(e => e.STARTSPECDAY)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('1900-1-1')");

                entity.Property(e => e.DATEID).HasDefaultValueSql("((-1))");

                entity.Property(e => e.DATE).HasColumnType("datetime");

                entity.Property(e => e.ENDSPECDAY)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('2099-12-31')");

                entity.Property(e => e.YUANYING)
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<USER_TEMP_SCH>(entity =>
            {
                entity.HasKey(e => new { e.USERID, e.COMETIME, e.LEAVETIME })
                    .HasName("USER_TEMP");

                entity.Property(e => e.COMETIME).HasColumnType("datetime");

                entity.Property(e => e.LEAVETIME).HasColumnType("datetime");

                entity.Property(e => e.FLAG).HasDefaultValueSql("((1))");

                entity.Property(e => e.SCHCLASSID).HasDefaultValueSql("((-1))");

                entity.Property(e => e.TYPE).HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<UserACMachines>(entity =>
            {
                entity.HasKey(e => new { e.UserID, e.Deviceid })
                    .HasName("UserACMachinesPK");
            });

            modelBuilder.Entity<UserACPrivilege>(entity =>
            {
                entity.HasKey(e => e.UserID)
                    .HasName("PK__UserACPr__1788CCAC0CEE6FD0");

                entity.Property(e => e.UserID).ValueGeneratedNever();
            });

            modelBuilder.Entity<UserUpdates>(entity =>
            {
                entity.HasKey(e => e.UpdateId)
                    .HasName("PK__UserUpda__7A0CF3C5C0E88078");

                entity.Property(e => e.BadgeNumber)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<UserUsedSClasses>(entity =>
            {
                entity.HasIndex(e => new { e.UserId, e.SchId })
                    .HasName("UQ__UserUsed__BB25543DBF866FD1")
                    .IsUnique();
            });

            modelBuilder.Entity<UsersMachines>(entity =>
            {
                entity.HasKey(e => new { e.USERID, e.DEVICEID })
                    .HasName("UsersMachinesPK");

                entity.Property(e => e.ID).ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<ZKAttendanceMonthStatistics>(entity =>
            {
                entity.Property(e => e.ChiDao).HasColumnType("decimal(9, 1)");

                entity.Property(e => e.ChuQinShiJian).HasColumnType("decimal(9, 1)");

                entity.Property(e => e.GongZuoShiJian).HasColumnType("decimal(9, 1)");

                entity.Property(e => e.JiaBan).HasColumnType("decimal(9, 1)");

                entity.Property(e => e.JieJiaRi).HasColumnType("decimal(9, 1)");

                entity.Property(e => e.JieJiaRiJiaBan).HasColumnType("decimal(9, 1)");

                entity.Property(e => e.KuangGong).HasColumnType("decimal(9, 1)");

                entity.Property(e => e.PingRi).HasColumnType("decimal(9, 1)");

                entity.Property(e => e.PingRiJiaBan).HasColumnType("decimal(9, 1)");

                entity.Property(e => e.QianDao).HasColumnType("decimal(9, 1)");

                entity.Property(e => e.QianTui).HasColumnType("decimal(9, 1)");

                entity.Property(e => e.QingJia).HasColumnType("decimal(9, 1)");

                entity.Property(e => e.ShiDao).HasColumnType("decimal(9, 1)");

                entity.Property(e => e.WaiChu).HasColumnType("decimal(9, 1)");

                entity.Property(e => e.WeiQianDao).HasColumnType("decimal(9, 1)");

                entity.Property(e => e.WeiQianTui).HasColumnType("decimal(9, 1)");

                entity.Property(e => e.YinGongWaiChu).HasColumnType("decimal(9, 1)");

                entity.Property(e => e.YingDao).HasColumnType("decimal(9, 1)");

                entity.Property(e => e.YingQian).HasColumnType("decimal(9, 1)");

                entity.Property(e => e.ZaoTui).HasColumnType("decimal(9, 1)");

                entity.Property(e => e.ZhouMo).HasColumnType("decimal(9, 1)");

                entity.Property(e => e.ZhouMoJiaBan).HasColumnType("decimal(9, 1)");
            });

            modelBuilder.Entity<acc_antiback>(entity =>
            {
                entity.Property(e => e.change_operator).HasMaxLength(50);

                entity.Property(e => e.change_time).HasColumnType("datetime");

                entity.Property(e => e.create_operator).HasMaxLength(50);

                entity.Property(e => e.create_time).HasColumnType("datetime");

                entity.Property(e => e.delete_operator).HasMaxLength(50);

                entity.Property(e => e.delete_time).HasColumnType("datetime");

                entity.Property(e => e.eight_mode)
                    .IsRequired()
                    .HasDefaultValueSql("('False')");

                entity.Property(e => e.five_mode)
                    .IsRequired()
                    .HasDefaultValueSql("('False')");

                entity.Property(e => e.four_mode)
                    .IsRequired()
                    .HasDefaultValueSql("('False')");

                entity.Property(e => e.nine_mode)
                    .IsRequired()
                    .HasDefaultValueSql("('False')");

                entity.Property(e => e.one_mode)
                    .IsRequired()
                    .HasDefaultValueSql("('False')");

                entity.Property(e => e.seven_mode)
                    .IsRequired()
                    .HasDefaultValueSql("('False')");

                entity.Property(e => e.six_mode)
                    .IsRequired()
                    .HasDefaultValueSql("('False')");

                entity.Property(e => e.status).HasDefaultValueSql("('0')");

                entity.Property(e => e.three_mode)
                    .IsRequired()
                    .HasDefaultValueSql("('False')");

                entity.Property(e => e.two_mode)
                    .IsRequired()
                    .HasDefaultValueSql("('False')");
            });

            modelBuilder.Entity<acc_auxiliary>(entity =>
            {
                entity.Property(e => e.aux_name).HasMaxLength(50);

                entity.Property(e => e.printer_number).HasMaxLength(50);
            });

            modelBuilder.Entity<acc_auxiliary1>(entity =>
            {
                entity.ToTable("acc_auxiliary", "jorgeLoa_usrals");

                entity.Property(e => e.aux_name).HasMaxLength(50);

                entity.Property(e => e.printer_number).HasMaxLength(50);
            });

            modelBuilder.Entity<acc_door>(entity =>
            {
                entity.Property(e => e.SRBOn).HasDefaultValueSql("((0))");

                entity.Property(e => e.back_lock)
                    .IsRequired()
                    .HasDefaultValueSql("('True')");

                entity.Property(e => e.card_intervaltime).HasDefaultValueSql("('2')");

                entity.Property(e => e.change_operator).HasMaxLength(50);

                entity.Property(e => e.change_time).HasColumnType("datetime");

                entity.Property(e => e.create_operator).HasMaxLength(50);

                entity.Property(e => e.create_time).HasColumnType("datetime");

                entity.Property(e => e.delete_operator).HasMaxLength(50);

                entity.Property(e => e.delete_time).HasColumnType("datetime");

                entity.Property(e => e.door_name)
                    .HasMaxLength(30)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.door_sensor_status).HasDefaultValueSql("('0')");

                entity.Property(e => e.force_pwd)
                    .HasMaxLength(100)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.inout_state).HasDefaultValueSql("('0')");

                entity.Property(e => e.is_att)
                    .IsRequired()
                    .HasDefaultValueSql("('False')");

                entity.Property(e => e.lock_active_id).HasDefaultValueSql("('1')");

                entity.Property(e => e.lock_delay).HasDefaultValueSql("('5')");

                entity.Property(e => e.open_door_delay).HasDefaultValueSql("((15))");

                entity.Property(e => e.opendoor_type).HasDefaultValueSql("('6')");

                entity.Property(e => e.reader_io_state).HasDefaultValueSql("('0')");

                entity.Property(e => e.reader_type).HasDefaultValueSql("('0')");

                entity.Property(e => e.sensor_delay).HasDefaultValueSql("('15')");

                entity.Property(e => e.status).HasDefaultValueSql("('0')");

                entity.Property(e => e.supper_pwd)
                    .HasMaxLength(100)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.wiegandInType).HasDefaultValueSql("((1))");

                entity.Property(e => e.wiegandOutType).HasDefaultValueSql("((1))");

                entity.Property(e => e.wiegand_fmt_id).HasDefaultValueSql("('1')");

                entity.Property(e => e.wiegand_fmt_out_id).HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<acc_firstopen>(entity =>
            {
                entity.Property(e => e.change_operator).HasMaxLength(50);

                entity.Property(e => e.change_time).HasColumnType("datetime");

                entity.Property(e => e.create_operator).HasMaxLength(50);

                entity.Property(e => e.create_time).HasColumnType("datetime");

                entity.Property(e => e.delete_operator).HasMaxLength(50);

                entity.Property(e => e.delete_time).HasColumnType("datetime");

                entity.Property(e => e.door_id).HasDefaultValueSql("('1')");

                entity.Property(e => e.status).HasDefaultValueSql("('0')");
            });

            modelBuilder.Entity<acc_firstopen_emp>(entity =>
            {
                entity.HasIndex(e => new { e.accfirstopen_id, e.employee_id })
                    .HasName("UQ__acc_firs__2300FB964B948A53")
                    .IsUnique();
            });

            modelBuilder.Entity<acc_holidays>(entity =>
            {
                entity.HasIndex(e => e.holiday_name)
                    .HasName("UQ__acc_holi__A65EC5D596976ADE")
                    .IsUnique();

                entity.Property(e => e.change_operator).HasMaxLength(50);

                entity.Property(e => e.change_time).HasColumnType("datetime");

                entity.Property(e => e.create_operator).HasMaxLength(50);

                entity.Property(e => e.create_time).HasColumnType("datetime");

                entity.Property(e => e.delete_operator).HasMaxLength(50);

                entity.Property(e => e.delete_time).HasColumnType("datetime");

                entity.Property(e => e.end_date).HasColumnType("datetime");

                entity.Property(e => e.holiday_name)
                    .HasMaxLength(30)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.holiday_type).HasDefaultValueSql("('1')");

                entity.Property(e => e.loop_by_year).HasDefaultValueSql("('2')");

                entity.Property(e => e.memo)
                    .HasMaxLength(70)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.start_date).HasColumnType("datetime");

                entity.Property(e => e.status).HasDefaultValueSql("('0')");
            });

            modelBuilder.Entity<acc_interlock>(entity =>
            {
                entity.HasIndex(e => e.device_id)
                    .HasName("UQ__acc_inte__3B085D8A12E6CC89")
                    .IsUnique();

                entity.Property(e => e.change_operator).HasMaxLength(50);

                entity.Property(e => e.create_operator).HasMaxLength(50);

                entity.Property(e => e.create_time).HasColumnType("datetime");

                entity.Property(e => e.delete_operator).HasMaxLength(50);

                entity.Property(e => e.delete_time).HasColumnType("datetime");

                entity.Property(e => e.four_mode)
                    .IsRequired()
                    .HasDefaultValueSql("('False')");

                entity.Property(e => e.one_mode)
                    .IsRequired()
                    .HasDefaultValueSql("('False')");

                entity.Property(e => e.status).HasDefaultValueSql("('0')");

                entity.Property(e => e.three_mode)
                    .IsRequired()
                    .HasDefaultValueSql("('False')");

                entity.Property(e => e.two_mode)
                    .IsRequired()
                    .HasDefaultValueSql("('False')");
            });

            modelBuilder.Entity<acc_levelset>(entity =>
            {
                entity.HasIndex(e => e.level_name)
                    .HasName("UQ__acc_leve__F94299E93D2AF455")
                    .IsUnique();

                entity.Property(e => e.change_operator).HasMaxLength(30);

                entity.Property(e => e.change_time).HasColumnType("datetime");

                entity.Property(e => e.create_operator).HasMaxLength(30);

                entity.Property(e => e.create_time).HasColumnType("datetime");

                entity.Property(e => e.delete_operator).HasMaxLength(30);

                entity.Property(e => e.delete_time).HasColumnType("datetime");

                entity.Property(e => e.level_name)
                    .HasMaxLength(30)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.status).HasDefaultValueSql("('0')");
            });

            modelBuilder.Entity<acc_levelset_door_group>(entity =>
            {
                entity.HasIndex(e => new { e.acclevelset_id, e.accdoor_id })
                    .HasName("UQ__acc_leve__7FF5F0ED20A4CD01")
                    .IsUnique();

                entity.Property(e => e.accdoor_device_id).HasDefaultValueSql("((0))");

                entity.Property(e => e.accdoor_no_exp).HasDefaultValueSql("((0))");

                entity.Property(e => e.level_timeseg_id).HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<acc_levelset_emp>(entity =>
            {
                entity.HasIndex(e => new { e.acclevelset_id, e.employee_id })
                    .HasName("UQ__acc_leve__C9BFF5562C67BFA6")
                    .IsUnique();
            });

            modelBuilder.Entity<acc_linkageio>(entity =>
            {
                entity.HasIndex(e => e.linkage_name)
                    .HasName("UQ__acc_link__68BC678249ED6600")
                    .IsUnique();

                entity.Property(e => e.change_operator).HasMaxLength(50);

                entity.Property(e => e.change_time).HasColumnType("datetime");

                entity.Property(e => e.create_operator).HasMaxLength(50);

                entity.Property(e => e.create_time).HasColumnType("datetime");

                entity.Property(e => e.delay_time).HasDefaultValueSql("('0')");

                entity.Property(e => e.delete_operator).HasMaxLength(50);

                entity.Property(e => e.delete_time).HasMaxLength(50);

                entity.Property(e => e.in_address).HasDefaultValueSql("('0')");

                entity.Property(e => e.linkage_name).HasMaxLength(50);

                entity.Property(e => e.status).HasDefaultValueSql("('0')");

                entity.Property(e => e.trigger_opt).HasDefaultValueSql("('0')");

                entity.Property(e => e.video_linkageio_id).HasDefaultValueSql("('20')");
            });

            modelBuilder.Entity<acc_map>(entity =>
            {
                entity.HasIndex(e => e.map_name)
                    .HasName("UQ__acc_map__F3B4E0D18771A67D")
                    .IsUnique();

                entity.Property(e => e.change_operator).HasMaxLength(50);

                entity.Property(e => e.change_time).HasColumnType("datetime");

                entity.Property(e => e.create_operator).HasMaxLength(50);

                entity.Property(e => e.create_time).HasColumnType("datetime");

                entity.Property(e => e.delete_operator).HasMaxLength(50);

                entity.Property(e => e.delete_time).HasColumnType("datetime");

                entity.Property(e => e.height).HasDefaultValueSql("('0')");

                entity.Property(e => e.map_name)
                    .HasMaxLength(30)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.status).HasDefaultValueSql("('0')");

                entity.Property(e => e.width).HasDefaultValueSql("('0')");
            });

            modelBuilder.Entity<acc_mapdoorpos>(entity =>
            {
                entity.Property(e => e.change_operator).HasMaxLength(50);

                entity.Property(e => e.change_time).HasColumnType("datetime");

                entity.Property(e => e.create_operator).HasMaxLength(50);

                entity.Property(e => e.create_time).HasColumnType("datetime");

                entity.Property(e => e.delete_operator).HasMaxLength(50);

                entity.Property(e => e.delete_time).HasColumnType("datetime");

                entity.Property(e => e.status).HasDefaultValueSql("('0')");

                entity.Property(e => e.width).HasDefaultValueSql("('40')");
            });

            modelBuilder.Entity<acc_monitor_log>(entity =>
            {
                entity.HasIndex(e => new { e.time, e.pin, e.card_no, e.device_id, e.verified, e.state, e.event_type, e.description, e.event_point_type, e.event_point_id })
                    .HasName("UQ__acc_moni__EC0AB1E4116A4C30")
                    .IsUnique();

                entity.Property(e => e.card_no).HasMaxLength(50);

                entity.Property(e => e.change_operator).HasMaxLength(50);

                entity.Property(e => e.change_time).HasColumnType("datetime");

                entity.Property(e => e.create_operator).HasMaxLength(50);

                entity.Property(e => e.create_time).HasColumnType("datetime");

                entity.Property(e => e.delete_operator).HasMaxLength(50);

                entity.Property(e => e.delete_time).HasColumnType("datetime");

                entity.Property(e => e.description).HasMaxLength(200);

                entity.Property(e => e.device_name).HasMaxLength(50);

                entity.Property(e => e.device_sn).HasMaxLength(50);

                entity.Property(e => e.event_point_id).HasDefaultValueSql("('-1')");

                entity.Property(e => e.event_point_name).HasMaxLength(200);

                entity.Property(e => e.event_point_type).HasDefaultValueSql("('-1')");

                entity.Property(e => e.pin).HasMaxLength(50);

                entity.Property(e => e.status).HasDefaultValueSql("('0')");

                entity.Property(e => e.time).HasColumnType("datetime");

                entity.Property(e => e.verified).HasDefaultValueSql("('200')");
            });

            modelBuilder.Entity<acc_morecardempgroup>(entity =>
            {
                entity.HasIndex(e => e.group_name)
                    .HasName("UQ__acc_more__E8F4F58D06925DC8")
                    .IsUnique();

                entity.Property(e => e.change_operator).HasMaxLength(50);

                entity.Property(e => e.change_time).HasColumnType("datetime");

                entity.Property(e => e.create_operator).HasMaxLength(50);

                entity.Property(e => e.create_time).HasColumnType("datetime");

                entity.Property(e => e.delete_operator).HasMaxLength(50);

                entity.Property(e => e.delete_time).HasColumnType("datetime");

                entity.Property(e => e.group_name).HasMaxLength(50);

                entity.Property(e => e.memo).HasMaxLength(50);

                entity.Property(e => e.status).HasDefaultValueSql("('0')");
            });

            modelBuilder.Entity<acc_morecardgroup>(entity =>
            {
                entity.Property(e => e.change_operator).HasMaxLength(50);

                entity.Property(e => e.change_time).HasColumnType("datetime");

                entity.Property(e => e.create_operator).HasMaxLength(50);

                entity.Property(e => e.create_time).HasColumnType("datetime");

                entity.Property(e => e.delete_operator).HasMaxLength(50);

                entity.Property(e => e.delete_time).HasColumnType("datetime");

                entity.Property(e => e.status).HasDefaultValueSql("('0')");
            });

            modelBuilder.Entity<acc_morecardset>(entity =>
            {
                entity.HasIndex(e => e.comb_name)
                    .HasName("UQ__acc_more__DFE86271A237DABC")
                    .IsUnique();

                entity.Property(e => e.change_operator).HasMaxLength(50);

                entity.Property(e => e.change_time).HasColumnType("datetime");

                entity.Property(e => e.comb_name).HasMaxLength(50);

                entity.Property(e => e.create_operator).HasMaxLength(50);

                entity.Property(e => e.create_time).HasColumnType("datetime");

                entity.Property(e => e.delete_operator).HasMaxLength(50);

                entity.Property(e => e.delete_time).HasColumnType("datetime");

                entity.Property(e => e.status).HasDefaultValueSql("('0')");
            });

            modelBuilder.Entity<acc_reader>(entity =>
            {
                entity.Property(e => e.reader_name).HasMaxLength(50);
            });

            modelBuilder.Entity<acc_reader1>(entity =>
            {
                entity.ToTable("acc_reader", "jorgeLoa_usrals");

                entity.Property(e => e.reader_name).HasMaxLength(50);
            });

            modelBuilder.Entity<acc_timeseg>(entity =>
            {
                entity.HasIndex(e => e.timeseg_name)
                    .HasName("UQ__acc_time__F4DBA3AAC84E276A")
                    .IsUnique();

                entity.Property(e => e.change_operator).HasMaxLength(50);

                entity.Property(e => e.change_time).HasColumnType("datetime");

                entity.Property(e => e.create_operator).HasMaxLength(50);

                entity.Property(e => e.create_time).HasColumnType("datetime");

                entity.Property(e => e.delete_operator).HasMaxLength(50);

                entity.Property(e => e.delete_time).HasColumnType("datetime");

                entity.Property(e => e.friday_end1)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.friday_end2)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.friday_end3)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.friday_start1)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.friday_start2)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.friday_start3)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.holidaytype1_end1)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.holidaytype1_end2)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.holidaytype1_end3)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.holidaytype1_start1)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.holidaytype1_start2)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.holidaytype1_start3)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.holidaytype2_end1)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.holidaytype2_end2)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.holidaytype2_end3)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.holidaytype2_start1)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.holidaytype2_start2)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.holidaytype2_start3)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.holidaytype3_end1)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.holidaytype3_end2)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.holidaytype3_end3)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.holidaytype3_start1)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.holidaytype3_start2)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.holidaytype3_start3)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.memo)
                    .HasMaxLength(70)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.monday_end1)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.monday_end2)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.monday_end3)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.monday_start1)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.monday_start2)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.monday_start3)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.saturday_end1)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.saturday_end2)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.saturday_end3)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.saturday_start1)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.saturday_start2)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.saturday_start3)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.status).HasDefaultValueSql("('0')");

                entity.Property(e => e.sunday_end1)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.sunday_end2)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.sunday_end3)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.sunday_start1)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.sunday_start2)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.sunday_start3)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.thursday_end1)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.thursday_end2)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.thursday_end3)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.thursday_start1)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.thursday_start2)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.thursday_start3)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.timeseg_name).HasMaxLength(50);

                entity.Property(e => e.tuesday_end1)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.tuesday_end2)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.tuesday_end3)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.tuesday_start1)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.tuesday_start2)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.tuesday_start3)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.wednesday_end1)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.wednesday_end2)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.wednesday_end3)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.wednesday_start1)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.wednesday_start2)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");

                entity.Property(e => e.wednesday_start3)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('00:00')");
            });

            modelBuilder.Entity<acc_wiegandfmt>(entity =>
            {
                entity.Property(e => e.change_operator).HasMaxLength(50);

                entity.Property(e => e.change_time).HasColumnType("datetime");

                entity.Property(e => e.create_operator).HasMaxLength(50);

                entity.Property(e => e.create_time).HasColumnType("datetime");

                entity.Property(e => e.delete_operator).HasMaxLength(50);

                entity.Property(e => e.delete_time).HasColumnType("datetime");

                entity.Property(e => e.wiegand_name)
                    .IsRequired()
                    .HasMaxLength(30)
                    .HasDefaultValueSql("('')");
            });

            modelBuilder.Entity<acholiday>(entity =>
            {
                entity.HasKey(e => e.primaryid)
                    .HasName("PK__acholida__77EFAA48030F9B7E");

                entity.Property(e => e.begindate).HasColumnType("datetime");

                entity.Property(e => e.enddate).HasColumnType("datetime");
            });

            modelBuilder.Entity<action_log>(entity =>
            {
                entity.Property(e => e.action_time).HasColumnType("datetime");

                entity.Property(e => e.change_message).HasMaxLength(500);

                entity.Property(e => e.object_repr).HasMaxLength(50);
            });

            modelBuilder.Entity<att_attreport>(entity =>
            {
                entity.HasKey(e => e.ItemName)
                    .HasName("PK__att_attr__4E4373F67B3992BF");

                entity.Property(e => e.ItemName).HasMaxLength(50);

                entity.Property(e => e.ItemType).HasMaxLength(50);

                entity.Property(e => e.Published).HasDefaultValueSql("('0')");
            });

            modelBuilder.Entity<att_waitforprocessdata>(entity =>
            {
                entity.Property(e => e.change_operator).HasMaxLength(50);

                entity.Property(e => e.change_time).HasColumnType("datetime");

                entity.Property(e => e.create_operator).HasMaxLength(50);

                entity.Property(e => e.create_time).HasColumnType("datetime");

                entity.Property(e => e.delete_operator).HasMaxLength(50);

                entity.Property(e => e.delete_time).HasColumnType("datetime");

                entity.Property(e => e.endtime).HasColumnType("datetime");

                entity.Property(e => e.flag).HasDefaultValueSql("('1')");

                entity.Property(e => e.starttime).HasColumnType("datetime");

                entity.Property(e => e.status).HasDefaultValueSql("('0')");
            });

            modelBuilder.Entity<attcalclog>(entity =>
            {
                entity.Property(e => e.DeptID).HasDefaultValueSql("('0')");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.OperTime).HasColumnType("datetime");

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.Type).HasDefaultValueSql("('0')");
            });

            modelBuilder.Entity<attexception>(entity =>
            {
                entity.HasIndex(e => new { e.UserId, e.AttDate, e.StartTime })
                    .HasName("UQ__attexcep__6462F384661CDDCE")
                    .IsUnique();

                entity.Property(e => e.AttDate).HasColumnType("datetime");

                entity.Property(e => e.AuditExcID).HasDefaultValueSql("('0')");

                entity.Property(e => e.EndTime).HasColumnType("datetime");

                entity.Property(e => e.ExceptionID).HasDefaultValueSql("('0')");

                entity.Property(e => e.InScopeTime).HasDefaultValueSql("('0')");

                entity.Property(e => e.Minsworkday).HasDefaultValueSql("('0')");

                entity.Property(e => e.Minsworkday1).HasDefaultValueSql("('0')");

                entity.Property(e => e.OldAuditExcID).HasDefaultValueSql("('0')");

                entity.Property(e => e.OverlapTime).HasDefaultValueSql("('0')");

                entity.Property(e => e.OverlapWorkDayTai).HasDefaultValueSql("('1')");

                entity.Property(e => e.StartTime).HasColumnType("datetime");

                entity.Property(e => e.TimeLong).HasDefaultValueSql("('0')");

                entity.Property(e => e.change_operator).HasMaxLength(50);

                entity.Property(e => e.change_time).HasColumnType("datetime");

                entity.Property(e => e.create_operator).HasMaxLength(50);

                entity.Property(e => e.create_time).HasColumnType("datetime");

                entity.Property(e => e.delete_operator).HasMaxLength(50);

                entity.Property(e => e.delete_time).HasColumnType("datetime");

                entity.Property(e => e.schid).HasDefaultValueSql("('0')");

                entity.Property(e => e.schindex).HasDefaultValueSql("('0')");

                entity.Property(e => e.status).HasDefaultValueSql("('0')");
            });

            modelBuilder.Entity<auth_group>(entity =>
            {
                entity.HasIndex(e => e.name)
                    .HasName("UQ__auth_gro__72E12F1B80CF13D7")
                    .IsUnique();

                entity.Property(e => e.name).HasMaxLength(50);
            });

            modelBuilder.Entity<auth_group_permissions>(entity =>
            {
                entity.HasIndex(e => new { e.group_id, e.permission_id })
                    .HasName("UQ__auth_gro__6B24A40E17EB8056")
                    .IsUnique();
            });

            modelBuilder.Entity<auth_message>(entity =>
            {
                entity.Property(e => e.message).HasMaxLength(50);
            });

            modelBuilder.Entity<auth_permission>(entity =>
            {
                entity.HasIndex(e => new { e.content_type_id, e.codename })
                    .HasName("UQ__auth_per__0A068CF7829C99B0")
                    .IsUnique();

                entity.Property(e => e.codename).HasMaxLength(50);

                entity.Property(e => e.name).HasMaxLength(50);
            });

            modelBuilder.Entity<auth_user>(entity =>
            {
                entity.Property(e => e.last_login).HasColumnType("datetime");

                entity.Property(e => e.password).HasMaxLength(50);

                entity.Property(e => e.username).HasMaxLength(50);
            });

            modelBuilder.Entity<auth_user_groups>(entity =>
            {
                entity.HasIndex(e => new { e.user_id, e.group_id })
                    .HasName("UQ__auth_use__A4E94E5471CC3C2A")
                    .IsUnique();
            });

            modelBuilder.Entity<auth_user_user_permissions>(entity =>
            {
                entity.HasIndex(e => new { e.user_id, e.permission_id })
                    .HasName("UQ__auth_use__07ED06A135CBC8F0")
                    .IsUnique();
            });

            modelBuilder.Entity<base_additiondata>(entity =>
            {
                entity.Property(e => e.create_time).HasColumnType("datetime");

                entity.Property(e => e.data).HasMaxLength(50);

                entity.Property(e => e.delete_time).HasColumnType("datetime");

                entity.Property(e => e.key).HasMaxLength(50);

                entity.Property(e => e.object_id).HasMaxLength(50);

                entity.Property(e => e.value).HasMaxLength(50);
            });

            modelBuilder.Entity<base_appoption>(entity =>
            {
                entity.Property(e => e.change_operator).HasMaxLength(50);

                entity.Property(e => e.change_time).HasColumnType("datetime");

                entity.Property(e => e.create_operator).HasMaxLength(50);

                entity.Property(e => e.create_time).HasColumnType("datetime");

                entity.Property(e => e.delete_operator).HasMaxLength(50);

                entity.Property(e => e.delete_time).HasColumnType("datetime");

                entity.Property(e => e.discribe).HasMaxLength(50);

                entity.Property(e => e.optname).HasMaxLength(50);

                entity.Property(e => e.value).HasMaxLength(50);
            });

            modelBuilder.Entity<base_basecode>(entity =>
            {
                entity.Property(e => e.change_operator).HasMaxLength(50);

                entity.Property(e => e.change_time).HasColumnType("datetime");

                entity.Property(e => e.content).HasMaxLength(50);

                entity.Property(e => e.content_class).HasDefaultValueSql("('0')");

                entity.Property(e => e.create_operator).HasMaxLength(50);

                entity.Property(e => e.create_time).HasColumnType("datetime");

                entity.Property(e => e.delete_operator).HasMaxLength(50);

                entity.Property(e => e.delete_time).HasColumnType("datetime");

                entity.Property(e => e.display).HasMaxLength(50);

                entity.Property(e => e.is_add).HasMaxLength(50);

                entity.Property(e => e.remark).HasMaxLength(50);

                entity.Property(e => e.value).HasMaxLength(50);
            });

            modelBuilder.Entity<base_datatranslation>(entity =>
            {
                entity.Property(e => e.change_operator).HasMaxLength(50);

                entity.Property(e => e.change_time).HasColumnType("datetime");

                entity.Property(e => e.create_operator).HasMaxLength(50);

                entity.Property(e => e.create_time).HasColumnType("datetime");

                entity.Property(e => e.delete_operator).HasMaxLength(50);

                entity.Property(e => e.delete_time).HasColumnType("datetime");

                entity.Property(e => e.display).HasMaxLength(50);

                entity.Property(e => e.language).HasMaxLength(50);

                entity.Property(e => e.property).HasMaxLength(50);

                entity.Property(e => e.value).HasMaxLength(50);
            });

            modelBuilder.Entity<base_operatortemplate>(entity =>
            {
                entity.HasIndex(e => new { e.user_id, e.finger_id, e.fpversion })
                    .HasName("UQ__base_ope__8B92977F1EA1218E")
                    .IsUnique();

                entity.Property(e => e.bitmap_picture).HasColumnType("image");

                entity.Property(e => e.bitmap_picture2).HasColumnType("image");

                entity.Property(e => e.bitmap_picture3).HasColumnType("image");

                entity.Property(e => e.bitmap_picture4).HasColumnType("image");

                entity.Property(e => e.change_operator).HasMaxLength(50);

                entity.Property(e => e.change_time).HasColumnType("datetime");

                entity.Property(e => e.create_operator).HasMaxLength(50);

                entity.Property(e => e.create_time).HasColumnType("datetime");

                entity.Property(e => e.delete_operator).HasMaxLength(50);

                entity.Property(e => e.delete_time).HasColumnType("datetime");

                entity.Property(e => e.fpversion).HasMaxLength(50);

                entity.Property(e => e.template1).HasColumnType("image");

                entity.Property(e => e.template2).HasColumnType("image");

                entity.Property(e => e.template3).HasColumnType("image");

                entity.Property(e => e.utime).HasColumnType("datetime");
            });

            modelBuilder.Entity<base_option>(entity =>
            {
                entity.Property(e => e._default)
                    .HasColumnName("default")
                    .HasMaxLength(50);

                entity.Property(e => e.app_label).HasMaxLength(50);

                entity.Property(e => e.catalog).HasMaxLength(50);

                entity.Property(e => e.change_operator).HasMaxLength(50);

                entity.Property(e => e.change_time).HasColumnType("datetime");

                entity.Property(e => e.create_operator).HasMaxLength(50);

                entity.Property(e => e.create_time).HasColumnType("datetime");

                entity.Property(e => e.delete_operator).HasMaxLength(50);

                entity.Property(e => e.delete_time).HasColumnType("datetime");

                entity.Property(e => e.for_personal)
                    .IsRequired()
                    .HasDefaultValueSql("('True')");

                entity.Property(e => e.group).HasMaxLength(50);

                entity.Property(e => e.help_text).HasMaxLength(50);

                entity.Property(e => e.name).HasMaxLength(50);

                entity.Property(e => e.read_only)
                    .IsRequired()
                    .HasDefaultValueSql("('False')");

                entity.Property(e => e.required)
                    .IsRequired()
                    .HasDefaultValueSql("('False')");

                entity.Property(e => e.type)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('CharField')");

                entity.Property(e => e.validator).HasMaxLength(50);

                entity.Property(e => e.verbose_name).HasMaxLength(50);

                entity.Property(e => e.visible)
                    .IsRequired()
                    .HasDefaultValueSql("('False')");

                entity.Property(e => e.widget).HasMaxLength(50);
            });

            modelBuilder.Entity<base_personaloption>(entity =>
            {
                entity.HasIndex(e => new { e.option_id, e.user_id })
                    .HasName("UQ__base_per__1F712D6AE6488871")
                    .IsUnique();

                entity.Property(e => e.change_operator).HasMaxLength(50);

                entity.Property(e => e.change_time).HasColumnType("datetime");

                entity.Property(e => e.create_operator).HasMaxLength(50);

                entity.Property(e => e.create_time).HasColumnType("datetime");

                entity.Property(e => e.delete_operator).HasMaxLength(50);

                entity.Property(e => e.delete_time).HasColumnType("datetime");

                entity.Property(e => e.value).HasMaxLength(50);
            });

            modelBuilder.Entity<base_strresource>(entity =>
            {
                entity.Property(e => e.app).HasMaxLength(50);

                entity.Property(e => e.change_operator).HasMaxLength(50);

                entity.Property(e => e.change_time).HasColumnType("datetime");

                entity.Property(e => e.create_operator).HasMaxLength(50);

                entity.Property(e => e.create_time).HasColumnType("datetime");

                entity.Property(e => e.delete_operator).HasMaxLength(50);

                entity.Property(e => e.delete_time).HasColumnType("datetime");

                entity.Property(e => e.str).HasMaxLength(50);
            });

            modelBuilder.Entity<base_strtranslation>(entity =>
            {
                entity.Property(e => e.change_operator).HasMaxLength(50);

                entity.Property(e => e.change_time).HasColumnType("datetime");

                entity.Property(e => e.create_operator).HasMaxLength(50);

                entity.Property(e => e.create_time).HasColumnType("datetime");

                entity.Property(e => e.delete_operator).HasMaxLength(50);

                entity.Property(e => e.delete_time).HasColumnType("datetime");

                entity.Property(e => e.display).HasMaxLength(50);

                entity.Property(e => e.language).HasMaxLength(50);
            });

            modelBuilder.Entity<base_systemoption>(entity =>
            {
                entity.Property(e => e.change_operator).HasMaxLength(50);

                entity.Property(e => e.change_time).HasColumnType("datetime");

                entity.Property(e => e.create_operator).HasMaxLength(50);

                entity.Property(e => e.create_time).HasColumnType("datetime");

                entity.Property(e => e.delete_operator).HasMaxLength(50);

                entity.Property(e => e.delete_time).HasColumnType("datetime");

                entity.Property(e => e.value).HasMaxLength(50);
            });

            modelBuilder.Entity<dbapp_viewmodel>(entity =>
            {
                entity.Property(e => e.change_operator).HasMaxLength(50);

                entity.Property(e => e.change_time).HasColumnType("datetime");

                entity.Property(e => e.create_operator).HasMaxLength(50);

                entity.Property(e => e.create_time).HasColumnType("datetime");

                entity.Property(e => e.delete_operator).HasMaxLength(50);

                entity.Property(e => e.delete_time).HasColumnType("datetime");

                entity.Property(e => e.info).HasMaxLength(50);

                entity.Property(e => e.name).HasMaxLength(50);

                entity.Property(e => e.viewtype).HasMaxLength(50);
            });

            modelBuilder.Entity<dbbackuplog>(entity =>
            {
                entity.Property(e => e.change_operator).HasMaxLength(50);

                entity.Property(e => e.change_time).HasColumnType("datetime");

                entity.Property(e => e.create_operator).HasMaxLength(50);

                entity.Property(e => e.create_time).HasColumnType("datetime");

                entity.Property(e => e.delete_operator).HasMaxLength(50);

                entity.Property(e => e.delete_time).HasColumnType("datetime");

                entity.Property(e => e.imflag)
                    .IsRequired()
                    .HasDefaultValueSql("('False')");

                entity.Property(e => e.starttime).HasColumnType("datetime");

                entity.Property(e => e.successflag).HasMaxLength(50);
            });

            modelBuilder.Entity<devcmds>(entity =>
            {
                entity.Property(e => e.CmdCommitTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('2011-07-15 16:06:23.608000')");

                entity.Property(e => e.CmdContent)
                    .IsRequired()
                    .HasColumnType("text");

                entity.Property(e => e.CmdImmediately)
                    .IsRequired()
                    .HasDefaultValueSql("('False')");

                entity.Property(e => e.CmdOverTime).HasColumnType("datetime");

                entity.Property(e => e.CmdReturnContent).HasMaxLength(50);

                entity.Property(e => e.CmdTransTime).HasColumnType("datetime");

                entity.Property(e => e.change_operator).HasMaxLength(50);

                entity.Property(e => e.change_time).HasColumnType("datetime");

                entity.Property(e => e.create_operator).HasMaxLength(50);

                entity.Property(e => e.create_time).HasColumnType("datetime");

                entity.Property(e => e.delete_operator).HasMaxLength(50);

                entity.Property(e => e.delete_time).HasColumnType("datetime");

                entity.Property(e => e.status).HasDefaultValueSql("('0')");
            });

            modelBuilder.Entity<devcmds_bak>(entity =>
            {
                entity.Property(e => e.CmdCommitTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('2011-07-15 16:06:23.952000')");

                entity.Property(e => e.CmdContent).HasMaxLength(50);

                entity.Property(e => e.CmdImmediately)
                    .IsRequired()
                    .HasDefaultValueSql("('False')");

                entity.Property(e => e.CmdOverTime).HasColumnType("datetime");

                entity.Property(e => e.CmdReturnContent).HasMaxLength(50);

                entity.Property(e => e.CmdTransTime).HasColumnType("datetime");

                entity.Property(e => e.change_operator).HasMaxLength(50);

                entity.Property(e => e.change_time).HasColumnType("datetime");

                entity.Property(e => e.create_operator).HasMaxLength(50);

                entity.Property(e => e.create_time).HasColumnType("datetime");

                entity.Property(e => e.delete_operator).HasMaxLength(50);

                entity.Property(e => e.delete_time).HasColumnType("datetime");

                entity.Property(e => e.status).HasDefaultValueSql("('0')");
            });

            modelBuilder.Entity<devlog>(entity =>
            {
                entity.Property(e => e.Cnt).HasDefaultValueSql("('1')");

                entity.Property(e => e.ECnt).HasDefaultValueSql("('0')");

                entity.Property(e => e.OP)
                    .IsRequired()
                    .HasMaxLength(40)
                    .HasDefaultValueSql("('TRANSACT')");

                entity.Property(e => e.Object).HasMaxLength(50);

                entity.Property(e => e.OpTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('2011-07-15 16:06:23.952000')");
            });

            modelBuilder.Entity<django_content_type>(entity =>
            {
                entity.HasIndex(e => new { e.app_label, e.model })
                    .HasName("UQ__django_c__C65C7C817EF0C4C0")
                    .IsUnique();

                entity.Property(e => e.app_label).HasMaxLength(50);

                entity.Property(e => e.model).HasMaxLength(50);

                entity.Property(e => e.name).HasMaxLength(50);
            });

            modelBuilder.Entity<django_session>(entity =>
            {
                entity.HasKey(e => e.session_key)
                    .HasName("PK__django_s__B3BA0F1F7625FA09");

                entity.Property(e => e.session_key).HasMaxLength(40);

                entity.Property(e => e.expire_date).HasColumnType("datetime");

                entity.Property(e => e.session_data).HasMaxLength(50);
            });

            modelBuilder.Entity<empitemdefine>(entity =>
            {
                entity.HasKey(e => e.ItemName)
                    .HasName("PK__empitemd__4E4373F6672FD788");

                entity.Property(e => e.ItemName).HasMaxLength(100);

                entity.Property(e => e.ItemType).HasMaxLength(50);

                entity.Property(e => e.ItemValue).HasMaxLength(50);

                entity.Property(e => e.Published).HasDefaultValueSql("('0')");
            });

            modelBuilder.Entity<iclock_dstime>(entity =>
            {
                entity.HasIndex(e => e.dst_name)
                    .HasName("UQ__iclock_d__F7CC25765BA8613E")
                    .IsUnique();

                entity.Property(e => e.change_operator).HasMaxLength(50);

                entity.Property(e => e.change_time).HasColumnType("datetime");

                entity.Property(e => e.create_operator).HasMaxLength(50);

                entity.Property(e => e.create_time).HasColumnType("datetime");

                entity.Property(e => e.delete_operator).HasMaxLength(50);

                entity.Property(e => e.delete_time).HasColumnType("datetime");

                entity.Property(e => e.dst_name).HasMaxLength(50);

                entity.Property(e => e.end_time).HasMaxLength(50);

                entity.Property(e => e.mode).HasDefaultValueSql("('0')");

                entity.Property(e => e.start_time).HasMaxLength(50);

                entity.Property(e => e.status).HasDefaultValueSql("('0')");
            });

            modelBuilder.Entity<iclock_oplog>(entity =>
            {
                entity.HasIndex(e => new { e.SN, e.OPTime })
                    .HasName("UQ__iclock_o__ECDF8D7483466CA4")
                    .IsUnique();

                entity.Property(e => e.OP).HasDefaultValueSql("('0')");

                entity.Property(e => e.OPTime).HasColumnType("datetime");

                entity.Property(e => e.admin).HasDefaultValueSql("('0')");
            });

            modelBuilder.Entity<iclock_testdata>(entity =>
            {
                entity.Property(e => e.area_id).HasDefaultValueSql("('1')");

                entity.Property(e => e.change_operator).HasMaxLength(50);

                entity.Property(e => e.change_time).HasColumnType("datetime");

                entity.Property(e => e.create_operator).HasMaxLength(50);

                entity.Property(e => e.create_time).HasColumnType("datetime");

                entity.Property(e => e.delete_operator).HasMaxLength(50);

                entity.Property(e => e.delete_time).HasColumnType("datetime");

                entity.Property(e => e.status).HasDefaultValueSql("('0')");
            });

            modelBuilder.Entity<iclock_testdata_admin_area>(entity =>
            {
                entity.HasIndex(e => new { e.testdata_id, e.area_id })
                    .HasName("UQ__iclock_t__CD3299B35DE438D6")
                    .IsUnique();
            });

            modelBuilder.Entity<iclock_testdata_admin_dept>(entity =>
            {
                entity.HasIndex(e => new { e.testdata_id, e.department_id })
                    .HasName("UQ__iclock_t__58957D27491CC919")
                    .IsUnique();
            });

            modelBuilder.Entity<operatecmds>(entity =>
            {
                entity.Property(e => e.Author_id).HasMaxLength(50);

                entity.Property(e => e.CmdCommitTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('2011-07-15 16:06:23.608000')");

                entity.Property(e => e.CmdContent).HasMaxLength(50);

                entity.Property(e => e.change_operator).HasMaxLength(50);

                entity.Property(e => e.change_time).HasColumnType("datetime");

                entity.Property(e => e.cmm_system).HasDefaultValueSql("('1')");

                entity.Property(e => e.commit_time).HasColumnType("datetime");

                entity.Property(e => e.create_operator).HasMaxLength(50);

                entity.Property(e => e.create_time).HasColumnType("datetime");

                entity.Property(e => e.delete_operator).HasMaxLength(50);

                entity.Property(e => e.delete_time).HasColumnType("datetime");

                entity.Property(e => e.process_count).HasDefaultValueSql("('0')");

                entity.Property(e => e.receive_data).HasMaxLength(50);

                entity.Property(e => e.status).HasDefaultValueSql("('0')");

                entity.Property(e => e.success_flag).HasDefaultValueSql("('1')");
            });

            modelBuilder.Entity<personnel_area>(entity =>
            {
                entity.Property(e => e.areaid).HasMaxLength(50);

                entity.Property(e => e.areaname).HasMaxLength(50);

                entity.Property(e => e.change_operator).HasMaxLength(50);

                entity.Property(e => e.change_time).HasColumnType("datetime");

                entity.Property(e => e.create_operator).HasMaxLength(50);

                entity.Property(e => e.create_time).HasColumnType("datetime");

                entity.Property(e => e.delete_operator).HasMaxLength(50);

                entity.Property(e => e.delete_time).HasColumnType("datetime");

                entity.Property(e => e.parent_id).HasDefaultValueSql("((0))");

                entity.Property(e => e.remark).HasMaxLength(50);

                entity.Property(e => e.status).HasDefaultValueSql("('0')");
            });

            modelBuilder.Entity<personnel_cardtype>(entity =>
            {
                entity.Property(e => e.cardtypecode).HasMaxLength(50);

                entity.Property(e => e.cardtypename).HasMaxLength(50);

                entity.Property(e => e.change_operator).HasMaxLength(50);

                entity.Property(e => e.change_time).HasColumnType("datetime");

                entity.Property(e => e.create_operator).HasMaxLength(50);

                entity.Property(e => e.create_time).HasColumnType("datetime");

                entity.Property(e => e.delete_operator).HasMaxLength(50);

                entity.Property(e => e.delete_time).HasColumnType("datetime");
            });

            modelBuilder.Entity<personnel_empchange>(entity =>
            {
                entity.HasKey(e => e.changeno)
                    .HasName("PK__personne__1A36289F853AA179");

                entity.Property(e => e.approvalstatus).HasDefaultValueSql("('2')");

                entity.Property(e => e.change_operator).HasMaxLength(50);

                entity.Property(e => e.change_time).HasColumnType("datetime");

                entity.Property(e => e.changedate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('2011-07-15 16:06:23.936000')");

                entity.Property(e => e.changereason).HasMaxLength(50);

                entity.Property(e => e.create_operator).HasMaxLength(50);

                entity.Property(e => e.create_time).HasColumnType("datetime");

                entity.Property(e => e.delete_operator).HasMaxLength(50);

                entity.Property(e => e.delete_time).HasColumnType("datetime");

                entity.Property(e => e.isvalid)
                    .IsRequired()
                    .HasDefaultValueSql("('0')");

                entity.Property(e => e.newvalue).HasMaxLength(50);

                entity.Property(e => e.oldvalue).HasMaxLength(50);

                entity.Property(e => e.remark).HasMaxLength(50);
            });

            modelBuilder.Entity<personnel_issuecard>(entity =>
            {
                entity.Property(e => e.cardno).HasMaxLength(50);

                entity.Property(e => e.cardpwd).HasMaxLength(50);

                entity.Property(e => e.cardstatus)
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('1')");

                entity.Property(e => e.change_operator).HasMaxLength(50);

                entity.Property(e => e.change_time).HasColumnType("datetime");

                entity.Property(e => e.create_operator).HasMaxLength(50);

                entity.Property(e => e.create_time).HasColumnType("datetime");

                entity.Property(e => e.delete_operator).HasMaxLength(50);

                entity.Property(e => e.delete_time).HasColumnType("datetime");

                entity.Property(e => e.effectivenessdate).HasColumnType("datetime");

                entity.Property(e => e.failuredate).HasColumnType("datetime");

                entity.Property(e => e.issuedate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('2011-07-15')");

                entity.Property(e => e.isvalid)
                    .IsRequired()
                    .HasDefaultValueSql("('1')");
            });

            modelBuilder.Entity<personnel_leavelog>(entity =>
            {
                entity.Property(e => e.change_operator).HasMaxLength(50);

                entity.Property(e => e.change_time).HasColumnType("datetime");

                entity.Property(e => e.create_operator).HasMaxLength(50);

                entity.Property(e => e.create_time).HasColumnType("datetime");

                entity.Property(e => e.delete_operator).HasMaxLength(50);

                entity.Property(e => e.delete_time).HasColumnType("datetime");

                entity.Property(e => e.isClassAccess)
                    .IsRequired()
                    .HasDefaultValueSql("('0')");

                entity.Property(e => e.isClassAtt)
                    .IsRequired()
                    .HasDefaultValueSql("('0')");

                entity.Property(e => e.isReturnCard)
                    .IsRequired()
                    .HasDefaultValueSql("('True')");

                entity.Property(e => e.isReturnClothes)
                    .IsRequired()
                    .HasDefaultValueSql("('True')");

                entity.Property(e => e.isReturnTools)
                    .IsRequired()
                    .HasDefaultValueSql("('True')");

                entity.Property(e => e.leavedate).HasColumnType("datetime");

                entity.Property(e => e.reason).HasMaxLength(50);
            });

            modelBuilder.Entity<userinfo_attarea>(entity =>
            {
                entity.HasIndex(e => new { e.employee_id, e.area_id })
                    .HasName("UQ__userinfo__6CABDD7FA9B4AECD")
                    .IsUnique();
            });

            modelBuilder.Entity<worktable_groupmsg>(entity =>
            {
                entity.Property(e => e.change_operator).HasMaxLength(50);

                entity.Property(e => e.change_time).HasColumnType("datetime");

                entity.Property(e => e.create_operator).HasMaxLength(50);

                entity.Property(e => e.create_time).HasColumnType("datetime");

                entity.Property(e => e.delete_operator).HasMaxLength(50);

                entity.Property(e => e.delete_time).HasColumnType("datetime");

                entity.Property(e => e.status).HasDefaultValueSql("('0')");
            });

            modelBuilder.Entity<worktable_instantmsg>(entity =>
            {
                entity.Property(e => e.change_operator).HasMaxLength(50);

                entity.Property(e => e.change_time).HasColumnType("datetime");

                entity.Property(e => e.content).HasMaxLength(50);

                entity.Property(e => e.create_operator).HasMaxLength(50);

                entity.Property(e => e.create_time).HasColumnType("datetime");

                entity.Property(e => e.delete_operator).HasMaxLength(50);

                entity.Property(e => e.delete_time).HasColumnType("datetime");

                entity.Property(e => e.monitor_last_time)
                    .HasColumnName("monitor last time")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<worktable_msgtype>(entity =>
            {
                entity.Property(e => e.change_operator).HasMaxLength(50);

                entity.Property(e => e.change_time).HasColumnType("datetime");

                entity.Property(e => e.create_operator).HasMaxLength(50);

                entity.Property(e => e.create_time).HasColumnType("datetime");

                entity.Property(e => e.delete_operator).HasMaxLength(50);

                entity.Property(e => e.delete_time).HasColumnType("datetime");

                entity.Property(e => e.msg_ahead_remind).HasDefaultValueSql("('0')");

                entity.Property(e => e.msg_keep_time).HasDefaultValueSql("('1')");

                entity.Property(e => e.msgtype_name).HasMaxLength(50);

                entity.Property(e => e.msgtype_value).HasMaxLength(50);

                entity.Property(e => e.type)
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('0')");
            });

            modelBuilder.Entity<worktable_usrmsg>(entity =>
            {
                entity.Property(e => e.change_operator).HasMaxLength(50);

                entity.Property(e => e.change_time).HasColumnType("datetime");

                entity.Property(e => e.create_operator).HasMaxLength(50);

                entity.Property(e => e.create_time).HasColumnType("datetime");

                entity.Property(e => e.delete_operator).HasMaxLength(50);

                entity.Property(e => e.delete_time).HasColumnType("datetime");

                entity.Property(e => e.msg_id).HasMaxLength(50);

                entity.Property(e => e.status).HasDefaultValueSql("('0')");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
