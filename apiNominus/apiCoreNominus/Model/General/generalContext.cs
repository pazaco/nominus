﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;

namespace apiCoreNominus.Model
{
    public partial class GeneralContext : DbContext
    {
        public GeneralContext() { }
        public GeneralContext(DbContextOptions<GeneralContext> options) : base(options) { }
        public virtual DbSet<isoUserAgent> isoUserAgent { get; set; }
        public virtual DbSet<isoPrimaAFP> isoPrimaAFP { get; set; }
        public virtual DbSet<nomCliente> nomCliente { get; set; }
        public virtual DbSet<nomConexion> nomConexion { get; set; }
        public virtual DbSet<nomCredencial> nomCredencial { get; set; }
        public virtual DbSet<nomTipoDb> nomTipoDb { get; set; }
        public virtual DbSet<nomBuilder> nomBuilder { get; set; }
        public virtual DbSet<nomBuilderCliente> nomBuilderCliente { get; set; }
        public virtual DbSet<nomPersonal> nomPersonal { get; set; }
        public virtual DbSet<nomPersonalPerfil> nomPersonalPerfil { get; set; }
        public virtual DbSet<nomPersonalPerfilCliente> nomPersonalPerfilCliente { get; set; }
        public virtual DbSet<nomSesion> nomSesion { get; set; }
        public virtual DbSet<sisConfig> sisConfig { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                IConfigurationRoot configuration = new ConfigurationBuilder()
                  .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                  .AddJsonFile("appsettings.json")
                  .Build();
                optionsBuilder.UseSqlServer(configuration.GetConnectionString("general"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<isoUserAgent>(entity => {
                entity.HasKey(e => e.iUserAgent);
            });

            modelBuilder.Entity<isoPrimaAFP>(entity => {
                entity.HasKey(e => e.bRegimenPensionario);
            });

            modelBuilder.Entity<nomCliente>(entity =>
            {
                entity.HasKey(e => e.sCliente);

                entity.Property(e => e.sCliente).ValueGeneratedNever();

                entity.Property(e => e.xCliente)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<nomConexion>(entity =>
            {
                entity.HasKey(e => e.sConexion);

                entity.Property(e => e.bTipoDb).HasDefaultValueSql("((3))");

                entity.Property(e => e.xNombreDb)
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<nomCredencial>(entity =>
            {
                entity.HasKey(e => e.sCredencial);

                entity.Property(e => e.xPrefijo)
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.xServerName)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.xUserId)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<nomTipoDb>(entity =>
            {
                entity.HasKey(e => e.bTipoDb);

                entity.Property(e => e.xTipoDb)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<nomBuilder>(entity =>
            {
                entity.HasKey(e => e.iBuilder);

                entity.Property(e => e.cBuilder)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
                entity.Property(e => e.jBuilder)
                    .IsRequired()
                    .IsUnicode(false);
            });

            modelBuilder.Entity<nomBuilderCliente>(entity =>
            {
                entity.HasKey(e => new { e.iBuilder, e.sCliente });
                entity.Property(e => e.jBuilder)
                    .IsRequired()
                    .IsUnicode(false);
            });

            modelBuilder.Entity<nomPersonal>(entity =>
            {
                entity.HasKey(e => e.iPersonaNominus);

                entity.Property(e => e.xCorreo)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<nomPersonalPerfil>(entity =>
            {
                entity.HasKey(e => e.bPerfil);

                entity.Property(e => e.xPerfil)
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<nomPersonalPerfilCliente>(entity =>
            {
                entity.HasKey(e => new { e.iPersonaNominus, e.sCliente, e.bPerfil });

            });

            modelBuilder.Entity<nomSesion>(entity => {
                entity.HasKey(e => e.gSesion);
                });

            modelBuilder.Entity<sisConfig>(entity => {
                entity.HasKey(e => e.sVariable);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
