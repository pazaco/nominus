﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class nomTipoDb
    {
        public byte bTipoDb { get; set; }
        public string xTipoDb { get; set; }
    }
}
