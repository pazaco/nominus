﻿using System;

namespace apiCoreNominus.Model
{
    public partial class nomSesion
    {
        public Guid gSesion { get; set; }
        public int iPersonaNominus { get; set; }
        public int iUserAgent { get; set; }
        public string xTokenFireAuth { get; set; }
        public DateTime dInicio { get; set; }
    }
}
