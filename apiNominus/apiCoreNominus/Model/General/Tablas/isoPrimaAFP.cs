﻿namespace apiCoreNominus.Model
{
    public partial class isoPrimaAFP
    {
        public byte bRegimenPensionario { get; set; }
        public string cClaveBusqueda { get; set; }
        public double? rComisionFlujo { get; set; }
        public double? rComisionMixta { get; set; }
        public double? rPrima { get; set; }
        public double? rAporte { get; set; }
        public decimal? mRemuneracionMaxima { get; set; }
    }
}
