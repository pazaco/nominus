﻿namespace apiCoreNominus.Model
{
    public partial class nomConexion
    {
        public short sConexion { get; set; }
        public short sCliente { get; set; }
        public byte bTipoDb { get; set; }
        public short? sCredencial { get; set; }
        public string xNombreDb { get; set; }
    }
}
