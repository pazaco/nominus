﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class nomPersonalPerfilCliente
    {
        public int iPersonaNominus { get; set; }
        public short sCliente { get; set; }
        public byte bPerfil { get; set; }
        public DateTime? dVigencia { get; set; }
        public Boolean lDefault { get; set; }

    }
}
