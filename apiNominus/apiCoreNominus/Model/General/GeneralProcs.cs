﻿using Microsoft.EntityFrameworkCore.Internal;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace apiCoreNominus.Model
{
    public partial class Procs
    {
        public static GenStringConexion Conexion(TuplaClienteTipoDb clienteTipoDb)
        {
            GenStringConexion rsp = null;
            string[] _pass = new string[] { "", "R3mun3r@rt3", "777Nominus", "19@Als", "a1b300", "v4leria1$", "", "777$Minita", "127#Col*", "Clini##373", "3262#Loaiza", "Rg45=f$4" };
            using (GeneralContext dg = new GeneralContext())
            {
                List<nomConexion> _cnn = dg.nomConexion.Where(c => c.sCliente == clienteTipoDb.sCliente).ToList();
                List<nomTipoDb> _tdb = dg.nomTipoDb.Where(c => c.bTipoDb == clienteTipoDb.bTipoDb).ToList();
                List<nomCredencial> _crd = dg.nomCredencial.ToList();
                rsp = _cnn
                    .Join(_tdb, cn => cn.bTipoDb, cd => cd.bTipoDb, (cn, cd) => new { cn, cd })
                    .Join(_crd, j1 => j1.cn.sCredencial, cc => cc.sCredencial, (j1, cc) =>
                        new GenStringConexion
                        {
                            bTipoDb = j1.cd.bTipoDb,
                            xTipoDb = j1.cd.xTipoDb,
                            xConexion = Helpers.Strings.ConnectionString(cc.xServerName, cc.xPrefijo + "_" + j1.cn.xNombreDb, cc.xPrefijo + "_" + cc.xUserId, _pass[cc.bModeloSeguro ?? 0])
                        }).FirstOrDefault();
            }
            return rsp;
        }

        public static short? HoldingSesion(Guid? sesionPersona)
        {
            short? sServidor = null;
            if (sesionPersona != null)
            {
                using GeneralContext dbG = new GeneralContext();
                nomSesion _ses = dbG.nomSesion.FirstOrDefault(s => s.gSesion == sesionPersona);
                if (_ses != null)
                {
                    nomPersonalPerfilCliente _pfc = dbG.nomPersonalPerfilCliente.FirstOrDefault(p => p.iPersonaNominus == _ses.iPersonaNominus && p.lDefault == true);
                    if (_pfc != null)
                    {
                        sServidor = _pfc.sCliente;
                    }
                }
            }
            return sServidor;
        }
        public static nomPersonalPerfilCliente PerfilSesion(Guid? sesionPersona)
        {
            nomPersonalPerfilCliente rsp = null;
            if (sesionPersona != null)
            {
                using (GeneralContext dbG = new GeneralContext())
                {
                    nomSesion _ses = dbG.nomSesion.FirstOrDefault(s => s.gSesion == sesionPersona);
                    if (_ses != null)
                    {
                         rsp  = dbG.nomPersonalPerfilCliente.FirstOrDefault(p => p.iPersonaNominus == _ses.iPersonaNominus && p.lDefault == true);
                    }
                }
            }
            return rsp;
        }
    }

    #region Outputs
    public class GenBaseConexion
    {
        [Key] public byte bTipoDb { get; set; }
        public string xTipoDb { get; set; }
        public string xServerName { get; set; }
        public string xDb { get; set; }
        public string xUserId { get; set; }
        public byte bModeloSeguro { get; set; }
    }
    public class GenStringConexion
    {
        [Key] public byte bTipoDb { get; set; }
        public string xTipoDb { get; set; }
        public string xConexion { get; set; }
    }
    #endregion

    #region Inputs
    public class TuplaClienteTipoDb
    {
        public short sCliente { get; set; }
        public byte bTipoDb { get; set; }
    }

    public class TuplaAddBuilder
    {
        public string cBuilder { get; set; }
        public string xTitulo { get; set; }

    }

    public class RangoFechas
    {
        public DateTime? dDesde { get; set; }
        public DateTime? dHasta { get; set; }
    }

    public class RspBuilderInicial
    {
        public RspBuilderInicial(TuplaAddBuilder tBuild)
        {
            this.xTitulo = tBuild.xTitulo;
            aCampos = new List<string>();
            aReferencias = new List<string>();
            switch (tBuild.cBuilder.Substring(0, 3))
            {
                case "tbl":
                    cTipo = "tabla";
                    break;
                case "pnl":
                    cTipo = "panel";
                    break;
                case "tit":
                    cTipo = "titulo";
                    break;
                default:
                    cTipo = "form";
                    break;
            }
        }
        public string cTipo { get; set; }
        public string cBuilder { get; set; }
        public List<string> aCampos { get; set; }
        public List<string> aReferencias { get; set; }
        public string xTitulo { get; set; }
    }





    #endregion
}
