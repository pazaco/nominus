﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class asiFlujoPapeleta
    {
        public string iFlujoPapeleta { get; set; }
        public byte bTipoPapeleta { get; set; }
        public int iPapeleta { get; set; }
        public short sConsecutivo { get; set; }
        public string cTipoFlujo { get; set; }
        public DateTime? dFlujo { get; set; }
        public int? iPersona { get; set; }
        public string cRol { get; set; }
        public string xNotificacion { get; set; }
        public int? iNotificacion { get; set; }
    }
}
