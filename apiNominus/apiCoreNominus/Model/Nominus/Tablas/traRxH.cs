﻿using System;

namespace apiCoreNominus.Model
{
    public partial class traRxH
    {
        public int iRxH { get; set; }
        public int iContrato { get; set; }
        public int iPeriodo { get; set; }
        public byte bTipoComprobante { get; set; }
        public short sSerieComprobante { get; set; }
        public int iNumComprobante { get; set; }
        public decimal mMonto { get; set; }
        public DateTime dEmision { get; set; }
        public DateTime dPago { get; set; }
        public bool bRetencion4taCateg { get; set; }

    }
}
