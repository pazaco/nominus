﻿namespace apiCoreNominus.Model
{
    public partial class isoSubClaseConcepto
    {
        public byte bSubClaseConcepto { get; set; }
        public byte bClaseConcepto { get; set; }
        public string xSubClaseConcepto { get; set; }

    }
}
