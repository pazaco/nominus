﻿namespace apiCoreNominus.Model
{
    public partial class isoRegimenPensionario
    {
        public byte bRegimenPensionario { get; set; }
        public string xRegimenPensionario { get; set; }
        public bool bAFP { get; set; }
        public bool bSectorPrivado { get; set; }
        public bool bSectorPublico { get; set; }
        public bool bSectorOtros { get; set; }

    }
}
