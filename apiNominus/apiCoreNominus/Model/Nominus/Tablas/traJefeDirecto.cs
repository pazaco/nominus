﻿namespace apiCoreNominus.Model
{
    public partial class traJefeDirecto
    {
        public int iJefeDirecto { get; set; }
        public int iContrato { get; set; }
        public int iTrabajador { get; set; }

    }

    public class TraJefeDirecto : traJefeDirecto
    {
        public string xJefe { get; set; }
    }
}
