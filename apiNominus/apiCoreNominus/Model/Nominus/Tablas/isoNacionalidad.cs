﻿namespace apiCoreNominus.Model
{
    public partial class isoNacionalidad
    {
        public short sNacionalidad { get; set; }
        public string xNacionalidad { get; set; }
        public string xCiudadano { get; set; }
        public short? sPais { get; set; }
    }
}
