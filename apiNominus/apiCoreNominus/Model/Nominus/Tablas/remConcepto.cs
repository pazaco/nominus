﻿namespace apiCoreNominus.Model
{
    public partial class remConcepto
    {
        public short sConcepto { get; set; }
        public string cConcepto { get; set; }
        public string xConcepto { get; set; }
        public string xTip { get; set; }
        public string xDescripcion { get; set; }
        public byte bClaseConcepto { get; set; }
        public string cConceptoSunat { get; set; }
        public byte bTipoConceptoDefault { get; set; }
        public short sUnidadMedida { get; set; }

    }
}
