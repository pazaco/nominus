﻿using System;

namespace apiCoreNominus.Model
{
    public partial class orgEmpresaActividad
    {
        public short sEmpresa { get; set; }
        public string xActividad { get; set; }
        public DateTime? dInicio { get; set; }

    }
}
