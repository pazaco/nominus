﻿namespace apiCoreNominus.Model
{
    public partial class remConceptoAsistencia
    {
        public short sConcepto { get; set; }
        public byte bMaxDias { get; set; }
        public int iColor { get; set; }

    }
}
