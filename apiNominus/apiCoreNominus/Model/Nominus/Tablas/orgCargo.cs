﻿namespace apiCoreNominus.Model
{
    public partial class orgCargo
    {
        public short sCargo { get; set; }
        public string xCargo { get; set; }
        public short? sArea { get; set; }
        public byte? bNivelCargo { get; set; }
        public int? iCodSunat { get; set; }
    }
}
