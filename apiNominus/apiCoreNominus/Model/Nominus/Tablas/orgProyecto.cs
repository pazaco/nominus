﻿using System;

namespace apiCoreNominus.Model
{
    public partial class orgProyecto
    {
        public short sProyecto { get; set; }
        public string xProyecto { get; set; }
        public string xDescripcion { get; set; }
        public short? sCliente { get; set; }
        public int iUsuarioReg { get; set; }
        public DateTime dFechaReg { get; set; }

    }
}
