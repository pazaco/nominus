﻿namespace apiCoreNominus.Model
{
    public partial class perFamiliar
    {
        public int iFamiliar { get; set; }
        public int iPersona { get; set; }
        public int iPersonaFam { get; set; }
        public byte bVinculoFamiliar { get; set; }

    }
}
