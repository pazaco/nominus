﻿namespace apiCoreNominus.Model
{
    public partial class isoCalculo
    {
        public byte bCalculo { get; set; }
        public byte bTipoCalculo { get; set; }
        public string xCalculo { get; set; }
        public byte bDeclarar { get; set; }
        public byte bUsar { get; set; }

    }
}
