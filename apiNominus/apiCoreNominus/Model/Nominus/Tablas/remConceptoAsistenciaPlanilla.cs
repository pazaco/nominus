﻿namespace apiCoreNominus.Model
{
    public partial class remConceptoAsistenciaPlanilla
    {
        public short sConcepto { get; set; }
        public byte bPlanilla { get; set; }
        public byte bMaxDias { get; set; }

    }
}
