﻿using System;

namespace apiCoreNominus.Model
{
    public partial class traAsistenciaHora
    {
        public int iAsistencia { get; set; }
        public DateTime dtDesde { get; set; }
        public DateTime dtHasta { get; set; }
        public int? iMinutos { get; set; }

    }
}
