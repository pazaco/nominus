﻿namespace apiCoreNominus.Model
{
    public partial class orgEmpresaCalculoCtaContableNeto
    {
        public short sEmpresa { get; set; }
        public byte bCalculo { get; set; }
        public short sCuenta { get; set; }
        public byte bMostrar { get; set; }

    }
}
