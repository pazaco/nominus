﻿namespace apiCoreNominus.Model
{
    public partial class sysMontoEmpresa
    {
        public short sEmpresa { get; set; }
        public short sSysMonto { get; set; }
        public decimal dMonto { get; set; }

    }
}
