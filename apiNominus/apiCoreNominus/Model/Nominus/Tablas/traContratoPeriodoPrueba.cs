﻿using System;

namespace apiCoreNominus.Model
{
    public partial class traContratoPeriodoPrueba
    {
        public int iContratoPeriodo { get; set; }
        public DateTime dHasta { get; set; }

    }
}
