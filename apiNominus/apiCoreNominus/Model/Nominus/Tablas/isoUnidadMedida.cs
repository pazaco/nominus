﻿namespace apiCoreNominus.Model
{
    public partial class isoUnidadMedida
    {

        public short sUnidadMedida { get; set; }
        public string xUnidadMedida { get; set; }
        public string xAbreviado { get; set; }

    }
}
