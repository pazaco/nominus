﻿namespace apiCoreNominus.Model
{
    public partial class remPlanilla
    {
        public byte bPlanilla { get; set; }
        public string xPlanilla { get; set; }
        public byte bTipoPlanilla { get; set; }
        public byte bDeclarar { get; set; }

    }
}
