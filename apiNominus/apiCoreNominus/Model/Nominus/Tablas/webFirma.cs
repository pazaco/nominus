﻿using System;

namespace apiCoreNominus.Model
{
    public partial class webFirma
    {
        public int iFirma { get; set; }
        public int? iNotificacion { get; set; }
        public byte? bRol { get; set; }
        public int? iPersona { get; set; }
        public DateTime? dLeido { get; set; }
        public DateTime? dFirmado { get; set; }

    }
}
