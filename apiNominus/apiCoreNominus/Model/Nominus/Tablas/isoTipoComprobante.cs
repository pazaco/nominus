﻿namespace apiCoreNominus.Model
{
    public partial class isoTipoComprobante
    {
        public byte bTipoComprobante { get; set; }
        public string xTipoComprobante { get; set; }
        public string xCodigo { get; set; }
    }
}
