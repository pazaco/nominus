﻿namespace apiCoreNominus.Model
{
    public partial class traVidaLey
    {
        public int iConceptoFijo { get; set; }
        public short? sAseguradora { get; set; }
        public string xCodigo { get; set; }
        public decimal? rTasa { get; set; }
    }
}
