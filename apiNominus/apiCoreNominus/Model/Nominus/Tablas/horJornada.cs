﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class horJornada
    {
        public short sJornada { get; set; }
        public string xJornada { get; set; }
        public short sHorario { get; set; }
        public string cDias { get; set; }
        public byte? bRequerimientoMarca { get; set; }
        public byte? bOrden { get; set; }
    }
}
