﻿namespace apiCoreNominus.Model
{
    public partial class sysGrupoReporte
    {
        public byte bGrupoReporte { get; set; }
        public string xGrupoReporte { get; set; }
        public byte bOrden { get; set; }
    }
}
