﻿using System;

namespace apiCoreNominus.Model
{
    public partial class isoRegimenPensionarioInfo
    {
        public byte bRegimenPensionario { get; set; }
        public DateTime dDesde { get; set; }
        public decimal rAporte { get; set; }
        public decimal rPrima { get; set; }
        public decimal rComisionFlujo { get; set; }
        public decimal rComisionMixta { get; set; }

    }
}
