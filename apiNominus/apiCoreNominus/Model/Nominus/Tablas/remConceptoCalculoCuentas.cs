﻿namespace apiCoreNominus.Model
{
    public partial class remConceptoCalculoCuentas
    {
        public int iConceptoCalculo { get; set; }
        public short? sCtaDebe { get; set; }
        public short? sCtaHaber { get; set; }
        public byte bMostrarDebe { get; set; }
        public byte bMostrarHaber { get; set; }

    }
}
