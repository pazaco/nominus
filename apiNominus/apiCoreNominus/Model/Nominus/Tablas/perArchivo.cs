﻿using System;

namespace apiCoreNominus.Model
{
    public partial class perArchivo
    {
        public int iArchivo { get; set; }
        public short sOrigenArchivo { get; set; }
        public int iIdOrigenArchivo { get; set; }
        public DateTime? dDesde { get; set; }
        public DateTime? dHasta { get; set; }
        public int iUsuarioReg { get; set; }
        public DateTime dFechaReg { get; set; }
        public byte[] Archivo { get; set; }

    }
}
