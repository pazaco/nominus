﻿namespace apiCoreNominus.Model
{
    public partial class autClaim
    {
        public string cClaim { get; set; }
        public string xClaim { get; set; }
        public byte? bGrupo { get; set; }
        public bool? lPublico { get; set; }

    }
}
