﻿namespace apiCoreNominus.Model
{
    public partial class isoDistrito
    {

        public short sDistrito { get; set; }
        public short sProvincia { get; set; }
        public string xDistrito { get; set; }
        public string xCodSunat { get; set; }

    }
}
