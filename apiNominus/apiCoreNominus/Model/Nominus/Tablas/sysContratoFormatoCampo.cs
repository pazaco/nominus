﻿namespace apiCoreNominus.Model
{
    public partial class sysContratoFormatoCampo
    {
        public short sCampo { get; set; }
        public string xCampo { get; set; }
        public string xDescripcion { get; set; }
    }
}
