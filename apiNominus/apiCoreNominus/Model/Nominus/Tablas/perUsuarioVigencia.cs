﻿using System;

namespace apiCoreNominus.Model
{
    public partial class perUsuarioVigencia
    {
        public int iUsuario { get; set; }
        public DateTime? dDesde { get; set; }
        public DateTime? dHasta { get; set; }

    }
}
