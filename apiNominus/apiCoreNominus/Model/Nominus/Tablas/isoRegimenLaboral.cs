﻿namespace apiCoreNominus.Model
{
    public partial class isoRegimenLaboral
    {
        public byte bRegimenLaboral { get; set; }
        public string xRegimenLaboral { get; set; }
        public string xAbreviado { get; set; }
        public bool bSectorPrivado { get; set; }
        public bool bSectorPublico { get; set; }
        public bool bSectorOtros { get; set; }

    }
}
