﻿using System;

namespace apiCoreNominus.Model
{
    public partial class traHorarioRotativoActividad
    {
        public int iHorarioRotativoActividad { get; set; }
        public int iHorarioRotativo { get; set; }
        public byte bTipoActividadHorario { get; set; }
        public DateTime dtDesde { get; set; }
        public byte bTolerancia { get; set; }
    }
}
