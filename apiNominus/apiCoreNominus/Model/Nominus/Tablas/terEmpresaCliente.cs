﻿using System;

namespace apiCoreNominus.Model
{
    public partial class terEmpresaCliente
    {
        public short sEmpresa { get; set; }
        public int iUsuarioReg { get; set; }
        public DateTime dFechaReg { get; set; }
    }
}
