﻿namespace apiCoreNominus.Model
{
    public partial class isoEntidadFinanciera
    {
        public short sEntidadFinanciera { get; set; }
        public string xEntidadFinanciera { get; set; }
        public string xAbreviado { get; set; }

    }
}
