﻿namespace apiCoreNominus.Model
{
    public partial class sysAccesoTab
    {

        public int iTab { get; set; }
        public short sMenu { get; set; }
        public string xNombre { get; set; }
        public byte bTabIndex { get; set; }
        public short sOrden { get; set; }
        public bool? bAcceso { get; set; }
        public bool? bAgregar { get; set; }
        public bool? bEditar { get; set; }
        public bool? bEliminar { get; set; }
        public bool bExportar { get; set; }
        public bool bImportar { get; set; }
        public bool bConfig { get; set; }
        public bool bProceso { get; set; }
        public bool bImprimir { get; set; }

    }
}
