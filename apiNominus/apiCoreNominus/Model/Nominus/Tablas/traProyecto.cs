﻿namespace apiCoreNominus.Model
{
    public partial class traProyecto
    {
        public int iContratoProyecto { get; set; }
        public int iContrato { get; set; }
        public short sProyecto { get; set; }
        public decimal rPorcentaje { get; set; }
        public bool? bPrincipal { get; set; }

    }
}
