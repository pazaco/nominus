﻿using System;

namespace apiCoreNominus.Model
{
    public partial class traHorarioSemanalActividad
    {
        public int iHorarioSemanalActividad { get; set; }
        public int iHorarioSemanal { get; set; }
        public byte bDiaSemana { get; set; }
        public byte bTipoActividadHorario { get; set; }
        public TimeSpan tDesde { get; set; }
        public byte bTolerancia { get; set; }

    }
}
