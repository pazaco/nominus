﻿namespace apiCoreNominus.Model
{
    public partial class isoTipoTrabajador
    {
        public byte bTipoTrabajador { get; set; }
        public string xTipoTrabajador { get; set; }
        public bool bSectorPrivado { get; set; }
        public bool bSectorPublico { get; set; }
        public bool bSectorOtros { get; set; }

    }
}
