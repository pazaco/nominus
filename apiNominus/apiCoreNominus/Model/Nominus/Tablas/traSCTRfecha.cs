﻿using System;

namespace apiCoreNominus.Model
{
    public partial class traSCTRfecha
    {
        public int iContrato { get; set; }
        public DateTime dDesde { get; set; }
        public DateTime? dHasta { get; set; }
        public byte bSCTRestado { get; set; }


    }
}
