﻿using System;

namespace apiCoreNominus.Model
{
    public partial class traAsistenciaFechaCalculo
    {
        public int iAsistencia { get; set; }
        public DateTime dDesde { get; set; }
        public DateTime dHasta { get; set; }
        public int? iDias { get; set; }
        public int iCalculoPlanilla { get; set; }

    }
}
