﻿namespace apiCoreNominus.Model
{
    public partial class isoMoneda
    {
        public short sMoneda { get; set; }
        public string xMoneda { get; set; }
        public string xMonedaPlural { get; set; }
        public string xSimbolo { get; set; }
        public string xCodigo { get; set; }
        public byte rDecimales { get; set; }

    }
}
