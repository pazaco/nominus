﻿namespace apiCoreNominus.Model
{
    public partial class sysEpsPlantilla
    {
        public byte bEpsPlantilla { get; set; }
        public string xEpsPlantilla { get; set; }
        public bool bDefault { get; set; }
        public bool bPermiteCantidad { get; set; }
        public bool? bContarParaFactor { get; set; }
        public bool? bContarParaCalculo { get; set; }
        public bool? bAumentarIGV { get; set; }
        public bool bNoAsumeEmpresa { get; set; }

    }
}
