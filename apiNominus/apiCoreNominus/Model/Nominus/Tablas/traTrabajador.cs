﻿using iTextSharp.text;
using System.Collections.Generic;
using System.Security.Policy;

namespace apiCoreNominus.Model
{
    public partial class traTrabajador
    {
        public int iTrabajador { get; set; }
        public byte bRegimenPensionario { get; set; }
        public bool bDiscapacidad { get; set; }
        public byte bConvenioEvitarDobleTributacion { get; set; }
        public byte bEstadoCivil { get; set; }
    }

    public class TraTrabajador: traTrabajador
    {
        public perPersona persona { get; set; }
        public autUsuario auth { get; set; }
        public string xGenero { get; set; }
        public string xRegimenPensionario { get; set; }
        public string xTipoDocIdentidad { get; set; }
        public string xEstadoCivil { get; set; }
        public IEnumerable<TraContrato> contratos { get; set; }
    }


    public class PerTrabajador : perPersona
    {
        public PerTrabajador()
        {
            xEstadoCivil = (new string[] { "", "Solter", "Casad", "Divorciad", "Viud", "Conviviente" })[bEstadoCivil] + (bEstadoCivil % 5 == 0 ? "" : bGenero == 0 ? "o" : "a");
        }
        public byte bRegimenPensionario { get; set; }
        public bool bDiscapacidad { get; set; }
        public byte bConvenioEvitarDobleTributacion { get; set; }
        public byte bEstadoCivil { get; set; }
        public string xEstadoCivil { get; set; }
        public string xTipoDocIdentidad { get; set; }
        public IEnumerable<TraContrato> contratos { get; set; }
    }


    public class TraTrabajadorAsistencia : TraTrabajador
    {
        public List<horProgramacion> horario { get; set; }
        public List<marMarcacion> marcacion { get; set; }
        public List<asiControl> controlAsistencia { get; set; }
        public List<asiPapeleta> papeletas { get; set; }
    }
}
