﻿namespace apiCoreNominus.Model
{
    public partial class traSCTR
    {

        public int iContrato { get; set; }
        public byte bTipoSctrPension { get; set; }
        public byte bRiesgoSctrPension { get; set; }
        public byte bTipoSctrSalud { get; set; }
        public byte bRiesgoSctrSalud { get; set; }

    }
}
