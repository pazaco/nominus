﻿namespace apiCoreNominus.Model
{
    public partial class terEmpresa
    {
        public short sEmpresa { get; set; }
        public string cRUC { get; set; }
        public string xRazonSocial { get; set; }
        public string xNombreComercial { get; set; }
        public string xAbreviado { get; set; }

    }
}
