﻿namespace apiCoreNominus.Model
{
    public partial class horHorario
    {
        public short sHorario { get; set; }
        public short sEmpresa { get; set; }
        public string xHorario { get; set; }
        public byte? bPrioridad { get; set; }
    }
}
