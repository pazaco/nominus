﻿namespace apiCoreNominus.Model
{
    public partial class sysMontoAnual
    {
        public short sSysMonto { get; set; }
        public short sAno { get; set; }
        public decimal dMonto { get; set; }

    }
}
