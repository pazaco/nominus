﻿namespace apiCoreNominus.Model
{
    public partial class traHorarioSemanal
    {

        public int iHorarioSemanal { get; set; }
        public int iContrato { get; set; }
        public short sHorario { get; set; }
        public byte bConsiderarCalculo { get; set; }

    }
}
