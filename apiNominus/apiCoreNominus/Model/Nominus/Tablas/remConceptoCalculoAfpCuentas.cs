﻿namespace apiCoreNominus.Model
{
    public partial class remConceptoCalculoAfpCuentas
    {
        public int iConceptoCalculo { get; set; }
        public byte bRegimenPensionario { get; set; }
        public short? sCtaDebe { get; set; }
        public short? sCtaHaber { get; set; }
        public byte bMostrar { get; set; }

    }
}
