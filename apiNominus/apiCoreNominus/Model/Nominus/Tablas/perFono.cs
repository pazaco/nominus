﻿namespace apiCoreNominus.Model
{
    public partial class perFono
    {
        public int iFono { get; set; }
        public int iPersona { get; set; }
        public string xNumeroFono { get; set; }
        public string xDescripcion { get; set; }
        public byte bPrioridad { get; set; }

    }
}
