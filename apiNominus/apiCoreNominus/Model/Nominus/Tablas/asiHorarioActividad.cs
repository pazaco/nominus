﻿using System;

namespace apiCoreNominus.Model
{
    public partial class asiHorarioActividad
    {
        public int iHorarioActividad { get; set; }
        public short sHorario { get; set; }
        public byte bDiaSemana { get; set; }
        public byte bTipoActividadHorario { get; set; }
        public TimeSpan tDesde { get; set; }
        public byte bTolerancia { get; set; }

    }
}
