﻿using System;

namespace apiCoreNominus.Model
{
    public partial class asiMarcacionTemp
    {
        public long iMarcacionTemp { get; set; }
        public string xCodigoPersonal { get; set; }
        public DateTime dtMarcacion { get; set; }
        public short? sRelojMarcacion { get; set; }
        public bool bLeido { get; set; }
    }
}
