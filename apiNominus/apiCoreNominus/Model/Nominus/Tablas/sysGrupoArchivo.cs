﻿namespace apiCoreNominus.Model
{
    public partial class sysGrupoArchivo
    {

        public byte bGrupoArchivo { get; set; }
        public string xGrupoArchivo { get; set; }
        public byte bOrden { get; set; }

    }
}
