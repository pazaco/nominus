﻿namespace apiCoreNominus.Model
{
    public partial class isoCategoriaOcupacional
    {

        public byte bCategoriaOcupacional { get; set; }
        public string xCategoriaOcupacional { get; set; }
        public bool bSectorPrivado { get; set; }
        public bool bSectorPublico { get; set; }
        public bool bSectorOtros { get; set; }

    }
}
