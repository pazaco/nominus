﻿namespace apiCoreNominus.Model
{
    public partial class orgEmpresaCtaBancaria
    {
        public short sEmpresaCtaBancaria { get; set; }
        public short sEmpresa { get; set; }
        public byte bTipoCtaBancaria { get; set; }
        public short sEntidadFinanciera { get; set; }
        public short sMoneda { get; set; }
        public string xCtaBancaria { get; set; }

    }
}
