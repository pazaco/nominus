﻿namespace apiCoreNominus.Model
{
    public partial class traConceptoManual
    {
        public int iConceptoManual { get; set; }
        public int iContrato { get; set; }
        public int iPeriodo { get; set; }
        public short sConcepto { get; set; }
        public decimal mValor { get; set; }

    }
}
