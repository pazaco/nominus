﻿namespace apiCoreNominus.Model
{
    public partial class sysArchivo
    {
        public short sArchivo { get; set; }
        public string xArchivo { get; set; }
        public byte bGrupoArchivo { get; set; }
        public string xObjectDw { get; set; }
        public short sOrden { get; set; }
        public string xExtension { get; set; }
        public bool? bEstado { get; set; }

    }
}
