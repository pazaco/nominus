﻿using System;

namespace apiCoreNominus.Model
{
    public partial class asiFeriado
    {
        public short sFeriado { get; set; }
        public string xFeriado { get; set; }
        public bool bFijo { get; set; }
        public bool bEspecifico {get;set;}
       public DateTime dFecha { get; set; }
    }
}
