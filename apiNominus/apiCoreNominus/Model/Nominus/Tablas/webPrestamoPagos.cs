﻿using System;

namespace apiCoreNominus.Model
{
    public partial class webPrestamoPagos
    {
        public int iPrestamoCuota { get; set; }
        public int iPrestamo { get; set; }
        public byte bCalculo { get; set; }
        public DateTime dFecha { get; set; }
        public int mValor { get; set; }
        public bool bAnulado { get; set; }
        public bool bCalculoAdelanto { get; set; }
        public int? iCalculoPlanilla { get; set; }
    }
}
