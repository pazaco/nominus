﻿namespace apiCoreNominus.Model
{
    public partial class sysAccesoMenu
    {

        public short sMenu { get; set; }
        public string xNombre { get; set; }
        public string xMenu { get; set; }
        public short sOrden { get; set; }

    }
}
