﻿namespace apiCoreNominus.Model
{
    public partial class remImportaPlanillaColumna
    {
        public byte bCalculo { get; set; }
        public short sConcepto { get; set; }
        public byte bColumna { get; set; }
    }
}
