﻿namespace apiCoreNominus.Model
{
    public partial class traCentroCosto
    {
        public int iContratoCentroCosto { get; set; }
        public int iContrato { get; set; }
        public short sCentroCosto { get; set; }
        public decimal rPorcentaje { get; set; }
        public bool? bPrincipal { get; set; }

    }
}
