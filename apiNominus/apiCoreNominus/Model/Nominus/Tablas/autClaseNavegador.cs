﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class autClaseNavegador
    {

        public byte bClaseNavegador { get; set; }
        public string xClaseNavegador { get; set; }
        public bool? lMobile { get; set; }

    }
}
