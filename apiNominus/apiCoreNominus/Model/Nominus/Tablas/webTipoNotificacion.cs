﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class webTipoNotificacion
    {
         public byte bTipoNotificacion { get; set; }
        public string xTipoNotificacion { get; set; }
        public string xFaIcono { get; set; }

    }
}
