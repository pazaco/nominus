﻿using System;

namespace apiCoreNominus.Model
{
    public partial class autSesion
    {
        public Guid gSesion { get; set; }
        public int iUsuario { get; set; }
        public string cClaim { get; set; }
        public DateTime? dVigencia { get; set; }
        public short? sNavegador { get; set; }

    }
}
