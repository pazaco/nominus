﻿namespace apiCoreNominus.Model
{
    public partial class remConceptoCalculo
    {
        public int iConceptoCalculo { get; set; }
        public short sEmpresa { get; set; }
        public byte bCalculo { get; set; }
        public byte bPlanilla { get; set; }
        public short sConcepto { get; set; }
        public byte bTipoConcepto { get; set; }
        public short sOrdenCalculo { get; set; }
        public short sOrdenMostrar { get; set; }
        public bool bAfecta { get; set; }

    }
}
