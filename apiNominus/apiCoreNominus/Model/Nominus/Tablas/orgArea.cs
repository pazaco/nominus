﻿namespace apiCoreNominus.Model
{
    public partial class orgArea
    {
        public short sArea { get; set; }
        public string xArea { get; set; }
        public short? sAreaPadre { get; set; }

    }
}
