﻿namespace apiCoreNominus.Model
{
    public partial class isoMotivoCese
    {

        public byte bMotivoCese { get; set; }
        public string xMotivoCese { get; set; }
        public bool bSectorPrivado { get; set; }
        public bool bSectorPublico { get; set; }
        public bool bSectorOtros { get; set; }
        public bool bSoloPensionista { get; set; }

    }
}
