﻿using System;

namespace apiCoreNominus.Model
{
    public partial class horMargenIncidencia
    {
        public int iMargen { get; set; }
        public short sMargen { get; set; }
        public int? iDiferencia { get; set; }
        public short? sIncidencia { get; set; }
    }
}
