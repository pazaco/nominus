﻿namespace apiCoreNominus.Model
{
    public partial class asiTransaccion
    {
        public string iTransaccion { get; set; }
        public byte bTipoPapeleta { get; set; }
        public int iPapeleta { get; set; }
        public short sConsecutivo { get; set; }
        public short? sTipoTransaccion { get; set; }
        public string jsonTransaccion { get; set; }

    }
}
