﻿using System;

namespace apiCoreNominus.Model
{
    public partial class traContratoPeriodoCese
    {
        public int iContratoPeriodo { get; set; }
        public DateTime dCese { get; set; }
        public byte bMotivo { get; set; }
        public string xObservacion { get; set; }
        public DateTime? dLiquidacion { get; set; }
        public string xObservacionLiquidacion { get; set; }
    }
}
