﻿namespace apiCoreNominus.Model
{
    public partial class orgEmpresaRepLegal
    {
        public short sEmpresa { get; set; }
        public int? iPersona { get; set; }
        public string xPartida { get; set; }

    }
}
