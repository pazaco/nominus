﻿using System;

namespace apiCoreNominus.Model
{
    public partial class webPrestamo
    {
        public int iPrestamo { get; set; }
        public int iContrato { get; set; }
        public short sConcepto { get; set; }
        public short sMoneda { get; set; }
        public decimal mValor { get; set; }
        public DateTime? dPrestamo { get; set; }
        public int? mPagos { get; set; }
    }
}
