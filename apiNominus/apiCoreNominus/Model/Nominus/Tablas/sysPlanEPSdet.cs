﻿using System;

namespace apiCoreNominus.Model
{
    public partial class sysPlanEPSdet
    {

        public short sPlanEPSdet { get; set; }
        public short sPlanEPS { get; set; }
        public byte bEpsPlantilla { get; set; }
        public decimal mValor { get; set; }
        public int iUsuarioReg { get; set; }
        public DateTime dFechaReg { get; set; }

    }
}
