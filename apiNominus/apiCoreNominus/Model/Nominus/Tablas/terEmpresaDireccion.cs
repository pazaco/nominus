﻿namespace apiCoreNominus.Model
{
    public partial class terEmpresaDireccion
    {
        public short sEmpresa { get; set; }
        public byte? bTipoVia { get; set; }
        public string xNombreVia { get; set; }
        public string xNroVia { get; set; }
        public string xDpto { get; set; }
        public string xInterior { get; set; }
        public string xManzana { get; set; }
        public string xLote { get; set; }
        public string xKilometro { get; set; }
        public string xBlock { get; set; }
        public string xEtapa { get; set; }
        public byte? bTipoZona { get; set; }
        public string xNombreZona { get; set; }
        public string xReferencia { get; set; }
        public short sDistrito { get; set; }

    }
}
