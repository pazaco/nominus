﻿namespace apiCoreNominus.Model
{
    public partial class remImportaPlanilla
    {
        public byte bCalculo { get; set; }
        public int iPersona { get; set; }
        public short sEmpresa { get; set; }
        public short sAno { get; set; }
        public byte bMes { get; set; }
        public byte bNumero { get; set; }
        public byte bPlanilla { get; set; }
        public short sConcepto { get; set; }
        public decimal mMonto { get; set; }
    }
}
