﻿namespace apiCoreNominus.Model
{
    public partial class remConceptoAsistenciaEmpresaPlanilla
    {
        public short sConcepto { get; set; }
        public short sEmpresa { get; set; }
        public byte bPlanilla { get; set; }
        public byte bMaxDias { get; set; }

    }
}
