﻿namespace apiCoreNominus.Model
{
    public partial class orgSede
    {

        public short sSede { get; set; }
        public string xSede { get; set; }
        public short CodEstablecimiento { get; set; }
        public bool bCentroRiesgo { get; set; }

    }
}
