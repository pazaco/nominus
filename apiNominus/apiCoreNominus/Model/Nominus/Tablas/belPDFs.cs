﻿using System;

namespace apiCoreNominus.Model
{
    public partial class belPDFs
    {
        public string cPDF { get; set; }
        public int? iCalculoPlanilla { get; set; }
        public DateTime? dGeneracion { get; set; }
    }
}
