﻿using apiCoreNominus.Model;
using System;
using System.Collections.Generic;
using System.Security.Policy;

namespace apiCoreNominus.Model
{
    public class traContrato
    {
        public int iContrato { get; set; }
        public int iTrabajador { get; set; }
        public short sEmpresa { get; set; }
        public short sSede { get; set; }
        public short sCargo { get; set; }
    }

    public class traContratoLaboral
    {
        public int iContrato { get; set; }
        public byte bPlanilla { get; set; }
        public byte bTipoTrabajador { get; set; }
        public bool bRegAtipicoDeJornadaTrab { get; set; }
        public bool bJornadaTrabMax { get; set; }
        public bool bHorarioNoc { get; set; }
        public bool bSindicalizado { get; set; }
        public bool bRentas5taExoneradas { get; set; }
        public byte bSituacion { get; set; }
        public byte bSituacionEspecial { get; set; }
        public byte bCategoriaOcupacional { get; set; }
        public byte bCategoria { get; set; }
        public byte bTipoPago { get; set; }
        public byte bPeriodicidadRem { get; set; }
    }

    public class TraContrato : traContratoLaboral
    {
        public int iTrabajador { get; set; }
        public short sEmpresa { get; set; }
        public short sSede { get; set; }
        public short sCargo { get; set; }
        public string xEmpresa { get; set; }
        public string xEmpresaAbreviado { get; set; }
        public string xSede { get; set; }
        public string xCargo { get; set; }
        public string xTipoTrabajador { get; set; }
        public int? iJefe { get; set; }
        public string xJefe { get; set; }
        public IEnumerable<TraContratoPeriodo> periodos { get; set; }
        public TraContratoPeriodo vigente { get; set; }
    }

    public class traStatusContrato 
    {
        public int iContrato { get; set; }
        public int iContratoPeriodo { get; set; }
        public int iTrabajador { get; set; }
        public short sEmpresa { get; set; }
        public short sSede { get; set; }
        public short sCargo { get; set; }
        public string cTipo { get; set; }
        public DateTime? dDesde { get; set; }
        public DateTime? dHasta { get; set; }
        public byte bFormato { get; set; }
    }

    public class traEstadoContrato
    {
        public int iTrabajador { get; set; }
        public int iContrato { get; set; }
        public int iContratoPeriodo { get; set; }
        public short sEmpresa { get; set; }
        public short sSede { get; set; }
        public string xSede { get; set; }
        public short sCargo { get; set; }
        public string xCargo { get; set; }
        public byte bPlanilla { get; set; }
        public DateTime dDesde { get; set; }
        public DateTime? dHasta { get; set; }
        public byte bFormato { get; set; }
        public DateTime? dCese { get; set; } 
        public byte? bMotivo { get; set; }
        public int? iJefe { get; set; }
        public byte bEstadoTrabajador { get; set; }
    }


    public class TraStatusContrato : traStatusContrato
    {
        public string xEmpresa { get; set; }
        public string xSede { get; set; }
        public string xCargo { get; set; }
        public string xFormato { get; set; }
        public string xTipoContrato { get; set; }
        public traContratoPeriodoCese cese { get; set; }
        public string xMotivoCese { get; set; }
    }
}
