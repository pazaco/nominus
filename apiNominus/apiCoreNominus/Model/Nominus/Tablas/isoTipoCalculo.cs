﻿namespace apiCoreNominus.Model
{
    public partial class isoTipoCalculo
    {
        public byte bTipoCalculo { get; set; }
        public string xTipoCalculo { get; set; }
    }
}
