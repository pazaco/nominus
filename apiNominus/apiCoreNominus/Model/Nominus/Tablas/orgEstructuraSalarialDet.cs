﻿namespace apiCoreNominus.Model
{
    public partial class orgEstructuraSalarialDet
    {
        public int iEstructuraSalarialDet { get; set; }
        public short sEstructuraSalarial { get; set; }
        public short sConcepto { get; set; }
        public decimal mMonto { get; set; }

    }
}
