﻿using System;

namespace apiCoreNominus.Model
{
    public partial class asiPapeleta
    {
        public byte bTipoPapeleta { get; set; }
        public int iPapeleta { get; set; }
        public int? iAsistencia { get; set; }
        public short? sIncidencia { get; set; }
        public double? fDebe { get; set; }
        public double? fHaber { get; set; }
        public double? fDescuento { get; set; }
        public double? fExtras { get; set; }
        public double? fInjustificadas { get; set; }
        public DateTime? dAlta { get; set; }

    }
}
