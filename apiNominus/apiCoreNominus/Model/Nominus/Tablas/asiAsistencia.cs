﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class asiAsistencia
    {
        public int iAsistencia { get; set; }
        public int iContrato { get; set; }
        public DateTime dFecha { get; set; }
    }

    public class AsiAsistencia : asiAsistencia
    {
        public List<AsiControl> controles { get; set; }
        public AsiEvaluacionCOVID evaluacionCOVID { get; set; }
        public MarDispositivoTeletrabajo gps { get; set; }
    }
}
