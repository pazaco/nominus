﻿using System;

namespace apiCoreNominus.Model
{
    public class perVistaContractual
    {
        public int iPersona { get; set; }
        public short sEmpresa { get; set; }
        public int iContrato { get; set; }
        public int iContratoPeriodo { get; set; }
        public string xApellidos { get; set; }
        public string xNombres { get; set; }
        public string xDocIdentidad { get; set; }
        public DateTime? dDesde { get; set; }
        public DateTime? dHasta { get; set; }
        public DateTime? dCese { get; set; }
        public string xSede { get; set; }
        public string xCargo { get; set; }
    }
}
