﻿namespace apiCoreNominus.Model
{
    public partial class orgEmpresaImagen
    {
        public short sEmpresaImagen { get; set; }
        public short sEmpresa { get; set; }
        public byte bTipoImagen { get; set; }
        public string jImagen { get; set; }
        public byte[] iImagen { get; set; }

    }
}
