﻿using System;

namespace apiCoreNominus.Model
{
    public partial class traAsistenciaHoraCalculo
    {
        public int iAsistencia { get; set; }
        public DateTime dtDesde { get; set; }
        public DateTime dtHasta { get; set; }
        public int? iMinutos { get; set; }
        public int iCalculoPlanilla { get; set; }

    }
}
