﻿namespace apiCoreNominus.Model
{
    public partial class orgEmpresaCtaContable
    {
        public short sCuenta { get; set; }
        public short sEmpresa { get; set; }
        public string xCuenta { get; set; }
        public string xNombre { get; set; }

    }
}
