﻿namespace apiCoreNominus.Model
{
    public partial class horTipoRotacion
    {
        public short sRotacion { get; set; }
        public string cFactor { get; set; }
        public byte? bFactor { get; set; }
        public byte? bCoeficiente { get; set; }
        public string cDescriptor { get; set; }
        public short sHorario { get; set; }
    }
}
