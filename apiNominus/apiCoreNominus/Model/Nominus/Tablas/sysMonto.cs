﻿namespace apiCoreNominus.Model
{
    public partial class sysMonto
    {
        public short sSysMonto { get; set; }
        public string xSysMonto { get; set; }
        public string xDescripcion { get; set; }
        public decimal dMonto { get; set; }

    }
}
