﻿namespace apiCoreNominus.Model
{
    public partial class isoDepartamento
    {

        public short sDepartamento { get; set; }
        public short sPais { get; set; }
        public string xDepartamento { get; set; }
        public string xCodSunat { get; set; }

    }
}
