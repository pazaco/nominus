﻿namespace apiCoreNominus.Model
{
    public partial class orgNivelCentroCosto
    {

        public byte bNivel { get; set; }
        public string xNivel { get; set; }
        public byte? bNivelPadre { get; set; }

    }
}
