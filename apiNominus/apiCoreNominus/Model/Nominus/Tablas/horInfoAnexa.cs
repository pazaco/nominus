﻿using System;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class horInfoAnexa
    {
        public short sInfoAnexa { get; set; }
        public short? sJornada { get; set; }
        public string xDescriptor { get; set; }

    }
}
