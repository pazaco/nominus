﻿using System;

namespace apiCoreNominus.Model
{
    public partial class sysAdministrador
    {
        public int iPersona { get; set; }
        public DateTime? dVigencia { get; set; }
    }
}
