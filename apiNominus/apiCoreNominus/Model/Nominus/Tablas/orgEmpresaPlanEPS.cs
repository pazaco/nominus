﻿namespace apiCoreNominus.Model
{
    public partial class orgEmpresaPlanEPS
    {
        public short sEmpresaPlanEPS { get; set; }
        public short sEmpresa { get; set; }
        public short sPlanEPS { get; set; }

    }
}
