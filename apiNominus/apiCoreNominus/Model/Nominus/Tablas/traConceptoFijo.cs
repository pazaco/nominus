﻿using System;

namespace apiCoreNominus.Model
{
    public partial class traConceptoFijo
    {
        public int iConceptoFijo { get; set; }
        public byte bCalculo { get; set; }
        public int iContrato { get; set; }
        public short sConcepto { get; set; }
        public DateTime? dDesde { get; set; }
        public DateTime? dHasta { get; set; }
        public decimal mValor { get; set; }

    }
}
