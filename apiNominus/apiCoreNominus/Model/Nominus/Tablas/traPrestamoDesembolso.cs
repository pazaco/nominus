﻿using System;

namespace apiCoreNominus.Model
{
    public partial class traPrestamoDesembolso
    {
        public int iPrestamo { get; set; }
        public DateTime dFecha { get; set; }
        public short? sConcepto { get; set; }

    }
}
