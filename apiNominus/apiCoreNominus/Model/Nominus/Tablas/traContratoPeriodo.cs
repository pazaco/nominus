﻿using System;

namespace apiCoreNominus.Model
{
    public partial class traContratoPeriodo
    {
        public int iContratoPeriodo { get; set; }
        public int iContrato { get; set; }
        public DateTime dDesde { get; set; }
        public DateTime? dHasta { get; set; }
        public byte bFormato { get; set; }
    }

    public class TraContratoPeriodo : traContratoPeriodo
    {
        public string xTipoContrato { get; set; }
        public TraContratoFormato formato { get; set; }
        public traContratoPeriodoCese cese { get; set; }
    }
}
