﻿using System;

namespace apiCoreNominus.Model
{
    public partial class sysPlanEPS
    {
        public short sPlanEPS { get; set; }
        public short sAseguradora { get; set; }
        public string xPlanEPS { get; set; }
        public int iUsuarioReg { get; set; }
        public DateTime dFechaReg { get; set; }

    }
}
