﻿using System;
using System.Collections.Generic;
using System.Data;

namespace apiCoreNominus.Model
{
    public partial class asiControl
    {
        public int iControl { get; set; }
        public int? iAsistencia { get; set; }
        public short? sControl { get; set; }
        public DateTime? dHoraHorario { get; set; }
        public DateTime? dHoraControl { get; set; }
        public short? sIncidencia { get; set; }
        public byte? bTipoPapeleta { get; set; }
        public int? iPapeleta { get; set; }
    }

    public class AsiControl : asiControl
    {
        public string cTipoControl { get; set; }
        public string xMargen { get; set; }
        public IEnumerable<horMargenIncidencia> margenes { get; set; }
        public DateTime? dMinima { get; set; }
        public DateTime? dMaxima { get; set; }
    }
}
