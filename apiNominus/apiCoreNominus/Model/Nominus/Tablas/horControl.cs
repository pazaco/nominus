﻿using System;

namespace apiCoreNominus.Model
{
    public partial class horControl
    {
        public short sJornada { get; set; }
        public short sControl { get; set; }
        public string cTipoControl { get; set; }
        public TimeSpan dHora { get; set; }
        public short? sControlPrevio { get; set; }
        public byte? sDiasPrevio { get; set; }
        public short? sMargen { get; set; }

    }
}
