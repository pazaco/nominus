﻿namespace apiCoreNominus.Model
{
    public partial class sysConcepto
    {
        public short sSysConcepto { get; set; }
        public string xSysConcepto { get; set; }
        public string xDescripcion { get; set; }
        public short sConcepto { get; set; }

    }
}
