﻿using System;

namespace apiCoreNominus.Model
{
    public partial class sysOrigenArchivo
    {
        public short sOrigenArchivo { get; set; }
        public string xOrigenArchivo { get; set; }
        public string xTabla { get; set; }
        public bool bMostrar { get; set; }
        public int iUsuarioReg { get; set; }
        public DateTime dFechaReg { get; set; }
    }
}
