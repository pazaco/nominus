﻿using System;

namespace apiCoreNominus.Model
{
    public partial class asiDiaria
    {
        public int iTrabajador { get; set; }
        public DateTime dAsistencia { get; set; }
        public DateTime? hET { get; set; }
        public DateTime? hER { get; set; }
        public DateTime? hSR { get; set; }
        public DateTime? hST { get; set; }
    }
}
