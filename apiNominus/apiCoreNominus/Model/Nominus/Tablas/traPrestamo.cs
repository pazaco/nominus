﻿namespace apiCoreNominus.Model
{
    public  class traPrestamo
    {
        public int iPrestamo { get; set; }
        public int iContrato { get; set; }
        public short sConcepto { get; set; }
        public short sMoneda { get; set; }
        public decimal mValor { get; set; }

    }
}
