﻿using System;

namespace apiCoreNominus.Model
{
    public partial class traContratoPeriodoMintra
    {
        public int iContratoPeriodo { get; set; }
        public string xNumero { get; set; }
        public DateTime? dIngreso { get; set; }

    }
}
