﻿using System;

namespace apiCoreNominus.Model
{
    public partial class webMovimientoVacaciones
    {
        public long? iNovedadVacaciones { get; set; }
        public int? iContrato { get; set; }
        public short? sCiclo { get; set; }
        public DateTime dDesde { get; set; }
        public DateTime? dHasta { get; set; }
        public int? iDias { get; set; }
        public string cTipoNovedad { get; set; }
        public int? iFirmaJefe { get; set; }
        public int? iFirmaRRHH { get; set; }
        public string xDescripcion { get; set; }
    }
}
