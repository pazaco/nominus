﻿namespace apiCoreNominus.Model
{
    public partial class traArea
    {
        public int iContrato { get; set; }
        public short sArea { get; set; }
        public decimal rPorcentaje { get; set; }
        public bool? bPrincipal { get; set; }

    }
}
