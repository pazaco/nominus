﻿using System;

namespace apiCoreNominus.Model
{
    public partial class remPeriodo
    {
        public int iPeriodo { get; set; }
        public short sEmpresa { get; set; }
        public byte bCalculo { get; set; }
        public byte bPlanilla { get; set; }
        public int iAnoMes { get; set; }
        public short? sAno { get; set; }
        public byte? bMes { get; set; }
        public int? iAnoNumero { get; set; }
        public byte bNumero { get; set; }
        public DateTime dDesde { get; set; }
        public DateTime dHasta { get; set; }
        public DateTime dDesdeAsistencia { get; set; }
        public DateTime dHastaAsistencia { get; set; }
        public bool bEstado { get; set; }
        public bool bNotificado { get; set; }

    }
}
