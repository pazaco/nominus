﻿namespace apiCoreNominus.Model
{
    public partial class traContratoFormato : TraContratoFormato
    {
        public string jFormato { get; set; }
        public string jFormatoSinPrueba { get; set; }
        public string jFormatoRenovacion { get; set; }
    }

    public class TraContratoFormato
    {
        public byte bFormato { get; set; }
        public string xFormato { get; set; }
        public byte bTipoContrato { get; set; }
    }
}
