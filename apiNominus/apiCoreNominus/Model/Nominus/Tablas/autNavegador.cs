﻿namespace apiCoreNominus.Model
{
    public partial class autNavegador
    {

        public short sNavegador { get; set; }
        public string xNavegador { get; set; }
        public byte bClaseNavegador { get; set; }
        public int? iLatencia { get; set; }

    }
}
