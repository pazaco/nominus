﻿namespace apiCoreNominus.Model
{
    public partial class isoTipoPlanilla
    {
        public byte bTipoPlanilla { get; set; }
        public string xTipoPlanilla { get; set; }
        public byte bPeriodicidadRem { get; set; }

    }
}
