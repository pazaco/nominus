﻿using System;

namespace apiCoreNominus.Model
{
    public partial class asiMarcacion
    {
        public long iMarcacion { get; set; }
        public int iPersona { get; set; }
        public DateTime dtMarcacion { get; set; }
        public short sRelojMarcacion { get; set; }

    }
}
