﻿using System;

namespace apiCoreNominus.Model
{
    public partial class horProgramacion
    {
        public int iProgramacion { get; set; }
        public short? sRotacion { get; set; }
        public int iContrato { get; set; }
        public DateTime? dDesde { get; set; }
        public DateTime? dHasta { get; set; }
        public double? fHorasInicial { get; set; }
        public double? fHorasTrabajadas { get; set; }
        public double? fHorasRedimidas { get; set; }
        public byte? bPrioridad { get; set; }
    }
    public partial class horProgramacionActual
    {
        public int iProgramacion { get; set; }
        public short? sRotacion { get; set; }
        public int iContrato { get; set; }
        public DateTime? dDesde { get; set; }
        public DateTime? dHasta { get; set; }
        public double? fHorasInicial { get; set; }
        public double? fHorasTrabajadas { get; set; }
        public double? fHorasRedimidas { get; set; }
        public byte? bPrioridad { get; set; }
    }

    public partial class horDetProgramacion
    {
        public int iDetProgramacion { get; set; }
        public int iProgramacion { get; set; }
        public DateTime dFecha { get; set; }
        public short sJornada { get; set; }
    }

    public partial class horTag
    {
        public short sJornada { get; set; }
        public string cTag { get; set; }
        public string cColor { get; set; }
    }

}
