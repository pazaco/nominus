﻿using System;

namespace apiCoreNominus.Model
{
    public partial class traHorarioRotativo
    {

        public int iHorarioRotativo { get; set; }
        public int iContrato { get; set; }
        public short sHorario { get; set; }
        public DateTime dFecha { get; set; }

    }
}
