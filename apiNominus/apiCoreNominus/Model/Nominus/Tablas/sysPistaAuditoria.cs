﻿using System;

namespace apiCoreNominus.Model
{
    public partial class sysPistaAuditoria
    {
        public long iPistaAuditoria { get; set; }
        public int iSP { get; set; }
        public byte bAccion { get; set; }
        public int iTabla { get; set; }
        public byte bColumna { get; set; }
        public int iUsuario { get; set; }
        public int iId { get; set; }
        public string xDato { get; set; }
        public DateTime dtFechaHora { get; set; }
    }
}
