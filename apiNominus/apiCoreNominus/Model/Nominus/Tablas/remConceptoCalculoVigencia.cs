﻿using System;

namespace apiCoreNominus.Model
{
    public partial class remConceptoCalculoVigencia
    {
        public int iConceptoCalculo { get; set; }
        public DateTime? dDesde { get; set; }
        public DateTime? dHasta { get; set; }

    }
}
