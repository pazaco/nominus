﻿namespace apiCoreNominus.Model
{
    public partial class isoTipoVia
    {
        public byte bTipoVia { get; set; }
        public string xTipoVia { get; set; }
        public string xAbreviado { get; set; }

    }
}
