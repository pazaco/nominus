﻿using System;

namespace apiCoreNominus.Model
{
    public partial class orgEmpresaPeriodo
    {
        public short sEmpresa { get; set; }
        public DateTime? dDesde { get; set; }
        public DateTime? dHasta { get; set; }

    }
}
