﻿using iTextSharp.text;
using System.Collections.Generic;

namespace apiCoreNominus.Model
{
    public partial class orgEmpresa
    {
        public short sEmpresa { get; set; }
        public string cRUC { get; set; }
        public string xRazonSocial { get; set; }
        public string xNombreComercial { get; set; }
        public string xAbreviado { get; set; }
        public byte bRegimenLaboral { get; set; }
        public string jAppConfig { get; set; }
    }

    public class OrgEmpresaAppConfig
    {
        public string cEvalRiesgo { get; set; }
        public bool? lAsistencia { get; set; }
        public bool? lQRin { get; set; }
        public bool? lQRout { get; set; }
        public bool? lBEL { get; set; }
        public bool? lZkAccess { get; set; }
        public List<string> proxiWiFi { get; set; }
        public List<string> proxiBLE { get; set; }
        public List<OrgGPS> proxiGPS { get; set; }
    }
}
