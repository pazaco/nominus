﻿namespace apiCoreNominus.Model
{
    public partial class horIncidencia
    {
        public short sIncidencia { get; set; }
        public string xIncidencia { get; set; }
        public bool lNotificaEmpleado { get; set; }
        public bool lNotificaJefe { get; set; }
        public bool lNotificaRRHH { get; set; }
        public bool lAutorizaJefe { get; set; }
        public bool lAutorizaRRHH { get; set; }
        public bool bTipoPapeleta { get; set; }
    }
}
