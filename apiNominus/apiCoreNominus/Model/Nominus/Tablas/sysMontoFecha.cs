﻿using System;

namespace apiCoreNominus.Model
{
    public partial class sysMontoFecha
    {
        public short sSysMonto { get; set; }
        public DateTime dDesde { get; set; }
        public decimal dMonto { get; set; }

    }
}
