﻿namespace apiCoreNominus.Model
{
    public partial class isoTipoSuspensionRL
    {

        public byte bTipoSuspensionRL { get; set; }
        public string xTipoSuspensionRL { get; set; }
        public bool bImperfecta { get; set; }

    }
}
