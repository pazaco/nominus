﻿namespace apiCoreNominus.Model
{
    public partial class isoRegimenPensionarioCodigo
    {
        public byte bRegimenPensionario { get; set; }
        public string xCodigo { get; set; }

    }
}
