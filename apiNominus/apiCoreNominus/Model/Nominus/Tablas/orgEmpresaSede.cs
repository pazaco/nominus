﻿namespace apiCoreNominus.Model
{
    public partial class orgEmpresaSede
    {
        public int iEmpresaSede { get; set; }
        public short sEmpresa { get; set; }
        public short sSede { get; set; }
        public short sCodEstablecimiento { get; set; }
        public bool bCentroRiesgo { get; set; }

    }
}
