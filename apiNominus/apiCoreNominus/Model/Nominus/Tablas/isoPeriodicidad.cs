﻿namespace apiCoreNominus.Model
{
    public partial class isoPeriodicidad
    {
         public byte bPeriodicidadRem { get; set; }
        public string xPeriodicidadRem { get; set; }
        public byte? bPeriodicidadSunat { get; set; }

    }
}
