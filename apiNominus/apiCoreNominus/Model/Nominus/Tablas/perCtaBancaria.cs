﻿namespace apiCoreNominus.Model
{
    public partial class perCtaBancaria
    {
        public int iCtaBancaria { get; set; }
        public int iPersona { get; set; }
        public short sEmpresaCtaBancaria { get; set; }
        public byte bTipoCtaBancaria { get; set; }
        public short sEntidadFinanciera { get; set; }
        public short sMoneda { get; set; }
        public string xCtaBancaria { get; set; }
        public string xCtaInterBancaria { get; set; }

    }
}
