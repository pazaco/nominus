﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace apiCoreNominus.Model
{
    public partial class Procs
    {
        public static List<WebNotificar> Notificaciones(int iUsuario, short sServidor)
        {
            List<WebNotificar> resp = new List<WebNotificar>();
            using (NominusContext db = new NominusContext(sServidor))
            {
                resp = db.WebNotificar.FromSqlRaw("exec webNotificar @iUsuario", new SqlParameter("iUsuario", iUsuario)).ToList();
            }
            return resp;
        }
    }

    public class BigWebNotificar : WebNotificar
    {
        public short sServidor { get; set; }
    }

    public class WebNotificar
    {
        [Key] 
        public int iNotificacion { get; set; }
        public DateTime? dEnvio { get; set; }
        public byte? bTipoNotificacion { get; set; }
        public int? iReferencia { get; set; }
        public int? iContrato { get; set; }
        public string xAsunto { get; set; }
        public string xFaIcono { get; set; }
        public string xUrl { get; set; }
        public DateTime? dLeido { get; set; }
        public DateTime? dFirmado { get; set; }
    }



}
