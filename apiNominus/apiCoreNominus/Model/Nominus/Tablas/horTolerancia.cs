﻿namespace apiCoreNominus.Model
{
    public partial class horTolerancia
    {
        public byte bTolerancia { get; set; }
        public string xTolerancia { get; set; }
        public bool? lMarcaControl { get; set; }
        public bool lJefeAutoriza { get; set; }
        public bool? lNotificarJefe { get; set; }
        public bool? lAbrirPuerta { get; set; }
    }
}
