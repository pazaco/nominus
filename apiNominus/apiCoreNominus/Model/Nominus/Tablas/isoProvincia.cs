﻿namespace apiCoreNominus.Model
{
    public partial class isoProvincia
    {
        public short sProvincia { get; set; }
        public short sDepartamento { get; set; }
        public string xProvincia { get; set; }
        public string xCodSunat { get; set; }

    }
}
