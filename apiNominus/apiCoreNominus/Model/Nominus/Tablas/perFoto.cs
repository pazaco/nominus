﻿namespace apiCoreNominus.Model
{
    public partial class perFoto
    {
        public int iFoto { get; set; }
        public int iPersona { get; set; }
        public byte[] Foto { get; set; }
        public byte? bTipoFoto { get; set; }
        public string jFoto { get; set; }

    }
}
