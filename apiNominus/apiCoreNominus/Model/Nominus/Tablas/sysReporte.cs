﻿namespace apiCoreNominus.Model
{
    public partial class sysReporte
    {
        public short sReporte { get; set; }
        public string xReporte { get; set; }
        public byte bGrupoReporte { get; set; }
        public string xObjectDw { get; set; }
        public short sOrden { get; set; }
        public bool? bEstado { get; set; }
    }
}
