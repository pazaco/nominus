﻿using System;

namespace apiCoreNominus.Model
{
    public partial class asiRiesgoCOVID
    {
        public int iAsistencia { get; set; }
        public double fTemperatura { get; set; }
        public string aSintomas { get; set; }
        public byte bEvaluacion { get; set; }
        public bool? lContacto { get; set; }
        public bool? lLavadoManos { get; set; }
        public bool? lTapabocas { get; set; }
        public bool? lRopas { get; set; }
    }

    public partial class marRiesgoCOVID
    {
        public int iContrato { get; set; }
        public DateTime dFecha { get; set; }
        public double fPeso { get; set; }
        public byte bTalla { get; set; } 
        public string aEnfermedades { get; set; }
    }

    public class AsiEvaluacionCOVID : asiRiesgoCOVID
    {
        public marRiesgoCOVID general { get; set; }
    }

}

