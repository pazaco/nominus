﻿namespace apiCoreNominus.Model
{
    public partial class orgEstructuraSalarial
    {

        public short sEstructuraSalarial { get; set; }
        public string xEstructuraSalarial { get; set; }
        public byte bDiasEfectivos { get; set; }
        public byte bDiasTotal { get; set; }
        public bool? bHrsExt_diasEfectiv { get; set; }
        public bool? bHrsExt_incluyeAsigFam { get; set; }
        public bool bBonifNoc_diasEfectiv { get; set; }
        public bool bBonifNoc_incluyeAsigFam { get; set; }
        public byte bCantidadTrabajadores { get; set; }
        public decimal mSubTotal { get; set; }

    }
}
