﻿using System;

namespace apiCoreNominus.Model
{
    public partial class traAFP
    {
        public int iTrabajador { get; set; }
        public string xCUSPP { get; set; }
        public DateTime? dDesde { get; set; }
        public bool? bMixta { get; set; }

    }
}
