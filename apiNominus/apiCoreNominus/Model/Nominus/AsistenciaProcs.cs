﻿using apiCoreNominus.Controllers.Asistencia;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace apiCoreNominus.Model
{
    public partial class Procs
    {
        public static List<horJornada_> asiListaHorario(parHorario horario)
        {
            List<horJornada_> rsp = new List<horJornada_>();
            using (NominusContext dbN = new NominusContext(horario.sServidor))
            {
                rsp = dbN.horJornada
                    .Where(h => h.sHorario == horario.sHorario)
                    .Select(h => new horJornada_
                    {
                        sHorario = h.sHorario,
                        sJornada = h.sJornada,
                        xJornada = h.xJornada,
                        cDias = h.cDias,
                        bRequerimientoMarca = h.bRequerimientoMarca,
                        xRequerimientoMarca = dbN.horRequerimientoMarca.FirstOrDefault(c => c.bRequerimientoMarca == h.bRequerimientoMarca).xRequerimientoMarca,
                        lBorrable = !dbN.horControl.Any(j => j.sJornada == h.sJornada),
                        controles = dbN.horControl
                            .Where(co => co.sJornada == h.sJornada)
                            .Select(ct => new horControl_
                            {
                                sControl = ct.sControl,
                                sJornada = ct.sJornada,
                                cTipoControl = ct.cTipoControl,
                                dHora = ct.dHora,
                                sControlPrevio = ct.sControlPrevio,
                                sDiasPrevio = ct.sDiasPrevio,
                                sMargen = ct.sMargen,
                                xMargen = dbN.horMargen.FirstOrDefault(c => c.sMargen == ct.sMargen).xMargen,
                                lBorrable = dbN.asiControl.Any(c => c.sControl == ct.sControl),
                                cOrden = $"D{ct.sDiasPrevio}H{ct.dHora}"
                            }).ToList()
                    }).ToList();
            }
            return rsp;
        }

  


        public static List<AsiControl>  GetControlesAsistencia(NominusContext dbN, int iAsistencia)
        {
            List<AsiControl> rsp = dbN.asiControl.Where(f=>f.iAsistencia==iAsistencia).AsEnumerable()
                .Join(dbN.horControl, c => c.sControl, ch => ch.sControl, (c, ch) => new { c, ch })
                .Join(dbN.horMargen, c => c.ch.sMargen, chm => chm.sMargen, (c, chm) => new { c, chm })
                .GroupJoin(dbN.horMargenIncidencia, c => c.chm.sMargen, chmi => chmi.sMargen, (c, chmi) => new { c, cm = chmi.Any() ? chmi.OrderBy(o => o.iDiferencia).AsEnumerable() : null })
                .Select(s => new AsiControl {
                    iControl = s.c.c.c.iControl,
                    iAsistencia = s.c.c.c.iAsistencia,
                    sControl = s.c.c.c.sControl,
                    dHoraHorario = s.c.c.c.dHoraHorario,
                    dHoraControl = s.c.c.c.dHoraControl,
                    sIncidencia = s.c.c.c.sIncidencia,
                    bTipoPapeleta = s.c.c.c.bTipoPapeleta,
                    iPapeleta = s.c.c.c.iPapeleta,
                    cTipoControl = s.c.c.ch.cTipoControl,
                    xMargen = s.c.chm.xMargen,
                    margenes = s.cm
                }).OrderBy(o=>o.dHoraHorario).ToList();
            DateTime dPivot = rsp.Min(d => (DateTime)d.dHoraHorario).Date;
            DateTime dLimit = rsp.Max(d => (DateTime)d.dHoraHorario).Date.AddDays(1).AddMinutes(-1);
            foreach(AsiControl s in rsp)
            {
                if (s.dHoraControl != null)
                {
                    s.dMinima = s.dHoraControl;
                    s.dMaxima = s.dHoraControl;
                    dPivot = dPivot < (DateTime)s.dHoraControl ? (DateTime)s.dHoraControl : dPivot;
                } else {
                    if (s.margenes == null)
                    {
                        s.dMinima = ((DateTime)s.dHoraHorario).AddMinutes(-5);
                        s.dMaxima = ((DateTime)s.dHoraHorario).AddMinutes(5);
                    }
                    else
                    {
                        int minM = s.margenes.OrderBy(o => (int)o.iDiferencia).First().iDiferencia??0;
                        int maxM = s.margenes.OrderByDescending(o => (int)o.iDiferencia).First().iDiferencia??0;
                        DateTime minD = ((DateTime)s.dHoraHorario).AddMinutes((int)minM);
                        DateTime maxD = ((DateTime)s.dHoraHorario).AddMinutes((int)maxM);
                        s.dMinima = dPivot < minD ? minD : dPivot;
                        s.dMaxima = dLimit < maxD ? dLimit : maxD;
                        dPivot = dPivot < (DateTime)s.dMinima ? (DateTime)s.dMinima : dPivot;
                    }
                }
            }
            return rsp;
        }



    }

    public class horHorario_ : horHorario
    {
        public bool lBorrable { get; set; }
    }

    public class tipHorario
    {
        public List<horMargen> margenes { get; set; }
        public List<horRequerimientoMarca> requerimientosMarca { get; set; }
    }

    public class parNuevoHorario : parEmpresa
    {
        public string xHorario { get; set; }
    }

    public class parModifHorario : parNuevoHorario
    {
        public short sHorario { get; set; }
    }

    public class parHorario
    {
        public short sServidor { get; set; }
        public short sHorario { get; set; }
    }

    public class parActualizarJornadas : parHorario
    {
        public List<horJornada_> jornadas { get; set; }
    }

    public class horJornada_ : horJornada
    {
        public string xRequerimientoMarca { get; set; }
        public bool lBorrable { get; set; }
        public List<horControl_> controles { get; set; }
    }

    public class parNuevaJornada : parHorario
    {
        public string xJornada { get; set; }
        public string cDias { get; set; }
        public byte bRequerimientoMarca { get; set; }
        public byte bPrioridad { get; set; }
    }

    public class parModifJornada : parNuevaJornada
    {
        public short sJornada { get; set; }
    }

    public class horControl_ : horControl
    {
        public string xMargen { get; set; }
        public bool lBorrable { get; set; }
        public string cOrden { get; set; }
    }

    public class HorIncidencias
    {
        public List<horIncidencia> incidencias { get; set; }
        public List<asiTipoPapeleta> tiposPapeleta { get; set; }
    }

    public class setJornada
    {
        public List<horJornada_> jornadas { get; set; }
        public List<horControl_> controles { get; set; }
    }

    public  class horMargen_ : horMargen
    {
        public bool lBorrable { get; set; }
    }

    public class parNuevoEvento: parEvento
    {
        public string xMargen { get; set; }
    }

    public class parModifEvento: parNuevoEvento
    {
        public short sMargen { get; set; }
    }


}
