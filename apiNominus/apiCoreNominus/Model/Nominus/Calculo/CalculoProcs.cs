﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace apiCoreNominus.Model
{
    public partial class Procs
    {
        public static List<RemTvCalculo> treeviewCalculos(NominusContext dbN, byte bDeclarar = 9, byte bTodas = 0)
        {
            return dbN.RemTvCalculo.FromSqlRaw("exec remTvCalculo @bDeclarar,@bTodas", new SqlParameter("bDeclarar", bDeclarar), new SqlParameter("bTodas", bTodas)).ToList();
        }

        public static List<RemCboAno> comboAnho(NominusContext dbN, ParCboAno pars)
        {
            return dbN.RemCboAno.FromSqlRaw("exec isoCboAno @sEmpresa, @bCalculo, @bPlanilla, @bEstado",
                new SqlParameter("sEmpresa", pars.sEmpresa),
                new SqlParameter("bCalculo", pars.bCalculo),
                new SqlParameter("bPlanilla", pars.bPlanilla),
                new SqlParameter("bEstado", pars.bEstado)
                ).ToList();
        }

        public static List<RemCboMes> comboMes(NominusContext dbN, ParCboMes pars)
        {
            return dbN.RemCboMes.FromSqlRaw("exec isoCboMes @sEmpresa, @bCalculo, @bPlanilla, @sAno, @bEstado",
                new SqlParameter("sEmpresa", pars.sEmpresa),
                new SqlParameter("bCalculo", pars.bCalculo),
                new SqlParameter("bPlanilla", pars.bPlanilla),
                new SqlParameter("sAno", pars.sAno),
                new SqlParameter("bEstado", pars.bEstado)
                ).ToList();
        }

        public static List<RemCboNumero> comboNumero(NominusContext dbN, ParCboNumero pars)
        {
            return dbN.RemCboNumero.FromSqlRaw("exec isoCboNumero @sEmpresa, @bCalculo, @bPlanilla, @sAno,@bMes, @bEstado",
                new SqlParameter("sEmpresa", pars.sEmpresa),
                new SqlParameter("bCalculo", pars.bCalculo),
                new SqlParameter("bPlanilla", pars.bPlanilla),
                new SqlParameter("sAno", pars.sAno),
                new SqlParameter("bMes", pars.bMes),
                new SqlParameter("bEstado", pars.bEstado)
                ).ToList();
        }
    }
}
