﻿using Microsoft.AspNetCore.SignalR.Protocol;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;


namespace apiCoreNominus.Model
{
    public partial class NominusContext : DbContext
    {
        public virtual DbSet<remCalculoPlanilla> remCalculoPlanilla { get; set; }
        public virtual DbSet<remCalculoPlanillaConcepto> remCalculoPlanillaConcepto { get; set; }
        public virtual DbSet<remCalculoPlanillaConceptoAfecta> remCalculoPlanillaConceptoAfecta { get; set; }
        public virtual DbSet<remCalculoPlanillaConceptoCc> remCalculoPlanillaConceptoCc { get; set; }
        public virtual DbSet<remCalculoPlanillaConceptoCta> remCalculoPlanillaConceptoCta { get; set; }
        public virtual DbSet<remCalculoPlanillaConceptoFormula> remCalculoPlanillaConceptoFormula { get; set; }
        public virtual DbSet<RemTvCalculo> RemTvCalculo { get; set; }
        public virtual DbSet<RemCboAno> RemCboAno { get; set; }
        public virtual DbSet<RemCboMes> RemCboMes { get; set; }
        public virtual DbSet<RemCboNumero> RemCboNumero { get; set; }

        partial void OnModelCreatingCalculo(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<remCalculoPlanilla>(entity =>
            {
                entity.HasKey(e => e.iCalculoPlanilla);
                entity.Property(e => e.dCreacion)
                    .HasColumnType("datetime2(0)")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<remCalculoPlanillaConcepto>(entity =>
            {
                entity.HasKey(e => new { e.iCalculoPlanilla, e.sConcepto });
                entity.Property(e => e.mMonto).HasColumnType("money");
            });

            modelBuilder.Entity<remCalculoPlanillaConceptoAfecta>(entity =>
            {
                entity.HasKey(e => new { e.iCalculoPlanilla, e.sConcepto })
                    .HasName("PK_remCalculoPlanillaConceptoHist");
            });

            modelBuilder.Entity<remCalculoPlanillaConceptoCc>(entity =>
            {
                entity.HasKey(e => new { e.iCalculoPlanilla, e.sConcepto, e.sCentroCosto, e.bTurno });

                entity.Property(e => e.bTurno)
                    .HasDefaultValueSql("((1))")
                    .HasComment("1: Mañana, 2: Tarde, 3: Noche");

                entity.Property(e => e.mMonto).HasColumnType("money");

            });

            modelBuilder.Entity<remCalculoPlanillaConceptoCta>(entity =>
            {
                entity.HasKey(e => new { e.iCalculoPlanilla, e.sConcepto });
            });

            modelBuilder.Entity<remCalculoPlanillaConceptoFormula>(entity =>
            {
                entity.HasKey(e => new { e.iCalculoPlanilla, e.sConcepto });

                entity.Property(e => e.xFormulaOriginal)
                    .IsRequired()
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.xFormulaResuelta)
                    .IsRequired()
                    .HasMaxLength(1000)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<RemTvCalculo>(ent => ent.HasNoKey());
            modelBuilder.Entity<RemCboAno>(ent => ent.HasNoKey());
            modelBuilder.Entity<RemCboMes>(ent => ent.HasNoKey());
            modelBuilder.Entity<RemCboNumero>(ent => ent.HasNoKey());

        }
    }

    public partial class remCalculoPlanilla
    {
        public int iCalculoPlanilla { get; set; }
        public int iPeriodo { get; set; }
        public int iContrato { get; set; }
        public DateTime dCreacion { get; set; }
        public int iUsuarioReg { get; set; }

    }
    public partial class remCalculoPlanillaConcepto
    {
        public int iCalculoPlanilla { get; set; }
        public short sConcepto { get; set; }
        public decimal mMonto { get; set; }
    }
    public partial class remCalculoPlanillaConceptoAfecta
    {
        public int iCalculoPlanilla { get; set; }
        public short sConcepto { get; set; }
    }
    public partial class remCalculoPlanillaConceptoCc
    {
        public int iCalculoPlanilla { get; set; }
        public short sConcepto { get; set; }
        public short sCentroCosto { get; set; }
        public byte bTurno { get; set; }
        public decimal mMonto { get; set; }
    }
    public partial class remCalculoPlanillaConceptoCta
    {
        public int iCalculoPlanilla { get; set; }
        public short sConcepto { get; set; }
        public short? sCtaDebe { get; set; }
        public short? sCtaHaber { get; set; }
    }
    public partial class remCalculoPlanillaConceptoFormula
    {
        public int iCalculoPlanilla { get; set; }
        public short sConcepto { get; set; }
        public string xFormulaOriginal { get; set; }
        public string xFormulaResuelta { get; set; }
    }

    public class RemTvCalculo
    {
        public byte bCalculo { get; set; }
        public byte bTipoCalculo { get; set; }
        public string xTipoCalculo { get; set; }
        public string xCalculo { get; set; }
        public string xNombre { get; set; }
        public byte bDeclarar { get; set; }
    }

    public class RemCboAno
    {
        public short sAno { get; set; }
    }

    public class RemCboMes
    {
        public byte bMes { get; set; }
        public string xMes { get; set; }
    }

    public class RemCboNumero
    {
        public byte bNumero { get; set; }
        public string xNumero { get; set; }
    }



#region paramatrizadores
    public class ParTvCalculo
    {
        public ParTvCalculo(byte? bDeclarar, byte? bTodas)
        {
            this.bDeclarar = bDeclarar ?? 9;
            this.bTodas = bTodas ?? 0;
        }
        public byte bDeclarar { get; set; }
        public byte bTodas { get; set; }
    }
    public class ParCboAno
    {
        public ParCboAno(byte? bCalculo, byte? bPlanilla, byte? bEstado)
        {
            this.bCalculo = bCalculo ?? 0;
            this.bPlanilla = bPlanilla ?? 0;
            this.bEstado = bEstado ?? 9;
        }
        public short sEmpresa { get; set; }
        public byte bCalculo { get; set; }
        public byte bPlanilla { get; set; }
        public byte bEstado { get; set; }
    }

    public class ParCboMes
    {
        public ParCboMes(byte? bCalculo, byte? bPlanilla, byte? bEstado, short? sAno)
        {
            this.bCalculo = bCalculo ?? 0;
            this.bPlanilla = bPlanilla ?? 0;
            this.bEstado = bEstado ?? 9;
            this.sAno = sAno ?? (short)2020;
        }
        public short sEmpresa { get; set; }
        public byte bCalculo { get; set; }
        public byte bPlanilla { get; set; }
        public byte bEstado { get; set; }
        public short sAno { get; set; }
    }

    public class ParCboNumero
    {
        public ParCboNumero(byte? bCalculo, byte? bPlanilla, byte? bEstado, short? sAno, byte? bMes)
        {
            this.bCalculo = bCalculo ?? 0;
            this.bPlanilla = bPlanilla ?? 0;
            this.bEstado = bEstado ?? 9;
            this.sAno = sAno ?? (short)DateTime.Now.Year;
            this.bMes = bMes ?? (byte)DateTime.Now.Month;
        }
        public short sEmpresa { get; set; }
        public byte bCalculo { get; set; }
        public byte bPlanilla { get; set; }
        public byte bEstado { get; set; }
        public short sAno { get; set; }
        public byte bMes { get; set; }
    }

    #endregion
}
