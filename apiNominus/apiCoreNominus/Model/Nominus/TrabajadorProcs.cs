﻿using apiCoreNominus.Controllers.Asistencia;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace apiCoreNominus.Model
{
    public partial class Procs
    {
        public static List<traEstadoContrato> estadoContratos(NominusContext dbN, short sEmpresa, int? iPersona = null)
        {
            DateTime hoy = DateTime.Now.Date;
            return dbN.traContratoPeriodo.ToList()
                    .GroupJoin(dbN.traContratoPeriodoCese, p => p.iContratoPeriodo, ps => ps.iContratoPeriodo, (p, ps) => new { p, cese = ps.Any() ? ps.First() : null })
                    .Join(dbN.traContrato.Where(f => (iPersona == null || f.iTrabajador == (int)iPersona) && f.sEmpresa==sEmpresa), p => p.p.iContrato, pc => pc.iContrato, (p, pc) => new { p, pc })
                    .Join(dbN.orgCargo, p => p.pc.sCargo, pg => pg.sCargo, (p, pg) => new { p, pg })
                    .Join(dbN.orgSede, p => p.p.pc.sSede, pd => pd.sSede, (p, pd) => new { p, pd })
                    .Join(dbN.traContratoLaboral, p => p.p.p.p.p.iContrato, pl => pl.iContrato, (p, pl) => new { p, pl })
                    .GroupJoin(dbN.traJefeDirecto, p => p.p.p.p.p.p.iContrato, pj => pj.iContrato, (p, pj) => new { p, jefe = pj.Any() ? pj.First() : null })
                    .Select(s => new traEstadoContrato
                    {
                        iTrabajador = s.p.p.p.p.pc.iTrabajador,
                        iContrato = s.p.p.p.p.pc.iContrato,
                        iContratoPeriodo = s.p.p.p.p.p.p.iContratoPeriodo,
                        sEmpresa = s.p.p.p.p.pc.sEmpresa,
                        sSede = s.p.p.p.p.pc.sSede,
                        xSede = s.p.p.pd.xSede,
                        sCargo = s.p.p.p.p.pc.sCargo,
                        xCargo = s.p.p.p.pg.xCargo,
                        bPlanilla = s.p.pl.bPlanilla,
                        dDesde = s.p.p.p.p.p.p.dDesde,
                        dHasta = s.p.p.p.p.p.p.dHasta,
                        bFormato = s.p.p.p.p.p.p.bFormato,
                        dCese = s.p.p.p.p.p.cese != null ? (DateTime?)s.p.p.p.p.p.cese.dCese : null,
                        bMotivo = s.p.p.p.p.p.cese != null ? (byte?)s.p.p.p.p.p.cese.bMotivo : null,
                        iJefe = s.jefe != null ? (int?)s.jefe.iTrabajador : null,
                        bEstadoTrabajador = (byte)(s.p.p.p.p.p.cese != null ? 2 : (s.p.p.p.p.p.p.dHasta == null || ((DateTime)s.p.p.p.p.p.p.dHasta).Date >= hoy) ? 1 : 3)
                    }).ToList();
        }

        //        public static List<PerTrabajador> lstTrabajadores(NominusContext dbN)
        //        {
        //            List<traEstadoContrato> contratos = estadoContratos(dbN);

        //            return  dbN.perPersona.ToList()
        //                    .Join(dbN.traTrabajador, p => p.iPersona, t => t.iTrabajador, (p, t) => new { p, t })
        //                    .Join(dbN.isoTipoDocIdentidad, p => p.p.bTipoDocIdentidad, d => d.bTipoDocIdentidad, (p, d) => new { p, d })
        //                    .GroupJoin(contratos, p => p.p.p.iPersona, c => c.iTrabajador, (p, pc) => new { p, contratos = pc.Any() ? pc.ToList() : null })
        //                    .Select(s => new PerTrabajador
        //                    {
        //                        iPersona = s.p.p.p.iPersona,
        //                        bTipoDocIdentidad = s.p.p.p.bTipoDocIdentidad,
        //                        xDocIdentidad = s.p.p.p.xDocIdentidad,
        //                        xApellidoPaterno = s.p.p.p.xApellidoPaterno,
        //                        xApellidoMaterno = s.p.p.p.xApellidoMaterno,
        //                        xNombres = s.p.p.p.xNombres,
        //                        dNacimiento = s.p.p.p.dNacimiento,
        //                        bGenero = s.p.p.p.bGenero,
        //                        xConcatenado = s.p.p.p.xConcatenado,
        //                        iUsuarioReg = s.p.p.p.iUsuarioReg,
        //                        dFechaReg = s.p.p.p.dFechaReg,
        //                        xTipoDocIdentidad = s.p.d.xTipoDocIdentidad,
        ////                        contrato = s.contratos,
        //                        bRegimenPensionario = s.p.p.t.bRegimenPensionario,
        //                        bDiscapacidad = s.p.p.t.bDiscapacidad,
        //                        bConvenioEvitarDobleTributacion = s.p.p.t.bConvenioEvitarDobleTributacion,
        //                        bEstadoCivil = s.p.p.t.bEstadoCivil
        //                    }).ToList();

        //        }

        public static List<TraTrabajador> lstTrabajadoresVigentes(NominusContext dbN, short sEmpresa, int? iJefe = null)
        {
            return GetTrabajadores(dbN).Where(t => t.contratos != null && t.contratos.Where(c=>c.vigente!=null && c.sEmpresa==sEmpresa).Any()).ToList();
        }

        //public static List<PerColaborador> lstTrabajadoresxJefe(NominusContext dbN,parJefeEmpresa par)
        //{
        //    return GetTrabajadores(dbN)
        //        .Where(t => t.contratos != null
        //        && t.contratos.Where(c => c.vigente != null
        //        && c.vigente.cese == null
        //        && ((DateTime)c.vigente.dHasta).Date <= DateTime.Now.Date).Any())
        //        .Select(s=>new PerColaborador { 
                  
                   
                
        //        }).ToList();
        //}

        public static List<TraTrabajador> lstTrabajadoresPorRenovar(NominusContext dbN)
        {
            return GetTrabajadores(dbN)
                .Where(t => t.contratos != null
                        && t.contratos.Where(c => c.vigente != null
                                                && c.vigente.cese == null
                                                && ((DateTime)c.vigente.dHasta).Date <= DateTime.Now.Date).Any()).ToList();
        }

        public static List<TraContratoPeriodo> GetPeriodos(NominusContext dbN)
        {
            return dbN.traContratoPeriodo.AsEnumerable()
                .GroupJoin(dbN.traContratoPeriodoCese.AsEnumerable(), p => p.iContratoPeriodo, pc => pc.iContratoPeriodo, (p, pc) => new { p, cese = pc.Any() ? pc.First() : null })
//                .Join(dbN.traContratoFormato.AsEnumerable(), p => p.p.bFormato, pf => pf.bFormato, (p, pf) => new { p, pf })
                .Join(dbN.traContratoFormato.AsEnumerable(), 
                        p => p.p.bFormato, pf => pf.bFormato, 
                        (p, f) => new { p, pf = new TraContratoFormato { bFormato = f.bFormato, bTipoContrato = f.bTipoContrato, xFormato = f.xFormato } })
                .Join(dbN.isoTipoContrato.AsEnumerable(), p => p.pf.bTipoContrato, pft => pft.bTipoContrato, (p, pft) => new { p, pft })
                .Select(s => new TraContratoPeriodo
                {
                    iContratoPeriodo = s.p.p.p.iContratoPeriodo,
                    iContrato = s.p.p.p.iContrato,
                    dDesde = s.p.p.p.dDesde,
                    dHasta = s.p.p.p.dHasta,
                    bFormato = s.p.p.p.bFormato,
                    cese = s.p.p.cese,
                    formato = s.p.pf,
                    xTipoContrato = s.pft.xTipoContrato,
                }).ToList();
        }
        
        public static List<TraContrato> GetContratos(NominusContext dbN)
        {
            return dbN.traContratoLaboral.AsEnumerable()
                .Join(dbN.traContrato, c => c.iContrato, ct => ct.iContrato, (c, ct) => new { c, ct })
                .Join(dbN.orgEmpresa, c => c.ct.sEmpresa, ce => ce.sEmpresa, (c, ce) => new { c, ce })
                .Join(dbN.orgSede, c => c.c.ct.sSede, cs => cs.sSede, (c, cs) => new { c, cs })
                .Join(dbN.orgCargo, c => c.c.c.ct.sCargo, cc => cc.sCargo, (c, cc) => new { c, cc })
                .Join(dbN.isoTipoTrabajador, c => c.c.c.c.c.bTipoTrabajador, tt => tt.bTipoTrabajador, (c, tt) => new { c, tt })
                .GroupJoin(GetJefeDirecto(dbN), c => c.c.c.c.c.ct.iContrato, cj => cj.iContrato, (c, cjj) => new { c, cj = cjj.Any() ? cjj.First() : null })
                .GroupJoin(GetPeriodos(dbN), c => c.c.c.c.c.c.ct.iContrato, cp => cp.iContrato, (c, cp) => new { c, periodos = cp.Any() ? cp : null })
                .Select(s => new TraContrato
                {
                    iContrato = s.c.c.c.c.c.c.c.iContrato,
                    bPlanilla = s.c.c.c.c.c.c.c.bPlanilla,
                    bTipoTrabajador = s.c.c.c.c.c.c.c.bTipoTrabajador,
                    bRegAtipicoDeJornadaTrab = s.c.c.c.c.c.c.c.bRegAtipicoDeJornadaTrab,
                    bJornadaTrabMax = s.c.c.c.c.c.c.c.bJornadaTrabMax,
                    bHorarioNoc = s.c.c.c.c.c.c.c.bHorarioNoc,
                    bSindicalizado = s.c.c.c.c.c.c.c.bSindicalizado,
                    bRentas5taExoneradas = s.c.c.c.c.c.c.c.bRentas5taExoneradas,
                    bSituacion = s.c.c.c.c.c.c.c.bSituacion,
                    bSituacionEspecial = s.c.c.c.c.c.c.c.bSituacionEspecial,
                    bCategoriaOcupacional = s.c.c.c.c.c.c.c.bCategoriaOcupacional,
                    bCategoria = s.c.c.c.c.c.c.c.bCategoria,
                    bTipoPago = s.c.c.c.c.c.c.c.bTipoPago,
                    bPeriodicidadRem = s.c.c.c.c.c.c.c.bPeriodicidadRem,
                    iTrabajador = s.c.c.c.c.c.c.ct.iTrabajador,
                    sEmpresa = s.c.c.c.c.c.c.ct.sEmpresa,
                    sSede = s.c.c.c.c.c.c.ct.sSede,
                    sCargo = s.c.c.c.c.c.c.ct.sCargo,
                    iJefe = s.c.cj != null ? s.c.cj.iTrabajador : (int?)null,
                    xJefe = s.c.cj != null ? s.c.cj.xJefe : null,
                    xCargo = s.c.c.c.cc.xCargo,
                    xEmpresa = s.c.c.c.c.c.ce.xNombreComercial,
                    xEmpresaAbreviado = s.c.c.c.c.c.ce.xAbreviado,
                    xSede = s.c.c.c.c.cs.xSede,
                    xTipoTrabajador = s.c.c.tt.xTipoTrabajador,
                    periodos = s.periodos,
                    vigente = s.periodos.Any() ? s.periodos.OrderBy(p => p.dDesde).Last() : null
                }).ToList();
        }

        public static dynamic GetContratosTrabajador(NominusContext dbN,int iTrabajador)
        {
            return dbN.traContratoLaboral.AsEnumerable()
                .Join(dbN.traContrato.Where(tr=>tr.iTrabajador==iTrabajador), c => c.iContrato, ct => ct.iContrato, (c, ct) => new { c, ct })
                .Join(dbN.orgEmpresa, c => c.ct.sEmpresa, ce => ce.sEmpresa, (c, ce) => new { c, ce })
                .Join(dbN.orgSede, c => c.c.ct.sSede, cs => cs.sSede, (c, cs) => new { c, cs })
                .Join(dbN.orgCargo, c => c.c.c.ct.sCargo, cc => cc.sCargo, (c, cc) => new { c, cc })
                .Join(dbN.isoTipoTrabajador, c => c.c.c.c.c.bTipoTrabajador, tt => tt.bTipoTrabajador, (c, tt) => new { c, tt })
                .GroupJoin(GetJefeDirecto(dbN), c => c.c.c.c.c.ct.iContrato, cj => cj.iContrato, (c, cjj) => new { c, cj = cjj.Any() ? cjj.First() : null })
                .GroupJoin(GetPeriodos(dbN), c => c.c.c.c.c.c.ct.iContrato, cp => cp.iContrato, (c, cp) => new { c, periodos = cp.Any() ? cp : null })
                .Select(s => new TraContrato
                {
                    iContrato = s.c.c.c.c.c.c.c.iContrato,
                    bPlanilla = s.c.c.c.c.c.c.c.bPlanilla,
                    bTipoTrabajador = s.c.c.c.c.c.c.c.bTipoTrabajador,
                    bRegAtipicoDeJornadaTrab = s.c.c.c.c.c.c.c.bRegAtipicoDeJornadaTrab,
                    bJornadaTrabMax = s.c.c.c.c.c.c.c.bJornadaTrabMax,
                    bHorarioNoc = s.c.c.c.c.c.c.c.bHorarioNoc,
                    bSindicalizado = s.c.c.c.c.c.c.c.bSindicalizado,
                    bRentas5taExoneradas = s.c.c.c.c.c.c.c.bRentas5taExoneradas,
                    bSituacion = s.c.c.c.c.c.c.c.bSituacion,
                    bSituacionEspecial = s.c.c.c.c.c.c.c.bSituacionEspecial,
                    bCategoriaOcupacional = s.c.c.c.c.c.c.c.bCategoriaOcupacional,
                    bCategoria = s.c.c.c.c.c.c.c.bCategoria,
                    bTipoPago = s.c.c.c.c.c.c.c.bTipoPago,
                    bPeriodicidadRem = s.c.c.c.c.c.c.c.bPeriodicidadRem,
                    iTrabajador = s.c.c.c.c.c.c.ct.iTrabajador,
                    sEmpresa = s.c.c.c.c.c.c.ct.sEmpresa,
                    sSede = s.c.c.c.c.c.c.ct.sSede,
                    sCargo = s.c.c.c.c.c.c.ct.sSede,
                    iJefe = s.c.cj != null ? s.c.cj.iTrabajador : (int?)null,
                    xJefe = s.c.cj != null ? s.c.cj.xJefe : null,
                    xCargo = s.c.c.c.cc.xCargo,
                    xEmpresa = s.c.c.c.c.c.ce.xNombreComercial,
                    xEmpresaAbreviado = s.c.c.c.c.c.ce.xAbreviado,
                    xSede = s.c.c.c.c.cs.xSede,
                    xTipoTrabajador = s.c.c.tt.xTipoTrabajador,
                    periodos = s.periodos,
                    vigente = s.periodos.Any() ? s.periodos.OrderBy(p => p.dDesde).Last() : null
                })
                .ToList();
        }

        public static List<TraTrabajador> GetTrabajadores(NominusContext dbN)
        {
            return dbN.traTrabajador.AsEnumerable()
                .GroupJoin(dbN.perPersona.AsEnumerable(), t => t.iTrabajador, p => p.iPersona, (t, p) => new { t, persona = p.First() })
                .Join(dbN.isoRegimenPensionario, t => t.t.bRegimenPensionario, r => r.bRegimenPensionario, (t, r) => new { t, r })
                .Join(dbN.isoTipoDocIdentidad, t => t.t.persona.bTipoDocIdentidad, pd => pd.bTipoDocIdentidad, (t, td) => new { t, td })
                .GroupJoin(dbN.autUsuario, t => t.t.t.t.iTrabajador, ta => ta.iUsuario, (t, ta) => new { t, auth = ta.Any() ? ta.First() : null })
                .GroupJoin(GetContratos(dbN), t => t.t.t.t.t.iTrabajador, tt => tt.iTrabajador, (t, tt) => new { t, contratos = tt.Any() ? tt : null })
                .Select(s => new TraTrabajador
                {
                    iTrabajador = s.t.t.t.t.t.iTrabajador,
                    bRegimenPensionario = s.t.t.t.t.t.bRegimenPensionario,
                    bDiscapacidad = s.t.t.t.t.t.bDiscapacidad,
                    bConvenioEvitarDobleTributacion = s.t.t.t.t.t.bConvenioEvitarDobleTributacion,
                    bEstadoCivil = s.t.t.t.t.t.bEstadoCivil,
                    persona = s.t.t.t.t.persona,
                    auth = s.t.auth,
                    xRegimenPensionario = s.t.t.t.r.xRegimenPensionario,
                    xTipoDocIdentidad = s.t.t.td.xTip,
                    contratos = s.contratos,
                    xGenero = s.t.t.t.t.persona != null ? s.t.t.t.t.persona.bGenero == 0 ? "Femenino" : "Masculino" : null,
                    xEstadoCivil = (new string[] { "", "Solter", "Casad", "Divorciad", "Viud", "Conviviente" })[s.t.t.t.t.t.bEstadoCivil]
                    + (s.t.t.t.t.t.bEstadoCivil % 5 == 0 ? "" : s.t.t.t.t.persona != null && s.t.t.t.t.persona.bGenero == 0 ? "a" : "o")
                }).ToList();
        }

        public static List<PerColaborador> GetColaboradores(NominusContext dbN,short sEmpresa, int? iJefe=null)
        {
            return GetTrabajadores(dbN).AsEnumerable()
                        .Where(tr => tr.contratos != null
                                            && tr.contratos.Where(c => c.vigente != null
                                                && c.vigente.cese == null
                                                && c.sEmpresa == sEmpresa
                                                && (c.iJefe == iJefe || iJefe == null)).Any())
                        .GroupJoin(dbN.horProgramacion.AsEnumerable()
                            .GroupJoin(dbN.horDetProgramacion, kp => kp.iProgramacion, d => d.iProgramacion, (p, s) => new HorProgramacion
                            {
                                iProgramacion = p.iProgramacion,
                                iContrato = p.iContrato,
                                sRotacion = p.sRotacion,
                                dDesde = p.dDesde,
                                dHasta = p.dHasta,
                                bPrioridad = p.bPrioridad,
                                fHorasInicial = p.fHorasInicial,
                                fHorasRedimidas = p.fHorasRedimidas,
                                fHorasTrabajadas = p.fHorasTrabajadas,
                                detalle = s.Any() ? s.AsEnumerable() : null
                            })
                        , t => t.contratos.Last().iContrato, p => p.iContrato, (t, p) => new { t, programacion = p.Any() ? p.First() : new HorProgramacion { iProgramacion = 0, iContrato = t.contratos.Last().iContrato } })
                        .Select(s => new PerColaborador
                        {
                            iContrato = s.t.contratos.Last().iContrato,
                            iContratoPeriodo = s.t.contratos.Last().vigente.iContratoPeriodo,
                            iTrabajador = s.t.iTrabajador,
                            sSede = s.t.contratos.Last().sSede,
                            sCargo = s.t.contratos.Last().sCargo,
                            cTipo = s.t.contratos.Select(sv => new { cTipo = sv.vigente.dHasta != null ? "A" : sv.vigente.dHasta >= DateTime.Now.Date ? "F" : "V" }).Last().cTipo,
                            dDesde = s.t.contratos.First().vigente.dDesde,
                            dHasta = s.t.contratos.Last().vigente.dHasta,
                            sEmpresa = s.t.contratos.Last().sEmpresa,
                            bFormato = s.t.contratos.Last().vigente.bFormato,
                            xColaborador = s.t.persona.xApellidoPaterno + " " + (s.t.persona.xApellidoMaterno ?? "") + " " + (s.t.persona.xNombres ?? ""),
                            xSede = s.t.contratos.Last().xSede,
                            xCargo = s.t.contratos.Last().xCargo,
                            iJefe = s.t.contratos.Last().iJefe,
                            programacion = s.programacion
                        }).OrderBy(o => o.xColaborador).ToList();


        }

        public static TraTrabajador GetTrabajador(NominusContext dbN,int iTrabajador)
        {
            return dbN.traTrabajador.Where(f=>f.iTrabajador==iTrabajador).AsEnumerable()
                .GroupJoin(dbN.perPersona.AsEnumerable(), t => t.iTrabajador, p => p.iPersona, (t, p) => new { t, persona = p.First() })
                .Join(dbN.isoRegimenPensionario, t => t.t.bRegimenPensionario, r => r.bRegimenPensionario, (t, r) => new { t, r })
                .Join(dbN.isoTipoDocIdentidad, t => t.t.persona.bTipoDocIdentidad, pd => pd.bTipoDocIdentidad, (t, td) => new { t, td })
                .GroupJoin(dbN.autUsuario, t => t.t.t.t.iTrabajador, ta => ta.iUsuario, (t, ta) => new { t, auth = ta.Any() ? ta.First() : null })
                .GroupJoin(GetContratos(dbN), t => t.t.t.t.t.iTrabajador, tt => tt.iTrabajador, (t, tt) => new { t, contratos = tt.Any() ? tt : null })
                .Select(s => new TraTrabajador
                {
                    iTrabajador = s.t.t.t.t.t.iTrabajador,
                    bRegimenPensionario = s.t.t.t.t.t.bRegimenPensionario,
                    bDiscapacidad = s.t.t.t.t.t.bDiscapacidad,
                    bConvenioEvitarDobleTributacion = s.t.t.t.t.t.bConvenioEvitarDobleTributacion,
                    bEstadoCivil = s.t.t.t.t.t.bEstadoCivil,
                    persona = s.t.t.t.t.persona,
                    auth = s.t.auth,
                    xRegimenPensionario = s.t.t.t.r.xRegimenPensionario,
                    xTipoDocIdentidad = s.t.t.td.xTip,
                    contratos = s.contratos,
                    xGenero = s.t.t.t.t.persona != null ? s.t.t.t.t.persona.bGenero == 0 ? "Femenino" : "Masculino" : null,
                    xEstadoCivil = (new string[] { "", "Solter", "Casad", "Divorciad", "Viud", "Conviviente" })[s.t.t.t.t.t.bEstadoCivil]
                    + (s.t.t.t.t.t.bEstadoCivil % 5 == 0 ? "" : s.t.t.t.t.persona != null && s.t.t.t.t.persona.bGenero == 0 ? "a" : "o")
                }).FirstOrDefault();
        }

        public static List<TraJefeDirecto> GetJefeDirecto(NominusContext dbN)
        {
            return dbN.traJefeDirecto.AsEnumerable()
                .Join(dbN.perPersona, p => p.iTrabajador, pp => pp.iPersona, (p, pp) => new { p, pp })
                .Select(s => new TraJefeDirecto
                {
                    iContrato = s.p.iContrato,
                    iJefeDirecto = s.p.iJefeDirecto,
                    iTrabajador = s.p.iTrabajador,
                    xJefe = (s.pp.xApellidoPaterno + " " + (s.pp.xApellidoMaterno ?? "") + " " + s.pp.xNombres).Replace("  "," ")
                }).ToList();
        }

    }
}