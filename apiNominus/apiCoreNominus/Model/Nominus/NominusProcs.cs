﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using apiCoreNominus.Model;
using apiCoreNominus.Controllers;
using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace apiCoreNominus.Model
{
    public partial class Procs
    {
        public static List<NomContrato> Contratos(int iPersona, short sServidor)
        {
            List<NomContrato> resp;
            try
            {
                using (NominusContext db = new NominusContext(sServidor))
                {
                    resp = db.NomContrato.FromSqlRaw("exec autContratos @iPersona", new SqlParameter("@iPersona", iPersona)).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resp;
        }

        public static List<NomSincro> SincronizarAuth(short sServidor)
        {
            List<NomSincro> resp = new List<NomSincro>();
            using (NominusContext db = new NominusContext(sServidor))
            {
                using (AuthContext dbA = new AuthContext())
                {
                    List<autUsuario> _usu = db.autUsuario.ToList();
                    foreach (autUsuario _us in _usu)
                    {
                        authUsuario _aus = dbA.authUsuario.FirstOrDefault(c => c.email == _us.xEmail);
                        if (_aus == null)
                        {
                            _aus = new authUsuario
                            {
                                gUsuario = Guid.NewGuid(),
                                email = _us.xEmail,
                            };
                            dbA.authUsuario.Add(_aus);
                            dbA.SaveChanges();
                        }
                        authAcceso _aac = dbA.authAcceso.FirstOrDefault(c => c.gUsuario == _aus.gUsuario && c.sServidor == sServidor && c.iPersona == _us.iUsuario);
                        if (_aac == null)
                        {
                            _aac = new authAcceso
                            {
                                gAcceso = Guid.NewGuid(),
                                gUsuario = _aus.gUsuario,
                                iPersona = _us.iUsuario,
                                sServidor = sServidor
                            };
                            dbA.authAcceso.Add(_aac);
                            dbA.SaveChanges();
                            resp.Add(new NomSincro
                            {
                                email = _us.xEmail,
                                iPersona = _us.iUsuario,
                                cSincro = "autorizado"
                            });
                        }

                        //TODO:   aqui voy terminar Join
                    }
                }
            }
            return resp;
        }

        public static List<belCalculos> belCalculosPersona(int iPersona, short sServidor)
        {
            List<belCalculos> resp;
            try
            {
                using (NominusContext db = new NominusContext(sServidor))
                {
                    SqlParameter par_gToken = new SqlParameter("@iPersona", iPersona);
                    resp = db._belCalculos.FromSqlRaw("exec belCalculosPersona @iPersona", par_gToken).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resp;
        }

        public static List<belCalculos> belCalculos(Guid gSesion, short sServidor)
        {
            List<belCalculos> resp = new List<belCalculos>();
            using (NominusContext db = new NominusContext(sServidor))
            {
                resp = db._belCalculos.FromSqlRaw("exec belCalculos @gSesion", new SqlParameter("@gSesion", gSesion)).ToList();
            }
            return resp;
        }
        public static List<webNotificar> webNotificar(int iUsuario, short sServidor)
        {
            List<webNotificar> resp = new List<webNotificar>();
            using (NominusContext db = new NominusContext(sServidor))
            {
                resp = db._webNotificar.FromSqlRaw("exec webNotificar @iUsuario", new SqlParameter("@iUsuario", iUsuario)).ToList();
            }
            return resp;
        }
        public static List<autAvatar> autAvatar(int iPersona, short sServidor)
        {
            List<autAvatar> resp = new List<autAvatar>();
            using (NominusContext db = new NominusContext(sServidor))
            {
                resp = db._autAvatar.FromSqlRaw("exec autAvatar @iPersona", new SqlParameter("@iPersona", iPersona)).ToList();
            }
            return resp;
        }

        public static IQueryable<AutContratos> autContratos(int iPersona, short sServidor)
        {
            IQueryable<AutContratos> resp;
            try
            {
                using (NominusContext db = new NominusContext(sServidor))
                {
                    resp = db._autContratos.FromSqlRaw("exec autContratos @iPersona", new SqlParameter("@iPersona", iPersona));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resp;
        }
        public static List<remBoletaCabecera> remBoletaCabecera(int iCalculoPlanilla, byte bMes, byte bNumero, short sServidor)
        {
            List<remBoletaCabecera> resp;
            try
            {
                using (NominusContext db = new NominusContext(sServidor))
                {
                    resp = db._remBoletaCabecera.FromSqlRaw("exec remBoletaCabecera @iCalculoPlanilla,@bMes,@bNumero", new SqlParameter("iCalculoPlanilla", iCalculoPlanilla), new SqlParameter("bMes", bMes), new SqlParameter("bNumero", bNumero)).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resp;
        }
        public static List<remBoletaCabeceraAsistenciaFechas> remBoletaCabeceraAsistenciaFechas(byte bMes, byte bNumero, int iCalculoPlanilla, short sServidor)
        {
            List<remBoletaCabeceraAsistenciaFechas> resp;
            try
            {
                using (NominusContext db = new NominusContext(sServidor))
                {
                    resp = db._remBoletaCabeceraAsistenciaFechas.FromSqlRaw("exec remBoletaCabeceraAsistenciaFechas @p3,@p1,@p2", new SqlParameter("p1", bMes), new SqlParameter("p2", bNumero), new SqlParameter("p3", iCalculoPlanilla)).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resp;
        }
        public static List<remBoletaCabeceraDatos> remBoletaCabeceraDatos(byte bColumna, int iCalculoPlanilla, short sServidor)
        {
            List<remBoletaCabeceraDatos> resp;
            try
            {
                using (NominusContext db = new NominusContext(sServidor))
                {
                    string xSQL = $"exec remBoletaCabeceraDatos {iCalculoPlanilla}, {bColumna}";
                    resp = db._remBoletaCabeceraDatos.FromSqlRaw(xSQL).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resp;
        }
        public static List<remBoletaConcepto> remBoletaConcepto(int iCalculoPlanilla, byte bMes, byte bNumero, byte bClaseConcepto, short sServidor)
        {
            List<remBoletaConcepto> resp;
            try
            {
                using (NominusContext db = new NominusContext(sServidor))
                {
                    resp = db._remBoletaConcepto.FromSqlRaw("exec remBoletaConcepto @p1,@p2,@p3,@p4", new SqlParameter("p1", iCalculoPlanilla), new SqlParameter("p2", bMes), new SqlParameter("p3", bNumero), new SqlParameter("p4", bClaseConcepto)).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resp;
        }
        public static List<traContratosxPersona> traContratosxPersona(int iPersona, short sServidor)
        {
            List<traContratosxPersona> resp;
            try
            {
                using (NominusContext db = new NominusContext(sServidor))
                {
                    resp = db._traContratosxPersona.FromSqlRaw("exec traContratosxPersona @p1", new SqlParameter("p1", iPersona)).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resp;
        }

        public static List<webPagosPrestamo> webPagosPrestamo(int iPrestamo, short sServidor)
        {
            List<webPagosPrestamo> resp;
            try
            {
                using (NominusContext db = new NominusContext(sServidor))
                {
                    resp = db._webPagosPrestamo.FromSqlRaw("exec webPagosPrestamo @p1", new SqlParameter("p1", iPrestamo)).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resp;
        }

        public static List<webVacaciones> webVacaciones(int iContrato, short sServidor)
        {
            List<webVacaciones> resp;
            try
            {
                using (NominusContext db = new NominusContext(sServidor))
                {
                    resp = db._webVacaciones.FromSqlRaw("exec webVacaciones @p1", new SqlParameter("p1", iContrato)).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resp;
        }

        public static List<belCalculosxPeriodo> belCalculosxPeriodo(Guid gToken, short sServidor)
        {
            List<belCalculosxPeriodo> resp;
            try
            {
                using (NominusContext db = new NominusContext(sServidor))
                {
                    SqlParameter par_gToken = new SqlParameter("gToken", gToken);
                    resp = db._belCalculosxPeriodo.FromSqlRaw("exec belCalculosxPeriodo @gToken", par_gToken).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resp;
        }

        public static List<remBoletaCabeceraEmpresa> remBoletaCabeceraEmpresa(short sEmpresa, short sServidor)
        {

            List<remBoletaCabeceraEmpresa> resp;
            try
            {
                using (NominusContext db = new NominusContext(sServidor))
                {
                    resp = db._remBoletaCabeceraEmpresa.FromSqlRaw("exec remBoletaCabeceraEmpresa @sEmpresa", new SqlParameter("@sEmpresa", sEmpresa)).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resp;
        }

        public static List<remBoletaCabeceraTitulo> remBoletaCabeceraTitulo(int iCalculoPlanilla, byte bMes, byte bNumero, short sServidor)
        {
            List<remBoletaCabeceraTitulo> resp;
            try
            {
                using (NominusContext db = new NominusContext(sServidor))
                {
                    resp = db._remBoletaCabeceraTitulo.FromSqlRaw("exec remBoletaCabeceraTitulo @iCalculoPlanilla,@bMes,@bNumero", new SqlParameter("@iCalculoPlanilla", iCalculoPlanilla), new SqlParameter("@bMes", bMes), new SqlParameter("@bNumero", bNumero)).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resp;
        }

    }

    public class NomContrato
    {
        public DateTime? dDesde { get; set; }
        public DateTime? dHasta { get; set; }
        [Key]
        public int iContrato { get; set; }
        public string jImagen { get; set; }
        public short sCargo { get; set; }
        public short sEmpresa { get; set; }
        public string xAbreviado { get; set; }
        public string xCargo { get; set; }
        public string xRazonSocial { get; set; }
    }

    public class NomSincro
    {
        [Key]
        public string email { get; set; }
        public int iPersona { get; set; }
        public string cSincro { get; set; }

    }

    public class _acceso : authAcceso
    {
        public string email { get; set; }
    }


    public class scalarString
    {
        public string xString { get; set; }
    }
    public class scalarNumero
    {
        public int iNumero { get; set; }
    }

    public partial class belCalculosxPeriodo
    {
        public int iNotificacion { get; set; }
        public byte bTipoNotificacion { get; set; }
        public string xTipoNotificacion { get; set; }
        public Nullable<bool> lPush { get; set; }
        public int iCalculoPlanilla { get; set; }
        public int iAnoMes { get; set; }
        public byte bCalculo { get; set; }
        public string xNotificacion { get; set; }
        public System.DateTime dNotificacion { get; set; }
        public Nullable<System.DateTime> dLeida { get; set; }
    }
    public partial class belCalculos
    {
        public int iCalculoPlanilla { get; set; }
        public int iPeriodo { get; set; }
        public int iContrato { get; set; }
        public int iAnoMes { get; set; }
        public byte bNumero { get; set; }
        public Nullable<byte> bMes { get; set; }
        public int bBEL { get; set; }
        public string xCalculo { get; set; }
    }

    public partial class remBoletaCabecera
    {
        public short sEmpresa { get; set; }
        public string txtfirmaizq1 { get; set; }
        public string txtfirmaizq2 { get; set; }
        public string txtfirmader1 { get; set; }
        public string txtfirmader2 { get; set; }
        public string txtneto { get; set; }
        public string monedaneto { get; set; }
        public decimal? neto { get; set; }
        public int bfirma { get; set; }
        public short slogo { get; set; }
        public int bselloagua { get; set; }
        public string xingresos { get; set; }
        public string xdescuentos { get; set; }
        public string xempleador { get; set; }
        public int AnchoTitulo { get; set; }
        public short sEmpresaImagenLogo { get; set; }
        public short sEmpresaImagenFirma { get; set; }
    }
    public partial class remBoletaCabeceraAsistenciaFechas
    {
        public string Fecha01 { get; set; }
        public string Fecha02 { get; set; }
        public string Fecha03 { get; set; }
        public string Nombre01 { get; set; }
        public string Nombre02 { get; set; }
        public string Nombre03 { get; set; }
    }
    public partial class remBoletaCabeceraDatos
    {
        public byte ID { get; set; }
        public string xNombre { get; set; }
        public string xSeparador { get; set; }
        public string xDato { get; set; }
        public Nullable<int> bBold { get; set; }
    }
    public class remBoletaCabeceraEmpresa
    {
        public string xDato { get; set; }
    }
    public class remBoletaCabeceraTitulo
    {
        public string xDato { get; set; }
    }
    public partial class remBoletaConcepto
    {
        public string xClaseConcepto { get; set; }
        public string xSubClaseConcepto { get; set; }
        public string Concepto { get; set; }
        public decimal mMonto { get; set; }
        public short sOrdenMostrar { get; set; }
        public string MontoInfo { get; set; }
        public string MontoHora { get; set; }
        public byte OrdenSubClaseConcepto { get; set; }
    }
    public partial class traContratosxPersona
    {
        public int iContrato { get; set; }
        public short sEmpresa { get; set; }
        public short sCargo { get; set; }
        public string xContrato { get; set; }
    }

    public partial class webPagosPrestamo
    {
        public int iPrestamoCuota { get; set; }
        public int iPrestamo { get; set; }
        public byte bCalculo { get; set; }
        public System.DateTime dFecha { get; set; }
        public int mValor { get; set; }
        public bool bAnulado { get; set; }
        public Nullable<int> iCalculoPlanilla { get; set; }
    }
    public partial class webPrestamos
    {
        public int iPrestamo { get; set; }
        public int iContrato { get; set; }
        public short sConcepto { get; set; }
        public short sMoneda { get; set; }
        public decimal mValor { get; set; }
        public Nullable<System.DateTime> dPrestamo { get; set; }
        public Nullable<int> mPagos { get; set; }
    }
    public partial class webVacaciones
    {
        public Nullable<long> iNovedadVacaciones { get; set; }
        public Nullable<int> iContrato { get; set; }
        public Nullable<short> sCiclo { get; set; }
        public System.DateTime dDesde { get; set; }
        public Nullable<System.DateTime> dHasta { get; set; }
        public Nullable<int> iDias { get; set; }
        public string cTipoNovedad { get; set; }
        public string xDescripcion { get; set; }
    }

    public class AutContratos
    {
        public DateTime? dDesde { get; set; }
        public DateTime? dHasta { get; set; }
        public int iContrato { get; set; }
        public string jImagen { get; set; }
        public short sCargo { get; set; }
        public short sEmpresa { get; set; }
        public string xAbreviado { get; set; }
        public string xCargo { get; set; }
        public string xRazonSocial { get; set; }
    }
    public class WebPrestamos
    {
        public DateTime? dPrestamo { get; set; }
        public int iContrato { get; set; }
        public int iPrestamo { get; set; }
        public decimal? mPagos { get; set; }
        public decimal mValor { get; set; }
        public short sConcepto { get; set; }
        public short sMoneda { get; set; }
    }
    public class autAvatar
    {
        public int iPersona { get; set; }
        public string xTip { get; set; }
        public string xDocIdentidad { get; set; }
        public string xUsuario { get; set; }
        public string jFoto { get; set; }
        public string cGenero { get; set; }
    }
    public class autAvatarRegistro
    {
        public string xNombres { get; set; }
        public string xEmail { get; set; }
    }

    public class webNotificar
    {
        public int iNotificacion { get; set; }
        public Nullable<System.DateTime> dEnvio { get; set; }
        public Nullable<byte> bTipoNotificacion { get; set; }
        public Nullable<int> iReferencia { get; set; }
        public Nullable<int> iContrato { get; set; }
        public string xAsunto { get; set; }
        public string xFaIcono { get; set; }
        public string xUrl { get; set; }
        public Nullable<System.DateTime> dLeido { get; set; }
        public Nullable<System.DateTime> dFirmado { get; set; }
    }

    public class ParContrato
    {
        public int iContrato { get; set; }
        public short sServidor { get; set; }
    }
    public class parEmpresa
    {
        public short sServidor { get; set; }
        public short sEmpresa { get; set; }
    }

    public class parEvento
    {
        public short sServidor { get; set; }
        public string cTipoControl { get; set; }
    }



}
