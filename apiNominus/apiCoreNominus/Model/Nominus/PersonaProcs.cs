﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;

namespace apiCoreNominus.Model
{
    public partial class Procs
    {
        public static List<perPersona> PerBuscarPersonas(ParBuscaPersona parBusca, int iUsuario)
        {
            List<perPersona> rsp = new List<perPersona>();
            using (NominusContext db = new NominusContext(parBusca.sServidor))
            {
                List<perPersona> rqst = db.perPersona
                    .FromSqlInterpolated($"perBuscarPersonas {parBusca.xCadena},{iUsuario}")
                    .ToList();
                int rCant = 0;
                while (rCant < rqst.Count && rCant < parBusca.sTop)
                {
                    rsp.Add(rqst[rCant]);
                    rCant++;
                }
            }
            return rsp;
        }

        public static PerNavegante PerPerfil(Guid token)
        {
            PerNavegante perfil = new PerNavegante();
            using (AuthContext dbA = new AuthContext())
            {
                authSesion _ses = dbA.authSesion.FirstOrDefault(c => c.gToken == token);
                if (_ses != null && _ses.sServidor != null)
                {
                    short _serv = _ses.sServidor ?? 0;
                    using (NominusContext db = new NominusContext(_serv))
                    {
                        authAcceso _acc = dbA.authAcceso.FirstOrDefault(c => c.gUsuario == _ses.gUsuario && c.sServidor == _serv);
                        traContratoPeriodo _tcp = db.traContratoPeriodo.FirstOrDefault(c => c.iContratoPeriodo == _ses.iContrato);
                        if (_acc != null)
                        {
                            authUsuario _usu = dbA.authUsuario.FirstOrDefault(c => c.gUsuario == _acc.gUsuario);
                            List<nomPersonalPerfilCliente> _super;
                            nomPersonal _super1;
                            using (GeneralContext dg = new GeneralContext())
                            {
                                _super = dg.nomPersonalPerfilCliente.Where(c => c.bPerfil == 1).ToList();
                                _super1 = dg.nomPersonal.FirstOrDefault(c => c.xCorreo == _usu.email);
                            }
                            if (_acc != null)
                            {
                                perfil.iUsuario = _acc.iPersona;
                                perfil.sServidor = _ses.sServidor;
                                perfil.iContrato = _tcp.iContrato;
                                perfil.esAdmin = db.sysAdministrador.Any(c => c.iPersona == _acc.iPersona);
                                perfil.esSuper = _super.Any(c => c.sCliente == _ses.sServidor && c.iPersonaNominus == _super1.iPersonaNominus);
                                perfil.esJefe = db.traJefeDirecto.Any(c => c.iTrabajador == _acc.iPersona);
                                perfil.tieneContrato = db.traContrato.Any(c => c.iTrabajador == _acc.iPersona);
                                perfil.tienePrestamo = db.traPrestamo
                                    .Join(db.traContrato,
                                        cp => cp.iContrato,
                                        ct => ct.iContrato,
                                        (cp, ct) => new { cp, ct })
                                    .Any(s => s.ct.iTrabajador == _acc.iPersona);
                            }
                        }
                    }
                }
            }
            return perfil;
        }
        public static PerNavegante PerPerfilContrato(Guid token, short sServidor, int iContratoPeriodo)
        {
            PerNavegante perfil = new PerNavegante();
            using (AuthContext dbA = new AuthContext())
            {
                authSesion _ses = dbA.authSesion.FirstOrDefault(c => c.gToken == token);

                using (NominusContext db = new NominusContext(sServidor))
                {
                    traContratoPeriodo _tcp = db.traContratoPeriodo.FirstOrDefault(c => c.iContratoPeriodo == iContratoPeriodo);
                    traContrato _ctt = db.traContrato.Find(_tcp.iContrato);
                    authAcceso _acc = dbA.authAcceso.FirstOrDefault(c => c.iPersona==_ctt.iTrabajador && c.sServidor == sServidor);
                    if (_acc != null)
                    {
                        authUsuario _usu = dbA.authUsuario.FirstOrDefault(c => c.gUsuario == _acc.gUsuario);
                        List<nomPersonalPerfilCliente> _super;
                        using (GeneralContext dg = new GeneralContext())
                        {
                            nomPersonal _userG = dg.nomPersonal.FirstOrDefault(c => c.xCorreo == _usu.email);
                            _super = dg.nomPersonalPerfilCliente.Where(c => c.bPerfil == 1 && c.sCliente == sServidor).ToList();

                            if (_acc != null)
                            {
                                perfil.iUsuario = _acc.iPersona;
                                perfil.sServidor = _ses.sServidor;
                                perfil.iContrato = _tcp.iContrato;
                                perfil.esAdmin = db.sysAdministrador.Any(c => c.iPersona == _acc.iPersona);
                                if (_userG != null)
                                {
                                    perfil.esSuper = _super.Any(c => c.iPersonaNominus == _userG.iPersonaNominus);
                                } else
                                {
                                    perfil.esSuper = false;
                                }
                                perfil.esJefe = db.traJefeDirecto.Any(c => c.iTrabajador == _acc.iPersona);
                                perfil.tieneContrato = db.traContrato.Any(c => c.iTrabajador == _acc.iPersona);
                                perfil.tienePrestamo = db.traPrestamo
                                    .Join(db.traContrato,
                                        cp => cp.iContrato,
                                        ct => ct.iContrato,
                                        (cp, ct) => new { cp, ct })
                                    .Any(s => s.ct.iTrabajador == _acc.iPersona);
                                _ses.sServidor = sServidor;
                                _ses.iContrato = iContratoPeriodo;
                                dbA.Entry(_ses).State = EntityState.Modified;
                                dbA.SaveChanges();
                            }
                        }
                    }
                }
            }
            return perfil;
        }

        public static PerNavegante PerPerfilServidor(Guid token, short sServidor)
        {
            PerNavegante perfil = new PerNavegante();
            using (AuthContext dbA = new AuthContext())
            {
                authSesion _ses = dbA.authSesion.FirstOrDefault(c => c.gToken == token);

                using (NominusContext db = new NominusContext(sServidor))
                {
                    authAcceso _acc = dbA.authAcceso.FirstOrDefault(c => c.gUsuario == _ses.gUsuario && c.sServidor == sServidor);
                    if (_acc != null)
                    {
                        authUsuario _usu = dbA.authUsuario.FirstOrDefault(c => c.gUsuario == _acc.gUsuario);
                        List<nomPersonalPerfilCliente> _super;
                        using (GeneralContext dg = new GeneralContext())
                        {
                            nomPersonal _userG = dg.nomPersonal.FirstOrDefault(c => c.xCorreo == _usu.email);
                            _super = dg.nomPersonalPerfilCliente.Where(c => c.bPerfil == 1 && c.sCliente == sServidor).ToList();
                            if (_acc != null)
                            {
                                perfil.iUsuario = _acc.iPersona;
                                perfil.sServidor = _ses.sServidor;
                                perfil.esAdmin = db.sysAdministrador.Any(c => c.iPersona == _acc.iPersona);
                                perfil.esSuper = _super.Any(c => c.iPersonaNominus == _userG.iPersonaNominus);
                                perfil.esJefe = db.traJefeDirecto.Any(c => c.iTrabajador == _acc.iPersona);
                                perfil.tieneContrato = db.traContrato.Any(c => c.iTrabajador == _acc.iPersona);
                                perfil.tienePrestamo = db.traPrestamo
                                    .Join(db.traContrato,
                                        cp => cp.iContrato,
                                        ct => ct.iContrato,
                                        (cp, ct) => new { cp, ct })
                                    .Any(s => s.ct.iTrabajador == _acc.iPersona);
                                _ses.sServidor = sServidor;
                                dbA.Entry(_ses).State = EntityState.Modified;
                                dbA.SaveChanges();
                            }
                        }
                    }
                }
            }
            return perfil;
        }

        public static Guid crearAuth(string xEmail, int iUsuario, short sServidor)
        {
            Guid gUsuario = Guid.NewGuid();
            using (AuthContext dbA = new AuthContext())
            {
                authUsuario _usuario = dbA.authUsuario.FirstOrDefault(c => c.email == xEmail);
                if (_usuario == null)
                {
                    _usuario = new authUsuario
                    {
                        email = xEmail,
                        gUsuario = gUsuario
                    };
                    dbA.authUsuario.Add(_usuario);
                    dbA.SaveChanges();
                }
                authAcceso _accs = dbA.authAcceso.FirstOrDefault(c => c.gUsuario == gUsuario && c.sServidor == sServidor);
                if (_accs == null)
                {
                    dbA.authAcceso.Add(new authAcceso
                    {
                        gAcceso = Guid.NewGuid(),
                        gUsuario = _usuario.gUsuario,
                        sServidor = sServidor,
                        iPersona = iUsuario
                    });
                    dbA.SaveChanges();
                }
            }
            return gUsuario;
        }

        public static IQueryable<perPersona> buscarPersonas(string cadena, short sServidor)
        {
            IQueryable<perPersona> resp;
            try
            {
                using (NominusContext db = new NominusContext(sServidor))
                {
                    resp = db.perPersona.FromSqlRaw("exec perBuscarPersonas @xBuscar", new SqlParameter("@xBuscar", cadena));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resp;
        }

        public static PerInfoPersona infoPersona(ParPersona persona)
        {
            PerInfoPersona rsp = new PerInfoPersona();
            using (NominusContext db = new NominusContext(persona.sServidor))
            {
                rsp.infoContrato = db._perInfoContrato.FromSqlRaw("exec perInfoContrato @iPersona", new SqlParameter("@iPersona", persona.iPersona)).FirstOrDefault();
            }
            if (persona.iIdZk != null)
            {
                using (MarcaZkContext dbZ = new MarcaZkContext(persona.sServidor))
                {
                    rsp.biometrica = dbZ.BioTemplate.Where(c => c.UserID == persona.iIdZk && c.Version == "10.0");
                }
            }
            return rsp;
        }

        public static AutConsolaCliente PerListaContractual(string xCorreo, short sCliente, byte bPerfil)
        {
            AutConsolaCliente rsp = new AutConsolaCliente();
            using (NominusContext dbN = new NominusContext(sCliente))
            {
                List<orgEmpresa> _emp = dbN.orgEmpresa.ToList();
                List<orgEmpresaImagen> _eim = dbN.orgEmpresaImagen.Where(c => c.bTipoImagen == 1).ToList();
                rsp.empresas = _emp.SelectMany(
                    imgs => _eim.Where(cim => cim.sEmpresa == imgs.sEmpresa),
                    (emp, img) => new { emp, img }).Select(s => new AutEmpresa
                    {
                        sEmpresa = s.emp.sEmpresa,
                        xAbreviado = s.emp.xAbreviado,
                        xRazonSocial = s.emp.xRazonSocial,
                        jImagen = s.img.jImagen,
                        jAppConfig = s.emp.jAppConfig                        
                    });
                autUsuario _autUsr = dbN.autUsuario.FirstOrDefault(c => c.xEmail == xCorreo);
                if (_autUsr != null)
                {
                    rsp.trabajador = GetTrabajadores(dbN).Where(c => c.iTrabajador == _autUsr.iUsuario).FirstOrDefault();
                }
                if (bPerfil == 4 || bPerfil==5)  // Auxiliar RRHH o consultor sólo ve personal con contrato
                {
                    rsp.colaboradores = GetTrabajadores(dbN)
                        .OrderBy(o=>o.persona.xApellidoPaterno).ThenBy(o=>o.persona.xApellidoMaterno).ThenBy(o=>o.persona.xNombres).ToList();
                }
                else if (bPerfil == 7 || bPerfil == 8)
                {  // Supervisor o jefe, ve sólo contratos de sus colaboradores
                    if (_autUsr != null)
                    {
                        rsp.colaboradores = GetTrabajadores(dbN)
                            .OrderBy(o => o.persona.xApellidoPaterno).ThenBy(o => o.persona.xApellidoMaterno).ThenBy(o => o.persona.xNombres).ToList();
                    }
                }
                else
                {
                    rsp.colaboradores = GetTrabajadores(dbN)
                        .OrderBy(o => o.persona.xApellidoPaterno).ThenBy(o => o.persona.xApellidoMaterno).ThenBy(o => o.persona.xNombres).ToList();
                }
            }
            return rsp;
        }
    }

    #region Outputs Per
    public class PerNavegante
    {
        public int? iUsuario { get; set; }
        public bool esSuper { get; set; }
        public bool esAdmin { get; set; }
        public bool esJefe { get; set; }
        public bool tieneContrato { get; set; }
        public bool tienePrestamo { get; set; }
        public short? sServidor { get; set; }
        public int? iContrato { get; set; }
    }

    public class PerPersonaUsuario : perPersona
    {
        public int? zkUser { get; set; }
        public Guid? gUsuario { get; set; }
        public string xEmail { get; set; }
    }

    #endregion

    #region Inputs 
    public class ParBuscaPersona
    {
        public ParBuscaPersona()
        {
            sTop = 10;
        }
        public short sServidor { get; set; }
        public string xCadena { get; set; }
        public short sTop { get; set; } 
    }

    public class ParPersona
    {
        public short sServidor { get; set; }
        public int iPersona { get; set; }
        public int? iIdZk { get; set; }
    }

    public class PerInfoContrato
    {
        public int iContrato { get; set; }
        public short sSede { get; set; }
        public DateTime dDesde { get; set; }
        public DateTime? dHasta { get; set; }
        public short sCargo { get; set; }
        public string xSede { get; set; }
        public string xCargo { get; set; }
    }

    public class PerInfoPersona
    {
        public PerInfoContrato infoContrato { get; set; }
        public IQueryable<BioTemplate> biometrica { get; set; }
    }

    #endregion

}