﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace apiCoreNominus.Model
{
    public partial class NominusContext : DbContext
    {
        public virtual DbSet<marDispositivo> marDispositivo { get; set; }
        public virtual DbSet<marEmparejar> marEmparejar { get; set; }
        public virtual DbSet<marGeometria> marGeometria { get; set; }
        public virtual DbSet<marMapaGPS> marMapaGPS { get; set; }
        public virtual DbSet<marMarcacion> marMarcacion { get; set; }
        public virtual DbSet<marModo> marModo { get; set; }
        public virtual DbSet<marPlataforma> marPlataforma { get; set; }
        public virtual DbSet<marRiesgoCOVID> marRiesgoCOVID { get; set; }

        partial void OnModelCreatingMarcacion(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<marEmparejar>(entity =>
            {
                entity.HasKey(e => e.iPersona);
            });
            modelBuilder.Entity<marDispositivo>(entity =>
            {
                entity.HasKey(e => e.sDispositivo);
                entity.Property(e => e.sDispositivo).ValueGeneratedNever();
                entity.Property(e => e.xDescripcion).HasMaxLength(50).IsUnicode(false);
            });

            modelBuilder.Entity<marGeometria>(entity => {
                entity.HasKey(e => new { e.sDispositivo, e.bPunto });
            });

            modelBuilder.Entity<marMapaGPS>(entity => {
                entity.HasKey(p => new { p.sDispositivo, p.bPunto, p.bPuntoGPS });
            });

            modelBuilder.Entity<marMarcacion>(entity =>
            {
                entity.HasKey(e => e.iMarca);
                entity.Property(e => e.dHora).HasColumnType("datetime");
            });

            modelBuilder.Entity<marModo>(entity =>
            {
                entity.HasKey(e => e.bModo);
                entity.Property(e => e.xModo).HasMaxLength(40).IsUnicode(false);
            });

            modelBuilder.Entity<marPlataforma>(entity =>
            {
                entity.HasKey(e => e.bPlataforma);
                entity.Property(e => e.xPlataforma).HasMaxLength(40).IsUnicode(false);
            });

            modelBuilder.Entity<marRiesgoCOVID>(entity => {
                entity.HasKey(c => new { c.iContrato, c.dFecha });
            });
        }
    }

    public partial class marDispositivo
    {
        public short sDispositivo { get; set; }
        public string xDescripcion { get; set; }
        public string cLlave { get; set; }
        public byte? bPlataforma { get; set; }
        public int? iDispositivoInterno { get; set; }
        public short? sSede { get; set; }
    }
    public class marEmparejar
    {
        public int iPersona { get; set; }
        public DateTime dVencimiento { get; set; }
        public string xRespuesta { get; set; }
    }
    public class marGeometria
    {
        public short sDispositivo { get; set; }
        public byte bPunto { get; set; }
        public string xPunto { get; set; }
        public int? iAprobador { get; set; }
        public DateTime? dAprobacion { get; set; }
    }
    public class marMapaGPS
    {
        public short sDispositivo { get; set; }
        public byte bPunto { get; set; }
        public byte bPuntoGPS { get; set; }
        public double? fLon { get; set; }
        public double? fLat { get; set; }
    }
    public partial class marMarcacion
    {
        public int iMarca { get; set; }
        public short? sDispositivo { get; set; }
        public int? iPersona { get; set; }
        public DateTime? dHora { get; set; }
        public int? iControl { get; set; }

    }
    public partial class marModo
    {
        public byte bModo { get; set; }
        public string xModo { get; set; }

    }
    public partial class marPlataforma
    {
        public byte bPlataforma { get; set; }
        public string xPlataforma { get; set; }
        public byte? bModo { get; set; }
        public DateTime? dUltimaSincronizacion { get; set; }
    }
    public class MarGeometriaRadial : marGeometria
    {
        public marMapaGPS punto { get; set; }
    }

    public class MarGeometriArea : marGeometria
    {
        public IEnumerable<marMapaGPS> puntos { get; set; }
    }

    public class MarDispositivoTeletrabajo : marDispositivo
    {
        public IEnumerable<MarGeometriaRadial> ubicaciones { get; set; }
    }

}
