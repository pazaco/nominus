﻿using Org.BouncyCastle.Crmf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace apiCoreNominus.Model
{
    public partial class Procs
    {

        public static MarDispositivoTeletrabajo GetDispositivo(NominusContext dbN, int iTrabajador)
        {
            return dbN.marDispositivo.Where(f => f.iDispositivoInterno == iTrabajador).AsEnumerable()
            .GroupJoin(dbN.marGeometria.AsEnumerable()
            .GroupJoin(dbN.marMapaGPS,
                g => new { g.sDispositivo, g.bPunto }, mg => new { mg.sDispositivo, mg.bPunto },
                (g, mg) => new MarGeometriaRadial
                {
                    sDispositivo = g.sDispositivo,
                    bPunto = g.bPunto,
                    xPunto = g.xPunto,
                    iAprobador = g.iAprobador,
                    dAprobacion = g.dAprobacion,
                    punto = mg.Any() ? mg.First() : null
                }).AsEnumerable(),
            d => d.sDispositivo, dg => dg.sDispositivo, (d, dg) => new MarDispositivoTeletrabajo
            {
                sDispositivo = d.sDispositivo,
                xDescripcion = d.xDescripcion,
                cLlave = d.cLlave,
                bPlataforma = d.bPlataforma,
                iDispositivoInterno = d.iDispositivoInterno,
                sSede = d.sSede,
                ubicaciones = dg.Any() ? dg.AsEnumerable() : null
            }).FirstOrDefault();
        }
    }

    public class MarInMarca
    {
        public short sServidor { get; set; }
        public int iControl { get; set; }
        public string cPuerta { get; set; }
        public DateTime dHora { get; set; }
    }

    public class MarOutMarca
    {
        public byte bRespuesta { get; set; }
        public string[] aRespuestaData { get; set; }
        public int? iControl { get; set; }
    }

}
