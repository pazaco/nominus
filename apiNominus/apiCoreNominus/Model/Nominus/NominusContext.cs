﻿using Microsoft.EntityFrameworkCore;

namespace apiCoreNominus.Model
{
    public partial class NominusContext : DbContext
    {
        private short sServidor;
        private bool _Connected = false;

        public NominusContext(short sServidor)
        {
            this.sServidor = sServidor;
        }

        public NominusContext(DbContextOptions<NominusContext> options)
            : base(options)
        {
        }

        public bool isConnected()
        {
            return _Connected;
        }

        public virtual DbSet<appTipoVacaciones> appTipoVacaciones { get; set; }
        public virtual DbSet<appVacaciones> appVacaciones { get; set; }
        public virtual DbSet<asiAsistencia> asiAsistencia { get; set; }
        public virtual DbSet<asiControl> asiControl { get; set; }
        public virtual DbSet<asiDiaria> asiDiaria { get; set; }
        public virtual DbSet<asiFeriado> asiFeriado { get; set; }
        public virtual DbSet<asiFlujoPapeleta> asiFlujoPapeleta { get; set; }
        public virtual DbSet<asiHorario> asiHorario { get; set; }
        public virtual DbSet<asiHorarioActividad> asiHorarioActividad { get; set; }
        public virtual DbSet<asiMarcacion> asiMarcacion { get; set; }
        public virtual DbSet<asiMarcacionTemp> asiMarcacionTemp { get; set; }
        public virtual DbSet<asiPapeleta> asiPapeleta { get; set; }
        public virtual DbSet<asiRelojMarcacion> asiRelojMarcacion { get; set; }
        public virtual DbSet<asiRiesgoCOVID> asiRiesgoCOVID { get; set; }
        public virtual DbSet<asiRolNotificador> asiRolNotificador { get; set; }
        public virtual DbSet<asiTipoFlujoPapeleta> asiTipoFlujoPapeleta { get; set; }
        public virtual DbSet<asiTipoPapeleta> asiTipoPapeleta { get; set; }
        public virtual DbSet<asiTipoTransaccion> asiTipoTransaccion { get; set; }
        public virtual DbSet<asiTransaccion> asiTransaccion { get; set; }
        public virtual DbSet<autClaim> autClaim { get; set; }
        public virtual DbSet<autClaseNavegador> autClaseNavegador { get; set; }
        public virtual DbSet<autGrupoClaim> autGrupoClaim { get; set; }
        public virtual DbSet<autNavegador> autNavegador { get; set; }
        public virtual DbSet<autSesion> autSesion { get; set; }
        public virtual DbSet<autUsuario> autUsuario { get; set; }
        public virtual DbSet<autAvatar> _autAvatar { get; set; }
        public virtual DbSet<AutContratos> _autContratos { get; set; }
        public virtual DbSet<autUsuarioClaim> autUsuarioClaim { get; set; }
        public virtual DbSet<belPDFs> belPDFs { get; set; }
        public virtual DbSet<belCalculos> _belCalculos { get; set; }
        public virtual DbSet<horControl> horControl { get; set; }
        public virtual DbSet<horHorario> horHorario { get; set; }
        public virtual DbSet<horIncidencia> horIncidencia { get; set; }
        public virtual DbSet<horInfoAnexa> horInfoAnexa { get; set; }
        public virtual DbSet<horJornada> horJornada { get; set; }
        public virtual DbSet<horMargen> horMargen { get; set; }
        public virtual DbSet<horMargenIncidencia> horMargenIncidencia { get; set; }
        public virtual DbSet<horProgramacion> horProgramacion { get; set; }
        public virtual DbSet<horProgramacionActual> horProgramacionActual { get; set; }
        public virtual DbSet<horDetProgramacion> horDetProgramacion { get; set; }
        public virtual DbSet<horRequerimientoMarca> horRequerimientoMarca { get; set; }
        public virtual DbSet<horTag> horTag { get; set; }
        public virtual DbSet<horTipoRotacion> horTipoRotacion { get; set; }
        public virtual DbSet<horTolerancia> horTolerancia { get; set; }
        public virtual DbSet<isoCalculo> isoCalculo { get; set; }
        public virtual DbSet<isoCategoriaOcupacional> isoCategoriaOcupacional { get; set; }
        public virtual DbSet<isoClaseConcepto> isoClaseConcepto { get; set; }
        public virtual DbSet<isoDepartamento> isoDepartamento { get; set; }
        public virtual DbSet<isoDistrito> isoDistrito { get; set; }
        public virtual DbSet<isoDocSustentaVinculoFamiliar> isoDocSustentaVinculoFamiliar { get; set; }
        public virtual DbSet<isoEntidadFinanciera> isoEntidadFinanciera { get; set; }
        public virtual DbSet<isoInstitucionEducativa> isoInstitucionEducativa { get; set; }
        public virtual DbSet<isoMoneda> isoMoneda { get; set; }
        public virtual DbSet<isoMotivoBajaDerechohabiente> isoMotivoBajaDerechohabiente { get; set; }
        public virtual DbSet<isoMotivoCese> isoMotivoCese { get; set; }
        public virtual DbSet<isoNacionalidad> isoNacionalidad { get; set; }
        public virtual DbSet<isoOcupacionSunat> isoOcupacionSunat { get; set; }
        public virtual DbSet<isoPais> isoPais { get; set; }
        public virtual DbSet<isoPeriodicidad> isoPeriodicidad { get; set; }
        public virtual DbSet<isoProvincia> isoProvincia { get; set; }
        public virtual DbSet<isoRegimenLaboral> isoRegimenLaboral { get; set; }
        public virtual DbSet<isoRegimenPensionario> isoRegimenPensionario { get; set; }
        public virtual DbSet<isoRegimenPensionarioCodigo> isoRegimenPensionarioCodigo { get; set; }
        public virtual DbSet<isoRegimenPensionarioInfo> isoRegimenPensionarioInfo { get; set; }
        public virtual DbSet<isoSCTRestado> isoSCTRestado { get; set; }
        public virtual DbSet<isoSituacionSunat> isoSituacionSunat { get; set; }
        public virtual DbSet<isoSubClaseConcepto> isoSubClaseConcepto { get; set; }
        public virtual DbSet<isoTipoActividad> isoTipoActividad { get; set; }
        public virtual DbSet<isoTipoActividadHorario> isoTipoActividadHorario { get; set; }
        public virtual DbSet<isoTipoCalculo> isoTipoCalculo { get; set; }
        public virtual DbSet<isoTipoComprobante> isoTipoComprobante { get; set; }
        public virtual DbSet<isoTipoConcepto> isoTipoConcepto { get; set; }
        public virtual DbSet<isoTipoContrato> isoTipoContrato { get; set; }
        public virtual DbSet<isoTipoCtaBancaria> isoTipoCtaBancaria { get; set; }
        public virtual DbSet<isoTipoDocIdentidad> isoTipoDocIdentidad { get; set; }
        public virtual DbSet<isoTipoPago> isoTipoPago { get; set; }
        public virtual DbSet<isoTipoPlanilla> isoTipoPlanilla { get; set; }
        public virtual DbSet<isoTipoSuspensionRL> isoTipoSuspensionRL { get; set; }
        public virtual DbSet<isoTipoTrabajador> isoTipoTrabajador { get; set; }
        public virtual DbSet<isoTipoVia> isoTipoVia { get; set; }
        public virtual DbSet<isoTipoZona> isoTipoZona { get; set; }
        public virtual DbSet<isoUbicacion> isoUbicacion { get; set; }
        public virtual DbSet<isoUnidadMedida> isoUnidadMedida { get; set; }
        public virtual DbSet<isoVinculoFamiliar> isoVinculoFamiliar { get; set; }
        public virtual DbSet<orgArea> orgArea { get; set; }
        public virtual DbSet<orgCargo> orgCargo { get; set; }
        public virtual DbSet<orgCargoFemenino> orgCargoFemenino { get; set; }
        public virtual DbSet<orgCargoFuncion> orgCargoFuncion { get; set; }
        public virtual DbSet<orgCargoServicio> orgCargoServicio { get; set; }
        public virtual DbSet<orgCentroCosto> orgCentroCosto { get; set; }
        public virtual DbSet<orgCentroCostoCodigo> orgCentroCostoCodigo { get; set; }
        public virtual DbSet<orgCentroCostoRelacion> orgCentroCostoRelacion { get; set; }
        public virtual DbSet<orgDiccCargoSunat> orgDiccCargoSunat { get; set; }
        public virtual DbSet<orgEmpresa> orgEmpresa { get; set; }
        public virtual DbSet<orgEmpresaActividad> orgEmpresaActividad { get; set; }
        public virtual DbSet<orgEmpresaCalculoCtaContableNeto> orgEmpresaCalculoCtaContableNeto { get; set; }
        public virtual DbSet<orgEmpresaCtaBancaria> orgEmpresaCtaBancaria { get; set; }
        public virtual DbSet<orgEmpresaCtaContable> orgEmpresaCtaContable { get; set; }
        public virtual DbSet<orgEmpresaDireccion> orgEmpresaDireccion { get; set; }
        public virtual DbSet<orgEmpresaImagen> orgEmpresaImagen { get; set; }
        public virtual DbSet<orgEmpresaPeriodo> orgEmpresaPeriodo { get; set; }
        public virtual DbSet<orgEmpresaPlanEPS> orgEmpresaPlanEPS { get; set; }
        public virtual DbSet<orgEmpresaRepLegal> orgEmpresaRepLegal { get; set; }
        public virtual DbSet<orgEmpresaSede> orgEmpresaSede { get; set; }
        public virtual DbSet<orgEmpresaSedeDireccion> orgEmpresaSedeDireccion { get; set; }
        public virtual DbSet<orgEmpresaTelefono> orgEmpresaTelefono { get; set; }
        public virtual DbSet<orgEstructuraSalarial> orgEstructuraSalarial { get; set; }
        public virtual DbSet<orgEstructuraSalarialDet> orgEstructuraSalarialDet { get; set; }
        public virtual DbSet<orgNivelCargo> orgNivelCargo { get; set; }
        public virtual DbSet<orgNivelCentroCosto> orgNivelCentroCosto { get; set; }
        public virtual DbSet<orgProyecto> orgProyecto { get; set; }
        public virtual DbSet<orgSede> orgSede { get; set; }
        public virtual DbSet<perArchivo> perArchivo { get; set; }
        public virtual DbSet<perBuscar> perBuscar { get; set; }
        public virtual DbSet<perCtaBancaria> perCtaBancaria { get; set; }
        public virtual DbSet<perDireccion> perDireccion { get; set; }
        public virtual DbSet<perDireccionCanonica> perDireccionCanonica { get; set; }
        public virtual DbSet<perEducacion> perEducacion { get; set; }
        public virtual DbSet<perFamiliar> perFamiliar { get; set; }
        public virtual DbSet<perFamiliarBajaDH> perFamiliarBajaDH { get; set; }
        public virtual DbSet<perFamiliarDocSustentaVinculo> perFamiliarDocSustentaVinculo { get; set; }
        public virtual DbSet<perFono> perFono { get; set; }
        public virtual DbSet<perFoto> perFoto { get; set; }
        public virtual DbSet<perNacionalidad> perNacionalidad { get; set; }
        public virtual DbSet<perPaisEmisorDocIdentidad> perPaisEmisorDocIdentidad { get; set; }
        public virtual DbSet<perPersona> perPersona { get; set; }
        public virtual DbSet<perPersonaCodigo> perPersonaCodigo { get; set; }
        public virtual DbSet<perTipoFoto> perTipoFoto { get; set; }
        public virtual DbSet<perUsuario> perUsuario { get; set; }
        public virtual DbSet<perUsuarioAccesoEmpresa> perUsuarioAccesoEmpresa { get; set; }
        public virtual DbSet<perUsuarioAccesoTab> perUsuarioAccesoTab { get; set; }
        public virtual DbSet<perUsuarioSuper> perUsuarioSuper { get; set; }
        public virtual DbSet<perUsuarioVigencia> perUsuarioVigencia { get; set; }
        public virtual DbSet<perVistaContractual> perVistaContractual { get; set; }
        public virtual DbSet<remConcepto> remConcepto { get; set; }
        public virtual DbSet<remConceptoAsistencia> remConceptoAsistencia { get; set; }
        public virtual DbSet<remConceptoAsistenciaEmpresaPlanilla> remConceptoAsistenciaEmpresaPlanilla { get; set; }
        public virtual DbSet<remConceptoAsistenciaPlanilla> remConceptoAsistenciaPlanilla { get; set; }
        public virtual DbSet<remConceptoCalculo> remConceptoCalculo { get; set; }
        public virtual DbSet<remConceptoCalculoAfpCuentas> remConceptoCalculoAfpCuentas { get; set; }
        public virtual DbSet<remConceptoCalculoAsistencia> remConceptoCalculoAsistencia { get; set; }
        public virtual DbSet<remConceptoCalculoCuentas> remConceptoCalculoCuentas { get; set; }
        public virtual DbSet<remConceptoCalculoDependencia> remConceptoCalculoDependencia { get; set; }
        public virtual DbSet<remConceptoCalculoDias> remConceptoCalculoDias { get; set; }
        public virtual DbSet<remConceptoCalculoFormula> remConceptoCalculoFormula { get; set; }
        public virtual DbSet<remConceptoCalculoHistory> remConceptoCalculoHistory { get; set; }
        public virtual DbSet<remConceptoCalculoManual> remConceptoCalculoManual { get; set; }
        public virtual DbSet<remConceptoCalculoMonto> remConceptoCalculoMonto { get; set; }
        public virtual DbSet<remConceptoCalculoSumado> remConceptoCalculoSumado { get; set; }
        public virtual DbSet<remConceptoCalculoVigencia> remConceptoCalculoVigencia { get; set; }
        public virtual DbSet<remConceptoSubClase> remConceptoSubClase { get; set; }
        public virtual DbSet<remConceptoTipoSuspensionRL> remConceptoTipoSuspensionRL { get; set; }
        public virtual DbSet<remImportaPlanilla> remImportaPlanilla { get; set; }
        public virtual DbSet<remImportaPlanillaColumna> remImportaPlanillaColumna { get; set; }
        public virtual DbSet<remPeriodo> remPeriodo { get; set; }
        public virtual DbSet<remPlanilla> remPlanilla { get; set; }
        public virtual DbSet<sysAccesoMenu> sysAccesoMenu { get; set; }
        public virtual DbSet<sysAccesoTab> sysAccesoTab { get; set; }
        public virtual DbSet<sysAdministrador> sysAdministrador { get; set; }
        public virtual DbSet<sysArchivo> sysArchivo { get; set; }
        public virtual DbSet<sysConcepto> sysConcepto { get; set; }
        public virtual DbSet<sysConfig> sysConfig { get; set; }
        public virtual DbSet<sysContratoFormatoCampo> sysContratoFormatoCampo { get; set; }
        public virtual DbSet<sysEpsPlantilla> sysEpsPlantilla { get; set; }
        public virtual DbSet<sysGrupoArchivo> sysGrupoArchivo { get; set; }
        public virtual DbSet<sysGrupoConfig> sysGrupoConfig { get; set; }
        public virtual DbSet<sysGrupoReporte> sysGrupoReporte { get; set; }
        public virtual DbSet<sysMonto> sysMonto { get; set; }
        public virtual DbSet<sysMontoAnual> sysMontoAnual { get; set; }
        public virtual DbSet<sysMontoEmpresa> sysMontoEmpresa { get; set; }
        public virtual DbSet<sysMontoFecha> sysMontoFecha { get; set; }
        public virtual DbSet<sysOrigenArchivo> sysOrigenArchivo { get; set; }
        public virtual DbSet<sysPistaAuditoria> sysPistaAuditoria { get; set; }
        public virtual DbSet<sysPlanEPS> sysPlanEPS { get; set; }
        public virtual DbSet<sysPlanEPSdependencia> sysPlanEPSdependencia { get; set; }
        public virtual DbSet<sysPlanEPSdet> sysPlanEPSdet { get; set; }
        public virtual DbSet<sysReporte> sysReporte { get; set; }
        public virtual DbSet<sysTexto> sysTexto { get; set; }
        public virtual DbSet<terEmpresa> terEmpresa { get; set; }
        public virtual DbSet<terEmpresaAseguradoraSalud> terEmpresaAseguradoraSalud { get; set; }
        public virtual DbSet<terEmpresaCliente> terEmpresaCliente { get; set; }
        public virtual DbSet<terEmpresaDireccion> terEmpresaDireccion { get; set; }
        public virtual DbSet<terEmpresaTelefono> terEmpresaTelefono { get; set; }
        public virtual DbSet<traAFP> traAFP { get; set; }
        public virtual DbSet<traArea> traArea { get; set; }
        public virtual DbSet<traAsistencia> traAsistencia { get; set; }
        public virtual DbSet<traAsistenciaCentroCosto> traAsistenciaCentroCosto { get; set; }
        public virtual DbSet<traAsistenciaEstructuraSalarial> traAsistenciaEstructuraSalarial { get; set; }
        public virtual DbSet<traAsistenciaFecha> traAsistenciaFecha { get; set; }
        public virtual DbSet<traAsistenciaFechaCalculo> traAsistenciaFechaCalculo { get; set; }
        public virtual DbSet<traAsistenciaHora> traAsistenciaHora { get; set; }
        public virtual DbSet<traAsistenciaHoraCalculo> traAsistenciaHoraCalculo { get; set; }
        public virtual DbSet<traAsistenciaMarcacion> traAsistenciaMarcacion { get; set; }
        public virtual DbSet<traAsistenciaMotivo> traAsistenciaMotivo { get; set; }
        public virtual DbSet<traAsistenciaRelacion> traAsistenciaRelacion { get; set; }
        public virtual DbSet<traAsistenciaVacaciones> traAsistenciaVacaciones { get; set; }
        public virtual DbSet<traCentroCosto> traCentroCosto { get; set; }
        public virtual DbSet<traConceptoFijo> traConceptoFijo { get; set; }
        public virtual DbSet<traConceptoManual> traConceptoManual { get; set; }
        public virtual DbSet<traConceptoManualCentroCosto> traConceptoManualCentroCosto { get; set; }
        public virtual DbSet<traContrato> traContrato { get; set; }
        public virtual DbSet<traContratoFormato> traContratoFormato { get; set; }
        public virtual DbSet<traContratoLaboral> traContratoLaboral { get; set; }
        public virtual DbSet<traContratoListaNegra> traContratoListaNegra { get; set; }
        public virtual DbSet<traContratoPeriodo> traContratoPeriodo { get; set; }
        public virtual DbSet<traContratoPeriodoCese> traContratoPeriodoCese { get; set; }
        public virtual DbSet<traContratoPeriodoMintra> traContratoPeriodoMintra { get; set; }
        public virtual DbSet<traContratoPeriodoPrueba> traContratoPeriodoPrueba { get; set; }
        public virtual DbSet<traEPS> traEPS { get; set; }
        public virtual DbSet<traEPSdet> traEPSdet { get; set; }
        // public virtual DbSet<traEstadoContrato> traEstadoContrato { get; set; }
        public virtual DbSet<traHorarioRotativo> traHorarioRotativo { get; set; }
        public virtual DbSet<traHorarioRotativoActividad> traHorarioRotativoActividad { get; set; }
        public virtual DbSet<traHorarioSemanal> traHorarioSemanal { get; set; }
        public virtual DbSet<traHorarioSemanalActividad> traHorarioSemanalActividad { get; set; }
        public virtual DbSet<traJefeDirecto> traJefeDirecto { get; set; }
        public virtual DbSet<traMarcacion> traMarcacion { get; set; }
        public virtual DbSet<traPrestamo> traPrestamo { get; set; }
        public virtual DbSet<traPrestamoCalculo> traPrestamoCalculo { get; set; }
        public virtual DbSet<traPrestamoCuota> traPrestamoCuota { get; set; }
        public virtual DbSet<traPrestamoCuotaCalculo> traPrestamoCuotaCalculo { get; set; }
        public virtual DbSet<traPrestamoDesembolso> traPrestamoDesembolso { get; set; }
        public virtual DbSet<traPrestamoMotivo> traPrestamoMotivo { get; set; }
        public virtual DbSet<traProyecto> traProyecto { get; set; }
        public virtual DbSet<traRUC> traRUC { get; set; }
        public virtual DbSet<traRxH> traRxH { get; set; }
        public virtual DbSet<traSCTR> traSCTR { get; set; }
        public virtual DbSet<traSCTRfecha> traSCTRfecha { get; set; }
        public virtual DbSet<traStatusContrato> traStatusContrato { get; set; }
        public virtual DbSet<traTrabajador> traTrabajador { get; set; }
        public virtual DbSet<traVidaLey> traVidaLey { get; set; }
        public virtual DbSet<webFirma> webFirma { get; set; }
        public virtual DbSet<webMovimientoVacaciones> webMovimientoVacaciones { get; set; }
        public virtual DbSet<webNotificacion> webNotificacion { get; set; }
        public virtual DbSet<webPrestamo> webPrestamo { get; set; }
        public virtual DbSet<webPrestamoPagos> webPrestamoPagos { get; set; }
        public virtual DbSet<webRolFirma> webRolFirma { get; set; }
        public virtual DbSet<webTipoNotificacion> webTipoNotificacion { get; set; }
        public virtual DbSet<webNotificar> _webNotificar { get; set; }
        public virtual DbSet<webUrl> webUrl { get; set; }
        public virtual DbSet<belCalculosxPeriodo> _belCalculosxPeriodo { get; set; }
        public virtual DbSet<remBoletaCabecera> _remBoletaCabecera { get; set; }
        public virtual DbSet<remBoletaCabeceraEmpresa> _remBoletaCabeceraEmpresa { get; set; }
        public virtual DbSet<remBoletaCabeceraTitulo> _remBoletaCabeceraTitulo { get; set; }
        public virtual DbSet<remBoletaCabeceraAsistenciaFechas> _remBoletaCabeceraAsistenciaFechas { get; set; }
        public virtual DbSet<remBoletaCabeceraDatos> _remBoletaCabeceraDatos { get; set; }
        public virtual DbSet<remBoletaConcepto> _remBoletaConcepto { get; set; }
        public virtual DbSet<traContratosxPersona> _traContratosxPersona { get; set; }
        public virtual DbSet<webPagosPrestamo> _webPagosPrestamo { get; set; }
        public virtual DbSet<webVacaciones> _webVacaciones { get; set; }
        public virtual DbSet<PerInfoContrato> _perInfoContrato { get; set; }


        // DbSets sobre procedures
        public virtual DbSet<NomContrato> NomContrato { get; set; }
        public virtual DbSet<WebNotificar> WebNotificar { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                GenStringConexion strConn = Procs.Conexion(new TuplaClienteTipoDb { bTipoDb = 3, sCliente = this.sServidor });
                if (strConn == null)
                {
                    _Connected = false;
                }
                else
                {
                    _Connected = true;
                    optionsBuilder.UseSqlServer(strConn.xConexion);
                }
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<appTipoVacaciones>(entity =>
                {
                    entity.HasKey(e => e.cTipoNovedad)
                        .HasName("PK__appTipoV__E17E15E1B65DFD32");

                    entity.Property(e => e.cTipoNovedad)
                        .HasMaxLength(1)
                        .IsUnicode(false)
                        .IsFixedLength();

                    entity.Property(e => e.xTipoNovedad)
                        .IsRequired()
                        .HasMaxLength(12)
                        .IsUnicode(false);
                });

            modelBuilder.Entity<appVacaciones>(entity =>
            {
                entity.HasKey(e => e.iNovedadVacaciones)
                    .HasName("PK__appVacac__465A4C2FB050FB82");

                entity.Property(e => e.cTipoNovedad)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.dDesde).HasColumnType("date");

                entity.Property(e => e.dHasta).HasColumnType("date");

                entity.Property(e => e.xDescripcion).IsUnicode(false);

            });

            modelBuilder.Entity<asiAsistencia>(entity =>
            {
                entity.HasKey(e => e.iAsistencia);

                entity.Property(e => e.dFecha).HasColumnType("date");
            });

            modelBuilder.Entity<asiControl>(entity =>
            {
                entity.HasKey(e => e.iControl);

                entity.Property(e => e.dHoraControl).HasColumnType("datetime2(0)");

                entity.Property(e => e.dHoraHorario).HasColumnType("datetime2(0)");

            });

            modelBuilder.Entity<asiDiaria>(entity =>
            {
                entity.HasKey(e => new { e.iTrabajador, e.dAsistencia });

                entity.Property(e => e.dAsistencia).HasColumnType("date");

                entity.Property(e => e.hER).HasColumnType("datetime2(0)");

                entity.Property(e => e.hET).HasColumnType("datetime2(0)");

                entity.Property(e => e.hSR).HasColumnType("datetime2(0)");

                entity.Property(e => e.hST).HasColumnType("datetime2(0)");
            });

            modelBuilder.Entity<asiFeriado>(entity =>
            {
                entity.HasKey(e => e.sFeriado);

                entity.Property(e => e.bFijo).HasComment("Feriado fijo no cambia de fecha cada año, por ej: 01/01/xxxx siempre sera feriado por año nuevo, pero si no es fijo cambia cada año, por ej: jueves y viernes santo.");

                entity.Property(e => e.dFecha).HasColumnType("date");

                entity.Property(e => e.xFeriado)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<asiFlujoPapeleta>(entity =>
            {
                entity.HasKey(e => e.iFlujoPapeleta);

                entity.Property(e => e.iFlujoPapeleta)
                    .HasMaxLength(10)
                    .IsFixedLength();

                entity.Property(e => e.cRol)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.cTipoFlujo)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.dFlujo).HasColumnType("datetime2(0)");

                entity.Property(e => e.xNotificacion).IsUnicode(false);

            });

            modelBuilder.Entity<asiHorario>(entity =>
            {
                entity.HasKey(e => e.sHorario);

                entity.Property(e => e.xHorario)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<asiHorarioActividad>(entity =>
            {
                entity.HasKey(e => e.iHorarioActividad);

                entity.Property(e => e.bDiaSemana).HasComment("0:Todos los días, 1:Lunes");

                entity.Property(e => e.bTipoActividadHorario).HasComment("");

                entity.Property(e => e.tDesde).HasColumnType("time(0)");
            });

            modelBuilder.Entity<asiMarcacion>(entity =>
            {
                entity.HasKey(e => e.iMarcacion)
                    .HasName("PK_asiMarcacion_1");

                entity.Property(e => e.dtMarcacion).HasColumnType("datetime2(0)");
            });

            modelBuilder.Entity<asiMarcacionTemp>(entity =>
            {
                entity.HasKey(e => e.iMarcacionTemp)
                    .HasName("PK_asiMarcacion");

                entity.Property(e => e.xCodigoPersonal)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<asiPapeleta>(entity =>
            {
                entity.HasKey(e => new { e.bTipoPapeleta, e.iPapeleta });

                entity.Property(e => e.dAlta).HasColumnType("date");

            });

            modelBuilder.Entity<asiRelojMarcacion>(entity =>
            {
                entity.HasKey(e => e.sRelojMarcacion);

                entity.Property(e => e.sRelojMarcacion)
                    .HasComment("0: Sin reloj, -1:Manual, -2: Importado")
                    .ValueGeneratedNever();

                entity.Property(e => e.xRelojMarcacion)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<asiRiesgoCOVID>(entity => {
                entity.HasKey(c => c.iAsistencia);
            });

            modelBuilder.Entity<asiRolNotificador>(entity =>
            {
                entity.HasKey(e => e.cRol);

                entity.Property(e => e.cRol)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.xRol)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<asiTipoFlujoPapeleta>(entity =>
            {
                entity.HasKey(e => e.cTipoFlujo);

                entity.Property(e => e.cTipoFlujo)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.xTipoFlujo)
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<asiTipoPapeleta>(entity =>
            {
                entity.HasKey(e => e.bTipoPapeleta);

                entity.Property(e => e.xTipoPapeleta)
                    .HasMaxLength(80)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<asiTipoTransaccion>(entity =>
            {
                entity.HasKey(e => e.sTipoTransaccion);

                entity.Property(e => e.sTipoTransaccion).ValueGeneratedNever();

                entity.Property(e => e.xTipoTransaccion)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<asiTransaccion>(entity =>
            {
                entity.HasKey(e => e.iTransaccion);

                entity.Property(e => e.iTransaccion)
                    .HasMaxLength(10)
                    .IsFixedLength();

                entity.Property(e => e.jsonTransaccion).IsUnicode(false);

            });

            modelBuilder.Entity<autClaim>(entity =>
            {
                entity.HasKey(e => e.cClaim);

                entity.Property(e => e.cClaim)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.lPublico).HasDefaultValueSql("((1))");

                entity.Property(e => e.xClaim)
                    .HasMaxLength(100)
                    .IsUnicode(false);

            });

            modelBuilder.Entity<autClaseNavegador>(entity =>
            {
                entity.HasKey(e => e.bClaseNavegador);

                entity.Property(e => e.xClaseNavegador)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AutContratos>(entity =>
            {
                entity.HasKey(e => e.iContrato);

            });

            modelBuilder.Entity<autAvatar>(entity =>
            {
                entity.HasKey(e => e.iPersona);
            });


            modelBuilder.Entity<autGrupoClaim>(entity =>
            {
                entity.HasKey(e => e.bGrupo);

                entity.Property(e => e.xGrupo)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<autNavegador>(entity =>
            {
                entity.HasKey(e => e.sNavegador);

                entity.Property(e => e.sNavegador).ValueGeneratedNever();

                entity.Property(e => e.iLatencia).HasDefaultValueSql("((30))");

                entity.Property(e => e.xNavegador)
                    .IsRequired()
                    .IsUnicode(false);

            });

            modelBuilder.Entity<autSesion>(entity =>
            {
                entity.HasKey(e => e.gSesion)
                    .HasName("PK_autSesion_1");

                entity.Property(e => e.gSesion).HasDefaultValueSql("(newid())");

                entity.Property(e => e.cClaim)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.dVigencia).HasColumnType("datetime");

            });

            modelBuilder.Entity<autUsuario>(entity =>
            {
                entity.HasKey(e => e.iUsuario);

                entity.HasIndex(e => e.xEmail)
                    .HasName("IX_xEmail_unique")
                    .IsUnique();

                entity.Property(e => e.iUsuario).ValueGeneratedNever();

                entity.Property(e => e.dCreacion)
                    .HasColumnType("datetime2(0)")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.dUltimoLogin).HasColumnType("datetime2(0)");

                entity.Property(e => e.hSemilla).HasMaxLength(64);

                entity.Property(e => e.xEmail)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.xPreguntaSecreta)
                    .HasMaxLength(100)
                    .IsUnicode(false);

            });

            modelBuilder.Entity<autUsuarioClaim>(entity =>
            {
                entity.HasKey(e => new { e.iUsuario, e.cClaim });

                entity.Property(e => e.cClaim)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.xValor).IsUnicode(false);

            });

            modelBuilder.Entity<belCalculos>(entity => {
                entity.HasKey(e => e.iCalculoPlanilla);
            });

            modelBuilder.Entity<belCalculosxPeriodo>(entity => {
                entity.HasKey(e => e.iCalculoPlanilla);
            });

            modelBuilder.Entity<belPDFs>(entity =>
            {
                entity.HasKey(e => e.cPDF);

                entity.Property(e => e.cPDF)
                    .HasMaxLength(16)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.dGeneracion).HasColumnType("datetime2(0)");
            });

            modelBuilder.Entity<horControl>(entity =>
            {
                entity.HasKey(e => e.sControl)
                    .HasName("PK_asi_Control_1");

                entity.Property(e => e.sControl).ValueGeneratedNever();

                entity.Property(e => e.cTipoControl)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.dHora).HasColumnType("time(0)");

                entity.Property(e => e.sDiasPrevio).HasDefaultValueSql("((0))");

            });

            modelBuilder.Entity<horHorario>(entity =>
            {
                entity.HasKey(e => e.sHorario)
                    .HasName("PK_asi_Horario");

                entity.Property(e => e.xHorario)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<horIncidencia>(entity =>
            {
                entity.HasKey(e => e.sIncidencia);

                entity.Property(e => e.xIncidencia)
                    .HasMaxLength(60)
                    .IsUnicode(false);

            });

            modelBuilder.Entity<horInfoAnexa>(entity =>
            {
                entity.HasKey(e => e.sInfoAnexa);

                entity.Property(e => e.sInfoAnexa).ValueGeneratedNever();

                entity.Property(e => e.xDescriptor)
                    .HasMaxLength(40)
                    .IsUnicode(false);

            });

            modelBuilder.Entity<horJornada>(entity =>
            {
                entity.HasKey(e => e.sJornada)
                    .HasName("PK_asi_Jornada");

                entity.Property(e => e.sJornada).ValueGeneratedOnAdd();

                entity.Property(e => e.cDias)
                    .HasMaxLength(8)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('LMWJVSD')");

                entity.Property(e => e.xJornada)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<horMargen>(entity =>
            {
                entity.HasKey(e => e.sMargen);

                entity.Property(e => e.sMargen).ValueGeneratedNever();

                entity.Property(e => e.cTipoControl)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.xMargen)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<horMargenIncidencia>(entity =>
            {
                entity.HasKey(e => e.iMargen);

                entity.Property(e => e.iMargen)
                    .HasMaxLength(10)
                    .IsFixedLength();

                entity.Property(e => e.sIncidencia).HasDefaultValueSql("((0))");

            });

            modelBuilder.Entity<horDetProgramacion>(entity =>
            {
                entity.HasKey(e => e.iDetProgramacion);
                entity.Property(e => e.dFecha).HasColumnType("date");
            });

            modelBuilder.Entity<horProgramacion>(entity =>
            {
                entity.HasKey(e => e.iProgramacion);
                entity.Property(e => e.dDesde).HasColumnType("date");
                entity.Property(e => e.dHasta).HasColumnType("date");
            });

            modelBuilder.Entity<horProgramacionActual>(entity =>
            {
                entity.HasKey(e => e.iProgramacion);
                entity.Property(e => e.dDesde).HasColumnType("date");
                entity.Property(e => e.dHasta).HasColumnType("date");
            });

            modelBuilder.Entity<horRequerimientoMarca>(entity =>
            {
                entity.HasKey(e => e.bRequerimientoMarca);

                entity.Property(e => e.xRequerimientoMarca)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<horTag>(entity =>
            {
                entity.HasKey(e => e.sJornada);
            });

            modelBuilder.Entity<horTipoRotacion>(entity =>
            {
                entity.HasKey(e => e.sRotacion);

                entity.Property(e => e.sRotacion).ValueGeneratedNever();

                entity.Property(e => e.cFactor)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();
            });

            modelBuilder.Entity<horTolerancia>(entity =>
            {
                entity.HasKey(e => e.bTolerancia)
                    .HasName("PK_asi_Tolerancia");

                entity.Property(e => e.lMarcaControl)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.lNotificarJefe)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.xTolerancia)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<isoCalculo>(entity =>
            {
                entity.HasKey(e => e.bCalculo);

                entity.Property(e => e.bCalculo).HasComment("1:Adelanto, 2:Boleta, 3:Liquidación, 4:Vacaciones, 5:Gratificaciones, 6:CTS, 7:Utilidades");

                entity.Property(e => e.bTipoCalculo)
                    .HasDefaultValueSql("((2))")
                    .HasComment("1:Remunerar, 2:Provisionar");

                entity.Property(e => e.xCalculo)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

            });

            modelBuilder.Entity<isoCategoriaOcupacional>(entity =>
            {
                entity.HasKey(e => e.bCategoriaOcupacional);

                entity.Property(e => e.xCategoriaOcupacional)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<isoClaseConcepto>(entity =>
            {
                entity.HasKey(e => e.bClaseConcepto);

                entity.Property(e => e.xClaseConcepto)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<isoDepartamento>(entity =>
            {
                entity.HasKey(e => e.sDepartamento);

                entity.Property(e => e.xCodSunat)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.xDepartamento)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

            });

            modelBuilder.Entity<isoDistrito>(entity =>
            {
                entity.HasKey(e => e.sDistrito);

                entity.Property(e => e.xCodSunat)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.xDistrito)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

            });

            modelBuilder.Entity<isoDocSustentaVinculoFamiliar>(entity =>
            {
                entity.HasKey(e => e.bDocSustentaVinculo)
                    .HasName("PK_isoFamiliarDocSustentaVinculo");

                entity.Property(e => e.bDocSustentaVinculo).HasComment("(Tabla 27) Documento que sustenta vínculo familiar");

                entity.Property(e => e.xDocSustentaVinculo)
                    .IsRequired()
                    .HasMaxLength(40)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<isoEntidadFinanciera>(entity =>
            {
                entity.HasKey(e => e.sEntidadFinanciera);

                entity.Property(e => e.sEntidadFinanciera).ValueGeneratedNever();

                entity.Property(e => e.xAbreviado)
                    .IsRequired()
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.xEntidadFinanciera)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<isoInstitucionEducativa>(entity =>
            {
                entity.HasKey(e => e.iInstitucionEducativa);

                entity.Property(e => e.iInstitucionEducativa).ValueGeneratedNever();

                entity.Property(e => e.cInstitucionEducativa)
                    .HasMaxLength(9)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.xInstitucionEducativa)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<isoMoneda>(entity =>
            {
                entity.HasKey(e => e.sMoneda);

                entity.Property(e => e.sMoneda).ValueGeneratedNever();

                entity.Property(e => e.xCodigo)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.xMoneda)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.xMonedaPlural)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.xSimbolo)
                    .IsRequired()
                    .HasMaxLength(3);
            });

            modelBuilder.Entity<isoMotivoBajaDerechohabiente>(entity =>
            {
                entity.HasKey(e => e.bMotivoBaja);

                entity.Property(e => e.bMotivoBaja).HasComment("(Tabla 20) Motivo de baja como derechohabiente");

                entity.Property(e => e.xMotivoBaja)
                    .IsRequired()
                    .HasMaxLength(35)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<isoMotivoCese>(entity =>
            {
                entity.HasKey(e => e.bMotivoCese)
                    .HasName("PK_traContratoCeseMotivo");

                entity.Property(e => e.xMotivoCese)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<isoNacionalidad>(entity =>
            {
                entity.HasKey(e => e.sNacionalidad);

                entity.Property(e => e.sNacionalidad)
                    .HasComment("Código de nacionalidad de T-REGISTROS tabla 4")
                    .ValueGeneratedNever();

                entity.Property(e => e.sPais).HasComment("");

                entity.Property(e => e.xCiudadano)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.xNacionalidad)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<isoOcupacionSunat>(entity =>
            {
                entity.HasKey(e => e.cOcupacion)
                    .HasName("PK_isoCargoSunat");

                entity.Property(e => e.cOcupacion)
                    .HasMaxLength(6)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.xOcupacion)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<isoPais>(entity =>
            {
                entity.HasKey(e => e.sPais)
                    .HasName("PK_isoPais_1");

                entity.Property(e => e.sPais).ValueGeneratedNever();

                entity.Property(e => e.cPais)
                    .IsRequired()
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.cPais3)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.xPais)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<isoPeriodicidad>(entity =>
            {
                entity.HasKey(e => e.bPeriodicidadRem);

                entity.Property(e => e.xPeriodicidadRem)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<isoProvincia>(entity =>
            {
                entity.HasKey(e => e.sProvincia);

                entity.Property(e => e.xCodSunat)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.xProvincia)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

            });

            modelBuilder.Entity<isoRegimenLaboral>(entity =>
            {
                entity.HasKey(e => e.bRegimenLaboral);

                entity.Property(e => e.xAbreviado)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.xRegimenLaboral)
                    .IsRequired()
                    .HasMaxLength(60)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<isoRegimenPensionario>(entity =>
            {
                entity.HasKey(e => e.bRegimenPensionario)
                    .HasName("PK_traAFP");

                entity.Property(e => e.xRegimenPensionario)
                    .IsRequired()
                    .HasMaxLength(60)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<isoRegimenPensionarioCodigo>(entity =>
            {
                entity.HasKey(e => e.bRegimenPensionario);

                entity.Property(e => e.xCodigo)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false);

            });

            modelBuilder.Entity<isoRegimenPensionarioInfo>(entity =>
            {
                entity.HasKey(e => new { e.bRegimenPensionario, e.dDesde })
                    .HasName("PK_isoAFPinfo");

                entity.Property(e => e.dDesde).HasColumnType("date");

                entity.Property(e => e.rAporte)
                    .HasColumnType("decimal(5, 2)")
                    .HasDefaultValueSql("((10.00))");

                entity.Property(e => e.rComisionFlujo).HasColumnType("decimal(5, 2)");

                entity.Property(e => e.rComisionMixta).HasColumnType("decimal(5, 2)");

                entity.Property(e => e.rPrima).HasColumnType("decimal(5, 2)");

            });

            modelBuilder.Entity<isoSCTRestado>(entity =>
            {
                entity.HasKey(e => e.bSCTRestado);

                entity.Property(e => e.xSCTRestado)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<isoSituacionSunat>(entity =>
            {
                entity.HasKey(e => e.bSituacion);

                entity.Property(e => e.xSituacion)
                    .IsRequired()
                    .HasMaxLength(75)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<isoSubClaseConcepto>(entity =>
            {
                entity.HasKey(e => e.bSubClaseConcepto);

                entity.Property(e => e.xSubClaseConcepto)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

            });

            modelBuilder.Entity<isoTipoActividad>(entity =>
            {
                entity.HasKey(e => e.iTipoActividad);

                entity.Property(e => e.iTipoActividad)
                    .HasComment("(Tabla 1) TIPO DE ACTIVIDAD (Se usa en Estructura 2 y 3)")
                    .ValueGeneratedNever();

                entity.Property(e => e.xTipoActividad)
                    .IsRequired()
                    .HasMaxLength(40)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<isoTipoActividadHorario>(entity =>
            {
                entity.HasKey(e => e.bTipoActividadHorario);

                entity.Property(e => e.xTipoActividadHorario)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<isoTipoCalculo>(entity =>
            {
                entity.HasKey(e => e.bTipoCalculo)
                    .HasName("PK_remTipoCalculo");

                entity.Property(e => e.bTipoCalculo).HasComment("1:Remunerar, 2:Provisionar");

                entity.Property(e => e.xTipoCalculo)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<isoTipoComprobante>(entity =>
            {
                entity.HasKey(e => e.bTipoComprobante);

                entity.Property(e => e.bTipoComprobante).HasComment("1:R=RECIBO POR HONORARIOS,2:N=NOTA DE CRÉDITO,3:D=DIETA,4:O=OTRO COMPROBANTE");

                entity.Property(e => e.xCodigo)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.xTipoComprobante)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<isoTipoConcepto>(entity =>
            {
                entity.HasKey(e => e.bTipoConcepto)
                    .HasName("PK_remTipoConcepto");

                entity.Property(e => e.bTipoConcepto).HasComment("1:BD, 2:Manual, 3:Fijo, 4:Asistencia (mov.), 5:Préstamo, 6:Formula, 7:Historico, 8:Acumulador, 9:Importe");

                entity.Property(e => e.xTipoConcepto)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasComment("1:BD, 2:Manual, 3:Fijo, 4:Asistencia (mov.), 5:Préstamo, 6:Formula, 7:Historico, 8:Acumulador, 9:Importe");
            });

            modelBuilder.Entity<isoTipoContrato>(entity =>
            {
                entity.HasKey(e => e.bTipoContrato);

                entity.Property(e => e.xTipoContrato)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<isoTipoCtaBancaria>(entity =>
            {
                entity.HasKey(e => e.bTipoCtaBancaria);

                entity.Property(e => e.xTipoCtaBancaria)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<isoTipoDocIdentidad>(entity =>
            {
                entity.HasKey(e => e.bTipoDocIdentidad);

                entity.Property(e => e.xTip)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.xTipoDocIdentidad)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<isoTipoPago>(entity =>
            {
                entity.HasKey(e => e.bTipoPago);

                entity.Property(e => e.xTipoPago)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<isoTipoPlanilla>(entity =>
            {
                entity.HasKey(e => e.bTipoPlanilla);

                entity.Property(e => e.xTipoPlanilla)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

            });

            modelBuilder.Entity<isoTipoSuspensionRL>(entity =>
            {
                entity.HasKey(e => e.bTipoSuspensionRL);

                entity.Property(e => e.bTipoSuspensionRL).HasComment("Código de la SUNAT, agregar formato 0#");

                entity.Property(e => e.xTipoSuspensionRL)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<isoTipoTrabajador>(entity =>
            {
                entity.HasKey(e => e.bTipoTrabajador);

                entity.Property(e => e.xTipoTrabajador)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<isoTipoVia>(entity =>
            {
                entity.HasKey(e => e.bTipoVia);

                entity.Property(e => e.xAbreviado)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.xTipoVia)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<isoTipoZona>(entity =>
            {
                entity.HasKey(e => e.bTipoZona);

                entity.Property(e => e.xAbreviado)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.xTipoZona)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<isoUbicacion>(entity =>
            {
                entity.HasKey(e => e.bUbicacion);

                entity.Property(e => e.bUbicacion)
                    .HasComment("Casa, Oficina, Familiar, Vecino, Vacaciones, etc.")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.xUbicacion)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<isoUnidadMedida>(entity =>
            {
                entity.HasKey(e => e.sUnidadMedida);

                entity.Property(e => e.xAbreviado)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.xUnidadMedida)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<isoVinculoFamiliar>(entity =>
            {
                entity.HasKey(e => e.bVinculoFamiliar);

                entity.Property(e => e.bVinculoFamiliar).HasComment("(Tabla 19) Vínculo familiar");

                entity.Property(e => e.xVinculoFamiliar)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });


            modelBuilder.Entity<orgArea>(entity =>
            {
                entity.HasKey(e => e.sArea);

                entity.Property(e => e.xArea)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

            });

            modelBuilder.Entity<orgCargo>(entity =>
            {
                entity.HasKey(e => e.sCargo);

                entity.Property(e => e.xCargo)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

            });

            modelBuilder.Entity<orgCargoFemenino>(entity =>
            {
                entity.HasKey(e => e.sCargo);

                entity.Property(e => e.sCargo).ValueGeneratedNever();

                entity.Property(e => e.xCargo)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

            });

            modelBuilder.Entity<orgCargoFuncion>(entity =>
            {
                entity.HasKey(e => e.iCargoFuncion);

                entity.Property(e => e.xFuncion)
                    .IsRequired()
                    .HasMaxLength(300)
                    .IsUnicode(false);

            });

            modelBuilder.Entity<orgCargoServicio>(entity =>
            {
                entity.HasKey(e => e.sCargo);

                entity.Property(e => e.sCargo).ValueGeneratedNever();

                entity.Property(e => e.xServicios)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

            });

            modelBuilder.Entity<orgCentroCosto>(entity =>
            {
                entity.HasKey(e => e.sCentroCosto);

                entity.Property(e => e.xCentroCosto)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

            });

            modelBuilder.Entity<orgCentroCostoCodigo>(entity =>
            {
                entity.HasKey(e => e.sCentroCosto);

                entity.Property(e => e.sCentroCosto).ValueGeneratedNever();

                entity.Property(e => e.xCodigo)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

            });

            modelBuilder.Entity<orgCentroCostoRelacion>(entity =>
            {
                entity.HasKey(e => new { e.sCentroCosto, e.sCentroCostoRelacion });

                entity.Property(e => e.sCentroCostoRelacion).HasComment("Código del nivel superior al actual con el que se relaciona");

            });

            modelBuilder.Entity<orgDiccCargoSunat>(entity =>
            {
                entity.HasKey(e => new { e.sCargo, e.cOcupacion });

                entity.Property(e => e.cOcupacion)
                    .HasMaxLength(6)
                    .IsUnicode(false)
                    .IsFixedLength();

            });

            modelBuilder.Entity<orgEmpresa>(entity =>
            {
                entity.HasKey(e => e.sEmpresa);

                entity.HasIndex(e => e.cRUC)
                    .HasName("IX_orgEmpresa")
                    .IsUnique();

                entity.Property(e => e.bRegimenLaboral)
                    .HasDefaultValueSql("((1))")
                    .HasComment("(Tabla 33) 01:Privado general - DL. N°728");

                entity.Property(e => e.cRUC)
                    .IsRequired()
                    .HasMaxLength(11)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.xAbreviado)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.xNombreComercial)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.xRazonSocial)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

            });

            modelBuilder.Entity<orgEmpresaActividad>(entity =>
            {
                entity.HasKey(e => e.sEmpresa);

                entity.Property(e => e.sEmpresa).ValueGeneratedNever();

                entity.Property(e => e.dInicio).HasColumnType("date");

                entity.Property(e => e.xActividad).IsUnicode(false);

            });

            modelBuilder.Entity<orgEmpresaCalculoCtaContableNeto>(entity =>
            {
                entity.HasKey(e => new { e.sEmpresa, e.bCalculo });

            });

            modelBuilder.Entity<orgEmpresaCtaBancaria>(entity =>
            {
                entity.HasKey(e => e.sEmpresaCtaBancaria);

                entity.Property(e => e.bTipoCtaBancaria).HasDefaultValueSql("((1))");

                entity.Property(e => e.sMoneda).HasDefaultValueSql("((604))");

                entity.Property(e => e.xCtaBancaria)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

            });

            modelBuilder.Entity<orgEmpresaCtaContable>(entity =>
            {
                entity.HasKey(e => e.sCuenta);

                entity.HasIndex(e => new { e.sEmpresa, e.xCuenta })
                    .HasName("IX_orgEmpresaCtaContable");

                entity.Property(e => e.xCuenta)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.xNombre)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

            });

            modelBuilder.Entity<orgEmpresaDireccion>(entity =>
            {
                entity.HasKey(e => e.sEmpresa);

                entity.Property(e => e.sEmpresa).ValueGeneratedNever();

                entity.Property(e => e.bTipoVia).HasComment("Calle, Jirón, Avenida, etc.");

                entity.Property(e => e.xBlock)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xDpto)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xEtapa)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xInterior)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xKilometro)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xLote)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xManzana)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xNombreVia)
                    .HasMaxLength(300)
                    .IsUnicode(false)
                    .HasComment("Aqui guarda el concatenado, si no se pudo migrar por separado");

                entity.Property(e => e.xNombreZona)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.xNroVia)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xReferencia)
                    .HasMaxLength(100)
                    .IsUnicode(false);

            });

            modelBuilder.Entity<orgEmpresaImagen>(entity =>
            {
                entity.HasKey(e => e.sEmpresaImagen);

                entity.Property(e => e.bTipoImagen).HasComment("1:Logo,2:Firma,3:Sello agua");

                entity.Property(e => e.iImagen).HasColumnType("image");

                entity.Property(e => e.jImagen).IsUnicode(false);

            });

            modelBuilder.Entity<orgEmpresaPeriodo>(entity =>
            {
                entity.HasKey(e => e.sEmpresa)
                    .HasName("PK_orgEmpresaEstado");

                entity.Property(e => e.sEmpresa).ValueGeneratedNever();

                entity.Property(e => e.dDesde).HasColumnType("date");

                entity.Property(e => e.dHasta).HasColumnType("date");

            });

            modelBuilder.Entity<orgEmpresaPlanEPS>(entity =>
            {
                entity.HasKey(e => e.sEmpresaPlanEPS);

            });

            modelBuilder.Entity<orgEmpresaRepLegal>(entity =>
            {
                entity.HasKey(e => e.sEmpresa);

                entity.Property(e => e.sEmpresa).ValueGeneratedNever();

                entity.Property(e => e.xPartida)
                    .HasMaxLength(50)
                    .IsUnicode(false);

            });

            modelBuilder.Entity<orgEmpresaSede>(entity =>
            {
                entity.HasKey(e => e.iEmpresaSede);
            });

            modelBuilder.Entity<orgEmpresaSedeDireccion>(entity =>
            {
                entity.HasKey(e => e.iEmpresaSede);

                entity.Property(e => e.iEmpresaSede).ValueGeneratedNever();

                entity.Property(e => e.bTipoVia).HasComment("Calle, Jirón, Avenida, etc.");

                entity.Property(e => e.xBlock)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xDpto)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xEtapa)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xInterior)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xKilometro)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xLote)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xManzana)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xNombreVia)
                    .HasMaxLength(300)
                    .IsUnicode(false)
                    .HasComment("Aqui guarda el concatenado, si no se pudo migrar por separado");

                entity.Property(e => e.xNombreZona)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.xNroVia)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xReferencia)
                    .HasMaxLength(100)
                    .IsUnicode(false);

            });

            modelBuilder.Entity<orgEmpresaTelefono>(entity =>
            {
                entity.HasKey(e => new { e.sEmpresa, e.xTelefono });

                entity.Property(e => e.xTelefono)
                    .HasMaxLength(20)
                    .IsUnicode(false);

            });

            modelBuilder.Entity<orgEstructuraSalarial>(entity =>
            {
                entity.HasKey(e => e.sEstructuraSalarial);

                entity.Property(e => e.bCantidadTrabajadores).HasDefaultValueSql("((1))");

                entity.Property(e => e.bDiasEfectivos).HasDefaultValueSql("((26))");

                entity.Property(e => e.bDiasTotal).HasDefaultValueSql("((30))");

                entity.Property(e => e.bHrsExt_diasEfectiv)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.bHrsExt_incluyeAsigFam)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.mSubTotal)
                    .HasColumnType("money")
                    .HasDefaultValueSql("((0.00))");

                entity.Property(e => e.xEstructuraSalarial)
                    .HasMaxLength(500)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<orgEstructuraSalarialDet>(entity =>
            {
                entity.HasKey(e => e.iEstructuraSalarialDet);

                entity.Property(e => e.mMonto).HasColumnType("money");

            });

            modelBuilder.Entity<orgNivelCargo>(entity =>
            {
                entity.HasKey(e => e.bNivel)
                    .HasName("PK_orgNivel");

                entity.Property(e => e.xNivel)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<orgNivelCentroCosto>(entity =>
            {
                entity.HasKey(e => e.bNivel);

                entity.Property(e => e.xNivel)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

            });

            modelBuilder.Entity<orgProyecto>(entity =>
            {
                entity.HasKey(e => e.sProyecto);

                entity.Property(e => e.dFechaReg).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.xDescripcion).IsUnicode(false);

                entity.Property(e => e.xProyecto)
                    .HasMaxLength(50)
                    .IsUnicode(false);

            });

            modelBuilder.Entity<orgSede>(entity =>
            {
                entity.HasKey(e => e.sSede);

                entity.Property(e => e.CodEstablecimiento)
                    .HasDefaultValueSql("((1))")
                    .HasComment("(Estructura 1) Código de establecimiento");

                entity.Property(e => e.bCentroRiesgo).HasComment("(Estructura 1) 0 = No es Centro de Riesgo y  1= Es Centro de Riesgo.");

                entity.Property(e => e.xSede)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<perArchivo>(entity =>
            {
                entity.HasKey(e => e.iArchivo);

                entity.Property(e => e.Archivo).HasColumnType("image");

                entity.Property(e => e.dDesde).HasColumnType("date");

                entity.Property(e => e.dFechaReg)
                    .HasColumnType("datetime2(0)")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.dHasta).HasColumnType("date");

                entity.Property(e => e.iIdOrigenArchivo).HasComment("Por defecto es el código de Persona, pero podría ser contrato, etc.");

                entity.Property(e => e.sOrigenArchivo).HasComment("1: Contrato periodo, 2: CV, etc.");

            });

            modelBuilder.Entity<perBuscar>(entity =>
            {
                entity.HasKey(e => e.iPersona)
                    .HasName("PK_PersonaBuscar");

                entity.Property(e => e.iPersona).ValueGeneratedNever();

                entity.Property(e => e.xAlias)
                    .HasMaxLength(50)
                    .IsUnicode(false);

            });

            modelBuilder.Entity<perCtaBancaria>(entity =>
            {
                entity.HasKey(e => e.iCtaBancaria);

                entity.Property(e => e.bTipoCtaBancaria)
                    .HasDefaultValueSql("((1))")
                    .HasComment("1: HABERES, 2:CTA");

                entity.Property(e => e.sMoneda).HasDefaultValueSql("((604))");

                entity.Property(e => e.xCtaBancaria)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.xCtaInterBancaria)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<perDireccion>(entity =>
            {
                entity.HasKey(e => e.iDireccion);

                entity.Property(e => e.bPrioridad).HasDefaultValueSql("((10))");

                entity.Property(e => e.bTipoVia).HasComment("Calle, Jirón, Avenida, etc.");

                entity.Property(e => e.bUbicacion)
                    .HasDefaultValueSql("((1))")
                    .HasComment("Casa, Oficina, Familiar, Vecino, Vacaciones, etc.");

                entity.Property(e => e.xBlock)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xDpto)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xEtapa)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xInterior)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xKilometro)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xLote)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xManzana)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xNombreVia)
                    .HasMaxLength(300)
                    .IsUnicode(false)
                    .HasComment("Aqui gusrda el concatenado, si no se pudo migrar por separado");

                entity.Property(e => e.xNombreZona)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.xNroVia)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xReferencia)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<perDireccionCanonica>(entity =>
            {
                entity.HasKey(e => e.bDireccion);

                entity.Property(e => e.bDireccion)
                    .HasComment("Casa, Oficina, Familiar, Vecino, Vacaciones, etc.")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.xDireccion)
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<perEducacion>(entity =>
            {
                entity.HasKey(e => e.iEducacion);

                entity.Property(e => e.cPais)
                    .IsRequired()
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.dDesde).HasColumnType("date");

                entity.Property(e => e.dHasta).HasColumnType("date");

                entity.Property(e => e.xTitulo)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<perFamiliar>(entity =>
            {
                entity.HasKey(e => e.iFamiliar);
            });

            modelBuilder.Entity<perFamiliarBajaDH>(entity =>
            {
                entity.HasKey(e => e.iFamiliar)
                    .HasName("PK_isoFamiliarBaja");

                entity.Property(e => e.iFamiliar).ValueGeneratedNever();
            });

            modelBuilder.Entity<perFamiliarDocSustentaVinculo>(entity =>
            {
                entity.HasKey(e => e.iFamiliar);

                entity.Property(e => e.iFamiliar).ValueGeneratedNever();
            });

            modelBuilder.Entity<perFono>(entity =>
            {
                entity.HasKey(e => e.iFono)
                    .HasName("PK_PersonaFono_1");

                entity.Property(e => e.bPrioridad)
                    .HasDefaultValueSql("((10))")
                    .HasComment("Por ejemplo Prioridad 1 es su casa, los demás no son tan importantes");

                entity.Property(e => e.xDescripcion)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.xNumeroFono)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<perFoto>(entity =>
            {
                entity.HasKey(e => e.iFoto)
                    .HasName("PK_PersonaFoto");

                entity.Property(e => e.Foto).HasColumnType("image");

                entity.Property(e => e.jFoto).IsUnicode(false);
            });

            modelBuilder.Entity<perNacionalidad>(entity =>
            {
                entity.HasKey(e => e.iPersona)
                    .HasName("PK_perPersonaNacionalidad");

                entity.Property(e => e.iPersona).ValueGeneratedNever();
            });

            modelBuilder.Entity<perPaisEmisorDocIdentidad>(entity =>
            {
                entity.HasKey(e => new { e.iPersona, e.sPais });
            });

            modelBuilder.Entity<perPersona>(entity =>
            {
                entity.HasKey(e => e.iPersona)
                    .HasName("PK_Persona");

                entity.Property(e => e.bGenero).HasComment("Identifica el genero (Masculino/Femenino)");

                entity.Property(e => e.dNacimiento)
                    .HasColumnType("date")
                    .HasComment("Fecha de nacimiento");

                entity.Property(e => e.xApellidoMaterno)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.xApellidoPaterno)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.xConcatenado)
                    .HasMaxLength(229)
                    .IsUnicode(false)
                    .HasComputedColumnSql("(((((((([xDocIdentidad]+' ')+[xApellidoPaterno])+' ')+[xApellidoMaterno])+' ')+[xNombres])+' ')+CONVERT([char](10),[dNacimiento],(103)))");

                entity.Property(e => e.xDocIdentidad)
                    .IsRequired()
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.xNombres)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<perPersonaCodigo>(entity =>
            {
                entity.HasKey(e => e.iPersona);

                entity.Property(e => e.iPersona).ValueGeneratedNever();

                entity.Property(e => e.xCodigo)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<perTipoFoto>(entity =>
            {
                entity.HasKey(e => e.bTipoFoto);

                entity.Property(e => e.bTipoFoto).ValueGeneratedOnAdd();

                entity.Property(e => e.xTipoFoto)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<perUsuario>(entity =>
            {
                entity.HasKey(e => e.iUsuario);

                entity.Property(e => e.iUsuario).ValueGeneratedNever();

                entity.Property(e => e.xClave).HasMaxLength(50);

                entity.Property(e => e.xUsuario)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<perVistaContractual>(entity => {
                entity.HasKey(e => new { e.iPersona, e.sEmpresa, e.iContrato, e.iContratoPeriodo });
            });

            modelBuilder.Entity<perUsuarioAccesoEmpresa>(entity =>
            {
                entity.HasKey(e => e.iUsuarioEmpresa);
            });

            modelBuilder.Entity<perUsuarioAccesoTab>(entity =>
            {
                entity.HasKey(e => e.iUsuarioAcceso);

                entity.HasIndex(e => new { e.iUsuario, e.iTab })
                    .HasName("IX_perUsuarioAccesoTab")
                    .IsUnique();
            });

            modelBuilder.Entity<perUsuarioSuper>(entity =>
            {
                entity.HasKey(e => e.iUsuario);

                entity.Property(e => e.iUsuario).ValueGeneratedNever();
            });

            modelBuilder.Entity<perUsuarioVigencia>(entity =>
            {
                entity.HasKey(e => e.iUsuario);

                entity.Property(e => e.iUsuario).ValueGeneratedNever();

                entity.Property(e => e.dDesde).HasColumnType("date");

                entity.Property(e => e.dHasta).HasColumnType("date");
            });

            modelBuilder.Entity<remBoletaCabecera>(entity =>
            {
                entity.HasKey(e => e.sEmpresa);
            });
            modelBuilder.Entity<remBoletaCabeceraEmpresa>(entity =>
            {
                entity.HasKey(e => e.xDato);
            });
            modelBuilder.Entity<remBoletaCabeceraTitulo>(entity =>
            {
                entity.HasKey(e => e.xDato);
            });
            modelBuilder.Entity<remBoletaCabeceraAsistenciaFechas>(entity =>
            {
                entity.HasKey(e => e.Fecha01);
            });
            modelBuilder.Entity<remBoletaConcepto>(entity =>
            {
                entity.HasKey(e => e.sOrdenMostrar);
            });

            modelBuilder.Entity<remConcepto>(entity =>
            {
                entity.HasKey(e => e.sConcepto);

                entity.Property(e => e.bClaseConcepto).HasComment("0:Info,1:Asistencia,2:Ingresos,3:Descuentos,4:Empleador");

                entity.Property(e => e.bTipoConceptoDefault)
                    .HasDefaultValueSql("((1))")
                    .HasComment("0:Sistema,1:Manual,2:Fórmula,3:Sumado,etc.");

                entity.Property(e => e.cConcepto)
                    .IsRequired()
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .IsFixedLength()
                    .HasComment("S");

                entity.Property(e => e.cConceptoSunat)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength()
                    .HasComment("Código PLAME");

                entity.Property(e => e.xConcepto)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("Descripción para boleta");

                entity.Property(e => e.xDescripcion)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasComment("Descripción larga para LBS");

                entity.Property(e => e.xTip)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasComment("Descripción corta para columnas de reportes u otros");
            });

            modelBuilder.Entity<remConceptoAsistencia>(entity =>
            {
                entity.HasKey(e => e.sConcepto);

                entity.Property(e => e.sConcepto).ValueGeneratedNever();

                entity.Property(e => e.iColor).HasDefaultValueSql("((16777215))");
            });

            modelBuilder.Entity<remConceptoAsistenciaEmpresaPlanilla>(entity =>
            {
                entity.HasKey(e => new { e.sConcepto, e.sEmpresa, e.bPlanilla });
            });

            modelBuilder.Entity<remConceptoAsistenciaPlanilla>(entity =>
            {
                entity.HasKey(e => new { e.sConcepto, e.bPlanilla });

                entity.Property(e => e.bMaxDias).HasDefaultValueSql("((15))");

            });

            modelBuilder.Entity<remConceptoCalculo>(entity =>
            {
                entity.HasKey(e => e.iConceptoCalculo);

                entity.HasIndex(e => new { e.sEmpresa, e.bCalculo, e.bPlanilla, e.sConcepto })
                    .HasName("IX_remConceptoCalculo")
                    .IsUnique();

                entity.Property(e => e.bAfecta).HasComment("Interviene en la boleta: Se muestra y se suma");

                entity.Property(e => e.bCalculo)
                    .HasDefaultValueSql("((2))")
                    .HasComment("1:Adelanto, 2:Boleta, 3:Liquidación, 4:Vacaciones, 5:Gratificaciones, 6:CTS, 7:Utilidades");

                entity.Property(e => e.bPlanilla).HasComment("1:Empleados, etc.");

                entity.Property(e => e.bTipoConcepto)
                    .HasDefaultValueSql("((1))")
                    .HasComment("0:Sistema,1:Manual,2:Fórmula,3:Sumado,4:Promedio,etc.");

                entity.Property(e => e.sConcepto).HasComment("Ejemplo: S0001, A1010, I1010, D2000, E3000");

                entity.Property(e => e.sOrdenCalculo).HasComment("Ordena en que se resolverá formulas");

                entity.Property(e => e.sOrdenMostrar).HasComment("Orden e que se mostrará en la boleta y reportes");

            });

            modelBuilder.Entity<remConceptoCalculoAfpCuentas>(entity =>
            {
                entity.HasKey(e => new { e.iConceptoCalculo, e.bRegimenPensionario });

                entity.Property(e => e.bMostrar).HasComment("0= Total, 1=Centro de costo, 2=Trabajador");

            });

            modelBuilder.Entity<remConceptoCalculoAsistencia>(entity =>
            {
                entity.HasKey(e => e.iConceptoCalculo);

                entity.Property(e => e.iConceptoCalculo).ValueGeneratedNever();

                entity.Property(e => e.sUnidadMedida).HasComment("Unidad de medida que traerá como resultado en el cálculo, es decir, que transformará la unidad de medida si este es diferente al que está en el concepto.");

            });

            modelBuilder.Entity<remConceptoCalculoCuentas>(entity =>
            {
                entity.HasKey(e => e.iConceptoCalculo);

                entity.Property(e => e.iConceptoCalculo).ValueGeneratedNever();

                entity.Property(e => e.bMostrarDebe).HasComment("0= Total, 1=Centro de costo, 2=Trabajador");
            });

            modelBuilder.Entity<remConceptoCalculoDependencia>(entity =>
            {
                entity.HasKey(e => new { e.iConceptoCalculo, e.sConcepto });

                entity.Property(e => e.sConcepto).HasComment("Concepto del cual depende para calcularlo. Ej: D6090 Renta 5ta, depende que haya D5510 Renta neta anual para calcularse.");
            });

            modelBuilder.Entity<remConceptoCalculoDias>(entity =>
            {
                entity.HasKey(e => e.iConceptoCalculo);

                entity.Property(e => e.iConceptoCalculo).ValueGeneratedNever();
            });

            modelBuilder.Entity<remConceptoCalculoFormula>(entity =>
            {
                entity.HasKey(e => e.iConceptoCalculo);

                entity.Property(e => e.iConceptoCalculo).ValueGeneratedNever();

                entity.Property(e => e.xFormula)
                    .IsRequired()
                    .HasMaxLength(1000)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<remConceptoCalculoHistory>(entity =>
            {
                entity.HasKey(e => e.iConceptoCalculo);

                entity.Property(e => e.iConceptoCalculo).ValueGeneratedNever();

                entity.Property(e => e.bActualConsiderar).HasComment("0:Periodo actual,1:Mes anterior,2:Número anterior,3:Mes actual si hay monto,4:Número actual si hay monto,5:Mes actual si cesó último día del mes,6:Mes actual si cesó último día del periodo actual,7:Mes actual si cesó en día especifico:");

                entity.Property(e => e.bAfecta).HasComment("0:Todas,1:Solo si es afecto,2:Solo si no es afecto");

                entity.Property(e => e.bCalculo).HasComment("0:Todos de Remunerar, 10:Todos de Provisionar");

                entity.Property(e => e.bDivideEntre).HasDefaultValueSql("((1))");

                entity.Property(e => e.bFuente).HasComment("1:Concepto,2:Total ingresos,3:Total descuentos,4:Neto,5:Total empleador");

                entity.Property(e => e.bIngreso).HasComment("1:Último ingreso a la empresa,2:Todos los ingresos de la empresa,3:Todas las empresas");

                entity.Property(e => e.bMinConsiderar).HasDefaultValueSql("((1))");

                entity.Property(e => e.bTipoAnos).HasComment("0:Todos los años,1:Año actual,2:Años anteriores");

                entity.Property(e => e.bTipoInfo).HasComment("1:Sumar,2:Dividir,3:Promediar entre meses hay,4:Promediar meses rango,5:Promediar entre números hay,6:Promediar entre números rango,7:Meses hay monto,8:Meses rango,9:Números hay monto,10:Números rango");

                entity.Property(e => e.bTipoPeriodos).HasComment("0:Año completo,1:Mes actual,2:Mes especifico,3:Meses anteriores,4:Número actual,5:Número especifico,6:Números anteriores,7:Periodo de Vacaciones,8:Periodo de gratificaciones,9:Periodo de CTS,10:Desde ingreso");
            });

            modelBuilder.Entity<remConceptoCalculoManual>(entity =>
            {
                entity.HasKey(e => e.iConceptoCalculo);

                entity.Property(e => e.iConceptoCalculo).ValueGeneratedNever();
            });

            modelBuilder.Entity<remConceptoCalculoMonto>(entity =>
            {
                entity.HasKey(e => e.iConceptoCalculo);

                entity.Property(e => e.iConceptoCalculo).ValueGeneratedNever();

                entity.Property(e => e.mMonto).HasColumnType("money");
            });

            modelBuilder.Entity<remConceptoCalculoSumado>(entity =>
            {
                entity.HasKey(e => new { e.iConceptoCalculo, e.sConcepto });
            });

            modelBuilder.Entity<remConceptoCalculoVigencia>(entity =>
            {
                entity.HasKey(e => e.iConceptoCalculo);

                entity.Property(e => e.iConceptoCalculo).ValueGeneratedNever();

                entity.Property(e => e.dDesde).HasColumnType("date");

                entity.Property(e => e.dHasta).HasColumnType("date");
            });

            modelBuilder.Entity<remConceptoSubClase>(entity =>
            {
                entity.HasKey(e => e.sConcepto)
                    .HasName("PK_remConceptoSubClase_1");

                entity.Property(e => e.sConcepto).ValueGeneratedNever();
            });

            modelBuilder.Entity<remConceptoTipoSuspensionRL>(entity =>
            {
                entity.HasKey(e => e.sConcepto);

                entity.Property(e => e.sConcepto).ValueGeneratedNever();
            });

            modelBuilder.Entity<remImportaPlanilla>(entity =>
            {
                entity.HasKey(e => new { e.bCalculo, e.iPersona, e.sEmpresa, e.sAno, e.bMes, e.bNumero, e.bPlanilla, e.sConcepto });

                entity.Property(e => e.mMonto).HasColumnType("money");
            });

            modelBuilder.Entity<remImportaPlanillaColumna>(entity =>
            {
                entity.HasKey(e => new { e.bCalculo, e.sConcepto });

                entity.HasIndex(e => new { e.bCalculo, e.bColumna })
                    .HasName("IX_remImportaPlanillaColumna")
                    .IsUnique();
            });

            modelBuilder.Entity<remPeriodo>(entity =>
            {
                entity.HasKey(e => e.iPeriodo);

                entity.HasIndex(e => new { e.sEmpresa, e.bCalculo, e.bPlanilla, e.iAnoMes, e.bNumero })
                    .HasName("IX_remPeriodo")
                    .IsUnique();

                entity.Property(e => e.bMes).HasComputedColumnSql("(CONVERT([tinyint],right([iAnoMes],(2)),(0)))");

                entity.Property(e => e.bNotificado).HasComment("1: Los trabajadores podrán ver su boleta en la web, y se enviará notificaciones a los trabajadores de esto.");

                entity.Property(e => e.dDesde).HasColumnType("date");

                entity.Property(e => e.dDesdeAsistencia).HasColumnType("date");

                entity.Property(e => e.dHasta).HasColumnType("date");

                entity.Property(e => e.dHastaAsistencia).HasColumnType("date");

                entity.Property(e => e.iAnoNumero)
                    .HasComment("YYYYnn")
                    .HasComputedColumnSql("(CONVERT([int],left([iAnoMes],(4))+right('0'+CONVERT([varchar](2),[bNumero],(0)),(2)),(0)))");

                entity.Property(e => e.sAno).HasComputedColumnSql("(CONVERT([smallint],left([iAnoMes],(4)),(0)))");

                entity.Property(e => e.sEmpresa).HasDefaultValueSql("((1))");
            });

            modelBuilder.Entity<remPlanilla>(entity =>
            {
                entity.HasKey(e => e.bPlanilla);

                entity.Property(e => e.bPlanilla).ValueGeneratedOnAdd();

                entity.Property(e => e.bDeclarar).HasDefaultValueSql("((1))");

                entity.Property(e => e.bTipoPlanilla).HasComment("1: Empleado, 2: Obrero Regimen común (Mensual), 3: Obrero Regimen común (Semanal), 4: Practicante, 5: Obrero Construcción Civil (Semanal), 6:CAS");

                entity.Property(e => e.xPlanilla)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<traContratosxPersona>(entity =>
            {
                entity.HasKey(e => e.iContrato);
            });

            modelBuilder.Entity<traPrestamo>(entity =>
            {
                entity.HasKey(e => e.iPrestamo);
            });

            modelBuilder.Entity<sysAccesoMenu>(entity =>
            {
                entity.HasKey(e => e.sMenu);

                entity.Property(e => e.sMenu).ValueGeneratedNever();

                entity.Property(e => e.xMenu)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.xNombre)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<sysAccesoTab>(entity =>
            {
                entity.HasKey(e => e.iTab);

                entity.Property(e => e.iTab).ValueGeneratedNever();

                entity.Property(e => e.bAcceso)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.bAgregar)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.bEditar)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.bEliminar)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.xNombre)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<sysAdministrador>(entity =>
            {
                entity.HasKey(e => e.iPersona);

                entity.Property(e => e.iPersona).ValueGeneratedNever();

                entity.Property(e => e.dVigencia).HasColumnType("date");
            });

            modelBuilder.Entity<sysArchivo>(entity =>
            {
                entity.HasKey(e => e.sArchivo);

                entity.Property(e => e.sArchivo).ValueGeneratedNever();

                entity.Property(e => e.bEstado)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.xArchivo)
                    .IsRequired()
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.xExtension)
                    .IsRequired()
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('TXT')");

                entity.Property(e => e.xObjectDw)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<sysConcepto>(entity =>
            {
                entity.HasKey(e => e.sSysConcepto)
                    .HasName("PK_SysConcepto");

                entity.Property(e => e.sSysConcepto).ValueGeneratedNever();

                entity.Property(e => e.xDescripcion)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.xSysConcepto)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<sysConfig>(entity =>
            {
                entity.HasKey(e => e.sConfig);

                entity.Property(e => e.sConfig).ValueGeneratedNever();

                entity.Property(e => e.xConfig)
                    .IsRequired()
                    .HasMaxLength(59)
                    .IsUnicode(false);

                entity.Property(e => e.xDato)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<sysContratoFormatoCampo>(entity =>
            {
                entity.HasKey(e => e.sCampo);

                entity.Property(e => e.sCampo).ValueGeneratedNever();

                entity.Property(e => e.xCampo)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.xDescripcion)
                    .IsRequired()
                    .IsUnicode(false);
            });

            modelBuilder.Entity<sysEpsPlantilla>(entity =>
            {
                entity.HasKey(e => e.bEpsPlantilla);

                entity.Property(e => e.bAumentarIGV)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.bContarParaCalculo)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.bContarParaFactor)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.xEpsPlantilla)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<sysGrupoArchivo>(entity =>
            {
                entity.HasKey(e => e.bGrupoArchivo);

                entity.Property(e => e.xGrupoArchivo)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<sysGrupoConfig>(entity =>
            {
                entity.HasKey(e => e.bGrupoConfig);

                entity.Property(e => e.xGrupoConfig)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<sysGrupoReporte>(entity =>
            {
                entity.HasKey(e => e.bGrupoReporte);

                entity.Property(e => e.xGrupoReporte)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<sysMonto>(entity =>
            {
                entity.HasKey(e => e.sSysMonto)
                    .HasName("PK_SysMonto");

                entity.Property(e => e.sSysMonto).ValueGeneratedNever();

                entity.Property(e => e.dMonto).HasColumnType("decimal(18, 6)");

                entity.Property(e => e.xDescripcion)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.xSysMonto)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<sysMontoAnual>(entity =>
            {
                entity.HasKey(e => new { e.sSysMonto, e.sAno })
                    .HasName("PK_SysMontoAnual");

                entity.Property(e => e.dMonto).HasColumnType("decimal(18, 6)");
            });

            modelBuilder.Entity<sysMontoEmpresa>(entity =>
            {
                entity.HasKey(e => new { e.sEmpresa, e.sSysMonto })
                    .HasName("PK_SysMontoEmpresa");

                entity.Property(e => e.dMonto).HasColumnType("decimal(18, 6)");
            });

            modelBuilder.Entity<sysMontoFecha>(entity =>
            {
                entity.HasKey(e => new { e.sSysMonto, e.dDesde });

                entity.Property(e => e.dDesde).HasColumnType("date");

                entity.Property(e => e.dMonto).HasColumnType("decimal(18, 6)");

            });

            modelBuilder.Entity<sysOrigenArchivo>(entity =>
            {
                entity.HasKey(e => e.sOrigenArchivo);

                entity.Property(e => e.sOrigenArchivo)
                    .HasComment("Mostrar en el combo para seleccionar en el mantenimiento")
                    .ValueGeneratedNever();

                entity.Property(e => e.dFechaReg).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.xOrigenArchivo)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.xTabla)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<sysPistaAuditoria>(entity =>
            {
                entity.HasKey(e => e.iPistaAuditoria);

                entity.Property(e => e.bAccion).HasComment("1:Insert, 2: Update, 3: Delete");

                entity.Property(e => e.dtFechaHora)
                    .HasColumnType("datetime2(0)")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.xDato)
                    .IsRequired()
                    .IsUnicode(false);
            });

            modelBuilder.Entity<sysPlanEPS>(entity =>
            {
                entity.HasKey(e => e.sPlanEPS);

                entity.Property(e => e.dFechaReg)
                    .HasColumnType("datetime2(0)")
                    .HasDefaultValueSql("(getdate())")
                    .HasComment("Fecha y hora del último afecto al registro (insertó o modificó)");

                entity.Property(e => e.iUsuarioReg).HasComment("Último Usuario que afecto al registro (insertó o modificó)");

                entity.Property(e => e.xPlanEPS)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<sysPlanEPSdependencia>(entity =>
            {
                entity.HasKey(e => e.sPlanEPS);

                entity.Property(e => e.sPlanEPS).ValueGeneratedNever();

            });

            modelBuilder.Entity<sysPlanEPSdet>(entity =>
            {
                entity.HasKey(e => e.sPlanEPSdet);

                entity.Property(e => e.bEpsPlantilla).HasComment("1:Titular, 2:Conyuge, 3:Hijos <, 4:Hijos >, 5:Padres, 6:Padres mayores");

                entity.Property(e => e.dFechaReg)
                    .HasColumnType("datetime2(0)")
                    .HasDefaultValueSql("(getdate())")
                    .HasComment("Fecha y hora del último afecto al registro (insertó o modificó)");

                entity.Property(e => e.iUsuarioReg).HasComment("Último Usuario que afecto al registro (insertó o modificó)");

                entity.Property(e => e.mValor).HasColumnType("money");

            });

            modelBuilder.Entity<sysReporte>(entity =>
            {
                entity.HasKey(e => e.sReporte);

                entity.Property(e => e.sReporte).ValueGeneratedNever();

                entity.Property(e => e.bEstado)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.xObjectDw)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.xReporte)
                    .IsRequired()
                    .HasMaxLength(1000)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<sysTexto>(entity =>
            {
                entity.HasKey(e => e.sSysTexto)
                    .HasName("PK_SysTexto");

                entity.Property(e => e.sSysTexto).ValueGeneratedNever();

                entity.Property(e => e.xDescripcion)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.xSysTexto)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.xTexto)
                    .IsRequired()
                    .IsUnicode(false);
            });

            modelBuilder.Entity<terEmpresa>(entity =>
            {
                entity.HasKey(e => e.sEmpresa);

                entity.HasIndex(e => e.cRUC)
                    .HasName("IX_terEmpresa")
                    .IsUnique();

                entity.Property(e => e.cRUC)
                    .IsRequired()
                    .HasMaxLength(11)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.xAbreviado)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.xNombreComercial)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.xRazonSocial)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<terEmpresaAseguradoraSalud>(entity =>
            {
                entity.HasKey(e => e.sEmpresa);

                entity.Property(e => e.sEmpresa).ValueGeneratedNever();

                entity.Property(e => e.dFechaReg)
                    .HasColumnType("datetime2(0)")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<terEmpresaCliente>(entity =>
            {
                entity.HasKey(e => e.sEmpresa);

                entity.Property(e => e.sEmpresa).ValueGeneratedNever();

                entity.Property(e => e.dFechaReg)
                    .HasColumnType("datetime2(0)")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<terEmpresaDireccion>(entity =>
            {
                entity.HasKey(e => e.sEmpresa);

                entity.Property(e => e.sEmpresa).ValueGeneratedNever();

                entity.Property(e => e.bTipoVia).HasComment("Calle, Jirón, Avenida, etc.");

                entity.Property(e => e.xBlock)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xDpto)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xEtapa)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xInterior)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xKilometro)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xLote)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xManzana)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xNombreVia)
                    .HasMaxLength(300)
                    .IsUnicode(false)
                    .HasComment("Aqui guarda el concatenado, si no se pudo migrar por separado");

                entity.Property(e => e.xNombreZona)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.xNroVia)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xReferencia)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<terEmpresaTelefono>(entity =>
            {
                entity.HasKey(e => new { e.sEmpresa, e.xTelefono });

                entity.Property(e => e.xTelefono)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<traAFP>(entity =>
            {
                entity.HasKey(e => e.iTrabajador)
                    .HasName("PK_traAFP_1");

                entity.Property(e => e.iTrabajador).ValueGeneratedNever();

                entity.Property(e => e.bMixta)
                    .IsRequired()
                    .HasDefaultValueSql("((1))")
                    .HasComment("Tipo de comisión: 0: comisión FLUJO, 1: comisión MIXTA");

                entity.Property(e => e.dDesde).HasColumnType("date");

                entity.Property(e => e.xCUSPP)
                    .HasMaxLength(12)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<traArea>(entity =>
            {
                entity.HasKey(e => new { e.iContrato, e.sArea });

                entity.Property(e => e.bPrincipal)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.rPorcentaje)
                    .HasColumnType("decimal(5, 2)")
                    .HasDefaultValueSql("((100))");
            });

            modelBuilder.Entity<traAsistencia>(entity =>
            {
                entity.HasKey(e => e.iAsistencia)
                    .HasName("PK_traActividadAsistencia");
            });

            modelBuilder.Entity<traAsistenciaCentroCosto>(entity =>
            {
                entity.HasKey(e => e.iAsistencia);

                entity.Property(e => e.iAsistencia).ValueGeneratedNever();
            });

            modelBuilder.Entity<traAsistenciaEstructuraSalarial>(entity =>
            {
                entity.HasKey(e => e.iAsistencia);

                entity.Property(e => e.iAsistencia).ValueGeneratedNever();
            });

            modelBuilder.Entity<traAsistenciaFecha>(entity =>
            {
                entity.HasKey(e => e.iAsistencia)
                    .HasName("PK_iActividadAsistenciaFechas");

                entity.Property(e => e.iAsistencia).ValueGeneratedNever();

                entity.Property(e => e.dDesde).HasColumnType("date");

                entity.Property(e => e.dHasta).HasColumnType("date");

                entity.Property(e => e.iDias).HasComputedColumnSql("(datediff(day,[dDesde],[dHasta])+(1))");
            });

            modelBuilder.Entity<traAsistenciaFechaCalculo>(entity =>
            {
                entity.HasKey(e => new { e.iAsistencia, e.iCalculoPlanilla });

                entity.Property(e => e.dDesde).HasColumnType("date");

                entity.Property(e => e.dHasta).HasColumnType("date");

                entity.Property(e => e.iDias).HasComputedColumnSql("(datediff(day,[dDesde],[dHasta])+(1))");
            });

            modelBuilder.Entity<traAsistenciaHora>(entity =>
            {
                entity.HasKey(e => e.iAsistencia)
                    .HasName("PK_traActividadAsistenciaHora");

                entity.Property(e => e.iAsistencia).ValueGeneratedNever();

                entity.Property(e => e.dtDesde).HasColumnType("datetime2(0)");

                entity.Property(e => e.dtHasta).HasColumnType("datetime2(0)");

                entity.Property(e => e.iMinutos).HasComputedColumnSql("(datediff(minute,[dtDesde],[dtHasta]))");
            });

            modelBuilder.Entity<traAsistenciaHoraCalculo>(entity =>
            {
                entity.HasKey(e => new { e.iAsistencia, e.iCalculoPlanilla });

                entity.Property(e => e.iMinutos).HasComputedColumnSql("(datediff(minute,[dtDesde],[dtHasta]))");
            });

            modelBuilder.Entity<traAsistenciaMarcacion>(entity =>
            {
                entity.HasKey(e => new { e.iAsistencia, e.iMarcacion });
            });

            modelBuilder.Entity<traAsistenciaMotivo>(entity =>
            {
                entity.HasKey(e => e.iAsistencia);

                entity.Property(e => e.iAsistencia).ValueGeneratedNever();

                entity.Property(e => e.xMotivo)
                    .IsRequired()
                    .HasMaxLength(500)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<traAsistenciaRelacion>(entity =>
            {
                entity.HasKey(e => new { e.iAsistencia, e.iAsistenciaRelacion });
            });

            modelBuilder.Entity<traAsistenciaVacaciones>(entity =>
            {
                entity.HasKey(e => e.iAsistencia);

                entity.Property(e => e.iAsistencia).ValueGeneratedNever();

                entity.Property(e => e.bPagada).HasComment("0:Gozada y Pagada, 1:Pagada, 2:Gozada, 3:Comprada");
            });

            modelBuilder.Entity<traCentroCosto>(entity =>
            {
                entity.HasKey(e => e.iContratoCentroCosto);

                entity.Property(e => e.bPrincipal)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.rPorcentaje)
                    .HasColumnType("decimal(5, 2)")
                    .HasDefaultValueSql("((100.00))");
            });

            modelBuilder.Entity<traConceptoFijo>(entity =>
            {
                entity.HasKey(e => e.iConceptoFijo)
                    .HasName("PK_traConceptosFijos");

                entity.HasIndex(e => new { e.bCalculo, e.iContrato, e.sConcepto, e.dDesde })
                    .HasName("IX_traConceptoFijo")
                    .IsUnique();

                entity.Property(e => e.bCalculo).HasDefaultValueSql("((2))");

                entity.Property(e => e.dDesde).HasColumnType("date");

                entity.Property(e => e.dHasta).HasColumnType("date");

                entity.Property(e => e.mValor).HasColumnType("money");
            });

            modelBuilder.Entity<traConceptoManual>(entity =>
            {
                entity.HasKey(e => e.iConceptoManual);

                entity.Property(e => e.mValor).HasColumnType("money");
            });

            modelBuilder.Entity<traConceptoManualCentroCosto>(entity =>
            {
                entity.HasKey(e => e.iConceptoManual);

                entity.Property(e => e.iConceptoManual).ValueGeneratedNever();
            });

            modelBuilder.Entity<traContrato>(entity =>
            {
                entity.HasKey(e => e.iContrato);

                entity.Property(e => e.sCargo).HasDefaultValueSql("((1))");

                entity.Property(e => e.sSede).HasDefaultValueSql("((1))");
            });

            modelBuilder.Entity<traContratoFormato>(entity =>
            {
                entity.HasKey(e => e.bFormato);

                entity.Property(e => e.bFormato).ValueGeneratedOnAdd();

                entity.Property(e => e.bTipoContrato).HasDefaultValueSql("((99))");

                entity.Property(e => e.xFormato)
                    .IsRequired()
                    .HasMaxLength(80)
                    .IsUnicode(false)
                    .HasComment("Nombre del Formato del documento word de contrato");
            });

            modelBuilder.Entity<traContratoLaboral>(entity =>
            {
                entity.HasKey(e => e.iContrato);

                entity.Property(e => e.iContrato).ValueGeneratedNever();

                entity.Property(e => e.bCategoria)
                    .HasDefaultValueSql("((1))")
                    .HasComment("(Estructura 11) Categoría: 1: Trabajador; 2: Pensionista;  4:Personal de Terceros y 5:Personal en Formación-modalidad formativa laboral.");

                entity.Property(e => e.bCategoriaOcupacional)
                    .HasDefaultValueSql("((3))")
                    .HasComment("(Tabla 24) 1:Ejecutivo, 2:Obrero, 3:Empleado");

                entity.Property(e => e.bPeriodicidadRem)
                    .HasDefaultValueSql("((1))")
                    .HasComment("(tabla 13) 1:Mensual, 2:Quincenal, 3:Semanal, etc.");

                entity.Property(e => e.bRegAtipicoDeJornadaTrab).HasComment("Sujeto a régimen alternativo, acumulativo o atípico de jornada de trabajo y descanso.");

                entity.Property(e => e.bSituacion)
                    .HasDefaultValueSql("((11))")
                    .HasComment("(tabla 15) 0: Baja, 1: Activo o Subsidiado, etc.");

                entity.Property(e => e.bSituacionEspecial).HasComment("1:Trabajador de dirección / 2:Trabajador de confianza / 0:Ninguna.");

                entity.Property(e => e.bTipoPago)
                    .HasDefaultValueSql("((2))")
                    .HasComment("(Tabla 16) 1: Efectivo, 2:Depósito en cuenta, 3:Otros");

                entity.Property(e => e.bTipoTrabajador).HasDefaultValueSql("((21))");
            });

            modelBuilder.Entity<traContratoListaNegra>(entity =>
            {
                entity.HasKey(e => e.iContrato);

                entity.Property(e => e.iContrato).ValueGeneratedNever();

                entity.Property(e => e.xMotivo)
                    .IsRequired()
                    .IsUnicode(false);
            });

            modelBuilder.Entity<traContratoPeriodo>(entity =>
            {
                entity.HasKey(e => e.iContratoPeriodo);
                entity.Property(e => e.iContratoPeriodo).ValueGeneratedNever();
                entity.Property(e => e.dDesde).HasColumnType("date");
                entity.Property(e => e.dHasta).HasColumnType("date");
            });

            modelBuilder.Entity<traContratoPeriodoCese>(entity =>
            {
                entity.HasKey(e => e.iContratoPeriodo);
            });

            modelBuilder.Entity<traContratoPeriodoMintra>(entity =>
            {
                entity.HasKey(e => e.iContratoPeriodo);

                entity.Property(e => e.iContratoPeriodo).ValueGeneratedNever();

                entity.Property(e => e.dIngreso).HasColumnType("date");

                entity.Property(e => e.xNumero)
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<traContratoPeriodoPrueba>(entity =>
            {
                entity.HasKey(e => e.iContratoPeriodo);

                entity.Property(e => e.iContratoPeriodo).ValueGeneratedNever();

                entity.Property(e => e.dHasta).HasColumnType("date");
            });

            modelBuilder.Entity<traEPS>(entity =>
            {
                entity.HasKey(e => e.iConceptoFijo)
                    .HasName("PK_traEPS_1");

                entity.Property(e => e.iConceptoFijo).ValueGeneratedNever();

                entity.Property(e => e.dPorcEmpleador)
                    .HasColumnType("decimal(5, 2)")
                    .HasDefaultValueSql("((0.00))");

                entity.Property(e => e.xCodigo)
                    .HasMaxLength(15)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<traEPSdet>(entity =>
            {
                entity.HasKey(e => new { e.iConceptoFijo, e.sPlanEPSdet })
                    .HasName("PK_traEPSdet_1");

                entity.Property(e => e.bCantidad).HasDefaultValueSql("((1))");

                entity.Property(e => e.dFechaReg)
                    .HasColumnType("datetime2(0)")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.dPorcEmpleador).HasColumnType("decimal(5, 2)");
            });

            //modelBuilder.Entity<traEstadoContrato>(entity =>
            //{
            //    entity.HasKey(e => e.iContratoPeriodo);
            //});

            modelBuilder.Entity<traHorarioRotativo>(entity =>
            {
                entity.HasKey(e => e.iHorarioRotativo);

                entity.HasIndex(e => new { e.iContrato, e.dFecha })
                    .HasName("IX_traHorarioRotativo")
                    .IsUnique();

                entity.Property(e => e.dFecha).HasColumnType("date");
            });

            modelBuilder.Entity<traHorarioRotativoActividad>(entity =>
            {
                entity.HasKey(e => e.iHorarioRotativoActividad);

                entity.Property(e => e.dtDesde).HasColumnType("datetime2(0)");
            });

            modelBuilder.Entity<traHorarioSemanal>(entity =>
            {
                entity.HasKey(e => e.iHorarioSemanal);

                entity.Property(e => e.bConsiderarCalculo)
                    .HasDefaultValueSql("((1))")
                    .HasComment("0:No considerar en cálculos, 1:Considerar");
            });

            modelBuilder.Entity<traHorarioSemanalActividad>(entity =>
            {
                entity.HasKey(e => e.iHorarioSemanalActividad);

                entity.Property(e => e.tDesde).HasColumnType("time(0)");
            });

            modelBuilder.Entity<traJefeDirecto>(entity =>
            {
                entity.HasKey(e => e.iJefeDirecto);

                entity.Property(e => e.iTrabajador).HasComment("iTrabajador del Jefe Directo");
            });

            modelBuilder.Entity<traMarcacion>(entity =>
            {
                entity.HasKey(e => e.iCalificacionMarcacion);

                entity.Property(e => e.dFecha).HasColumnType("date");

                entity.Property(e => e.dtMarcacion).HasColumnType("datetime2(0)");
            });

            modelBuilder.Entity<traPrestamoCalculo>(entity =>
            {
                entity.HasKey(e => e.iPrestamo);

                entity.Property(e => e.iPrestamo).ValueGeneratedNever();
            });

            modelBuilder.Entity<traPrestamoCuota>(entity =>
            {
                entity.HasKey(e => e.iPrestamoCuota);

                entity.Property(e => e.bCalculoAdelanto)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.dFecha).HasColumnType("date");
            });

            modelBuilder.Entity<traPrestamoCuotaCalculo>(entity =>
            {
                entity.HasKey(e => new { e.iPrestamoCuota, e.iCalculoPlanilla });
            });

            modelBuilder.Entity<traPrestamoDesembolso>(entity =>
            {
                entity.HasKey(e => e.iPrestamo);

                entity.Property(e => e.iPrestamo).ValueGeneratedNever();

                entity.Property(e => e.dFecha).HasColumnType("date");
            });

            modelBuilder.Entity<traPrestamoMotivo>(entity =>
            {
                entity.HasKey(e => e.iPrestamo);

                entity.Property(e => e.iPrestamo).ValueGeneratedNever();

                entity.Property(e => e.xMotivo)
                    .IsRequired()
                    .HasMaxLength(500)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<traProyecto>(entity =>
            {
                entity.HasKey(e => e.iContratoProyecto);

                entity.Property(e => e.bPrincipal)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.rPorcentaje)
                    .HasColumnType("decimal(5, 2)")
                    .HasDefaultValueSql("((100))");
            });

            modelBuilder.Entity<traRUC>(entity =>
            {
                entity.HasKey(e => e.iTrabajador);

                entity.Property(e => e.iTrabajador).ValueGeneratedNever();

                entity.Property(e => e.cRUC)
                    .IsRequired()
                    .HasMaxLength(11)
                    .IsUnicode(false)
                    .IsFixedLength();
            });

            modelBuilder.Entity<traRxH>(entity =>
            {
                entity.HasKey(e => e.iRxH);

                entity.HasIndex(e => new { e.iContrato, e.iPeriodo })
                    .HasName("IX_traRxH");

                entity.HasIndex(e => new { e.bTipoComprobante, e.sSerieComprobante, e.iNumComprobante })
                    .HasName("IX_traRxH_1")
                    .IsUnique();

                entity.Property(e => e.bTipoComprobante)
                    .HasDefaultValueSql("((1))")
                    .HasComment("1:R=RECIBO POR HONORARIOS,2:N=NOTA DE CRÉDITO,3:D=DIETA,4:O=OTRO COMPROBANTE");

                entity.Property(e => e.dEmision).HasColumnType("date");

                entity.Property(e => e.dPago).HasColumnType("date");

                entity.Property(e => e.mMonto).HasColumnType("money");
            });

            modelBuilder.Entity<traSCTR>(entity =>
            {
                entity.HasKey(e => e.iContrato);

                entity.Property(e => e.iContrato).ValueGeneratedNever();

                entity.Property(e => e.bRiesgoSctrPension).HasDefaultValueSql("((5))");

                entity.Property(e => e.bRiesgoSctrSalud).HasDefaultValueSql("((5))");
            });

            modelBuilder.Entity<traSCTRfecha>(entity =>
            {
                entity.HasKey(e => new { e.iContrato, e.dDesde });

                entity.Property(e => e.dDesde).HasColumnType("date");

                entity.Property(e => e.bSCTRestado).HasDefaultValueSql("((1))");

                entity.Property(e => e.dHasta).HasColumnType("date");
            });

            modelBuilder.Entity<traStatusContrato>(entity =>
            {
                entity.HasKey(e => e.iContratoPeriodo );
            });

            modelBuilder.Entity<traTrabajador>(entity =>
            {
                entity.HasKey(e => e.iTrabajador)
                    .HasName("PK_Trabajador");

                entity.Property(e => e.iTrabajador)
                    .HasComment("")
                    .ValueGeneratedNever();

                entity.Property(e => e.bConvenioEvitarDobleTributacion).HasComment("(Tabla 25) 0:Nunguno, 1:Canada, 2:Chile, 3:Can, 4:Brasil");

                entity.Property(e => e.bEstadoCivil)
                    .HasDefaultValueSql("((1))")
                    .HasComment("1: Soltero, 2:Casado, 3:divorciado, 4:Viudo");

                entity.Property(e => e.bRegimenPensionario).HasDefaultValueSql("((99))");
            });

            modelBuilder.Entity<traVidaLey>(entity =>
            {
                entity.HasKey(e => e.iConceptoFijo)
                    .HasName("PK_traTrabajadorVidaLey");

                entity.Property(e => e.iConceptoFijo).ValueGeneratedNever();

                entity.Property(e => e.rTasa).HasColumnType("decimal(5, 2)");

                entity.Property(e => e.xCodigo)
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<webFirma>(entity =>
            {
                entity.HasKey(e => e.iFirma);

                entity.Property(e => e.dFirmado).HasColumnType("datetime2(0)");

                entity.Property(e => e.dLeido).HasColumnType("datetime2(0)");
            });

            modelBuilder.Entity<webMovimientoVacaciones>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("webMovimientoVacaciones");

                entity.Property(e => e.cTipoNovedad)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.dDesde).HasColumnType("date");

                entity.Property(e => e.dHasta).HasColumnType("date");

                entity.Property(e => e.xDescripcion).IsUnicode(false);
            });
            modelBuilder.Entity<webNotificar>(entity =>
            {
                entity.HasKey(e => e.iNotificacion);
            });

            modelBuilder.Entity<webNotificacion>(entity =>
            {
                entity.HasKey(e => e.iNotificacion);

                entity.Property(e => e.dEnvio).HasColumnType("datetime2(0)");

                entity.Property(e => e.xAsunto).IsUnicode(false);
            });
            modelBuilder.Entity<webPagosPrestamo>(entity =>
            {
                entity.HasKey(e => new { e.iPrestamo, e.iPrestamoCuota });
            });

            modelBuilder.Entity<webPrestamo>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("webPrestamo");

                entity.Property(e => e.dPrestamo).HasColumnType("date");

                entity.Property(e => e.mValor).HasColumnType("money");
            });

            modelBuilder.Entity<webPrestamoPagos>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("webPrestamoPagos");

                entity.Property(e => e.dFecha).HasColumnType("date");
            });

            modelBuilder.Entity<webRolFirma>(entity =>
            {
                entity.HasKey(e => e.bRol);

                entity.Property(e => e.xFaIcono)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.xRol)
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<webTipoNotificacion>(entity =>
            {
                entity.HasKey(e => e.bTipoNotificacion);

                entity.Property(e => e.xFaIcono)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.xTipoNotificacion)
                    .HasMaxLength(40)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<webUrl>(entity =>
            {
                entity.HasKey(e => e.iUrl);

                entity.Property(e => e.iUrl).ValueGeneratedNever();

                entity.Property(e => e.xUrl).IsUnicode(false);
            });

            modelBuilder.Entity<webVacaciones>(entity =>
            {
                entity.HasKey(e => e.iContrato);
            });

            modelBuilder.Entity<PerInfoContrato>(entity =>
            {
                entity.HasKey(e => new { e.iContrato, e.dDesde });
            });

            OnModelCreatingMarcacion(modelBuilder);            
            OnModelCreatingCalculo(modelBuilder);

        }

        partial void OnModelCreatingMarcacion(ModelBuilder modelBuilder);
        partial void OnModelCreatingCalculo(ModelBuilder modelBuilder);
    }
}
