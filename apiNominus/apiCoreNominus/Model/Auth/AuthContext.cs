﻿using Microsoft.EntityFrameworkCore;

namespace apiCoreNominus.Model
{
    public partial class AuthContext : DbContext
    {
        public AuthContext()
        {
        }

        public AuthContext(DbContextOptions<AuthContext> options)
            : base(options)
        {
        }

        public virtual DbSet<autSemilla> autSemilla { get; set; }
        public virtual DbSet<authAcceso> authAcceso { get; set; }
        public virtual DbSet<authClaseNavegador> authClaseNavegador { get; set; }
        public virtual DbSet<authNavegador> authNavegador { get; set; }
        public virtual DbSet<authProvider> authProvider { get; set; }
        public virtual DbSet<authSeed> authSeed { get; set; }
        public virtual DbSet<authServidor> authServidor { get; set; }
        public virtual DbSet<AuthServidorUsuario> authServidorUsuario { get; set; }
        public virtual DbSet<authSesion> authSesion { get; set; }
        public virtual DbSet<authUsuario> authUsuario { get; set; }
        public virtual DbSet<authVoz> authVoz { get; set; }
        public virtual DbSet<notiControl> notiControl { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                GenStringConexion strConn = Procs.Conexion(new TuplaClienteTipoDb { bTipoDb = 2, sCliente = 1 });
                optionsBuilder.UseSqlServer(strConn.xConexion);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:DefaultSchema", "radical_auth");

            modelBuilder.Entity<autSemilla>(entity =>
            {
                entity.HasKey(e => e.bSpinner);

                entity.ToTable("autSemilla", "dbo");

                entity.Property(e => e.cSeed)
                    .IsRequired()
                    .HasMaxLength(128)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.dEfectivo).HasColumnType("datetime2(0)");
            });

            modelBuilder.Entity<authAcceso>(entity =>
            {
                entity.HasKey(e => e.gAcceso)
                    .HasName("PK_Accesos");

                entity.ToTable("authAcceso", "dbo");

                entity.Property(e => e.gAcceso).ValueGeneratedNever();

            });

            modelBuilder.Entity<authClaseNavegador>(entity =>
            {
                entity.HasKey(e => e.bClaseNavegador)
                    .HasName("PK_autClaseNavegador");

                entity.ToTable("authClaseNavegador", "dbo");

                entity.Property(e => e.xClaseNavegador)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<authNavegador>(entity =>
            {
                entity.HasKey(e => e.sNavegador)
                    .HasName("PK_autNavegador");

                entity.ToTable("authNavegador", "dbo");

                entity.Property(e => e.sNavegador).ValueGeneratedNever();

                entity.Property(e => e.iLatencia).HasDefaultValueSql("((30))");

                entity.Property(e => e.xNavegador)
                    .IsRequired()
                    .IsUnicode(false);

            });

            modelBuilder.Entity<authProvider>(entity =>
            {
                entity.HasKey(e => e.bProvider);

                entity.ToTable("authProvider", "dbo");

                entity.Property(e => e.providerId)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<authSeed>(entity =>
            {
                entity.HasKey(e => e.gSeed);

                entity.ToTable("authSeed", "dbo");

                entity.Property(e => e.gSeed).ValueGeneratedNever();
            });

            modelBuilder.Entity<AuthServidorUsuario>(entity => {
                entity.HasKey(e => new { e.sServidor, e.iPersona });
            });

            modelBuilder.Entity<authServidor>(entity =>
            {
                entity.HasKey(e => e.sServidor)
                    .HasName("PK_Servidores");

                entity.ToTable("authServidor", "dbo");

                entity.Property(e => e.sServidor).ValueGeneratedNever();

                entity.Property(e => e.xAsistenciaZK).IsUnicode(false);

                entity.Property(e => e.xConexion).IsUnicode(false);

                entity.Property(e => e.xDescripcion)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.xModeloAPI)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<authSesion>(entity =>
            {
                entity.HasKey(e => e.gToken);

                entity.ToTable("authSesion", "dbo");

                entity.Property(e => e.gToken).HasDefaultValueSql("(newid())");

                entity.Property(e => e.dFin).HasColumnType("datetime2(0)");

                entity.Property(e => e.dInicio).HasColumnType("datetime2(0)");

                entity.Property(e => e.xTokenMsg)
                    .HasMaxLength(200)
                    .IsUnicode(false);

            });

            modelBuilder.Entity<authUsuario>(entity =>
            {
                entity.HasKey(e => e.gUsuario)
                    .HasName("PK_Usuarios");

                entity.ToTable("authUsuario", "dbo");

                entity.HasIndex(e => e.email)
                    .HasName("IX_xEmail")
                    .IsUnique();

                entity.Property(e => e.gUsuario).ValueGeneratedNever();

                entity.Property(e => e.displayName)
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.email)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.phoneNumber)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.photo).IsUnicode(false);

            });

            modelBuilder.Entity<authVoz>(entity =>
            {
                entity.HasKey(e => e.iVoz);

                entity.ToTable("authVoz", "dbo");

                entity.Property(e => e.dVoz).HasColumnType("datetime2(0)");

                entity.Property(e => e.hVoz).IsUnicode(false);


            });

            modelBuilder.Entity<notiControl>(entity =>
            {
                entity.HasKey(e => new { e.iServidor, e.iNotificacion });

                entity.ToTable("notiControl", "dbo");

                entity.Property(e => e.dEnvio).HasColumnType("datetime2(0)");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
