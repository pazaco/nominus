﻿namespace apiCoreNominus.Model
{
    public partial class authServidor
    {

        public short sServidor { get; set; }
        public string xDescripcion { get; set; }
        public string xConexion { get; set; }
        public string xModeloAPI { get; set; }
        public string xAsistenciaZK { get; set; }

    }
}
