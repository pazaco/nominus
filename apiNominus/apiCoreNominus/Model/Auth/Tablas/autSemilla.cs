﻿using System;

namespace apiCoreNominus.Model
{
    public partial class autSemilla
    {
        public byte bSpinner { get; set; }
        public string cSeed { get; set; }
        public DateTime? dEfectivo { get; set; }
    }
}
