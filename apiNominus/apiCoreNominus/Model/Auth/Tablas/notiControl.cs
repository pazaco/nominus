﻿using System;

namespace apiCoreNominus.Model
{
    public partial class notiControl
    {
        public int iServidor { get; set; }
        public int iNotificacion { get; set; }
        public DateTime? dEnvio { get; set; }
    }
}
