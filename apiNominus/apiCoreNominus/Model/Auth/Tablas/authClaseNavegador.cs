﻿namespace apiCoreNominus.Model
{
    public partial class authClaseNavegador
    {
        public byte bClaseNavegador { get; set; }
        public string xClaseNavegador { get; set; }
        public bool? lMobile { get; set; }

    }
}
