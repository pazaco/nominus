﻿using System;

namespace apiCoreNominus.Model
{
    public partial class authSesion
    {
        public Guid gToken { get; set; }
        public Guid? gUsuario { get; set; }
        public DateTime dInicio { get; set; }
        public DateTime? dFin { get; set; }
        public short? sNavegador { get; set; }
        public string xTokenMsg { get; set; }
        public short? sServidor { get; set; }
        public int? iContrato { get; set; }

    }
}
