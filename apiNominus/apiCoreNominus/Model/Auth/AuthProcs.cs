﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using apiCoreNominus.Controllers.Asistencia;

namespace apiCoreNominus.Model
{
    public partial class Procs
    {
        public static void CompletarEnlaces(authUsuario usuario)
        {
            using (AuthContext dbA = new AuthContext())
            {
                List<authServidor> _srvs = dbA.authServidor.ToList();
                foreach (authServidor _srv in _srvs)
                {
                    using (NominusContext db = new NominusContext(_srv.sServidor))
                    {
                        autUsuario usr = db.autUsuario.FirstOrDefault(c => c.xEmail == usuario.email);
                        if (usr != null)
                        {
                            dbA.authAcceso.Add(new authAcceso
                            {
                                gAcceso = Guid.NewGuid(),
                                sServidor = _srv.sServidor,
                                gUsuario = usuario.gUsuario,
                                iPersona = usr.iUsuario
                            });
                            dbA.SaveChanges();
                        }
                    }
                }
            }
        }

        public static List<AutServidor> ContratosServidor(string xEmail)
        {
            List<AutServidor> resp = new List<AutServidor>();
            using (AuthContext dbA = new AuthContext())
            {
                Guid gUsuario = dbA.authUsuario.FirstOrDefault(c => c.email == xEmail).gUsuario;
                List<authServidor> _srvs = ServersxPersona(gUsuario);
                foreach (authServidor _srv in _srvs)
                {
                    AutServidor cServ;

                    cServ = new AutServidor { sServidor = _srv.sServidor, empresas = new List<AutEmpresa>() };
                    NominusContext db = new NominusContext(_srv.sServidor);
                    MarcaZkContext dbz = new MarcaZkContext(_srv.sServidor);
                    bool isZk = dbz.isConnected();
                    List<authAcceso> _accesos = dbA.authAcceso.Where(c => c.gUsuario == gUsuario && c.sServidor == _srv.sServidor).ToList();
                    foreach (authAcceso _acceso in _accesos)
                    {
                        cServ.iPersona = _acceso.iPersona;
                        List<NomContrato> _contratos = Procs.Contratos(_acceso.iPersona ?? 0, _srv.sServidor).OrderBy(c => c.sEmpresa).ToList();
                        int numContratos = _contratos.Count();
                        foreach (NomContrato _contrato in _contratos)
                        {
                            List<int> nAcc = new List<int>();
                            nAcc.Add(3);
                            if (db.sysAdministrador.Any(c => c.iPersona == _acceso.iPersona))
                            {
                                nAcc.Add(98);
                            }
                            if (db.traJefeDirecto.Any(c => c.iJefeDirecto == _acceso.iPersona))
                            {
                                nAcc.Add(5);
                            }
                            if (isZk && dbz.USERINFO.Any(c => c.Badgenumber == _acceso.iPersona.ToString()))
                            {
                                nAcc.Add(1);
                            }
                            if (db.remCalculoPlanilla.Any(c => c.iContrato == _contrato.iContrato))
                            {
                                nAcc.Add(2);
                            }
                            if (db.traPrestamo.Any(c => c.iContrato == _contrato.iContrato))
                            {
                                nAcc.Add(4);
                            }
                            if (cServ.empresas.FirstOrDefault(c => c.sEmpresa == _contrato.sEmpresa) == null)
                            {
                                cServ.empresas.Add(new AutEmpresa
                                {
                                    sEmpresa = _contrato.sEmpresa,
                                    jImagen = _contrato.jImagen,
                                    xAbreviado = _contrato.xAbreviado,
                                    xRazonSocial = _contrato.xRazonSocial,
                                    contratos = new List<AutContrato>()
                                });
                            }
                            int indexEmpresa = cServ.empresas.FindIndex(c => c.sEmpresa == _contrato.sEmpresa);
                            cServ.empresas[indexEmpresa].contratos.Add(new AutContrato
                            {
                                iContrato = _contrato.iContrato,
                                sCargo = _contrato.sCargo,
                                xCargo = _contrato.xCargo,
                                dDesde = _contrato.dDesde,
                                dHasta = _contrato.dHasta,
                                accesos = nAcc
                            });
                        }
                    }
                    resp.Add(cServ);
                }
            }
            return resp;
        }

        public static List<AutIndiceContrato> IndiceContratos(Guid token)
        {
            List<AutIndiceContrato> resp = new List<AutIndiceContrato>();
            using (AuthContext dbA = new AuthContext())
            {
                authSesion _ses = dbA.authSesion.FirstOrDefault(c => c.gToken == token);
                Guid gUsuario;
                if (_ses.gUsuario != null)
                {
                    gUsuario = (Guid)_ses.gUsuario;
                }
                else
                {
                    gUsuario = new Guid("00000000-0000-0000-0000-000000000000");
                }
                List<authServidor> _srvs = ServersxPersona(gUsuario);
                foreach (authServidor _srv in _srvs)
                {
                    authAcceso _acc = dbA.authAcceso.FirstOrDefault(c => c.gUsuario == gUsuario && c.sServidor == _srv.sServidor);
                    if (_acc != null)
                    {
                        using (NominusContext db = new NominusContext(_srv.sServidor))
                        {
                            List<AutIndiceContrato> lst = db.traStatusContrato
                                .Where(c => c.iTrabajador == _acc.iPersona)
                                .Select(s => new AutIndiceContrato
                                {
                                    sServidor = _srv.sServidor,
                                    iContrato = s.iContrato,
                                    iContratoPeriodo = s.iContratoPeriodo,
                                    iTrabajador = s.iTrabajador,
                                    sEmpresa = s.sEmpresa,
                                    sSede = s.sSede,
                                    sCargo = s.sCargo,
                                    cTipo = s.cTipo,
                                    dDesde = s.dDesde,
                                    dHasta = s.dHasta,
                                    lSeleccionado = (_ses != null && _ses.sServidor == _srv.sServidor && _ses.iContrato == s.iContratoPeriodo),
                                    xContrato = db.orgCargo.FirstOrDefault(c => c.sCargo == s.sCargo).xCargo + " (" + db.orgEmpresa.FirstOrDefault(c => c.sEmpresa == s.sEmpresa).xAbreviado + ")"
                                }).ToList();
                            if (lst != null)
                            {
                                resp.AddRange(lst);
                            }
                        }
                    }
                }
            }
            return resp;
        }
        
        public static List<AutIndiceAcceso> IndiceAccesos(Guid token)
        {
            List<AutIndiceAcceso> resp = new List<AutIndiceAcceso>();

            using (AuthContext dbA = new AuthContext())
            {
                authSesion _ses = dbA.authSesion.FirstOrDefault(c => c.gToken == token);
                authUsuario _usu = dbA.authUsuario.FirstOrDefault(c => c.gUsuario == _ses.gUsuario);
                bool esSuper = false;
                using (GeneralContext dg = new GeneralContext())
                {
                    nomPersonal _userG = dg.nomPersonal.FirstOrDefault(c => c.xCorreo == _usu.email);
                    List<authServidor> _srvs = dbA.authServidor.ToList();
                    foreach (authServidor _srv in _srvs)
                    {
                        esSuper = _userG!=null && dg.nomPersonalPerfilCliente.Any(c => c.bPerfil == 1 && c.iPersonaNominus == _userG.iPersonaNominus && c.sCliente==_srv.sServidor);
                        if (esSuper)
                        {
                            resp.Add(new AutIndiceAcceso
                            {
                                esSuper = true,
                                sServidor = _srv.sServidor,
                                lSeleccionado = (_ses.sServidor != null && _ses.sServidor == _srv.sServidor),
                                xServidor = _srv.xDescripcion,
                                controlAsistencia = dg.nomConexion.Any(c => c.sCliente == _srv.sServidor && c.bTipoDb == 5)
                        });
                        }
                        else
                        {
                            authAcceso _acc = dbA.authAcceso.FirstOrDefault(c => c.gUsuario == _ses.gUsuario && c.sServidor == _srv.sServidor);
                            if (_acc != null)
                            {
                                AutIndiceAcceso niAcc = new AutIndiceAcceso();
                                using (NominusContext db = new NominusContext(_srv.sServidor))
                                {
                                    niAcc.gAcceso = _acc.gAcceso;
                                    niAcc.sServidor = _srv.sServidor;
                                    niAcc.iUsuario = _acc.iPersona;
                                    niAcc.esAdmin = db.sysAdministrador.Any(c => c.iPersona == _acc.iPersona && (c.dVigencia == null || c.dVigencia >= DateTime.Now));
                                    niAcc.esJefe = db.traJefeDirecto.Any(c => c.iTrabajador == _acc.iPersona);
                                    niAcc.lSeleccionado = (_ses.sServidor != null && _ses.sServidor == _srv.sServidor);
                                    niAcc.xServidor = _srv.xDescripcion;
                                    niAcc.controlPrestamos = false;
                                    niAcc.controlVacaciones = true;
                                    niAcc.controlAsistencia = dg.nomConexion.Any(c => c.sCliente == _srv.sServidor && c.bTipoDb == 5);
                                }
                                resp.Add(niAcc);
                            }
                        }

                    }
                }
            }
            if (resp.Count == 1 && resp[0].lSeleccionado == false)
            {
                resp[0].lSeleccionado = true;
            }
            if (resp.Count > 1 && !resp.Any(c => c.lSeleccionado == true))
            {
                resp[0].lSeleccionado = true;
            }
            return resp;
        }

        public static List<int> Perfiles(string xEmail)
        {
            AuthContext dbA = new AuthContext();
            Guid gUsuario = dbA.authUsuario.FirstOrDefault(c => c.email == xEmail).gUsuario;
            List<authServidor> _srvs = ServersxPersona(gUsuario);
            List<int> resp = new List<int>();
            List<int> tempLst = new List<int>();
            foreach (authServidor _srv in _srvs)
            {
                if (_srv.sServidor != 8)
                {
                    NominusContext db = new NominusContext(_srv.sServidor);
                    List<authAcceso> _accesos = dbA.authAcceso.Where(c => c.gUsuario == gUsuario && c.sServidor == _srv.sServidor).ToList();
                    foreach (authAcceso _acceso in _accesos)
                    {
                        List<NomContrato> _contratos = Procs.Contratos(_acceso.iPersona ?? 0, _srv.sServidor).OrderBy(c => c.sEmpresa).ToList();
                        if (_contratos.Count > 0)
                        {
                            tempLst.Add(1);  // acceso a boletas en cualquier estado contractual
                            tempLst.Add(2);  // acceso a vacaciones en cualquier estado contractual
                        }
                        foreach (NomContrato _contrato in _contratos)
                        {
                            if (db.traMarcacion.Where(c => c.iContrato == _contrato.iContrato).ToList().Count > 0)
                            {
                                tempLst.Add(3); //acceso a asistencia calificada (que marcan reloj)
                            }
                            if (_contrato.dHasta == null || _contrato.dHasta > DateTime.Now)  //contrato vigente
                            {
                                //if (db.webPrestamos(_contrato.iContrato).ToList().Count > 0)
                                //{
                                //    tempLst.Add(4); //tiene algun prestamo
                                //}
                                if (db.traJefeDirecto.Where(c => c.iTrabajador == _acceso.iPersona).ToList().Count > 0)
                                {
                                    tempLst.Add(5); //es jefe de alguien
                                }
                            }
                        }
                    }
                }
            }
            for (int ii = 1; ii < 100; ii++)
            {
                if (tempLst.Contains(ii)) resp.Add(ii);
            }
            return resp;
        }

        public static AutContratoSeleccionado ContratoSel(AutIndiceContrato indiceContrato)
        {
            AutContratoSeleccionado resp = null;
            using (AuthContext dbA = new AuthContext())
            {
                using (NominusContext db = new NominusContext(indiceContrato.sServidor))
                {
                    USERINFO _uzk = null;
                    int iUserId = 0;
                    using( MarcaZkContext dbZ = new MarcaZkContext(indiceContrato.sServidor))
                    {
                        if (dbZ.isConnected())
                        {
                            _uzk = dbZ.USERINFO.FirstOrDefault(c => c.Badgenumber == indiceContrato.iTrabajador.ToString());
                            iUserId = _uzk.USERID;
                        }
                    }
                    orgEmpresa _empresa = db.orgEmpresa.Find(indiceContrato.sEmpresa);
                    orgCargo _car = db.orgCargo.Find(indiceContrato.sCargo);
                    orgEmpresaImagen _emi = db.orgEmpresaImagen.FirstOrDefault(c => c.sEmpresa == indiceContrato.sEmpresa && c.bTipoImagen == 1);



                    resp = new AutContratoSeleccionado
                    {
                        iContrato = indiceContrato.iContrato,
                        iContratoPeriodo = indiceContrato.iContratoPeriodo,
                        iPersona = indiceContrato.iTrabajador,
                        dDesde = indiceContrato.dDesde,
                        dHasta = indiceContrato.dHasta,
                        cTipo = indiceContrato.cTipo,
                        sServidor = indiceContrato.sServidor,
                        sEmpresa = indiceContrato.sEmpresa,
                        sCargo = indiceContrato.sCargo,
                        iZkUserId = iUserId,
                        xDescripcion = dbA.authServidor.Find(indiceContrato.sServidor).xDescripcion,
                        xRazonSocial = _empresa.xRazonSocial,
                        xAbreviado = _empresa.xAbreviado,
                        xCargo = _car == null ? "" : _car.xCargo,
                        jImagen = _emi != null ? _emi.jImagen : null,
                        jAppConfig = _empresa.jAppConfig

                    };
                };
            }
            return resp;
        }

        private static List<authServidor> ServersxPersona(Guid gUsuario)
        {
            AuthContext dbA = new AuthContext();
            List<authServidor> resp = new List<authServidor>();
            resp = dbA.authServidor.FromSqlRaw("exec authServidoresPersona @gUsuario", new SqlParameter("gUsuario", gUsuario)).ToList();
            return resp;
        }

        public static List<AuthServidorUsuario> authServidoresUsuario(Guid gToken)
        {
            AuthContext dbA = new AuthContext();
            List<AuthServidorUsuario> resp = new List<AuthServidorUsuario>();
            resp = dbA.authServidorUsuario.FromSqlRaw("exec authServidoresUsuario @gToken", new SqlParameter("gToken", gToken)).ToList();
            return resp;

        }

    }

    #region Outputs Aut
    public class AutFirebase
    {
        [Key]
        public Guid token { get; set; }
        public authUsuario usuario { get; set; }
        public List<AutServidor> contratos { get; set; }
        public List<AutIndiceContrato> indiceContratos { get; set; }
        public List<AutIndiceAcceso> indiceAcceso { get; set; }
        public AutContratoSeleccionado contrato { get; set; }
    }
    public class AutFire : AutFireAcceso
    {
        public List<AutIndiceContrato> indiceContratos { get; set; }
        public AutContratoSeleccionado contrato { get; set; }

    }

    public class AutConsola : AutConsolaCliente
    {
        public List<AutPersonalPerfilCliente> perfiles { get; set; }
        public nomPersonal persona { get; set; }
        public Guid gSesion { get; set; }
    }

    public class ParCambiaPerfil
    {
        public int iPersonaNominus { get; set; }
        public short? sEmpresa { get; set; }
        public short? sCliente { get; set; }
    }

    public class AutConsolaCliente
    {
        public IEnumerable<AutEmpresa> empresas { get; set; }
        public TraTrabajador trabajador { get; set; }
        public List<TraTrabajador> colaboradores { get; set; } 
    }

    public class AutPersonalPerfilCliente : nomPersonalPerfilCliente
    {
        public string xCliente { get; set; }
        public string xPerfil { get; set; }
    }

    public class AutFireAcceso
    {
        [Key]
        public Guid token { get; set; }
        public authUsuario usuario { get; set; }
        public List<AutIndiceAcceso> indiceAcceso { get; set; }
    }
    public class AutServidor
    {
        [Key]
        public short sServidor { get; set; }
        public List<AutEmpresa> empresas { get; set; }
        public int? iPersona { get; set; }
    }
    public class AutEmpresa
    {
        [Key]
        public short sEmpresa { get; set; }
        public string xRazonSocial { get; set; }
        public string xAbreviado { get; set; }
        public string jImagen { get; set; }
        public string jAppConfig { get; set; }
        public List<AutContrato> contratos { get; set; }
        public Nullable<Boolean> lDefault { get; set; }

    }
    public class AutContratoSeleccionado
    {
        [Key]
        public int iContrato { get; set; }
        public int iContratoPeriodo { get; set; }
        public string cTipo { get; set; }
        public short sServidor { get; set; }
        public string xDescripcion { get; set; }
        public int iZkUserId { get; set; }
        public short sEmpresa { get; set; }
        public int? iPersona { get; set; }
        public short sCargo { get; set; }
        public string xCargo { get; set; }
        public DateTime? dDesde { get; set; }
        public DateTime? dHasta { get; set; }
        public string xRazonSocial { get; set; }
        public string xAbreviado { get; set; }
        public string jImagen { get; set; }
        public string xApellidos { get; set; }
        public string xNombres { get; set; }
        public string jFoto { get; set; }
        public string jAppConfig { get; set; }
    }
    public class AutContrato
    {
        [Key]
        public int iContrato { get; set; }
        public short sCargo { get; set; }
        public string xCargo { get; set; }
        public DateTime? dDesde { get; set; }
        public DateTime? dHasta { get; set; }
        public List<int> accesos { get; set; }
    }


    public class AutIndiceContrato : traStatusContrato
    {
        public AutIndiceContrato() {
            lSeleccionado = false;
            }
        public  short sServidor { get; set; }
        public bool lSeleccionado { get; set; }
        public string xContrato { get; set; }
    }

    public class AutIndiceAcceso
    {
        public AutIndiceAcceso()
        {
            gAcceso = new Guid("00000000-0000-0000-0000-000000000000");
            esSuper = false;
            esAdmin = false;
            esJefe = false;
            lSeleccionado = false;
            controlAsistencia = false;
            controlPrestamos = false;
            controlVacaciones = false;
        }
        public Guid gAcceso { get; set; }
        public short sServidor { get; set; }
        public int? iUsuario { get; set; }
        public bool esSuper { get; set; }
        public bool esAdmin { get; set; }
        public bool esJefe { get; set; }
        public bool lSeleccionado { get; set; }
        public string xServidor { get; set; }
        public Boolean controlAsistencia { get; set; }
        public Boolean controlPrestamos { get; set; }
        public Boolean controlVacaciones { get; set; }
    }

    public class AuthServidorUsuario
    {
        public int iPersona { get; set; }
        public short sServidor { get; set; }
        public string xDescripcion { get; set; }
        public string xConexion { get; set; }
        public string xModeloAPI { get; set; }
        public string xAsistenciaZK { get; set; }
    }

    public class Jash
    {
        public string hash { get; set; }
        public string ex { get; set; }
    }

    #endregion

    #region Inputs 
    public class TuplaLoginFirebase
    {
        public TuplaLoginFirebase()
        {
            tokenMsg = "";
        }
        public string xEmail { get; set; }
        public string providerId { get; set; }
        public string tokenMsg { get; set; }
    }

    public class TuplaContrato
    {
        public short sServidor { get; set; }
        public int iContratoPeriodo { get; set; }
    }

    #endregion

}
