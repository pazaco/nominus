using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace apiCoreNominus
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("CorsCualquierOrigen", builder =>
                {
                    builder.AllowAnyOrigin()
                    .AllowAnyHeader()
                    .AllowAnyMethod();
                });
            });
            services.AddControllers();
            //services.AddDbContext<GeneralContext>(options =>
            //    options.UseSqlServer(Configuration.GetConnectionString("general")));
            services.AddMvc()
                .AddNewtonsoftJson();
            services.AddSwaggerDocument(config =>
            {
                config.PostProcess = document =>
                {
                    document.Info.Version = "v2.0.1102";
                    document.Info.Title = "Nominus API";
                    document.Info.Description = "Diccionario webAPI para la gesti�n de Nominus Planilla y Nominus Asistencia. Incluye Marcaci�n centralizada Zk";
                    document.Info.Contact = new NSwag.OpenApiContact { Name = "Jorge Loaiza Arango", Email = "jorge.loaiza@nominus.pe" };
                };
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseStaticFiles();
            app.UseOpenApi();
            app.UseSwaggerUi3();
            app.UseRouting();
            app.UseAuthorization();
            app.UseCors("CorsCualquierOrigen");
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
