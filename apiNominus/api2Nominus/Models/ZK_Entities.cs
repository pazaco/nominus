﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using api2Nominus.Models.asistenciaPK;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;


namespace api2Nominus.Models
{
    public partial class ZK_Entities : DbContext
    {

        private readonly string xConn;
        private static string cs;

        public ZK_Entities(string xConn = "ZK_01")
        {
            this.xConn = xConn;
            Database.SetCommandTimeout(25000);
        }

        public ZK_Entities(DbContextOptions<ZK_Entities> options)
            : base(options)
        { }

        public virtual DbSet<USERINFO> USERINFO { get; set; }
        public virtual DbSet<acc_door> acc_door { get; set; }
        public virtual DbSet<acc_monitor_log> acc_monitor_log { get; set; }
        public virtual DbSet<acc_levelset> acc_levelset { get; set; }
        public virtual DbSet<acc_levelset_emp> acc_levelset_emp { get; set; }
        public virtual DbSet<TEMPLATE> TEMPLATE { get; set; }
        public virtual DbSet<DEPARTMENTS> DEPARTMENTS { get; set; }
        public virtual DbSet<personnel_area> personnel_area { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                cs = ConfigurationManager.ConnectionStrings[this.xConn].ConnectionString;
                optionsBuilder.UseSqlServer(cs);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<USERINFO>(entity => { entity.HasKey(e => e.USERID); });
            modelBuilder.Entity<DEPARTMENTS>(entity => { entity.HasKey(e => e.DEPTID); });
            modelBuilder.Entity<TEMPLATE>(entity => { entity.HasKey(e => e.TEMPLATEID); });
            modelBuilder.Entity<acc_monitor_log>(entity => { entity.HasKey(e => e.id); });
            modelBuilder.Entity<acc_levelset>(entity => { entity.HasKey(e => e.id); });
            modelBuilder.Entity<acc_levelset_emp>(entity => { entity.HasKey(e => e.id); });
            modelBuilder.Entity<acc_door>(entity => { entity.HasKey(e => e.id); });
            modelBuilder.Entity<personnel_area>(entity => { entity.HasKey(e => e.id); });
        }
    }
}
