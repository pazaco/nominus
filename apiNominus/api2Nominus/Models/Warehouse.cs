﻿using api2Nominus.Models.asistenciaPK;
using api2Nominus.Models.auth;
using api2Nominus.Models.nominus;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace api2Nominus.Models
{
    public class WareHouse
    {
        private static string apiUladech = "https://api.uladech.edu.pe:8088/";
        private static string xSujeto = "asistencia-from-nominus.pe";


        public static async Task<List<authServidor>> ServerxPersona(int iPersona)
        {
            nominusAppEntities dbA = new nominusAppEntities();
            List<authServidor> resp = new List<authServidor>();
            List<authServidor> _servidores = dbA.authServidor.ToList();
            List<authAcceso> _accesos = dbA.authAcceso.Where(c => c.iPersona == iPersona).ToList();
            resp = _servidores
                .Join(_accesos, srv => srv.sServidor, acc => acc.sServidor, (srv, acc) => new { srv, acc })
                .Select(c => new authServidor { sServidor = c.srv.sServidor, xConexion = c.srv.xConexion, xDescripcion = c.srv.xDescripcion, xModeloAPI = c.srv.xModeloAPI })
                .ToList();
            await Task.Delay(1);
            return resp;
        }

        public static async Task<List<authServidor>> ServerxEmail(string xEmail)
        {
            nominusAppEntities dbA = new nominusAppEntities();
            List<authServidor> resp = new List<authServidor>();
            List<authServidor> _servidores = dbA.authServidor.ToList();
            authUsuario _usuario = dbA.authUsuario.FirstOrDefault(c => c.email == xEmail);
            if (_usuario != null)
            {
                List<authAcceso> _accesos = dbA.authAcceso.Where(c => c.gUsuario == _usuario.gUsuario).ToList();
                resp = _servidores
                    .Join(_accesos, srv => srv.sServidor, acc => acc.sServidor, (srv, acc) => new { srv, acc })
                    .Select(c => new authServidor { sServidor = c.srv.sServidor, xConexion = c.srv.xConexion, xDescripcion = c.srv.xDescripcion, xModeloAPI = c.srv.xModeloAPI })

                    .ToList();
            }
            await Task.Delay(1);
            return resp;
        }

        public static async Task<List<authServidor>> ServersxPersona(Guid gUsuario)
        {
            nominusAppEntities dbA = new nominusAppEntities();
            List<authServidor> csxp = await dbA.authServidoresPersona(gUsuario);
            return csxp;
        }

        public static async Task<List<aut_Contratos>> Contratos(string xEmail)
        {
            nominusAppEntities dbA = new nominusAppEntities();
            List<aut_Contratos> resp = new List<aut_Contratos>();
            Guid gUsuario = dbA.authUsuario.FirstOrDefault(c => c.email == xEmail).gUsuario;
            List<authServidor> _srvs = await ServerxEmail(xEmail);
            foreach (authServidor _srv in _srvs)
            {
                nominaEntities db = new nominaEntities(_srv.xConexion);

                foreach (authAcceso _acceso in dbA.authAcceso.Where(c => c.gUsuario == gUsuario && c.sServidor == _srv.sServidor).ToList())
                {
                    List<AutContratos> _contratos = (await db.autContratos((int)_acceso.iPersona)).ToList();
                    resp.AddRange(_contratos
                        .Select(c => new aut_Contratos
                        {
                            sServidor = _srv.sServidor,
                            iContrato = c.iContrato,
                            sEmpresa = c.sEmpresa,
                            sCargo = c.sCargo,
                            xCargo = c.xCargo,
                            xAbreviado = c.xAbreviado,
                            xRazonSocial = c.xRazonSocial,
                            jImagen = c.jImagen
                        }));
                }
            }
            return resp;
        }

        public static async Task<List<R_servidor>> ContratosServidor(string xEmail)
        {
            nominusAppEntities dbA = new nominusAppEntities();
            List<R_servidor> resp = new List<R_servidor>();
            Guid gUsuario = dbA.authUsuario.FirstOrDefault(c => c.email == xEmail).gUsuario;
            List<authServidor> _srvs = await ServersxPersona(gUsuario);
            foreach (authServidor _srv in _srvs)
            {
                R_servidor cServ;

                cServ = new R_servidor { sServidor = _srv.sServidor, empresas = new List<R_empresa>() };
                nominaEntities db = new nominaEntities(_srv.xConexion);
                ZK_Entities dbz = null;
                if (_srv.xAsistenciaZK != null && _srv.xAsistenciaZK != "")
                {
                    dbz = new ZK_Entities(_srv.xAsistenciaZK);
                }
                List<authAcceso> _accesos = dbA.authAcceso.Where(c => c.gUsuario == gUsuario && c.sServidor == _srv.sServidor).ToList();
                foreach (authAcceso _acceso in _accesos)
                {
                    cServ.iPersona = _acceso.iPersona;
                    List<AutContratos> _contratos = (await db.autContratos((int)_acceso.iPersona)).OrderBy(c => c.sEmpresa).ToList();
                    int numContratos = _contratos.Count();
                    foreach (AutContratos _contrato in _contratos)
                    {
                        List<int> nAcc = new List<int>();
                        nAcc.Add(3);
                        if (db.sysAdministrador.Any(c => c.iPersona == _acceso.iPersona))
                        {
                            nAcc.Add(98);
                        }
                        if (db.traJefeDirecto.Any(c => c.iJefeDirecto == _acceso.iPersona))
                        {
                            nAcc.Add(5);
                        }
                        if(dbz!=null && dbz.USERINFO.Any(c => c.Badgenumber == _acceso.iPersona.ToString())) {
                            nAcc.Add(1);
                        }
                        if (db.remCalculoPlanilla.Any(c => c.iContrato == _contrato.iContrato))
                        {
                            nAcc.Add(2);
                        }
                        if (db.traPrestamo.Any(c => c.iContrato == _contrato.iContrato))
                        {
                            nAcc.Add(4);
                        }
                        if (cServ.empresas.FirstOrDefault(c => c.sEmpresa == _contrato.sEmpresa) == null)
                        {
                            cServ.empresas.Add(new R_empresa
                            {
                                sEmpresa = _contrato.sEmpresa,
                                jImagen = _contrato.jImagen,
                                xAbreviado = _contrato.xAbreviado,
                                xRazonSocial = _contrato.xRazonSocial,
                                contratos = new List<R_contrato>()
                            });
                        }
                        int indexEmpresa = cServ.empresas.FindIndex(c => c.sEmpresa == _contrato.sEmpresa);
                        cServ.empresas[indexEmpresa].contratos.Add(new R_contrato
                        {
                            iContrato = _contrato.iContrato,
                            sCargo = _contrato.sCargo,
                            xCargo = _contrato.xCargo,
                            dDesde = _contrato.dDesde,
                            dHasta = _contrato.dHasta,
                            accesos = nAcc
                        });
                    }
                }
                resp.Add(cServ);
            }
            return resp;
        }

        public static async Task<List<RspBuscaPersona>> BuscaPersonas(ParBuscaPersona per)
        {
            List<RspBuscaPersona> rsp = new List<RspBuscaPersona>();
            JavaScriptSerializer jss = new JavaScriptSerializer();
            HttpWebRequest rqper = (HttpWebRequest)WebRequest.Create(apiUladech + "per/persona");
            rqper.Method = "POST";
            rqper.ContentType = "application/json;charset=\"utf-8\"";
            rqper.Headers.Add("xTokenMin", Token.getToken(xSujeto, 128, lUTC: true));
            string xPush = jss.Serialize(per);
            rqper.ContentLength =xPush.Length;
            StreamWriter requestWriter = new StreamWriter(rqper.GetRequestStream(), System.Text.Encoding.ASCII);
            requestWriter.Write(xPush);
            requestWriter.Close();
            WebResponse rspPer = rqper.GetResponse();
            Stream streamPer = rspPer.GetResponseStream();
            StreamReader readerPer = new StreamReader(streamPer);
            string jsonPer = readerPer.ReadToEnd();
            rsp = jss.Deserialize<List<RspBuscaPersona>>(jsonPer);
            await Task.Delay(1);
            return rsp;
        }

        public static async Task<string> HorarioDiario(Nullable<DateTime> dHoy)
        {
            nominusAppEntities dbA = new nominusAppEntities();
            //TODO:  Extender control de horario a otros servidores, 8 es uladech
            if (dHoy == null)
            {
                dHoy = DateTime.Now.Date;
            }
            List<string> faltantes = new List<string>();
            authServidor _srv = dbA.authServidor.Where(c => c.sServidor == 8).FirstOrDefault();
            nominaEntities db = new nominaEntities(_srv.xConexion);
            uladechEntities dbU = new uladechEntities();
            JavaScriptSerializer jss = new JavaScriptSerializer();
            string xApi = String.Format("asi/diario?dFecha={0:yyyy-MM-dd}", dHoy);
            HttpWebRequest rqsD = (HttpWebRequest)WebRequest.Create(apiUladech + xApi);
            rqsD.Method = "GET";
            rqsD.ContentType = "application/json:charset=\"utf-8\"";
            string tkn = Token.getToken(xSujeto, 128, lUTC: true);
            rqsD.Headers.Add("xTokenMin", tkn);
            WebResponse rspD = rqsD.GetResponse();
            Stream strD = rspD.GetResponseStream();
            StreamReader strrD = new StreamReader(strD);
            string jsDiario = strrD.ReadToEnd();
            List<RspHorarioDiario> horario = jss.Deserialize<List<RspHorarioDiario>>(jsDiario);
            List<asi_HorarioDiario> todoH = db.asi_HorarioDiario.ToList();
            db.asi_HorarioDiario.RemoveRange(todoH);
            db.asi_HorarioDiario.AddRange(horario.Select(c => new asi_HorarioDiario
            {
                cEmpleado = c.cEmpleado,
                bControl = c.bControl,
                cHora = c.cHora,
                cActividad = c.cActividad,
                cAula = c.cAula
            }).ToList());
            db.SaveChanges();
            await db.asi_CargaDiaria((DateTime)dHoy);
            if (dHoy >= DateTime.Now.Date)
            {
                await db.asi_SincronizarMarcacion();
            }
            await Task.Delay(1);
            return horario.Count.ToString();
        }

        public static List<asi_HorarioDiario> PreDiarioDocente(Nullable<DateTime> dHoy)
        {
            nominusAppEntities dbA = new nominusAppEntities();
            //TODO:  Extender Pre control de horario a otros servidores, 8 es uladech
            if (dHoy == null)
            {
                dHoy = DateTime.Now;
            }
            List<string> faltantes = new List<string>();
            authServidor _srv = dbA.authServidor.Where(c => c.sServidor == 8).FirstOrDefault();
            nominaEntities db = new nominaEntities(_srv.xConexion);
            uladechEntities dbU = new uladechEntities();
            JavaScriptSerializer jss = new JavaScriptSerializer();
            string xApi = String.Format("asi/diario?dFecha={0:yyyy-MM-dd}", dHoy);
            HttpWebRequest rqsD = (HttpWebRequest)WebRequest.Create(apiUladech + xApi);
            rqsD.Method = "GET";
            rqsD.ContentType = "application/json:charset=\"utf-8\"";
            string tkn = Token.getToken(xSujeto, 128, lUTC: true);
            rqsD.Headers.Add("xTokenMin", tkn);
            WebResponse rspD = rqsD.GetResponse();
            Stream strD = rspD.GetResponseStream();
            StreamReader strrD = new StreamReader(strD);
            string jsDiario = strrD.ReadToEnd();
            List<RspHorarioDiario> horario = jss.Deserialize<List<RspHorarioDiario>>(jsDiario);
            List<asi_HorarioDiario> todoH = db.asi_HorarioDiario.ToList();
            db.asi_HorarioDiario.RemoveRange(todoH);
            db.asi_HorarioDiario.AddRange(horario.Where(c => c.cActividad != null).Select(c => new asi_HorarioDiario
            {
                cEmpleado = c.cEmpleado,
                bControl = c.bControl,
                cHora = c.cHora,
                cActividad = c.cActividad,
                cAula = c.cAula
            }).ToList());
            db.SaveChanges();
            return db.asi_HorarioDiario.ToList();
        }

        public static void empleadoUladech(RespFirebase uchFirebase)
        {
            uchFirebase.uchempleado = EmpleadoUladech(uchFirebase.usuario.email);
        }

        public static List<string> IndiceUladech()
        {
            JavaScriptSerializer jss = new JavaScriptSerializer();
            HttpWebRequest rqst = (HttpWebRequest)WebRequest.Create(apiUladech + "per/indice");
            rqst.Method = "GET";
            rqst.ContentType = "application/json;charset=\"utf-8\"";
            rqst.Headers.Add("xTokenMin", Token.getToken(xSujeto, 128, lUTC: true));
            WebResponse webRsp = rqst.GetResponse();
            Stream webStream2 = webRsp.GetResponseStream();
            StreamReader rspReader = new StreamReader(webStream2);
            string rt = rspReader.ReadToEnd();
            List<uIndice> rspi = jss.Deserialize<List<uIndice>>(rt);
            return rspi.Select(c => c.cCodigoEmpleado).ToList();
        }

        public static OuchEmpleado EmpleadoUladech(string cCodigoUsuario)
        {
            OuchEmpleado ouch = new OuchEmpleado();
            JavaScriptSerializer jss = new JavaScriptSerializer();
            HttpWebRequest rqst2 = (HttpWebRequest)WebRequest.Create(apiUladech + "per/persona?cPersona=" + cCodigoUsuario);
            rqst2.Method = "GET";
            string FechaNula = (char)34 + "0000-00-00" + (char)34;
            rqst2.ContentType = "application/json;charset=\"utf-8\"";
            rqst2.Headers.Add("xTokenMin", Token.getToken(xSujeto, 128, lUTC: true));
            WebResponse webRsp2 = rqst2.GetResponse();
            Stream webStream2 = webRsp2.GetResponseStream();
            StreamReader rspReader2 = new StreamReader(webStream2);
            string rt2 = rspReader2.ReadToEnd();
            rt2 = rt2.Replace(FechaNula, "null");
            if (rt2.IndexOf("[") < 4) rt2 = "null";
            ouch = jss.Deserialize<OuchEmpleado>(rt2);
            if (ouch != null)
            {
                uladechEntities dbU = new uladechEntities();
                USERINFO uzk = dbU.USERINFO.FirstOrDefault(c => c.Badgenumber == ouch.cCodigoEmpleado);
                if (uzk != null)
                {
                    ouch.iZkEmpleado = uzk.USERID;
                }
            }
            return ouch;
        }

        public static async Task<OuchEmpleado> EmpleadoUladech(int iEmpleado)
        {
            OuchEmpleado rsp = new OuchEmpleado();
            uladechEntities dbU = new uladechEntities();
            string cCodEmpleado = dbU.USERINFO.Find(iEmpleado).Badgenumber;
            if(cCodEmpleado!=null) rsp = EmpleadoUladech(cCodEmpleado);
            await Task.Delay(1);
            return rsp;
        }

        public static async Task<OuchLogin> LoginUladech(string email)
        {
            nominusAppEntities dbA = new nominusAppEntities();
            OuchLogin ouch = new OuchLogin();
            JavaScriptSerializer jss = new JavaScriptSerializer();
            uladechEntities dbU = new uladechEntities();

            authUsuario _usu = dbA.authUsuario.Where(c => c.email == email).FirstOrDefault();
            if (_usu == null)
            {
                HttpWebRequest requestCodigo = (HttpWebRequest)WebRequest.Create(apiUladech + "aut/accesos?xEmail" + email);
                requestCodigo.Method = "GET";
                requestCodigo.ContentType = "application/json;charset=\"utf-8\"";
                requestCodigo.Headers.Add("xTokenMin", Token.getToken(xSujeto, 128, lUTC: true));
                try
                {
                    WebResponse rspCodigo = requestCodigo.GetResponse();
                    Stream codStream = rspCodigo.GetResponseStream();
                    StreamReader rspReader = new StreamReader(codStream);
                    string codTE = rspReader.ReadToEnd();
                    OuchLogin codigoLogin;
                    if (codTE != "[]")
                    {
                        codigoLogin = jss.Deserialize<OuchLogin>(codTE);
                        authProvider _prv = dbA.authProvider.Where(c => c.providerId == "google.com").FirstOrDefault();
                        _usu = new authUsuario
                        {
                            bProvider = _prv.bProvider,
                            email = email,
                            gUsuario = Guid.NewGuid()
                        };
                        dbA.authUsuario.Add(_usu);
                        dbA.SaveChanges();
                        USERINFO _user = dbU.USERINFO.Where(c => c.Badgenumber == codigoLogin.cCodigoUsuario).FirstOrDefault();
                        authAcceso _acc = new authAcceso
                        {
                            gAcceso = Guid.NewGuid(),
                            gUsuario = _usu.gUsuario,
                            sServidor = 8,
                            iPersona = _user.USERID
                        };
                        dbA.authAcceso.Add(_acc);
                        dbA.SaveChanges();
                    }
                }
                finally { }               
            }
            if (_usu != null)
            {
                authAcceso acceso = dbA.authAcceso.Where(c => c.sServidor == 8 && c.gUsuario == _usu.gUsuario).FirstOrDefault();
                USERINFO user = dbU.USERINFO.Find(acceso.iPersona);
                HttpWebRequest request1 = (HttpWebRequest)WebRequest.Create(apiUladech + "aut/accesos?cCodigoEmpleado=" + user.Badgenumber);
                request1.Method = "GET";
                request1.ContentType = "application/json;charset=\"utf-8\"";
                request1.Headers.Add("xTokenMin", Token.getToken(xSujeto, 128, lUTC: true));
                try
                {
                    WebResponse webResp = request1.GetResponse();
                    Stream webStream = webResp.GetResponseStream();
                    StreamReader rspReader = new StreamReader(webStream);
                    string rte = rspReader.ReadToEnd();
                    ouch = jss.Deserialize<OuchLogin>(rte);
                }
                catch { }
            }
            await Task.Delay(1);
            return ouch;
        }

        public static void EnlazarAppNominus(ParEnlazar parEnlazar)
        {
            new nominusAppEntities().authCrearEnlace(parEnlazar.sServidor, parEnlazar.iUsuario, parEnlazar.xCorreo);
        }

        public static async void CompletarEnlaces(authUsuario usuario)
        {
            nominusAppEntities dbA = new nominusAppEntities();
            List<authServidor> _srvs = dbA.authServidor.ToList();
            foreach (authServidor _srv in _srvs)
            {
                if (_srv.sServidor == 8)
                {
                    //uladech
                    uladechEntities db = new uladechEntities();
                    USERINFO usr = db.USERINFO.FirstOrDefault(c => c.email == usuario.email);
                    if (usr != null)
                    {

                        dbA.authCrearEnlace(_srv.sServidor, usr.USERID, usr.email);
                    }
                }
                else
                {
                    // no uladech
                    nominaEntities db = new nominaEntities(_srv.xConexion);
                    autUsuario usr = db.autUsuario.FirstOrDefault(c => c.xEmail == usuario.email);
                    if (usr != null)
                    {
                        dbA.authCrearEnlace(_srv.sServidor, usr.iUsuario, usr.xEmail);
                    }
                }
            }
            await Task.Delay(1);
        }

        public static async Task<List<int>> Perfiles(string xEmail)
        {
            nominusAppEntities dbA = new nominusAppEntities();
            Guid gUsuario = dbA.authUsuario.FirstOrDefault(c => c.email == xEmail).gUsuario;

            List<authServidor> _srvs = await ServersxPersona(gUsuario);
            List<int> resp = new List<int>();
            List<int> tempLst = new List<int>();

            foreach (authServidor _srv in _srvs)
            {
                if (_srv.sServidor != 8)
                {
                    nominaEntities db = new nominaEntities(_srv.xConexion);
                    List<authAcceso> _accesos = dbA.authAcceso.Where(c => c.gUsuario == gUsuario && c.sServidor == _srv.sServidor).ToList();
                    foreach (authAcceso _acceso in _accesos)
                    {
                        List<AutContratos> _contratos = (await db.autContratos((int)_acceso.iPersona)).OrderBy(c => c.sEmpresa).ToList();
                        if (_contratos.Count > 0)
                        {
                            tempLst.Add(1);  // acceso a boletas en cualquier estado contractual
                            tempLst.Add(2);  // acceso a vacaciones en cualquier estado contractual
                        }
                        foreach (AutContratos _contrato in _contratos)
                        {
                            if (db.traMarcacion.Where(c => c.iContrato == _contrato.iContrato).ToList().Count > 0)
                            {
                                tempLst.Add(3); //acceso a asistencia calificada (que marcan reloj)
                            }
                            if (_contrato.dHasta == null || _contrato.dHasta > DateTime.Now)  //contrato vigente
                            {
                                //if (db.webPrestamos(_contrato.iContrato).ToList().Count > 0)
                                //{
                                //    tempLst.Add(4); //tiene algun prestamo
                                //}
                                if (db.traJefeDirecto.Where(c => c.iTrabajador == _acceso.iPersona).ToList().Count > 0)
                                {
                                    tempLst.Add(5); //es jefe de alguien
                                }
                            }
                        }
                    }
                }
            }
            for (int ii = 1; ii < 100; ii++)
            {
                if (tempLst.Contains(ii)) resp.Add(ii);
            }
            return resp;
        }

        public static string EnviarPush(NotiPush notiPush)
        {
            string rsp;
            JavaScriptSerializer jss = new JavaScriptSerializer();
            string xPush = jss.Serialize(notiPush);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://fcm.googleapis.com/fcm/send");
            request.Method = "POST";
            request.ContentType = "application/json;charset=\"utf-8\"";
            request.Headers.Add("Authorization", "key=AAAAsb5bga8:APA91bGQ2z97vHe6xoKeJmpkED8UPRZ_bK-X3FGyyZXFV9DTEzrOOPxgFxA7KUu2-TME47Sz-W3CGSgAxgVLjsfBiIQzdmyjHz-HjXIMqmcXFXs8PjJz27GuN4lwRJ_aviDEtKdREiGvMZ7BMXVtwP0k-Ou6jILb7w");
            request.ContentLength = xPush.Length;
            StreamWriter requestWriter = new StreamWriter(request.GetRequestStream(), System.Text.Encoding.ASCII);
            requestWriter.Write(xPush);
            requestWriter.Close();
            try
            {
                WebResponse webResp = request.GetResponse();
                Stream webStream = webResp.GetResponseStream();
                StreamReader rspReader = new StreamReader(webStream);
                rsp = rspReader.ReadToEnd();
                rspReader.Close();
            }
            catch (Exception e)
            {
                rsp = e.Message;
            }
            return rsp;
        }

        public static async Task<string> ServerConnection(string xSrv)
        {
            nominusAppEntities dbA = new nominusAppEntities();
            string xEntity = "nominaEntities";
            if (xSrv != null && Regex.IsMatch(xSrv, @"^\d+$"))
            {
                authServidor _servidor = dbA.authServidor.Find(int.Parse(xSrv));
                if (_servidor != null)
                {
                    xEntity = _servidor.xConexion;
                }
            }
            await Task.Delay(1);
            return xEntity;
        }

        public static bool RayCasting(GeoPunto punto, List<GeoPunto> poligono)
        {
            int contador = 0;
            for (int ii = 0; ii < poligono.Count; ii++)
            {
                if (aDerecha(poligono[ii], poligono[(ii + 1) % poligono.Count], punto))
                {
                    contador++;
                }
            }
            return (contador % 2 == 1);
        }

        private static bool aDerecha(GeoPunto A, GeoPunto B, GeoPunto x)
        {
            if (A.fLon <= B.fLon)
            {
                if (x.fLon <= A.fLon || x.fLon > B.fLon || x.fLat >= A.fLat && x.fLat >= B.fLat)
                {
                    return false;
                }
                else if (x.fLat < A.fLat && x.fLat < B.fLat)
                {
                    return true;
                }
                else
                {
                    return (x.fLon - A.fLon) / (x.fLat - A.fLat) > (B.fLon - A.fLon) / (B.fLat - A.fLat);
                }
            }
            else {
                return aDerecha(B, A, x);
            }
        }

    }

    public class RspHorarioDiario
    {
        public string cEmpleado { get; set; }
        public short bControl { get; set; }
        public string cHora { get; set; }
        public string cActividad { get; set; }
        public string cAula { get; set; }
    }

    public class NotiNotification
    {
        public string title { get; set; }
        public string body { get; set; }
        public string icon { get; set; }
        public webNotificar data { get; set; }
    }

    public class ParBuscaPersona
    {
        public string xCargoParcial { get; set; }
        public string xNombreParcial { get; set; }
        public string xJefe { get; set; }
        public string xSituacion { get; set; }
        public short bCantidad { get; set; }
    }

    public class RspBuscaPersona
    {
        public string xCargo { get; set; }
        public string xEmail { get; set; }
        public string xNombre { get; set; }
        public string cSituacion { get; set; }
        public string xApellidos { get; set; }
        public string cJefeDirecto { get; set; }
        public string cCodigoEmpleado { get; set; }
        public List<string> cEstablecimiento { get; set; }
    }


    public class NotiPush
    {
        public NotiNotification notification { get; set; }
        public string to { get; set; }
    }

    public class RspGeoUbicacion
    {
        public int iEstablecimiento { get; set; }
        public string cEstablecimiento { get; set; }
        public short sOrtogono { get; set; }
        public bool lMatch { get; set; }
        public List<AgmPunto> puntos { get; set; }
    }

    public class aut_Contratos : AutContratos
    {
        public short sServidor { get; set; }
    }

    public class hdContrato
    {
        public short sServidor { get; set; }
        public int iContrato { get; set; }
    }

    public class BigBelCalculos : belCalculos
    {
        public short sServidor { get; set; }
    }

    public class R_servidor
    {
        public short sServidor { get; set; }
        public List<R_empresa> empresas { get; set; }
        public int? iPersona { get; set; }
    }

    public class R_empresa
    {
        public short sEmpresa { get; set; }
        public string xRazonSocial { get; set; }
        public string xAbreviado { get; set; }
        public string jImagen { get; set; }
        public List<R_contrato> contratos { get; set; }

    }

    public class R_contrato
    {
        public int iContrato { get; set; }
        public short sCargo { get; set; }
        public string xCargo { get; set; }
        public DateTime? dDesde { get; set; }
        public DateTime? dHasta { get; set; }
        public List<int> accesos { get; set; }

    }

    public class OuchEmpleado
    {
        public string xCargo { get; set; }
        public string xEmail { get; set; }
        public string xNombre { get; set; }
        public string cSituacion { get; set; }
        public string xApellidos { get; set; }
        public string cJefeDirecto { get; set; }
        public string cCodigoEmpleado { get; set; }
        public Nullable<int> iZkEmpleado { get; set; }
        public List<string> cEstablecimiento { get; set; }
        public Nullable<DateTime> dDesde { get; set; }
        public Nullable<DateTime> dHasta { get; set; }
        public Nullable<DateTime> dNacimiento { get; set; }
        public string cGenero { get; set; }
    }

    public class OuchLogin
    {
        public string xEmail { get; set; }
        public string cCodigoUsuario { get; set; }
        public List<int> bAccesos { get; set; }
    }

    public class RespFirebase
    {
        public Nullable<Guid> token { get; set; }
        public authUsuario usuario { get; set; }
        public List<R_servidor> contratos { get; set; }
        public List<int> perfiles { get; set; }
        public OuchEmpleado uchempleado { get; set; }
    }

    public class ParEnlazar
    {
        public short sServidor { get; set; }
        public string xCorreo { get; set; }
        public int iUsuario { get; set; }
    }

    public class uIndice
    {
        public string cCodigoEmpleado { get; set; }
    }

    public class GeoPunto
    {
        public double? fLat { get; set; }
        public double? fLon { get; set; }
    }

    public class AgmPunto
    {
        public double lat { get; set; }
        public double lng { get; set; }

    }

    public class ParCaminar
    {
        public List<string> establecimientos { get; set; }
        public GeoPunto punto { get; set; }
    }

    public class ParPoligono
    {
        public string cEstablecimiento { get; set; }
        public short sOrtogono { get; set; }
    }
}
