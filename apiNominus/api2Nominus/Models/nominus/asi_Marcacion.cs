﻿using System;
using System.Collections.Generic;

namespace api2Nominus.Models.nominus
{
    public partial class asi_Marcacion
    {
        public int iMarcacion { get; set; }
        public short? sDispositivo { get; set; }
        public short? sEstablecimiento { get; set; }
        public DateTime? dHora { get; set; }
        public int? iPersona { get; set; }
        public int? zkId { get; set; }

    }
}
