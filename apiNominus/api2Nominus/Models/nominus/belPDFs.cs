﻿using System;
using System.Collections.Generic;

namespace api2Nominus.Models.nominus
{
    public partial class belPDFs
    {
        public string cPDF { get; set; }
        public int? iCalculoPlanilla { get; set; }
        public DateTime? dGeneracion { get; set; }
    }
}
