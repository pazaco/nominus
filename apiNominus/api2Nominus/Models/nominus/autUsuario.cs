﻿using System;
using System.Collections.Generic;

namespace api2Nominus.Models.nominus
{
    public partial class autUsuario
    {
        public int iUsuario { get; set; }
        public string xEmail { get; set; }
        public byte[] hSemilla { get; set; }
        public byte[] hPassword { get; set; }
        public string xPreguntaSecreta { get; set; }
        public byte[] hRespuestaSecreta { get; set; }
        public DateTime? dCreacion { get; set; }
        public DateTime? dUltimoLogin { get; set; }
    }
}
