﻿using System;
using System.Collections.Generic;

namespace api2Nominus.Models.nominus
{
    public partial class asi_Resolucion
    {
        public byte bTipoPapeleta { get; set; }
        public int iPapeleta { get; set; }
        public short sConsecutivo { get; set; }
        public DateTime? dFecha { get; set; }
        public int? iAsistencia { get; set; }
        public short? bControl { get; set; }
        public DateTime? dHoraHorario { get; set; }
        public bool? lDiaCompleto { get; set; }
        public DateTime? dResolucion { get; set; }
    }
}
