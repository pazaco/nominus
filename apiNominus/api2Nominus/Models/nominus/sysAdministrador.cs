﻿using System;
using System.Collections.Generic;

namespace api2Nominus.Models.nominus
{
    public partial class sysAdministrador
    {
 
        public int iPersona { get; set; }
        public DateTime? dVigencia { get; set; }
    }
}
