﻿using System;
using System.Collections.Generic;

namespace api2Nominus.Models.nominus
{
    public partial class traMarcacion
    {
        public long iCalificacionMarcacion { get; set; }
        public long iMarcacion { get; set; }
        public int iContrato { get; set; }
        public DateTime dFecha { get; set; }
        public DateTime dtMarcacion { get; set; }
        public byte bTipoActividadHorario { get; set; }
    }
}
