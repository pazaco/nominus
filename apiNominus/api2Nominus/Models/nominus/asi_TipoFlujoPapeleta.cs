﻿using System;
using System.Collections.Generic;

namespace api2Nominus.Models.nominus
{
    public partial class asi_TipoFlujoPapeleta
    {

        public string cTipoFlujo { get; set; }
        public string xTipoFlujo { get; set; }
    }
}
