﻿using System;
using System.Collections.Generic;

namespace api2Nominus.Models.asistencia
{
    public partial class horHorario
    {
        public horHorario()
        {
            horJornada = new HashSet<horJornada>();
            horProgramacion = new HashSet<horProgramacion>();
        }

        public short sHorario { get; set; }
        public int? iEmpresa { get; set; }
        public string xHorario { get; set; }

        public virtual ICollection<horJornada> horJornada { get; set; }
        public virtual ICollection<horProgramacion> horProgramacion { get; set; }
    }
}
