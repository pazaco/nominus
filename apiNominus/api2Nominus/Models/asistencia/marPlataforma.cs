﻿using System;
using System.Collections.Generic;

namespace api2Nominus.Models.asistencia
{
    public partial class marPlataforma
    {
        public marPlataforma()
        {
            marDispositivo = new HashSet<marDispositivo>();
        }

        public byte bPlataforma { get; set; }
        public string xPlataforma { get; set; }
        public byte? bModo { get; set; }

        public virtual marModo bModoNavigation { get; set; }
        public virtual ICollection<marDispositivo> marDispositivo { get; set; }
    }
}
