﻿using System;
using System.Collections.Generic;

namespace api2Nominus.Models.asistencia
{
    public partial class asiPapeleta
    {
        public asiPapeleta()
        {
            asiControl = new HashSet<asiControl>();
            asiFlujoPapeleta = new HashSet<asiFlujoPapeleta>();
            asiTransaccion = new HashSet<asiTransaccion>();
        }

        public byte bTipoPapeleta { get; set; }
        public int iPapeleta { get; set; }
        public int? iAsistencia { get; set; }
        public short? sIncidencia { get; set; }
        public double? fDebe { get; set; }
        public double? fHaber { get; set; }
        public double? fDescuento { get; set; }
        public double? fExtras { get; set; }
        public double? fInjustificadas { get; set; }
        public DateTime? dAlta { get; set; }

        public virtual asiTipoPapeleta bTipoPapeletaNavigation { get; set; }
        public virtual ICollection<asiControl> asiControl { get; set; }
        public virtual ICollection<asiFlujoPapeleta> asiFlujoPapeleta { get; set; }
        public virtual ICollection<asiTransaccion> asiTransaccion { get; set; }
    }
}
