﻿using System;
using System.Collections.Generic;

namespace api2Nominus.Models.asistencia
{
    public partial class marDispositivo
    {
        public marDispositivo()
        {
            marMarcacion = new HashSet<marMarcacion>();
        }

        public short sDispositivo { get; set; }
        public string xDescripcion { get; set; }
        public byte? bPlataforma { get; set; }
        public short? sSede { get; set; }

        public virtual marPlataforma bPlataformaNavigation { get; set; }
        public virtual ICollection<marMarcacion> marMarcacion { get; set; }
    }
}
