﻿using System;
using System.Collections.Generic;

namespace api2Nominus.Models.asistencia
{
    public partial class asiFlujoPapeleta
    {
        public string iFlujoPapeleta { get; set; }
        public byte bTipoPapeleta { get; set; }
        public int iPapeleta { get; set; }
        public short sConsecutivo { get; set; }
        public string cTipoFlujo { get; set; }
        public DateTime? dFlujo { get; set; }
        public int? iPersona { get; set; }
        public string cRol { get; set; }
        public string xNotificacion { get; set; }
        public int? iNotificacion { get; set; }

        public virtual asiPapeleta asiPapeleta { get; set; }
        public virtual asiRolNotificador cRolNavigation { get; set; }
        public virtual asiTipoFlujoPapeleta cTipoFlujoNavigation { get; set; }
    }
}
