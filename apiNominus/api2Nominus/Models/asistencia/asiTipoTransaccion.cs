﻿using System;
using System.Collections.Generic;

namespace api2Nominus.Models.asistencia
{
    public partial class asiTipoTransaccion
    {
        public asiTipoTransaccion()
        {
            asiTransaccion = new HashSet<asiTransaccion>();
        }

        public short sTipoTransaccion { get; set; }
        public string xTipoTransaccion { get; set; }

        public virtual ICollection<asiTransaccion> asiTransaccion { get; set; }
    }
}
