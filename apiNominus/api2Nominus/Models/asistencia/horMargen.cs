﻿using System;
using System.Collections.Generic;

namespace api2Nominus.Models.asistencia
{
    public partial class horMargen
    {
        public horMargen()
        {
            horMargenIncidencia = new HashSet<horMargenIncidencia>();
        }

        public short sMargen { get; set; }
        public string xMargen { get; set; }
        public string cTipoControl { get; set; }

        public virtual ICollection<horMargenIncidencia> horMargenIncidencia { get; set; }
    }
}
