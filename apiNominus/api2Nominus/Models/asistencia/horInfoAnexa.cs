﻿using System;
using System.Collections.Generic;

namespace api2Nominus.Models.asistencia
{
    public partial class horInfoAnexa
    {
        public short sInfoAnexa { get; set; }
        public short? sJornada { get; set; }
        public string xDescriptor { get; set; }

        public virtual horJornada sJornadaNavigation { get; set; }
    }
}
