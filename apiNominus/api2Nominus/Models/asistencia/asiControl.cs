﻿using System;
using System.Collections.Generic;

namespace api2Nominus.Models.asistencia
{
    public partial class asiControl
    {
        public asiControl()
        {
            marMarcacion = new HashSet<marMarcacion>();
        }

        public int iControl { get; set; }
        public int? iAsistencia { get; set; }
        public short? sControl { get; set; }
        public DateTime? dHoraHorario { get; set; }
        public DateTime? dHoraControl { get; set; }
        public short? sIncidencia { get; set; }
        public byte? bTipoPapeleta { get; set; }
        public int? iPapeleta { get; set; }

        public virtual asiPapeleta asiPapeleta { get; set; }
        public virtual asiAsistencia iAsistenciaNavigation { get; set; }
        public virtual horIncidencia sIncidenciaNavigation { get; set; }
        public virtual ICollection<marMarcacion> marMarcacion { get; set; }
    }
}
