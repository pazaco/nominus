﻿using System;
using System.Collections.Generic;

namespace api2Nominus.Models.asistencia
{
    public partial class horProgramacion
    {
        public int iProgramacion { get; set; }
        public short? sRotacion { get; set; }
        public int? iContrato { get; set; }
        public short? sHorario { get; set; }
        public DateTime? dDesde { get; set; }
        public DateTime? dHasta { get; set; }
        public double? fSaldoHoras { get; set; }

        public virtual horHorario sHorarioNavigation { get; set; }
        public virtual horTipoRotacion sRotacionNavigation { get; set; }
    }
}
