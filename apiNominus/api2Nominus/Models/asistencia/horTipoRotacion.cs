﻿using System;
using System.Collections.Generic;

namespace api2Nominus.Models.asistencia
{
    public partial class horTipoRotacion
    {
        public horTipoRotacion()
        {
            horProgramacion = new HashSet<horProgramacion>();
        }

        public short sRotacion { get; set; }
        public string cFactor { get; set; }
        public byte? bFactor { get; set; }
        public byte? bCoeficiente { get; set; }

        public virtual ICollection<horProgramacion> horProgramacion { get; set; }
    }
}
