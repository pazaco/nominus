﻿using System;
using System.Collections.Generic;

namespace api2Nominus.Models.auth
{
    public partial class authAcceso
    {
        public Guid gAcceso { get; set; }
        public Guid? gUsuario { get; set; }
        public short? sServidor { get; set; }
        public int? iPersona { get; set; }

    }
}
