﻿using System;
using System.Collections.Generic;

namespace api2Nominus.Models.auth
{
    public partial class authSesion
    {
        public Guid gToken { get; set; }
        public Guid? gUsuario { get; set; }
        public DateTime dInicio { get; set; }
        public DateTime? dFin { get; set; }
        public short? sNavegador { get; set; }
        public string xTokenMsg { get; set; }

    }
}
