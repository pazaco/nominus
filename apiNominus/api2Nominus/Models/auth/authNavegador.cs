﻿using System;
using System.Collections.Generic;

namespace api2Nominus.Models.auth
{
    public partial class authNavegador
    {
        public short sNavegador { get; set; }
        public string xNavegador { get; set; }
        public byte bClaseNavegador { get; set; }
        public int? iLatencia { get; set; }
    }
}
