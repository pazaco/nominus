﻿using api2Nominus.Models.nominus;
using api2Nominus.Models.asistenciaPK;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace api2Nominus.Models
{
    public partial class nominaEntities : DbContext
    {

        private string xConn;
        private static string cs;

        public nominaEntities(string xConn = "nominaEntities")
        {
            this.xConn = xConn;
            Database.SetCommandTimeout(25000);
        }

        public nominaEntities(DbContextOptions<nominaEntities> options)
            : base(options)
        { }

        public virtual DbSet<appVacaciones> appVacaciones { get; set; }
        public virtual DbSet<asi_aggControlxEstablecimiento> _asi_aggControlxEstablecimiento { get; set; }
        public virtual DbSet<asi_Asistencia> asi_Asistencia { get; set; }
        public virtual DbSet<asi_AsistenciaDocente> asi_AsistenciaDocente { get; set; }
        public virtual DbSet<asi_Calificacion> asi_Calificacion { get; set; }
        public virtual DbSet<asi_CalificacionMensual> _asi_CalificacionMensual { get; set; }
        public virtual DbSet<asi_Control> asi_Control { get; set; }
        public virtual DbSet<asi_detFlujoPapeleta> _asi_detFlujoPapeleta { get; set; }
        public virtual DbSet<asi_detResolucion> _asi_detResolucion { get; set; }
        public virtual DbSet<asi_DiarioAsistencia> _asi_DiarioAsistencia { get; set; }
        public virtual DbSet<asi_DiarioControles> _asi_DiarioControles { get; set; }
        public virtual DbSet<asi_DiarioEstablecimientos> _asi_DiarioEstablecimientos { get; set; }
        public virtual DbSet<asi_DiarioFlujo> _asi_DiarioFlujo { get; set; }
        public virtual DbSet<asi_DiarioIncidencias> _asi_DiarioIncidencias { get; set; }
        public virtual DbSet<asi_FlujoDiarioEmpleado> _asi_FlujoDiarioEmpleado { get; set; }
        public virtual DbSet<asi_grafMarcado> _asi_grafMarcado { get; set; }
        public virtual DbSet<asi_IndicePapeletas> _asi_IndicePapeletas { get; set; }
        public virtual DbSet<asi_MarcacionDiarioEmpleado> _asi_MarcacionDiarioEmpleado { get; set; }
        public virtual DbSet<asi_Dispositivo> asi_Dispositivo { get; set; }
        public virtual DbSet<asi_Flujo> asi_Flujo { get; set; }
        public virtual DbSet<asi_FlujoDocente> asi_FlujoDocente { get; set; }
        public virtual DbSet<asi_FlujoPapeleta> asi_FlujoPapeleta { get; set; }
        public virtual DbSet<asi_HorarioDiario> asi_HorarioDiario { get; set; }
        public virtual DbSet<asi_Incidencia> asi_Incidencia { get; set; }
        public virtual DbSet<asi_Jefatura> asi_Jefatura { get; set; }
        public virtual DbSet<asi_Marcacion> asi_Marcacion { get; set; }
        public virtual DbSet<asi_Papeleta> asi_Papeleta { get; set; }
        public virtual DbSet<asi_Reclutamiento> _asi_Reclutamiento { get; set; }
        public virtual DbSet<asi_Resolucion> asi_Resolucion { get; set; }
        public virtual DbSet<asi_TipoFlujoPapeleta> asi_TipoFlujoPapeleta { get; set; }
        public virtual DbSet<asi_TipoPapeleta> asi_TipoPapeleta { get; set; }
        public virtual DbSet<autUsuario> autUsuario { get; set; }
        public virtual DbSet<orgGeoEstablecimiento> orgGeoEstablecimiento { get; set; }
        public virtual DbSet<sysAdministrador> sysAdministrador { get; set; }
        public virtual DbSet<traJefeDirecto> traJefeDirecto { get; set; }
        public virtual DbSet<traMarcacion> traMarcacion { get; set; }
        public virtual DbSet<traPrestamo> traPrestamo { get; set; }
        public virtual DbSet<vozFingerPrint> vozFingerPrint { get; set; }
        public virtual DbSet<vozFrase> vozFrase { get; set; }
        public virtual DbSet<vozMarcando> vozMarcando { get; set; }
        public virtual DbSet<vozTrack> vozTrack { get; set; }
        public virtual DbSet<webFirma> webFirma { get; set; }
        public virtual DbSet<webNotificacion> webNotificacion { get; set; }
        public virtual DbSet<autSesion> autSesion { get; set; }
        public virtual DbSet<autUsuarioClaim> autUsuarioClaim { get; set; }
        public virtual DbSet<perPersona> perPersona { get; set; }
        public virtual DbSet<traContrato> traContrato { get; set; }
        public virtual DbSet<AutContratos> _autContratos { get; set; }
        public virtual DbSet<WebPrestamos> _webPrestamos { get; set; }
        public virtual DbSet<autAvatar> _autAvatar { get; set; }
        public virtual DbSet<autAvatarRegistro> _autAvatarRegistro { get; set; }
        public virtual DbSet<belCalculos> _belCalculos { get; set; }
        public virtual DbSet<webNotificar> _webNotificar { get; set; }
        public virtual DbSet<scalarString> _scalarString { get; set; }
        public virtual DbSet<scalarNumero> _scalarNumero { get; set; }
        public virtual DbSet<belPDFs> belPDFs { get; set; }
        public virtual DbSet<orgEmpresaImagen> orgEmpresaImagen { get; set; }
        public virtual DbSet<remCalculoPlanilla> remCalculoPlanilla { get; set; }
        public virtual DbSet<belCalculosxPeriodo> _belCalculosxPeriodo { get; set; }
        public virtual DbSet<remBoletaCabecera> _remBoletaCabecera { get; set; }
        public virtual DbSet<remBoletaCabeceraEmpresa> _remBoletaCabeceraEmpresa { get; set; }
        public virtual DbSet<remBoletaCabeceraTitulo> _remBoletaCabeceraTitulo { get; set; }
        public virtual DbSet<remBoletaCabeceraAsistenciaFechas> _remBoletaCabeceraAsistenciaFechas { get; set; }
        public virtual DbSet<remBoletaCabeceraDatos1> _remBoletaCabeceraDatos1 { get; set; }
        public virtual DbSet<remBoletaCabeceraDatos2> _remBoletaCabeceraDatos2 { get; set; }
        public virtual DbSet<remBoletaConcepto> _remBoletaConcepto { get; set; }
        public virtual DbSet<traContratosxPersona> _traContratosxPersona { get; set; }
        public virtual DbSet<webPagosPrestamo> _webPagosPrestamo { get; set; }
        public virtual DbSet<webVacaciones> _webVacaciones { get; set; }
        public virtual DbSet<asi_PlanoMarcacion> _planoMarcacion { get; set; }
        public virtual DbSet<asi_PlanoHorarioControl> _planoHorarioControl { get; set; }
        public virtual DbSet<asi_PlanoAsistencia> _planoAsistencia { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                cs = ConfigurationManager.ConnectionStrings[this.xConn].ConnectionString;
                optionsBuilder.UseSqlServer(cs);
            }
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.4-servicing-10062")
                .HasAnnotation("Relational:DefaultSchema", "dbo");

            modelBuilder.Entity<appVacaciones>(entity =>
            {
                entity.HasKey(e => e.iNovedadVacaciones)
                    .HasName("PK__appVacac__465A4C2FB050FB82");

                entity.ToTable("appVacaciones", "dbo");

                entity.Property(e => e.cTipoNovedad)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.dDesde).HasColumnType("date");

                entity.Property(e => e.dHasta).HasColumnType("date");

                entity.Property(e => e.xDescripcion).IsUnicode(false);
            });

            modelBuilder.Entity<asi_Asistencia>(entity =>
            {
                entity.HasKey(e => e.iAsistencia);

                entity.ToTable("asi_Asistencia", "dbo");

                entity.Property(e => e.cPlanilla)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.dFecha).HasColumnType("date");
            });

            modelBuilder.Entity<asi_AsistenciaDocente>(entity =>
            {
                entity.HasKey(e => e.iAsistencia);

                entity.ToTable("asi_AsistenciaDocente", "dbo");

                entity.Property(e => e.iAsistencia).ValueGeneratedNever();

                entity.Property(e => e.cActividad)
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.cAula)
                    .HasMaxLength(12)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<asi_Calificacion>(entity =>
            {
                entity.HasKey(e => new { e.bControl, e.sCalificador });

                entity.ToTable("asi_Calificacion", "dbo");
            });

            modelBuilder.Entity<asi_Control>(entity =>
            {
                entity.HasKey(e => e.bControl);

                entity.ToTable("asi_Control", "dbo");

                entity.Property(e => e.bControl).ValueGeneratedNever();

                entity.Property(e => e.cHora).HasColumnType("time(0)");

                entity.Property(e => e.cTipoControl)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.xControl)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<asi_Dispositivo>(entity =>
            {
                entity.HasKey(e => e.sDispositivo);

                entity.ToTable("asi_Dispositivo", "dbo");

                entity.Property(e => e.sDispositivo).ValueGeneratedNever();

                entity.Property(e => e.xDispositivo)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<asi_Flujo>(entity =>
            {
                entity.HasKey(e => new { e.iAsistencia, e.bControl, e.dHoraHorario });

                entity.ToTable("asi_Flujo", "dbo");

                entity.Property(e => e.dHoraHorario).HasColumnType("datetime");

                entity.Property(e => e.dHoraControl).HasColumnType("datetime");

            });

            modelBuilder.Entity<asi_FlujoDocente>(entity =>
            {
                entity.HasKey(e => e.iFlujoDocente);

                entity.ToTable("asi_FlujoDocente", "dbo");

                entity.Property(e => e.cActividad)
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.cAula)
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.dHoraHorario).HasColumnType("datetime");
            });

            modelBuilder.Entity<asi_FlujoPapeleta>(entity =>
            {
                entity.HasKey(e => new { e.bTipoPapeleta, e.iPapeleta, e.iConsecutivo });
                entity.ToTable("asi_FlujoPapeleta", "dbo");
                entity.Property(e => e.cTipoFlujo)
                    .HasMaxLength(1)
                    .IsUnicode(false);
                entity.Property(e => e.dFlujo).HasColumnType("datetime");
                entity.Property(e => e.xObservacion).IsUnicode(false);
            });

            modelBuilder.Entity<asi_HorarioDiario>(entity =>
            {
                entity.HasKey(e => e.iHorarioDiario);

                entity.ToTable("asi_HorarioDiario", "dbo");

                entity.Property(e => e.cActividad)
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.cAula)
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.cEmpleado)
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.cHora)
                    .HasMaxLength(8)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<asi_Incidencia>(entity =>
            {
                entity.HasKey(e => e.sIncidencia);

                entity.ToTable("asi_Incidencia", "dbo");

                entity.Property(e => e.sIncidencia).ValueGeneratedNever();

                entity.Property(e => e.xIncidencia)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<asi_Jefatura>(entity =>
            {
                entity.HasKey(e => new { e.iContrato, e.iContratoJefe });

                entity.ToTable("asi_Jefatura", "dbo");

                entity.Property(e => e.bTipoJefe).HasDefaultValueSql("((1))");

                entity.Property(e => e.dVigente).HasColumnType("date");
            });


            modelBuilder.Entity<asi_Marcacion>(entity =>
            {
                entity.HasKey(e => e.iMarcacion);
                entity.ToTable("asi_Marcacion", "dbo");
                entity.Property(e => e.dHora).HasColumnType("datetime");
            });

            modelBuilder.Entity<asi_Papeleta>(entity =>
            {
                entity.HasKey(e => new { e.bTipoPapeleta, e.iPapeleta });
                entity.ToTable("asi_Papeleta", "dbo");
                entity.Property(e => e.cNotificar)
                    .HasMaxLength(1)
                    .IsUnicode(false);
                entity.Property(e => e.cNumeroFisico)
                    .HasMaxLength(12)
                    .IsUnicode(false);
                entity.Property(e => e.dCreacion).HasColumnType("date");
                entity.Property(e => e.dJefe).HasColumnType("date");
                entity.Property(e => e.dRRHH).HasColumnType("date");
            });

            modelBuilder.Entity<asi_Resolucion>(entity =>
            {
                entity.HasKey(e => new { e.bTipoPapeleta, e.iPapeleta, e.sConsecutivo });
                entity.ToTable("asi_Resolucion", "dbo");
                entity.Property(e => e.dFecha).HasColumnType("date");
                entity.Property(e => e.dHoraHorario).HasColumnType("datetime");
                entity.Property(e => e.dResolucion).HasColumnType("datetime");
            });

            modelBuilder.Entity<asi_TipoFlujoPapeleta>(entity =>
            {
                entity.HasKey(e => e.cTipoFlujo);

                entity.ToTable("asi_TipoFlujoPapeleta", "dbo");

                entity.Property(e => e.cTipoFlujo)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.xTipoFlujo)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<asi_TipoPapeleta>(entity =>
            {
                entity.HasKey(e => e.bTipoPapeleta);

                entity.ToTable("asi_TipoPapeleta", "dbo");

                entity.Property(e => e.xTipoPapeleta)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<autUsuario>(entity =>
            {
                entity.HasKey(e => e.iUsuario);

                entity.ToTable("autUsuario", "dbo");

                entity.HasIndex(e => e.xEmail)
                    .HasName("IX_xEmail_unique")
                    .IsUnique();

                entity.Property(e => e.iUsuario).ValueGeneratedNever();

                entity.Property(e => e.dCreacion)
                    .HasColumnType("datetime2(0)")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.dUltimoLogin).HasColumnType("datetime2(0)");

                entity.Property(e => e.hSemilla).HasMaxLength(64);

                entity.Property(e => e.xEmail)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.xPreguntaSecreta)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<orgGeoEstablecimiento>(entity =>
            {
                entity.HasKey(e => new { e.cEstablecimiento, e.sOrtogono, e.sPosicion });

                entity.ToTable("orgGeoEstablecimiento", "dbo");

                entity.Property(e => e.cEstablecimiento)
                    .HasMaxLength(4)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<traJefeDirecto>(entity =>
            {
                entity.HasKey(e => e.iJefeDirecto);

                entity.ToTable("traJefeDirecto", "dbo");
            });

            modelBuilder.Entity<traMarcacion>(entity =>
            {
                entity.HasKey(e => e.iCalificacionMarcacion);

                entity.ToTable("traMarcacion", "dbo");

                entity.Property(e => e.dFecha).HasColumnType("date");

                entity.Property(e => e.dtMarcacion).HasColumnType("datetime2(0)");
            });


            modelBuilder.Entity<vozFingerPrint>(entity =>
            {
                entity.HasKey(e => e.iFingerPrint);

                entity.ToTable("vozFingerPrint", "dbo");

                entity.Property(e => e.xFingerPrint).IsUnicode(false);

                entity.Property(e => e.xSoporte)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<vozFrase>(entity =>
            {
                entity.HasKey(e => e.bFrase);

                entity.ToTable("vozFrase", "dbo");

                entity.Property(e => e.xFrase)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<vozMarcando>(entity =>
            {
                entity.HasKey(e => e.iMarcando);

                entity.ToTable("vozMarcando", "dbo");

                entity.Property(e => e.iMarcando).ValueGeneratedNever();

                entity.Property(e => e.xFingerPrint).HasMaxLength(10);

            });

            modelBuilder.Entity<vozTrack>(entity =>
            {
                entity.HasKey(e => e.iTrack);

                entity.ToTable("vozTrack", "dbo");

            });

            modelBuilder.Entity<webFirma>(entity =>
            {
                entity.HasKey(e => e.iFirma);

                entity.ToTable("webFirma", "dbo");

                entity.Property(e => e.dFirmado).HasColumnType("datetime2(0)");

                entity.Property(e => e.dLeido).HasColumnType("datetime2(0)");

            });

            modelBuilder.Entity<webNotificacion>(entity =>
            {
                entity.HasKey(e => e.iNotificacion);

                entity.ToTable("webNotificacion", "dbo");

                entity.Property(e => e.dEnvio).HasColumnType("datetime2(0)");

                entity.Property(e => e.xAsunto).IsUnicode(false);
            });

            modelBuilder.Entity<AutContratos>(entity =>
            {
                entity.HasKey(ak => ak.iContrato);
            });

            modelBuilder.Entity<WebPrestamos>(entity =>
            {
                entity.HasKey(ak => ak.iPrestamo);
            });

            modelBuilder.Entity<autSesion>(entity =>
            {
                entity.HasKey(e => e.gSesion)
                    .HasName("PK_autSesion_1");

                entity.ToTable("autSesion", "dbo");

                entity.Property(e => e.gSesion).HasDefaultValueSql("(newid())");

                entity.Property(e => e.cClaim)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.dVigencia).HasColumnType("datetime");

            });

            modelBuilder.Entity<autUsuarioClaim>(entity =>
            {
                entity.HasKey(e => new { e.iUsuario, e.cClaim });

                entity.ToTable("autUsuarioClaim", "dbo");

                entity.Property(e => e.cClaim)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.xValor).IsUnicode(false);
            });

            modelBuilder.Entity<perPersona>(entity =>
            {
                entity.HasKey(e => e.iPersona)
                    .HasName("PK_Persona");

                entity.ToTable("perPersona", "dbo");

                entity.Property(e => e.dNacimiento).HasColumnType("date");

                entity.Property(e => e.xApellidoMaterno)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.xApellidoPaterno)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.xConcatenado)
                    .HasMaxLength(229)
                    .IsUnicode(false)
                    .HasComputedColumnSql("(((((((([xDocIdentidad]+' ')+[xApellidoPaterno])+' ')+[xApellidoMaterno])+' ')+[xNombres])+' ')+CONVERT([char](10),[dNacimiento],(103)))");

                entity.Property(e => e.xDocIdentidad)
                    .IsRequired()
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.xNombres)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<sysAdministrador>(entity =>
            {
                entity.HasKey(e => e.iPersona);
            });

            modelBuilder.Entity<traContrato>(entity =>
            {
                entity.HasKey(e => e.iContrato);

                entity.ToTable("traContrato", "dbo");

                entity.Property(e => e.sCargo).HasDefaultValueSql("((1))");

                entity.Property(e => e.sSede).HasDefaultValueSql("((1))");

            });

            modelBuilder.Entity<autAvatar>(ent =>
            {
                ent.HasKey(c => c.iPersona);
            });

            modelBuilder.Entity<autAvatarRegistro>(ent =>
            {
                ent.HasKey(c => c.xEmail);
            });
            modelBuilder.Entity<belCalculos>(ent =>
            {
                ent.HasKey(c => c.iCalculoPlanilla);
            });
            modelBuilder.Entity<webNotificar>(ent =>
            {
                ent.HasKey(c => c.iNotificacion);
            });
            modelBuilder.Entity<scalarString>(ent =>
            {
                ent.HasKey(c => c.xString);
            });
            modelBuilder.Entity<scalarNumero>(ent =>
            {
                ent.HasKey(c => c.iNumero);
            });
            modelBuilder.Entity<asi_aggControlxEstablecimiento>(ent =>
            {
                ent.HasKey(c => new { c.bControl, c.sEstablecimiento });
            });
            modelBuilder.Entity<asi_CalificacionMensual>(ent =>
            {
                ent.HasKey(c => c.iAsistencia);
            });
            modelBuilder.Entity<asi_detFlujoPapeleta>(ent =>
            {
                ent.HasKey(c => c.iConsecutivo);
            });
            modelBuilder.Entity<asi_detResolucion>(ent =>
            {
                ent.HasKey(c => c.sConsecutivo);
            });
            modelBuilder.Entity<asi_DiarioAsistencia>(ent =>
            {
                ent.HasKey(c => c.iAsistencia);
            });
            modelBuilder.Entity<asi_DiarioControles>(ent =>
            {
                ent.HasKey(c => c.bControl);
            });
            modelBuilder.Entity<asi_DiarioEstablecimientos>(ent =>
            {
                ent.HasKey(c => c.deptid);
            });
            modelBuilder.Entity<asi_DiarioFlujo>(ent =>
            {
                ent.HasKey(c => new { c.iAsistencia, c.bControl });
            });
            modelBuilder.Entity<asi_DiarioIncidencias>(ent =>
            {
                ent.HasKey(c => c.iAsistencia);
            });
            modelBuilder.Entity<asi_FlujoDiarioEmpleado>(ent =>
            {
                ent.HasKey(c => new { c.iAsistencia, c.bControl });
            });
            modelBuilder.Entity<asi_grafMarcado>(ent =>
            {
                ent.HasKey(c => new { c.cTurno, c.sEstablecimiento });
            });
            modelBuilder.Entity<asi_IndicePapeletas>(ent =>
            {
                ent.HasKey(c => new { c.bTipoPapeleta, c.iPapeleta });
            });
            modelBuilder.Entity<asi_MarcacionDiarioEmpleado>(ent =>
            {
                ent.HasKey(c => c.iMarcacion);
            });
            modelBuilder.Entity<asi_Reclutamiento>(ent =>
            {
                ent.HasKey(c => c.iHuellas);
            });
            modelBuilder.Entity<belPDFs>(entity =>
            {
                entity.HasKey(e => e.cPDF);

                entity.ToTable("belPDFs", "dbo");

                entity.Property(e => e.cPDF)
                    .HasMaxLength(16)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.dGeneracion).HasColumnType("datetime2(0)");
            });

            modelBuilder.Entity<orgEmpresaImagen>(entity =>
            {
                entity.HasKey(e => e.sEmpresaImagen);

                entity.ToTable("orgEmpresaImagen", "dbo");

                entity.Property(e => e.iImagen).HasColumnType("image");

                entity.Property(e => e.jImagen).IsUnicode(false);
            });

            modelBuilder.Entity<remCalculoPlanilla>(entity =>
            {
                entity.HasKey(e => e.iCalculoPlanilla);

                entity.ToTable("remCalculoPlanilla", "dbo");
            });
            modelBuilder.Entity<belCalculosxPeriodo>(ent => ent.HasKey(c => c.iCalculoPlanilla));
            modelBuilder.Entity<remBoletaCabecera>(ent => ent.HasKey(c => c.sEmpresa));
            modelBuilder.Entity<remBoletaCabeceraAsistenciaFechas>(ent => ent.HasKey(c => new { c.Fecha01, c.Fecha02, c.Fecha03 }));
            modelBuilder.Entity<remBoletaCabeceraDatos>(ent => ent.HasKey(c => c.ID));
            modelBuilder.Entity<remBoletaCabeceraEmpresa>(ent => ent.HasKey(c => c.xDato));
            modelBuilder.Entity<remBoletaCabeceraTitulo>(ent => ent.HasKey(c => c.xDato));
            modelBuilder.Entity<remBoletaConcepto>(ent => ent.HasKey(c => new { c.sOrdenMostrar, c.Concepto }));
            modelBuilder.Entity<traContratosxPersona>(ent => ent.HasKey(c => c.iContrato));
            modelBuilder.Entity<webPagosPrestamo>(ent => ent.HasKey(c => new { c.iPrestamo, c.iPrestamoCuota }));
            modelBuilder.Entity<webVacaciones>(ent => ent.HasKey(c => new { c.iContrato, c.sCiclo }));
            modelBuilder.Entity<asi_PlanoMarcacion>(ent => ent.HasKey(c => c.iMarcacion));
            modelBuilder.Entity<asi_PlanoHorarioControl>(ent => ent.HasKey(c => new { c.iAsistencia, c.bControl }));
            modelBuilder.Entity<asi_PlanoAsistencia>(ent => ent.HasKey(c => c.iAsistencia));
            }
        public async Task<int> asi_CargaDiaria(DateTime dia)
        {
            Database.SetCommandTimeout(3600);
            Database.ExecuteSqlCommand("exec asi_CargaDiaria @dHoy", new SqlParameter("@dHoy", dia));
            await Task.Delay(1);
            return 1;
        }
        public async void asi_CrearPruebaQR(int iUsuario)
        {
            Database.ExecuteSqlCommand("exec asi_CrearPruebaQR @p1", new SqlParameter("@p1", iUsuario));
            await Task.Delay(1);
        }
        public async void asi_BorrarPruebaQR(int iUsuario)
        {
            Database.ExecuteSqlCommand("exec asi_BorrarPruebaQR @p1", new SqlParameter("@p1", iUsuario));
            await Task.Delay(1);
        }
        public async void asi_RevaluarDia(DateTime dDia)
        {
            Database.ExecuteSqlCommand("exec asi_RevaluarDia @p1", new SqlParameter("@p1", dDia));
            await Task.Delay(1);
        }

        public async Task<int> asi_SincronizarMarcacion()
        {
            Database.SetCommandTimeout(3600);
            Database.ExecuteSqlCommand("exec asi_SincronizarMarcacion");
            await Task.Delay(1);
            return 1;
        }

        public virtual IQueryable<WebPrestamos> webPrestamos(int iContrato)
        {
            IQueryable<WebPrestamos> resp;
            try
            {
                resp = _webPrestamos.FromSql("exec webPrestamos @iContrato", new SqlParameter("@iContrato", iContrato));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resp;
        }

        public virtual IQueryable<perPersona> buscarPersonas(string cadena)
        {
            IQueryable<perPersona> resp;
            try
            {
                resp = perPersona.FromSql("exec perBuscarPersonas @xBuscar", new SqlParameter("@xBuscar", cadena));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resp;
        }
        
        public virtual async Task<IQueryable<asi_CalificacionMensual>> asi_CalificacionMensual(int iPersona, DateTime dFecha)
        {
            IQueryable<asi_CalificacionMensual> resp;
            try
            {
                resp = _asi_CalificacionMensual.FromSql("exec asi_CalificacionMensual @p1,@p2", new SqlParameter("@p2", dFecha), new SqlParameter("@p1", iPersona));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            await Task.Delay(1);
            return resp;
        }
        public virtual async Task<IQueryable<asi_Reclutamiento>> asi_aggReclutamientoGlobal()
        {
            IQueryable<asi_Reclutamiento> resp;
            try
            {
                resp = _asi_Reclutamiento.FromSql("exec asi_aggReclutamientoGlobal");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            await Task.Delay(1);
            return resp;
        }
        public virtual async Task<IQueryable<asi_detFlujoPapeleta>> asi_detFlujoPapeletaAsync(byte bTipoPapeleta, int iPapeleta)
        {
            IQueryable<asi_detFlujoPapeleta> resp;
            try
            {
                resp = _asi_detFlujoPapeleta.FromSql("exec asi_detFlujoPapeleta @p1,@p2", new SqlParameter("@p1", bTipoPapeleta), new SqlParameter("@p2", iPapeleta));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            await Task.Delay(1);
            return resp;
        }
        public virtual async Task<IQueryable<asi_detResolucion>> asi_detResolucionAsync(byte bTipoPapeleta, int iPapeleta)
        {
            IQueryable<asi_detResolucion> resp;
            try
            {
                resp = _asi_detResolucion.FromSql("exec asi_detResolucion @p1,@p2", new SqlParameter("@p1", bTipoPapeleta), new SqlParameter("@p2", iPapeleta));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            await Task.Delay(1);
            return resp;
        }
        public virtual async Task<IQueryable<asi_DiarioAsistencia>> asi_DiarioAsistenciaAsync(DateTime dia, short sEstablecimiento)
        {
            IQueryable<asi_DiarioAsistencia> resp;
            try
            {
                resp = _asi_DiarioAsistencia.FromSql("exec asi_DiarioAsistencia @p1,@p2", new SqlParameter("@p1", dia ), new SqlParameter("@p2", sEstablecimiento));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            await Task.Delay(1);
            return resp;
        }
        public virtual async Task<IQueryable<asi_DiarioControles>> asi_DiarioControlesAsync(DateTime dia, short sEstablecimiento)
        {
            IQueryable<asi_DiarioControles> resp;
            try
            {
                resp = _asi_DiarioControles.FromSql("exec asi_DiarioControles @p1,@p2", new SqlParameter("@p1", dia ), new SqlParameter("@p2", sEstablecimiento));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            await Task.Delay(1);
            return resp;
        }
        public virtual async Task<IQueryable<asi_DiarioFlujo>> asi_DiarioFlujoAsync(DateTime dia, short sEstablecimiento)
        {
            IQueryable<asi_DiarioFlujo> resp;
            try
            {
                resp = _asi_DiarioFlujo.FromSql("exec asi_DiarioFlujo @p1,@p2", new SqlParameter("@p1",dia ), new SqlParameter("@p2", sEstablecimiento));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            await Task.Delay(1);
            return resp;
        }

        public virtual async Task<IQueryable<asi_DiarioEstablecimientos>> asi_DiarioEstablecimientos(DateTime dia)
        {
            IQueryable<asi_DiarioEstablecimientos> resp;
            try
            {
                resp = _asi_DiarioEstablecimientos.FromSql("exec asi_DiarioEstablecimientos @p1", new SqlParameter("@p1", dia));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            await Task.Delay(1);
            return resp;
        }
        public virtual async Task<IQueryable<asi_DiarioIncidencias>> asi_DiarioIncidencias(DateTime dFecha,short sEstablecimiento,string xTitulo,string cGenero)
        {
            IQueryable<asi_DiarioIncidencias> resp;
            try
            {
                resp = _asi_DiarioIncidencias.FromSql("exec asi_DiarioIncidencias @p1,@p2,@p3,@p4", new SqlParameter("@p1",dFecha ), new SqlParameter("@p2",sEstablecimiento ), new SqlParameter("@p3",xTitulo ), new SqlParameter("@p4", cGenero ));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            await Task.Delay(1);
            return resp;
        }
        public virtual async Task<IQueryable<asi_FlujoDiarioEmpleado>> asi_FlujoDiarioEmpleado(int iAsistencia)
        {
            IQueryable<asi_FlujoDiarioEmpleado> resp;
            try
            {
                resp = _asi_FlujoDiarioEmpleado.FromSql("exec asi_FlujoDiarioEmpleado @p1", new SqlParameter("@p1", iAsistencia ));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            await Task.Delay(1);
            return resp;
        }
        public virtual async Task<IQueryable<asi_grafMarcado>> asi_grafMarcadoAsync(DateTime dia)
        {
            IQueryable<asi_grafMarcado> resp;
            try
            {
                resp = _asi_grafMarcado.FromSql("exec asi_grafMarcado @p1", new SqlParameter("@p1", dia ));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            await Task.Delay(1);
            return resp;
        }
        public virtual async Task<IQueryable<asi_IndicePapeletas>> asi_IndicePapeletas(int iPersona)
        {
            IQueryable<asi_IndicePapeletas> resp;
            try
            {
                resp = _asi_IndicePapeletas.FromSql("exec asi_IndicePapeletas @p1", new SqlParameter("@p1", iPersona));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            await Task.Delay(1);
            return resp;
        }
        public virtual async Task<IQueryable<asi_MarcacionDiarioEmpleado>> asi_MarcacionDiarioEmpleado(int iPersona,DateTime dFecha)
        {
            IQueryable<asi_MarcacionDiarioEmpleado> resp;
            try
            {
                resp = _asi_MarcacionDiarioEmpleado.FromSql("exec asi_MarcacionDiarioEmpleado @p1,@p2", new SqlParameter("@p2", dFecha), new SqlParameter("@p1", iPersona));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            await Task.Delay(1);
            return resp;
        }
        public async Task<IQueryable<asi_aggControlxEstablecimiento>> asi_aggControlxEstablecimiento(DateTime dFecha)
        {
            IQueryable<asi_aggControlxEstablecimiento> resp = null;
            try
            {
                resp = _asi_aggControlxEstablecimiento.FromSql("exec asi_aggControlxEstablecimiento @p1", new SqlParameter("@p1", dFecha));

            } catch(Exception ex)
            {
                throw ex;
            }
            await Task.Delay(1);
            return resp;
        }
        public async Task<string> asi_agregarMarcaQR(string cAula, DateTime dHora, int iAsistencia)
        {
            string resp;
            try
            {
                var rsp = _scalarString.FromSql("exec asi_agregarMarcaQR @cAula,@dHora,@iAsistencia",
                    new SqlParameter("@cAula", cAula),
                    new SqlParameter("@dHora", dHora),
                    new SqlParameter("@iAsistencia", iAsistencia)
                    );
                resp = rsp.ToList().FirstOrDefault().xString;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            await Task.Delay(1);
            return resp;
        }
        public async Task<string> asi_Revaluar(int iAsistencia)
        {
            string resp;
            try
            {
                resp = _scalarString.FromSql("exec asi_Revaluar @iAsistencia", new SqlParameter("@iAsistencia", iAsistencia)
                    ).ToList()[0].xString;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            await Task.Delay(1);
            return resp;
        }
        //public async Task<int> perAccesoData(Guid gSesion1,Guid gSesion2)
        //{
        //    SqlParameter iResp = new SqlParameter()
        //    {
        //        ParameterName = "@iResp",
        //        Direction = System.Data.ParameterDirection.Output
        //    };
        //    try
        //    {
        //        Database.ExecuteSqlCommand("exec perAccesoData @iAsistencia,@iResp", new SqlParameter("@iAsistencia", iAsistencia),iResp);

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    await Task.Delay(1);
        //    return (int)iResp.Value;
        //}

        public async Task<string> asi_CompletarPapeleta(byte bTipoPapeleta,int iPapeleta)
        {
            string resp;
            try
            {
                resp = _scalarString.FromSql("exec asi_CompletarPapeleta @p1,p2",
                    new SqlParameter("@p1", bTipoPapeleta),
                    new SqlParameter("@p2", iPapeleta)
                    ).ToList()[0].xString;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            await Task.Delay(1);
            return resp;
        }

        public async Task<List<belCalculos>> belCalculos(Guid gSesion)
        {
            List<belCalculos> resp = new List<belCalculos>();
            resp = _belCalculos.FromSql("exec belCalculos @gSesion", new SqlParameter("@gSesion", gSesion)).ToList();
            await Task.Delay(1);
            return resp;
        }
        public async Task<List<webNotificar>> webNotificar(int iUsuario)
        {
            List<webNotificar> resp = new List<webNotificar>();
            resp = _webNotificar.FromSql("exec webNotificar @iUsuario", new SqlParameter("@iUsuario", iUsuario)).ToList();
            await Task.Delay(1);
            return resp;
        }
        public async Task<List<autAvatar>> autAvatar(int iPersona)
        {
            List<autAvatar> resp = new List<autAvatar>();
            resp = _autAvatar.FromSql("exec autAvatar @iPersona", new SqlParameter("@iPersona", iPersona)).ToList();
            await Task.Delay(1);
            return resp;
        }

        public async Task<IQueryable<AutContratos>> autContratos(int iPersona)
        {
            IQueryable<AutContratos> resp;
            try
            {
                resp = _autContratos.FromSql("exec autContratos @iPersona", new SqlParameter("@iPersona", iPersona));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            await Task.Delay(1);
            return resp;
        }
        public async Task<IQueryable<remBoletaCabecera>> remBoletaCabecera(int iCalculoPlanilla,byte bMes, byte bNumero )
        {
            IQueryable<remBoletaCabecera> resp;
            try
            {
                resp = _remBoletaCabecera.FromSql("exec remBoletaCabecera @p1,@p2,@p3", new SqlParameter("@p2", bMes), new SqlParameter("@p3", bNumero),  new SqlParameter("@p1", iCalculoPlanilla));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            await Task.Delay(1);
            return resp;
        }
        public async Task<IQueryable<remBoletaCabeceraAsistenciaFechas>> remBoletaCabeceraAsistenciaFechas(byte bMes, byte bNumero, int iCalculoPlanilla)
        {
            IQueryable<remBoletaCabeceraAsistenciaFechas> resp;
            try
            {
                resp = _remBoletaCabeceraAsistenciaFechas.FromSql("exec remBoletaCabeceraAsistenciaFechas @p3,@p1,@p2",new SqlParameter("@p1", bMes), new SqlParameter("@p2", bNumero),  new SqlParameter("@p3", iCalculoPlanilla));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            await Task.Delay(1);
            return resp;
        }
        public async Task<IQueryable<remBoletaCabeceraDatos1>> remBoletaCabeceraDatos1(int iCalculoPlanilla)
        {
            IQueryable<remBoletaCabeceraDatos1> resp;
            try
            {
                string xSQL = $"exec remBoletaCabeceraDatos1 @p1";
                resp =  _remBoletaCabeceraDatos1.FromSql(xSQL, new SqlParameter("p1", iCalculoPlanilla));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            await Task.Delay(1);
            return resp;
        }
        public async Task<IQueryable<remBoletaCabeceraDatos2>> remBoletaCabeceraDatos2(int iCalculoPlanilla)
        {
            IQueryable<remBoletaCabeceraDatos2> resp;
            try
            {
                string xSQL = $"exec remBoletaCabeceraDatos2 @p1";
                resp = _remBoletaCabeceraDatos2.FromSql(xSQL, new SqlParameter("p1", iCalculoPlanilla));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            await Task.Delay(1);
            return resp;
        }

        public async Task<IQueryable<remBoletaConcepto>> remBoletaConcepto(int iCalculoPlanilla,byte bMes,byte bNumero,byte bClaseConcepto )
        {
            IQueryable<remBoletaConcepto> resp;
            try
            {
                resp = _remBoletaConcepto.FromSql("exec remBoletaConcepto @p1,@p2,@p3,@p4", new SqlParameter("@p1", iCalculoPlanilla), new SqlParameter("@p2", bMes), new SqlParameter("@p3", bNumero), new SqlParameter("@p4", bClaseConcepto));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            await Task.Delay(1);
            return resp;
        }
        public async Task<IQueryable<traContratosxPersona>> traContratosxPersona(int iPersona)
        {
            IQueryable<traContratosxPersona> resp;
            try
            {
                resp = _traContratosxPersona.FromSql("exec traContratosxPersona @p1", new SqlParameter("@p1", iPersona));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            await Task.Delay(1);
            return resp;
        }
        public async Task<IQueryable<webPagosPrestamo>> webPagosPrestamo(int iPrestamo)
        {
            IQueryable<webPagosPrestamo> resp;
            try
            {
                resp = _webPagosPrestamo.FromSql("exec webPagosPrestamo @p1", new SqlParameter("@p1", iPrestamo));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            await Task.Delay(1);
            return resp;
        }

        public async Task<IQueryable<webVacaciones>> webVacaciones(int iContrato)
        {
            IQueryable<webVacaciones> resp;
            try
            {
                resp = _webVacaciones.FromSql("exec webVacaciones @p1", new SqlParameter("@p1", iContrato));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            await Task.Delay(1);
            return resp;
        }

        public async Task<IQueryable<belCalculos>> belCalculosPersona(int iPersona)
        {
            IQueryable<belCalculos> resp;
            try
            {
                SqlParameter par_gToken = new SqlParameter("@iPersona", iPersona);
                resp = _belCalculos.FromSql("exec belCalculosPersona @iPersona", par_gToken);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            await Task.Delay(1);
            return resp;
        }
        public async Task<IQueryable<belCalculosxPeriodo>> belCalculosxPeriodo(Guid gToken)
        {
            IQueryable<belCalculosxPeriodo> resp;
            try
            {
                SqlParameter par_gToken = new SqlParameter("@gToken", gToken);
                resp = _belCalculosxPeriodo.FromSql("exec belCalculosxPeriodo @gToken", par_gToken);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            await Task.Delay(1);
            return resp;
        }
        public List<asi_PlanoMarcacion> asi_PlanoMarcacion(DateTime? dDesde, DateTime? dHasta, List<string> empleados)
        {

            List<asi_PlanoMarcacion> resp;
                uladechEntities dbu = new uladechEntities();
                List<DEPARTMENTS> depas = dbu.DEPARTMENTS.ToList(); ;
                List<asi_Flujo> flujos;
                List<USERINFO> cEmpleados;
                List<asi_Dispositivo> dispositivos = asi_Dispositivo.ToList();
                List<asi_Marcacion> marcas;
                TimeSpan finDia = new TimeSpan(23, 59, 59);
                DateTime dHastaMarcar = DateTime.Now;
                if (dHasta != null)
                {
                    dHasta = (DateTime)dHasta + finDia;
                    dHastaMarcar = (DateTime)dHasta;
                    dHastaMarcar = dHastaMarcar.AddDays(1);
                }
                marcas = asi_Marcacion.Where(m => (m.dHora >= dDesde || dDesde == null) && (m.dHora <= dHastaMarcar || dHasta == null)).ToList();
                if (empleados is null || empleados.Count == 0)
                {
                    cEmpleados = dbu.USERINFO.ToList();
                    flujos = asi_Flujo
                        .Join(asi_Asistencia.Where(asi => (asi.dFecha >= dDesde || dDesde == null) && (asi.dFecha <= dHasta || dHasta == null)).ToList(), x => x.iAsistencia, y => y.iAsistencia, (x, y) => x).ToList();
                }
                else
                {
                    cEmpleados = dbu.USERINFO.Join(empleados, x => x.Badgenumber, y => y, (x, y) => x).ToList();
                    flujos = asi_Flujo
                        .Join(asi_Asistencia.Where(asi => (asi.dFecha >= dDesde || dDesde == null) && (asi.dFecha <= dHasta || dHasta == null)).ToList(), x => x.iAsistencia, y => y.iAsistencia, (flu, asi) => new { flu, asi })
                        .Join(cEmpleados, x => x.asi.iContrato, y => y.USERID, (x, y) => x.flu).ToList();
                }
                resp = marcas
                      .Join(dispositivos, mar => mar.sDispositivo, dis => dis.sDispositivo, (mar, dis) => new { mar, dis })
                      .Join(cEmpleados, x => x.mar.iPersona, y => y.USERID, (mau, usr) => new { mau, usr })
                      .Join(depas,x=>(int)x.mau.mar.sEstablecimiento,y=>y.DEPTID, (mad,dep) => new { mad,dep })
                      .GroupJoin(flujos, x => x.mad.mau.mar.iMarcacion, y => y.iMarcacion, (x, y) => new { maf=x, _flu=y }).SelectMany( x => x._flu.DefaultIfEmpty(),(x,y)=> new { maf = x, flu = y } )
                      .Where(s=> s.maf.maf.mad.mau.mar.dHora <= dHasta || s.flu?.iAsistencia != null )
                      .Select(s => new asi_PlanoMarcacion
                       {
                           iMarcacion = s.maf.maf.mad.mau.mar.iMarcacion,
                           dHora = (DateTime)s.maf.maf.mad.mau.mar.dHora,
                           xDispositivo = s.maf.maf.mad.mau.dis.xDispositivo,
                           cEstablecimiento = s.maf.maf.dep.DEPTNAME,
                           cEmpleado = s.maf.maf.mad.usr.Badgenumber,
                           iAsistencia = s.flu?.iAsistencia,
                           bControl = s.flu?.bControl
                       }).ToList();
            return resp;
        }
        public List<asi_PlanoAsistencia> asi_PlanoAsistencia(DateTime? dDesde, DateTime? dHasta, List<string> empleados)
        {
            uladechEntities dbu = new uladechEntities();
            List<asi_PlanoAsistencia> resp;
            List<USERINFO> cEmpleados;
            try
            {
                if (empleados is null || empleados.Count == 0)
                {
                    cEmpleados = dbu.USERINFO.ToList();
                }
                else
                {
                    cEmpleados = dbu.USERINFO.Join(empleados, x => x.Badgenumber, y => y, (x, y) => x).ToList();
                }
                resp = asi_Asistencia
                    .Join(cEmpleados, x => x.iContrato, y => y.USERID, (asi, usr) => new { asi, usr })
                    .Select(s => new asi_PlanoAsistencia
                    {
                        iAsistencia = s.asi.iAsistencia,
                        dFecha = s.asi.dFecha,
                        cEmpleado = s.usr.Badgenumber,
                        fDescuento = (float?)s.asi.fDescuento,
                        fExtra = (float?)s.asi.fExtra
                    }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resp;
        }
        public List<asi_PlanoHorarioControl> asi_PlanoHorarioControl(DateTime? dDesde, DateTime? dHasta, List<string> empleados)
        {
            uladechEntities dbu = new uladechEntities();
            List<asi_PlanoHorarioControl> resp;
            List<USERINFO> cEmpleados;
            try
            {
                if (empleados is null || empleados.Count == 0)
                {
                    cEmpleados = dbu.USERINFO.ToList();
                }
                else
                {
                    cEmpleados = dbu.USERINFO.Join(empleados, x => x.Badgenumber, y => y, (x, y) => x).ToList();
                }
                resp = asi_Flujo
                        .Join(asi_Asistencia.Where(asi => (asi.dFecha >= dDesde || dDesde == null) && (asi.dFecha <= dHasta || dHasta == null)), x => x.iAsistencia, y => y.iAsistencia, (flu, asi) => new { flu, asi })
                        .Join(cEmpleados, x => x.asi.iContrato, y => y.USERID, (flu, usr) => new { flu,usr } )
                    .Select(s => new asi_PlanoHorarioControl
                    {
                        iAsistencia = s.flu.flu.iAsistencia,
                        bControl = s.flu.flu.bControl,
                        dHoraHorario = s.flu.flu.dHoraHorario,
                        dHoraControl= s.flu.flu.dHoraControl,
                        iMarcacion = s.flu.flu.iMarcacion,
                        sIncidencia = s.flu.flu.sIncidencia,
                        cEmpleado =s.usr.Badgenumber
                    }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resp;
        }

        public async Task<IQueryable<remBoletaCabeceraEmpresa>> remBoletaCabeceraEmpresa(short sEmpresa)
        {

            IQueryable<remBoletaCabeceraEmpresa> resp;
            try
            {
                resp = _remBoletaCabeceraEmpresa.FromSql("exec remBoletaCabeceraEmpresa @sEmpresa", new SqlParameter("@sEmpresa", sEmpresa));       
            }
            catch (Exception ex)
            {
                throw ex;
            }
            await Task.Delay(1);
            return resp;
        }
        public async Task<IQueryable<remBoletaCabeceraTitulo>> remBoletaCabeceraTitulo(int iCalculoPlanilla,byte bMes, byte bNumero )
        {
            IQueryable<remBoletaCabeceraTitulo> resp;
            try
            {
                resp = _remBoletaCabeceraTitulo.FromSql("exec remBoletaCabeceraTitulo @iCalculoPlanilla,@bMes,@bNumero", new SqlParameter("@iCalculoPlanilla", iCalculoPlanilla), new SqlParameter("@bMes", bMes), new SqlParameter("@bNumero", bNumero));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            await Task.Delay(1);
            return resp;
        }
    }

    public class AutContratos
    {
        public DateTime? dDesde { get; set; }
        public DateTime? dHasta { get; set; }
        public int iContrato { get; set; }
        public string jImagen { get; set; }
        public short sCargo { get; set; }
        public short sEmpresa { get; set; }
        public string xAbreviado { get; set; }
        public string xCargo { get; set; }
        public string xRazonSocial { get; set; }
    }
    public class WebPrestamos
    {
        public DateTime? dPrestamo { get; set; }
        public int iContrato { get; set; }
        public int iPrestamo { get; set; }
        public decimal? mPagos { get; set; }
        public decimal mValor { get; set; }
        public short sConcepto { get; set; }
        public short sMoneda { get; set; }
    }
    public class autAvatar
    {
        public int iPersona { get; set; }
        public string xTip { get; set; }
        public string xDocIdentidad { get; set; }
        public string xUsuario { get; set; }
        public string jFoto { get; set; }
        public string cGenero { get; set; }
    }
    public class autAvatarRegistro
    {
        public string xNombres { get; set; }
        public string xEmail { get; set; }
    }
    public partial class belCalculos
    {
        public int iCalculoPlanilla { get; set; }
        public int iPeriodo { get; set; }
        public int iContrato { get; set; }
        public int iAnoMes { get; set; }
        public byte bNumero { get; set; }
        public Nullable<byte> bMes { get; set; }
        public int bBEL { get; set; }
    }
    public partial class webNotificar
    {
        public int iNotificacion { get; set; }
        public Nullable<System.DateTime> dEnvio { get; set; }
        public Nullable<byte> bTipoNotificacion { get; set; }
        public Nullable<int> iReferencia { get; set; }
        public Nullable<int> iContrato { get; set; }
        public string xAsunto { get; set; }
        public string xFaIcono { get; set; }
        public string xUrl { get; set; }
        public Nullable<System.DateTime> dLeido { get; set; }
        public Nullable<System.DateTime> dFirmado { get; set; }
    }
    public partial class asi_aggControlxEstablecimiento
    {
        public short bControl { get; set; }
        public Nullable<short> sEstablecimiento { get; set; }
        public Nullable<int> iControl { get; set; }
        public Nullable<int> iMarcado { get; set; }
    }
    public partial class asi_CalificacionMensual
    {
        public Nullable<int> iAsistencia { get; set; }
        public Nullable<System.DateTime> dFecha { get; set; }
        public Nullable<byte> bCalificacion { get; set; }
    }
    public partial class asi_detFlujoPapeleta
    {
        public int iConsecutivo { get; set; }
        public string cTipoFlujo { get; set; }
        public Nullable<System.DateTime> dFlujo { get; set; }
        public Nullable<int> iEmpleado { get; set; }
        public string xObservacion { get; set; }
        public string name { get; set; }
    }
    public partial class asi_detResolucion
    {
        public short sConsecutivo { get; set; }
        public Nullable<System.DateTime> dFecha { get; set; }
        public Nullable<int> iAsistencia { get; set; }
        public Nullable<short> bControl { get; set; }
        public Nullable<System.TimeSpan> dResolucion { get; set; }
        public string xControl { get; set; }
    }
    public partial class asi_DiarioAsistencia
    {
        public int iAsistencia { get; set; }
        public int iContrato { get; set; }
        public System.DateTime dFecha { get; set; }
        public Nullable<double> fDescuento { get; set; }
        public Nullable<double> fExtra { get; set; }
        public Nullable<int> iCalculoPlanilla { get; set; }
        public Nullable<short> sEstablecimiento { get; set; }
        public string cPlanilla { get; set; }
        public string name { get; set; }
        public string lastname { get; set; }
        public Nullable<int> iEstado { get; set; }
    }
    public partial class asi_DiarioControles
    {
        public short bControl { get; set; }
        public string xControl { get; set; }
    }
    public partial class asi_DiarioEstablecimientos
    {
        public int deptid { get; set; }
        public string deptname { get; set; }
    }
    public partial class asi_DiarioFlujo
    {
        public int iAsistencia { get; set; }
        public short bControl { get; set; }
        public System.DateTime dHoraHorario { get; set; }
        public Nullable<System.DateTime> dHoraControl { get; set; }
        public Nullable<int> iMarcacion { get; set; }
        public Nullable<short> sIncidencia { get; set; }
    }
    public partial class asi_DiarioIncidencias
    {
        public int iAsistencia { get; set; }
        public int iContrato { get; set; }
        public Nullable<short> sEstablecimiento { get; set; }
        public string cPlanilla { get; set; }
        public string xNombre { get; set; }
        public string Gender { get; set; }
        public string Title { get; set; }
        public string xEstablecimiento { get; set; }
    }
    public partial class asi_FlujoDiarioEmpleado
    {
        public int iAsistencia { get; set; }
        public short bControl { get; set; }
        public System.DateTime dHoraHorario { get; set; }
        public Nullable<System.DateTime> dHoraControl { get; set; }
        public Nullable<int> iMarcacion { get; set; }
        public Nullable<short> sIncidencia { get; set; }
        public string xControl { get; set; }
        public string xIncidencia { get; set; }
        public Nullable<byte> bTipoPapeleta { get; set; }
        public Nullable<int> iPapeleta { get; set; }
    }
    public partial class asi_grafMarcado
    {
        public string cTurno { get; set; }
        public string cTipoControl { get; set; }
        public Nullable<short> sEstablecimiento { get; set; }
        public Nullable<int> esperado { get; set; }
        public Nullable<int> marcado { get; set; }
    }
    public partial class asi_IndicePapeletas
    {
        public byte bTipoPapeleta { get; set; }
        public int iPapeleta { get; set; }
        public System.DateTime dCreacion { get; set; }
        public Nullable<System.DateTime> dRRHH { get; set; }
        public string xTipoPapeleta { get; set; }
    }
    public partial class asi_MarcacionDiarioEmpleado
    {
        public int iMarcacion { get; set; }
        public Nullable<short> sDispositivo { get; set; }
        public Nullable<short> sEstablecimiento { get; set; }
        public Nullable<System.DateTime> dHora { get; set; }
        public Nullable<int> iPersona { get; set; }
        public Nullable<int> zkId { get; set; }
        public string xDispositivo { get; set; }
        public string xEstablecimiento { get; set; }
    }
    public partial class asi_Reclutamiento
    {
        public int iTotal { get; set; }
        public int iHuellas { get; set; }
        public int iVoces { get; set; }
    }
    public class scalarString
    {
        public string xString { get; set; }
    }
    public class scalarNumero
    {
        public int iNumero  { get; set; }
    }

    public partial class belCalculosxPeriodo
    {
        public int iNotificacion { get; set; }
        public byte bTipoNotificacion { get; set; }
        public string xTipoNotificacion { get; set; }
        public Nullable<bool> lPush { get; set; }
        public int iCalculoPlanilla { get; set; }
        public int iAnoMes { get; set; }
        public byte bCalculo { get; set; }
        public string xNotificacion { get; set; }
        public System.DateTime dNotificacion { get; set; }
        public Nullable<System.DateTime> dLeida { get; set; }
    }
    public partial class remBoletaCabecera
    {
        public short sEmpresa { get; set; }
        public string txtfirmaizq1 { get; set; }
        public string txtfirmaizq2 { get; set; }
        public string txtfirmader1 { get; set; }
        public string txtfirmader2 { get; set; }
        public string txtneto { get; set; }
        public string monedaneto { get; set; }
        public Nullable<decimal> neto { get; set; }
        public int bfirma { get; set; }
        public int slogo { get; set; }
        public int bselloagua { get; set; }
        public string xingresos { get; set; }
        public string xdescuentos { get; set; }
        public string xempleador { get; set; }
        public int AnchoTitulo { get; set; }
        public short sEmpresaImagenLogo { get; set; }
        public short sEmpresaImagenFirma { get; set; }
    }
    public partial class remBoletaCabeceraAsistenciaFechas
    {
        public string Fecha01 { get; set; }
        public string Fecha02 { get; set; }
        public string Fecha03 { get; set; }
        public string Nombre01 { get; set; }
        public string Nombre02 { get; set; }
        public string Nombre03 { get; set; }
    }
    public partial class remBoletaCabeceraDatos
    {
        public byte ID { get; set; }
        public string xNombre { get; set; }
        public string xSeparador { get; set; }
        public string xDato { get; set; }
        public Nullable<int> bBold { get; set; }
    }
    public partial class remBoletaCabeceraDatos1
    {
        public byte ID { get; set; }
        public string xNombre { get; set; }
        public string xSeparador { get; set; }
        public string xDato { get; set; }
        public Nullable<int> bBold { get; set; }
    }
    public partial class remBoletaCabeceraDatos2
    {
        public byte ID { get; set; }
        public string xNombre { get; set; }
        public string xSeparador { get; set; }
        public string xDato { get; set; }
        public Nullable<int> bBold { get; set; }
    }
    public class remBoletaCabeceraEmpresa
    {
        public string xDato { get; set; }
    }
    public class remBoletaCabeceraTitulo
    {
        public string xDato { get; set; }
    }
    public partial class remBoletaConcepto
    {
        public string xClaseConcepto { get; set; }
        public string xSubClaseConcepto { get; set; }
        public string Concepto { get; set; }
        public decimal mMonto { get; set; }
        public short sOrdenMostrar { get; set; }
        public string MontoInfo { get; set; }
        public string MontoHora { get; set; }
        public byte OrdenSubClaseConcepto { get; set; }
    }
    public partial class traContratosxPersona
    {
        public int iContrato { get; set; }
        public short sEmpresa { get; set; }
        public short sCargo { get; set; }
        public string xContrato { get; set; }
    }

    public partial class traPrestamo
    {
        [Key]
        public int iPrestamo { get; set; }
        public int iContrato { get; set; }
        public int sConcepto { get; set; }
        public short sMoneda { get; set; }
        public decimal mValor { get; set; }
    }

    public partial class webPagosPrestamo
    {
        public int iPrestamoCuota { get; set; }
        public int iPrestamo { get; set; }
        public byte bCalculo { get; set; }
        public System.DateTime dFecha { get; set; }
        public int mValor { get; set; }
        public bool bAnulado { get; set; }
        public Nullable<int> iCalculoPlanilla { get; set; }
    }
    public partial class webPrestamos
    {
        public int iPrestamo { get; set; }
        public int iContrato { get; set; }
        public short sConcepto { get; set; }
        public short sMoneda { get; set; }
        public decimal mValor { get; set; }
        public Nullable<System.DateTime> dPrestamo { get; set; }
        public Nullable<int> mPagos { get; set; }
    }
    public partial class webVacaciones
    {
        public Nullable<long> iNovedadVacaciones { get; set; }
        public Nullable<int> iContrato { get; set; }
        public Nullable<short> sCiclo { get; set; }
        public System.DateTime dDesde { get; set; }
        public Nullable<System.DateTime> dHasta { get; set; }
        public Nullable<int> iDias { get; set; }
        public string cTipoNovedad { get; set; }
        public string xDescripcion { get; set; }
    }

    public class asi_PlanoMarcacion
    {
        public int iMarcacion { get; set; }
        public DateTime dHora { get; set; }
        public string xDispositivo { get; set; }
        public string cEstablecimiento { get; set; }
        public string cEmpleado { get; set; }
        public int? iAsistencia { get; set; }
        public short? bControl { get; set; }
    }

    public class asi_PlanoHorarioControl
    {
        public int iAsistencia { get; set; }
        public short bControl { get; set; }
        public DateTime dHoraHorario { get; set; }
        public DateTime? dHoraControl { get; set; }
        public int? iMarcacion { get; set; }
        public short? sIncidencia { get; set; }
        public string cEmpleado { get; set; }
    }

    public class asi_PlanoAsistencia
    {
        public int iAsistencia { get; set; }
        public DateTime dFecha { get; set; }
        public string cEmpleado { get; set; }
        public float? fDescuento { get; set; }
        public float? fExtra { get; set; }
    }

}
