﻿using api2Nominus.Models;
using api2Nominus.Models.nominus;
using Microsoft.EntityFrameworkCore;
using api2Nominus.Models.auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Threading.Tasks;

namespace api2Nominus.Controllers
{
    [RoutePrefix("bel")]
    public class belController : ApiController
    {

        [Route("indice")]
        public async Task<List<BigBelCalculos>> getIndiceCalculosAsync()
        {
            nominusAppEntities dbA = new nominusAppEntities();
            List<BigBelCalculos> rsp = new List<BigBelCalculos>();
            string SesionPersona = HttpContext.Current.Request.Headers["sesionPersona"];
            Guid? gSesionPersona = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            List<AuthServidorUsuario> _servidoresUsr = (await dbA.authServidoresUsuario((Guid)gSesionPersona)).ToList();
            foreach (AuthServidorUsuario _srv in _servidoresUsr)
            {
                nominaEntities dbD = new nominaEntities(_srv.xConexion);

                List<belCalculos> calculos = (await dbD.belCalculosPersona(_srv.iPersona)).ToList();

                rsp.AddRange(calculos.Select(s => new BigBelCalculos
                {
                    sServidor = _srv.sServidor,
                    iCalculoPlanilla = s.iCalculoPlanilla,
                    iContrato = s.iContrato,
                    iPeriodo = s.iPeriodo,
                    iAnoMes = s.iAnoMes,
                    bMes = s.bMes,
                    bNumero = s.bNumero,
                    bBEL = s.bBEL
                }));
            }
            return rsp;
        }

        [Route("data")]
        public async Task<DataBoleta> PostDataBoleta([FromBody] ParCabecera parCabecera)
        {
            nominusAppEntities dbA = new nominusAppEntities();
            string SesionPersona = HttpContext.Current.Request.Headers["sesionPersona"];
            Guid? gSesionPersona = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            authServidor _srv = dbA.authServidor.Find(parCabecera.sServidor);
            nominaEntities db = new nominaEntities(_srv.xConexion);
            remCalculoPlanilla calculo = db.remCalculoPlanilla.Find(parCabecera.iCalculoPlanilla);
            traContrato contrato = db.traContrato.Find(calculo.iContrato);
            DataBoleta datas = new DataBoleta();
            webNotificacion wNot = db.webNotificacion.FirstOrDefault(c => c.iReferencia == parCabecera.iCalculoPlanilla && c.bTipoNotificacion == 1);
            if (wNot != null)
            {
                webFirma wFir = db.webFirma.FirstOrDefault(c => c.iNotificacion == wNot.iNotificacion && c.bRol == 1);
                if (wFir != null && wFir.dLeido == null)
                {
                    wFir.dLeido = DateTime.Now;
                    db.Entry(wFir).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
            datas.cabecera = (await db.remBoletaCabecera(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero)).ToList();
            datas.asistencia = (await db.remBoletaCabeceraAsistenciaFechas(parCabecera.bMes, parCabecera.bNumero,parCabecera.iCalculoPlanilla)).ToList();
            datas.datosCol1 = (await db.remBoletaCabeceraDatos1(parCabecera.iCalculoPlanilla)).ToList();
            datas.datosCol2 = (await db.remBoletaCabeceraDatos2(parCabecera.iCalculoPlanilla)).ToList();
            datas.empresa = (await db.remBoletaCabeceraEmpresa(contrato.sEmpresa)).ToList();
            datas.titulo = (await db.remBoletaCabeceraTitulo(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero)).ToList();
            datas.concepto1 = (await db.remBoletaConcepto(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero, 1)).ToList();
            datas.concepto2 = (await db.remBoletaConcepto(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero, 2)).ToList();
            datas.concepto3 = (await db.remBoletaConcepto(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero, 3)).ToList();
            datas.concepto4 = (await db.remBoletaConcepto(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero, 4)).ToList();
            datas.pdf = Pdfs.Boleta(datas, parCabecera, _srv.xConexion);
            return datas;
        }

    }

    public class TuplaContrato
    {
        public int iContrato { get; set; }
        public short sServidor { get; set; }
    }
    public class ParCabecera
    {
        public byte bMes { get; set; }
        public byte bNumero { get; set; }
        public int iCalculoPlanilla { get; set; }
        public Nullable<short> sServidor { get; set; }
    }

    public class ParBoletaConcepto : ParCabecera
    {
        public byte bClaseConcepto { get; set; }
    }

    public class ParCabeceraDatos
    {
        public byte bColumna { get; set; }
        public int iCalculoPlanilla { get; set; }
    }

    public class IndiceBoletas
    {
        public List<traContratosxPersona> contratos { get; set; }
        public List<belCalculos> calculos { get; set; }
    }

    public class DataBoleta
    {
        public List<remBoletaCabecera> cabecera { get; set; }
        public List<remBoletaCabeceraAsistenciaFechas> asistencia { get; set; }
        public List<remBoletaCabeceraDatos1> datosCol1 { get; set; }
        public List<remBoletaCabeceraDatos2> datosCol2 { get; set; }
        public List<remBoletaCabeceraEmpresa> empresa { get; set; }
        public List<remBoletaCabeceraTitulo> titulo { get; set; }
        public List<remBoletaConcepto> concepto1 { get; set; }
        public List<remBoletaConcepto> concepto2 { get; set; }
        public List<remBoletaConcepto> concepto3 { get; set; }
        public List<remBoletaConcepto> concepto4 { get; set; }
        public outBoleta pdf { get; set; }
    }

}

