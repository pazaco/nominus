﻿using api2Nominus.Models;
using api2Nominus.Models.nominus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace api2Nominus.Controllers
{
    [RoutePrefix("usuarios")]
    public class usuarioController : ApiController
    {
        [Route("buscarUsuario/{cadena}"), HttpGet]
        public async Task<List<perPersona>> BuscarUsuarioAsync(string cadena)
        {
            List<perPersona> rsp = null;
            nominusAppEntities dbA = new nominusAppEntities();
            string SesionPersona = HttpContext.Current.Request.Headers["sesionPersona"];
            string xServidor = HttpContext.Current.Request.Headers["selServidor"];
            short sServidor;
            if (short.TryParse(xServidor, out sServidor))
            {
                Guid? gSesionPersona = null;
                if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
                List<AuthServidorUsuario> _servidoresUsr = (await dbA.authServidoresUsuario((Guid)gSesionPersona)).ToList();
                AuthServidorUsuario _srv = _servidoresUsr.Find(c=>c.sServidor==sServidor);
                if (_srv != null)
                {
                    nominaEntities dbN = new nominaEntities(_srv.xConexion);
                    rsp = dbN.buscarPersonas(cadena).ToList();
                }

            }
            return rsp;

        }

    }
}
