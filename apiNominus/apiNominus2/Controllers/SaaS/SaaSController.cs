﻿using apiNominus2.Helpers;
using apiNominus2.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace apiNominus2.Controllers
{
    [Route("saas")]
    [ApiController]
    public class SaaSController : ControllerBase
    {
        [HttpPost("conexion")]
        public ZasStringConexion GetConexion([FromBody] Zas_Conexion param)
        {
            return Procs.ZasConexion(param.iCliente, param.sProducto, param.bModulo);
        }

        [HttpPost("auth")]
        public ZasHash PostLogin([FromBody] Per_Login login)
        {
            string xUserAgent = HttpContext.Request.Headers["user-agent"];
            string xTokenMinuto = HttpContext.Request.Headers["user-seed"];
            string xProducto = HttpContext.Request.Headers["producto"];
            string xSuper = "jorge@loaizas.com pablo.neyra.n@gmail.com";
            ZasHash rsp = null;
            DateTime hoy = DateTime.Now;
            short sProducto;
            if (short.TryParse(xProducto, out sProducto))
            {
                if (Strings.Minutero(xTokenMinuto) || true)
                {
                    SaaSContext dbA = new SaaSContext();
                    zasProducto _prd = dbA.zasProducto.FirstOrDefault(pr => pr.sProducto == sProducto);
                    if (_prd != null)
                    {
                        perSesion _ses = dbA.perSesion.FirstOrDefault(se => se.xTokenFCM == login.xTokenFCM && se.iUsuarioPerfilNavigation.iUsuarioNavigation.xCorreo == login.xEmail.Trim());
                        if (_ses != null)
                        {
                            perUserAgent _usa = dbA.perUserAgent.Find(_ses.iUserAgent);
                            perUsuarioPerfil _pup = dbA.perUsuarioPerfil.Find(_ses.iUsuarioPerfil);
                            zasNegocio _neg = dbA.zasNegocio.Find(_pup.iUnidadNegocio);
                            zasCliente _cli = dbA.zasCliente.Find(_neg.iCliente);
                            perUser _usu = dbA.perUser.Find(_pup.iUsuario);
                            perPerfil _prf = dbA.perPerfil.Find(_neg.sPerfil);
                            rsp = new ZasHash(_ses);
                            if (xUserAgent.Contains("Postman") && xSuper.Contains(login.xEmail)) rsp.dev = _ses;
                        }
                        else
                        {
                            perUserAgent _usa = dbA.perUserAgent.FirstOrDefault(us => us.xUserAgent.Trim() == xUserAgent);
                            if (_usa == null)
                            {
                                int iUsa = 0;
                                if (dbA.perUserAgent.Any())
                                {
                                    iUsa = dbA.perUserAgent.Max(_p => _p.iUserAgent);
                                    iUsa++;
                                }
                                _usa = new perUserAgent { iUserAgent = iUsa, xUserAgent = xUserAgent };
                                dbA.perUserAgent.Add(_usa);
                                dbA.SaveChanges();
                            }
                            perUser _usr = dbA.perUser.FirstOrDefault(us => us.xCorreo.Trim() == login.xEmail.Trim());
                            if (_usr != null)
                            {
                                perUsuarioPerfil _pup = dbA.perUsuarioPerfil
                                    .FirstOrDefault(up => up.iUsuario == _usr.iUsuario 
                                        && up.iUnidadNegocioNavigation.sPerfilNavigation.sProducto == sProducto
                                        && (up.dVigencia==null || up.dVigencia.Value.Date>=DateTime.Now.Date)
                                        && (up.iUnidadNegocioNavigation.dVigencia==null || up.iUnidadNegocioNavigation.dVigencia.Value.Date==DateTime.Now.Date));
                                if (_pup != null)
                                {
                                    if (_pup.dVigencia == null || _pup.dVigencia.Value.Date >= hoy.Date)
                                    {
                                        zasNegocio _neg = dbA.zasNegocio.Find(_pup.iUnidadNegocio);
                                        if (_neg.dVigencia == null || _neg.dVigencia.Value.Date >= hoy.Date)
                                        {
                                            _ses = new perSesion
                                            {
                                                gSesion = Guid.NewGuid(),
                                                iUsuarioPerfil = _pup.iUsuarioPerfil,
                                                iUserAgent = _usa.iUserAgent,
                                                dInicio = hoy,
                                                oSesion = "{}",
                                                xTokenFCM = login.xTokenFCM
                                            };
                                            dbA.perSesion.Add(_ses);
                                            dbA.SaveChanges();
                                            zasCliente _cli = dbA.zasCliente.Find(_neg.iCliente);
                                            perUser _usu = dbA.perUser.Find(_pup.iUsuario);
                                            perPerfil _prf = dbA.perPerfil.Find(_neg.sPerfil);
                                            rsp = new ZasHash(_ses);
                                            if (xUserAgent.Contains("Postman") && xSuper.Contains(login.xEmail)) rsp.dev = _ses;
                                        }
                                        else
                                        {
                                            rsp = new ZasHash("El acceso empresarial ha caducado.");
                                        }
                                    }
                                    else
                                    {
                                        rsp = new ZasHash("Su cuenta personal ha caducado.");
                                    }
                                }
                                else
                                {
                                    rsp = new ZasHash("Usuario no está asociado al producto '" + _prd.xProducto + "'.");
                                }
                            }
                            else
                            {
                                rsp = new ZasHash("El correo " + login.xEmail + " no pertenece a un usuario autorizado.");
                            }
                        }
                    }
                    else
                    {
                        rsp = new ZasHash("El producto indicado no existe como parte de este servicio.");
                    }
                }
                else
                {
                    rsp = new ZasHash("Dispositivo ó red no autorizada para conectarse con este servicio.");
                }
            }
            else
            {
                rsp = new ZasHash("Faltan parámetros.");
            }
            return rsp;
        }
    }
}

