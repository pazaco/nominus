﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Microsoft.Naming", "IDE1006:Estilos de nombres", Justification = "(estándar Nominus) primer caracter en minuscula indica DataType.", Scope = "module")]
