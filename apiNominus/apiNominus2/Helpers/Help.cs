﻿using apiNominus2.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace apiNominus2.Helpers
{
    public class Strings
    {
        static string Key { get; set; } = "->duolc.SOPniW<-";

        public static string ConnectionString(string xServerName, string xDb, string xUserId, string xPasswd)
        {
            string plantilla = AppSettingConexion("plantilla");
            return string.Format(plantilla, xServerName, xDb, xUserId, xPasswd);
        }

        private static string AppSettingConexion(string clave)
        {
            // DbContextOptionsBuilder OptionsBuilder = new DbContextOptionsBuilder();

            IConfigurationRoot configuration = new ConfigurationBuilder()
              .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
              .AddJsonFile("appsettings.json")
              .Build();
            return configuration.GetConnectionString(clave);
        }

        public static bool Minutero(string xTokenCliente)
        {
            DateTime ya = DateTime.UtcNow;
            byte[] iv = new byte[16];
            byte[] array;
            string text;

            using (Aes aes = Aes.Create())
            {
                byte bMatch = 0;
                while (bMatch < 2)
                {
                    aes.Key = Encoding.UTF8.GetBytes(Key);
                    aes.IV = iv;
                    aes.Padding = PaddingMode.PKCS7;
                    aes.Mode = CipherMode.CBC;
                    text = string.Format("_{0:mmddMM}Y{0:yy}T{0:mmHH}_", ya.AddMinutes(0-bMatch));

                    ICryptoTransform encryptor = aes.CreateEncryptor(aes.Key, aes.IV);
                    using (MemoryStream memoryStream = new MemoryStream())
                    {
                        using CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
                        using (StreamWriter streamWriter = new StreamWriter(cryptoStream))
                        {
                            streamWriter.Write(text);
                        }

                        array = memoryStream.ToArray();
                    }
                    if(xTokenCliente== Convert.ToBase64String(array))
                    {
                        return true;
                    }
                    bMatch++;
                }
            }
            return false;
        }

        public static string Encrypt128(dynamic objeto, string _key = null)
        {
            if (_key == null) _key = Key;

            byte[] iv = new byte[16];
            byte[] array;
            string text = JsonConvert.SerializeObject(objeto);

            using (Aes aes = Aes.Create())
            {
                aes.Key = Encoding.UTF8.GetBytes(_key);
                aes.IV = iv;
                aes.Padding = PaddingMode.PKCS7;
                aes.Mode = CipherMode.CBC;

                ICryptoTransform encryptor = aes.CreateEncryptor(aes.Key, aes.IV);

                using MemoryStream memoryStream = new MemoryStream();
                using CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, encryptor, CryptoStreamMode.Write);
                using (StreamWriter streamWriter = new StreamWriter((Stream)cryptoStream))
                {
                    streamWriter.Write(text);
                }

                array = memoryStream.ToArray();
            }
            return Convert.ToBase64String(array);
        }

        public static string Encrypt128(string text, string _key = null)
        {
            if (_key == null) _key = Key;

            byte[] iv = new byte[16];
            byte[] array;

            using (Aes aes = Aes.Create())
            {
                aes.Key = Encoding.UTF8.GetBytes(_key);
                aes.IV = iv;
                aes.Padding = PaddingMode.PKCS7;
                aes.Mode = CipherMode.CBC;

                ICryptoTransform encryptor = aes.CreateEncryptor(aes.Key, aes.IV);

                using MemoryStream memoryStream = new MemoryStream();
                using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, encryptor, CryptoStreamMode.Write))
                {
                    using (StreamWriter streamWriter = new StreamWriter((Stream)cryptoStream))
                    {
                        streamWriter.Write(text);
                    }

                    array = memoryStream.ToArray();
                }
            }
            return Convert.ToBase64String(array);
        }
         
    }

}
