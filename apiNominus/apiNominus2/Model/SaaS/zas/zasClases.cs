﻿using apiNominus2.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace apiNominus2.Model
{
    public class ZasHash
    {
        public ZasHash(dynamic objeto)
        {
            this.hash = Strings.Encrypt128(objeto);
        }
        public ZasHash(string xEx)
        {
            this.ex = xEx;
        }
        public string hash { get; set; }
        public string ex { get; set; }
        public dynamic dev { get; set; }
    }

    //public class ZasAuth
    //{
    //    public perSesion sesion { get; set; }
    //}







}
