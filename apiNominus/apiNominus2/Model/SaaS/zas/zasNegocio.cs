﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class zasNegocio
    {
        public zasNegocio()
        {
            perUsuarioPerfil = new HashSet<perUsuarioPerfil>();
        }

        public int iUnidadNegocio { get; set; }
        public int? iCliente { get; set; }
        public short? sPerfil { get; set; }
        public DateTime? dVigencia { get; set; }

        public virtual zasCliente iClienteNavigation { get; set; }
        public virtual perPerfil sPerfilNavigation { get; set; }
        [JsonIgnore] public virtual ICollection<perUsuarioPerfil> perUsuarioPerfil { get; set; }
    }
}
