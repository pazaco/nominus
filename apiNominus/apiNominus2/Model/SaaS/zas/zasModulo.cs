﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class zasModulo
    {
        public zasModulo()
        {
            zasDbModulo = new HashSet<zasDbModulo>();
        }

        public byte bModulo { get; set; }
        public string xModulo { get; set; }
        public string xPrefijo { get; set; }
        public byte? bSuper { get; set; }

        [JsonIgnore] public virtual ICollection<zasDbModulo> zasDbModulo { get; set; }
    }
}
