﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class zasTipoDocTributario
    {
        public zasTipoDocTributario()
        {
            zasCliente = new HashSet<zasCliente>();
        }

        public string cPais { get; set; }
        public string cTipoDocTributario { get; set; }
        public string xTipoDocTributario { get; set; }

        public virtual zasPais cPaisNavigation { get; set; }
        [JsonIgnore] public virtual ICollection<zasCliente> zasCliente { get; set; }
    }
}
