﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class zasDbModulo
    {
        public zasDbModulo()
        {
            zasConexion = new HashSet<zasConexion>();
        }

        public short sModulo { get; set; }
        public short sProducto { get; set; }
        public byte bModulo { get; set; }

        public virtual zasModulo bModuloNavigation { get; set; }
        public virtual zasProducto sProductoNavigation { get; set; }
        [JsonIgnore] public virtual ICollection<zasConexion> zasConexion { get; set; }
    }
}
