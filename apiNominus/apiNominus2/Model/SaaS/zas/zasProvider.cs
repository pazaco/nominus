﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class zasProvider
    {
        public zasProvider()
        {
            zasConexion = new HashSet<zasConexion>();
        }

        public byte bDbProvider { get; set; }
        public string xDbProvider { get; set; }
        public string xPlantilla { get; set; }

        [JsonIgnore] public virtual ICollection<zasConexion> zasConexion { get; set; }
    }
}
