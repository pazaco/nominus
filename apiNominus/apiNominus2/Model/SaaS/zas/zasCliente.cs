﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class zasCliente
    {
        public zasCliente()
        {
            zasConexion = new HashSet<zasConexion>();
            zasNegocio = new HashSet<zasNegocio>();
        }

        public int iCliente { get; set; }
        public string xNombreComercial { get; set; }
        public string xRazonSocial { get; set; }
        public string cPais { get; set; }
        public string cTipoDocTributario { get; set; }
        public string xDocTributario { get; set; }
        public string xDbNombre { get; set; }

        public virtual zasTipoDocTributario cPaiscTipoDocNavigation { get; set; }
        [JsonIgnore] public virtual ICollection<zasConexion> zasConexion { get; set; }
        [JsonIgnore] public virtual ICollection<zasNegocio> zasNegocio { get; set; }
    }
}
