﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class zasPais
    {
        public zasPais()
        {
            zasTipoDocTributario = new HashSet<zasTipoDocTributario>();
        }

        public string cPais { get; set; }
        public string xPais { get; set; }
        public string cMoneda { get; set; }
        public string cI18n { get; set; }

        [JsonIgnore] public virtual ICollection<zasTipoDocTributario> zasTipoDocTributario { get; set; }
    }
}
