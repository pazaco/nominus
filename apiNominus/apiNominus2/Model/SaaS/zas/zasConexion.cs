﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace apiNominus2.Model
{
    public partial class zasConexion
    {
        public int iCliente { get; set; }
        public short sModulo { get; set; }
        public byte? bDbProvider { get; set; }
        public string kCredencial { get; set; }
        public bool? lODBC { get; set; }

        public virtual zasProvider bDbProviderNavigation { get; set; }
        public virtual zasCliente iClienteNavigation { get; set; }
        public virtual zasDbModulo sModuloNavigation { get; set; }
    }

    public class Zas_Conexion
    {
        public int iCliente { get; set; }
        public short sProducto { get; set; }
        public byte bModulo { get; set; }
    }

    public class ZasStringConexion
    {
        public byte bTipoDb { get; set; }
        public string xTipoDb { get; set; }
        public string xConexion { get; set; }
    }
}
