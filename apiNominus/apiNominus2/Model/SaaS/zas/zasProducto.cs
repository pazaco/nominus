﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class zasProducto
    {
        public zasProducto()
        {
            perPerfil = new HashSet<perPerfil>();
            zasDbModulo = new HashSet<zasDbModulo>();
        }

        public short sProducto { get; set; }
        public string xProducto { get; set; }

        [JsonIgnore] public virtual ICollection<perPerfil> perPerfil { get; set; }
        [JsonIgnore] public virtual ICollection<zasDbModulo> zasDbModulo { get; set; }
    }
}
