﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Configuration;

#nullable disable

namespace apiNominus2.Model
{
    public partial class SaaSContext : DbContext
    {
        public SaaSContext()
        {
        }

        public SaaSContext(DbContextOptions<SaaSContext> options)
            : base(options)
        {
        }

        public virtual DbSet<perPerfil> perPerfil { get; set; }
        public virtual DbSet<perSesion> perSesion { get; set; }
        public virtual DbSet<perUser> perUser { get; set; }
        public virtual DbSet<perUserAgent> perUserAgent { get; set; }
        public virtual DbSet<perUsuarioPerfil> perUsuarioPerfil { get; set; }
        public virtual DbSet<zasCliente> zasCliente { get; set; }
        public virtual DbSet<zasConexion> zasConexion { get; set; }
        public virtual DbSet<zasDbModulo> zasDbModulo { get; set; }
        public virtual DbSet<zasModulo> zasModulo { get; set; }
        public virtual DbSet<zasNegocio> zasNegocio { get; set; }
        public virtual DbSet<zasPais> zasPais { get; set; }
        public virtual DbSet<zasProducto> zasProducto { get; set; }
        public virtual DbSet<zasProvider> zasProvider { get; set; }
        public virtual DbSet<zasTipoDocTributario> zasTipoDocTributario { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                IConfigurationRoot configuration = new ConfigurationBuilder()
                    .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                    .AddJsonFile("appsettings.json")
                    .Build();
                optionsBuilder.UseSqlServer(configuration.GetConnectionString("SaaS"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<perPerfil>(entity =>
            {
                entity.HasKey(e => e.sPerfil);

                entity.Property(e => e.sPerfil).ValueGeneratedNever();

                entity.Property(e => e.xPerfil)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.HasOne(d => d.sProductoNavigation)
                    .WithMany(p => p.perPerfil)
                    .HasForeignKey(d => d.sProducto)
                    .HasConstraintName("FK_perPerfil_zasProducto");
            });

            modelBuilder.Entity<perSesion>(entity =>
            {
                entity.HasKey(e => e.gSesion)
                    .HasName("PK_zasSesion");

                entity.Property(e => e.gSesion).ValueGeneratedNever();

                entity.Property(e => e.dInicio).HasPrecision(0);

                entity.Property(e => e.oSesion).IsUnicode(false);

                entity.Property(e => e.xTokenFCM).IsUnicode(false);

                entity.HasOne(d => d.iUserAgentNavigation)
                    .WithMany(p => p.perSesion)
                    .HasForeignKey(d => d.iUserAgent)
                    .HasConstraintName("FK_zasSesion_isoUserAgent");

                entity.HasOne(d => d.iUsuarioPerfilNavigation)
                    .WithMany(p => p.perSesion)
                    .HasForeignKey(d => d.iUsuarioPerfil)
                    .HasConstraintName("FK_perSesion_perUsuarioPerfil");
            });

            modelBuilder.Entity<perUser>(entity =>
            {
                entity.HasKey(e => e.iUsuario)
                    .HasName("PK_perUsuario");

                entity.Property(e => e.iUsuario).ValueGeneratedNever();

                entity.Property(e => e.cI18n)
                    .IsRequired()
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.xCelular)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.xCorreo)
                    .HasMaxLength(70)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<perUserAgent>(entity =>
            {
                entity.HasKey(e => e.iUserAgent)
                    .HasName("PK_isoUserAgent");

                entity.Property(e => e.iUserAgent).ValueGeneratedNever();

                entity.Property(e => e.xUserAgent).IsUnicode(false);
            });

            modelBuilder.Entity<perUsuarioPerfil>(entity =>
            {
                entity.HasKey(e => e.iUsuarioPerfil);

                entity.Property(e => e.iUsuarioPerfil).ValueGeneratedNever();

                entity.Property(e => e.dVigencia).HasColumnType("date");

                entity.HasOne(d => d.iUnidadNegocioNavigation)
                    .WithMany(p => p.perUsuarioPerfil)
                    .HasForeignKey(d => d.iUnidadNegocio)
                    .HasConstraintName("FK_perUsuarioPerfil_zasNegocio");

                entity.HasOne(d => d.iUsuarioNavigation)
                    .WithMany(p => p.perUsuarioPerfil)
                    .HasForeignKey(d => d.iUsuario)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_perUsuarioPerfil_perUsuario");
            });

            modelBuilder.Entity<zasCliente>(entity =>
            {
                entity.HasKey(e => e.iCliente);

                entity.Property(e => e.iCliente).ValueGeneratedNever();

                entity.Property(e => e.cPais)
                    .IsRequired()
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.cTipoDocTributario)
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.xDbNombre)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.xDocTributario)
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.xNombreComercial)
                    .IsRequired()
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.xRazonSocial)
                    .HasMaxLength(70)
                    .IsUnicode(false);

                entity.HasOne(d => d.cPaiscTipoDocNavigation)
                    .WithMany(p => p.zasCliente)
                    .HasForeignKey(d => new { d.cPais, d.cTipoDocTributario })
                    .HasConstraintName("FK_zasCliente_zasTipoDocTributario");
            });

            modelBuilder.Entity<zasConexion>(entity =>
            {
                entity.HasKey(e => new { e.iCliente, e.sModulo });

                entity.Property(e => e.kCredencial).IsUnicode(false);

                entity.HasOne(d => d.bDbProviderNavigation)
                    .WithMany(p => p.zasConexion)
                    .HasForeignKey(d => d.bDbProvider)
                    .HasConstraintName("FK_zasConexion_zasProvider");

                entity.HasOne(d => d.iClienteNavigation)
                    .WithMany(p => p.zasConexion)
                    .HasForeignKey(d => d.iCliente)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_zasConexion_zasCliente");

                entity.HasOne(d => d.sModuloNavigation)
                    .WithMany(p => p.zasConexion)
                    .HasForeignKey(d => d.sModulo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_zasConexion_zasDbCapa");
            });

            modelBuilder.Entity<zasDbModulo>(entity =>
            {
                entity.HasKey(e => e.sModulo)
                    .HasName("PK_zasDbCapa");

                entity.Property(e => e.sModulo).ValueGeneratedNever();

                entity.HasOne(d => d.bModuloNavigation)
                    .WithMany(p => p.zasDbModulo)
                    .HasForeignKey(d => d.bModulo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_zasDbModulo_zasModulo");

                entity.HasOne(d => d.sProductoNavigation)
                    .WithMany(p => p.zasDbModulo)
                    .HasForeignKey(d => d.sProducto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_zasDbCapa_zasProducto");
            });

            modelBuilder.Entity<zasModulo>(entity =>
            {
                entity.HasKey(e => e.bModulo);

                entity.Property(e => e.xModulo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.xPrefijo)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<zasNegocio>(entity =>
            {
                entity.HasKey(e => e.iUnidadNegocio);

                entity.Property(e => e.iUnidadNegocio).ValueGeneratedNever();

                entity.Property(e => e.dVigencia).HasColumnType("date");

                entity.HasOne(d => d.iClienteNavigation)
                    .WithMany(p => p.zasNegocio)
                    .HasForeignKey(d => d.iCliente)
                    .HasConstraintName("FK_zasNegocio_zasCliente");

                entity.HasOne(d => d.sPerfilNavigation)
                    .WithMany(p => p.zasNegocio)
                    .HasForeignKey(d => d.sPerfil)
                    .HasConstraintName("FK_zasNegocio_perPerfil1");
            });

            modelBuilder.Entity<zasPais>(entity =>
            {
                entity.HasKey(e => e.cPais);

                entity.Property(e => e.cPais)
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.cI18n)
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.cMoneda)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.xPais)
                    .IsRequired()
                    .HasMaxLength(40)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<zasProducto>(entity =>
            {
                entity.HasKey(e => e.sProducto);

                entity.Property(e => e.sProducto).ValueGeneratedNever();

                entity.Property(e => e.xProducto)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<zasProvider>(entity =>
            {
                entity.HasKey(e => e.bDbProvider);

                entity.Property(e => e.xDbProvider)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.xPlantilla)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<zasTipoDocTributario>(entity =>
            {
                entity.HasKey(e => new { e.cPais, e.cTipoDocTributario });

                entity.Property(e => e.cPais)
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.cTipoDocTributario)
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.xTipoDocTributario)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.cPaisNavigation)
                    .WithMany(p => p.zasTipoDocTributario)
                    .HasForeignKey(d => d.cPais)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_zasTipoDocTributario_zasPais");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
