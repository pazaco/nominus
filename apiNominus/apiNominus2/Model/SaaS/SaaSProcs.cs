﻿using apiNominus2.Helpers;
using apiNominus2.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace apiNominus2.Model
{
    public partial class Procs
    {

        public static ZasStringConexion ZasConexion(int iCliente,short sProducto,byte bModulo)
        {
            ZasStringConexion rsp = null;
            using (SaaSContext dbZ = new SaaSContext())
            {
                zasConexion _cnx = dbZ.zasConexion.FirstOrDefault(cx => cx.iCliente == iCliente && cx.sModuloNavigation.bModulo == bModulo && cx.sModuloNavigation.sProducto == sProducto);
                if (_cnx != null)
                {
                    zasProvider _prv = dbZ.zasProvider.Find(_cnx.bDbProvider);
                    zasCliente _cli = dbZ.zasCliente.Find(_cnx.iCliente);
                    zasDbModulo _dmd = dbZ.zasDbModulo.Find(_cnx.sModulo);
                    zasModulo _mod = dbZ.zasModulo.Find(_dmd.bModulo);
                    dynamic o = JsonConvert.DeserializeObject( _cnx.kCredencial);
                    rsp = new ZasStringConexion {
                        bTipoDb = _prv.bDbProvider,
                        xTipoDb = _prv.xDbProvider,
                        xConexion = string.Format(_prv.xPlantilla,_cli.xDbNombre,_mod.xPrefijo,o.userid,o.password)
                    };
                }
            }
            return rsp;
        }

        public string ZasVersion()
        {
            return "2.0.301";
        }
    }


   


}
