﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class perUsuarioPerfil
    {
        public perUsuarioPerfil()
        {
            perSesion = new HashSet<perSesion>();
        }

        public int iUsuarioPerfil { get; set; }
        public int? iUnidadNegocio { get; set; }
        public int iUsuario { get; set; }
        public DateTime? dVigencia { get; set; }
        public bool? lDefault { get; set; }

        public virtual zasNegocio iUnidadNegocioNavigation { get; set; }
        public virtual perUser iUsuarioNavigation { get; set; }
        [JsonIgnore] public virtual ICollection<perSesion> perSesion { get; set; }
    }
}
