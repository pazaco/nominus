﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class perPerfil
    {
        public perPerfil()
        {
            zasNegocio = new HashSet<zasNegocio>();
        }

        public short sPerfil { get; set; }
        public short? sProducto { get; set; }
        public string xPerfil { get; set; }

        public virtual zasProducto sProductoNavigation { get; set; }
        [JsonIgnore] public virtual ICollection<zasNegocio> zasNegocio { get; set; }
    }
}
