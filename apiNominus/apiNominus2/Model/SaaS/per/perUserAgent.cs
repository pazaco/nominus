﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class perUserAgent
    {
        public perUserAgent()
        {
            perSesion = new HashSet<perSesion>();
        }

        public int iUserAgent { get; set; }
        public string xUserAgent { get; set; }

        [JsonIgnore] public virtual ICollection<perSesion> perSesion { get; set; }
    }
}
