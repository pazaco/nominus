﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class perSesion
    {
        public Guid gSesion { get; set; }
        public int? iUsuarioPerfil { get; set; }
        public int? iUserAgent { get; set; }
        public DateTime? dInicio { get; set; }
        public string oSesion { get; set; }
        public string xTokenFCM { get; set; }

        public virtual perUserAgent iUserAgentNavigation { get; set; }
        public virtual perUsuarioPerfil iUsuarioPerfilNavigation { get; set; }
    }
}
