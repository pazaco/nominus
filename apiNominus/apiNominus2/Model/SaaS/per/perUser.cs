﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class perUser
    {
        public perUser()
        {
            perUsuarioPerfil = new HashSet<perUsuarioPerfil>();
        }

        public int iUsuario { get; set; }
        public string xCorreo { get; set; }
        public string xCelular { get; set; }
        public string cI18n { get; set; }

        [JsonIgnore] public virtual ICollection<perUsuarioPerfil> perUsuarioPerfil { get; set; }
    }

    public class Per_Login
    {
        public string xEmail { get; set; }
        public string xTokenFCM { get; set; }
    }

}
