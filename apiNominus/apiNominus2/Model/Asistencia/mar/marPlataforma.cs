﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class marPlataforma
    {
        public marPlataforma()
        {
            marDispositivo = new HashSet<marDispositivo>();
        }

        public byte bPlataforma { get; set; }
        public string xPlataforma { get; set; }
        public byte? bModo { get; set; }
        public DateTime? dUltimaSicronizacion { get; set; }

        public virtual marModo bModoNavigation { get; set; }
        [JsonIgnore] public virtual ICollection<marDispositivo> marDispositivo { get; set; }
    }
}
