﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace apiNominus2.Model
{
    public partial class AsistenciaContext : DbContext
    {
        public virtual DbSet<marDirectorio> marDirectorio { get; set; }
        public virtual DbSet<marDispositivo> marDispositivo { get; set; }
        public virtual DbSet<marEmparejar> marEmparejar { get; set; }
        public virtual DbSet<marGeometria> marGeometria { get; set; }
        public virtual DbSet<marMapaGPS> marMapaGPS { get; set; }
        public virtual DbSet<marMarcacion> marMarcacion { get; set; }
        public virtual DbSet<marModo> marModo { get; set; }
        public virtual DbSet<marPlataforma> marPlataforma { get; set; }
        public virtual DbSet<marRiesgoCOVID> marRiesgoCOVID { get; set; }

        partial void OnModelCreatingPartialMar(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<marDirectorio>(entity =>
            {
                entity.HasKey(e => new { e.iPersona, e.iContratoActual, e.bSistema });

                entity.Property(e => e.iContratoActual)
                    .HasMaxLength(10)
                    .IsFixedLength(true);

                entity.Property(e => e.xCodigo)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<marDispositivo>(entity =>
            {
                entity.HasKey(e => e.sDispositivo);

                entity.Property(e => e.sDispositivo).ValueGeneratedNever();

                entity.Property(e => e.cLlave)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.xDescripcion)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.bPlataformaNavigation)
                    .WithMany(p => p.marDispositivo)
                    .HasForeignKey(d => d.bPlataforma)
                    .HasConstraintName("FK_marDispositivo_marPlataforma");
            });
            modelBuilder.Entity<marEmparejar>(entity =>
            {
                entity.HasKey(e => e.iPersona);

                entity.Property(e => e.iPersona).ValueGeneratedNever();

                entity.Property(e => e.dVencimiento).HasColumnType("datetime");

                entity.Property(e => e.xRespuesta)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<marGeometria>(entity =>
            {
                entity.HasKey(e => new { e.sDispositivo, e.bPunto });

                entity.Property(e => e.dAprobacion).HasColumnType("date");

                entity.Property(e => e.xPunto)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.HasOne(d => d.sDispositivoNavigation)
                    .WithMany(p => p.marGeometria)
                    .HasForeignKey(d => d.sDispositivo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_marGeometria_marDispositivo");
            });
            modelBuilder.Entity<marMapaGPS>(entity =>
            {
                entity.HasKey(e => new { e.sDispositivo, e.bPunto, e.bPuntoGPS });

                entity.HasOne(d => d.marGeometria)
                    .WithMany(p => p.marMapaGPS)
                    .HasForeignKey(d => new { d.sDispositivo, d.bPunto })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_marMapaGPS_marGeometria");
            });
            modelBuilder.Entity<marMarcacion>(entity =>
            {
                entity.HasKey(e => e.iMarca);

                entity.Property(e => e.iMarca).ValueGeneratedNever();

                entity.Property(e => e.dHora).HasColumnType("datetime");

                entity.HasOne(d => d.iControlNavigation)
                    .WithMany(p => p.marMarcacion)
                    .HasForeignKey(d => d.iControl)
                    .HasConstraintName("FK_marMarcacion_asiControl");

                entity.HasOne(d => d.sDispositivoNavigation)
                    .WithMany(p => p.marMarcacion)
                    .HasForeignKey(d => d.sDispositivo)
                    .HasConstraintName("FK_marMarcacion_marDispositivo");
            });
            modelBuilder.Entity<marModo>(entity =>
            {
                entity.HasKey(e => e.bModo);

                entity.Property(e => e.xModo)
                    .HasMaxLength(40)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<marPlataforma>(entity =>
            {
                entity.HasKey(e => e.bPlataforma);

                entity.Property(e => e.dUltimaSicronizacion).HasColumnType("datetime");

                entity.Property(e => e.xPlataforma)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.HasOne(d => d.bModoNavigation)
                    .WithMany(p => p.marPlataforma)
                    .HasForeignKey(d => d.bModo)
                    .HasConstraintName("FK_marPlataforma_marModo");
            });
            modelBuilder.Entity<marRiesgoCOVID>(entity =>
            {
                entity.HasKey(e => new { e.iContrato, e.dFecha });

                entity.Property(e => e.dFecha).HasColumnType("date");

                entity.Property(e => e.aEnfermedades)
                    .HasMaxLength(15)
                    .IsUnicode(false);
            });



        }
    }
}
