﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class marModo
    {
        public marModo()
        {
            marPlataforma = new HashSet<marPlataforma>();
        }

        public byte bModo { get; set; }
        public string xModo { get; set; }

        [JsonIgnore] public virtual ICollection<marPlataforma> marPlataforma { get; set; }
    }
}
