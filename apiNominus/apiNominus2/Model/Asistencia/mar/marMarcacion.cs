﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class marMarcacion
    {
        public int iMarca { get; set; }
        public short? sDispositivo { get; set; }
        public int? iPersona { get; set; }
        public DateTime? dHora { get; set; }
        public int? iControl { get; set; }

        public virtual asiControl iControlNavigation { get; set; }
        public virtual marDispositivo sDispositivoNavigation { get; set; }
    }
}
