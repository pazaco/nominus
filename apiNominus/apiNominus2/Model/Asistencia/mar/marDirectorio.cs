﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class marDirectorio
    {
        public int iPersona { get; set; }
        public string iContratoActual { get; set; }
        public byte bSistema { get; set; }
        public string xCodigo { get; set; }
    }
}
