﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class marGeometria
    {
        public marGeometria()
        {
            marMapaGPS = new HashSet<marMapaGPS>();
        }

        public short sDispositivo { get; set; }
        public byte bPunto { get; set; }
        public string xPunto { get; set; }
        public int? iAprobador { get; set; }
        public DateTime? dAprobacion { get; set; }

        public virtual marDispositivo sDispositivoNavigation { get; set; }
        [JsonIgnore] public virtual ICollection<marMapaGPS> marMapaGPS { get; set; }
    }
}
