﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class marEmparejar
    {
        public int iPersona { get; set; }
        public DateTime? dVencimiento { get; set; }
        public string xRespuesta { get; set; }
    }
}
