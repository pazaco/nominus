﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class marDispositivo
    {
        public marDispositivo()
        {
            marGeometria = new HashSet<marGeometria>();
            marMarcacion = new HashSet<marMarcacion>();
        }

        public short sDispositivo { get; set; }
        public string xDescripcion { get; set; }
        public string cLlave { get; set; }
        public byte? bPlataforma { get; set; }
        public int? iDispositivoInterno { get; set; }
        public short? sSede { get; set; }

        public virtual marPlataforma bPlataformaNavigation { get; set; }
        [JsonIgnore] public virtual ICollection<marGeometria> marGeometria { get; set; }
        [JsonIgnore] public virtual ICollection<marMarcacion> marMarcacion { get; set; }
    }
}
