﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class marMapaGPS
    {
        public short sDispositivo { get; set; }
        public byte bPunto { get; set; }
        public byte bPuntoGPS { get; set; }
        public double? fLat { get; set; }
        public double? fLon { get; set; }

        public virtual marGeometria marGeometria { get; set; }
    }
}
