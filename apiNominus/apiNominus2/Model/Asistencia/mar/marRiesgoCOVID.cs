﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class marRiesgoCOVID
    {
        public int iContrato { get; set; }
        public DateTime dFecha { get; set; }
        public double? fPeso { get; set; }
        public byte? bTalla { get; set; }
        public string aEnfermedades { get; set; }
    }
}
