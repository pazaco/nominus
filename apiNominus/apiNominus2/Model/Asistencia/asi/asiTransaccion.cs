﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class asiTransaccion
    {
        public string iTransaccion { get; set; }
        public byte bTipoPapeleta { get; set; }
        public int iPapeleta { get; set; }
        public short sConsecutivo { get; set; }
        public short? sTipoTransaccion { get; set; }
        public string jsonTransaccion { get; set; }

        public virtual asiPapeleta asiPapeleta { get; set; }
        public virtual asiTipoTransaccion sTipoTransaccionNavigation { get; set; }
    }
}
