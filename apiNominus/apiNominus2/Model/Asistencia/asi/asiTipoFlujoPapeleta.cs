﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class asiTipoFlujoPapeleta
    {
        public asiTipoFlujoPapeleta()
        {
            asiFlujoPapeleta = new HashSet<asiFlujoPapeleta>();
        }

        public string cTipoFlujo { get; set; }
        public string xTipoFlujo { get; set; }

        [JsonIgnore] public virtual ICollection<asiFlujoPapeleta> asiFlujoPapeleta { get; set; }
    }
}
