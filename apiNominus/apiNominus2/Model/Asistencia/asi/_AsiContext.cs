﻿using Microsoft.EntityFrameworkCore;

#nullable disable

namespace apiNominus2.Model
{
    public partial class AsistenciaContext : DbContext
    {
        public virtual DbSet<asiAsistencia> asiAsistencia { get; set; }
        public virtual DbSet<asiControl> asiControl { get; set; }
        public virtual DbSet<asiFlujoPapeleta> asiFlujoPapeleta { get; set; }
        public virtual DbSet<asiPapeleta> asiPapeleta { get; set; }
        public virtual DbSet<asiRiesgoCOVID> asiRiesgoCOVID { get; set; }
        public virtual DbSet<asiRolNotificador> asiRolNotificador { get; set; }
        public virtual DbSet<asiTipoFlujoPapeleta> asiTipoFlujoPapeleta { get; set; }
        public virtual DbSet<asiTipoPapeleta> asiTipoPapeleta { get; set; }
        public virtual DbSet<asiTipoTransaccion> asiTipoTransaccion { get; set; }
        public virtual DbSet<asiTransaccion> asiTransaccion { get; set; }

        partial void OnModelCreatingPartialAsi(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<asiAsistencia>(entity =>
            {
                entity.HasKey(e => e.iAsistencia);

                entity.Property(e => e.iAsistencia).ValueGeneratedNever();

                entity.Property(e => e.dFecha).HasColumnType("date");
            });
            modelBuilder.Entity<asiControl>(entity =>
            {
                entity.HasKey(e => e.iControl);

                entity.Property(e => e.iControl).ValueGeneratedNever();

                entity.Property(e => e.dHoraControl).HasPrecision(0);

                entity.Property(e => e.dHoraHorario).HasPrecision(0);

                entity.HasOne(d => d.iAsistenciaNavigation)
                    .WithMany(p => p.asiControl)
                    .HasForeignKey(d => d.iAsistencia)
                    .HasConstraintName("FK_asiControl_asiAsistencia");

                entity.HasOne(d => d.sControlNavigation)
                    .WithMany(p => p.asiControl)
                    .HasForeignKey(d => d.sControl)
                    .HasConstraintName("FK_asiControl_horControl");

                entity.HasOne(d => d.asiPapeleta)
                    .WithMany(p => p.asiControl)
                    .HasForeignKey(d => new { d.bTipoPapeleta, d.iPapeleta })
                    .HasConstraintName("FK_asiControl_asiPapeleta");
            });
            modelBuilder.Entity<asiFlujoPapeleta>(entity =>
            {
                entity.HasKey(e => e.iFlujoPapeleta);

                entity.Property(e => e.iFlujoPapeleta)
                    .HasMaxLength(10)
                    .IsFixedLength(true);

                entity.Property(e => e.cRol)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.cTipoFlujo)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.dFlujo).HasPrecision(0);

                entity.Property(e => e.xNotificacion).IsUnicode(false);

                entity.HasOne(d => d.cRolNavigation)
                    .WithMany(p => p.asiFlujoPapeleta)
                    .HasForeignKey(d => d.cRol)
                    .HasConstraintName("FK_asiFlujoPapeleta_asiRolNotificador");

                entity.HasOne(d => d.cTipoFlujoNavigation)
                    .WithMany(p => p.asiFlujoPapeleta)
                    .HasForeignKey(d => d.cTipoFlujo)
                    .HasConstraintName("FK_asiFlujoPapeleta_asiTipoFlujoPapeleta");

                entity.HasOne(d => d.asiPapeleta)
                    .WithMany(p => p.asiFlujoPapeleta)
                    .HasForeignKey(d => new { d.bTipoPapeleta, d.iPapeleta })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_asiFlujoPapeleta_asiPapeleta");
            });
            modelBuilder.Entity<asiPapeleta>(entity =>
            {
                entity.HasKey(e => new { e.bTipoPapeleta, e.iPapeleta });

                entity.Property(e => e.dAlta).HasColumnType("date");

                entity.HasOne(d => d.bTipoPapeletaNavigation)
                    .WithMany(p => p.asiPapeleta)
                    .HasForeignKey(d => d.bTipoPapeleta)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_asiPapeleta_asiTipoPapeleta");
            });
            modelBuilder.Entity<asiRiesgoCOVID>(entity =>
            {
                entity.HasKey(e => e.iAsistencia);

                entity.Property(e => e.iAsistencia).ValueGeneratedNever();

                entity.Property(e => e.aSintomas)
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.HasOne(d => d.iAsistenciaNavigation)
                    .WithOne(p => p.asiRiesgoCOVID)
                    .HasForeignKey<asiRiesgoCOVID>(d => d.iAsistencia)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_asiRiesgoCOVID_asiAsistencia1");
            });
            modelBuilder.Entity<asiRolNotificador>(entity =>
            {
                entity.HasKey(e => e.cRol);

                entity.Property(e => e.cRol)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.xRol)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<asiTipoFlujoPapeleta>(entity =>
            {
                entity.HasKey(e => e.cTipoFlujo);

                entity.Property(e => e.cTipoFlujo)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.xTipoFlujo)
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<asiTipoPapeleta>(entity =>
            {
                entity.HasKey(e => e.bTipoPapeleta);

                entity.Property(e => e.xTipoPapeleta)
                    .HasMaxLength(80)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<asiTipoTransaccion>(entity =>
            {
                entity.HasKey(e => e.sTipoTransaccion);

                entity.Property(e => e.sTipoTransaccion).ValueGeneratedNever();

                entity.Property(e => e.xTipoTransaccion)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<asiTransaccion>(entity =>
            {
                entity.HasKey(e => e.iTransaccion);

                entity.Property(e => e.iTransaccion)
                    .HasMaxLength(10)
                    .IsFixedLength(true);

                entity.Property(e => e.jsonTransaccion).IsUnicode(false);

                entity.HasOne(d => d.sTipoTransaccionNavigation)
                    .WithMany(p => p.asiTransaccion)
                    .HasForeignKey(d => d.sTipoTransaccion)
                    .HasConstraintName("FK_asiTransaccion_asiTipoTransaccion");

                entity.HasOne(d => d.asiPapeleta)
                    .WithMany(p => p.asiTransaccion)
                    .HasForeignKey(d => new { d.bTipoPapeleta, d.iPapeleta })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_asiTransaccion_asiPapeleta");
            });
        }
    }
}