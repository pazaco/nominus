﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class asiRiesgoCOVID
    {
        public int iAsistencia { get; set; }
        public double? fTemperatura { get; set; }
        public string aSintomas { get; set; }
        public byte? bEvaluacion { get; set; }
        public bool? lContacto { get; set; }
        public bool? lLavadoManos { get; set; }
        public bool? lTapabocas { get; set; }
        public bool? lRopas { get; set; }

        public virtual asiAsistencia iAsistenciaNavigation { get; set; }
    }
}
