﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class asiTipoTransaccion
    {
        public asiTipoTransaccion()
        {
            asiTransaccion = new HashSet<asiTransaccion>();
        }

        public short sTipoTransaccion { get; set; }
        public string xTipoTransaccion { get; set; }

        [JsonIgnore] public virtual ICollection<asiTransaccion> asiTransaccion { get; set; }
    }
}
