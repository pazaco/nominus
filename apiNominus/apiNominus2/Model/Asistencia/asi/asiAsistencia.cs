﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class asiAsistencia
    {
        public asiAsistencia()
        {
            asiControl = new HashSet<asiControl>();
        }

        public int iAsistencia { get; set; }
        public int iContrato { get; set; }
        public DateTime dFecha { get; set; }

        public virtual asiRiesgoCOVID asiRiesgoCOVID { get; set; }
        [JsonIgnore] public virtual ICollection<asiControl> asiControl { get; set; }
    }
}
