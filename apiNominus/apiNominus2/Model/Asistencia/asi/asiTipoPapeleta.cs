﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class asiTipoPapeleta
    {
        public asiTipoPapeleta()
        {
            asiPapeleta = new HashSet<asiPapeleta>();
        }

        public byte bTipoPapeleta { get; set; }
        public string xTipoPapeleta { get; set; }

        [JsonIgnore] public virtual ICollection<asiPapeleta> asiPapeleta { get; set; }
    }
}
