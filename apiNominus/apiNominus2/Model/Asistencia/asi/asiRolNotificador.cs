﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class asiRolNotificador
    {
        public asiRolNotificador()
        {
            asiFlujoPapeleta = new HashSet<asiFlujoPapeleta>();
        }

        public string cRol { get; set; }
        public string xRol { get; set; }

        [JsonIgnore] public virtual ICollection<asiFlujoPapeleta> asiFlujoPapeleta { get; set; }
    }
}
