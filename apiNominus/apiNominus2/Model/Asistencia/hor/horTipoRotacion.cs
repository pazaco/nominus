﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class horTipoRotacion
    {
        public horTipoRotacion()
        {
            horProgramacion = new HashSet<horProgramacion>();
        }

        public short sRotacion { get; set; }
        public string cFactor { get; set; }
        public byte? bFactor { get; set; }
        public byte? bCoeficiente { get; set; }
        public string cDescriptor { get; set; }
        public short? sHorario { get; set; }

        public virtual horHorario sHorarioNavigation { get; set; }
        [JsonIgnore] public virtual ICollection<horProgramacion> horProgramacion { get; set; }
    }
}
