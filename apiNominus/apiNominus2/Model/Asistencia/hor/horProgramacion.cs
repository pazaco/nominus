﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class horProgramacion
    {
        public horProgramacion()
        {
            horDetProgramacion = new HashSet<horDetProgramacion>();
        }

        public int iProgramacion { get; set; }
        public short? sRotacion { get; set; }
        public int iContrato { get; set; }
        public DateTime? dDesde { get; set; }
        public DateTime? dHasta { get; set; }
        public double? fHorasInicial { get; set; }
        public double? fHorasTrabajadas { get; set; }
        public double? fHorasRedimidas { get; set; }
        public byte? bPrioridad { get; set; }

        public virtual horTipoRotacion sRotacionNavigation { get; set; }
        [JsonIgnore] public virtual ICollection<horDetProgramacion> horDetProgramacion { get; set; }
    }
}
