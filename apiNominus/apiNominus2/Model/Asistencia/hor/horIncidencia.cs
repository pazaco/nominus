﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class horIncidencia
    {
        public horIncidencia()
        {
            horMargenIncidencia = new HashSet<horMargenIncidencia>();
        }

        public short sIncidencia { get; set; }
        public string xIncidencia { get; set; }
        public bool? lNotificaEmpleado { get; set; }
        public bool? lNotificaJefe { get; set; }
        public bool? lNotificaRRHH { get; set; }
        public bool? lAutorizaJefe { get; set; }
        public bool? lAutorizaRRHH { get; set; }
        public byte? bTipoPapeleta { get; set; }

        [JsonIgnore] public virtual ICollection<horMargenIncidencia> horMargenIncidencia { get; set; }
    }
}
