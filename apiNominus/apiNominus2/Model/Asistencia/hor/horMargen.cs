﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class horMargen
    {
        public horMargen()
        {
            horControl = new HashSet<horControl>();
            horMargenIncidencia = new HashSet<horMargenIncidencia>();
        }

        public short sMargen { get; set; }
        public string xMargen { get; set; }
        public string cTipoControl { get; set; }

        [JsonIgnore] public virtual ICollection<horControl> horControl { get; set; }
        [JsonIgnore] public virtual ICollection<horMargenIncidencia> horMargenIncidencia { get; set; }
    }
}
