﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class horInfoAnexa
    {
        public short sInfoAnexa { get; set; }
        public short? sJornada { get; set; }
        public string xDescriptor { get; set; }

        public virtual horJornada sJornadaNavigation { get; set; }
    }
}
