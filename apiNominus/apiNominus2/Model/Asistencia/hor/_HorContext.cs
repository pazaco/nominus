﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace apiNominus2.Model
{
    public partial class AsistenciaContext : DbContext
    {
        public virtual DbSet<horControl> horControl { get; set; }
        public virtual DbSet<horDetProgramacion> horDetProgramacion { get; set; }
        public virtual DbSet<horHorario> horHorario { get; set; }
        public virtual DbSet<horIncidencia> horIncidencia { get; set; }
        public virtual DbSet<horInfoAnexa> horInfoAnexa { get; set; }
        public virtual DbSet<horJornada> horJornada { get; set; }
        public virtual DbSet<horMargen> horMargen { get; set; }
        public virtual DbSet<horMargenIncidencia> horMargenIncidencia { get; set; }
        public virtual DbSet<horProgramacion> horProgramacion { get; set; }
        public virtual DbSet<horRequerimientoMarca> horRequerimientoMarca { get; set; }
        public virtual DbSet<horTag> horTag { get; set; }
        public virtual DbSet<horTipoRotacion> horTipoRotacion { get; set; }

        partial void OnModelCreatingPartialHor(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<horControl>(entity =>
            {
                entity.HasKey(e => e.sControl)
                    .HasName("PK_asi_Control_1");

                entity.Property(e => e.sControl).ValueGeneratedNever();

                entity.Property(e => e.cTipoControl)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.dHora).HasColumnType("time(0)");

                entity.Property(e => e.sDiasPrevio).HasDefaultValueSql("((0))");

                entity.HasOne(d => d.sJornadaNavigation)
                    .WithMany(p => p.horControl)
                    .HasForeignKey(d => d.sJornada)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_asi_Control_asi_Jornada");

                entity.HasOne(d => d.sMargenNavigation)
                    .WithMany(p => p.horControl)
                    .HasForeignKey(d => d.sMargen)
                    .HasConstraintName("FK_horControl_horMargen");
            });
            modelBuilder.Entity<horDetProgramacion>(entity =>
            {
                entity.HasKey(e => e.iDetProgramacion);

                entity.Property(e => e.iDetProgramacion).ValueGeneratedNever();

                entity.Property(e => e.dFecha).HasColumnType("date");

                entity.HasOne(d => d.iProgramacionNavigation)
                    .WithMany(p => p.horDetProgramacion)
                    .HasForeignKey(d => d.iProgramacion)
                    .HasConstraintName("FK_horDetProgramacion_horProgramacion");

                entity.HasOne(d => d.sJornadaNavigation)
                    .WithMany(p => p.horDetProgramacion)
                    .HasForeignKey(d => d.sJornada)
                    .HasConstraintName("FK_horDetProgramacion_horTag");
            });
            modelBuilder.Entity<horHorario>(entity =>
            {
                entity.HasKey(e => e.sHorario);

                entity.Property(e => e.xHorario)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<horIncidencia>(entity =>
            {
                entity.HasKey(e => e.sIncidencia)
                    .HasName("PK_asi_Incidencia1");

                entity.Property(e => e.sIncidencia).ValueGeneratedNever();

                entity.Property(e => e.xIncidencia)
                    .HasMaxLength(60)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<horInfoAnexa>(entity =>
            {
                entity.HasKey(e => e.sInfoAnexa);

                entity.Property(e => e.sInfoAnexa).ValueGeneratedNever();

                entity.Property(e => e.xDescriptor)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.HasOne(d => d.sJornadaNavigation)
                    .WithMany(p => p.horInfoAnexa)
                    .HasForeignKey(d => d.sJornada)
                    .HasConstraintName("FK_horInfoAnexa_horJornada");
            });
            modelBuilder.Entity<horJornada>(entity =>
            {
                entity.HasKey(e => e.sJornada);

                entity.Property(e => e.sJornada).ValueGeneratedNever();

                entity.Property(e => e.cDias)
                    .HasMaxLength(8)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('LMWJVSD')");

                entity.Property(e => e.xJornada)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.bRequerimientoMarcaNavigation)
                    .WithMany(p => p.horJornada)
                    .HasForeignKey(d => d.bRequerimientoMarca)
                    .HasConstraintName("FK_horJornada_horRequerimientoMarca");

                entity.HasOne(d => d.sHorarioNavigation)
                    .WithMany(p => p.horJornada)
                    .HasForeignKey(d => d.sHorario)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_asi_Jornada_asi_Horario");
            });
            modelBuilder.Entity<horMargen>(entity =>
            {
                entity.HasKey(e => e.sMargen);

                entity.Property(e => e.sMargen).ValueGeneratedNever();

                entity.Property(e => e.cTipoControl)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.xMargen)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<horMargenIncidencia>(entity =>
            {
                entity.HasKey(e => e.iMargen);

                entity.Property(e => e.iMargen).ValueGeneratedNever();

                entity.Property(e => e.sIncidencia).HasDefaultValueSql("((0))");

                entity.HasOne(d => d.sIncidenciaNavigation)
                    .WithMany(p => p.horMargenIncidencia)
                    .HasForeignKey(d => d.sIncidencia)
                    .HasConstraintName("FK_horMargenIncidencia_horIncidencia");

                entity.HasOne(d => d.sMargenNavigation)
                    .WithMany(p => p.horMargenIncidencia)
                    .HasForeignKey(d => d.sMargen)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_horMargenIncidencia_horMargen");
            });
            modelBuilder.Entity<horProgramacion>(entity =>
            {
                entity.HasKey(e => e.iProgramacion);

                entity.Property(e => e.iProgramacion).ValueGeneratedNever();

                entity.Property(e => e.dDesde).HasColumnType("date");

                entity.Property(e => e.dHasta).HasColumnType("date");

                entity.Property(e => e.fHorasInicial).HasDefaultValueSql("((0))");

                entity.Property(e => e.fHorasRedimidas).HasDefaultValueSql("((0))");

                entity.Property(e => e.fHorasTrabajadas).HasDefaultValueSql("((0))");

                entity.HasOne(d => d.sRotacionNavigation)
                    .WithMany(p => p.horProgramacion)
                    .HasForeignKey(d => d.sRotacion)
                    .HasConstraintName("FK_horProgramacion_horTipoRotacion");
            });
            modelBuilder.Entity<horRequerimientoMarca>(entity =>
            {
                entity.HasKey(e => e.bRequerimientoMarca);

                entity.Property(e => e.xRequerimientoMarca)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<horTag>(entity =>
            {
                entity.HasKey(e => e.sJornada);

                entity.Property(e => e.sJornada).ValueGeneratedNever();

                entity.Property(e => e.cColor)
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.cTag)
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.HasOne(d => d.sJornadaNavigation)
                    .WithOne(p => p.horTag)
                    .HasForeignKey<horTag>(d => d.sJornada)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_horTag_horJornada");
            });
            modelBuilder.Entity<horTipoRotacion>(entity =>
            {
                entity.HasKey(e => e.sRotacion);

                entity.Property(e => e.sRotacion).ValueGeneratedNever();

                entity.Property(e => e.cDescriptor)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.cFactor)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.HasOne(d => d.sHorarioNavigation)
                    .WithMany(p => p.horTipoRotacion)
                    .HasForeignKey(d => d.sHorario)
                    .HasConstraintName("FK_horTipoRotacion_horHorario");
            });

        }
    }
}
