﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class horRequerimientoMarca
    {
        public horRequerimientoMarca()
        {
            horJornada = new HashSet<horJornada>();
        }

        public byte bRequerimientoMarca { get; set; }
        public string xRequerimientoMarca { get; set; }

        [JsonIgnore] public virtual ICollection<horJornada> horJornada { get; set; }
    }
}
