﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class horJornada
    {
        public horJornada()
        {
            horControl = new HashSet<horControl>();
            horInfoAnexa = new HashSet<horInfoAnexa>();
        }

        public short sJornada { get; set; }
        public string xJornada { get; set; }
        public short sHorario { get; set; }
        public string cDias { get; set; }
        public byte? bRequerimientoMarca { get; set; }
        public byte? bOrden { get; set; }

        public virtual horRequerimientoMarca bRequerimientoMarcaNavigation { get; set; }
        public virtual horHorario sHorarioNavigation { get; set; }
        public virtual horTag horTag { get; set; }
        [JsonIgnore] public virtual ICollection<horControl> horControl { get; set; }
        [JsonIgnore] public virtual ICollection<horInfoAnexa> horInfoAnexa { get; set; }
    }
}
