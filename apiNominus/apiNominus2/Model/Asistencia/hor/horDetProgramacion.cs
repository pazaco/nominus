﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class horDetProgramacion
    {
        public int iDetProgramacion { get; set; }
        public int? iProgramacion { get; set; }
        public DateTime? dFecha { get; set; }
        public short? sJornada { get; set; }

        public virtual horProgramacion iProgramacionNavigation { get; set; }
        public virtual horTag sJornadaNavigation { get; set; }
    }
}
