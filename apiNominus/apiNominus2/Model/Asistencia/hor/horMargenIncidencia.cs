﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class horMargenIncidencia
    {
        public int iMargen { get; set; }
        public short sMargen { get; set; }
        public int? iDiferencia { get; set; }
        public short? sIncidencia { get; set; }

        public virtual horIncidencia sIncidenciaNavigation { get; set; }
        public virtual horMargen sMargenNavigation { get; set; }
    }
}
