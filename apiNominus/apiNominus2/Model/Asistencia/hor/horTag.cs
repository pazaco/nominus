﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class horTag
    {
        public horTag()
        {
            horDetProgramacion = new HashSet<horDetProgramacion>();
        }

        public short sJornada { get; set; }
        public string cTag { get; set; }
        public string cColor { get; set; }

        public virtual horJornada sJornadaNavigation { get; set; }
        [JsonIgnore] public virtual ICollection<horDetProgramacion> horDetProgramacion { get; set; }
    }
}
