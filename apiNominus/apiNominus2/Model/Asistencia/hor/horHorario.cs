﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class horHorario
    {
        public horHorario()
        {
            horJornada = new HashSet<horJornada>();
            horTipoRotacion = new HashSet<horTipoRotacion>();
        }

        public short sHorario { get; set; }
        public short? sEmpresa { get; set; }
        public string xHorario { get; set; }
        public byte? bPrioridad { get; set; }

        [JsonIgnore] public virtual ICollection<horJornada> horJornada { get; set; }
        [JsonIgnore] public virtual ICollection<horTipoRotacion> horTipoRotacion { get; set; }
    }
}
