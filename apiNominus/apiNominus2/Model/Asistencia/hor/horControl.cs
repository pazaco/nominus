﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class horControl
    {
        public horControl()
        {
            asiControl = new HashSet<asiControl>();
        }

        public short sJornada { get; set; }
        public short sControl { get; set; }
        public string cTipoControl { get; set; }
        public TimeSpan dHora { get; set; }
        public short? sControlPrevio { get; set; }
        public byte? sDiasPrevio { get; set; }
        public short? sMargen { get; set; }

        public virtual horJornada sJornadaNavigation { get; set; }
        public virtual horMargen sMargenNavigation { get; set; }
        [JsonIgnore] public virtual ICollection<asiControl> asiControl { get; set; }
    }
}
