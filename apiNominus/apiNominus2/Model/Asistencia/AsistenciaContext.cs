﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Configuration;

#nullable disable

namespace apiNominus2.Model
{
    public partial class AsistenciaContext : DbContext
    {
        private int iCliente;
        private short sProducto;
        private byte bModulo;
        private bool _Connected = false;

        public AsistenciaContext(int iCliente, short sProducto, byte bModulo)
        {
            this.iCliente = iCliente;
            this.bModulo = bModulo;
            this.sProducto = sProducto;
        }


        public AsistenciaContext(DbContextOptions<AsistenciaContext> options)
            : base(options)
        {
        }

        public bool isConnected()
        {
            return _Connected;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                ZasStringConexion strConn = Procs.ZasConexion(iCliente,sProducto,bModulo);
                if (strConn == null)
                {
                    _Connected = false;
                }
                else
                {
                    _Connected = true;
                    optionsBuilder.UseSqlServer(strConn.xConexion);
                }


            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            OnModelCreatingPartialHor(modelBuilder);
            OnModelCreatingPartialMar(modelBuilder);
            OnModelCreatingPartialAsi(modelBuilder);
        }

        partial void OnModelCreatingPartialHor(ModelBuilder modelBuilder);
        partial void OnModelCreatingPartialMar(ModelBuilder modelBuilder);
        partial void OnModelCreatingPartialAsi(ModelBuilder modelBuilder);

    }
}
