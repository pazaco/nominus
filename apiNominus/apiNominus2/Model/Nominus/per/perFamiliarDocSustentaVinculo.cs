﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class perFamiliarDocSustentaVinculo
    {
        public int iFamiliar { get; set; }
        public byte bDocSustentaVinculo { get; set; }

        public virtual isoDocSustentaVinculoFamiliar bDocSustentaVinculoNavigation { get; set; }
        public virtual perFamiliar iFamiliarNavigation { get; set; }
    }
}
