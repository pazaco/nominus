﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class perPersonaCodigo
    {
        public int iPersona { get; set; }
        public string xCodigo { get; set; }

        public virtual perPersona iPersonaNavigation { get; set; }
    }
}
