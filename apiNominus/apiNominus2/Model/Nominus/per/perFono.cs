﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class perFono
    {
        public int iFono { get; set; }
        public int iPersona { get; set; }
        public string xNumeroFono { get; set; }
        public byte bTipo { get; set; }
        public string xDescripcion { get; set; }
        public byte bPrioridad { get; set; }

        public virtual perPersona iPersonaNavigation { get; set; }
    }
}
