﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class perUsuarioSuper
    {
        public int iUsuario { get; set; }

        public virtual perUsuario iUsuarioNavigation { get; set; }
    }
}
