﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class perFoto
    {
        public int iFoto { get; set; }
        public int iPersona { get; set; }
        public byte[] Foto { get; set; }
        public byte? bTipoFoto { get; set; }
        public string jFoto { get; set; }

        public virtual perTipoFoto bTipoFotoNavigation { get; set; }
        public virtual perPersona iPersonaNavigation { get; set; }
    }
}
