﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class perEducacion
    {
        public int iEducacion { get; set; }
        public int iPersona { get; set; }
        public byte bNivelEducativo { get; set; }
        public string cPais { get; set; }
        public int? iInstitucionEducativa { get; set; }
        public int? iCarrera { get; set; }
        public string xTitulo { get; set; }
        public short sAnoEgreso { get; set; }
        public byte? bAnos { get; set; }
        public byte? bMeses { get; set; }
        public byte? bDias { get; set; }
        public DateTime? dDesde { get; set; }
        public DateTime? dHasta { get; set; }

        public virtual isoInstitucionEducativa iInstitucionEducativaNavigation { get; set; }
        public virtual perPersona iPersonaNavigation { get; set; }
    }
}
