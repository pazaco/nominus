﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class perNacionalidad
    {
        public int iPersona { get; set; }
        public short sNacionalidad { get; set; }

        public virtual perPersona iPersonaNavigation { get; set; }
        public virtual isoPais sNacionalidadNavigation { get; set; }
    }
}
