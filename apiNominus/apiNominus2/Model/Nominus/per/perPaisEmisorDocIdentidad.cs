﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class perPaisEmisorDocIdentidad
    {
        public int iPersona { get; set; }
        public short sPais { get; set; }

        public virtual perPersona iPersonaNavigation { get; set; }
        public virtual isoPais sPaisNavigation { get; set; }
    }
}
