﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class perUsuario
    {
        public perUsuario()
        {
            perUsuarioAccesoEmpresa = new HashSet<perUsuarioAccesoEmpresa>();
            perUsuarioAccesoTab = new HashSet<perUsuarioAccesoTab>();
        }

        public int iUsuario { get; set; }
        public string xUsuario { get; set; }
        public string xClave { get; set; }

        public virtual perPersona iUsuarioNavigation { get; set; }
        public virtual perUsuarioSuper perUsuarioSuper { get; set; }
        public virtual perUsuarioVigencia perUsuarioVigencia { get; set; }
        [JsonIgnore] public virtual ICollection<perUsuarioAccesoEmpresa> perUsuarioAccesoEmpresa { get; set; }
        [JsonIgnore] public virtual ICollection<perUsuarioAccesoTab> perUsuarioAccesoTab { get; set; }
    }
}
