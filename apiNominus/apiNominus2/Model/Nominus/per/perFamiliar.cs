﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class perFamiliar
    {
        public int iFamiliar { get; set; }
        public int iPersona { get; set; }
        public int iPersonaFam { get; set; }
        public byte bVinculoFamiliar { get; set; }

        public virtual isoVinculoFamiliar bVinculoFamiliarNavigation { get; set; }
        public virtual perPersona iPersonaFamNavigation { get; set; }
        public virtual perPersona iPersonaNavigation { get; set; }
        public virtual perFamiliarBajaDH perFamiliarBajaDH { get; set; }
        public virtual perFamiliarDocSustentaVinculo perFamiliarDocSustentaVinculo { get; set; }
    }
}
