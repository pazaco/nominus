﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class perPersona
    {
        public perPersona()
        {
            orgEmpresaRepLegal = new HashSet<orgEmpresaRepLegal>();
            perCtaBancaria = new HashSet<perCtaBancaria>();
            perDireccion = new HashSet<perDireccion>();
            perEducacion = new HashSet<perEducacion>();
            perFamiliariPersonaFamNavigation = new HashSet<perFamiliar>();
            perFamiliariPersonaNavigation = new HashSet<perFamiliar>();
            perFono = new HashSet<perFono>();
            perFoto = new HashSet<perFoto>();
            perPaisEmisorDocIdentidad = new HashSet<perPaisEmisorDocIdentidad>();
            traContrato = new HashSet<traContrato>();
        }

        public int iPersona { get; set; }
        public byte bTipoDocIdentidad { get; set; }
        public string xDocIdentidad { get; set; }
        public string xApellidoPaterno { get; set; }
        public string xApellidoMaterno { get; set; }
        public string xNombres { get; set; }
        public DateTime dNacimiento { get; set; }
        public byte? bGenero { get; set; }
        public string xConcatenado { get; set; }
        public int iUsuarioReg { get; set; }
        public DateTime dFechaReg { get; set; }

        public virtual isoTipoDocIdentidad bTipoDocIdentidadNavigation { get; set; }
        public virtual autUsuario autUsuario { get; set; }
        public virtual perEmail perEmail { get; set; }
        public virtual perNacionalidad perNacionalidad { get; set; }
        public virtual perPersonaCodigo perPersonaCodigo { get; set; }
        public virtual perUsuario perUsuario { get; set; }
        public virtual traTrabajador traTrabajador { get; set; }
        [JsonIgnore] public virtual ICollection<orgEmpresaRepLegal> orgEmpresaRepLegal { get; set; }
        [JsonIgnore] public virtual ICollection<perCtaBancaria> perCtaBancaria { get; set; }
        [JsonIgnore] public virtual ICollection<perDireccion> perDireccion { get; set; }
        [JsonIgnore] public virtual ICollection<perEducacion> perEducacion { get; set; }
        [JsonIgnore] public virtual ICollection<perFamiliar> perFamiliariPersonaFamNavigation { get; set; }
        [JsonIgnore] public virtual ICollection<perFamiliar> perFamiliariPersonaNavigation { get; set; }
        [JsonIgnore] public virtual ICollection<perFono> perFono { get; set; }
        [JsonIgnore] public virtual ICollection<perFoto> perFoto { get; set; }
        [JsonIgnore] public virtual ICollection<perPaisEmisorDocIdentidad> perPaisEmisorDocIdentidad { get; set; }
        [JsonIgnore] public virtual ICollection<traContrato> traContrato { get; set; }
    }
}
