﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class perFamiliarBajaDH
    {
        public int iFamiliar { get; set; }
        public byte bMotivoBaja { get; set; }

        public virtual isoMotivoBajaDerechohabiente bMotivoBajaNavigation { get; set; }
        public virtual perFamiliar iFamiliarNavigation { get; set; }
    }
}
