﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class perUsuarioVigencia
    {
        public int iUsuario { get; set; }
        public DateTime? dDesde { get; set; }
        public DateTime? dHasta { get; set; }

        public virtual perUsuario iUsuarioNavigation { get; set; }
    }
}
