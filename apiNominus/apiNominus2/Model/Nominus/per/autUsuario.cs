﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class autUsuario
    {
       public int iUsuario { get; set; }
        public string xEmail { get; set; }
        public byte[] hSemilla { get; set; }
        public byte[] hPassword { get; set; }
        public string xPreguntaSecreta { get; set; }
        public byte[] hRespuestaSecreta { get; set; }
        public DateTime? dCreacion { get; set; }
        public DateTime? dUltimoLogin { get; set; }

        public virtual perPersona iUsuarioNavigation { get; set; }
    }
}
