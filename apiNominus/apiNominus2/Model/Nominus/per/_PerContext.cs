﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace apiNominus2.Model
{
    public partial class NominusContext : DbContext
    {
        public virtual DbSet<autUsuario> autUsuario { get; set; }
        public virtual DbSet<perArchivo> perArchivo { get; set; }
        public virtual DbSet<perCtaBancaria> perCtaBancaria { get; set; }
        public virtual DbSet<perDireccion> perDireccion { get; set; }
        public virtual DbSet<perEducacion> perEducacion { get; set; }
        public virtual DbSet<perEmail> perEmail { get; set; }
        public virtual DbSet<perFamiliar> perFamiliar { get; set; }
        public virtual DbSet<perFamiliarBajaDH> perFamiliarBajaDH { get; set; }
        public virtual DbSet<perFamiliarDocSustentaVinculo> perFamiliarDocSustentaVinculo { get; set; }
        public virtual DbSet<perFono> perFono { get; set; }
        public virtual DbSet<perFoto> perFoto { get; set; }
        public virtual DbSet<perNacionalidad> perNacionalidad { get; set; }
        public virtual DbSet<perPaisEmisorDocIdentidad> perPaisEmisorDocIdentidad { get; set; }
        public virtual DbSet<perPersona> perPersona { get; set; }
        public virtual DbSet<perPersonaCodigo> perPersonaCodigo { get; set; }
        public virtual DbSet<perTipoFoto> perTipoFoto { get; set; }
        public virtual DbSet<perUsuario> perUsuario { get; set; }
        public virtual DbSet<perUsuarioAccesoEmpresa> perUsuarioAccesoEmpresa { get; set; }
        public virtual DbSet<perUsuarioAccesoTab> perUsuarioAccesoTab { get; set; }
        public virtual DbSet<perUsuarioSuper> perUsuarioSuper { get; set; }
        public virtual DbSet<perUsuarioVigencia> perUsuarioVigencia { get; set; }

        partial void OnModelCreatingPartialPer(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<autUsuario>(entity =>
            {
                entity.HasKey(e => e.iUsuario);

                entity.HasIndex(e => e.xEmail, "IX_xEmail_unique")
                    .IsUnique();

                entity.Property(e => e.iUsuario).ValueGeneratedNever();

                entity.Property(e => e.dCreacion)
                    .HasPrecision(0)
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.dUltimoLogin).HasPrecision(0);

                entity.Property(e => e.hSemilla).HasMaxLength(64);

                entity.Property(e => e.xEmail)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.xPreguntaSecreta)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.iUsuarioNavigation)
                    .WithOne(p => p.autUsuario)
                    .HasForeignKey<autUsuario>(d => d.iUsuario)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_autUsuario_perPersona");
            });
            modelBuilder.Entity<perArchivo>(entity =>
            {
                entity.HasKey(e => e.iArchivo);

                entity.Property(e => e.Archivo).HasColumnType("image");

                entity.Property(e => e.dDesde).HasColumnType("date");

                entity.Property(e => e.dFechaReg)
                    .HasPrecision(0)
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.dHasta).HasColumnType("date");

                entity.Property(e => e.iIdOrigenArchivo).HasComment("Por defecto es el código de Persona, pero podría ser contrato, etc.");

                entity.Property(e => e.sOrigenArchivo).HasComment("1: Contrato periodo, 2: CV, etc.");

                entity.HasOne(d => d.sOrigenArchivoNavigation)
                    .WithMany(p => p.perArchivo)
                    .HasForeignKey(d => d.sOrigenArchivo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_perArchivo_sysOrigenArchivo");
            });
            modelBuilder.Entity<perCtaBancaria>(entity =>
            {
                entity.HasKey(e => e.iCtaBancaria);

                entity.HasIndex(e => new { e.iPersona, e.bTipoCtaBancaria }, "IX_perCtaBancaria")
                    .IsUnique();

                entity.Property(e => e.bTipoCtaBancaria)
                    .HasDefaultValueSql("((1))")
                    .HasComment("1: HABERES, 2:CTA");

                entity.Property(e => e.sMoneda).HasDefaultValueSql("((604))");

                entity.Property(e => e.xCtaBancaria)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.xCtaInterBancaria)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.HasOne(d => d.bTipoCtaBancariaNavigation)
                    .WithMany(p => p.perCtaBancaria)
                    .HasForeignKey(d => d.bTipoCtaBancaria)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_perCtaBancaria_isoTipoCtaBancaria");

                entity.HasOne(d => d.iPersonaNavigation)
                    .WithMany(p => p.perCtaBancaria)
                    .HasForeignKey(d => d.iPersona)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_perCtaBancaria_perPersona");

                entity.HasOne(d => d.sEmpresaCtaBancariaNavigation)
                    .WithMany(p => p.perCtaBancaria)
                    .HasForeignKey(d => d.sEmpresaCtaBancaria)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_perCtaBancaria_orgEmpresaCtaBancaria");

                entity.HasOne(d => d.sEntidadFinancieraNavigation)
                    .WithMany(p => p.perCtaBancaria)
                    .HasForeignKey(d => d.sEntidadFinanciera)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_perCtaBancaria_isoEntidadFinanciera");

                entity.HasOne(d => d.sMonedaNavigation)
                    .WithMany(p => p.perCtaBancaria)
                    .HasForeignKey(d => d.sMoneda)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_perCtaBancaria_isoMoneda");
            });
            modelBuilder.Entity<perDireccion>(entity =>
            {
                entity.HasKey(e => e.iDireccion);

                entity.Property(e => e.bPrioridad).HasDefaultValueSql("((10))");

                entity.Property(e => e.bTipoVia).HasComment("Calle, Jirón, Avenida, etc.");

                entity.Property(e => e.bUbicacion)
                    .HasDefaultValueSql("((1))")
                    .HasComment("Casa, Oficina, Familiar, Vecino, Vacaciones, etc.");

                entity.Property(e => e.xBlock)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xDpto)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xEtapa)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xInterior)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xKilometro)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xLote)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xManzana)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xNombreVia)
                    .HasMaxLength(300)
                    .IsUnicode(false)
                    .HasComment("Aqui gusrda el concatenado, si no se pudo migrar por separado");

                entity.Property(e => e.xNombreZona)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.xNroVia)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xReferencia)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.bTipoViaNavigation)
                    .WithMany(p => p.perDireccion)
                    .HasForeignKey(d => d.bTipoVia)
                    .HasConstraintName("FK_perDireccion_isoTipoVia");

                entity.HasOne(d => d.bTipoZonaNavigation)
                    .WithMany(p => p.perDireccion)
                    .HasForeignKey(d => d.bTipoZona)
                    .HasConstraintName("FK_perDireccion_isoTipoZona");

                entity.HasOne(d => d.bUbicacionNavigation)
                    .WithMany(p => p.perDireccion)
                    .HasForeignKey(d => d.bUbicacion)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_perDireccion_isoUbicacion");

                entity.HasOne(d => d.iPersonaNavigation)
                    .WithMany(p => p.perDireccion)
                    .HasForeignKey(d => d.iPersona)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_perDireccion_perPersona");

                entity.HasOne(d => d.sDistritoNavigation)
                    .WithMany(p => p.perDireccion)
                    .HasForeignKey(d => d.sDistrito)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_perDireccion_isoDistrito");
            });
            modelBuilder.Entity<perEducacion>(entity =>
            {
                entity.HasKey(e => e.iEducacion);

                entity.Property(e => e.cPais)
                    .IsRequired()
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.dDesde).HasColumnType("date");

                entity.Property(e => e.dHasta).HasColumnType("date");

                entity.Property(e => e.xTitulo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.iInstitucionEducativaNavigation)
                    .WithMany(p => p.perEducacion)
                    .HasForeignKey(d => d.iInstitucionEducativa)
                    .HasConstraintName("FK_perEducacion_isoInstitucionEducativa");

                entity.HasOne(d => d.iPersonaNavigation)
                    .WithMany(p => p.perEducacion)
                    .HasForeignKey(d => d.iPersona)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_perEducacion_perPersona");
            });
            modelBuilder.Entity<perEmail>(entity =>
            {
                entity.HasKey(e => e.iPersona);

                entity.Property(e => e.iPersona).ValueGeneratedNever();

                entity.Property(e => e.xEmail)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasComment("Aquí va el correo corporativo, porque el emial principal se guarda en la tabla autUsuario");

                entity.HasOne(d => d.iPersonaNavigation)
                    .WithOne(p => p.perEmail)
                    .HasForeignKey<perEmail>(d => d.iPersona)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_perEmail_perPersona");
            });
            modelBuilder.Entity<perFamiliar>(entity =>
            {
                entity.HasKey(e => e.iFamiliar);

                entity.HasOne(d => d.bVinculoFamiliarNavigation)
                    .WithMany(p => p.perFamiliar)
                    .HasForeignKey(d => d.bVinculoFamiliar)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_perFamiliar_isoVinculoFamiliar");

                entity.HasOne(d => d.iPersonaNavigation)
                    .WithMany(p => p.perFamiliariPersonaNavigation)
                    .HasForeignKey(d => d.iPersona)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_perFamiliar_perPersona");

                entity.HasOne(d => d.iPersonaFamNavigation)
                    .WithMany(p => p.perFamiliariPersonaFamNavigation)
                    .HasForeignKey(d => d.iPersonaFam)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_perFamiliar_perPersonaFam");
            });
            modelBuilder.Entity<perFamiliarBajaDH>(entity =>
            {
                entity.HasKey(e => e.iFamiliar)
                    .HasName("PK_isoFamiliarBaja");

                entity.Property(e => e.iFamiliar).ValueGeneratedNever();

                entity.HasOne(d => d.bMotivoBajaNavigation)
                    .WithMany(p => p.perFamiliarBajaDH)
                    .HasForeignKey(d => d.bMotivoBaja)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_isoFamiliarBaja_isoMotivoBajaDerechohabiente");

                entity.HasOne(d => d.iFamiliarNavigation)
                    .WithOne(p => p.perFamiliarBajaDH)
                    .HasForeignKey<perFamiliarBajaDH>(d => d.iFamiliar)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_isoFamiliarBaja_perFamiliar");
            });
            modelBuilder.Entity<perFamiliarDocSustentaVinculo>(entity =>
            {
                entity.HasKey(e => e.iFamiliar);

                entity.Property(e => e.iFamiliar).ValueGeneratedNever();

                entity.HasOne(d => d.bDocSustentaVinculoNavigation)
                    .WithMany(p => p.perFamiliarDocSustentaVinculo)
                    .HasForeignKey(d => d.bDocSustentaVinculo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_perFamiliarDocSustentaVinculo_isoDocSustentaVinculoFamiliar");

                entity.HasOne(d => d.iFamiliarNavigation)
                    .WithOne(p => p.perFamiliarDocSustentaVinculo)
                    .HasForeignKey<perFamiliarDocSustentaVinculo>(d => d.iFamiliar)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_perFamiliarDocSustentaVinculo_perFamiliar");
            });
            modelBuilder.Entity<perFono>(entity =>
            {
                entity.HasKey(e => e.iFono)
                    .HasName("PK_PersonaFono_1");

                entity.Property(e => e.bPrioridad)
                    .HasDefaultValueSql("((1))")
                    .HasComment("Por ejemplo Prioridad 1 es su casa, los demás no son tan importantes");

                entity.Property(e => e.bTipo)
                    .HasDefaultValueSql("((1))")
                    .HasComment("1: Personal, 2: Empresarial, 3: Otro");

                entity.Property(e => e.xDescripcion)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.xNumeroFono)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.HasOne(d => d.iPersonaNavigation)
                    .WithMany(p => p.perFono)
                    .HasForeignKey(d => d.iPersona)
                    .HasConstraintName("FK_PersonaFono_Persona");
            });
            modelBuilder.Entity<perFoto>(entity =>
            {
                entity.HasKey(e => e.iFoto)
                    .HasName("PK_PersonaFoto");

                entity.Property(e => e.Foto).HasColumnType("image");

                entity.Property(e => e.jFoto).IsUnicode(false);

                entity.HasOne(d => d.bTipoFotoNavigation)
                    .WithMany(p => p.perFoto)
                    .HasForeignKey(d => d.bTipoFoto)
                    .HasConstraintName("FK_perFoto_perTipoFoto");

                entity.HasOne(d => d.iPersonaNavigation)
                    .WithMany(p => p.perFoto)
                    .HasForeignKey(d => d.iPersona)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_perFoto_perPersona");
            });
            modelBuilder.Entity<perNacionalidad>(entity =>
            {
                entity.HasKey(e => e.iPersona)
                    .HasName("PK_perPersonaNacionalidad");

                entity.Property(e => e.iPersona).ValueGeneratedNever();

                entity.HasOne(d => d.iPersonaNavigation)
                    .WithOne(p => p.perNacionalidad)
                    .HasForeignKey<perNacionalidad>(d => d.iPersona)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_perPersonaNacionalidad_perPersona");

                entity.HasOne(d => d.sNacionalidadNavigation)
                    .WithMany(p => p.perNacionalidad)
                    .HasForeignKey(d => d.sNacionalidad)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_perNacionalidad_isoPais");
            });
            modelBuilder.Entity<perPaisEmisorDocIdentidad>(entity =>
            {
                entity.HasKey(e => new { e.iPersona, e.sPais });

                entity.HasOne(d => d.iPersonaNavigation)
                    .WithMany(p => p.perPaisEmisorDocIdentidad)
                    .HasForeignKey(d => d.iPersona)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_perPaisEmisorDocIdentidad_perPersona");

                entity.HasOne(d => d.sPaisNavigation)
                    .WithMany(p => p.perPaisEmisorDocIdentidad)
                    .HasForeignKey(d => d.sPais)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_perPaisEmisorDocIdentidad_isoPais");
            });
            modelBuilder.Entity<perPersona>(entity =>
            {
                entity.HasKey(e => e.iPersona)
                    .HasName("PK_Persona");

                entity.Property(e => e.bGenero).HasComment("Identifica el genero (Masculino/Femenino)");

                entity.Property(e => e.dFechaReg)
                    .HasPrecision(0)
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.dNacimiento)
                    .HasColumnType("date")
                    .HasComment("Fecha de nacimiento");

                entity.Property(e => e.xApellidoMaterno)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.xApellidoPaterno)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.xConcatenado)
                    .HasMaxLength(229)
                    .IsUnicode(false)
                    .HasComputedColumnSql("(((((((([xDocIdentidad]+' ')+[xApellidoPaterno])+' ')+[xApellidoMaterno])+' ')+[xNombres])+' ')+CONVERT([char](10),[dNacimiento],(103)))", true);

                entity.Property(e => e.xDocIdentidad)
                    .IsRequired()
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.xNombres)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.bTipoDocIdentidadNavigation)
                    .WithMany(p => p.perPersona)
                    .HasForeignKey(d => d.bTipoDocIdentidad)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_perPersona_isoTipoDocIdentidad");
            });
            modelBuilder.Entity<perPersonaCodigo>(entity =>
            {
                entity.HasKey(e => e.iPersona);

                entity.HasIndex(e => e.iPersona, "IX_perPersonaCodigo")
                    .IsUnique();

                entity.Property(e => e.iPersona).ValueGeneratedNever();

                entity.Property(e => e.xCodigo)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.iPersonaNavigation)
                    .WithOne(p => p.perPersonaCodigo)
                    .HasForeignKey<perPersonaCodigo>(d => d.iPersona)
                    .HasConstraintName("FK_perPersonaCodigo_perPersona");
            });
            modelBuilder.Entity<perTipoFoto>(entity =>
            {
                entity.HasKey(e => e.bTipoFoto);

                entity.Property(e => e.bTipoFoto).ValueGeneratedOnAdd();

                entity.Property(e => e.xTipoFoto)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<perUsuario>(entity =>
            {
                entity.HasKey(e => e.iUsuario);

                entity.Property(e => e.iUsuario).ValueGeneratedNever();

                entity.Property(e => e.xClave).HasMaxLength(50);

                entity.Property(e => e.xUsuario)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.iUsuarioNavigation)
                    .WithOne(p => p.perUsuario)
                    .HasForeignKey<perUsuario>(d => d.iUsuario)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_perUsuario_perPersona");
            });
            modelBuilder.Entity<perUsuarioAccesoEmpresa>(entity =>
            {
                entity.HasKey(e => e.iUsuarioEmpresa);

                entity.HasOne(d => d.iUsuarioNavigation)
                    .WithMany(p => p.perUsuarioAccesoEmpresa)
                    .HasForeignKey(d => d.iUsuario)
                    .HasConstraintName("FK_perUsuarioAccesoEmpresa_perUsuario");

                entity.HasOne(d => d.sEmpresaNavigation)
                    .WithMany(p => p.perUsuarioAccesoEmpresa)
                    .HasForeignKey(d => d.sEmpresa)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_perUsuarioAccesoEmpresa_orgEmpresa");
            });
            modelBuilder.Entity<perUsuarioAccesoTab>(entity =>
            {
                entity.HasKey(e => e.iUsuarioAcceso);

                entity.HasIndex(e => new { e.iUsuario, e.iTab }, "IX_perUsuarioAccesoTab")
                    .IsUnique();

                entity.HasOne(d => d.iTabNavigation)
                    .WithMany(p => p.perUsuarioAccesoTab)
                    .HasForeignKey(d => d.iTab)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_perUsuarioAccesoTab_sysAccesoTab");

                entity.HasOne(d => d.iUsuarioNavigation)
                    .WithMany(p => p.perUsuarioAccesoTab)
                    .HasForeignKey(d => d.iUsuario)
                    .HasConstraintName("FK_perUsuarioAccesoTab_perUsuario");
            });
            modelBuilder.Entity<perUsuarioSuper>(entity =>
            {
                entity.HasKey(e => e.iUsuario);

                entity.Property(e => e.iUsuario).ValueGeneratedNever();

                entity.HasOne(d => d.iUsuarioNavigation)
                    .WithOne(p => p.perUsuarioSuper)
                    .HasForeignKey<perUsuarioSuper>(d => d.iUsuario)
                    .HasConstraintName("FK_perUsuarioSuper_perUsuario");
            });
            modelBuilder.Entity<perUsuarioVigencia>(entity =>
            {
                entity.HasKey(e => e.iUsuario);

                entity.Property(e => e.iUsuario).ValueGeneratedNever();

                entity.Property(e => e.dDesde).HasColumnType("date");

                entity.Property(e => e.dHasta).HasColumnType("date");

                entity.HasOne(d => d.iUsuarioNavigation)
                    .WithOne(p => p.perUsuarioVigencia)
                    .HasForeignKey<perUsuarioVigencia>(d => d.iUsuario)
                    .HasConstraintName("FK_perUsuarioVigencia_perUsuario");
            });

        }
    }
}
