﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class perCtaBancaria
    {
        public int iCtaBancaria { get; set; }
        public int iPersona { get; set; }
        public short sEmpresaCtaBancaria { get; set; }
        public byte bTipoCtaBancaria { get; set; }
        public short sEntidadFinanciera { get; set; }
        public short sMoneda { get; set; }
        public string xCtaBancaria { get; set; }
        public string xCtaInterBancaria { get; set; }

        public virtual isoTipoCtaBancaria bTipoCtaBancariaNavigation { get; set; }
        public virtual perPersona iPersonaNavigation { get; set; }
        public virtual orgEmpresaCtaBancaria sEmpresaCtaBancariaNavigation { get; set; }
        public virtual isoEntidadFinanciera sEntidadFinancieraNavigation { get; set; }
        public virtual isoMoneda sMonedaNavigation { get; set; }
    }
}
