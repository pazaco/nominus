﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class perEmail
    {
        public int iPersona { get; set; }
        public string xEmail { get; set; }

        public virtual perPersona iPersonaNavigation { get; set; }
    }
}
