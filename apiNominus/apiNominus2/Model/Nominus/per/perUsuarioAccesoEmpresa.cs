﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class perUsuarioAccesoEmpresa
    {
        public int iUsuarioEmpresa { get; set; }
        public int iUsuario { get; set; }
        public short sEmpresa { get; set; }

        public virtual perUsuario iUsuarioNavigation { get; set; }
        public virtual orgEmpresa sEmpresaNavigation { get; set; }
    }
}
