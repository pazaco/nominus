﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class perTipoFoto
    {
        public perTipoFoto()
        {
            perFoto = new HashSet<perFoto>();
        }

        public byte bTipoFoto { get; set; }
        public string xTipoFoto { get; set; }

        [JsonIgnore] public virtual ICollection<perFoto> perFoto { get; set; }
    }
}
