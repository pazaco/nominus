﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class pshTipoNotificacion
    {
        public pshTipoNotificacion()
        {
            pshNotificacion = new HashSet<pshNotificacion>();
        }

        public byte bTipoNotificacion { get; set; }
        public string xTipoNotificacion { get; set; }
        public string cPrefijo { get; set; }
        public bool? lPush { get; set; }
        public string cBuffer { get; set; }
        public bool? lStorage { get; set; }

        [JsonIgnore] public virtual ICollection<pshNotificacion> pshNotificacion { get; set; }
    }
}
