﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace apiNominus2.Model
{
    public partial class NominusContext: DbContext
    {

        public virtual DbSet<appTipoVacaciones> appTipoVacaciones { get; set; }
        public virtual DbSet<appVacaciones> appVacaciones { get; set; }
        public virtual DbSet<belPDFs> belPDFs { get; set; }
        public virtual DbSet<pshData> pshData { get; set; }
        public virtual DbSet<pshNotificacion> pshNotificacion { get; set; }
        public virtual DbSet<pshTipoNotificacion> pshTipoNotificacion { get; set; }
        public virtual DbSet<webFirma> webFirma { get; set; }
        public virtual DbSet<webMovimientoVacaciones> webMovimientoVacaciones { get; set; }
        public virtual DbSet<webNotificacion> webNotificacion { get; set; }
        public virtual DbSet<webPrestamo> webPrestamo { get; set; }
        public virtual DbSet<webPrestamoPagos> webPrestamoPagos { get; set; }
        public virtual DbSet<webRolFirma> webRolFirma { get; set; }
        public virtual DbSet<webTipoNotificacion> webTipoNotificacion { get; set; }
        public virtual DbSet<webUrl> webUrl { get; set; }

        partial void OnModelCreatingPartialApp(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<appTipoVacaciones>(entity =>
            {
                entity.HasKey(e => e.cTipoNovedad)
                    .HasName("PK__appTipoV__E17E15E1AB6999D1");

                entity.Property(e => e.cTipoNovedad)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.xTipoNovedad)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<appVacaciones>(entity =>
            {
                entity.HasKey(e => e.iNovedadVacaciones)
                    .HasName("PK__appVacac__465A4C2FB050FB82");

                entity.Property(e => e.cTipoNovedad)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.dDesde).HasColumnType("date");

                entity.Property(e => e.dHasta).HasColumnType("date");

                entity.Property(e => e.xDescripcion).IsUnicode(false);

                entity.HasOne(d => d.cTipoNovedadNavigation)
                    .WithMany(p => p.appVacaciones)
                    .HasForeignKey(d => d.cTipoNovedad)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_appVacaciones_appTipoVacaciones");
            });
            modelBuilder.Entity<belPDFs>(entity =>
            {
                entity.HasKey(e => e.cPDF);

                entity.Property(e => e.cPDF)
                    .HasMaxLength(16)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.dGeneracion).HasPrecision(0);
            });
            modelBuilder.Entity<pshData>(entity =>
            {
                entity.HasKey(e => new { e.iNotificacion, e.bConsecutivo });

                entity.Property(e => e.jData).IsUnicode(false);

                entity.HasOne(d => d.pshNotificacion)
                    .WithOne(p => p.pshData)
                    .HasForeignKey<pshData>(d => new { d.iNotificacion, d.bConsecutivo })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_pshData_pshNotificacion");
            });
            modelBuilder.Entity<pshNotificacion>(entity =>
            {
                entity.HasKey(e => new { e.iNotificacion, e.bConsecutivo });

                entity.Property(e => e.dLeido).HasPrecision(0);

                entity.Property(e => e.dNotificar).HasPrecision(0);

                entity.Property(e => e.xNotificacion).IsUnicode(false);

                entity.Property(e => e.xTitulo)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.HasOne(d => d.bTipoNotificacionNavigation)
                    .WithMany(p => p.pshNotificacion)
                    .HasForeignKey(d => d.bTipoNotificacion)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_pshNotificacion_pshTipoNotificacion");
            });
            modelBuilder.Entity<pshTipoNotificacion>(entity =>
            {
                entity.HasKey(e => e.bTipoNotificacion);

                entity.Property(e => e.cBuffer)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.cPrefijo)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.xTipoNotificacion)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<webFirma>(entity =>
            {
                entity.HasKey(e => e.iFirma);

                entity.Property(e => e.dFirmado).HasPrecision(0);

                entity.Property(e => e.dLeido).HasPrecision(0);

                entity.HasOne(d => d.bRolNavigation)
                    .WithMany(p => p.webFirma)
                    .HasForeignKey(d => d.bRol)
                    .HasConstraintName("FK_webFirma_webRolFirma");

                entity.HasOne(d => d.iNotificacionNavigation)
                    .WithMany(p => p.webFirma)
                    .HasForeignKey(d => d.iNotificacion)
                    .HasConstraintName("FK_webFirma_webNotificacion");
            });

            modelBuilder.Entity<webMovimientoVacaciones>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("webMovimientoVacaciones");

                entity.Property(e => e.cTipoNovedad)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.dDesde).HasColumnType("date");

                entity.Property(e => e.dHasta).HasColumnType("date");

                entity.Property(e => e.xDescripcion).IsUnicode(false);
            });

            modelBuilder.Entity<webNotificacion>(entity =>
            {
                entity.HasKey(e => e.iNotificacion);

                entity.Property(e => e.dEnvio).HasPrecision(0);

                entity.Property(e => e.xAsunto).IsUnicode(false);

                entity.HasOne(d => d.bTipoNotificacionNavigation)
                    .WithMany(p => p.webNotificacion)
                    .HasForeignKey(d => d.bTipoNotificacion)
                    .HasConstraintName("FK_webNotificacion_webTipoNotificacion");
            });

            modelBuilder.Entity<webPrestamo>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("webPrestamo");

                entity.Property(e => e.dPrestamo).HasColumnType("date");

                entity.Property(e => e.mValor).HasColumnType("money");
            });

            modelBuilder.Entity<webPrestamoPagos>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("webPrestamoPagos");

                entity.Property(e => e.dFecha).HasColumnType("date");
            });

            modelBuilder.Entity<webRolFirma>(entity =>
            {
                entity.HasKey(e => e.bRol);

                entity.Property(e => e.xFaIcono)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.xRol)
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<webTipoNotificacion>(entity =>
            {
                entity.HasKey(e => e.bTipoNotificacion);

                entity.Property(e => e.xFaIcono)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.xTipoNotificacion)
                    .HasMaxLength(40)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<webUrl>(entity =>
            {
                entity.HasKey(e => e.iUrl);

                entity.Property(e => e.iUrl).ValueGeneratedNever();

                entity.Property(e => e.xUrl).IsUnicode(false);
            });

        }

    }
}
