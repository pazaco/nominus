﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class webFirma
    {
        public int iFirma { get; set; }
        public int? iNotificacion { get; set; }
        public byte? bRol { get; set; }
        public int? iPersona { get; set; }
        public DateTime? dLeido { get; set; }
        public DateTime? dFirmado { get; set; }

        public virtual webRolFirma bRolNavigation { get; set; }
        public virtual webNotificacion iNotificacionNavigation { get; set; }
    }
}
