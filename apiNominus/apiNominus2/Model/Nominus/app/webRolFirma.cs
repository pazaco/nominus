﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class webRolFirma
    {
        public webRolFirma()
        {
            webFirma = new HashSet<webFirma>();
        }

        public byte bRol { get; set; }
        public string xRol { get; set; }
        public string xFaIcono { get; set; }

        [JsonIgnore] public virtual ICollection<webFirma> webFirma { get; set; }
    }
}
