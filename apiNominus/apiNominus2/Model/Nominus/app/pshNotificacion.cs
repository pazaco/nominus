﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class pshNotificacion
    {
        public int iNotificacion { get; set; }
        public byte bConsecutivo { get; set; }
        public byte bTipoNotificacion { get; set; }
        public string xTitulo { get; set; }
        public string xNotificacion { get; set; }
        public int iPersonaNominus { get; set; }
        public DateTime? dNotificar { get; set; }
        public DateTime? dLeido { get; set; }
        public bool? lNotificado { get; set; }
        public bool? lFirmaDigital { get; set; }

        public virtual pshTipoNotificacion bTipoNotificacionNavigation { get; set; }
        public virtual pshData pshData { get; set; }
    }
}
