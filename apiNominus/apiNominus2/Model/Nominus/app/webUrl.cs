﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class webUrl
    {
        public int iUrl { get; set; }
        public string xUrl { get; set; }
    }
}
