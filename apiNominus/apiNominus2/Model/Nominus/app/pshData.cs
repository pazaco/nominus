﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class pshData
    {
        public int iNotificacion { get; set; }
        public byte bConsecutivo { get; set; }
        public string jData { get; set; }

        public virtual pshNotificacion pshNotificacion { get; set; }
    }
}
