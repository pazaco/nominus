﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class appVacaciones
    {
        public int iNovedadVacaciones { get; set; }
        public int? iContrato { get; set; }
        public short sCiclo { get; set; }
        public DateTime dDesde { get; set; }
        public DateTime? dHasta { get; set; }
        public int? iDias { get; set; }
        public string cTipoNovedad { get; set; }
        public int? iFirmaJefe { get; set; }
        public int? iFirmaRRHH { get; set; }
        public string xDescripcion { get; set; }

        public virtual appTipoVacaciones cTipoNovedadNavigation { get; set; }
    }
}
