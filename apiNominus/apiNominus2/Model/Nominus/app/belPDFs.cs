﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class belPDFs
    {
        public string cPDF { get; set; }
        public int? iCalculoPlanilla { get; set; }
        public DateTime? dGeneracion { get; set; }
    }
}
