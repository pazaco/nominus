﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class appTipoVacaciones
    {
        public appTipoVacaciones()
        {
            appVacaciones = new HashSet<appVacaciones>();
        }

        public string cTipoNovedad { get; set; }
        public string xTipoNovedad { get; set; }

        [JsonIgnore] public virtual ICollection<appVacaciones> appVacaciones { get; set; }
    }
}
