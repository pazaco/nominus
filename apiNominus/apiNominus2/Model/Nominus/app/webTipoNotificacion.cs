﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class webTipoNotificacion
    {
        public webTipoNotificacion()
        {
            webNotificacion = new HashSet<webNotificacion>();
        }

        public byte bTipoNotificacion { get; set; }
        public string xTipoNotificacion { get; set; }
        public string xFaIcono { get; set; }

        [JsonIgnore] public virtual ICollection<webNotificacion> webNotificacion { get; set; }
    }
}
