﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class webNotificacion
    {
        public webNotificacion()
        {
            webFirma = new HashSet<webFirma>();
        }

        public int iNotificacion { get; set; }
        public byte? bTipoNotificacion { get; set; }
        public int? iReferencia { get; set; }
        public int? iContrato { get; set; }
        public DateTime? dEnvio { get; set; }
        public string xAsunto { get; set; }
        public int? iUrl { get; set; }

        public virtual webTipoNotificacion bTipoNotificacionNavigation { get; set; }
        [JsonIgnore] public virtual ICollection<webFirma> webFirma { get; set; }
    }
}
