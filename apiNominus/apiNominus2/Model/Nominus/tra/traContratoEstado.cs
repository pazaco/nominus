﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class traContratoEstado
    {
        public int iContrato { get; set; }
        public int iTrabajador { get; set; }
        public short sEmpresa { get; set; }
        public short sSede { get; set; }
        public short sCargo { get; set; }
        public int iPeriodoVigente { get; set; }
        public DateTime dVigenteDesde { get; set; }
        public DateTime? dHasta { get; set; }
        public DateTime? dCese { get; set; }
        public DateTime dIngreso { get; set; }
        public int? iJefe { get; set; }
    }
}
