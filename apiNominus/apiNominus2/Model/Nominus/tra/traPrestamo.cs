﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class traPrestamo
    {
        public traPrestamo()
        {
            traPrestamoCuota = new HashSet<traPrestamoCuota>();
        }

        public int iPrestamo { get; set; }
        public int iContrato { get; set; }
        public short sConcepto { get; set; }
        public short sMoneda { get; set; }
        public decimal mValor { get; set; }

        public virtual traContrato iContratoNavigation { get; set; }
        public virtual remConcepto sConceptoNavigation { get; set; }
        public virtual isoMoneda sMonedaNavigation { get; set; }
        public virtual traPrestamoCalculo traPrestamoCalculo { get; set; }
        public virtual traPrestamoDesembolso traPrestamoDesembolso { get; set; }
        public virtual traPrestamoMotivo traPrestamoMotivo { get; set; }
        [JsonIgnore] public virtual ICollection<traPrestamoCuota> traPrestamoCuota { get; set; }
    }
}
