﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class traContratoListaNegra
    {
        public int iContrato { get; set; }
        public string xMotivo { get; set; }

        public virtual traContrato iContratoNavigation { get; set; }
    }
}
