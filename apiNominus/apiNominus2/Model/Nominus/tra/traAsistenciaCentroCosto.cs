﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class traAsistenciaCentroCosto
    {
        public int iAsistencia { get; set; }
        public short sCentroCosto { get; set; }

        public virtual traAsistencia iAsistenciaNavigation { get; set; }
        public virtual orgCentroCosto sCentroCostoNavigation { get; set; }
    }
}
