﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class traAsistenciaMotivo
    {
        public int iAsistencia { get; set; }
        public string xMotivo { get; set; }

        public virtual traAsistencia iAsistenciaNavigation { get; set; }
    }
}
