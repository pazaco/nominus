﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class traAFP
    {
        public int iTrabajador { get; set; }
        public string xCUSPP { get; set; }
        public DateTime? dDesde { get; set; }
        public bool? bMixta { get; set; }

        public virtual traTrabajador iTrabajadorNavigation { get; set; }
    }
}
