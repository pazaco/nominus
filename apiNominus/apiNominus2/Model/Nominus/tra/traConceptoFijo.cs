﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class traConceptoFijo
    {
        public int iConceptoFijo { get; set; }
        public byte bCalculo { get; set; }
        public int iContrato { get; set; }
        public short sConcepto { get; set; }
        public DateTime? dDesde { get; set; }
        public DateTime? dHasta { get; set; }
        public decimal mValor { get; set; }

        public virtual isoCalculo bCalculoNavigation { get; set; }
        public virtual traContrato iContratoNavigation { get; set; }
        public virtual remConcepto sConceptoNavigation { get; set; }
        public virtual traEPS traEPS { get; set; }
        public virtual traVidaLey traVidaLey { get; set; }
    }
}
