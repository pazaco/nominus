﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class traPrestamoCuota
    {
        public traPrestamoCuota()
        {
            traPrestamoCuotaCalculo = new HashSet<traPrestamoCuotaCalculo>();
        }

        public int iPrestamoCuota { get; set; }
        public int iPrestamo { get; set; }
        public byte bCalculo { get; set; }
        public DateTime dFecha { get; set; }
        public decimal mValor { get; set; }
        public bool bAnulado { get; set; }
        public bool bCalculoAdelanto { get; set; }

        public virtual isoCalculo bCalculoNavigation { get; set; }
        public virtual traPrestamo iPrestamoNavigation { get; set; }
        [JsonIgnore] public virtual ICollection<traPrestamoCuotaCalculo> traPrestamoCuotaCalculo { get; set; }
    }
}
