﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class traAsistenciaVacaciones
    {
        public int iAsistencia { get; set; }
        public byte bPagada { get; set; }

        public virtual traAsistencia iAsistenciaNavigation { get; set; }
    }
}
