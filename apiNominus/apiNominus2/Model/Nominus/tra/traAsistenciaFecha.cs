﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class traAsistenciaFecha
    {
        public traAsistenciaFecha()
        {
            traAsistenciaFechaCalculo = new HashSet<traAsistenciaFechaCalculo>();
        }

        public int iAsistencia { get; set; }
        public DateTime dDesde { get; set; }
        public DateTime dHasta { get; set; }
        public int? iDias { get; set; }

        public virtual traAsistencia iAsistenciaNavigation { get; set; }
        [JsonIgnore] public virtual ICollection<traAsistenciaFechaCalculo> traAsistenciaFechaCalculo { get; set; }
    }
}
