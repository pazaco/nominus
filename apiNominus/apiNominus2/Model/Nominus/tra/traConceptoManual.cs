﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class traConceptoManual
    {
        public int iConceptoManual { get; set; }
        public int iContrato { get; set; }
        public int iPeriodo { get; set; }
        public short sConcepto { get; set; }
        public decimal mValor { get; set; }

        public virtual traContrato iContratoNavigation { get; set; }
        public virtual remConcepto sConceptoNavigation { get; set; }
        public virtual traConceptoManualCentroCosto traConceptoManualCentroCosto { get; set; }
    }
}
