﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class traSCTR
    {
        public traSCTR()
        {
            traSCTRfecha = new HashSet<traSCTRfecha>();
        }

        public int iContrato { get; set; }
        public byte bTipoSctrPension { get; set; }
        public byte bRiesgoSctrPension { get; set; }
        public byte bTipoSctrSalud { get; set; }
        public byte bRiesgoSctrSalud { get; set; }

        public virtual traContrato iContratoNavigation { get; set; }
        [JsonIgnore] public virtual ICollection<traSCTRfecha> traSCTRfecha { get; set; }
    }
}
