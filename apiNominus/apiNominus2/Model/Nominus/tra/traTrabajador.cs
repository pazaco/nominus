﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class traTrabajador
    {
        public traTrabajador()
        {
            traJefeDirecto = new HashSet<traJefeDirecto>();
        }

        public int iTrabajador { get; set; }
        public byte bRegimenPensionario { get; set; }
        public bool bDiscapacidad { get; set; }
        public byte bConvenioEvitarDobleTributacion { get; set; }
        public byte bEstadoCivil { get; set; }

        public virtual isoRegimenPensionario bRegimenPensionarioNavigation { get; set; }
        public virtual perPersona iTrabajadorNavigation { get; set; }
        public virtual traAFP traAFP { get; set; }
        public virtual traRUC traRUC { get; set; }
        [JsonIgnore] public virtual ICollection<traJefeDirecto> traJefeDirecto { get; set; }
    }
}
