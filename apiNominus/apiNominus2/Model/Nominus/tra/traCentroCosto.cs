﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class traCentroCosto
    {
        public int iContratoCentroCosto { get; set; }
        public int iContrato { get; set; }
        public short sCentroCosto { get; set; }
        public decimal rPorcentaje { get; set; }
        public bool? bPrincipal { get; set; }

        public virtual traContrato iContratoNavigation { get; set; }
        public virtual orgCentroCosto sCentroCostoNavigation { get; set; }
    }
}
