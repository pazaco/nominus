﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class traPrestamoCalculo
    {
        public int iPrestamo { get; set; }
        public int? iCalculoPlanilla { get; set; }

        public virtual remCalculoPlanilla iCalculoPlanillaNavigation { get; set; }
        public virtual traPrestamo iPrestamoNavigation { get; set; }
    }
}
