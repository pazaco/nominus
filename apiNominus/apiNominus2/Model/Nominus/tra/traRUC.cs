﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class traRUC
    {
        public int iTrabajador { get; set; }
        public string cRUC { get; set; }

        public virtual traTrabajador iTrabajadorNavigation { get; set; }
    }
}
