﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class traAsistenciaMarcacion
    {
        public int iAsistencia { get; set; }
        public long iMarcacion { get; set; }

        public virtual traAsistencia iAsistenciaNavigation { get; set; }
    }
}
