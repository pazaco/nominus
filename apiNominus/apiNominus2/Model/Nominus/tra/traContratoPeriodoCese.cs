﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class traContratoPeriodoCese
    {
        public int iContratoPeriodo { get; set; }
        public DateTime dCese { get; set; }
        public byte bMotivo { get; set; }
        public string xObservacion { get; set; }
        public DateTime? dLiquidacion { get; set; }
        public string xObservacionLiquidacion { get; set; }

        public virtual isoMotivoCese bMotivoNavigation { get; set; }
        public virtual traContratoPeriodo iContratoPeriodoNavigation { get; set; }
    }
}
