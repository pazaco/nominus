﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class traVidaLey
    {
        public int iConceptoFijo { get; set; }
        public short? sAseguradora { get; set; }
        public string xCodigo { get; set; }
        public decimal? rTasa { get; set; }

        public virtual traConceptoFijo iConceptoFijoNavigation { get; set; }
        public virtual terEmpresaAseguradoraSalud sAseguradoraNavigation { get; set; }
    }
}
