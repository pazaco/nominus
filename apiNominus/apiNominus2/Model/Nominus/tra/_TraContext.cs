﻿using Microsoft.EntityFrameworkCore;

#nullable disable

namespace apiNominus2.Model
{
    public partial class NominusContext : DbContext
    {

        public virtual DbSet<traAFP> traAFP { get; set; }
        public virtual DbSet<traArea> traArea { get; set; }
        public virtual DbSet<traAsistencia> traAsistencia { get; set; }
        public virtual DbSet<traAsistenciaCentroCosto> traAsistenciaCentroCosto { get; set; }
        public virtual DbSet<traAsistenciaEstructuraSalarial> traAsistenciaEstructuraSalarial { get; set; }
        public virtual DbSet<traAsistenciaFecha> traAsistenciaFecha { get; set; }
        public virtual DbSet<traAsistenciaFechaCalculo> traAsistenciaFechaCalculo { get; set; }
        public virtual DbSet<traAsistenciaHora> traAsistenciaHora { get; set; }
        public virtual DbSet<traAsistenciaHoraCalculo> traAsistenciaHoraCalculo { get; set; }
        public virtual DbSet<traAsistenciaMarcacion> traAsistenciaMarcacion { get; set; }
        public virtual DbSet<traAsistenciaMotivo> traAsistenciaMotivo { get; set; }
        public virtual DbSet<traAsistenciaRelacion> traAsistenciaRelacion { get; set; }
        public virtual DbSet<traAsistenciaVacaciones> traAsistenciaVacaciones { get; set; }
        public virtual DbSet<traCentroCosto> traCentroCosto { get; set; }
        public virtual DbSet<traConceptoFijo> traConceptoFijo { get; set; }
        public virtual DbSet<traConceptoManual> traConceptoManual { get; set; }
        public virtual DbSet<traConceptoManualCentroCosto> traConceptoManualCentroCosto { get; set; }
        public virtual DbSet<traContrato> traContrato { get; set; }
        public virtual DbSet<traContratoFormato> traContratoFormato { get; set; }
        public virtual DbSet<traContratoLaboral> traContratoLaboral { get; set; }
        public virtual DbSet<traContratoListaNegra> traContratoListaNegra { get; set; }
        public virtual DbSet<traContratoPeriodo> traContratoPeriodo { get; set; }
        public virtual DbSet<traContratoPeriodoCese> traContratoPeriodoCese { get; set; }
        public virtual DbSet<traContratoPeriodoMintra> traContratoPeriodoMintra { get; set; }
        public virtual DbSet<traContratoPeriodoPrueba> traContratoPeriodoPrueba { get; set; }
        public virtual DbSet<traEPS> traEPS { get; set; }
        public virtual DbSet<traEPSdet> traEPSdet { get; set; }
        public virtual DbSet<traJefeDirecto> traJefeDirecto { get; set; }
        public virtual DbSet<traPrestamo> traPrestamo { get; set; }
        public virtual DbSet<traPrestamoCalculo> traPrestamoCalculo { get; set; }
        public virtual DbSet<traPrestamoCuota> traPrestamoCuota { get; set; }
        public virtual DbSet<traPrestamoCuotaCalculo> traPrestamoCuotaCalculo { get; set; }
        public virtual DbSet<traPrestamoDesembolso> traPrestamoDesembolso { get; set; }
        public virtual DbSet<traPrestamoMotivo> traPrestamoMotivo { get; set; }
        public virtual DbSet<traRUC> traRUC { get; set; }
        public virtual DbSet<traRxH> traRxH { get; set; }
        public virtual DbSet<traSCTR> traSCTR { get; set; }
        public virtual DbSet<traSCTRfecha> traSCTRfecha { get; set; }
        public virtual DbSet<traStatusContrato> traStatusContrato { get; set; }
        public virtual DbSet<traTrabajador> traTrabajador { get; set; }
        public virtual DbSet<traVidaLey> traVidaLey { get; set; }

        partial void OnModelCreatingPartialTra(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<traAFP>(entity =>
            {
                entity.HasKey(e => e.iTrabajador)
                    .HasName("PK_traAFP_1");

                entity.Property(e => e.iTrabajador).ValueGeneratedNever();

                entity.Property(e => e.bMixta)
                    .IsRequired()
                    .HasDefaultValueSql("((1))")
                    .HasComment("Tipo de comisión: 0: comisión FLUJO, 1: comisión MIXTA");

                entity.Property(e => e.dDesde).HasColumnType("date");

                entity.Property(e => e.xCUSPP)
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.HasOne(d => d.iTrabajadorNavigation)
                    .WithOne(p => p.traAFP)
                    .HasForeignKey<traAFP>(d => d.iTrabajador)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_traAFP_traTrabajador");
            });
            modelBuilder.Entity<traArea>(entity =>
            {
                entity.HasKey(e => new { e.iContrato, e.sArea });

                entity.Property(e => e.bPrincipal)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.rPorcentaje)
                    .HasColumnType("decimal(5, 2)")
                    .HasDefaultValueSql("((100))");

                entity.HasOne(d => d.iContratoNavigation)
                    .WithMany(p => p.traArea)
                    .HasForeignKey(d => d.iContrato)
                    .HasConstraintName("FK_traArea_traContrato");

                entity.HasOne(d => d.sAreaNavigation)
                    .WithMany(p => p.traArea)
                    .HasForeignKey(d => d.sArea)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_traArea_orgArea");
            });
            modelBuilder.Entity<traAsistencia>(entity =>
            {
                entity.HasKey(e => e.iAsistencia)
                    .HasName("PK_traActividadAsistencia");

                entity.HasOne(d => d.iContratoNavigation)
                    .WithMany(p => p.traAsistencia)
                    .HasForeignKey(d => d.iContrato)
                    .HasConstraintName("FK_traAsistencia_traContrato");

                entity.HasOne(d => d.sConceptoNavigation)
                    .WithMany(p => p.traAsistencia)
                    .HasForeignKey(d => d.sConcepto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_traAsistencia_remConcepto");
            });
            modelBuilder.Entity<traAsistenciaCentroCosto>(entity =>
            {
                entity.HasKey(e => e.iAsistencia);

                entity.Property(e => e.iAsistencia).ValueGeneratedNever();

                entity.HasOne(d => d.iAsistenciaNavigation)
                    .WithOne(p => p.traAsistenciaCentroCosto)
                    .HasForeignKey<traAsistenciaCentroCosto>(d => d.iAsistencia)
                    .HasConstraintName("FK_traAsistenciaCentroCosto_traAsistencia");

                entity.HasOne(d => d.sCentroCostoNavigation)
                    .WithMany(p => p.traAsistenciaCentroCosto)
                    .HasForeignKey(d => d.sCentroCosto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_traAsistenciaCentroCosto_orgCentroCosto");
            });
            modelBuilder.Entity<traAsistenciaEstructuraSalarial>(entity =>
            {
                entity.HasKey(e => e.iAsistencia);

                entity.Property(e => e.iAsistencia).ValueGeneratedNever();

                entity.HasOne(d => d.iAsistenciaNavigation)
                    .WithOne(p => p.traAsistenciaEstructuraSalarial)
                    .HasForeignKey<traAsistenciaEstructuraSalarial>(d => d.iAsistencia)
                    .HasConstraintName("FK_traAsistenciaEstructuraSalarial_traAsistencia");

                entity.HasOne(d => d.sEstructuraSalarialNavigation)
                    .WithMany(p => p.traAsistenciaEstructuraSalarial)
                    .HasForeignKey(d => d.sEstructuraSalarial)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_traAsistenciaEstructuraSalarial_orgEstructuraSalarial");
            });
            modelBuilder.Entity<traAsistenciaFecha>(entity =>
            {
                entity.HasKey(e => e.iAsistencia)
                    .HasName("PK_iActividadAsistenciaFechas");

                entity.Property(e => e.iAsistencia).ValueGeneratedNever();

                entity.Property(e => e.dDesde).HasColumnType("date");

                entity.Property(e => e.dHasta).HasColumnType("date");

                entity.Property(e => e.iDias).HasComputedColumnSql("(datediff(day,[dDesde],[dHasta])+(1))", true);

                entity.HasOne(d => d.iAsistenciaNavigation)
                    .WithOne(p => p.traAsistenciaFecha)
                    .HasForeignKey<traAsistenciaFecha>(d => d.iAsistencia)
                    .HasConstraintName("FK_traAsistenciaFecha_traAsistencia");
            });
            modelBuilder.Entity<traAsistenciaFechaCalculo>(entity =>
            {
                entity.HasKey(e => new { e.iAsistencia, e.iCalculoPlanilla });

                entity.Property(e => e.dDesde).HasColumnType("date");

                entity.Property(e => e.dHasta).HasColumnType("date");

                entity.Property(e => e.iDias).HasComputedColumnSql("(datediff(day,[dDesde],[dHasta])+(1))", true);

                entity.HasOne(d => d.iAsistenciaNavigation)
                    .WithMany(p => p.traAsistenciaFechaCalculo)
                    .HasForeignKey(d => d.iAsistencia)
                    .HasConstraintName("FK_traAsistenciaFechaCalculo_traAsistenciaFecha");

                entity.HasOne(d => d.iCalculoPlanillaNavigation)
                    .WithMany(p => p.traAsistenciaFechaCalculo)
                    .HasForeignKey(d => d.iCalculoPlanilla)
                    .HasConstraintName("FK_traAsistenciaFechaCalculo_remCalculoPlanilla");
            });
            modelBuilder.Entity<traAsistenciaHora>(entity =>
            {
                entity.HasKey(e => e.iAsistencia)
                    .HasName("PK_traActividadAsistenciaHora");

                entity.Property(e => e.iAsistencia).ValueGeneratedNever();

                entity.Property(e => e.dtDesde).HasPrecision(0);

                entity.Property(e => e.dtHasta).HasPrecision(0);

                entity.Property(e => e.iMinutos).HasComputedColumnSql("(datediff(minute,[dtDesde],[dtHasta]))", true);

                entity.HasOne(d => d.iAsistenciaNavigation)
                    .WithOne(p => p.traAsistenciaHora)
                    .HasForeignKey<traAsistenciaHora>(d => d.iAsistencia)
                    .HasConstraintName("FK_traAsistenciaHora_traAsistencia");
            });
            modelBuilder.Entity<traAsistenciaHoraCalculo>(entity =>
            {
                entity.HasKey(e => new { e.iAsistencia, e.iCalculoPlanilla });

                entity.Property(e => e.iMinutos).HasComputedColumnSql("(datediff(minute,[dtDesde],[dtHasta]))", true);

                entity.HasOne(d => d.iAsistenciaNavigation)
                    .WithMany(p => p.traAsistenciaHoraCalculo)
                    .HasForeignKey(d => d.iAsistencia)
                    .HasConstraintName("FK_traAsistenciaHoraCalculo_traAsistencia");

                entity.HasOne(d => d.iCalculoPlanillaNavigation)
                    .WithMany(p => p.traAsistenciaHoraCalculo)
                    .HasForeignKey(d => d.iCalculoPlanilla)
                    .HasConstraintName("FK_traAsistenciaHoraCalculo_remCalculoPlanilla");
            });
            modelBuilder.Entity<traAsistenciaMarcacion>(entity =>
            {
                entity.HasKey(e => new { e.iAsistencia, e.iMarcacion });

                entity.HasOne(d => d.iAsistenciaNavigation)
                    .WithMany(p => p.traAsistenciaMarcacion)
                    .HasForeignKey(d => d.iAsistencia)
                    .HasConstraintName("FK_traAsistenciaMarcacion_traAsistencia");

            });
            modelBuilder.Entity<traAsistenciaMotivo>(entity =>
            {
                entity.HasKey(e => e.iAsistencia);

                entity.Property(e => e.iAsistencia).ValueGeneratedNever();

                entity.Property(e => e.xMotivo)
                    .IsRequired()
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.HasOne(d => d.iAsistenciaNavigation)
                    .WithOne(p => p.traAsistenciaMotivo)
                    .HasForeignKey<traAsistenciaMotivo>(d => d.iAsistencia)
                    .HasConstraintName("FK_traAsistenciaMotivo_traAsistencia");
            });
            modelBuilder.Entity<traAsistenciaRelacion>(entity =>
            {
                entity.HasKey(e => new { e.iAsistencia, e.iAsistenciaRelacion });
            });
            modelBuilder.Entity<traAsistenciaVacaciones>(entity =>
            {
                entity.HasKey(e => e.iAsistencia);

                entity.Property(e => e.iAsistencia).ValueGeneratedNever();

                entity.Property(e => e.bPagada).HasComment("0:Gozada y Pagada, 1:Pagada, 2:Gozada, 3:Comprada");

                entity.HasOne(d => d.iAsistenciaNavigation)
                    .WithOne(p => p.traAsistenciaVacaciones)
                    .HasForeignKey<traAsistenciaVacaciones>(d => d.iAsistencia)
                    .HasConstraintName("FK_traAsistenciaVacaciones_traAsistencia");
            });
            modelBuilder.Entity<traCentroCosto>(entity =>
            {
                entity.HasKey(e => e.iContratoCentroCosto);

                entity.Property(e => e.bPrincipal)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.rPorcentaje)
                    .HasColumnType("decimal(5, 2)")
                    .HasDefaultValueSql("((100.00))");

                entity.HasOne(d => d.iContratoNavigation)
                    .WithMany(p => p.traCentroCosto)
                    .HasForeignKey(d => d.iContrato)
                    .HasConstraintName("FK_traCentroCosto_traContrato");

                entity.HasOne(d => d.sCentroCostoNavigation)
                    .WithMany(p => p.traCentroCosto)
                    .HasForeignKey(d => d.sCentroCosto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_traCentroCosto_orgCentroCosto");
            });
            modelBuilder.Entity<traConceptoFijo>(entity =>
            {
                entity.HasKey(e => e.iConceptoFijo)
                    .HasName("PK_traConceptosFijos");

                entity.HasIndex(e => new { e.bCalculo, e.iContrato, e.sConcepto, e.dDesde }, "IX_traConceptoFijo")
                    .IsUnique();

                entity.Property(e => e.bCalculo).HasDefaultValueSql("((2))");

                entity.Property(e => e.dDesde).HasColumnType("date");

                entity.Property(e => e.dHasta).HasColumnType("date");

                entity.Property(e => e.mValor).HasColumnType("money");

                entity.HasOne(d => d.bCalculoNavigation)
                    .WithMany(p => p.traConceptoFijo)
                    .HasForeignKey(d => d.bCalculo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_traConceptoFijo_isoCalculo");

                entity.HasOne(d => d.iContratoNavigation)
                    .WithMany(p => p.traConceptoFijo)
                    .HasForeignKey(d => d.iContrato)
                    .HasConstraintName("FK_traConceptoFijo_traContrato");

                entity.HasOne(d => d.sConceptoNavigation)
                    .WithMany(p => p.traConceptoFijo)
                    .HasForeignKey(d => d.sConcepto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_traConceptoFijo_remConcepto");
            });
            modelBuilder.Entity<traConceptoManual>(entity =>
            {
                entity.HasKey(e => e.iConceptoManual);

                entity.Property(e => e.mValor).HasColumnType("money");

                entity.HasOne(d => d.iContratoNavigation)
                    .WithMany(p => p.traConceptoManual)
                    .HasForeignKey(d => d.iContrato)
                    .HasConstraintName("FK_traConceptoManual_traContrato");

                entity.HasOne(d => d.sConceptoNavigation)
                    .WithMany(p => p.traConceptoManual)
                    .HasForeignKey(d => d.sConcepto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_traConceptoManual_remConcepto");
            });
            modelBuilder.Entity<traConceptoManualCentroCosto>(entity =>
            {
                entity.HasKey(e => e.iConceptoManual);

                entity.Property(e => e.iConceptoManual).ValueGeneratedNever();

                entity.HasOne(d => d.iConceptoManualNavigation)
                    .WithOne(p => p.traConceptoManualCentroCosto)
                    .HasForeignKey<traConceptoManualCentroCosto>(d => d.iConceptoManual)
                    .HasConstraintName("FK_traConceptoManualCentroCosto_traConceptoManual");

                entity.HasOne(d => d.sCentroCostoNavigation)
                    .WithMany(p => p.traConceptoManualCentroCosto)
                    .HasForeignKey(d => d.sCentroCosto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_traConceptoManualCentroCosto_orgCentroCosto");
            });
            modelBuilder.Entity<traContrato>(entity =>
            {
                entity.HasKey(e => e.iContrato);

                entity.Property(e => e.sCargo).HasDefaultValueSql("((1))");

                entity.Property(e => e.sSede).HasDefaultValueSql("((1))");

                entity.HasOne(d => d.iTrabajadorNavigation)
                    .WithMany(p => p.traContrato)
                    .HasForeignKey(d => d.iTrabajador)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_traContrato_perPersona");

                entity.HasOne(d => d.sCargoNavigation)
                    .WithMany(p => p.traContrato)
                    .HasForeignKey(d => d.sCargo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_traContrato_orgCargo");

                entity.HasOne(d => d.sEmpresaNavigation)
                    .WithMany(p => p.traContrato)
                    .HasForeignKey(d => d.sEmpresa)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_traContrato_orgEmpresa");

                entity.HasOne(d => d.sSedeNavigation)
                    .WithMany(p => p.traContrato)
                    .HasForeignKey(d => d.sSede)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_traContrato_orgSede");
            });
            modelBuilder.Entity<traContratoEstado>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("traContratoEstado");

                entity.Property(e => e.dCese).HasColumnType("date");

                entity.Property(e => e.dHasta).HasColumnType("date");

                entity.Property(e => e.dIngreso).HasColumnType("date");

                entity.Property(e => e.dVigenteDesde).HasColumnType("date");
            });
            modelBuilder.Entity<traContratoFormato>(entity =>
            {
                entity.HasKey(e => e.bFormato);

                entity.Property(e => e.bFormato).ValueGeneratedOnAdd();

                entity.Property(e => e.bTipoContrato).HasDefaultValueSql("((99))");

                entity.Property(e => e.xFormato)
                    .IsRequired()
                    .HasMaxLength(80)
                    .IsUnicode(false)
                    .HasComment("Nombre del Formato del documento word de contrato");

                entity.HasOne(d => d.bTipoContratoNavigation)
                    .WithMany(p => p.traContratoFormato)
                    .HasForeignKey(d => d.bTipoContrato)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_traContratoFormato_isoTipoContrato");
            });
            modelBuilder.Entity<traContratoLaboral>(entity =>
            {
                entity.HasKey(e => e.iContrato);

                entity.Property(e => e.iContrato).ValueGeneratedNever();

                entity.Property(e => e.bCategoria)
                    .HasDefaultValueSql("((1))")
                    .HasComment("(Estructura 11) Categoría: 1: Trabajador; 2: Pensionista;  4:Personal de Terceros y 5:Personal en Formación-modalidad formativa laboral.");

                entity.Property(e => e.bCategoriaOcupacional)
                    .HasDefaultValueSql("((3))")
                    .HasComment("(Tabla 24) 1:Ejecutivo, 2:Obrero, 3:Empleado");

                entity.Property(e => e.bPeriodicidadRem)
                    .HasDefaultValueSql("((1))")
                    .HasComment("(tabla 13) 1:Mensual, 2:Quincenal, 3:Semanal, etc.");

                entity.Property(e => e.bRegAtipicoDeJornadaTrab).HasComment("Sujeto a régimen alternativo, acumulativo o atípico de jornada de trabajo y descanso.");

                entity.Property(e => e.bSituacion)
                    .HasDefaultValueSql("((11))")
                    .HasComment("(tabla 15) 0: Baja, 1: Activo o Subsidiado, etc.");

                entity.Property(e => e.bSituacionEspecial).HasComment("1:Trabajador de dirección / 2:Trabajador de confianza / 0:Ninguna.");

                entity.Property(e => e.bTipoPago)
                    .HasDefaultValueSql("((2))")
                    .HasComment("(Tabla 16) 1: Efectivo, 2:Depósito en cuenta, 3:Otros");

                entity.Property(e => e.bTipoTrabajador).HasDefaultValueSql("((21))");

                entity.HasOne(d => d.bCategoriaOcupacionalNavigation)
                    .WithMany(p => p.traContratoLaboral)
                    .HasForeignKey(d => d.bCategoriaOcupacional)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_traContratoLaboral_isoCategoriaOcupacional");

                entity.HasOne(d => d.bPeriodicidadRemNavigation)
                    .WithMany(p => p.traContratoLaboral)
                    .HasForeignKey(d => d.bPeriodicidadRem)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_traContratoLaboral_isoPeriodicidad");

                entity.HasOne(d => d.bPlanillaNavigation)
                    .WithMany(p => p.traContratoLaboral)
                    .HasForeignKey(d => d.bPlanilla)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_traContratoLaboral_remPlanilla");

                entity.HasOne(d => d.bSituacionNavigation)
                    .WithMany(p => p.traContratoLaboral)
                    .HasForeignKey(d => d.bSituacion)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_traContratoLaboral_isoSituacionSunat");

                entity.HasOne(d => d.bTipoPagoNavigation)
                    .WithMany(p => p.traContratoLaboral)
                    .HasForeignKey(d => d.bTipoPago)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_traContratoLaboral_isoTipoPago");

                entity.HasOne(d => d.bTipoTrabajadorNavigation)
                    .WithMany(p => p.traContratoLaboral)
                    .HasForeignKey(d => d.bTipoTrabajador)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_traContratoLaboral_isoTipoTrabajador");

                entity.HasOne(d => d.iContratoNavigation)
                    .WithOne(p => p.traContratoLaboral)
                    .HasForeignKey<traContratoLaboral>(d => d.iContrato)
                    .HasConstraintName("FK_traContratoLaboral_traContrato");
            });
            modelBuilder.Entity<traContratoListaNegra>(entity =>
            {
                entity.HasKey(e => e.iContrato);

                entity.Property(e => e.iContrato).ValueGeneratedNever();

                entity.Property(e => e.xMotivo)
                    .IsRequired()
                    .IsUnicode(false);

                entity.HasOne(d => d.iContratoNavigation)
                    .WithOne(p => p.traContratoListaNegra)
                    .HasForeignKey<traContratoListaNegra>(d => d.iContrato)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_traContratoListaNegra_traContrato");
            });
            modelBuilder.Entity<traContratoPeriodo>(entity =>
            {
                entity.HasKey(e => e.iContratoPeriodo);

                entity.Property(e => e.bFormato).HasComment("Código de Formato del documento word de contrato ");

                entity.Property(e => e.dDesde).HasColumnType("date");

                entity.Property(e => e.dHasta).HasColumnType("date");

                entity.HasOne(d => d.bFormatoNavigation)
                    .WithMany(p => p.traContratoPeriodo)
                    .HasForeignKey(d => d.bFormato)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_traContratoPeriodo_traContratoFormato");

                entity.HasOne(d => d.iContratoNavigation)
                    .WithMany(p => p.traContratoPeriodo)
                    .HasForeignKey(d => d.iContrato)
                    .HasConstraintName("FK_traContratoPeriodo_traContrato");
            });
            modelBuilder.Entity<traContratoPeriodoCese>(entity =>
            {
                entity.HasKey(e => e.iContratoPeriodo);

                entity.Property(e => e.iContratoPeriodo).ValueGeneratedNever();

                entity.Property(e => e.dCese).HasColumnType("date");

                entity.Property(e => e.dLiquidacion).HasColumnType("date");

                entity.Property(e => e.xObservacion).IsUnicode(false);

                entity.Property(e => e.xObservacionLiquidacion).IsUnicode(false);

                entity.HasOne(d => d.bMotivoNavigation)
                    .WithMany(p => p.traContratoPeriodoCese)
                    .HasForeignKey(d => d.bMotivo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_traTrabajadorPeriodoContratoCese_isoMotivoCese");

                entity.HasOne(d => d.iContratoPeriodoNavigation)
                    .WithOne(p => p.traContratoPeriodoCese)
                    .HasForeignKey<traContratoPeriodoCese>(d => d.iContratoPeriodo)
                    .HasConstraintName("FK_traContratoPeriodoCese_traContratoPeriodo");
            });
            modelBuilder.Entity<traContratoPeriodoMintra>(entity =>
            {
                entity.HasKey(e => e.iContratoPeriodo);

                entity.Property(e => e.iContratoPeriodo).ValueGeneratedNever();

                entity.Property(e => e.dIngreso).HasColumnType("date");

                entity.Property(e => e.xNumero)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.HasOne(d => d.iContratoPeriodoNavigation)
                    .WithOne(p => p.traContratoPeriodoMintra)
                    .HasForeignKey<traContratoPeriodoMintra>(d => d.iContratoPeriodo)
                    .HasConstraintName("FK_traContratoPeriodoMintra_traContratoPeriodo");
            });
            modelBuilder.Entity<traContratoPeriodoPrueba>(entity =>
            {
                entity.HasKey(e => e.iContratoPeriodo);

                entity.Property(e => e.iContratoPeriodo).ValueGeneratedNever();

                entity.Property(e => e.dHasta).HasColumnType("date");

                entity.HasOne(d => d.iContratoPeriodoNavigation)
                    .WithOne(p => p.traContratoPeriodoPrueba)
                    .HasForeignKey<traContratoPeriodoPrueba>(d => d.iContratoPeriodo)
                    .HasConstraintName("FK_traContratoPeriodoPrueba_traContratoPeriodo");
            });
            modelBuilder.Entity<traEPS>(entity =>
            {
                entity.HasKey(e => e.iConceptoFijo)
                    .HasName("PK_traEPS_1");

                entity.Property(e => e.iConceptoFijo).ValueGeneratedNever();

                entity.Property(e => e.dPorcEmpleador)
                    .HasColumnType("decimal(5, 2)")
                    .HasDefaultValueSql("((0.00))");

                entity.Property(e => e.xCodigo)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.HasOne(d => d.iConceptoFijoNavigation)
                    .WithOne(p => p.traEPS)
                    .HasForeignKey<traEPS>(d => d.iConceptoFijo)
                    .HasConstraintName("FK_traEPS_traConceptoFijo");
            });
            modelBuilder.Entity<traEPSdet>(entity =>
            {
                entity.HasKey(e => new { e.iConceptoFijo, e.sPlanEPSdet })
                    .HasName("PK_traEPSdet_1");

                entity.Property(e => e.bCantidad).HasDefaultValueSql("((1))");

                entity.Property(e => e.dFechaReg)
                    .HasPrecision(0)
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.dPorcEmpleador).HasColumnType("decimal(5, 2)");

                entity.HasOne(d => d.iConceptoFijoNavigation)
                    .WithMany(p => p.traEPSdet)
                    .HasForeignKey(d => d.iConceptoFijo)
                    .HasConstraintName("FK_traEPSdet_traEPS");

                entity.HasOne(d => d.sPlanEPSdetNavigation)
                    .WithMany(p => p.traEPSdet)
                    .HasForeignKey(d => d.sPlanEPSdet)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_traEPSdet_sysPlanEPSdet");
            });

            modelBuilder.Entity<traJefeDirecto>(entity =>
            {
                entity.HasKey(e => e.iJefeDirecto);

                entity.Property(e => e.iTrabajador).HasComment("iTrabajador del Jefe Directo");

                entity.HasOne(d => d.iContratoNavigation)
                    .WithMany(p => p.traJefeDirecto)
                    .HasForeignKey(d => d.iContrato)
                    .HasConstraintName("FK_traJefeDirecto_traContrato");

                entity.HasOne(d => d.iTrabajadorNavigation)
                    .WithMany(p => p.traJefeDirecto)
                    .HasForeignKey(d => d.iTrabajador)
                    .HasConstraintName("FK_traJefeDirecto_traTrabajador");
            });

            modelBuilder.Entity<traPrestamo>(entity =>
            {
                entity.HasKey(e => e.iPrestamo);

                entity.Property(e => e.mValor).HasColumnType("money");

                entity.HasOne(d => d.iContratoNavigation)
                    .WithMany(p => p.traPrestamo)
                    .HasForeignKey(d => d.iContrato)
                    .HasConstraintName("FK_traPrestamo_traContrato");

                entity.HasOne(d => d.sConceptoNavigation)
                    .WithMany(p => p.traPrestamo)
                    .HasForeignKey(d => d.sConcepto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_traPrestamo_remConcepto");

                entity.HasOne(d => d.sMonedaNavigation)
                    .WithMany(p => p.traPrestamo)
                    .HasForeignKey(d => d.sMoneda)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_traPrestamo_isoMoneda");
            });
            modelBuilder.Entity<traPrestamoCalculo>(entity =>
            {
                entity.HasKey(e => e.iPrestamo);

                entity.Property(e => e.iPrestamo).ValueGeneratedNever();

                entity.HasOne(d => d.iCalculoPlanillaNavigation)
                    .WithMany(p => p.traPrestamoCalculo)
                    .HasForeignKey(d => d.iCalculoPlanilla)
                    .HasConstraintName("FK_traPrestamoCalculo_remCalculoPlanilla");

                entity.HasOne(d => d.iPrestamoNavigation)
                    .WithOne(p => p.traPrestamoCalculo)
                    .HasForeignKey<traPrestamoCalculo>(d => d.iPrestamo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_traPrestamoCalculo_traPrestamo");
            });
            modelBuilder.Entity<traPrestamoCuota>(entity =>
            {
                entity.HasKey(e => e.iPrestamoCuota);

                entity.Property(e => e.dFecha).HasColumnType("date");

                entity.Property(e => e.mValor).HasColumnType("money");

                entity.HasOne(d => d.bCalculoNavigation)
                    .WithMany(p => p.traPrestamoCuota)
                    .HasForeignKey(d => d.bCalculo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_traPrestamoCuota_isoCalculo");

                entity.HasOne(d => d.iPrestamoNavigation)
                    .WithMany(p => p.traPrestamoCuota)
                    .HasForeignKey(d => d.iPrestamo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_traPrestamoCuota_traPrestamo");
            });
            modelBuilder.Entity<traPrestamoCuotaCalculo>(entity =>
            {
                entity.HasKey(e => new { e.iPrestamoCuota, e.iCalculoPlanilla });

                entity.HasOne(d => d.iCalculoPlanillaNavigation)
                    .WithMany(p => p.traPrestamoCuotaCalculo)
                    .HasForeignKey(d => d.iCalculoPlanilla)
                    .HasConstraintName("FK_traPrestamoCuotaCalculo_remCalculoPlanilla");

                entity.HasOne(d => d.iPrestamoCuotaNavigation)
                    .WithMany(p => p.traPrestamoCuotaCalculo)
                    .HasForeignKey(d => d.iPrestamoCuota)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_traPrestamoCuotaCalculo_traPrestamoCuota");
            });
            modelBuilder.Entity<traPrestamoDesembolso>(entity =>
            {
                entity.HasKey(e => e.iPrestamo);

                entity.Property(e => e.iPrestamo).ValueGeneratedNever();

                entity.Property(e => e.dFecha).HasColumnType("date");

                entity.HasOne(d => d.iPrestamoNavigation)
                    .WithOne(p => p.traPrestamoDesembolso)
                    .HasForeignKey<traPrestamoDesembolso>(d => d.iPrestamo)
                    .HasConstraintName("FK_traPrestamoDesembolso_traPrestamo");

                entity.HasOne(d => d.sConceptoNavigation)
                    .WithMany(p => p.traPrestamoDesembolso)
                    .HasForeignKey(d => d.sConcepto)
                    .HasConstraintName("FK_traPrestamoDesembolso_remConcepto");
            });
            modelBuilder.Entity<traPrestamoMotivo>(entity =>
            {
                entity.HasKey(e => e.iPrestamo);

                entity.Property(e => e.iPrestamo).ValueGeneratedNever();

                entity.Property(e => e.xMotivo)
                    .IsRequired()
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.HasOne(d => d.iPrestamoNavigation)
                    .WithOne(p => p.traPrestamoMotivo)
                    .HasForeignKey<traPrestamoMotivo>(d => d.iPrestamo)
                    .HasConstraintName("FK_traPrestamoMotivo_traPrestamo");
            });

            modelBuilder.Entity<traRUC>(entity =>
            {
                entity.HasKey(e => e.iTrabajador);

                entity.Property(e => e.iTrabajador).ValueGeneratedNever();

                entity.Property(e => e.cRUC)
                    .IsRequired()
                    .HasMaxLength(11)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.HasOne(d => d.iTrabajadorNavigation)
                    .WithOne(p => p.traRUC)
                    .HasForeignKey<traRUC>(d => d.iTrabajador)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_traRUC_traTrabajador");
            });
            modelBuilder.Entity<traRxH>(entity =>
            {
                entity.HasKey(e => e.iRxH);

                entity.HasIndex(e => new { e.iContrato, e.iPeriodo }, "IX_traRxH");

                entity.HasIndex(e => new { e.bTipoComprobante, e.sSerieComprobante, e.iNumComprobante }, "IX_traRxH_1")
                    .IsUnique();

                entity.Property(e => e.bTipoComprobante)
                    .HasDefaultValueSql("((1))")
                    .HasComment("1:R=RECIBO POR HONORARIOS,2:N=NOTA DE CRÉDITO,3:D=DIETA,4:O=OTRO COMPROBANTE");

                entity.Property(e => e.dEmision).HasColumnType("date");

                entity.Property(e => e.dPago).HasColumnType("date");

                entity.Property(e => e.mMonto).HasColumnType("money");

                entity.HasOne(d => d.bTipoComprobanteNavigation)
                    .WithMany(p => p.traRxH)
                    .HasForeignKey(d => d.bTipoComprobante)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_traRxH_isoTipoComprobante");

                entity.HasOne(d => d.iContratoNavigation)
                    .WithMany(p => p.traRxH)
                    .HasForeignKey(d => d.iContrato)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_traRxH_traContrato");

                entity.HasOne(d => d.iPeriodoNavigation)
                    .WithMany(p => p.traRxH)
                    .HasForeignKey(d => d.iPeriodo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_traRxH_remPeriodo");
            });
            modelBuilder.Entity<traSCTR>(entity =>
            {
                entity.HasKey(e => e.iContrato);

                entity.Property(e => e.iContrato).ValueGeneratedNever();

                entity.Property(e => e.bRiesgoSctrPension).HasDefaultValueSql("((5))");

                entity.Property(e => e.bRiesgoSctrSalud).HasDefaultValueSql("((5))");

                entity.HasOne(d => d.iContratoNavigation)
                    .WithOne(p => p.traSCTR)
                    .HasForeignKey<traSCTR>(d => d.iContrato)
                    .HasConstraintName("FK_traSCTR_traContrato");
            });
            modelBuilder.Entity<traSCTRfecha>(entity =>
            {
                entity.HasKey(e => new { e.iContrato, e.dDesde });

                entity.Property(e => e.dDesde).HasColumnType("date");

                entity.Property(e => e.bSCTRestado).HasDefaultValueSql("((1))");

                entity.Property(e => e.dHasta).HasColumnType("date");

                entity.HasOne(d => d.iContratoNavigation)
                    .WithMany(p => p.traSCTRfecha)
                    .HasForeignKey(d => d.iContrato)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_traSCTRfecha_traSCTR");
            });
            modelBuilder.Entity<traStatusContrato>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("traStatusContrato");

                entity.Property(e => e.cTipo)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.dDesde).HasColumnType("date");

                entity.Property(e => e.dHasta).HasColumnType("date");
            });
            modelBuilder.Entity<traTrabajador>(entity =>
            {
                entity.HasKey(e => e.iTrabajador)
                    .HasName("PK_Trabajador");

                entity.Property(e => e.iTrabajador)
                    .ValueGeneratedNever()
                    .HasComment("");

                entity.Property(e => e.bConvenioEvitarDobleTributacion).HasComment("(Tabla 25) 0:Nunguno, 1:Canada, 2:Chile, 3:Can, 4:Brasil");

                entity.Property(e => e.bEstadoCivil)
                    .HasDefaultValueSql("((1))")
                    .HasComment("1: Soltero, 2:Casado, 3:divorciado, 4:Viudo");

                entity.Property(e => e.bRegimenPensionario).HasDefaultValueSql("((99))");

                entity.HasOne(d => d.bRegimenPensionarioNavigation)
                    .WithMany(p => p.traTrabajador)
                    .HasForeignKey(d => d.bRegimenPensionario)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_traTrabajador_isoRegimenPensionario");

                entity.HasOne(d => d.iTrabajadorNavigation)
                    .WithOne(p => p.traTrabajador)
                    .HasForeignKey<traTrabajador>(d => d.iTrabajador)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_traTrabajador_perPersona");
            });
            modelBuilder.Entity<traVidaLey>(entity =>
            {
                entity.HasKey(e => e.iConceptoFijo)
                    .HasName("PK_traTrabajadorVidaLey");

                entity.Property(e => e.iConceptoFijo).ValueGeneratedNever();

                entity.Property(e => e.rTasa).HasColumnType("decimal(5, 2)");

                entity.Property(e => e.xCodigo)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.HasOne(d => d.iConceptoFijoNavigation)
                    .WithOne(p => p.traVidaLey)
                    .HasForeignKey<traVidaLey>(d => d.iConceptoFijo)
                    .HasConstraintName("FK_traVidaLey_traConceptoFijo");

                entity.HasOne(d => d.sAseguradoraNavigation)
                    .WithMany(p => p.traVidaLey)
                    .HasForeignKey(d => d.sAseguradora)
                    .HasConstraintName("FK_traVidaLey_terEmpresaAseguradoraSalud");
            });

        }
    }
}