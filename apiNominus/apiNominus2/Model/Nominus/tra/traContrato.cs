﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class traContrato
    {
        public traContrato()
        {
            remCalculoPlanilla = new HashSet<remCalculoPlanilla>();
            traArea = new HashSet<traArea>();
            traAsistencia = new HashSet<traAsistencia>();
            traCentroCosto = new HashSet<traCentroCosto>();
            traConceptoFijo = new HashSet<traConceptoFijo>();
            traConceptoManual = new HashSet<traConceptoManual>();
            traContratoPeriodo = new HashSet<traContratoPeriodo>();
            traJefeDirecto = new HashSet<traJefeDirecto>();
            traPrestamo = new HashSet<traPrestamo>();
            traRxH = new HashSet<traRxH>();
        }

        public int iContrato { get; set; }
        public int iTrabajador { get; set; }
        public short sEmpresa { get; set; }
        public short sSede { get; set; }
        public short sCargo { get; set; }

        public virtual perPersona iTrabajadorNavigation { get; set; }
        public virtual orgCargo sCargoNavigation { get; set; }
        public virtual orgEmpresa sEmpresaNavigation { get; set; }
        public virtual orgSede sSedeNavigation { get; set; }
        public virtual traContratoLaboral traContratoLaboral { get; set; }
        public virtual traContratoListaNegra traContratoListaNegra { get; set; }
        public virtual traSCTR traSCTR { get; set; }
        [JsonIgnore] public virtual ICollection<remCalculoPlanilla> remCalculoPlanilla { get; set; }
        [JsonIgnore] public virtual ICollection<traArea> traArea { get; set; }
        [JsonIgnore] public virtual ICollection<traAsistencia> traAsistencia { get; set; }
        [JsonIgnore] public virtual ICollection<traCentroCosto> traCentroCosto { get; set; }
        [JsonIgnore] public virtual ICollection<traConceptoFijo> traConceptoFijo { get; set; }
        [JsonIgnore] public virtual ICollection<traConceptoManual> traConceptoManual { get; set; }
        [JsonIgnore] public virtual ICollection<traContratoPeriodo> traContratoPeriodo { get; set; }
        [JsonIgnore] public virtual ICollection<traJefeDirecto> traJefeDirecto { get; set; }
        [JsonIgnore] public virtual ICollection<traPrestamo> traPrestamo { get; set; }
        [JsonIgnore] public virtual ICollection<traRxH> traRxH { get; set; }
    }
}
