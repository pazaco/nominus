﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class traAsistenciaEstructuraSalarial
    {
        public int iAsistencia { get; set; }
        public short sEstructuraSalarial { get; set; }

        public virtual traAsistencia iAsistenciaNavigation { get; set; }
        public virtual orgEstructuraSalarial sEstructuraSalarialNavigation { get; set; }
    }
}
