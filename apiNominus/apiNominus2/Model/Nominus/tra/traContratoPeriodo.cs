﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class traContratoPeriodo
    {
        public int iContratoPeriodo { get; set; }
        public int iContrato { get; set; }
        public DateTime dDesde { get; set; }
        public DateTime? dHasta { get; set; }
        public byte bFormato { get; set; }

        public virtual traContratoFormato bFormatoNavigation { get; set; }
        public virtual traContrato iContratoNavigation { get; set; }
        public virtual traContratoPeriodoCese traContratoPeriodoCese { get; set; }
        public virtual traContratoPeriodoMintra traContratoPeriodoMintra { get; set; }
        public virtual traContratoPeriodoPrueba traContratoPeriodoPrueba { get; set; }
    }
}
