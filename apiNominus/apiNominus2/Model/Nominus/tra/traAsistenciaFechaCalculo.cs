﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class traAsistenciaFechaCalculo
    {
        public int iAsistencia { get; set; }
        public DateTime dDesde { get; set; }
        public DateTime dHasta { get; set; }
        public int? iDias { get; set; }
        public int iCalculoPlanilla { get; set; }

        public virtual traAsistenciaFecha iAsistenciaNavigation { get; set; }
        public virtual remCalculoPlanilla iCalculoPlanillaNavigation { get; set; }
    }
}
