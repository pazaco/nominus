﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class traRxH
    {
        public int iRxH { get; set; }
        public int iContrato { get; set; }
        public int iPeriodo { get; set; }
        public byte bTipoComprobante { get; set; }
        public short sSerieComprobante { get; set; }
        public int iNumComprobante { get; set; }
        public decimal mMonto { get; set; }
        public DateTime dEmision { get; set; }
        public DateTime dPago { get; set; }
        public bool bRetencion4taCateg { get; set; }

        public virtual isoTipoComprobante bTipoComprobanteNavigation { get; set; }
        public virtual traContrato iContratoNavigation { get; set; }
        public virtual remPeriodo iPeriodoNavigation { get; set; }
    }
}
