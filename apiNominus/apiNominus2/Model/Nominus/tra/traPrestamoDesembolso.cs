﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class traPrestamoDesembolso
    {
        public int iPrestamo { get; set; }
        public DateTime dFecha { get; set; }
        public short? sConcepto { get; set; }

        public virtual traPrestamo iPrestamoNavigation { get; set; }
        public virtual remConcepto sConceptoNavigation { get; set; }
    }
}
