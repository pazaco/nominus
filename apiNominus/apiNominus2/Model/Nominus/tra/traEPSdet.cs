﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class traEPSdet
    {
        public int iConceptoFijo { get; set; }
        public short sPlanEPSdet { get; set; }
        public byte bCantidad { get; set; }
        public decimal? dPorcEmpleador { get; set; }
        public int iUsuarioReg { get; set; }
        public DateTime dFechaReg { get; set; }

        public virtual traEPS iConceptoFijoNavigation { get; set; }
        public virtual sysPlanEPSdet sPlanEPSdetNavigation { get; set; }
    }
}
