﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class traStatusContrato
    {
        public int iContrato { get; set; }
        public int? iContratoPeriodo { get; set; }
        public int iTrabajador { get; set; }
        public short sEmpresa { get; set; }
        public short sSede { get; set; }
        public short sCargo { get; set; }
        public string cTipo { get; set; }
        public DateTime? dDesde { get; set; }
        public DateTime? dHasta { get; set; }
        public byte? bFormato { get; set; }
        public int? iJefe { get; set; }
    }
}
