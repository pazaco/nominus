﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class traArea
    {
        public int iContrato { get; set; }
        public short sArea { get; set; }
        public decimal rPorcentaje { get; set; }
        public bool? bPrincipal { get; set; }

        public virtual traContrato iContratoNavigation { get; set; }
        public virtual orgArea sAreaNavigation { get; set; }
    }
}
