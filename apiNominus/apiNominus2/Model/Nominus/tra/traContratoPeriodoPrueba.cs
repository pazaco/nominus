﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class traContratoPeriodoPrueba
    {
        public int iContratoPeriodo { get; set; }
        public DateTime dHasta { get; set; }

        public virtual traContratoPeriodo iContratoPeriodoNavigation { get; set; }
    }
}
