﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class traAsistencia
    {
        public traAsistencia()
        {
            traAsistenciaHoraCalculo = new HashSet<traAsistenciaHoraCalculo>();
            traAsistenciaMarcacion = new HashSet<traAsistenciaMarcacion>();
        }

        public int iAsistencia { get; set; }
        public int iContrato { get; set; }
        public short sConcepto { get; set; }

        public virtual traContrato iContratoNavigation { get; set; }
        public virtual remConcepto sConceptoNavigation { get; set; }
        public virtual traAsistenciaCentroCosto traAsistenciaCentroCosto { get; set; }
        public virtual traAsistenciaEstructuraSalarial traAsistenciaEstructuraSalarial { get; set; }
        public virtual traAsistenciaFecha traAsistenciaFecha { get; set; }
        public virtual traAsistenciaHora traAsistenciaHora { get; set; }
        public virtual traAsistenciaMotivo traAsistenciaMotivo { get; set; }
        public virtual traAsistenciaVacaciones traAsistenciaVacaciones { get; set; }
        [JsonIgnore] public virtual ICollection<traAsistenciaHoraCalculo> traAsistenciaHoraCalculo { get; set; }
        [JsonIgnore] public virtual ICollection<traAsistenciaMarcacion> traAsistenciaMarcacion { get; set; }
    }
}
