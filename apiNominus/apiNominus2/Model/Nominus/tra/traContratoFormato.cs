﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class traContratoFormato
    {
        public traContratoFormato()
        {
            traContratoPeriodo = new HashSet<traContratoPeriodo>();
        }

        public byte bFormato { get; set; }
        public string xFormato { get; set; }
        public string jFormato { get; set; }
        public string jFormatoSinPrueba { get; set; }
        public string jFormatoRenovacion { get; set; }
        public byte bTipoContrato { get; set; }

        public virtual isoTipoContrato bTipoContratoNavigation { get; set; }
        [JsonIgnore] public virtual ICollection<traContratoPeriodo> traContratoPeriodo { get; set; }
    }
}
