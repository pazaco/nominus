﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class traAsistenciaHoraCalculo
    {
        public int iAsistencia { get; set; }
        public DateTime dtDesde { get; set; }
        public DateTime dtHasta { get; set; }
        public int? iMinutos { get; set; }
        public int iCalculoPlanilla { get; set; }

        public virtual traAsistencia iAsistenciaNavigation { get; set; }
        public virtual remCalculoPlanilla iCalculoPlanillaNavigation { get; set; }
    }
}
