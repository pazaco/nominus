﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class traAsistenciaHora
    {
        public int iAsistencia { get; set; }
        public DateTime dtDesde { get; set; }
        public DateTime dtHasta { get; set; }
        public int? iMinutos { get; set; }

        public virtual traAsistencia iAsistenciaNavigation { get; set; }
    }
}
