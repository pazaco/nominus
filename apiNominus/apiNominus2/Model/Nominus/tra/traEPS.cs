﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class traEPS
    {
        public traEPS()
        {
            traEPSdet = new HashSet<traEPSdet>();
        }

        public int iConceptoFijo { get; set; }
        public string xCodigo { get; set; }
        public decimal dPorcEmpleador { get; set; }

        public virtual traConceptoFijo iConceptoFijoNavigation { get; set; }
        [JsonIgnore] public virtual ICollection<traEPSdet> traEPSdet { get; set; }
    }
}
