﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class traPrestamoMotivo
    {
        public int iPrestamo { get; set; }
        public string xMotivo { get; set; }

        public virtual traPrestamo iPrestamoNavigation { get; set; }
    }
}
