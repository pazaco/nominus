﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class traJefeDirecto
    {
        public int iJefeDirecto { get; set; }
        public int iContrato { get; set; }
        public int iTrabajador { get; set; }

        public virtual traContrato iContratoNavigation { get; set; }
        public virtual traTrabajador iTrabajadorNavigation { get; set; }
    }
}
