﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class traContratoLaboral
    {
        public int iContrato { get; set; }
        public byte bPlanilla { get; set; }
        public byte bTipoTrabajador { get; set; }
        public bool bRegAtipicoDeJornadaTrab { get; set; }
        public bool bJornadaTrabMax { get; set; }
        public bool bHorarioNoc { get; set; }
        public bool bSindicalizado { get; set; }
        public bool bRentas5taExoneradas { get; set; }
        public byte bSituacion { get; set; }
        public byte bSituacionEspecial { get; set; }
        public byte bCategoriaOcupacional { get; set; }
        public byte bCategoria { get; set; }
        public byte bTipoPago { get; set; }
        public byte bPeriodicidadRem { get; set; }

        public virtual isoCategoriaOcupacional bCategoriaOcupacionalNavigation { get; set; }
        public virtual isoPeriodicidad bPeriodicidadRemNavigation { get; set; }
        public virtual remPlanilla bPlanillaNavigation { get; set; }
        public virtual isoSituacionSunat bSituacionNavigation { get; set; }
        public virtual isoTipoPago bTipoPagoNavigation { get; set; }
        public virtual isoTipoTrabajador bTipoTrabajadorNavigation { get; set; }
        public virtual traContrato iContratoNavigation { get; set; }
    }
}
