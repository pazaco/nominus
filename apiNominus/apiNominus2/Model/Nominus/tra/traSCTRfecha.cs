﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class traSCTRfecha
    {
        public int iContrato { get; set; }
        public DateTime dDesde { get; set; }
        public DateTime? dHasta { get; set; }
        public byte bSCTRestado { get; set; }

        public virtual traSCTR iContratoNavigation { get; set; }
    }
}
