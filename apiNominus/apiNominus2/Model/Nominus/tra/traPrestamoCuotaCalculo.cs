﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class traPrestamoCuotaCalculo
    {
        public int iPrestamoCuota { get; set; }
        public int iCalculoPlanilla { get; set; }

        public virtual remCalculoPlanilla iCalculoPlanillaNavigation { get; set; }
        public virtual traPrestamoCuota iPrestamoCuotaNavigation { get; set; }
    }
}
