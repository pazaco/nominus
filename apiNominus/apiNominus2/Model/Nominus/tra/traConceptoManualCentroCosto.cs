﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class traConceptoManualCentroCosto
    {
        public int iConceptoManual { get; set; }
        public short sCentroCosto { get; set; }

        public virtual traConceptoManual iConceptoManualNavigation { get; set; }
        public virtual orgCentroCosto sCentroCostoNavigation { get; set; }
    }
}
