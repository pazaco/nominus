﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class traContratoPeriodoMintra
    {
        public int iContratoPeriodo { get; set; }
        public string xNumero { get; set; }
        public DateTime? dIngreso { get; set; }

        public virtual traContratoPeriodo iContratoPeriodoNavigation { get; set; }
    }
}
