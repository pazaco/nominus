﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class traAsistenciaRelacion
    {
        public int iAsistencia { get; set; }
        public int iAsistenciaRelacion { get; set; }
    }
}
