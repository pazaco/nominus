﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class isoTipoContrato
    {
        public isoTipoContrato()
        {
            traContratoFormato = new HashSet<traContratoFormato>();
        }

        public byte bTipoContrato { get; set; }
        public string xTipoContrato { get; set; }

        [JsonIgnore] public virtual ICollection<traContratoFormato> traContratoFormato { get; set; }
    }
}
