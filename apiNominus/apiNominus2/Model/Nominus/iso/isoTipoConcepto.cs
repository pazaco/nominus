﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class isoTipoConcepto
    {
        public isoTipoConcepto()
        {
            remConcepto = new HashSet<remConcepto>();
            remConceptoCalculo = new HashSet<remConceptoCalculo>();
        }

        public byte bTipoConcepto { get; set; }
        public string xTipoConcepto { get; set; }

        [JsonIgnore] public virtual ICollection<remConcepto> remConcepto { get; set; }
        [JsonIgnore] public virtual ICollection<remConceptoCalculo> remConceptoCalculo { get; set; }
    }
}
