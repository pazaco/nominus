﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class isoCalculo
    {
        public isoCalculo()
        {
            orgEmpresaCalculoCtaContableNeto = new HashSet<orgEmpresaCalculoCtaContableNeto>();
            remConceptoCalculo = new HashSet<remConceptoCalculo>();
            remConceptoCalculoHistory = new HashSet<remConceptoCalculoHistory>();
            remPeriodo = new HashSet<remPeriodo>();
            traConceptoFijo = new HashSet<traConceptoFijo>();
            traPrestamoCuota = new HashSet<traPrestamoCuota>();
        }

        public byte bCalculo { get; set; }
        public byte bTipoCalculo { get; set; }
        public string xCalculo { get; set; }
        public byte bDeclarar { get; set; }
        public byte bUsar { get; set; }

        public virtual isoTipoCalculo bTipoCalculoNavigation { get; set; }
        [JsonIgnore] public virtual ICollection<orgEmpresaCalculoCtaContableNeto> orgEmpresaCalculoCtaContableNeto { get; set; }
        [JsonIgnore] public virtual ICollection<remConceptoCalculo> remConceptoCalculo { get; set; }
        [JsonIgnore] public virtual ICollection<remConceptoCalculoHistory> remConceptoCalculoHistory { get; set; }
        [JsonIgnore] public virtual ICollection<remPeriodo> remPeriodo { get; set; }
        [JsonIgnore] public virtual ICollection<traConceptoFijo> traConceptoFijo { get; set; }
        [JsonIgnore] public virtual ICollection<traPrestamoCuota> traPrestamoCuota { get; set; }
    }
}
