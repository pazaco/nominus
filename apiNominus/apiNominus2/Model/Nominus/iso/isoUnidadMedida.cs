﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class isoUnidadMedida
    {
        public isoUnidadMedida()
        {
            remConcepto = new HashSet<remConcepto>();
            remConceptoCalculoAsistencia = new HashSet<remConceptoCalculoAsistencia>();
        }

        public short sUnidadMedida { get; set; }
        public string xUnidadMedida { get; set; }
        public string xAbreviado { get; set; }

        [JsonIgnore] public virtual ICollection<remConcepto> remConcepto { get; set; }
        [JsonIgnore] public virtual ICollection<remConceptoCalculoAsistencia> remConceptoCalculoAsistencia { get; set; }
    }
}
