﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class isoTipoCalculo
    {
        public isoTipoCalculo()
        {
            isoCalculo = new HashSet<isoCalculo>();
        }

        public byte bTipoCalculo { get; set; }
        public string xTipoCalculo { get; set; }

        [JsonIgnore] public virtual ICollection<isoCalculo> isoCalculo { get; set; }
    }
}
