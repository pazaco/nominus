﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class isoNacionalidad
    {
        public short sNacionalidad { get; set; }
        public string xNacionalidad { get; set; }
        public string xCiudadano { get; set; }
        public short? sPais { get; set; }
    }
}
