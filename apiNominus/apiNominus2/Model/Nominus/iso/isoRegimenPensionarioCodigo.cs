﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class isoRegimenPensionarioCodigo
    {
        public byte bRegimenPensionario { get; set; }
        public string xCodigo { get; set; }

        public virtual isoRegimenPensionario bRegimenPensionarioNavigation { get; set; }
    }
}
