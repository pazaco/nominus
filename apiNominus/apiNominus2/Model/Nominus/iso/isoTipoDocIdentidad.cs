﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class isoTipoDocIdentidad
    {
        public isoTipoDocIdentidad()
        {
            perPersona = new HashSet<perPersona>();
        }

        public byte bTipoDocIdentidad { get; set; }
        public string xTipoDocIdentidad { get; set; }
        public string xTip { get; set; }

        [JsonIgnore] public virtual ICollection<perPersona> perPersona { get; set; }
    }
}
