﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class isoInstitucionEducativa
    {
        public isoInstitucionEducativa()
        {
            perEducacion = new HashSet<perEducacion>();
        }

        public int iInstitucionEducativa { get; set; }
        public string xInstitucionEducativa { get; set; }
        public string cInstitucionEducativa { get; set; }

        [JsonIgnore] public virtual ICollection<perEducacion> perEducacion { get; set; }
    }
}
