﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class isoTipoActividad
    {
        public int iTipoActividad { get; set; }
        public string xTipoActividad { get; set; }
    }
}
