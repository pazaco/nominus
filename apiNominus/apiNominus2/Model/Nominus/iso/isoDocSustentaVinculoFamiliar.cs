﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class isoDocSustentaVinculoFamiliar
    {
        public isoDocSustentaVinculoFamiliar()
        {
            perFamiliarDocSustentaVinculo = new HashSet<perFamiliarDocSustentaVinculo>();
        }

        public byte bDocSustentaVinculo { get; set; }
        public string xDocSustentaVinculo { get; set; }

        [JsonIgnore] public virtual ICollection<perFamiliarDocSustentaVinculo> perFamiliarDocSustentaVinculo { get; set; }
    }
}
