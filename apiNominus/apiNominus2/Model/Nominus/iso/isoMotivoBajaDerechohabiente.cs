﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class isoMotivoBajaDerechohabiente
    {
        public isoMotivoBajaDerechohabiente()
        {
            perFamiliarBajaDH = new HashSet<perFamiliarBajaDH>();
        }

        public byte bMotivoBaja { get; set; }
        public string xMotivoBaja { get; set; }

        [JsonIgnore] public virtual ICollection<perFamiliarBajaDH> perFamiliarBajaDH { get; set; }
    }
}
