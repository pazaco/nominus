﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class isoMotivoCese
    {
        public isoMotivoCese()
        {
            traContratoPeriodoCese = new HashSet<traContratoPeriodoCese>();
        }

        public byte bMotivoCese { get; set; }
        public string xMotivoCese { get; set; }
        public bool bSectorPrivado { get; set; }
        public bool bSectorPublico { get; set; }
        public bool bSectorOtros { get; set; }
        public bool bSoloPensionista { get; set; }

        [JsonIgnore] public virtual ICollection<traContratoPeriodoCese> traContratoPeriodoCese { get; set; }
    }
}
