﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class isoMoneda
    {
        public isoMoneda()
        {
            orgEmpresaCtaBancaria = new HashSet<orgEmpresaCtaBancaria>();
            perCtaBancaria = new HashSet<perCtaBancaria>();
            traPrestamo = new HashSet<traPrestamo>();
        }

        public short sMoneda { get; set; }
        public string xMoneda { get; set; }
        public string xMonedaPlural { get; set; }
        public string xSimbolo { get; set; }
        public string xCodigo { get; set; }
        public byte rDecimales { get; set; }

        [JsonIgnore] public virtual ICollection<orgEmpresaCtaBancaria> orgEmpresaCtaBancaria { get; set; }
        [JsonIgnore] public virtual ICollection<perCtaBancaria> perCtaBancaria { get; set; }
        [JsonIgnore] public virtual ICollection<traPrestamo> traPrestamo { get; set; }
    }
}
