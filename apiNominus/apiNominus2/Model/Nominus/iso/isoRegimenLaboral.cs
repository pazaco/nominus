﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class isoRegimenLaboral
    {
        public isoRegimenLaboral()
        {
            orgEmpresa = new HashSet<orgEmpresa>();
        }

        public byte bRegimenLaboral { get; set; }
        public string xRegimenLaboral { get; set; }
        public string xAbreviado { get; set; }
        public bool bSectorPrivado { get; set; }
        public bool bSectorPublico { get; set; }
        public bool bSectorOtros { get; set; }

        [JsonIgnore] public virtual ICollection<orgEmpresa> orgEmpresa { get; set; }
    }
}
