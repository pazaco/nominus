﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{

    public partial class isoPais
    {
        public isoPais()
        {
            isoDepartamento = new HashSet<isoDepartamento>();
            perNacionalidad = new HashSet<perNacionalidad>();
            perPaisEmisorDocIdentidad = new HashSet<perPaisEmisorDocIdentidad>();
        }

        public short sPais { get; set; }
        public string cPais { get; set; }
        public string xPais { get; set; }
        public string cPais3 { get; set; }

        [JsonIgnore] public virtual ICollection<isoDepartamento> isoDepartamento { get; set; }
        [JsonIgnore] public virtual ICollection<perNacionalidad> perNacionalidad { get; set; }
        [JsonIgnore] public virtual ICollection<perPaisEmisorDocIdentidad> perPaisEmisorDocIdentidad { get; set; }
    }

    public partial class isoDepartamento
    {
        public isoDepartamento()
        {
            isoProvincia = new HashSet<isoProvincia>();
        }

        public short sDepartamento { get; set; }
        public short sPais { get; set; }
        public string xDepartamento { get; set; }
        public string xCodSunat { get; set; }

        public virtual isoPais sPaisNavigation { get; set; }
        [JsonIgnore] public virtual ICollection<isoProvincia> isoProvincia { get; set; }
    }

    public partial class isoProvincia
    {
        public isoProvincia()
        {
            isoDistrito = new HashSet<isoDistrito>();
        }

        public short sProvincia { get; set; }
        public short sDepartamento { get; set; }
        public string xProvincia { get; set; }
        public string xCodSunat { get; set; }

        public virtual isoDepartamento sDepartamentoNavigation { get; set; }
        [JsonIgnore] public virtual ICollection<isoDistrito> isoDistrito { get; set; }
    }

    public partial class isoDistrito
    {
        public isoDistrito()
        {
            orgEmpresaDireccion = new HashSet<orgEmpresaDireccion>();
            orgEmpresaSedeDireccion = new HashSet<orgEmpresaSedeDireccion>();
            perDireccion = new HashSet<perDireccion>();
            terEmpresaDireccion = new HashSet<terEmpresaDireccion>();
        }

        public short sDistrito { get; set; }
        public short sProvincia { get; set; }
        public string xDistrito { get; set; }
        public string xCodSunat { get; set; }

        public virtual isoProvincia sProvinciaNavigation { get; set; }
        [JsonIgnore] public virtual ICollection<orgEmpresaDireccion> orgEmpresaDireccion { get; set; }
        [JsonIgnore] public virtual ICollection<orgEmpresaSedeDireccion> orgEmpresaSedeDireccion { get; set; }
        [JsonIgnore] public virtual ICollection<perDireccion> perDireccion { get; set; }
        [JsonIgnore] public virtual ICollection<terEmpresaDireccion> terEmpresaDireccion { get; set; }
    }

}
