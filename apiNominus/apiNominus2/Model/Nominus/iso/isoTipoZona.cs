﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class isoTipoZona
    {
        public isoTipoZona()
        {
            orgEmpresaDireccion = new HashSet<orgEmpresaDireccion>();
            orgEmpresaSedeDireccion = new HashSet<orgEmpresaSedeDireccion>();
            perDireccion = new HashSet<perDireccion>();
            terEmpresaDireccion = new HashSet<terEmpresaDireccion>();
        }

        public byte bTipoZona { get; set; }
        public string xTipoZona { get; set; }
        public string xAbreviado { get; set; }

        [JsonIgnore] public virtual ICollection<orgEmpresaDireccion> orgEmpresaDireccion { get; set; }
        [JsonIgnore] public virtual ICollection<orgEmpresaSedeDireccion> orgEmpresaSedeDireccion { get; set; }
        [JsonIgnore] public virtual ICollection<perDireccion> perDireccion { get; set; }
        [JsonIgnore] public virtual ICollection<terEmpresaDireccion> terEmpresaDireccion { get; set; }
    }
}
