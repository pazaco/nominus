﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class isoClaseConcepto
    {
        public isoClaseConcepto()
        {
            isoSubClaseConcepto = new HashSet<isoSubClaseConcepto>();
            remConcepto = new HashSet<remConcepto>();
        }

        public byte bClaseConcepto { get; set; }
        public string xClaseConcepto { get; set; }

        [JsonIgnore] public virtual ICollection<isoSubClaseConcepto> isoSubClaseConcepto { get; set; }
        [JsonIgnore] public virtual ICollection<remConcepto> remConcepto { get; set; }
    }
}
