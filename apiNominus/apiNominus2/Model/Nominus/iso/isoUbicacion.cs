﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class isoUbicacion
    {
        public isoUbicacion()
        {
            perDireccion = new HashSet<perDireccion>();
        }

        public byte bUbicacion { get; set; }
        public string xUbicacion { get; set; }

        [JsonIgnore] public virtual ICollection<perDireccion> perDireccion { get; set; }
    }
}
