﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class isoPeriodicidad
    {
        public isoPeriodicidad()
        {
            isoTipoPlanilla = new HashSet<isoTipoPlanilla>();
            traContratoLaboral = new HashSet<traContratoLaboral>();
        }

        public byte bPeriodicidadRem { get; set; }
        public string xPeriodicidadRem { get; set; }
        public byte? bPeriodicidadSunat { get; set; }

        [JsonIgnore] public virtual ICollection<isoTipoPlanilla> isoTipoPlanilla { get; set; }
        [JsonIgnore] public virtual ICollection<traContratoLaboral> traContratoLaboral { get; set; }
    }
}
