﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class isoSituacionSunat
    {
        public isoSituacionSunat()
        {
            traContratoLaboral = new HashSet<traContratoLaboral>();
        }

        public byte bSituacion { get; set; }
        public string xSituacion { get; set; }

        [JsonIgnore] public virtual ICollection<traContratoLaboral> traContratoLaboral { get; set; }
    }
}
