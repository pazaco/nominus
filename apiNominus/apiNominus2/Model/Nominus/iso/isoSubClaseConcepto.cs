﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class isoSubClaseConcepto
    {
        public isoSubClaseConcepto()
        {
            remConceptoSubClase = new HashSet<remConceptoSubClase>();
        }

        public byte bSubClaseConcepto { get; set; }
        public byte bClaseConcepto { get; set; }
        public string xSubClaseConcepto { get; set; }

        public virtual isoClaseConcepto bClaseConceptoNavigation { get; set; }
        [JsonIgnore] public virtual ICollection<remConceptoSubClase> remConceptoSubClase { get; set; }
    }
}
