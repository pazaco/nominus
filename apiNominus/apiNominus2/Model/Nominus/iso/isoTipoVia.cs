﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class isoTipoVia
    {
        public isoTipoVia()
        {
            orgEmpresaDireccion = new HashSet<orgEmpresaDireccion>();
            orgEmpresaSedeDireccion = new HashSet<orgEmpresaSedeDireccion>();
            perDireccion = new HashSet<perDireccion>();
            terEmpresaDireccion = new HashSet<terEmpresaDireccion>();
        }

        public byte bTipoVia { get; set; }
        public string xTipoVia { get; set; }
        public string xAbreviado { get; set; }

        [JsonIgnore] public virtual ICollection<orgEmpresaDireccion> orgEmpresaDireccion { get; set; }
        [JsonIgnore] public virtual ICollection<orgEmpresaSedeDireccion> orgEmpresaSedeDireccion { get; set; }
        [JsonIgnore] public virtual ICollection<perDireccion> perDireccion { get; set; }
        [JsonIgnore] public virtual ICollection<terEmpresaDireccion> terEmpresaDireccion { get; set; }
    }
}
