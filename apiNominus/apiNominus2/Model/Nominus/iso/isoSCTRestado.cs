﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class isoSCTRestado
    {
        public byte bSCTRestado { get; set; }
        public string xSCTRestado { get; set; }
    }
}
