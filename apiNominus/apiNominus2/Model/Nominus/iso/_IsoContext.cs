﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace apiNominus2.Model
{
    public partial class NominusContext : DbContext
    {
        public virtual DbSet<isoCalculo> isoCalculo { get; set; }
        public virtual DbSet<isoCategoriaOcupacional> isoCategoriaOcupacional { get; set; }
        public virtual DbSet<isoClaseConcepto> isoClaseConcepto { get; set; }
        public virtual DbSet<isoDepartamento> isoDepartamento { get; set; }
        public virtual DbSet<isoDistrito> isoDistrito { get; set; }
        public virtual DbSet<isoDocSustentaVinculoFamiliar> isoDocSustentaVinculoFamiliar { get; set; }
        public virtual DbSet<isoEntidadFinanciera> isoEntidadFinanciera { get; set; }
        public virtual DbSet<isoInstitucionEducativa> isoInstitucionEducativa { get; set; }
        public virtual DbSet<isoMoneda> isoMoneda { get; set; }
        public virtual DbSet<isoMotivoBajaDerechohabiente> isoMotivoBajaDerechohabiente { get; set; }
        public virtual DbSet<isoMotivoCese> isoMotivoCese { get; set; }
        public virtual DbSet<isoNacionalidad> isoNacionalidad { get; set; }
        public virtual DbSet<isoOcupacionSunat> isoOcupacionSunat { get; set; }
        public virtual DbSet<isoPais> isoPais { get; set; }
        public virtual DbSet<isoPeriodicidad> isoPeriodicidad { get; set; }
        public virtual DbSet<isoProvincia> isoProvincia { get; set; }
        public virtual DbSet<isoRegimenLaboral> isoRegimenLaboral { get; set; }
        public virtual DbSet<isoRegimenPensionario> isoRegimenPensionario { get; set; }
        public virtual DbSet<isoRegimenPensionarioCodigo> isoRegimenPensionarioCodigo { get; set; }
        public virtual DbSet<isoRegimenPensionarioInfo> isoRegimenPensionarioInfo { get; set; }
        public virtual DbSet<isoSCTRestado> isoSCTRestado { get; set; }
        public virtual DbSet<isoSituacionSunat> isoSituacionSunat { get; set; }
        public virtual DbSet<isoSubClaseConcepto> isoSubClaseConcepto { get; set; }
        public virtual DbSet<isoTipoActividad> isoTipoActividad { get; set; }
        public virtual DbSet<isoTipoCalculo> isoTipoCalculo { get; set; }
        public virtual DbSet<isoTipoComprobante> isoTipoComprobante { get; set; }
        public virtual DbSet<isoTipoConcepto> isoTipoConcepto { get; set; }
        public virtual DbSet<isoTipoContrato> isoTipoContrato { get; set; }
        public virtual DbSet<isoTipoCtaBancaria> isoTipoCtaBancaria { get; set; }
        public virtual DbSet<isoTipoDocIdentidad> isoTipoDocIdentidad { get; set; }
        public virtual DbSet<isoTipoPago> isoTipoPago { get; set; }
        public virtual DbSet<isoTipoPlanilla> isoTipoPlanilla { get; set; }
        public virtual DbSet<isoTipoSuspensionRL> isoTipoSuspensionRL { get; set; }
        public virtual DbSet<isoTipoTrabajador> isoTipoTrabajador { get; set; }
        public virtual DbSet<isoTipoVia> isoTipoVia { get; set; }
        public virtual DbSet<isoTipoZona> isoTipoZona { get; set; }
        public virtual DbSet<isoUbicacion> isoUbicacion { get; set; }
        public virtual DbSet<isoUnidadMedida> isoUnidadMedida { get; set; }
        public virtual DbSet<isoVinculoFamiliar> isoVinculoFamiliar { get; set; }

        partial void OnModelCreatingPartialIso(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<isoCalculo>(entity =>
            {
                entity.HasKey(e => e.bCalculo);

                entity.Property(e => e.bCalculo).HasComment("1:Adelanto, 2:Boleta, 3:Liquidación, 4:Vacaciones, 5:Gratificaciones, 6:CTS, 7:Utilidades");

                entity.Property(e => e.bTipoCalculo)
                    .HasDefaultValueSql("((2))")
                    .HasComment("1:Remunerar, 2:Provisionar");

                entity.Property(e => e.xCalculo)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.HasOne(d => d.bTipoCalculoNavigation)
                    .WithMany(p => p.isoCalculo)
                    .HasForeignKey(d => d.bTipoCalculo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_isoCalculo_isoTipoCalculo");
            });
            modelBuilder.Entity<isoCategoriaOcupacional>(entity =>
            {
                entity.HasKey(e => e.bCategoriaOcupacional);

                entity.Property(e => e.xCategoriaOcupacional)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<isoClaseConcepto>(entity =>
            {
                entity.HasKey(e => e.bClaseConcepto);

                entity.Property(e => e.xClaseConcepto)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<isoDepartamento>(entity =>
            {
                entity.HasKey(e => e.sDepartamento);

                entity.Property(e => e.xCodSunat)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.xDepartamento)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.sPaisNavigation)
                    .WithMany(p => p.isoDepartamento)
                    .HasForeignKey(d => d.sPais)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_isoDepartamento_isoPais");
            });
            modelBuilder.Entity<isoDistrito>(entity =>
            {
                entity.HasKey(e => e.sDistrito);

                entity.Property(e => e.xCodSunat)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.xDistrito)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.sProvinciaNavigation)
                    .WithMany(p => p.isoDistrito)
                    .HasForeignKey(d => d.sProvincia)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_isoDistrito_isoProvincia");
            });
            modelBuilder.Entity<isoDocSustentaVinculoFamiliar>(entity =>
            {
                entity.HasKey(e => e.bDocSustentaVinculo)
                    .HasName("PK_isoFamiliarDocSustentaVinculo");

                entity.Property(e => e.bDocSustentaVinculo).HasComment("(Tabla 27) Documento que sustenta vínculo familiar");

                entity.Property(e => e.xDocSustentaVinculo)
                    .IsRequired()
                    .HasMaxLength(40)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<isoEntidadFinanciera>(entity =>
            {
                entity.HasKey(e => e.sEntidadFinanciera);

                entity.Property(e => e.sEntidadFinanciera).ValueGeneratedNever();

                entity.Property(e => e.xAbreviado)
                    .IsRequired()
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.xEntidadFinanciera)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<isoInstitucionEducativa>(entity =>
            {
                entity.HasKey(e => e.iInstitucionEducativa);

                entity.Property(e => e.iInstitucionEducativa).ValueGeneratedNever();

                entity.Property(e => e.cInstitucionEducativa)
                    .HasMaxLength(9)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.xInstitucionEducativa)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<isoMoneda>(entity =>
            {
                entity.HasKey(e => e.sMoneda);

                entity.Property(e => e.sMoneda).ValueGeneratedNever();

                entity.Property(e => e.xCodigo)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.xMoneda)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.xMonedaPlural)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.xSimbolo)
                    .IsRequired()
                    .HasMaxLength(3);
            });
            modelBuilder.Entity<isoMotivoBajaDerechohabiente>(entity =>
            {
                entity.HasKey(e => e.bMotivoBaja);

                entity.Property(e => e.bMotivoBaja).HasComment("(Tabla 20) Motivo de baja como derechohabiente");

                entity.Property(e => e.xMotivoBaja)
                    .IsRequired()
                    .HasMaxLength(35)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<isoMotivoCese>(entity =>
            {
                entity.HasKey(e => e.bMotivoCese)
                    .HasName("PK_traContratoCeseMotivo");

                entity.Property(e => e.xMotivoCese)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<isoNacionalidad>(entity =>
            {
                entity.HasKey(e => e.sNacionalidad);

                entity.Property(e => e.sNacionalidad)
                    .ValueGeneratedNever()
                    .HasComment("Código de nacionalidad de T-REGISTROS tabla 4");

                entity.Property(e => e.sPais).HasComment("");

                entity.Property(e => e.xCiudadano)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.xNacionalidad)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<isoOcupacionSunat>(entity =>
            {
                entity.HasKey(e => e.cOcupacion)
                    .HasName("PK_isoCargoSunat");

                entity.Property(e => e.cOcupacion)
                    .HasMaxLength(6)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.xOcupacion)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<isoPais>(entity =>
            {
                entity.HasKey(e => e.sPais)
                    .HasName("PK_isoPais_1");

                entity.Property(e => e.sPais).ValueGeneratedNever();

                entity.Property(e => e.cPais)
                    .IsRequired()
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.cPais3)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.xPais)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<isoPeriodicidad>(entity =>
            {
                entity.HasKey(e => e.bPeriodicidadRem);

                entity.Property(e => e.xPeriodicidadRem)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<isoProvincia>(entity =>
            {
                entity.HasKey(e => e.sProvincia);

                entity.Property(e => e.xCodSunat)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.xProvincia)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.sDepartamentoNavigation)
                    .WithMany(p => p.isoProvincia)
                    .HasForeignKey(d => d.sDepartamento)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_isoProvincia_isoDepartamento");
            });
            modelBuilder.Entity<isoRegimenLaboral>(entity =>
            {
                entity.HasKey(e => e.bRegimenLaboral);

                entity.Property(e => e.xAbreviado)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.xRegimenLaboral)
                    .IsRequired()
                    .HasMaxLength(60)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<isoRegimenPensionario>(entity =>
            {
                entity.HasKey(e => e.bRegimenPensionario)
                    .HasName("PK_traAFP");

                entity.Property(e => e.xRegimenPensionario)
                    .IsRequired()
                    .HasMaxLength(60)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<isoRegimenPensionarioCodigo>(entity =>
            {
                entity.HasKey(e => e.bRegimenPensionario);

                entity.Property(e => e.xCodigo)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.HasOne(d => d.bRegimenPensionarioNavigation)
                    .WithOne(p => p.isoRegimenPensionarioCodigo)
                    .HasForeignKey<isoRegimenPensionarioCodigo>(d => d.bRegimenPensionario)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_isoRegimenPensionarioCodigo_isoRegimenPensionario");
            });
            modelBuilder.Entity<isoRegimenPensionarioInfo>(entity =>
            {
                entity.HasKey(e => new { e.bRegimenPensionario, e.dDesde })
                    .HasName("PK_isoAFPinfo");

                entity.Property(e => e.dDesde).HasColumnType("date");

                entity.Property(e => e.rAporte)
                    .HasColumnType("decimal(5, 2)")
                    .HasDefaultValueSql("((10.00))");

                entity.Property(e => e.rComisionFlujo).HasColumnType("decimal(5, 2)");

                entity.Property(e => e.rComisionMixta).HasColumnType("decimal(5, 2)");

                entity.Property(e => e.rPrima).HasColumnType("decimal(5, 2)");

                entity.HasOne(d => d.bRegimenPensionarioNavigation)
                    .WithMany(p => p.isoRegimenPensionarioInfo)
                    .HasForeignKey(d => d.bRegimenPensionario)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_isoAFPinfo_isoAFP");
            });
            modelBuilder.Entity<isoSCTRestado>(entity =>
            {
                entity.HasKey(e => e.bSCTRestado);
                entity.Property(e => e.xSCTRestado)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<isoSituacionSunat>(entity =>
            {
                entity.HasKey(e => e.bSituacion);

                entity.Property(e => e.xSituacion)
                    .IsRequired()
                    .HasMaxLength(75)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<isoSubClaseConcepto>(entity =>
            {
                entity.HasKey(e => e.bSubClaseConcepto);

                entity.Property(e => e.xSubClaseConcepto)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.HasOne(d => d.bClaseConceptoNavigation)
                    .WithMany(p => p.isoSubClaseConcepto)
                    .HasForeignKey(d => d.bClaseConcepto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_isoSubClaseConcepto_isoClaseConcepto");
            });
            modelBuilder.Entity<isoTipoActividad>(entity =>
            {
                entity.HasKey(e => e.iTipoActividad);

                entity.Property(e => e.iTipoActividad)
                    .ValueGeneratedNever()
                    .HasComment("(Tabla 1) TIPO DE ACTIVIDAD (Se usa en Estructura 2 y 3)");

                entity.Property(e => e.xTipoActividad)
                    .IsRequired()
                    .HasMaxLength(40)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<isoTipoCalculo>(entity =>
            {
                entity.HasKey(e => e.bTipoCalculo)
                    .HasName("PK_remTipoCalculo");

                entity.Property(e => e.bTipoCalculo).HasComment("1:Remunerar, 2:Provisionar");

                entity.Property(e => e.xTipoCalculo)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<isoTipoComprobante>(entity =>
            {
                entity.HasKey(e => e.bTipoComprobante);

                entity.Property(e => e.bTipoComprobante).HasComment("1:R=RECIBO POR HONORARIOS,2:N=NOTA DE CRÉDITO,3:D=DIETA,4:O=OTRO COMPROBANTE");

                entity.Property(e => e.xCodigo)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.xTipoComprobante)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<isoTipoConcepto>(entity =>
            {
                entity.HasKey(e => e.bTipoConcepto)
                    .HasName("PK_remTipoConcepto");

                entity.Property(e => e.bTipoConcepto).HasComment("1:BD, 2:Manual, 3:Fijo, 4:Asistencia (mov.), 5:Préstamo, 6:Formula, 7:Historico, 8:Acumulador, 9:Importe");

                entity.Property(e => e.xTipoConcepto)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasComment("1:BD, 2:Manual, 3:Fijo, 4:Asistencia (mov.), 5:Préstamo, 6:Formula, 7:Historico, 8:Acumulador, 9:Importe");
            });
            modelBuilder.Entity<isoTipoContrato>(entity =>
            {
                entity.HasKey(e => e.bTipoContrato);

                entity.Property(e => e.xTipoContrato)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<isoTipoCtaBancaria>(entity =>
            {
                entity.HasKey(e => e.bTipoCtaBancaria);

                entity.Property(e => e.xTipoCtaBancaria)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<isoTipoDocIdentidad>(entity =>
            {
                entity.HasKey(e => e.bTipoDocIdentidad);

                entity.Property(e => e.xTip)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.xTipoDocIdentidad)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<isoTipoPago>(entity =>
            {
                entity.HasKey(e => e.bTipoPago);

                entity.Property(e => e.xTipoPago)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<isoTipoPlanilla>(entity =>
            {
                entity.HasKey(e => e.bTipoPlanilla);

                entity.Property(e => e.xTipoPlanilla)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.HasOne(d => d.bPeriodicidadRemNavigation)
                    .WithMany(p => p.isoTipoPlanilla)
                    .HasForeignKey(d => d.bPeriodicidadRem)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_isoTipoPlanilla_isoPeriodicidad");
            });
            modelBuilder.Entity<isoTipoSuspensionRL>(entity =>
            {
                entity.HasKey(e => e.bTipoSuspensionRL);

                entity.Property(e => e.bTipoSuspensionRL).HasComment("Código de la SUNAT, agregar formato 0#");

                entity.Property(e => e.xTipoSuspensionRL)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<isoTipoTrabajador>(entity =>
            {
                entity.HasKey(e => e.bTipoTrabajador);

                entity.Property(e => e.xTipoTrabajador)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<isoTipoVia>(entity =>
            {
                entity.HasKey(e => e.bTipoVia);

                entity.Property(e => e.xAbreviado)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.xTipoVia)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<isoTipoZona>(entity =>
            {
                entity.HasKey(e => e.bTipoZona);

                entity.Property(e => e.xAbreviado)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.xTipoZona)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<isoUbicacion>(entity =>
            {
                entity.HasKey(e => e.bUbicacion);

                entity.Property(e => e.bUbicacion)
                    .ValueGeneratedOnAdd()
                    .HasComment("Casa, Oficina, Familiar, Vecino, Vacaciones, etc.");

                entity.Property(e => e.xUbicacion)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<isoUnidadMedida>(entity =>
            {
                entity.HasKey(e => e.sUnidadMedida);

                entity.Property(e => e.xAbreviado)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.xUnidadMedida)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<isoVinculoFamiliar>(entity =>
            {
                entity.HasKey(e => e.bVinculoFamiliar);

                entity.Property(e => e.bVinculoFamiliar).HasComment("(Tabla 19) Vínculo familiar");

                entity.Property(e => e.xVinculoFamiliar)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });
       }
    }
}
