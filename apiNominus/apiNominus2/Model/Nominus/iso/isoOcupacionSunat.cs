﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class isoOcupacionSunat
    {
        public isoOcupacionSunat()
        {
            orgDiccCargoSunat = new HashSet<orgDiccCargoSunat>();
        }

        public string cOcupacion { get; set; }
        public string xOcupacion { get; set; }

        [JsonIgnore] public virtual ICollection<orgDiccCargoSunat> orgDiccCargoSunat { get; set; }
    }
}
