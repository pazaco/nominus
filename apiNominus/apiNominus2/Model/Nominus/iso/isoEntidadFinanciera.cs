﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class isoEntidadFinanciera
    {
        public isoEntidadFinanciera()
        {
            orgEmpresaCtaBancaria = new HashSet<orgEmpresaCtaBancaria>();
            perCtaBancaria = new HashSet<perCtaBancaria>();
        }

        public short sEntidadFinanciera { get; set; }
        public string xEntidadFinanciera { get; set; }
        public string xAbreviado { get; set; }

        [JsonIgnore] public virtual ICollection<orgEmpresaCtaBancaria> orgEmpresaCtaBancaria { get; set; }
        [JsonIgnore] public virtual ICollection<perCtaBancaria> perCtaBancaria { get; set; }
    }
}
