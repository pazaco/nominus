﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class isoCategoriaOcupacional
    {
        public isoCategoriaOcupacional()
        {
            traContratoLaboral = new HashSet<traContratoLaboral>();
        }

        public byte bCategoriaOcupacional { get; set; }
        public string xCategoriaOcupacional { get; set; }
        public bool bSectorPrivado { get; set; }
        public bool bSectorPublico { get; set; }
        public bool bSectorOtros { get; set; }

        [JsonIgnore] public virtual ICollection<traContratoLaboral> traContratoLaboral { get; set; }
    }
}
