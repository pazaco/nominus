﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class isoTipoPago
    {
        public isoTipoPago()
        {
            traContratoLaboral = new HashSet<traContratoLaboral>();
        }

        public byte bTipoPago { get; set; }
        public string xTipoPago { get; set; }

        [JsonIgnore] public virtual ICollection<traContratoLaboral> traContratoLaboral { get; set; }
    }
}
