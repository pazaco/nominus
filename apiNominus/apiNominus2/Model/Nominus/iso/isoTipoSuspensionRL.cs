﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class isoTipoSuspensionRL
    {
        public isoTipoSuspensionRL()
        {
            remConceptoTipoSuspensionRL = new HashSet<remConceptoTipoSuspensionRL>();
        }

        public byte bTipoSuspensionRL { get; set; }
        public string xTipoSuspensionRL { get; set; }
        public bool bImperfecta { get; set; }

        [JsonIgnore] public virtual ICollection<remConceptoTipoSuspensionRL> remConceptoTipoSuspensionRL { get; set; }
    }
}
