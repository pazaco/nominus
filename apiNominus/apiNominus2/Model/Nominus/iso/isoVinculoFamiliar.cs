﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class isoVinculoFamiliar
    {
        public isoVinculoFamiliar()
        {
            perFamiliar = new HashSet<perFamiliar>();
        }

        public byte bVinculoFamiliar { get; set; }
        public string xVinculoFamiliar { get; set; }

        [JsonIgnore] public virtual ICollection<perFamiliar> perFamiliar { get; set; }
    }
}
