﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class isoTipoPlanilla
    {
        public isoTipoPlanilla()
        {
            remPlanilla = new HashSet<remPlanilla>();
        }

        public byte bTipoPlanilla { get; set; }
        public string xTipoPlanilla { get; set; }
        public byte bPeriodicidadRem { get; set; }

        public virtual isoPeriodicidad bPeriodicidadRemNavigation { get; set; }
        [JsonIgnore] public virtual ICollection<remPlanilla> remPlanilla { get; set; }
    }
}
