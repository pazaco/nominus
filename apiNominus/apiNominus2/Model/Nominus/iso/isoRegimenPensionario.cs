﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class isoRegimenPensionario
    {
        public isoRegimenPensionario()
        {
            isoRegimenPensionarioInfo = new HashSet<isoRegimenPensionarioInfo>();
            remConceptoCalculoAfpCuentas = new HashSet<remConceptoCalculoAfpCuentas>();
            traTrabajador = new HashSet<traTrabajador>();
        }

        public byte bRegimenPensionario { get; set; }
        public string xRegimenPensionario { get; set; }
        public bool bAFP { get; set; }
        public bool bSectorPrivado { get; set; }
        public bool bSectorPublico { get; set; }
        public bool bSectorOtros { get; set; }

        public virtual isoRegimenPensionarioCodigo isoRegimenPensionarioCodigo { get; set; }
        [JsonIgnore] public virtual ICollection<isoRegimenPensionarioInfo> isoRegimenPensionarioInfo { get; set; }
        [JsonIgnore] public virtual ICollection<remConceptoCalculoAfpCuentas> remConceptoCalculoAfpCuentas { get; set; }
        [JsonIgnore] public virtual ICollection<traTrabajador> traTrabajador { get; set; }
    }
}
