﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class isoTipoCtaBancaria
    {
        public isoTipoCtaBancaria()
        {
            perCtaBancaria = new HashSet<perCtaBancaria>();
        }

        public byte bTipoCtaBancaria { get; set; }
        public string xTipoCtaBancaria { get; set; }

        [JsonIgnore] public virtual ICollection<perCtaBancaria> perCtaBancaria { get; set; }
    }
}
