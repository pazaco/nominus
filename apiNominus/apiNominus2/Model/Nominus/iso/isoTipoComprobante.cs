﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class isoTipoComprobante
    {
        public isoTipoComprobante()
        {
            traRxH = new HashSet<traRxH>();
        }

        public byte bTipoComprobante { get; set; }
        public string xTipoComprobante { get; set; }
        public string xCodigo { get; set; }

        [JsonIgnore] public virtual ICollection<traRxH> traRxH { get; set; }
    }
}
