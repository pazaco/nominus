﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class orgEmpresaPlanEPS
    {
        public short sEmpresaPlanEPS { get; set; }
        public short sEmpresa { get; set; }
        public short sPlanEPS { get; set; }

        public virtual orgEmpresa sEmpresaNavigation { get; set; }
        public virtual sysPlanEPS sPlanEPSNavigation { get; set; }
    }
}
