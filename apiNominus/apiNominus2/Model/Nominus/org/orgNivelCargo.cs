﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class orgNivelCargo
    {
        public orgNivelCargo()
        {
            InversebNivelPadreNavigation = new HashSet<orgNivelCargo>();
            orgCargo = new HashSet<orgCargo>();
        }

        public byte bNivel { get; set; }
        public string xNivel { get; set; }
        public byte? bNivelPadre { get; set; }

        public virtual orgNivelCargo bNivelPadreNavigation { get; set; }
        [JsonIgnore] public virtual ICollection<orgNivelCargo> InversebNivelPadreNavigation { get; set; }
        [JsonIgnore] public virtual ICollection<orgCargo> orgCargo { get; set; }
    }
}
