﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class orgEmpresaImagen
    {
        public short sEmpresaImagen { get; set; }
        public short sEmpresa { get; set; }
        public byte bTipoImagen { get; set; }
        public string jImagen { get; set; }
        public byte[] iImagen { get; set; }

        public virtual orgEmpresa sEmpresaNavigation { get; set; }
    }
}
