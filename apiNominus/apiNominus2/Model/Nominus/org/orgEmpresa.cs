﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class orgEmpresa
    {
        public orgEmpresa()
        {
            orgEmpresaCalculoCtaContableNeto = new HashSet<orgEmpresaCalculoCtaContableNeto>();
            orgEmpresaCentroCosto = new HashSet<orgEmpresaCentroCosto>();
            orgEmpresaCtaBancaria = new HashSet<orgEmpresaCtaBancaria>();
            orgEmpresaCtaContable = new HashSet<orgEmpresaCtaContable>();
            orgEmpresaImagen = new HashSet<orgEmpresaImagen>();
            orgEmpresaPlanEPS = new HashSet<orgEmpresaPlanEPS>();
            orgEmpresaSede = new HashSet<orgEmpresaSede>();
            orgEmpresaTelefono = new HashSet<orgEmpresaTelefono>();
            perUsuarioAccesoEmpresa = new HashSet<perUsuarioAccesoEmpresa>();
            remConceptoAsistenciaEmpresaPlanilla = new HashSet<remConceptoAsistenciaEmpresaPlanilla>();
            remConceptoCalculo = new HashSet<remConceptoCalculo>();
            sysMontoEmpresa = new HashSet<sysMontoEmpresa>();
            traContrato = new HashSet<traContrato>();
        }

        public short sEmpresa { get; set; }
        public string cRUC { get; set; }
        public string xRazonSocial { get; set; }
        public string xNombreComercial { get; set; }
        public string xAbreviado { get; set; }
        public byte bRegimenLaboral { get; set; }
        public string jAppConfig { get; set; }

        public virtual isoRegimenLaboral bRegimenLaboralNavigation { get; set; }
        public virtual orgEmpresaActividad orgEmpresaActividad { get; set; }
        public virtual orgEmpresaDireccion orgEmpresaDireccion { get; set; }
        public virtual orgEmpresaPeriodo orgEmpresaPeriodo { get; set; }
        public virtual orgEmpresaRepLegal orgEmpresaRepLegal { get; set; }
        [JsonIgnore] public virtual ICollection<orgEmpresaCalculoCtaContableNeto> orgEmpresaCalculoCtaContableNeto { get; set; }
        [JsonIgnore] public virtual ICollection<orgEmpresaCentroCosto> orgEmpresaCentroCosto { get; set; }
        [JsonIgnore] public virtual ICollection<orgEmpresaCtaBancaria> orgEmpresaCtaBancaria { get; set; }
        [JsonIgnore] public virtual ICollection<orgEmpresaCtaContable> orgEmpresaCtaContable { get; set; }
        [JsonIgnore] public virtual ICollection<orgEmpresaImagen> orgEmpresaImagen { get; set; }
        [JsonIgnore] public virtual ICollection<orgEmpresaPlanEPS> orgEmpresaPlanEPS { get; set; }
        [JsonIgnore] public virtual ICollection<orgEmpresaSede> orgEmpresaSede { get; set; }
        [JsonIgnore] public virtual ICollection<orgEmpresaTelefono> orgEmpresaTelefono { get; set; }
        [JsonIgnore] public virtual ICollection<perUsuarioAccesoEmpresa> perUsuarioAccesoEmpresa { get; set; }
        [JsonIgnore] public virtual ICollection<remConceptoAsistenciaEmpresaPlanilla> remConceptoAsistenciaEmpresaPlanilla { get; set; }
        [JsonIgnore] public virtual ICollection<remConceptoCalculo> remConceptoCalculo { get; set; }
        [JsonIgnore] public virtual ICollection<sysMontoEmpresa> sysMontoEmpresa { get; set; }
        [JsonIgnore] public virtual ICollection<traContrato> traContrato { get; set; }
    }
}
