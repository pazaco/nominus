﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class orgCargoFemenino
    {
        public short sCargo { get; set; }
        public string xCargo { get; set; }

        public virtual orgCargo sCargoNavigation { get; set; }
    }
}
