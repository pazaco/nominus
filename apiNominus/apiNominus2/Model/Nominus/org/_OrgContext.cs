﻿using Microsoft.EntityFrameworkCore;

#nullable disable

namespace apiNominus2.Model
{
    public partial class NominusContext : DbContext
    {
        public virtual DbSet<orgArea> orgArea { get; set; }
        public virtual DbSet<orgCargo> orgCargo { get; set; }
        public virtual DbSet<orgCargoFemenino> orgCargoFemenino { get; set; }
        public virtual DbSet<orgCargoFuncion> orgCargoFuncion { get; set; }
        public virtual DbSet<orgCargoServicio> orgCargoServicio { get; set; }
        public virtual DbSet<orgCentroCosto> orgCentroCosto { get; set; }
        public virtual DbSet<orgCentroCostoCodigo> orgCentroCostoCodigo { get; set; }
        public virtual DbSet<orgCentroCostoRelacion> orgCentroCostoRelacion { get; set; }
        public virtual DbSet<orgDiccCargoSunat> orgDiccCargoSunat { get; set; }
        public virtual DbSet<orgEmpresa> orgEmpresa { get; set; }
        public virtual DbSet<orgEmpresaActividad> orgEmpresaActividad { get; set; }
        public virtual DbSet<orgEmpresaCalculoCtaContableNeto> orgEmpresaCalculoCtaContableNeto { get; set; }
        public virtual DbSet<orgEmpresaCentroCosto> orgEmpresaCentroCosto { get; set; }
        public virtual DbSet<orgEmpresaCtaBancaria> orgEmpresaCtaBancaria { get; set; }
        public virtual DbSet<orgEmpresaCtaContable> orgEmpresaCtaContable { get; set; }
        public virtual DbSet<orgEmpresaDireccion> orgEmpresaDireccion { get; set; }
        public virtual DbSet<orgEmpresaImagen> orgEmpresaImagen { get; set; }
        public virtual DbSet<orgEmpresaPeriodo> orgEmpresaPeriodo { get; set; }
        public virtual DbSet<orgEmpresaPlanEPS> orgEmpresaPlanEPS { get; set; }
        public virtual DbSet<orgEmpresaRepLegal> orgEmpresaRepLegal { get; set; }
        public virtual DbSet<orgEmpresaSede> orgEmpresaSede { get; set; }
        public virtual DbSet<orgEmpresaSedeDireccion> orgEmpresaSedeDireccion { get; set; }
        public virtual DbSet<orgEmpresaTelefono> orgEmpresaTelefono { get; set; }
        public virtual DbSet<orgEstablecimiento> orgEstablecimiento { get; set; }
        public virtual DbSet<orgEstructuraSalarial> orgEstructuraSalarial { get; set; }
        public virtual DbSet<orgEstructuraSalarialDet> orgEstructuraSalarialDet { get; set; }
        public virtual DbSet<orgGeoEstablecimiento> orgGeoEstablecimiento { get; set; }
        public virtual DbSet<orgGrupo> orgGrupo { get; set; }
        public virtual DbSet<orgNivelCargo> orgNivelCargo { get; set; }
        public virtual DbSet<orgSede> orgSede { get; set; }

        partial void OnModelCreatingPartialOrg(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<orgArea>(entity =>
            {
                entity.HasKey(e => e.sArea);

                entity.Property(e => e.xArea)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.sAreaPadreNavigation)
                    .WithMany(p => p.InversesAreaPadreNavigation)
                    .HasForeignKey(d => d.sAreaPadre)
                    .HasConstraintName("FK_orgArea_orgArea");
            });
            modelBuilder.Entity<orgCargo>(entity =>
            {
                entity.HasKey(e => e.sCargo);

                entity.Property(e => e.xCargo)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.bNivelCargoNavigation)
                    .WithMany(p => p.orgCargo)
                    .HasForeignKey(d => d.bNivelCargo)
                    .HasConstraintName("FK_orgCargo_orgNivelCargo");

                entity.HasOne(d => d.sAreaNavigation)
                    .WithMany(p => p.orgCargo)
                    .HasForeignKey(d => d.sArea)
                    .HasConstraintName("FK_orgCargo_orgArea");
            });
            modelBuilder.Entity<orgCargoFemenino>(entity =>
            {
                entity.HasKey(e => e.sCargo);

                entity.Property(e => e.sCargo).ValueGeneratedNever();

                entity.Property(e => e.xCargo)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.sCargoNavigation)
                    .WithOne(p => p.orgCargoFemenino)
                    .HasForeignKey<orgCargoFemenino>(d => d.sCargo)
                    .HasConstraintName("FK_orgCargoFemenino_orgCargo");
            });
            modelBuilder.Entity<orgCargoFuncion>(entity =>
            {
                entity.HasKey(e => e.iCargoFuncion);

                entity.Property(e => e.xFuncion)
                    .IsRequired()
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.HasOne(d => d.sCargoNavigation)
                    .WithMany(p => p.orgCargoFuncion)
                    .HasForeignKey(d => d.sCargo)
                    .HasConstraintName("FK_orgCargoFuncion_orgCargo");
            });
            modelBuilder.Entity<orgCargoServicio>(entity =>
            {
                entity.HasKey(e => e.sCargo);

                entity.Property(e => e.sCargo).ValueGeneratedNever();

                entity.Property(e => e.xServicios)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.sCargoNavigation)
                    .WithOne(p => p.orgCargoServicio)
                    .HasForeignKey<orgCargoServicio>(d => d.sCargo)
                    .HasConstraintName("FK_orgCargoServicio_orgCargo");
            });
            modelBuilder.Entity<orgCentroCosto>(entity =>
            {
                entity.HasKey(e => e.sCentroCosto);

                entity.Property(e => e.xCentroCosto)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

            });
            modelBuilder.Entity<orgCentroCostoCodigo>(entity =>
            {
                entity.HasKey(e => e.sCentroCosto);

                entity.Property(e => e.sCentroCosto).ValueGeneratedNever();

                entity.Property(e => e.xCodigo)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.sCentroCostoNavigation)
                    .WithOne(p => p.orgCentroCostoCodigo)
                    .HasForeignKey<orgCentroCostoCodigo>(d => d.sCentroCosto)
                    .HasConstraintName("FK_orgCentroCostoCodigo_orgCentroCosto");
            });
            modelBuilder.Entity<orgCentroCostoRelacion>(entity =>
            {
                entity.HasKey(e => new { e.sCentroCosto, e.sCentroCostoRelacion });

                entity.Property(e => e.sCentroCostoRelacion).HasComment("Código del nivel superior al actual con el que se relaciona");

                entity.HasOne(d => d.sCentroCostoNavigation)
                    .WithMany(p => p.orgCentroCostoRelacionsCentroCostoNavigation)
                    .HasForeignKey(d => d.sCentroCosto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_orgCentroCostoRelacion_orgCentroCosto");

                entity.HasOne(d => d.sCentroCostoRelacionNavigation)
                    .WithMany(p => p.orgCentroCostoRelacionsCentroCostoRelacionNavigation)
                    .HasForeignKey(d => d.sCentroCostoRelacion)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_orgCentroCostoRelacion_orgCentroCosto1");
            });
            modelBuilder.Entity<orgDiccCargoSunat>(entity =>
            {
                entity.HasKey(e => new { e.sCargo, e.cOcupacion });

                entity.Property(e => e.cOcupacion)
                    .HasMaxLength(6)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.HasOne(d => d.cOcupacionNavigation)
                    .WithMany(p => p.orgDiccCargoSunat)
                    .HasForeignKey(d => d.cOcupacion)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_orgDiccCargoSunat_isoOcupacionSunat");

                entity.HasOne(d => d.sCargoNavigation)
                    .WithMany(p => p.orgDiccCargoSunat)
                    .HasForeignKey(d => d.sCargo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_orgDiccCargoSunat_orgCargo");
            });
            modelBuilder.Entity<orgEmpresa>(entity =>
            {
                entity.HasKey(e => e.sEmpresa);

                entity.HasIndex(e => e.cRUC, "IX_orgEmpresa")
                    .IsUnique();

                entity.Property(e => e.bRegimenLaboral)
                    .HasDefaultValueSql("((1))")
                    .HasComment("(Tabla 33) 01:Privado general - DL. N°728");

                entity.Property(e => e.cRUC)
                    .IsRequired()
                    .HasMaxLength(11)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.jAppConfig).IsUnicode(false);

                entity.Property(e => e.xAbreviado)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.xNombreComercial)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.xRazonSocial)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.HasOne(d => d.bRegimenLaboralNavigation)
                    .WithMany(p => p.orgEmpresa)
                    .HasForeignKey(d => d.bRegimenLaboral)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_orgEmpresa_isoRegimenLaboral");
            });
            modelBuilder.Entity<orgEmpresaActividad>(entity =>
            {
                entity.HasKey(e => e.sEmpresa);

                entity.Property(e => e.sEmpresa).ValueGeneratedNever();

                entity.Property(e => e.dInicio).HasColumnType("date");

                entity.Property(e => e.xActividad).IsUnicode(false);

                entity.HasOne(d => d.sEmpresaNavigation)
                    .WithOne(p => p.orgEmpresaActividad)
                    .HasForeignKey<orgEmpresaActividad>(d => d.sEmpresa)
                    .HasConstraintName("FK_orgEmpresaActividad_orgEmpresa");
            });
            modelBuilder.Entity<orgEmpresaCalculoCtaContableNeto>(entity =>
            {
                entity.HasKey(e => new { e.sEmpresa, e.bCalculo, e.bPlanilla });

                entity.Property(e => e.bPlanilla).HasDefaultValueSql("((1))");

                entity.HasOne(d => d.bCalculoNavigation)
                    .WithMany(p => p.orgEmpresaCalculoCtaContableNeto)
                    .HasForeignKey(d => d.bCalculo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_orgEmpresaCalculoCtaContableNeto_isoCalculo");

                entity.HasOne(d => d.bPlanillaNavigation)
                    .WithMany(p => p.orgEmpresaCalculoCtaContableNeto)
                    .HasForeignKey(d => d.bPlanilla)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_orgEmpresaCalculoCtaContableNeto_remPlanilla");

                entity.HasOne(d => d.sCuentaNavigation)
                    .WithMany(p => p.orgEmpresaCalculoCtaContableNeto)
                    .HasForeignKey(d => d.sCuenta)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_orgEmpresaCalculoCtaContableNeto_orgEmpresaCtaContable");

                entity.HasOne(d => d.sEmpresaNavigation)
                    .WithMany(p => p.orgEmpresaCalculoCtaContableNeto)
                    .HasForeignKey(d => d.sEmpresa)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_orgEmpresaCalculoCtaContableNeto_orgEmpresa");
            });
            modelBuilder.Entity<orgEmpresaCentroCosto>(entity =>
            {
                entity.HasKey(e => e.iEmpresaCentroCosto);

                entity.HasIndex(e => new { e.sEmpresa, e.sCentroCosto }, "IX_orgEmpresaCentroCosto")
                    .IsUnique();

                entity.Property(e => e.xCodigo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.xCuenta)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.HasOne(d => d.sCentroCostoNavigation)
                    .WithMany(p => p.orgEmpresaCentroCosto)
                    .HasForeignKey(d => d.sCentroCosto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_orgEmpresaCentroCosto_orgCentroCosto");

                entity.HasOne(d => d.sEmpresaNavigation)
                    .WithMany(p => p.orgEmpresaCentroCosto)
                    .HasForeignKey(d => d.sEmpresa)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_orgEmpresaCentroCosto_orgEmpresa");
            });
            modelBuilder.Entity<orgEmpresaCtaBancaria>(entity =>
            {
                entity.HasKey(e => e.sEmpresaCtaBancaria);

                entity.Property(e => e.bTipoCtaBancaria).HasDefaultValueSql("((1))");

                entity.Property(e => e.sMoneda).HasDefaultValueSql("((604))");

                entity.Property(e => e.xCtaBancaria)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.HasOne(d => d.sEmpresaNavigation)
                    .WithMany(p => p.orgEmpresaCtaBancaria)
                    .HasForeignKey(d => d.sEmpresa)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_orgEmpresaCtaBancaria_orgEmpresa");

                entity.HasOne(d => d.sEntidadFinancieraNavigation)
                    .WithMany(p => p.orgEmpresaCtaBancaria)
                    .HasForeignKey(d => d.sEntidadFinanciera)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_orgEmpresaCtaBancaria_isoEntidadFinanciera");

                entity.HasOne(d => d.sMonedaNavigation)
                    .WithMany(p => p.orgEmpresaCtaBancaria)
                    .HasForeignKey(d => d.sMoneda)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_orgEmpresaCtaBancaria_isoMoneda");
            });
            modelBuilder.Entity<orgEmpresaCtaContable>(entity =>
            {
                entity.HasKey(e => e.sCuenta);

                entity.HasIndex(e => new { e.sEmpresa, e.xCuenta }, "IX_orgEmpresaCtaContable");

                entity.Property(e => e.bDebeHAber).HasComment("0:Ambas, 1:Debe, 2:Haber");

                entity.Property(e => e.xCuenta)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.xNombre)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.sEmpresaNavigation)
                    .WithMany(p => p.orgEmpresaCtaContable)
                    .HasForeignKey(d => d.sEmpresa)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_orgEmpresaCtaContable_orgEmpresa");
            });
            modelBuilder.Entity<orgEmpresaDireccion>(entity =>
            {
                entity.HasKey(e => e.sEmpresa);

                entity.Property(e => e.sEmpresa).ValueGeneratedNever();

                entity.Property(e => e.bTipoVia).HasComment("Calle, Jirón, Avenida, etc.");

                entity.Property(e => e.xBlock)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xDpto)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xEtapa)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xInterior)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xKilometro)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xLote)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xManzana)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xNombreVia)
                    .HasMaxLength(300)
                    .IsUnicode(false)
                    .HasComment("Aqui guarda el concatenado, si no se pudo migrar por separado");

                entity.Property(e => e.xNombreZona)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.xNroVia)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xReferencia)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.bTipoViaNavigation)
                    .WithMany(p => p.orgEmpresaDireccion)
                    .HasForeignKey(d => d.bTipoVia)
                    .HasConstraintName("FK_orgEmpresaDireccion_isoTipoVia");

                entity.HasOne(d => d.bTipoZonaNavigation)
                    .WithMany(p => p.orgEmpresaDireccion)
                    .HasForeignKey(d => d.bTipoZona)
                    .HasConstraintName("FK_orgEmpresaDireccion_isoTipoZona");

                entity.HasOne(d => d.sDistritoNavigation)
                    .WithMany(p => p.orgEmpresaDireccion)
                    .HasForeignKey(d => d.sDistrito)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_orgEmpresaDireccion_isoDistrito");

                entity.HasOne(d => d.sEmpresaNavigation)
                    .WithOne(p => p.orgEmpresaDireccion)
                    .HasForeignKey<orgEmpresaDireccion>(d => d.sEmpresa)
                    .HasConstraintName("FK_orgEmpresaDireccion_orgEmpresa");
            });
            modelBuilder.Entity<orgEmpresaImagen>(entity =>
            {
                entity.HasKey(e => e.sEmpresaImagen);

                entity.Property(e => e.sEmpresaImagen).ValueGeneratedNever();

                entity.Property(e => e.bTipoImagen).HasComment("1:Logo,2:Firma,3:Sello agua");

                entity.Property(e => e.iImagen).HasColumnType("image");

                entity.Property(e => e.jImagen).IsUnicode(false);

                entity.HasOne(d => d.sEmpresaNavigation)
                    .WithMany(p => p.orgEmpresaImagen)
                    .HasForeignKey(d => d.sEmpresa)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_orgEmpresaImagen_orgEmpresa");
            });
            modelBuilder.Entity<orgEmpresaPeriodo>(entity =>
            {
                entity.HasKey(e => e.sEmpresa)
                    .HasName("PK_orgEmpresaEstado");

                entity.Property(e => e.sEmpresa).ValueGeneratedNever();

                entity.Property(e => e.dDesde).HasColumnType("date");

                entity.Property(e => e.dHasta).HasColumnType("date");

                entity.HasOne(d => d.sEmpresaNavigation)
                    .WithOne(p => p.orgEmpresaPeriodo)
                    .HasForeignKey<orgEmpresaPeriodo>(d => d.sEmpresa)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_orgEmpresaPeriodo_orgEmpresa");
            });
            modelBuilder.Entity<orgEmpresaPlanEPS>(entity =>
            {
                entity.HasKey(e => e.sEmpresaPlanEPS);

                entity.HasIndex(e => new { e.sEmpresa, e.sPlanEPS }, "IX_orgEmpresaPlanEPS")
                    .IsUnique();

                entity.HasOne(d => d.sEmpresaNavigation)
                    .WithMany(p => p.orgEmpresaPlanEPS)
                    .HasForeignKey(d => d.sEmpresa)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_orgEmpresaPlanEPS_orgEmpresa");

                entity.HasOne(d => d.sPlanEPSNavigation)
                    .WithMany(p => p.orgEmpresaPlanEPS)
                    .HasForeignKey(d => d.sPlanEPS)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_orgEmpresaPlanEPS_sysPlanEPS");
            });
            modelBuilder.Entity<orgEmpresaRepLegal>(entity =>
            {
                entity.HasKey(e => e.sEmpresa);

                entity.Property(e => e.sEmpresa).ValueGeneratedNever();

                entity.Property(e => e.xPartida)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.iPersonaNavigation)
                    .WithMany(p => p.orgEmpresaRepLegal)
                    .HasForeignKey(d => d.iPersona)
                    .HasConstraintName("FK_orgEmpresaRepLegal_perPersona");

                entity.HasOne(d => d.sEmpresaNavigation)
                    .WithOne(p => p.orgEmpresaRepLegal)
                    .HasForeignKey<orgEmpresaRepLegal>(d => d.sEmpresa)
                    .HasConstraintName("FK_orgEmpresaRepLegal_orgEmpresa");
            });
            modelBuilder.Entity<orgEmpresaSede>(entity =>
            {
                entity.HasKey(e => e.iEmpresaSede);

                entity.HasOne(d => d.sEmpresaNavigation)
                    .WithMany(p => p.orgEmpresaSede)
                    .HasForeignKey(d => d.sEmpresa)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_orgEmpresaSede_orgEmpresa");

                entity.HasOne(d => d.sSedeNavigation)
                    .WithMany(p => p.orgEmpresaSede)
                    .HasForeignKey(d => d.sSede)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_orgEmpresaSede_orgSede");
            });
            modelBuilder.Entity<orgEmpresaSedeDireccion>(entity =>
            {
                entity.HasKey(e => e.iEmpresaSede);

                entity.Property(e => e.iEmpresaSede).ValueGeneratedNever();

                entity.Property(e => e.bTipoVia).HasComment("Calle, Jirón, Avenida, etc.");

                entity.Property(e => e.xBlock)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xDpto)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xEtapa)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xInterior)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xKilometro)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xLote)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xManzana)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xNombreVia)
                    .HasMaxLength(300)
                    .IsUnicode(false)
                    .HasComment("Aqui guarda el concatenado, si no se pudo migrar por separado");

                entity.Property(e => e.xNombreZona)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.xNroVia)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xReferencia)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.bTipoViaNavigation)
                    .WithMany(p => p.orgEmpresaSedeDireccion)
                    .HasForeignKey(d => d.bTipoVia)
                    .HasConstraintName("FK_orgEmpresaSedeDireccion_isoTipoVia");

                entity.HasOne(d => d.bTipoZonaNavigation)
                    .WithMany(p => p.orgEmpresaSedeDireccion)
                    .HasForeignKey(d => d.bTipoZona)
                    .HasConstraintName("FK_orgEmpresaSedeDireccion_isoTipoZona");

                entity.HasOne(d => d.iEmpresaSedeNavigation)
                    .WithOne(p => p.orgEmpresaSedeDireccion)
                    .HasForeignKey<orgEmpresaSedeDireccion>(d => d.iEmpresaSede)
                    .HasConstraintName("FK_orgEmpresaSedeDireccion_orgEmpresaSede");

                entity.HasOne(d => d.sDistritoNavigation)
                    .WithMany(p => p.orgEmpresaSedeDireccion)
                    .HasForeignKey(d => d.sDistrito)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_orgEmpresaSedeDireccion_isoDistrito");
            });
            modelBuilder.Entity<orgEmpresaTelefono>(entity =>
            {
                entity.HasKey(e => new { e.sEmpresa, e.xTelefono });

                entity.Property(e => e.xTelefono)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.HasOne(d => d.sEmpresaNavigation)
                    .WithMany(p => p.orgEmpresaTelefono)
                    .HasForeignKey(d => d.sEmpresa)
                    .HasConstraintName("FK_orgEmpresaTelefono_orgEmpresa");
            });
            modelBuilder.Entity<orgEstablecimiento>(entity =>
            {
                entity.HasKey(e => e.cEstablecimiento);

                entity.Property(e => e.cEstablecimiento)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.xEstablecimiento)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<orgEstructuraSalarial>(entity =>
            {
                entity.HasKey(e => e.sEstructuraSalarial);

                entity.Property(e => e.bCantidadTrabajadores).HasDefaultValueSql("((1))");

                entity.Property(e => e.bDiasEfectivos).HasDefaultValueSql("((26))");

                entity.Property(e => e.bDiasTotal).HasDefaultValueSql("((30))");

                entity.Property(e => e.bHrsExt_diasEfectiv)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.bHrsExt_incluyeAsigFam)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.mSubTotal)
                    .HasColumnType("money")
                    .HasDefaultValueSql("((0.00))");

                entity.Property(e => e.xEstructuraSalarial)
                    .HasMaxLength(500)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<orgEstructuraSalarialDet>(entity =>
            {
                entity.HasKey(e => e.iEstructuraSalarialDet);

                entity.Property(e => e.mMonto).HasColumnType("money");

                entity.HasOne(d => d.sConceptoNavigation)
                    .WithMany(p => p.orgEstructuraSalarialDet)
                    .HasForeignKey(d => d.sConcepto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_orgEstructuraSalarialDet_remConcepto");

                entity.HasOne(d => d.sEstructuraSalarialNavigation)
                    .WithMany(p => p.orgEstructuraSalarialDet)
                    .HasForeignKey(d => d.sEstructuraSalarial)
                    .HasConstraintName("FK_orgEstructuraSalarialDet_orgEstructuraSalarial");
            });
            modelBuilder.Entity<orgGeoEstablecimiento>(entity =>
            {
                entity.HasKey(e => new { e.cEstablecimiento, e.sOrtogono, e.sPosicion });

                entity.Property(e => e.cEstablecimiento)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength(true);
            });
            modelBuilder.Entity<orgGrupo>(entity =>
            {
                entity.HasKey(e => e.sGrupo);

                entity.Property(e => e.xGrupo)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<orgNivelCargo>(entity =>
            {
                entity.HasKey(e => e.bNivel)
                    .HasName("PK_orgNivel");

                entity.Property(e => e.xNivel)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.bNivelPadreNavigation)
                    .WithMany(p => p.InversebNivelPadreNavigation)
                    .HasForeignKey(d => d.bNivelPadre)
                    .HasConstraintName("FK_orgNivelCargo_orgNivelCargo");
            });
            modelBuilder.Entity<orgSede>(entity =>
            {
                entity.HasKey(e => e.sSede);

                entity.Property(e => e.CodEstablecimiento)
                    .HasDefaultValueSql("((1))")
                    .HasComment("(Estructura 1) Código de establecimiento");

                entity.Property(e => e.bCentroRiesgo).HasComment("(Estructura 1) 0 = No es Centro de Riesgo y  1= Es Centro de Riesgo.");

                entity.Property(e => e.xSede)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });
        }
    }
}