﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class orgEmpresaSede
    {
        public int iEmpresaSede { get; set; }
        public short sEmpresa { get; set; }
        public short sSede { get; set; }
        public short sCodEstablecimiento { get; set; }
        public bool bCentroRiesgo { get; set; }

        public virtual orgEmpresa sEmpresaNavigation { get; set; }
        public virtual orgSede sSedeNavigation { get; set; }
        public virtual orgEmpresaSedeDireccion orgEmpresaSedeDireccion { get; set; }
    }
}
