﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class orgEmpresaTelefono
    {
        public short sEmpresa { get; set; }
        public string xTelefono { get; set; }

        public virtual orgEmpresa sEmpresaNavigation { get; set; }
    }
}
