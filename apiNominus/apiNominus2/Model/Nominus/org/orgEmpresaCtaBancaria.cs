﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class orgEmpresaCtaBancaria
    {
        public orgEmpresaCtaBancaria()
        {
            perCtaBancaria = new HashSet<perCtaBancaria>();
        }

        public short sEmpresaCtaBancaria { get; set; }
        public short sEmpresa { get; set; }
        public byte bTipoCtaBancaria { get; set; }
        public short sEntidadFinanciera { get; set; }
        public short sMoneda { get; set; }
        public string xCtaBancaria { get; set; }

        public virtual orgEmpresa sEmpresaNavigation { get; set; }
        public virtual isoEntidadFinanciera sEntidadFinancieraNavigation { get; set; }
        public virtual isoMoneda sMonedaNavigation { get; set; }
        [JsonIgnore] public virtual ICollection<perCtaBancaria> perCtaBancaria { get; set; }
    }
}
