﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class orgEmpresaCtaContable
    {
        public orgEmpresaCtaContable()
        {
            orgEmpresaCalculoCtaContableNeto = new HashSet<orgEmpresaCalculoCtaContableNeto>();
            remCalculoPlanillaConceptoCtasCtaDebeNavigation = new HashSet<remCalculoPlanillaConceptoCta>();
            remCalculoPlanillaConceptoCtasCtaHaberNavigation = new HashSet<remCalculoPlanillaConceptoCta>();
            remConceptoCalculoAfpCuentassCtaDebeNavigation = new HashSet<remConceptoCalculoAfpCuentas>();
            remConceptoCalculoAfpCuentassCtaHaberNavigation = new HashSet<remConceptoCalculoAfpCuentas>();
            remConceptoCalculoCuentassCtaDebeNavigation = new HashSet<remConceptoCalculoCuentas>();
            remConceptoCalculoCuentassCtaHaberNavigation = new HashSet<remConceptoCalculoCuentas>();
        }

        public short sCuenta { get; set; }
        public short sEmpresa { get; set; }
        public string xCuenta { get; set; }
        public string xNombre { get; set; }
        public byte bDebeHAber { get; set; }

        public virtual orgEmpresa sEmpresaNavigation { get; set; }
        [JsonIgnore] public virtual ICollection<orgEmpresaCalculoCtaContableNeto> orgEmpresaCalculoCtaContableNeto { get; set; }
        [JsonIgnore] public virtual ICollection<remCalculoPlanillaConceptoCta> remCalculoPlanillaConceptoCtasCtaDebeNavigation { get; set; }
        [JsonIgnore] public virtual ICollection<remCalculoPlanillaConceptoCta> remCalculoPlanillaConceptoCtasCtaHaberNavigation { get; set; }
        [JsonIgnore] public virtual ICollection<remConceptoCalculoAfpCuentas> remConceptoCalculoAfpCuentassCtaDebeNavigation { get; set; }
        [JsonIgnore] public virtual ICollection<remConceptoCalculoAfpCuentas> remConceptoCalculoAfpCuentassCtaHaberNavigation { get; set; }
        [JsonIgnore] public virtual ICollection<remConceptoCalculoCuentas> remConceptoCalculoCuentassCtaDebeNavigation { get; set; }
        [JsonIgnore] public virtual ICollection<remConceptoCalculoCuentas> remConceptoCalculoCuentassCtaHaberNavigation { get; set; }
    }
}
