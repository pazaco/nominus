﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class orgArea
    {
        public orgArea()
        {
            InversesAreaPadreNavigation = new HashSet<orgArea>();
            orgCargo = new HashSet<orgCargo>();
            traArea = new HashSet<traArea>();
        }

        public short sArea { get; set; }
        public string xArea { get; set; }
        public short? sAreaPadre { get; set; }

        public virtual orgArea sAreaPadreNavigation { get; set; }
        [JsonIgnore] public virtual ICollection<orgArea> InversesAreaPadreNavigation { get; set; }
        [JsonIgnore] public virtual ICollection<orgCargo> orgCargo { get; set; }
        [JsonIgnore] public virtual ICollection<traArea> traArea { get; set; }
    }
}
