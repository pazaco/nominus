﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class orgEmpresaCalculoCtaContableNeto
    {
        public short sEmpresa { get; set; }
        public byte bCalculo { get; set; }
        public byte bPlanilla { get; set; }
        public short sCuenta { get; set; }
        public byte bMostrar { get; set; }

        public virtual isoCalculo bCalculoNavigation { get; set; }
        public virtual remPlanilla bPlanillaNavigation { get; set; }
        public virtual orgEmpresaCtaContable sCuentaNavigation { get; set; }
        public virtual orgEmpresa sEmpresaNavigation { get; set; }
    }
}
