﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class orgEstructuraSalarial
    {
        public orgEstructuraSalarial()
        {
            orgEstructuraSalarialDet = new HashSet<orgEstructuraSalarialDet>();
            traAsistenciaEstructuraSalarial = new HashSet<traAsistenciaEstructuraSalarial>();
        }

        public short sEstructuraSalarial { get; set; }
        public string xEstructuraSalarial { get; set; }
        public byte bDiasEfectivos { get; set; }
        public byte bDiasTotal { get; set; }
        public bool? bHrsExt_diasEfectiv { get; set; }
        public bool? bHrsExt_incluyeAsigFam { get; set; }
        public bool bBonifNoc_diasEfectiv { get; set; }
        public bool bBonifNoc_incluyeAsigFam { get; set; }
        public byte bCantidadTrabajadores { get; set; }
        public decimal mSubTotal { get; set; }

        [JsonIgnore] public virtual ICollection<orgEstructuraSalarialDet> orgEstructuraSalarialDet { get; set; }
        [JsonIgnore] public virtual ICollection<traAsistenciaEstructuraSalarial> traAsistenciaEstructuraSalarial { get; set; }
    }
}
