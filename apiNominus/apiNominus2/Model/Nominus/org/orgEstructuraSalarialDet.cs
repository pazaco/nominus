﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class orgEstructuraSalarialDet
    {
        public int iEstructuraSalarialDet { get; set; }
        public short sEstructuraSalarial { get; set; }
        public short sConcepto { get; set; }
        public decimal mMonto { get; set; }

        public virtual remConcepto sConceptoNavigation { get; set; }
        public virtual orgEstructuraSalarial sEstructuraSalarialNavigation { get; set; }
    }
}
