﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class orgGeoEstablecimiento
    {
        public string cEstablecimiento { get; set; }
        public short sOrtogono { get; set; }
        public short sPosicion { get; set; }
        public double? fLat { get; set; }
        public double? fLon { get; set; }
    }
}
