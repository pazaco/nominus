﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class orgSede
    {
        public orgSede()
        {
            orgEmpresaSede = new HashSet<orgEmpresaSede>();
            traContrato = new HashSet<traContrato>();
        }

        public short sSede { get; set; }
        public string xSede { get; set; }
        public short CodEstablecimiento { get; set; }
        public bool bCentroRiesgo { get; set; }

        [JsonIgnore] public virtual ICollection<orgEmpresaSede> orgEmpresaSede { get; set; }
        [JsonIgnore] public virtual ICollection<traContrato> traContrato { get; set; }
    }
}
