﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class orgCentroCosto
    {
        public orgCentroCosto()
        {
            orgCentroCostoRelacionsCentroCostoNavigation = new HashSet<orgCentroCostoRelacion>();
            orgCentroCostoRelacionsCentroCostoRelacionNavigation = new HashSet<orgCentroCostoRelacion>();
            orgEmpresaCentroCosto = new HashSet<orgEmpresaCentroCosto>();
            traAsistenciaCentroCosto = new HashSet<traAsistenciaCentroCosto>();
            traCentroCosto = new HashSet<traCentroCosto>();
            traConceptoManualCentroCosto = new HashSet<traConceptoManualCentroCosto>();
        }

        public short sCentroCosto { get; set; }
        public string xCentroCosto { get; set; }
        public byte bNivelCentroCosto { get; set; }

        public virtual orgCentroCostoCodigo orgCentroCostoCodigo { get; set; }
        [JsonIgnore] public virtual ICollection<orgCentroCostoRelacion> orgCentroCostoRelacionsCentroCostoNavigation { get; set; }
        [JsonIgnore] public virtual ICollection<orgCentroCostoRelacion> orgCentroCostoRelacionsCentroCostoRelacionNavigation { get; set; }
        [JsonIgnore] public virtual ICollection<orgEmpresaCentroCosto> orgEmpresaCentroCosto { get; set; }
        [JsonIgnore] public virtual ICollection<traAsistenciaCentroCosto> traAsistenciaCentroCosto { get; set; }
        [JsonIgnore] public virtual ICollection<traCentroCosto> traCentroCosto { get; set; }
        [JsonIgnore] public virtual ICollection<traConceptoManualCentroCosto> traConceptoManualCentroCosto { get; set; }
    }
}
