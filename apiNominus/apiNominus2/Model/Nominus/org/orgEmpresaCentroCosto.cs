﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class orgEmpresaCentroCosto
    {
        public int iEmpresaCentroCosto { get; set; }
        public short sEmpresa { get; set; }
        public short sCentroCosto { get; set; }
        public string xCodigo { get; set; }
        public string xCuenta { get; set; }

        public virtual orgCentroCosto sCentroCostoNavigation { get; set; }
        public virtual orgEmpresa sEmpresaNavigation { get; set; }
    }
}
