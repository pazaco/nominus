﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class orgCargoServicio
    {
        public short sCargo { get; set; }
        public string xServicios { get; set; }

        public virtual orgCargo sCargoNavigation { get; set; }
    }
}
