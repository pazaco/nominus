﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class orgEmpresaRepLegal
    {
        public short sEmpresa { get; set; }
        public int? iPersona { get; set; }
        public string xPartida { get; set; }

        public virtual perPersona iPersonaNavigation { get; set; }
        public virtual orgEmpresa sEmpresaNavigation { get; set; }
    }
}
