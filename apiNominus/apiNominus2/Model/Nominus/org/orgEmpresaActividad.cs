﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class orgEmpresaActividad
    {
        public short sEmpresa { get; set; }
        public string xActividad { get; set; }
        public DateTime? dInicio { get; set; }

        public virtual orgEmpresa sEmpresaNavigation { get; set; }
    }
}
