﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class orgDiccCargoSunat
    {
        public short sCargo { get; set; }
        public string cOcupacion { get; set; }

        public virtual isoOcupacionSunat cOcupacionNavigation { get; set; }
        public virtual orgCargo sCargoNavigation { get; set; }
    }
}
