﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class orgEmpresaPeriodo
    {
        public short sEmpresa { get; set; }
        public DateTime? dDesde { get; set; }
        public DateTime? dHasta { get; set; }

        public virtual orgEmpresa sEmpresaNavigation { get; set; }
    }
}
