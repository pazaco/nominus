﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class orgCargo
    {
        public orgCargo()
        {
            orgCargoFuncion = new HashSet<orgCargoFuncion>();
            orgDiccCargoSunat = new HashSet<orgDiccCargoSunat>();
            traContrato = new HashSet<traContrato>();
        }

        public short sCargo { get; set; }
        public string xCargo { get; set; }
        public short? sArea { get; set; }
        public byte? bNivelCargo { get; set; }
        public int? iCodSunat { get; set; }

        public virtual orgNivelCargo bNivelCargoNavigation { get; set; }
        public virtual orgArea sAreaNavigation { get; set; }
        public virtual orgCargoFemenino orgCargoFemenino { get; set; }
        public virtual orgCargoServicio orgCargoServicio { get; set; }
        [JsonIgnore] public virtual ICollection<orgCargoFuncion> orgCargoFuncion { get; set; }
        [JsonIgnore] public virtual ICollection<orgDiccCargoSunat> orgDiccCargoSunat { get; set; }
        [JsonIgnore] public virtual ICollection<traContrato> traContrato { get; set; }
    }
}
