﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class orgEstablecimiento
    {
        public string cEstablecimiento { get; set; }
        public string xEstablecimiento { get; set; }
    }
}
