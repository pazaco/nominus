﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class orgCargoFuncion
    {
        public int iCargoFuncion { get; set; }
        public short sCargo { get; set; }
        public string xFuncion { get; set; }

        public virtual orgCargo sCargoNavigation { get; set; }
    }
}
