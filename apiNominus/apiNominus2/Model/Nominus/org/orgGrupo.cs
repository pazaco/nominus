﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class orgGrupo
    {
        public short sGrupo { get; set; }
        public string xGrupo { get; set; }
        public short? sGrupoPadre { get; set; }
    }
}
