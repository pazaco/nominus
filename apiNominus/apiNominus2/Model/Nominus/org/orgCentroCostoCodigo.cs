﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class orgCentroCostoCodigo
    {
        public short sCentroCosto { get; set; }
        public string xCodigo { get; set; }

        public virtual orgCentroCosto sCentroCostoNavigation { get; set; }
    }
}
