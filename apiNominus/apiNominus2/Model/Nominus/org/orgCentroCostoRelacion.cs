﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class orgCentroCostoRelacion
    {
        public short sCentroCosto { get; set; }
        public short sCentroCostoRelacion { get; set; }

        public virtual orgCentroCosto sCentroCostoNavigation { get; set; }
        public virtual orgCentroCosto sCentroCostoRelacionNavigation { get; set; }
    }
}
