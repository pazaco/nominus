﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Configuration;

#nullable disable

namespace apiNominus2.Model
{
    public partial class NominusContext : DbContext
    {

        private int iCliente;
        private short sProducto;
        private byte bModulo;
        private bool _Connected = false;

        public NominusContext(int iCliente,short sProducto, byte bModulo)
        {
            this.iCliente = iCliente;
            this.bModulo = bModulo;
            this.sProducto = sProducto;
        }

        public NominusContext(DbContextOptions<AsistenciaContext> options)
            : base(options)
        {
        }

        public bool isConnected()
        {
            return _Connected;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                ZasStringConexion strConn = Procs.ZasConexion(iCliente,sProducto,bModulo);
                if (strConn == null)
                {
                    _Connected = false;
                }
                else
                {
                    _Connected = true;
                    optionsBuilder.UseSqlServer(strConn.xConexion);
                }


            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            OnModelCreatingPartialPer(modelBuilder);
            OnModelCreatingPartialTra(modelBuilder);
            OnModelCreatingPartialIso(modelBuilder);
            OnModelCreatingPartialApp(modelBuilder);
            OnModelCreatingPartialOrg(modelBuilder);
        }

        partial void OnModelCreatingPartialPer(ModelBuilder modelBuilder);
        partial void OnModelCreatingPartialTra(ModelBuilder modelBuilder);
        partial void OnModelCreatingPartialIso(ModelBuilder modelBuilder);
        partial void OnModelCreatingPartialApp(ModelBuilder modelBuilder);
        partial void OnModelCreatingPartialOrg(ModelBuilder modelBuilder);

    }
}
