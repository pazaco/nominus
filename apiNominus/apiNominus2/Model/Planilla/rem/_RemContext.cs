﻿using Microsoft.EntityFrameworkCore;

#nullable disable

namespace apiNominus2.Model
{
    public partial class PlanillaContext : DbContext
    {
        public virtual DbSet<remCalculoPlanilla> remCalculoPlanilla { get; set; }
        public virtual DbSet<remCalculoPlanillaConcepto> remCalculoPlanillaConcepto { get; set; }
        public virtual DbSet<remCalculoPlanillaConceptoAfecta> remCalculoPlanillaConceptoAfecta { get; set; }
        public virtual DbSet<remCalculoPlanillaConceptoCc> remCalculoPlanillaConceptoCc { get; set; }
        public virtual DbSet<remCalculoPlanillaConceptoCta> remCalculoPlanillaConceptoCta { get; set; }
        public virtual DbSet<remCalculoPlanillaConceptoFormula> remCalculoPlanillaConceptoFormula { get; set; }
        public virtual DbSet<remConcepto> remConcepto { get; set; }
        public virtual DbSet<remConceptoAsistencia> remConceptoAsistencia { get; set; }
        public virtual DbSet<remConceptoAsistenciaEmpresaPlanilla> remConceptoAsistenciaEmpresaPlanilla { get; set; }
        public virtual DbSet<remConceptoAsistenciaPlanilla> remConceptoAsistenciaPlanilla { get; set; }
        public virtual DbSet<remConceptoCalculo> remConceptoCalculo { get; set; }
        public virtual DbSet<remConceptoCalculoAfpCuentas> remConceptoCalculoAfpCuentas { get; set; }
        public virtual DbSet<remConceptoCalculoAsistencia> remConceptoCalculoAsistencia { get; set; }
        public virtual DbSet<remConceptoCalculoCuentas> remConceptoCalculoCuentas { get; set; }
        public virtual DbSet<remConceptoCalculoDependencia> remConceptoCalculoDependencia { get; set; }
        public virtual DbSet<remConceptoCalculoDias> remConceptoCalculoDias { get; set; }
        public virtual DbSet<remConceptoCalculoFormula> remConceptoCalculoFormula { get; set; }
        public virtual DbSet<remConceptoCalculoHistory> remConceptoCalculoHistory { get; set; }
        public virtual DbSet<remConceptoCalculoManual> remConceptoCalculoManual { get; set; }
        public virtual DbSet<remConceptoCalculoMonto> remConceptoCalculoMonto { get; set; }
        public virtual DbSet<remConceptoCalculoSumado> remConceptoCalculoSumado { get; set; }
        public virtual DbSet<remConceptoCalculoVigencia> remConceptoCalculoVigencia { get; set; }
        public virtual DbSet<remConceptoSubClase> remConceptoSubClase { get; set; }
        public virtual DbSet<remConceptoTipoSuspensionRL> remConceptoTipoSuspensionRL { get; set; }
        public virtual DbSet<remImportaPlanilla> remImportaPlanilla { get; set; }
        public virtual DbSet<remImportaPlanillaCabecera> remImportaPlanillaCabecera { get; set; }
        public virtual DbSet<remImportaPlanillaColumna> remImportaPlanillaColumna { get; set; }
        public virtual DbSet<remPeriodo> remPeriodo { get; set; }
        public virtual DbSet<remPeriodoTipoCambio> remPeriodoTipoCambio { get; set; }
        public virtual DbSet<remPlanilla> remPlanilla { get; set; }

        partial void OnModelCreatingPartialRem(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<remCalculoPlanilla>(entity =>
            {
                entity.HasKey(e => e.iCalculoPlanilla);

                entity.Property(e => e.dCreacion)
                    .HasPrecision(0)
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.iContratoNavigation)
                    .WithMany(p => p.remCalculoPlanilla)
                    .HasForeignKey(d => d.iContrato)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_remCalculoPlanilla_traContrato");

                entity.HasOne(d => d.iPeriodoNavigation)
                    .WithMany(p => p.remCalculoPlanilla)
                    .HasForeignKey(d => d.iPeriodo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_remCalculoPlanilla_remPeriodo");
            });
            modelBuilder.Entity<remCalculoPlanillaConcepto>(entity =>
            {
                entity.HasKey(e => new { e.iCalculoPlanilla, e.sConcepto });

                entity.Property(e => e.mMonto).HasColumnType("money");

                entity.HasOne(d => d.iCalculoPlanillaNavigation)
                    .WithMany(p => p.remCalculoPlanillaConcepto)
                    .HasForeignKey(d => d.iCalculoPlanilla)
                    .HasConstraintName("FK_remCalculoPlanillaConcepto_remCalculoPlanilla");

                entity.HasOne(d => d.sConceptoNavigation)
                    .WithMany(p => p.remCalculoPlanillaConcepto)
                    .HasForeignKey(d => d.sConcepto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_remCalculoPlanillaConcepto_remConcepto");
            });
            modelBuilder.Entity<remCalculoPlanillaConceptoAfecta>(entity =>
            {
                entity.HasKey(e => new { e.iCalculoPlanilla, e.sConcepto })
                    .HasName("PK_remCalculoPlanillaConceptoHist");

                entity.HasOne(d => d.remCalculoPlanillaConcepto)
                    .WithOne(p => p.remCalculoPlanillaConceptoAfecta)
                    .HasForeignKey<remCalculoPlanillaConceptoAfecta>(d => new { d.iCalculoPlanilla, d.sConcepto })
                    .HasConstraintName("FK_remCalculoPlanillaConceptoAfecta_remCalculoPlanillaConcepto");
            });
            modelBuilder.Entity<remCalculoPlanillaConceptoCc>(entity =>
            {
                entity.HasKey(e => new { e.iCalculoPlanilla, e.sConcepto, e.sCentroCosto, e.bTurno });

                entity.Property(e => e.bTurno)
                    .HasDefaultValueSql("((1))")
                    .HasComment("1: Mañana, 2: Tarde, 3: Noche");

                entity.Property(e => e.mMonto).HasColumnType("money");

                entity.HasOne(d => d.iCalculoPlanillaNavigation)
                    .WithMany(p => p.remCalculoPlanillaConceptoCc)
                    .HasForeignKey(d => d.iCalculoPlanilla)
                    .HasConstraintName("FK_remCalculoPlanillaConceptoCc_remCalculoPlanilla");
            });
            modelBuilder.Entity<remCalculoPlanillaConceptoCta>(entity =>
            {
                entity.HasKey(e => new { e.iCalculoPlanilla, e.sConcepto });

                entity.HasOne(d => d.sCtaDebeNavigation)
                    .WithMany(p => p.remCalculoPlanillaConceptoCtasCtaDebeNavigation)
                    .HasForeignKey(d => d.sCtaDebe)
                    .HasConstraintName("FK_remCalculoPlanillaConceptoCta_orgEmpresaCtaContable");

                entity.HasOne(d => d.sCtaHaberNavigation)
                    .WithMany(p => p.remCalculoPlanillaConceptoCtasCtaHaberNavigation)
                    .HasForeignKey(d => d.sCtaHaber)
                    .HasConstraintName("FK_remCalculoPlanillaConceptoCta_orgEmpresaCtaContable1");

                entity.HasOne(d => d.remCalculoPlanillaConcepto)
                    .WithOne(p => p.remCalculoPlanillaConceptoCta)
                    .HasForeignKey<remCalculoPlanillaConceptoCta>(d => new { d.iCalculoPlanilla, d.sConcepto })
                    .HasConstraintName("FK_remCalculoPlanillaConceptoCta_remCalculoPlanillaConcepto");
            });
            modelBuilder.Entity<remCalculoPlanillaConceptoFormula>(entity =>
            {
                entity.HasKey(e => new { e.iCalculoPlanilla, e.sConcepto });

                entity.Property(e => e.xFormulaOriginal)
                    .IsRequired()
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.xFormulaResuelta)
                    .IsRequired()
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.HasOne(d => d.remCalculoPlanillaConcepto)
                    .WithOne(p => p.remCalculoPlanillaConceptoFormula)
                    .HasForeignKey<remCalculoPlanillaConceptoFormula>(d => new { d.iCalculoPlanilla, d.sConcepto })
                    .HasConstraintName("FK_remCalculoPlanillaConceptoFormula_remCalculoPlanillaConcepto");
            });
            modelBuilder.Entity<remConcepto>(entity =>
            {
                entity.HasKey(e => e.sConcepto);

                entity.HasIndex(e => e.cConcepto, "IX_remConcepto")
                    .IsUnique();

                entity.Property(e => e.bClaseConcepto).HasComment("0:Info,1:Asistencia,2:Ingresos,3:Descuentos,4:Empleador");

                entity.Property(e => e.bTipoConceptoDefault)
                    .HasDefaultValueSql("((1))")
                    .HasComment("0:Sistema,1:Manual,2:Fórmula,3:Sumado,etc.");

                entity.Property(e => e.cConcepto)
                    .IsRequired()
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .IsFixedLength(true)
                    .HasComment("S");

                entity.Property(e => e.cConceptoSunat)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength(true)
                    .HasComment("Código PLAME");

                entity.Property(e => e.xConcepto)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("Descripción para boleta");

                entity.Property(e => e.xDescripcion)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasComment("Descripción larga para LBS");

                entity.Property(e => e.xTip)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasComment("Descripción corta para columnas de reportes u otros");

                entity.HasOne(d => d.bClaseConceptoNavigation)
                    .WithMany(p => p.remConcepto)
                    .HasForeignKey(d => d.bClaseConcepto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_remConcepto_isoClaseConcepto");

                entity.HasOne(d => d.bTipoConceptoDefaultNavigation)
                    .WithMany(p => p.remConcepto)
                    .HasForeignKey(d => d.bTipoConceptoDefault)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_remConcepto_isoTipoConcepto");

                entity.HasOne(d => d.sUnidadMedidaNavigation)
                    .WithMany(p => p.remConcepto)
                    .HasForeignKey(d => d.sUnidadMedida)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_remConcepto_isoUnidadMedida");
            });
            modelBuilder.Entity<remConceptoAsistencia>(entity =>
            {
                entity.HasKey(e => e.sConcepto);

                entity.Property(e => e.sConcepto).ValueGeneratedNever();

                entity.Property(e => e.bAfecta)
                    .HasDefaultValueSql("((2))")
                    .HasComment("0:Ninguno, 1:Asistencia, 2:Inasistencia");

                entity.Property(e => e.bMaxDias).HasComment("Máximo de días en el año (Por ejemplo: Descanso médico: 20, Maternidad: 98)");

                entity.Property(e => e.iColor).HasDefaultValueSql("((16777215))");

                entity.HasOne(d => d.sConceptoNavigation)
                    .WithOne(p => p.remConceptoAsistencia)
                    .HasForeignKey<remConceptoAsistencia>(d => d.sConcepto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_remConceptoAsistencia_remConcepto");
            });
            modelBuilder.Entity<remConceptoAsistenciaEmpresaPlanilla>(entity =>
            {
                entity.HasKey(e => new { e.sConcepto, e.sEmpresa, e.bPlanilla });

                entity.HasOne(d => d.bPlanillaNavigation)
                    .WithMany(p => p.remConceptoAsistenciaEmpresaPlanilla)
                    .HasForeignKey(d => d.bPlanilla)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_remConceptoAsistenciaEmpresaPlanilla_remPlanilla");

                entity.HasOne(d => d.sConceptoNavigation)
                    .WithMany(p => p.remConceptoAsistenciaEmpresaPlanilla)
                    .HasForeignKey(d => d.sConcepto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_remConceptoAsistenciaEmpresaPlanilla_remConceptoAsistencia");

                entity.HasOne(d => d.sEmpresaNavigation)
                    .WithMany(p => p.remConceptoAsistenciaEmpresaPlanilla)
                    .HasForeignKey(d => d.sEmpresa)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_remConceptoAsistenciaEmpresaPlanilla_orgEmpresa");
            });
            modelBuilder.Entity<remConceptoAsistenciaPlanilla>(entity =>
            {
                entity.HasKey(e => new { e.sConcepto, e.bPlanilla });

                entity.Property(e => e.bMaxDias).HasDefaultValueSql("((15))");

                entity.HasOne(d => d.bPlanillaNavigation)
                    .WithMany(p => p.remConceptoAsistenciaPlanilla)
                    .HasForeignKey(d => d.bPlanilla)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_remConceptoAsistenciaPlanilla_remPlanilla");

                entity.HasOne(d => d.sConceptoNavigation)
                    .WithMany(p => p.remConceptoAsistenciaPlanilla)
                    .HasForeignKey(d => d.sConcepto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_remConceptoAsistenciaPlanilla_remConcepto");
            });
            modelBuilder.Entity<remConceptoCalculo>(entity =>
            {
                entity.HasKey(e => e.iConceptoCalculo);

                entity.HasIndex(e => new { e.sEmpresa, e.bCalculo, e.bPlanilla, e.sConcepto }, "IX_remConceptoCalculo")
                    .IsUnique();

                entity.Property(e => e.bAfecta).HasComment("Interviene en la boleta: Se muestra y se suma");

                entity.Property(e => e.bCalculo)
                    .HasDefaultValueSql("((2))")
                    .HasComment("1:Adelanto, 2:Boleta, 3:Liquidación, 4:Vacaciones, 5:Gratificaciones, 6:CTS, 7:Utilidades");

                entity.Property(e => e.bManual).HasComment("Si el concepto se puede ingresar de foma manual (Concepto Periodo/Fijo)");

                entity.Property(e => e.bPlanilla).HasComment("1:Empleados, etc.");

                entity.Property(e => e.bTipoConcepto)
                    .HasDefaultValueSql("((1))")
                    .HasComment("0:Sistema,1:Manual,2:Fórmula,3:Sumado,4:Promedio,etc.");

                entity.Property(e => e.sConcepto).HasComment("Ejemplo: S0001, A1010, I1010, D2000, E3000");

                entity.Property(e => e.sOrdenCalculo).HasComment("Ordena en que se resolverá formulas");

                entity.Property(e => e.sOrdenMostrar).HasComment("Orden e que se mostrará en la boleta y reportes");

                entity.HasOne(d => d.bCalculoNavigation)
                    .WithMany(p => p.remConceptoCalculo)
                    .HasForeignKey(d => d.bCalculo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_remConceptoCalculo_isoCalculo");

                entity.HasOne(d => d.bPlanillaNavigation)
                    .WithMany(p => p.remConceptoCalculo)
                    .HasForeignKey(d => d.bPlanilla)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_remConceptoCalculo_remPlanilla");

                entity.HasOne(d => d.bTipoConceptoNavigation)
                    .WithMany(p => p.remConceptoCalculo)
                    .HasForeignKey(d => d.bTipoConcepto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_remConceptoCalculo_isoTipoConcepto");

                entity.HasOne(d => d.sConceptoNavigation)
                    .WithMany(p => p.remConceptoCalculo)
                    .HasForeignKey(d => d.sConcepto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_remConceptoCalculo_remConcepto");

                entity.HasOne(d => d.sEmpresaNavigation)
                    .WithMany(p => p.remConceptoCalculo)
                    .HasForeignKey(d => d.sEmpresa)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_remConceptoCalculo_orgEmpresa");
            });
            modelBuilder.Entity<remConceptoCalculoAfpCuentas>(entity =>
            {
                entity.HasKey(e => new { e.iConceptoCalculo, e.bRegimenPensionario });

                entity.Property(e => e.bMostrar).HasComment("0= Total, 1=Centro de costo, 2=Trabajador");

                entity.HasOne(d => d.bRegimenPensionarioNavigation)
                    .WithMany(p => p.remConceptoCalculoAfpCuentas)
                    .HasForeignKey(d => d.bRegimenPensionario)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_remConceptoCalculoAfpCuentas_isoRegimenPensionario");

                entity.HasOne(d => d.iConceptoCalculoNavigation)
                    .WithMany(p => p.remConceptoCalculoAfpCuentas)
                    .HasForeignKey(d => d.iConceptoCalculo)
                    .HasConstraintName("FK_remConceptoCalculoAfpCuentas_remConceptoCalculo");

                entity.HasOne(d => d.sCtaDebeNavigation)
                    .WithMany(p => p.remConceptoCalculoAfpCuentassCtaDebeNavigation)
                    .HasForeignKey(d => d.sCtaDebe)
                    .HasConstraintName("FK_remConceptoCalculoAfpCuentas_orgEmpresaCtaContable");

                entity.HasOne(d => d.sCtaHaberNavigation)
                    .WithMany(p => p.remConceptoCalculoAfpCuentassCtaHaberNavigation)
                    .HasForeignKey(d => d.sCtaHaber)
                    .HasConstraintName("FK_remConceptoCalculoAfpCuentas_orgEmpresaCtaContable1");
            });
            modelBuilder.Entity<remConceptoCalculoAsistencia>(entity =>
            {
                entity.HasKey(e => e.iConceptoCalculo);

                entity.Property(e => e.iConceptoCalculo).ValueGeneratedNever();

                entity.Property(e => e.sUnidadMedida).HasComment("Unidad de medida que traerá como resultado en el cálculo, es decir, que transformará la unidad de medida si este es diferente al que está en el concepto.");

                entity.HasOne(d => d.iConceptoCalculoNavigation)
                    .WithOne(p => p.remConceptoCalculoAsistencia)
                    .HasForeignKey<remConceptoCalculoAsistencia>(d => d.iConceptoCalculo)
                    .HasConstraintName("FK_remConceptoCalculoAsistencia_remConceptoCalculo");

                entity.HasOne(d => d.sUnidadMedidaNavigation)
                    .WithMany(p => p.remConceptoCalculoAsistencia)
                    .HasForeignKey(d => d.sUnidadMedida)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_remConceptoCalculoAsistencia_isoUnidadMedida");
            });
            modelBuilder.Entity<remConceptoCalculoCuentas>(entity =>
            {
                entity.HasKey(e => e.iConceptoCalculo);

                entity.Property(e => e.iConceptoCalculo).ValueGeneratedNever();

                entity.Property(e => e.bMostrarDebe).HasComment("0= Total, 1=Centro de costo, 2=Trabajador");

                entity.HasOne(d => d.iConceptoCalculoNavigation)
                    .WithOne(p => p.remConceptoCalculoCuentas)
                    .HasForeignKey<remConceptoCalculoCuentas>(d => d.iConceptoCalculo)
                    .HasConstraintName("FK_remConceptoCalculoCuentas_remConceptoCalculo");

                entity.HasOne(d => d.sCtaDebeNavigation)
                    .WithMany(p => p.remConceptoCalculoCuentassCtaDebeNavigation)
                    .HasForeignKey(d => d.sCtaDebe)
                    .HasConstraintName("FK_remConceptoCalculoCuentas_orgEmpresaCtaContable");

                entity.HasOne(d => d.sCtaHaberNavigation)
                    .WithMany(p => p.remConceptoCalculoCuentassCtaHaberNavigation)
                    .HasForeignKey(d => d.sCtaHaber)
                    .HasConstraintName("FK_remConceptoCalculoCuentas_orgEmpresaCtaContable1");
            });
            modelBuilder.Entity<remConceptoCalculoDependencia>(entity =>
            {
                entity.HasKey(e => new { e.iConceptoCalculo, e.sConcepto });

                entity.Property(e => e.sConcepto).HasComment("Concepto del cual depende para calcularlo. Ej: D6090 Renta 5ta, depende que haya D5510 Renta neta anual para calcularse.");

                entity.HasOne(d => d.iConceptoCalculoNavigation)
                    .WithMany(p => p.remConceptoCalculoDependencia)
                    .HasForeignKey(d => d.iConceptoCalculo)
                    .HasConstraintName("FK_remConceptoCalculoDependencia_remConceptoCalculo");

                entity.HasOne(d => d.sConceptoNavigation)
                    .WithMany(p => p.remConceptoCalculoDependencia)
                    .HasForeignKey(d => d.sConcepto)
                    .HasConstraintName("FK_remConceptoCalculoDependencia_remConcepto");
            });
            modelBuilder.Entity<remConceptoCalculoDias>(entity =>
            {
                entity.HasKey(e => e.iConceptoCalculo);

                entity.Property(e => e.iConceptoCalculo).ValueGeneratedNever();

                entity.HasOne(d => d.iConceptoCalculoNavigation)
                    .WithOne(p => p.remConceptoCalculoDias)
                    .HasForeignKey<remConceptoCalculoDias>(d => d.iConceptoCalculo)
                    .HasConstraintName("FK_remConceptoCalculoDias_remConceptoCalculo");

                entity.HasOne(d => d.sConceptoDiasNavigation)
                    .WithMany(p => p.remConceptoCalculoDias)
                    .HasForeignKey(d => d.sConceptoDias)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_remConceptoCalculoDias_remConcepto");
            });
            modelBuilder.Entity<remConceptoCalculoFormula>(entity =>
            {
                entity.HasKey(e => e.iConceptoCalculo);

                entity.Property(e => e.iConceptoCalculo).ValueGeneratedNever();

                entity.Property(e => e.xFormula)
                    .IsRequired()
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.HasOne(d => d.iConceptoCalculoNavigation)
                    .WithOne(p => p.remConceptoCalculoFormula)
                    .HasForeignKey<remConceptoCalculoFormula>(d => d.iConceptoCalculo)
                    .HasConstraintName("FK_remConceptoCalculoFormula_remConceptoCalculo");
            });
            modelBuilder.Entity<remConceptoCalculoHistory>(entity =>
            {
                entity.HasKey(e => e.iConceptoCalculo);

                entity.Property(e => e.iConceptoCalculo).ValueGeneratedNever();

                entity.Property(e => e.bActualConsiderar).HasComment("0:Periodo actual,1:Mes anterior,2:Número anterior,3:Mes actual si hay monto,4:Número actual si hay monto,5:Mes actual si cesó último día del mes,6:Mes actual si cesó último día del periodo actual,7:Mes actual si cesó en día especifico:");

                entity.Property(e => e.bAfecta).HasComment("0:Todas,1:Solo si es afecto,2:Solo si no es afecto");

                entity.Property(e => e.bCalculo).HasComment("0:Todos de Remunerar, 10:Todos de Provisionar");

                entity.Property(e => e.bCalculoDeclara)
                    .HasDefaultValueSql("((2))")
                    .HasComment("1:Calculo se declara, 0:Cálculo no se declara, 2: Todas (no importa si se declara o no)");

                entity.Property(e => e.bDivideEntre).HasDefaultValueSql("((1))");

                entity.Property(e => e.bFuente).HasComment("1:Concepto,2:Total ingresos,3:Total descuentos,4:Neto,5:Total empleador");

                entity.Property(e => e.bIngreso).HasComment("1:Último ingreso a la empresa,2:Todos los ingresos de la empresa,3:Todas las empresas");

                entity.Property(e => e.bMinConsiderar).HasDefaultValueSql("((1))");

                entity.Property(e => e.bTipoAnos).HasComment("0:Todos los años,1:Año actual,2:Años anteriores");

                entity.Property(e => e.bTipoInfo).HasComment("1:Sumar,2:Dividir,3:Promediar entre meses hay,4:Promediar meses rango,5:Promediar entre números hay,6:Promediar entre números rango,7:Meses hay monto,8:Meses rango,9:Números hay monto,10:Números rango");

                entity.Property(e => e.bTipoPeriodos).HasComment("0:Año completo,1:Mes actual,2:Mes especifico,3:Meses anteriores,4:Número actual,5:Número especifico,6:Números anteriores,7:Periodo de Vacaciones,8:Periodo de gratificaciones,9:Periodo de CTS,10:Desde ingreso");

                entity.HasOne(d => d.bCalculoNavigation)
                    .WithMany(p => p.remConceptoCalculoHistory)
                    .HasForeignKey(d => d.bCalculo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_remConceptoCalculoHistory_isoCalculo");

                entity.HasOne(d => d.iConceptoCalculoNavigation)
                    .WithOne(p => p.remConceptoCalculoHistory)
                    .HasForeignKey<remConceptoCalculoHistory>(d => d.iConceptoCalculo)
                    .HasConstraintName("FK_remConceptoCalculoHistory_remConceptoCalculo");

                entity.HasOne(d => d.sConceptoNavigation)
                    .WithMany(p => p.remConceptoCalculoHistory)
                    .HasForeignKey(d => d.sConcepto)
                    .HasConstraintName("FK_remConceptoCalculoHistory_remConcepto");
            });
            modelBuilder.Entity<remConceptoCalculoManual>(entity =>
            {
                entity.HasKey(e => e.iConceptoCalculo);

                entity.Property(e => e.iConceptoCalculo).ValueGeneratedNever();

                entity.HasOne(d => d.iConceptoCalculoNavigation)
                    .WithOne(p => p.remConceptoCalculoManual)
                    .HasForeignKey<remConceptoCalculoManual>(d => d.iConceptoCalculo)
                    .HasConstraintName("FK_remConceptoCalculoManual_remConceptoCalculo");

                entity.HasOne(d => d.sConceptoManualNavigation)
                    .WithMany(p => p.remConceptoCalculoManual)
                    .HasForeignKey(d => d.sConceptoManual)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_remConceptoCalculoManual_remConcepto");
            });
            modelBuilder.Entity<remConceptoCalculoMonto>(entity =>
            {
                entity.HasKey(e => e.iConceptoCalculo);

                entity.Property(e => e.iConceptoCalculo).ValueGeneratedNever();

                entity.Property(e => e.mMonto).HasColumnType("money");

                entity.HasOne(d => d.iConceptoCalculoNavigation)
                    .WithOne(p => p.remConceptoCalculoMonto)
                    .HasForeignKey<remConceptoCalculoMonto>(d => d.iConceptoCalculo)
                    .HasConstraintName("FK_remConceptoCalculoMonto_remConceptoCalculo");
            });
            modelBuilder.Entity<remConceptoCalculoSumado>(entity =>
            {
                entity.HasKey(e => new { e.iConceptoCalculo, e.sConcepto });

                entity.HasOne(d => d.iConceptoCalculoNavigation)
                    .WithMany(p => p.remConceptoCalculoSumado)
                    .HasForeignKey(d => d.iConceptoCalculo)
                    .HasConstraintName("FK_remConceptoCalculoSumado_remConceptoCalculo");

                entity.HasOne(d => d.sConceptoNavigation)
                    .WithMany(p => p.remConceptoCalculoSumado)
                    .HasForeignKey(d => d.sConcepto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_remConceptoCalculoSumado_remConcepto");
            });
            modelBuilder.Entity<remConceptoCalculoVigencia>(entity =>
            {
                entity.HasKey(e => e.iConceptoCalculo);

                entity.Property(e => e.iConceptoCalculo).ValueGeneratedNever();

                entity.Property(e => e.dDesde).HasColumnType("date");

                entity.Property(e => e.dHasta).HasColumnType("date");

                entity.HasOne(d => d.iConceptoCalculoNavigation)
                    .WithOne(p => p.remConceptoCalculoVigencia)
                    .HasForeignKey<remConceptoCalculoVigencia>(d => d.iConceptoCalculo)
                    .HasConstraintName("FK_remConceptoCalculoVigencia_remConceptoCalculo");
            });
            modelBuilder.Entity<remConceptoSubClase>(entity =>
            {
                entity.HasKey(e => e.sConcepto)
                    .HasName("PK_remConceptoSubClase_1");

                entity.Property(e => e.sConcepto).ValueGeneratedNever();

                entity.HasOne(d => d.bSubClaseConceptoNavigation)
                    .WithMany(p => p.remConceptoSubClase)
                    .HasForeignKey(d => d.bSubClaseConcepto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_remConceptoSubClase_isoSubClaseConcepto");

                entity.HasOne(d => d.sConceptoNavigation)
                    .WithOne(p => p.remConceptoSubClase)
                    .HasForeignKey<remConceptoSubClase>(d => d.sConcepto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_remConceptoSubClase_remConcepto");
            });
            modelBuilder.Entity<remConceptoTipoSuspensionRL>(entity =>
            {
                entity.HasKey(e => e.sConcepto);

                entity.Property(e => e.sConcepto).ValueGeneratedNever();

                entity.HasOne(d => d.bTipoSuspensionRLNavigation)
                    .WithMany(p => p.remConceptoTipoSuspensionRL)
                    .HasForeignKey(d => d.bTipoSuspensionRL)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_remConceptoTipoSuspensionRL_isoTipoSuspensionRL");

                entity.HasOne(d => d.sConceptoNavigation)
                    .WithOne(p => p.remConceptoTipoSuspensionRL)
                    .HasForeignKey<remConceptoTipoSuspensionRL>(d => d.sConcepto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_remConceptoTipoSuspensionRL_remConcepto");
            });
            modelBuilder.Entity<remImportaPlanilla>(entity =>
            {
                entity.HasKey(e => new { e.iImportaPlanilla, e.sConcepto })
                    .HasName("PK_remImportaPlanilla_1");

                entity.Property(e => e.mMonto).HasColumnType("money");

                entity.HasOne(d => d.iImportaPlanillaNavigation)
                    .WithMany(p => p.remImportaPlanilla)
                    .HasForeignKey(d => d.iImportaPlanilla)
                    .HasConstraintName("FK_remImportaPlanilla_remImportaPlanillaCabecera");
            });
            modelBuilder.Entity<remImportaPlanillaCabecera>(entity =>
            {
                entity.HasKey(e => e.iImportaPlanilla);

                entity.Property(e => e.codigo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.trabajador)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.xdocidentidad)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<remImportaPlanillaColumna>(entity =>
            {
                entity.HasKey(e => new { e.bCalculo, e.sConcepto });

                entity.HasIndex(e => new { e.bCalculo, e.bColumna }, "IX_remImportaPlanillaColumna")
                    .IsUnique();
            });
            modelBuilder.Entity<remPeriodo>(entity =>
            {
                entity.HasKey(e => e.iPeriodo);

                entity.HasIndex(e => new { e.sEmpresa, e.bCalculo, e.bPlanilla, e.iAnoMes, e.bNumero }, "IX_remPeriodo")
                    .IsUnique();

                entity.Property(e => e.bMes).HasComputedColumnSql("(CONVERT([tinyint],right([iAnoMes],(2)),(0)))", true);

                entity.Property(e => e.bNotificado).HasComment("1: Los trabajadores podrán ver su boleta en la web, y se enviará notificaciones a los trabajadores de esto.");

                entity.Property(e => e.dDesde).HasColumnType("date");

                entity.Property(e => e.dDesdeAsistencia).HasColumnType("date");

                entity.Property(e => e.dHasta).HasColumnType("date");

                entity.Property(e => e.dHastaAsistencia).HasColumnType("date");

                entity.Property(e => e.iAnoMesNumero)
                    .HasMaxLength(8)
                    .IsUnicode(false)
                    .HasComputedColumnSql("(CONVERT([varchar](6),[iAnoMes])+right('00'+CONVERT([varchar](2),[bNumero]),(2)))", true)
                    .HasComment("YYYYMMnn");

                entity.Property(e => e.iAnoNumero)
                    .HasComputedColumnSql("(CONVERT([int],left([iAnoMes],(4))+right('0'+CONVERT([varchar](2),[bNumero],(0)),(2)),(0)))", true)
                    .HasComment("YYYYnn");

                entity.Property(e => e.sAno).HasComputedColumnSql("(CONVERT([smallint],left([iAnoMes],(4)),(0)))", true);

                entity.Property(e => e.sEmpresa).HasDefaultValueSql("((1))");

                entity.HasOne(d => d.bCalculoNavigation)
                    .WithMany(p => p.remPeriodo)
                    .HasForeignKey(d => d.bCalculo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_remPeriodo_isoCalculo");

                entity.HasOne(d => d.bPlanillaNavigation)
                    .WithMany(p => p.remPeriodo)
                    .HasForeignKey(d => d.bPlanilla)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_remPeriodo_remPlanilla");
            });
            modelBuilder.Entity<remPeriodoTipoCambio>(entity =>
            {
                entity.HasKey(e => new { e.iPeriodo, e.sMoneda });

                entity.Property(e => e.sMoneda).HasDefaultValueSql("((997))");

                entity.Property(e => e.rTipoCambio).HasColumnType("decimal(18, 4)");

                entity.HasOne(d => d.iPeriodoNavigation)
                    .WithMany(p => p.remPeriodoTipoCambio)
                    .HasForeignKey(d => d.iPeriodo)
                    .HasConstraintName("FK_remPeriodoTipoCambio_remPeriodo");
            });
            modelBuilder.Entity<remPlanilla>(entity =>
            {
                entity.HasKey(e => e.bPlanilla);

                entity.Property(e => e.bPlanilla).ValueGeneratedOnAdd();

                entity.Property(e => e.bDeclarar).HasDefaultValueSql("((1))");

                entity.Property(e => e.bTipoPlanilla).HasComment("1: Empleado, 2: Obrero Regimen común (Mensual), 3: Obrero Regimen común (Semanal), 4: Practicante, 5: Obrero Construcción Civil (Semanal), 6:CAS");

                entity.Property(e => e.xPlanilla)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.HasOne(d => d.bTipoPlanillaNavigation)
                    .WithMany(p => p.remPlanilla)
                    .HasForeignKey(d => d.bTipoPlanilla)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_remPlanilla_isoTipoPlanilla");
            });

        }
    }
}