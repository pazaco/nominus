﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class remConceptoCalculo
    {
        public remConceptoCalculo()
        {
            remConceptoCalculoAfpCuentas = new HashSet<remConceptoCalculoAfpCuentas>();
            remConceptoCalculoDependencia = new HashSet<remConceptoCalculoDependencia>();
            remConceptoCalculoSumado = new HashSet<remConceptoCalculoSumado>();
        }

        public int iConceptoCalculo { get; set; }
        public short sEmpresa { get; set; }
        public byte bCalculo { get; set; }
        public byte bPlanilla { get; set; }
        public short sConcepto { get; set; }
        public byte bTipoConcepto { get; set; }
        public short sOrdenCalculo { get; set; }
        public short sOrdenMostrar { get; set; }
        public bool bAfecta { get; set; }
        public bool bManual { get; set; }

        public virtual isoCalculo bCalculoNavigation { get; set; }
        public virtual remPlanilla bPlanillaNavigation { get; set; }
        public virtual isoTipoConcepto bTipoConceptoNavigation { get; set; }
        public virtual remConcepto sConceptoNavigation { get; set; }
        public virtual orgEmpresa sEmpresaNavigation { get; set; }
        public virtual remConceptoCalculoAsistencia remConceptoCalculoAsistencia { get; set; }
        public virtual remConceptoCalculoCuentas remConceptoCalculoCuentas { get; set; }
        public virtual remConceptoCalculoDias remConceptoCalculoDias { get; set; }
        public virtual remConceptoCalculoFormula remConceptoCalculoFormula { get; set; }
        public virtual remConceptoCalculoHistory remConceptoCalculoHistory { get; set; }
        public virtual remConceptoCalculoManual remConceptoCalculoManual { get; set; }
        public virtual remConceptoCalculoMonto remConceptoCalculoMonto { get; set; }
        public virtual remConceptoCalculoVigencia remConceptoCalculoVigencia { get; set; }
        [JsonIgnore] public virtual ICollection<remConceptoCalculoAfpCuentas> remConceptoCalculoAfpCuentas { get; set; }
        [JsonIgnore] public virtual ICollection<remConceptoCalculoDependencia> remConceptoCalculoDependencia { get; set; }
        [JsonIgnore] public virtual ICollection<remConceptoCalculoSumado> remConceptoCalculoSumado { get; set; }
    }
}
