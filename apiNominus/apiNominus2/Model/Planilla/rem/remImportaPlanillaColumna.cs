﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class remImportaPlanillaColumna
    {
        public byte bCalculo { get; set; }
        public short sConcepto { get; set; }
        public byte bColumna { get; set; }
    }
}
