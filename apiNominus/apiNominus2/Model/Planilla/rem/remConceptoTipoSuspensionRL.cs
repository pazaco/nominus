﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class remConceptoTipoSuspensionRL
    {
        public short sConcepto { get; set; }
        public byte bTipoSuspensionRL { get; set; }

        public virtual isoTipoSuspensionRL bTipoSuspensionRLNavigation { get; set; }
        public virtual remConcepto sConceptoNavigation { get; set; }
    }
}
