﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class remConceptoCalculoDias
    {
        public int iConceptoCalculo { get; set; }
        public short sConceptoDias { get; set; }

        public virtual remConceptoCalculo iConceptoCalculoNavigation { get; set; }
        public virtual remConcepto sConceptoDiasNavigation { get; set; }
    }
}
