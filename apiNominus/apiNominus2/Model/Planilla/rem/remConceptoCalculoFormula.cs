﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class remConceptoCalculoFormula
    {
        public int iConceptoCalculo { get; set; }
        public string xFormula { get; set; }

        public virtual remConceptoCalculo iConceptoCalculoNavigation { get; set; }
    }
}
