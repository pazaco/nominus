﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class remImportaPlanilla
    {
        public int iImportaPlanilla { get; set; }
        public short sConcepto { get; set; }
        public decimal mMonto { get; set; }

        public virtual remImportaPlanillaCabecera iImportaPlanillaNavigation { get; set; }
    }
}
