﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class remConceptoAsistencia
    {
        public remConceptoAsistencia()
        {
            remConceptoAsistenciaEmpresaPlanilla = new HashSet<remConceptoAsistenciaEmpresaPlanilla>();
        }

        public short sConcepto { get; set; }
        public byte bMaxDias { get; set; }
        public int iColor { get; set; }
        public byte bAfecta { get; set; }

        public virtual remConcepto sConceptoNavigation { get; set; }
        [JsonIgnore] public virtual ICollection<remConceptoAsistenciaEmpresaPlanilla> remConceptoAsistenciaEmpresaPlanilla { get; set; }
    }
}
