﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class remCalculoPlanillaConceptoAfecta
    {
        public int iCalculoPlanilla { get; set; }
        public short sConcepto { get; set; }

        public virtual remCalculoPlanillaConcepto remCalculoPlanillaConcepto { get; set; }
    }
}
