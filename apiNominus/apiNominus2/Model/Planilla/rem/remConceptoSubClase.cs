﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class remConceptoSubClase
    {
        public short sConcepto { get; set; }
        public byte bSubClaseConcepto { get; set; }

        public virtual isoSubClaseConcepto bSubClaseConceptoNavigation { get; set; }
        public virtual remConcepto sConceptoNavigation { get; set; }
    }
}
