﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class remPeriodo
    {
        public remPeriodo()
        {
            remCalculoPlanilla = new HashSet<remCalculoPlanilla>();
            remPeriodoTipoCambio = new HashSet<remPeriodoTipoCambio>();
            traRxH = new HashSet<traRxH>();
        }

        public int iPeriodo { get; set; }
        public short sEmpresa { get; set; }
        public byte bCalculo { get; set; }
        public byte bPlanilla { get; set; }
        public int iAnoMes { get; set; }
        public short? sAno { get; set; }
        public byte? bMes { get; set; }
        public int? iAnoNumero { get; set; }
        public string iAnoMesNumero { get; set; }
        public byte bNumero { get; set; }
        public DateTime dDesde { get; set; }
        public DateTime dHasta { get; set; }
        public DateTime dDesdeAsistencia { get; set; }
        public DateTime dHastaAsistencia { get; set; }
        public bool bEstado { get; set; }
        public bool bNotificado { get; set; }

        public virtual isoCalculo bCalculoNavigation { get; set; }
        public virtual remPlanilla bPlanillaNavigation { get; set; }
        [JsonIgnore] public virtual ICollection<remCalculoPlanilla> remCalculoPlanilla { get; set; }
        [JsonIgnore] public virtual ICollection<remPeriodoTipoCambio> remPeriodoTipoCambio { get; set; }
        [JsonIgnore] public virtual ICollection<traRxH> traRxH { get; set; }
    }
}
