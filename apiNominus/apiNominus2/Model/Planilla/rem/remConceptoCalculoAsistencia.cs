﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class remConceptoCalculoAsistencia
    {
        public int iConceptoCalculo { get; set; }
        public short sUnidadMedida { get; set; }

        public virtual remConceptoCalculo iConceptoCalculoNavigation { get; set; }
        public virtual isoUnidadMedida sUnidadMedidaNavigation { get; set; }
    }
}
