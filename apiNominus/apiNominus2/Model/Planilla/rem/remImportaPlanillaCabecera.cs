﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class remImportaPlanillaCabecera
    {
        public remImportaPlanillaCabecera()
        {
            remImportaPlanilla = new HashSet<remImportaPlanilla>();
        }

        public int iImportaPlanilla { get; set; }
        public byte bCalculo { get; set; }
        public int? iPersona { get; set; }
        public short sEmpresa { get; set; }
        public short sAno { get; set; }
        public byte bMes { get; set; }
        public byte bNumero { get; set; }
        public byte bPlanilla { get; set; }
        public string trabajador { get; set; }
        public string codigo { get; set; }
        public byte? btipodocidentidad { get; set; }
        public string xdocidentidad { get; set; }

        [JsonIgnore] public virtual ICollection<remImportaPlanilla> remImportaPlanilla { get; set; }
    }
}
