﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class remConcepto
    {
        public remConcepto()
        {
            orgEstructuraSalarialDet = new HashSet<orgEstructuraSalarialDet>();
            remCalculoPlanillaConcepto = new HashSet<remCalculoPlanillaConcepto>();
            remConceptoAsistenciaPlanilla = new HashSet<remConceptoAsistenciaPlanilla>();
            remConceptoCalculo = new HashSet<remConceptoCalculo>();
            remConceptoCalculoDependencia = new HashSet<remConceptoCalculoDependencia>();
            remConceptoCalculoDias = new HashSet<remConceptoCalculoDias>();
            remConceptoCalculoHistory = new HashSet<remConceptoCalculoHistory>();
            remConceptoCalculoManual = new HashSet<remConceptoCalculoManual>();
            remConceptoCalculoSumado = new HashSet<remConceptoCalculoSumado>();
            sysConcepto = new HashSet<sysConcepto>();
            traAsistencia = new HashSet<traAsistencia>();
            traConceptoFijo = new HashSet<traConceptoFijo>();
            traConceptoManual = new HashSet<traConceptoManual>();
            traPrestamo = new HashSet<traPrestamo>();
            traPrestamoDesembolso = new HashSet<traPrestamoDesembolso>();
        }

        public short sConcepto { get; set; }
        public string cConcepto { get; set; }
        public string xConcepto { get; set; }
        public string xTip { get; set; }
        public string xDescripcion { get; set; }
        public byte bClaseConcepto { get; set; }
        public string cConceptoSunat { get; set; }
        public byte bTipoConceptoDefault { get; set; }
        public short sUnidadMedida { get; set; }

        public virtual isoClaseConcepto bClaseConceptoNavigation { get; set; }
        public virtual isoTipoConcepto bTipoConceptoDefaultNavigation { get; set; }
        public virtual isoUnidadMedida sUnidadMedidaNavigation { get; set; }
        public virtual remConceptoAsistencia remConceptoAsistencia { get; set; }
        public virtual remConceptoSubClase remConceptoSubClase { get; set; }
        public virtual remConceptoTipoSuspensionRL remConceptoTipoSuspensionRL { get; set; }
        [JsonIgnore] public virtual ICollection<orgEstructuraSalarialDet> orgEstructuraSalarialDet { get; set; }
        [JsonIgnore] public virtual ICollection<remCalculoPlanillaConcepto> remCalculoPlanillaConcepto { get; set; }
        [JsonIgnore] public virtual ICollection<remConceptoAsistenciaPlanilla> remConceptoAsistenciaPlanilla { get; set; }
        [JsonIgnore] public virtual ICollection<remConceptoCalculo> remConceptoCalculo { get; set; }
        [JsonIgnore] public virtual ICollection<remConceptoCalculoDependencia> remConceptoCalculoDependencia { get; set; }
        [JsonIgnore] public virtual ICollection<remConceptoCalculoDias> remConceptoCalculoDias { get; set; }
        [JsonIgnore] public virtual ICollection<remConceptoCalculoHistory> remConceptoCalculoHistory { get; set; }
        [JsonIgnore] public virtual ICollection<remConceptoCalculoManual> remConceptoCalculoManual { get; set; }
        [JsonIgnore] public virtual ICollection<remConceptoCalculoSumado> remConceptoCalculoSumado { get; set; }
        [JsonIgnore] public virtual ICollection<sysConcepto> sysConcepto { get; set; }
        [JsonIgnore] public virtual ICollection<traAsistencia> traAsistencia { get; set; }
        [JsonIgnore] public virtual ICollection<traConceptoFijo> traConceptoFijo { get; set; }
        [JsonIgnore] public virtual ICollection<traConceptoManual> traConceptoManual { get; set; }
        [JsonIgnore] public virtual ICollection<traPrestamo> traPrestamo { get; set; }
        [JsonIgnore] public virtual ICollection<traPrestamoDesembolso> traPrestamoDesembolso { get; set; }
    }
}
