﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class remCalculoPlanillaConceptoCc
    {
        public int iCalculoPlanilla { get; set; }
        public short sConcepto { get; set; }
        public short sCentroCosto { get; set; }
        public byte bTurno { get; set; }
        public decimal mMonto { get; set; }

        public virtual remCalculoPlanilla iCalculoPlanillaNavigation { get; set; }
    }
}
