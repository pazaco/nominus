﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class remConceptoAsistenciaPlanilla
    {
        public short sConcepto { get; set; }
        public byte bPlanilla { get; set; }
        public byte bMaxDias { get; set; }

        public virtual remPlanilla bPlanillaNavigation { get; set; }
        public virtual remConcepto sConceptoNavigation { get; set; }
    }
}
