﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class remCalculoPlanillaConcepto
    {
        public int iCalculoPlanilla { get; set; }
        public short sConcepto { get; set; }
        public decimal mMonto { get; set; }

        public virtual remCalculoPlanilla iCalculoPlanillaNavigation { get; set; }
        public virtual remConcepto sConceptoNavigation { get; set; }
        public virtual remCalculoPlanillaConceptoAfecta remCalculoPlanillaConceptoAfecta { get; set; }
        public virtual remCalculoPlanillaConceptoCta remCalculoPlanillaConceptoCta { get; set; }
        public virtual remCalculoPlanillaConceptoFormula remCalculoPlanillaConceptoFormula { get; set; }
    }
}
