﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class remConceptoCalculoCuentas
    {
        public int iConceptoCalculo { get; set; }
        public short? sCtaDebe { get; set; }
        public short? sCtaHaber { get; set; }
        public byte bMostrarDebe { get; set; }
        public byte bMostrarHaber { get; set; }

        public virtual remConceptoCalculo iConceptoCalculoNavigation { get; set; }
        public virtual orgEmpresaCtaContable sCtaDebeNavigation { get; set; }
        public virtual orgEmpresaCtaContable sCtaHaberNavigation { get; set; }
    }
}
