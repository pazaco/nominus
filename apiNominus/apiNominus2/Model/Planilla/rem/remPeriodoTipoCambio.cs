﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class remPeriodoTipoCambio
    {
        public int iPeriodo { get; set; }
        public short sMoneda { get; set; }
        public decimal rTipoCambio { get; set; }

        public virtual remPeriodo iPeriodoNavigation { get; set; }
    }
}
