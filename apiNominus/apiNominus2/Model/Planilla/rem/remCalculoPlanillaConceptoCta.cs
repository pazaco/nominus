﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class remCalculoPlanillaConceptoCta
    {
        public int iCalculoPlanilla { get; set; }
        public short sConcepto { get; set; }
        public short? sCtaDebe { get; set; }
        public short? sCtaHaber { get; set; }

        public virtual remCalculoPlanillaConcepto remCalculoPlanillaConcepto { get; set; }
        public virtual orgEmpresaCtaContable sCtaDebeNavigation { get; set; }
        public virtual orgEmpresaCtaContable sCtaHaberNavigation { get; set; }
    }
}
