﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class remConceptoCalculoVigencia
    {
        public int iConceptoCalculo { get; set; }
        public DateTime? dDesde { get; set; }
        public DateTime? dHasta { get; set; }

        public virtual remConceptoCalculo iConceptoCalculoNavigation { get; set; }
    }
}
