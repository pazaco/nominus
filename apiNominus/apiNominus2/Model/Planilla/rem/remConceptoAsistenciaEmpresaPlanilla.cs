﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class remConceptoAsistenciaEmpresaPlanilla
    {
        public short sConcepto { get; set; }
        public short sEmpresa { get; set; }
        public byte bPlanilla { get; set; }
        public byte bMaxDias { get; set; }

        public virtual remPlanilla bPlanillaNavigation { get; set; }
        public virtual remConceptoAsistencia sConceptoNavigation { get; set; }
        public virtual orgEmpresa sEmpresaNavigation { get; set; }
    }
}
