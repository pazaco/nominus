﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class remConceptoCalculoDependencia
    {
        public int iConceptoCalculo { get; set; }
        public short sConcepto { get; set; }

        public virtual remConceptoCalculo iConceptoCalculoNavigation { get; set; }
        public virtual remConcepto sConceptoNavigation { get; set; }
    }
}
