﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class remConceptoCalculoManual
    {
        public int iConceptoCalculo { get; set; }
        public short sConceptoManual { get; set; }

        public virtual remConceptoCalculo iConceptoCalculoNavigation { get; set; }
        public virtual remConcepto sConceptoManualNavigation { get; set; }
    }
}
