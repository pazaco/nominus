﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class remCalculoPlanilla
    {
        public remCalculoPlanilla()
        {
            remCalculoPlanillaConcepto = new HashSet<remCalculoPlanillaConcepto>();
            remCalculoPlanillaConceptoCc = new HashSet<remCalculoPlanillaConceptoCc>();
            traAsistenciaFechaCalculo = new HashSet<traAsistenciaFechaCalculo>();
            traAsistenciaHoraCalculo = new HashSet<traAsistenciaHoraCalculo>();
            traPrestamoCalculo = new HashSet<traPrestamoCalculo>();
            traPrestamoCuotaCalculo = new HashSet<traPrestamoCuotaCalculo>();
        }

        public int iCalculoPlanilla { get; set; }
        public int iPeriodo { get; set; }
        public int iContrato { get; set; }
        public DateTime dCreacion { get; set; }
        public int iUsuarioReg { get; set; }

        public virtual traContrato iContratoNavigation { get; set; }
        public virtual remPeriodo iPeriodoNavigation { get; set; }
        [JsonIgnore] public virtual ICollection<remCalculoPlanillaConcepto> remCalculoPlanillaConcepto { get; set; }
        [JsonIgnore] public virtual ICollection<remCalculoPlanillaConceptoCc> remCalculoPlanillaConceptoCc { get; set; }
        [JsonIgnore] public virtual ICollection<traAsistenciaFechaCalculo> traAsistenciaFechaCalculo { get; set; }
        [JsonIgnore] public virtual ICollection<traAsistenciaHoraCalculo> traAsistenciaHoraCalculo { get; set; }
        [JsonIgnore] public virtual ICollection<traPrestamoCalculo> traPrestamoCalculo { get; set; }
        [JsonIgnore] public virtual ICollection<traPrestamoCuotaCalculo> traPrestamoCuotaCalculo { get; set; }
    }
}
