﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class remConceptoCalculoHistory
    {
        public int iConceptoCalculo { get; set; }
        public byte bCalculo { get; set; }
        public byte bAnos { get; set; }
        public byte bTipoAnos { get; set; }
        public byte bPeriodos { get; set; }
        public byte bTipoPeriodos { get; set; }
        public byte bActualConsiderar { get; set; }
        public byte? bDiaEspecifico { get; set; }
        public byte bTipoInfo { get; set; }
        public byte bDivideEntre { get; set; }
        public byte bMinConsiderar { get; set; }
        public byte bFuente { get; set; }
        public short? sConcepto { get; set; }
        public byte bIngreso { get; set; }
        public byte bAfecta { get; set; }
        public byte bCalculoDeclara { get; set; }

        public virtual isoCalculo bCalculoNavigation { get; set; }
        public virtual remConceptoCalculo iConceptoCalculoNavigation { get; set; }
        public virtual remConcepto sConceptoNavigation { get; set; }
    }
}
