﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class remConceptoCalculoMonto
    {
        public int iConceptoCalculo { get; set; }
        public decimal mMonto { get; set; }

        public virtual remConceptoCalculo iConceptoCalculoNavigation { get; set; }
    }
}
