﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class remPlanilla
    {
        public remPlanilla()
        {
            orgEmpresaCalculoCtaContableNeto = new HashSet<orgEmpresaCalculoCtaContableNeto>();
            remConceptoAsistenciaEmpresaPlanilla = new HashSet<remConceptoAsistenciaEmpresaPlanilla>();
            remConceptoAsistenciaPlanilla = new HashSet<remConceptoAsistenciaPlanilla>();
            remConceptoCalculo = new HashSet<remConceptoCalculo>();
            remPeriodo = new HashSet<remPeriodo>();
            traContratoLaboral = new HashSet<traContratoLaboral>();
        }

        public byte bPlanilla { get; set; }
        public string xPlanilla { get; set; }
        public byte bTipoPlanilla { get; set; }
        public byte bDeclarar { get; set; }

        public virtual isoTipoPlanilla bTipoPlanillaNavigation { get; set; }
        [JsonIgnore] public virtual ICollection<orgEmpresaCalculoCtaContableNeto> orgEmpresaCalculoCtaContableNeto { get; set; }
        [JsonIgnore] public virtual ICollection<remConceptoAsistenciaEmpresaPlanilla> remConceptoAsistenciaEmpresaPlanilla { get; set; }
        [JsonIgnore] public virtual ICollection<remConceptoAsistenciaPlanilla> remConceptoAsistenciaPlanilla { get; set; }
        [JsonIgnore] public virtual ICollection<remConceptoCalculo> remConceptoCalculo { get; set; }
        [JsonIgnore] public virtual ICollection<remPeriodo> remPeriodo { get; set; }
        [JsonIgnore] public virtual ICollection<traContratoLaboral> traContratoLaboral { get; set; }
    }
}
