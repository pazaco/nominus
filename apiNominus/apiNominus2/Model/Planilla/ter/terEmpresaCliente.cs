﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class terEmpresaCliente
    {
        public short sEmpresa { get; set; }
        public int iUsuarioReg { get; set; }
        public DateTime dFechaReg { get; set; }

        public virtual terEmpresa sEmpresaNavigation { get; set; }
    }
}
