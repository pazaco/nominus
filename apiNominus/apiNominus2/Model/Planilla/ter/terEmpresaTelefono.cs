﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class terEmpresaTelefono
    {
        public short sEmpresa { get; set; }
        public string xTelefono { get; set; }

        public virtual terEmpresa sEmpresaNavigation { get; set; }
    }
}
