﻿using Microsoft.EntityFrameworkCore;

#nullable disable

namespace apiNominus2.Model
{
    public partial class PlanillaContext : DbContext
    {
        public virtual DbSet<terEmpresa> terEmpresa { get; set; }
        public virtual DbSet<terEmpresaAseguradoraSalud> terEmpresaAseguradoraSalud { get; set; }
        public virtual DbSet<terEmpresaCliente> terEmpresaCliente { get; set; }
        public virtual DbSet<terEmpresaDireccion> terEmpresaDireccion { get; set; }
        public virtual DbSet<terEmpresaTelefono> terEmpresaTelefono { get; set; }

        partial void OnModelCreatingPartialTer(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<terEmpresa>(entity =>
            {
                entity.HasKey(e => e.sEmpresa);

                entity.HasIndex(e => e.cRUC, "IX_terEmpresa")
                    .IsUnique();

                entity.Property(e => e.cRUC)
                    .IsRequired()
                    .HasMaxLength(11)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.xAbreviado)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.xNombreComercial)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.xRazonSocial)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<terEmpresaAseguradoraSalud>(entity =>
            {
                entity.HasKey(e => e.sEmpresa);

                entity.Property(e => e.sEmpresa).ValueGeneratedNever();

                entity.Property(e => e.dFechaReg)
                    .HasPrecision(0)
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.sEmpresaNavigation)
                    .WithOne(p => p.terEmpresaAseguradoraSalud)
                    .HasForeignKey<terEmpresaAseguradoraSalud>(d => d.sEmpresa)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_terEmpresaAseguradoraSalud_terEmpresa");
            });
            modelBuilder.Entity<terEmpresaCliente>(entity =>
            {
                entity.HasKey(e => e.sEmpresa);

                entity.Property(e => e.sEmpresa).ValueGeneratedNever();

                entity.Property(e => e.dFechaReg)
                    .HasPrecision(0)
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.sEmpresaNavigation)
                    .WithOne(p => p.terEmpresaCliente)
                    .HasForeignKey<terEmpresaCliente>(d => d.sEmpresa)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_terEmpresaCliente_terEmpresa");
            });
            modelBuilder.Entity<terEmpresaDireccion>(entity =>
            {
                entity.HasKey(e => e.sEmpresa);

                entity.Property(e => e.sEmpresa).ValueGeneratedNever();

                entity.Property(e => e.bTipoVia).HasComment("Calle, Jirón, Avenida, etc.");

                entity.Property(e => e.xBlock)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xDpto)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xEtapa)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xInterior)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xKilometro)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xLote)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xManzana)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xNombreVia)
                    .HasMaxLength(300)
                    .IsUnicode(false)
                    .HasComment("Aqui guarda el concatenado, si no se pudo migrar por separado");

                entity.Property(e => e.xNombreZona)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.xNroVia)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.xReferencia)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.bTipoViaNavigation)
                    .WithMany(p => p.terEmpresaDireccion)
                    .HasForeignKey(d => d.bTipoVia)
                    .HasConstraintName("FK_terEmpresaDireccion_isoTipoVia");

                entity.HasOne(d => d.bTipoZonaNavigation)
                    .WithMany(p => p.terEmpresaDireccion)
                    .HasForeignKey(d => d.bTipoZona)
                    .HasConstraintName("FK_terEmpresaDireccion_isoTipoZona");

                entity.HasOne(d => d.sDistritoNavigation)
                    .WithMany(p => p.terEmpresaDireccion)
                    .HasForeignKey(d => d.sDistrito)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_terEmpresaDireccion_isoDistrito");

                entity.HasOne(d => d.sEmpresaNavigation)
                    .WithOne(p => p.terEmpresaDireccion)
                    .HasForeignKey<terEmpresaDireccion>(d => d.sEmpresa)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_terEmpresaDireccion_terEmpresa");
            });
            modelBuilder.Entity<terEmpresaTelefono>(entity =>
            {
                entity.HasKey(e => new { e.sEmpresa, e.xTelefono });

                entity.Property(e => e.xTelefono)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.HasOne(d => d.sEmpresaNavigation)
                    .WithMany(p => p.terEmpresaTelefono)
                    .HasForeignKey(d => d.sEmpresa)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_terEmpresaTelefono_terEmpresa");
            });
        }
    }
}