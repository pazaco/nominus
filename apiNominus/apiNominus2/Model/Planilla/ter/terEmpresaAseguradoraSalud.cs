﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class terEmpresaAseguradoraSalud
    {
        public terEmpresaAseguradoraSalud()
        {
            sysPlanEPS = new HashSet<sysPlanEPS>();
            traVidaLey = new HashSet<traVidaLey>();
        }

        public short sEmpresa { get; set; }
        public int iUsuarioReg { get; set; }
        public DateTime dFechaReg { get; set; }

        public virtual terEmpresa sEmpresaNavigation { get; set; }
        [JsonIgnore] public virtual ICollection<sysPlanEPS> sysPlanEPS { get; set; }
        [JsonIgnore] public virtual ICollection<traVidaLey> traVidaLey { get; set; }
    }
}
