﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class terEmpresa
    {
        public terEmpresa()
        {
            terEmpresaTelefono = new HashSet<terEmpresaTelefono>();
        }

        public short sEmpresa { get; set; }
        public string cRUC { get; set; }
        public string xRazonSocial { get; set; }
        public string xNombreComercial { get; set; }
        public string xAbreviado { get; set; }

        public virtual terEmpresaAseguradoraSalud terEmpresaAseguradoraSalud { get; set; }
        public virtual terEmpresaCliente terEmpresaCliente { get; set; }
        public virtual terEmpresaDireccion terEmpresaDireccion { get; set; }
        [JsonIgnore] public virtual ICollection<terEmpresaTelefono> terEmpresaTelefono { get; set; }
    }
}
