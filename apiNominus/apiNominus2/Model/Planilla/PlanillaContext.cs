﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Configuration;

#nullable disable

namespace apiNominus2.Model
{
    public partial class PlanillaContext : DbContext
    {

        private int iCliente;
        private short sProducto;
        private byte bModulo;
        private bool _Connected = false;

        public PlanillaContext(int iCliente, short sProducto, byte bModulo)
        {
            this.iCliente = iCliente;
            this.bModulo = bModulo;
            this.sProducto = sProducto;
        }


        public PlanillaContext(DbContextOptions<AsistenciaContext> options)
            : base(options)
        {
        }

        public bool isConnected()
        {
            return _Connected;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                ZasStringConexion strConn = Procs.ZasConexion(iCliente,sProducto,bModulo);
                if (strConn == null)
                {
                    _Connected = false;
                }
                else
                {
                    _Connected = true;
                    optionsBuilder.UseSqlServer(strConn.xConexion);
                }


            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            OnModelCreatingPartialSys(modelBuilder);
            OnModelCreatingPartialRem(modelBuilder);
            OnModelCreatingPartialTer(modelBuilder);
        }

        partial void OnModelCreatingPartialSys(ModelBuilder modelBuilder);
        partial void OnModelCreatingPartialRem(ModelBuilder modelBuilder);
        partial void OnModelCreatingPartialTer(ModelBuilder modelBuilder);

    }
}
