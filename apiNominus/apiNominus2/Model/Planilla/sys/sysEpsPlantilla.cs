﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class sysEpsPlantilla
    {
        public sysEpsPlantilla()
        {
            sysPlanEPSdet = new HashSet<sysPlanEPSdet>();
        }

        public byte bEpsPlantilla { get; set; }
        public string xEpsPlantilla { get; set; }
        public bool bDefault { get; set; }
        public bool bPermiteCantidad { get; set; }
        public bool? bContarParaFactor { get; set; }
        public bool? bContarParaCalculo { get; set; }
        public byte bAumentarIGV { get; set; }
        public bool bNoAsumeEmpresa { get; set; }

        [JsonIgnore] public virtual ICollection<sysPlanEPSdet> sysPlanEPSdet { get; set; }
    }
}
