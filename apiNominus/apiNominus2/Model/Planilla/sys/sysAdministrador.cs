﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class sysAdministrador
    {
        public int iPersona { get; set; }
        public DateTime? dVigencia { get; set; }
    }
}
