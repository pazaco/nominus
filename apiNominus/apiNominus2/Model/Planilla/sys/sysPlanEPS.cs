﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class sysPlanEPS
    {
        public sysPlanEPS()
        {
            orgEmpresaPlanEPS = new HashSet<orgEmpresaPlanEPS>();
            sysPlanEPSdependenciasPlanEPSPadreNavigation = new HashSet<sysPlanEPSdependencia>();
            sysPlanEPSdet = new HashSet<sysPlanEPSdet>();
        }

        public short sPlanEPS { get; set; }
        public short sAseguradora { get; set; }
        public string xPlanEPS { get; set; }
        public int iUsuarioReg { get; set; }
        public DateTime dFechaReg { get; set; }

        public virtual terEmpresaAseguradoraSalud sAseguradoraNavigation { get; set; }
        public virtual sysPlanEPSdependencia sysPlanEPSdependenciasPlanEPSNavigation { get; set; }
        [JsonIgnore] public virtual ICollection<orgEmpresaPlanEPS> orgEmpresaPlanEPS { get; set; }
        [JsonIgnore] public virtual ICollection<sysPlanEPSdependencia> sysPlanEPSdependenciasPlanEPSPadreNavigation { get; set; }
        [JsonIgnore] public virtual ICollection<sysPlanEPSdet> sysPlanEPSdet { get; set; }
    }
}
