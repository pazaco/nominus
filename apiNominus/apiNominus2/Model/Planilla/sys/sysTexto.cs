﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class sysTexto
    {
        public short sSysTexto { get; set; }
        public string xSysTexto { get; set; }
        public string xDescripcion { get; set; }
        public string xTexto { get; set; }
    }
}
