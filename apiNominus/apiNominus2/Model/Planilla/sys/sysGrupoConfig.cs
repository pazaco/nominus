﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class sysGrupoConfig
    {
        public sysGrupoConfig()
        {
            sysConfig = new HashSet<sysConfig>();
        }

        public byte bGrupoConfig { get; set; }
        public string xGrupoConfig { get; set; }
        public byte bOrden { get; set; }

        [JsonIgnore] public virtual ICollection<sysConfig> sysConfig { get; set; }
    }
}
