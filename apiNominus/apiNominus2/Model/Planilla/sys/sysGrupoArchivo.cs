﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class sysGrupoArchivo
    {
        public sysGrupoArchivo()
        {
            sysArchivo = new HashSet<sysArchivo>();
        }

        public byte bGrupoArchivo { get; set; }
        public string xGrupoArchivo { get; set; }
        public byte bOrden { get; set; }

        [JsonIgnore] public virtual ICollection<sysArchivo> sysArchivo { get; set; }
    }
}
