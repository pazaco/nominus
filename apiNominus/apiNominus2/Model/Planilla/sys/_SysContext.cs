﻿using Microsoft.EntityFrameworkCore;

#nullable disable

namespace apiNominus2.Model
{
    public partial class PlanillaContext : DbContext
    {
        public virtual DbSet<sysAccesoMenu> sysAccesoMenu { get; set; }
        public virtual DbSet<sysAccesoTab> sysAccesoTab { get; set; }
        public virtual DbSet<sysAdministrador> sysAdministrador { get; set; }
        public virtual DbSet<sysArchivo> sysArchivo { get; set; }
        public virtual DbSet<sysConcepto> sysConcepto { get; set; }
        public virtual DbSet<sysConfig> sysConfig { get; set; }
        public virtual DbSet<sysContratoFormatoCampo> sysContratoFormatoCampo { get; set; }
        public virtual DbSet<sysEpsPlantilla> sysEpsPlantilla { get; set; }
        public virtual DbSet<sysGrupoArchivo> sysGrupoArchivo { get; set; }
        public virtual DbSet<sysGrupoConfig> sysGrupoConfig { get; set; }
        public virtual DbSet<sysGrupoReporte> sysGrupoReporte { get; set; }
        public virtual DbSet<sysMonto> sysMonto { get; set; }
        public virtual DbSet<sysMontoAnual> sysMontoAnual { get; set; }
        public virtual DbSet<sysMontoEmpresa> sysMontoEmpresa { get; set; }
        public virtual DbSet<sysMontoFecha> sysMontoFecha { get; set; }
        public virtual DbSet<sysMontoGrupo> sysMontoGrupo { get; set; }
        public virtual DbSet<sysOrigenArchivo> sysOrigenArchivo { get; set; }
        public virtual DbSet<sysPistaAuditoria> sysPistaAuditoria { get; set; }
        public virtual DbSet<sysPlanEPS> sysPlanEPS { get; set; }
        public virtual DbSet<sysPlanEPSdependencia> sysPlanEPSdependencia { get; set; }
        public virtual DbSet<sysPlanEPSdet> sysPlanEPSdet { get; set; }
        public virtual DbSet<sysReporte> sysReporte { get; set; }
        public virtual DbSet<sysTexto> sysTexto { get; set; }

        partial void OnModelCreatingPartialSys(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<sysAccesoMenu>(entity =>
            {
                entity.HasKey(e => e.sMenu);

                entity.Property(e => e.sMenu).ValueGeneratedNever();

                entity.Property(e => e.xMenu)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.xNombre)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<sysAccesoTab>(entity =>
            {
                entity.HasKey(e => e.iTab);

                entity.Property(e => e.iTab).ValueGeneratedNever();

                entity.Property(e => e.bAcceso)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.bAgregar)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.bEditar)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.bEliminar)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.xNombre)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.sMenuNavigation)
                    .WithMany(p => p.sysAccesoTab)
                    .HasForeignKey(d => d.sMenu)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_sysAccesoTab_sysAccesoMenu");
            });
            modelBuilder.Entity<sysAdministrador>(entity =>
            {
                entity.HasKey(e => e.iPersona);

                entity.Property(e => e.iPersona).ValueGeneratedNever();

                entity.Property(e => e.dVigencia).HasColumnType("date");
            });
            modelBuilder.Entity<sysArchivo>(entity =>
            {
                entity.HasKey(e => e.sArchivo);

                entity.Property(e => e.sArchivo).ValueGeneratedNever();

                entity.Property(e => e.bEstado)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.xArchivo)
                    .IsRequired()
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.xExtension)
                    .IsRequired()
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('TXT')");

                entity.Property(e => e.xObjectDw)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.bGrupoArchivoNavigation)
                    .WithMany(p => p.sysArchivo)
                    .HasForeignKey(d => d.bGrupoArchivo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_sysArchivo_sysGrupoArchivo");
            });
            modelBuilder.Entity<sysConcepto>(entity =>
            {
                entity.HasKey(e => e.sSysConcepto)
                    .HasName("PK_SysConcepto");

                entity.Property(e => e.sSysConcepto).ValueGeneratedNever();

                entity.Property(e => e.xDescripcion)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.xSysConcepto)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.sConceptoNavigation)
                    .WithMany(p => p.sysConcepto)
                    .HasForeignKey(d => d.sConcepto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SysConcepto_remConcepto");
            });
            modelBuilder.Entity<sysConfig>(entity =>
            {
                entity.HasKey(e => e.sConfig);

                entity.Property(e => e.sConfig).ValueGeneratedNever();

                entity.Property(e => e.xConfig)
                    .IsRequired()
                    .HasMaxLength(59)
                    .IsUnicode(false);

                entity.Property(e => e.xDato)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.bGrupoConfigNavigation)
                    .WithMany(p => p.sysConfig)
                    .HasForeignKey(d => d.bGrupoConfig)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_sysConfig_sysGrupoConfig");
            });
            modelBuilder.Entity<sysContratoFormatoCampo>(entity =>
            {
                entity.HasKey(e => e.sCampo);

                entity.Property(e => e.sCampo).ValueGeneratedNever();

                entity.Property(e => e.xCampo)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.xDescripcion)
                    .IsRequired()
                    .IsUnicode(false);
            });
            modelBuilder.Entity<sysEpsPlantilla>(entity =>
            {
                entity.HasKey(e => e.bEpsPlantilla);

                entity.Property(e => e.bAumentarIGV)
                    .HasDefaultValueSql("((1))")
                    .HasComment("1:Sumar IGV, 0:No (nada), 2: Restar IGV");

                entity.Property(e => e.bContarParaCalculo)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.bContarParaFactor)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.xEpsPlantilla)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<sysGrupoArchivo>(entity =>
            {
                entity.HasKey(e => e.bGrupoArchivo);

                entity.Property(e => e.xGrupoArchivo)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<sysGrupoConfig>(entity =>
            {
                entity.HasKey(e => e.bGrupoConfig);

                entity.Property(e => e.xGrupoConfig)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<sysGrupoReporte>(entity =>
            {
                entity.HasKey(e => e.bGrupoReporte);

                entity.Property(e => e.xGrupoReporte)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<sysMonto>(entity =>
            {
                entity.HasKey(e => e.sSysMonto)
                    .HasName("PK_SysMonto");

                entity.Property(e => e.sSysMonto).ValueGeneratedNever();

                entity.Property(e => e.dMonto).HasColumnType("decimal(18, 6)");

                entity.Property(e => e.xDescripcion)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.xSysMonto)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.bMontoGrupoNavigation)
                    .WithMany(p => p.sysMonto)
                    .HasForeignKey(d => d.bMontoGrupo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_sysMonto_sysMontoGrupo");
            });
            modelBuilder.Entity<sysMontoAnual>(entity =>
            {
                entity.HasKey(e => new { e.sSysMonto, e.sAno })
                    .HasName("PK_SysMontoAnual");

                entity.Property(e => e.dMonto).HasColumnType("decimal(18, 6)");

                entity.HasOne(d => d.sSysMontoNavigation)
                    .WithMany(p => p.sysMontoAnual)
                    .HasForeignKey(d => d.sSysMonto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SysMontoAnual_SysMonto");
            });
            modelBuilder.Entity<sysMontoEmpresa>(entity =>
            {
                entity.HasKey(e => new { e.sEmpresa, e.sSysMonto })
                    .HasName("PK_SysMontoEmpresa");

                entity.Property(e => e.dMonto).HasColumnType("decimal(18, 6)");

                entity.HasOne(d => d.sEmpresaNavigation)
                    .WithMany(p => p.sysMontoEmpresa)
                    .HasForeignKey(d => d.sEmpresa)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SysMontoEmpresa_orgEmpresa");

                entity.HasOne(d => d.sSysMontoNavigation)
                    .WithMany(p => p.sysMontoEmpresa)
                    .HasForeignKey(d => d.sSysMonto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SysMontoEmpresa_SysMonto");
            });
            modelBuilder.Entity<sysMontoFecha>(entity =>
            {
                entity.HasKey(e => new { e.sSysMonto, e.dDesde });

                entity.Property(e => e.dDesde).HasColumnType("date");

                entity.Property(e => e.dMonto).HasColumnType("decimal(18, 6)");

                entity.HasOne(d => d.sSysMontoNavigation)
                    .WithMany(p => p.sysMontoFecha)
                    .HasForeignKey(d => d.sSysMonto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_sysMontoFecha_sysMonto");
            });
            modelBuilder.Entity<sysMontoGrupo>(entity =>
            {
                entity.HasKey(e => e.bMontoGrupo);

                entity.Property(e => e.xMontoGrupo)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<sysOrigenArchivo>(entity =>
            {
                entity.HasKey(e => e.sOrigenArchivo);

                entity.Property(e => e.sOrigenArchivo).HasComment("Mostrar en el combo para seleccionar en el mantenimiento");

                entity.Property(e => e.dFechaReg)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.xOrigenArchivo)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.xTabla)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<sysPistaAuditoria>(entity =>
            {
                entity.HasKey(e => e.iPistaAuditoria);

                entity.Property(e => e.bAccion).HasComment("1:Insert, 2: Update, 3: Delete");

                entity.Property(e => e.dtFechaHora)
                    .HasPrecision(0)
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.xDato)
                    .IsRequired()
                    .IsUnicode(false);
            });
            modelBuilder.Entity<sysPlanEPS>(entity =>
            {
                entity.HasKey(e => e.sPlanEPS);

                entity.Property(e => e.dFechaReg)
                    .HasPrecision(0)
                    .HasDefaultValueSql("(getdate())")
                    .HasComment("Fecha y hora del último afecto al registro (insertó o modificó)");

                entity.Property(e => e.iUsuarioReg).HasComment("Último Usuario que afecto al registro (insertó o modificó)");

                entity.Property(e => e.xPlanEPS)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.sAseguradoraNavigation)
                    .WithMany(p => p.sysPlanEPS)
                    .HasForeignKey(d => d.sAseguradora)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_sysPlanEPS_terEmpresaAseguradoraSalud");
            });
            modelBuilder.Entity<sysPlanEPSdependencia>(entity =>
            {
                entity.HasKey(e => e.sPlanEPS);

                entity.Property(e => e.sPlanEPS).ValueGeneratedNever();

                entity.HasOne(d => d.sPlanEPSNavigation)
                    .WithOne(p => p.sysPlanEPSdependenciasPlanEPSNavigation)
                    .HasForeignKey<sysPlanEPSdependencia>(d => d.sPlanEPS)
                    .HasConstraintName("FK_sysPlanEPSdependencia_sysPlanEPS");

                entity.HasOne(d => d.sPlanEPSPadreNavigation)
                    .WithMany(p => p.sysPlanEPSdependenciasPlanEPSPadreNavigation)
                    .HasForeignKey(d => d.sPlanEPSPadre)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_sysPlanEPSdependencia_sysPlanEPS1");
            });
            modelBuilder.Entity<sysPlanEPSdet>(entity =>
            {
                entity.HasKey(e => e.sPlanEPSdet);

                entity.Property(e => e.bEpsPlantilla).HasComment("1:Titular, 2:Conyuge, 3:Hijos <, 4:Hijos >, 5:Padres, 6:Padres mayores");

                entity.Property(e => e.dFechaReg)
                    .HasPrecision(0)
                    .HasDefaultValueSql("(getdate())")
                    .HasComment("Fecha y hora del último afecto al registro (insertó o modificó)");

                entity.Property(e => e.iUsuarioReg).HasComment("Último Usuario que afecto al registro (insertó o modificó)");

                entity.Property(e => e.mValor).HasColumnType("money");

                entity.HasOne(d => d.bEpsPlantillaNavigation)
                    .WithMany(p => p.sysPlanEPSdet)
                    .HasForeignKey(d => d.bEpsPlantilla)
                    .HasConstraintName("FK_sysPlanEPSdet_sysEpsPlantilla");

                entity.HasOne(d => d.sPlanEPSNavigation)
                    .WithMany(p => p.sysPlanEPSdet)
                    .HasForeignKey(d => d.sPlanEPS)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_sysPlanEPSdet_sysPlanEPS");
            });
            modelBuilder.Entity<sysReporte>(entity =>
            {
                entity.HasKey(e => e.sReporte);

                entity.Property(e => e.sReporte).ValueGeneratedNever();

                entity.Property(e => e.bEstado)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.xObjectDw)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.xReporte)
                    .IsRequired()
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.HasOne(d => d.bGrupoReporteNavigation)
                    .WithMany(p => p.sysReporte)
                    .HasForeignKey(d => d.bGrupoReporte)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_sysReporte_sysGrupoReporte");
            });
            modelBuilder.Entity<sysTexto>(entity =>
            {
                entity.HasKey(e => e.sSysTexto)
                    .HasName("PK_SysTexto");

                entity.Property(e => e.sSysTexto).ValueGeneratedNever();

                entity.Property(e => e.xDescripcion)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.xSysTexto)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.xTexto)
                    .IsRequired()
                    .IsUnicode(false);
            });

        }
    }
}