﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class sysOrigenArchivo
    {
        public sysOrigenArchivo()
        {
            perArchivo = new HashSet<perArchivo>();
        }

        public short sOrigenArchivo { get; set; }
        public string xOrigenArchivo { get; set; }
        public string xTabla { get; set; }
        public bool bMostrar { get; set; }
        public int iUsuarioReg { get; set; }
        public DateTime dFechaReg { get; set; }

        [JsonIgnore] public virtual ICollection<perArchivo> perArchivo { get; set; }
    }
}
