﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class sysContratoFormatoCampo
    {
        public short sCampo { get; set; }
        public string xCampo { get; set; }
        public string xDescripcion { get; set; }
    }
}
