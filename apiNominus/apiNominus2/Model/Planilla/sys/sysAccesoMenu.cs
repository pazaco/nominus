﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class sysAccesoMenu
    {
        public sysAccesoMenu()
        {
            sysAccesoTab = new HashSet<sysAccesoTab>();
        }

        public short sMenu { get; set; }
        public string xNombre { get; set; }
        public string xMenu { get; set; }
        public short sOrden { get; set; }

        [JsonIgnore] public virtual ICollection<sysAccesoTab> sysAccesoTab { get; set; }
    }
}
