﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class sysMontoAnual
    {
        public short sSysMonto { get; set; }
        public short sAno { get; set; }
        public decimal dMonto { get; set; }

        public virtual sysMonto sSysMontoNavigation { get; set; }
    }
}
