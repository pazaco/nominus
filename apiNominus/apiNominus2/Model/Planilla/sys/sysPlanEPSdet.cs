﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class sysPlanEPSdet
    {
        public sysPlanEPSdet()
        {
            traEPSdet = new HashSet<traEPSdet>();
        }

        public short sPlanEPSdet { get; set; }
        public short sPlanEPS { get; set; }
        public byte bEpsPlantilla { get; set; }
        public decimal mValor { get; set; }
        public int iUsuarioReg { get; set; }
        public DateTime dFechaReg { get; set; }

        public virtual sysEpsPlantilla bEpsPlantillaNavigation { get; set; }
        public virtual sysPlanEPS sPlanEPSNavigation { get; set; }
        [JsonIgnore] public virtual ICollection<traEPSdet> traEPSdet { get; set; }
    }
}
