﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class sysPistaAuditoria
    {
        public long iPistaAuditoria { get; set; }
        public int iSP { get; set; }
        public byte bAccion { get; set; }
        public int iTabla { get; set; }
        public byte bColumna { get; set; }
        public int iUsuario { get; set; }
        public int iId { get; set; }
        public string xDato { get; set; }
        public DateTime dtFechaHora { get; set; }
    }
}
