﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class sysMonto
    {
        public sysMonto()
        {
            sysMontoAnual = new HashSet<sysMontoAnual>();
            sysMontoEmpresa = new HashSet<sysMontoEmpresa>();
            sysMontoFecha = new HashSet<sysMontoFecha>();
        }

        public short sSysMonto { get; set; }
        public string xSysMonto { get; set; }
        public string xDescripcion { get; set; }
        public decimal dMonto { get; set; }
        public byte bMontoGrupo { get; set; }

        public virtual sysMontoGrupo bMontoGrupoNavigation { get; set; }
        [JsonIgnore] public virtual ICollection<sysMontoAnual> sysMontoAnual { get; set; }
        [JsonIgnore] public virtual ICollection<sysMontoEmpresa> sysMontoEmpresa { get; set; }
        [JsonIgnore] public virtual ICollection<sysMontoFecha> sysMontoFecha { get; set; }
    }
}
