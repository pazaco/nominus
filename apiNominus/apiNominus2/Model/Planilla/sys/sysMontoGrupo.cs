﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class sysMontoGrupo
    {
        public sysMontoGrupo()
        {
            sysMonto = new HashSet<sysMonto>();
        }

        public byte bMontoGrupo { get; set; }
        public string xMontoGrupo { get; set; }
        public byte bOrden { get; set; }

        [JsonIgnore] public virtual ICollection<sysMonto> sysMonto { get; set; }
    }
}
