﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class sysGrupoReporte
    {
        public sysGrupoReporte()
        {
            sysReporte = new HashSet<sysReporte>();
        }

        public byte bGrupoReporte { get; set; }
        public string xGrupoReporte { get; set; }
        public byte bOrden { get; set; }

        [JsonIgnore] public virtual ICollection<sysReporte> sysReporte { get; set; }
    }
}
