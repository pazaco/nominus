﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class sysMontoEmpresa
    {
        public short sEmpresa { get; set; }
        public short sSysMonto { get; set; }
        public decimal dMonto { get; set; }

        public virtual orgEmpresa sEmpresaNavigation { get; set; }
        public virtual sysMonto sSysMontoNavigation { get; set; }
    }
}
