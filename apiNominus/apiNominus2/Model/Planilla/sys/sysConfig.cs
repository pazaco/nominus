﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class sysConfig
    {
        public short sConfig { get; set; }
        public string xConfig { get; set; }
        public byte bGrupoConfig { get; set; }
        public string xDato { get; set; }

        public virtual sysGrupoConfig bGrupoConfigNavigation { get; set; }
    }
}
