﻿using System;
using System.Collections.Generic;

#nullable disable

namespace apiNominus2.Model
{
    public partial class sysPlanEPSdependencia
    {
        public short sPlanEPS { get; set; }
        public short sPlanEPSPadre { get; set; }

        public virtual sysPlanEPS sPlanEPSNavigation { get; set; }
        public virtual sysPlanEPS sPlanEPSPadreNavigation { get; set; }
    }
}
