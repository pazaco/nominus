﻿using System;
using System.Collections.Generic;

namespace api4Nominus.Models.asistencia
{
    public partial class horMargenIncidencia
    {
        public string iMargen { get; set; }
        public short sMargen { get; set; }
        public string sDiferencia { get; set; }
        public int? iDiferencia { get; set; }
        public short? sIncidencia { get; set; }

        public virtual horIncidencia sIncidenciaNavigation { get; set; }
        public virtual horMargen sMargenNavigation { get; set; }
    }
}
