﻿using System;
using System.Collections.Generic;

namespace api4Nominus.Models.asistencia
{
    public partial class horRequerimientoMarca
    {
        public horRequerimientoMarca()
        {
            horJornada = new HashSet<horJornada>();
        }

        public byte bRequerimientoMarca { get; set; }
        public string xRequerimientoMarca { get; set; }

        public virtual ICollection<horJornada> horJornada { get; set; }
    }
}
