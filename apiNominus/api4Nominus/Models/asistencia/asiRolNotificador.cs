﻿using System;
using System.Collections.Generic;

namespace api4Nominus.Models.asistencia
{
    public partial class asiRolNotificador
    {
        public asiRolNotificador()
        {
            asiFlujoPapeleta = new HashSet<asiFlujoPapeleta>();
        }

        public string cRol { get; set; }
        public string xRol { get; set; }

        public virtual ICollection<asiFlujoPapeleta> asiFlujoPapeleta { get; set; }
    }
}
