﻿using System;
using System.Collections.Generic;

namespace api4Nominus.Models.asistencia
{
    public partial class asiTipoFlujoPapeleta
    {
        public asiTipoFlujoPapeleta()
        {
            asiFlujoPapeleta = new HashSet<asiFlujoPapeleta>();
        }

        public string cTipoFlujo { get; set; }
        public string xTipoFlujo { get; set; }

        public virtual ICollection<asiFlujoPapeleta> asiFlujoPapeleta { get; set; }
    }
}
