﻿using System;
using System.Collections.Generic;

namespace api4Nominus.Models.asistencia
{
    public partial class asiAsistencia
    {
        public asiAsistencia()
        {
            asiControl = new HashSet<asiControl>();
        }

        public int iAsistencia { get; set; }
        public int iContrato { get; set; }
        public DateTime dFecha { get; set; }

        public virtual ICollection<asiControl> asiControl { get; set; }
    }
}
