﻿using System;
using System.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace api4Nominus.Models.asistencia
{
    public partial class asistDb : DbContext
    {

        private string xConn;
        private static string cs;

        public asistDb(string xConn = "nominaEntities")
        {
            this.xConn = xConn;
            Database.SetCommandTimeout(25000);
        }

        public asistDb(DbContextOptions<asistDb> options)
            : base(options)
        {
        }

        public virtual DbSet<asiAsistencia> asiAsistencia { get; set; }
        public virtual DbSet<asiControl> asiControl { get; set; }
        public virtual DbSet<asiFlujoPapeleta> asiFlujoPapeleta { get; set; }
        public virtual DbSet<asiPapeleta> asiPapeleta { get; set; }
        public virtual DbSet<asiRolNotificador> asiRolNotificador { get; set; }
        public virtual DbSet<asiTipoFlujoPapeleta> asiTipoFlujoPapeleta { get; set; }
        public virtual DbSet<asiTipoPapeleta> asiTipoPapeleta { get; set; }
        public virtual DbSet<asiTipoTransaccion> asiTipoTransaccion { get; set; }
        public virtual DbSet<asiTransaccion> asiTransaccion { get; set; }
        public virtual DbSet<horControl> horControl { get; set; }
        public virtual DbSet<horHorario> horHorario { get; set; }
        public virtual DbSet<horIncidencia> horIncidencia { get; set; }
        public virtual DbSet<horInfoAnexa> horInfoAnexa { get; set; }
        public virtual DbSet<horJornada> horJornada { get; set; }
        public virtual DbSet<horMargen> horMargen { get; set; }
        public virtual DbSet<horMargenIncidencia> horMargenIncidencia { get; set; }
        public virtual DbSet<horProgramacion> horProgramacion { get; set; }
        public virtual DbSet<horRequerimientoMarca> horRequerimientoMarca { get; set; }
        public virtual DbSet<horTipoRotacion> horTipoRotacion { get; set; }
        public virtual DbSet<horTolerancia> horTolerancia { get; set; }
        public virtual DbSet<marDispositivo> marDispositivo { get; set; }
        public virtual DbSet<marMarcacion> marMarcacion { get; set; }
        public virtual DbSet<marModo> marModo { get; set; }
        public virtual DbSet<marPlataforma> marPlataforma { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                cs = ConfigurationManager.ConnectionStrings[this.xConn].ConnectionString;
                optionsBuilder.UseSqlServer(cs);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.4-servicing-10062")
                .HasAnnotation("Relational:DefaultSchema", "jorgeLoa_rem");

            modelBuilder.Entity<asiAsistencia>(entity =>
            {
                entity.HasKey(e => e.iAsistencia);

                entity.ToTable("asiAsistencia", "dbo");

                entity.Property(e => e.iAsistencia).ValueGeneratedNever();

                entity.Property(e => e.dFecha).HasColumnType("date");
            });

            modelBuilder.Entity<asiControl>(entity =>
            {
                entity.HasKey(e => e.iControl);

                entity.ToTable("asiControl", "dbo");

                entity.Property(e => e.iControl).ValueGeneratedNever();

                entity.Property(e => e.dHoraControl).HasColumnType("datetime2(0)");

                entity.Property(e => e.dHoraHorario).HasColumnType("datetime2(0)");

                entity.HasOne(d => d.iAsistenciaNavigation)
                    .WithMany(p => p.asiControl)
                    .HasForeignKey(d => d.iAsistencia)
                    .HasConstraintName("FK_asiControl_asiAsistencia");

                entity.HasOne(d => d.sIncidenciaNavigation)
                    .WithMany(p => p.asiControl)
                    .HasForeignKey(d => d.sIncidencia)
                    .HasConstraintName("FK_asiControl_horIncidencia");

                entity.HasOne(d => d.asiPapeleta)
                    .WithMany(p => p.asiControl)
                    .HasForeignKey(d => new { d.bTipoPapeleta, d.iPapeleta })
                    .HasConstraintName("FK_asiControl_asiPapeleta");
            });

            modelBuilder.Entity<asiFlujoPapeleta>(entity =>
            {
                entity.HasKey(e => e.iFlujoPapeleta);

                entity.ToTable("asiFlujoPapeleta", "dbo");

                entity.Property(e => e.iFlujoPapeleta)
                    .HasMaxLength(10)
                    .ValueGeneratedNever();

                entity.Property(e => e.cRol)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.cTipoFlujo)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.dFlujo).HasColumnType("datetime2(0)");

                entity.Property(e => e.xNotificacion).IsUnicode(false);

                entity.HasOne(d => d.cRolNavigation)
                    .WithMany(p => p.asiFlujoPapeleta)
                    .HasForeignKey(d => d.cRol)
                    .HasConstraintName("FK_asiFlujoPapeleta_asiRolNotificador");

                entity.HasOne(d => d.cTipoFlujoNavigation)
                    .WithMany(p => p.asiFlujoPapeleta)
                    .HasForeignKey(d => d.cTipoFlujo)
                    .HasConstraintName("FK_asiFlujoPapeleta_asiTipoFlujoPapeleta");

                entity.HasOne(d => d.asiPapeleta)
                    .WithMany(p => p.asiFlujoPapeleta)
                    .HasForeignKey(d => new { d.bTipoPapeleta, d.iPapeleta })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_asiFlujoPapeleta_asiPapeleta");
            });

            modelBuilder.Entity<asiPapeleta>(entity =>
            {
                entity.HasKey(e => new { e.bTipoPapeleta, e.iPapeleta });

                entity.ToTable("asiPapeleta", "dbo");

                entity.Property(e => e.dAlta).HasColumnType("date");

                entity.HasOne(d => d.bTipoPapeletaNavigation)
                    .WithMany(p => p.asiPapeleta)
                    .HasForeignKey(d => d.bTipoPapeleta)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_asiPapeleta_asiTipoPapeleta");
            });

            modelBuilder.Entity<asiRolNotificador>(entity =>
            {
                entity.HasKey(e => e.cRol);

                entity.ToTable("asiRolNotificador", "dbo");

                entity.Property(e => e.cRol)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.xRol)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<asiTipoFlujoPapeleta>(entity =>
            {
                entity.HasKey(e => e.cTipoFlujo);

                entity.ToTable("asiTipoFlujoPapeleta", "dbo");

                entity.Property(e => e.cTipoFlujo)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.xTipoFlujo)
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<asiTipoPapeleta>(entity =>
            {
                entity.HasKey(e => e.bTipoPapeleta);

                entity.ToTable("asiTipoPapeleta", "dbo");

                entity.Property(e => e.xTipoPapeleta)
                    .HasMaxLength(80)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<asiTipoTransaccion>(entity =>
            {
                entity.HasKey(e => e.sTipoTransaccion);

                entity.ToTable("asiTipoTransaccion", "dbo");

                entity.Property(e => e.sTipoTransaccion).ValueGeneratedNever();

                entity.Property(e => e.xTipoTransaccion)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<asiTransaccion>(entity =>
            {
                entity.HasKey(e => e.iTransaccion);

                entity.ToTable("asiTransaccion", "dbo");

                entity.Property(e => e.iTransaccion)
                    .HasMaxLength(10)
                    .ValueGeneratedNever();

                entity.Property(e => e.jsonTransaccion).IsUnicode(false);

                entity.HasOne(d => d.sTipoTransaccionNavigation)
                    .WithMany(p => p.asiTransaccion)
                    .HasForeignKey(d => d.sTipoTransaccion)
                    .HasConstraintName("FK_asiTransaccion_asiTipoTransaccion");

                entity.HasOne(d => d.asiPapeleta)
                    .WithMany(p => p.asiTransaccion)
                    .HasForeignKey(d => new { d.bTipoPapeleta, d.iPapeleta })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_asiTransaccion_asiPapeleta");
            });

            modelBuilder.Entity<horControl>(entity =>
            {
                entity.HasKey(e => e.sControl)
                    .HasName("PK_asi_Control_1");

                entity.ToTable("horControl", "dbo");

                entity.Property(e => e.sControl).ValueGeneratedNever();

                entity.Property(e => e.cTipoControl)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.dHora).HasColumnType("time(0)");

                entity.Property(e => e.sDiasPrevio).HasDefaultValueSql("((0))");

                entity.HasOne(d => d.sJornadaNavigation)
                    .WithMany(p => p.horControl)
                    .HasForeignKey(d => d.sJornada)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_asi_Control_asi_Jornada");
            });

            modelBuilder.Entity<horHorario>(entity =>
            {
                entity.HasKey(e => e.sHorario)
                    .HasName("PK_asi_Horario");

                entity.ToTable("horHorario", "dbo");

                entity.Property(e => e.xHorario)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<horIncidencia>(entity =>
            {
                entity.HasKey(e => e.sIncidencia)
                    .HasName("PK_asi_Incidencia");

                entity.ToTable("horIncidencia", "dbo");

                entity.Property(e => e.sIncidencia).ValueGeneratedNever();

                entity.Property(e => e.xIncidencia)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.HasOne(d => d.bToleranciaNavigation)
                    .WithMany(p => p.horIncidencia)
                    .HasForeignKey(d => d.bTolerancia)
                    .HasConstraintName("FK_asi_Incidencia_asi_Tolerancia");
            });

            modelBuilder.Entity<horInfoAnexa>(entity =>
            {
                entity.HasKey(e => e.sInfoAnexa);

                entity.ToTable("horInfoAnexa", "dbo");

                entity.Property(e => e.sInfoAnexa).ValueGeneratedNever();

                entity.Property(e => e.xDescriptor)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.HasOne(d => d.sJornadaNavigation)
                    .WithMany(p => p.horInfoAnexa)
                    .HasForeignKey(d => d.sJornada)
                    .HasConstraintName("FK_horInfoAnexa_horJornada");
            });

            modelBuilder.Entity<horJornada>(entity =>
            {
                entity.HasKey(e => e.sJornada)
                    .HasName("PK_asi_Jornada");

                entity.ToTable("horJornada", "dbo");

                entity.Property(e => e.cDias)
                    .HasMaxLength(7)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('LMWJVSD')");

                entity.Property(e => e.xJornada)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.bRequerimientoMarcaNavigation)
                    .WithMany(p => p.horJornada)
                    .HasForeignKey(d => d.bRequerimientoMarca)
                    .HasConstraintName("FK_horJornada_horRequerimientoMarca");

                entity.HasOne(d => d.sHorarioNavigation)
                    .WithMany(p => p.horJornada)
                    .HasForeignKey(d => d.sHorario)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_asi_Jornada_asi_Horario");
            });

            modelBuilder.Entity<horMargen>(entity =>
            {
                entity.HasKey(e => e.sMargen);

                entity.ToTable("horMargen", "dbo");

                entity.Property(e => e.sMargen).ValueGeneratedNever();

                entity.Property(e => e.cTipoControl)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.xMargen)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<horMargenIncidencia>(entity =>
            {
                entity.HasKey(e => e.iMargen);

                entity.ToTable("horMargenIncidencia", "dbo");

                entity.Property(e => e.iMargen)
                    .HasMaxLength(10)
                    .ValueGeneratedNever();

                entity.Property(e => e.sDiferencia)
                    .IsRequired()
                    .HasMaxLength(10);

                entity.Property(e => e.sIncidencia).HasDefaultValueSql("((0))");

                entity.HasOne(d => d.sIncidenciaNavigation)
                    .WithMany(p => p.horMargenIncidencia)
                    .HasForeignKey(d => d.sIncidencia)
                    .HasConstraintName("FK_asi_MargenIncidencia_asi_Incidencia");

                entity.HasOne(d => d.sMargenNavigation)
                    .WithMany(p => p.horMargenIncidencia)
                    .HasForeignKey(d => d.sMargen)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_horMargenIncidencia_horMargen");
            });

            modelBuilder.Entity<horProgramacion>(entity =>
            {
                entity.HasKey(e => e.iProgramacion);

                entity.ToTable("horProgramacion", "dbo");

                entity.Property(e => e.iProgramacion).ValueGeneratedNever();

                entity.Property(e => e.dDesde).HasColumnType("date");

                entity.Property(e => e.dHasta).HasColumnType("date");

                entity.HasOne(d => d.sHorarioNavigation)
                    .WithMany(p => p.horProgramacion)
                    .HasForeignKey(d => d.sHorario)
                    .HasConstraintName("FK_horProgramacion_horHorario");

                entity.HasOne(d => d.sRotacionNavigation)
                    .WithMany(p => p.horProgramacion)
                    .HasForeignKey(d => d.sRotacion)
                    .HasConstraintName("FK_horProgramacion_horTipoRotacion");
            });

            modelBuilder.Entity<horRequerimientoMarca>(entity =>
            {
                entity.HasKey(e => e.bRequerimientoMarca);

                entity.ToTable("horRequerimientoMarca", "dbo");

                entity.Property(e => e.xRequerimientoMarca)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<horTipoRotacion>(entity =>
            {
                entity.HasKey(e => e.sRotacion);

                entity.ToTable("horTipoRotacion", "dbo");

                entity.Property(e => e.sRotacion).ValueGeneratedNever();

                entity.Property(e => e.cFactor)
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<horTolerancia>(entity =>
            {
                entity.HasKey(e => e.bTolerancia)
                    .HasName("PK_asi_Tolerancia");

                entity.ToTable("horTolerancia", "dbo");

                entity.Property(e => e.lJefeAutoriza).HasDefaultValueSql("((0))");

                entity.Property(e => e.lMarcaControl).HasDefaultValueSql("((1))");

                entity.Property(e => e.lNotificarJefe).HasDefaultValueSql("((1))");

                entity.Property(e => e.xTolerancia)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<marDispositivo>(entity =>
            {
                entity.HasKey(e => e.sDispositivo);

                entity.ToTable("marDispositivo", "dbo");

                entity.Property(e => e.sDispositivo).ValueGeneratedNever();

                entity.Property(e => e.xDescripcion)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.bPlataformaNavigation)
                    .WithMany(p => p.marDispositivo)
                    .HasForeignKey(d => d.bPlataforma)
                    .HasConstraintName("FK_marDispositivo_marPlataforma");
            });

            modelBuilder.Entity<marMarcacion>(entity =>
            {
                entity.HasKey(e => e.iMarca);

                entity.ToTable("marMarcacion", "dbo");

                entity.Property(e => e.iMarca).ValueGeneratedNever();

                entity.Property(e => e.dHora).HasColumnType("datetime");

                entity.HasOne(d => d.iControlNavigation)
                    .WithMany(p => p.marMarcacion)
                    .HasForeignKey(d => d.iControl)
                    .HasConstraintName("FK_marMarcacion_asiControl");

                entity.HasOne(d => d.sDispositivoNavigation)
                    .WithMany(p => p.marMarcacion)
                    .HasForeignKey(d => d.sDispositivo)
                    .HasConstraintName("FK_marMarcacion_marDispositivo");
            });

            modelBuilder.Entity<marModo>(entity =>
            {
                entity.HasKey(e => e.bModo);

                entity.ToTable("marModo", "dbo");

                entity.Property(e => e.xModo)
                    .HasMaxLength(40)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<marPlataforma>(entity =>
            {
                entity.HasKey(e => e.bPlataforma);

                entity.ToTable("marPlataforma", "dbo");

                entity.Property(e => e.xPlataforma)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.HasOne(d => d.bModoNavigation)
                    .WithMany(p => p.marPlataforma)
                    .HasForeignKey(d => d.bModo)
                    .HasConstraintName("FK_marPlataforma_marModo");
            });
        }

    }
}
