﻿using System;
using System.Collections.Generic;

namespace api4Nominus.Models.asistencia
{
    public partial class horIncidencia
    {
        public horIncidencia()
        {
            asiControl = new HashSet<asiControl>();
            horMargenIncidencia = new HashSet<horMargenIncidencia>();
        }

        public short sIncidencia { get; set; }
        public string xIncidencia { get; set; }
        public byte? bTolerancia { get; set; }

        public virtual horTolerancia bToleranciaNavigation { get; set; }
        public virtual ICollection<asiControl> asiControl { get; set; }
        public virtual ICollection<horMargenIncidencia> horMargenIncidencia { get; set; }
    }
}
