﻿using System;
using System.Collections.Generic;

namespace api4Nominus.Models.asistencia
{
    public partial class asiTipoPapeleta
    {
        public asiTipoPapeleta()
        {
            asiPapeleta = new HashSet<asiPapeleta>();
        }

        public byte bTipoPapeleta { get; set; }
        public string xTipoPapeleta { get; set; }

        public virtual ICollection<asiPapeleta> asiPapeleta { get; set; }
    }
}
