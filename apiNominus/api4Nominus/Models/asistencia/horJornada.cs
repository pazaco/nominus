﻿using System;
using System.Collections.Generic;

namespace api4Nominus.Models.asistencia
{
    public partial class horJornada
    {
        public horJornada()
        {
            horControl = new HashSet<horControl>();
            horInfoAnexa = new HashSet<horInfoAnexa>();
        }

        public short sJornada { get; set; }
        public string xJornada { get; set; }
        public short sHorario { get; set; }
        public string cDias { get; set; }
        public byte? bRequerimientoMarca { get; set; }

        public virtual horRequerimientoMarca bRequerimientoMarcaNavigation { get; set; }
        public virtual horHorario sHorarioNavigation { get; set; }
        public virtual ICollection<horControl> horControl { get; set; }
        public virtual ICollection<horInfoAnexa> horInfoAnexa { get; set; }
    }
}
