﻿using System;
using System.Collections.Generic;

namespace api4Nominus.Models.asistencia
{
    public partial class marModo
    {
        public marModo()
        {
            marPlataforma = new HashSet<marPlataforma>();
        }

        public byte bModo { get; set; }
        public string xModo { get; set; }

        public virtual ICollection<marPlataforma> marPlataforma { get; set; }
    }
}
