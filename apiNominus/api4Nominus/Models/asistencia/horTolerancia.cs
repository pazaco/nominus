﻿using System;
using System.Collections.Generic;

namespace api4Nominus.Models.asistencia
{
    public partial class horTolerancia
    {
        public horTolerancia()
        {
            horIncidencia = new HashSet<horIncidencia>();
        }

        public byte bTolerancia { get; set; }
        public string xTolerancia { get; set; }
        public bool? lMarcaControl { get; set; }
        public bool? lJefeAutoriza { get; set; }
        public bool? lNotificarJefe { get; set; }
        public bool? lAbrirPuerta { get; set; }

        public virtual ICollection<horIncidencia> horIncidencia { get; set; }
    }
}
