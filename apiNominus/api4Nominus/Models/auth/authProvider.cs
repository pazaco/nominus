﻿using System;
using System.Collections.Generic;

namespace api4Nominus.Models.auth
{
    public partial class authProvider
    {
        public byte bProvider { get; set; }
        public string providerId { get; set; }

    }
}
