﻿using System;
using System.Collections.Generic;

namespace api4Nominus.Models.auth
{
    public partial class authVoz
    {
        public int iVoz { get; set; }
        public DateTime? dVoz { get; set; }
        public Guid? gUsuario { get; set; }
        public string hVoz { get; set; }
    }
}
