﻿using System;
using System.Collections.Generic;

namespace api4Nominus.Models.auth
{
    public partial class authClaseNavegador
    {
        public byte bClaseNavegador { get; set; }
        public string xClaseNavegador { get; set; }
        public bool? lMobile { get; set; }

    }
}
