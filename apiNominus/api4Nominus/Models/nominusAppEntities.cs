﻿using System;
using Microsoft.EntityFrameworkCore;
using api4Nominus.Models.auth;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Configuration;
using Microsoft.Data.SqlClient;


namespace api4Nominus.Models
{
    public partial class nominusAppEntities : DbContext
    {
        public nominusAppEntities()
        {
            Database.SetCommandTimeout(25000);
        }

        public nominusAppEntities(DbContextOptions<nominusAppEntities> options)
            : base(options)
        {
        }

        public virtual DbSet<authAcceso> authAcceso { get; set; }
        public virtual DbSet<authClaseNavegador> authClaseNavegador { get; set; }
        public virtual DbSet<authNavegador> authNavegador { get; set; }
        public virtual DbSet<authProvider> authProvider { get; set; }
        public virtual DbSet<authServidor> authServidor { get; set; }
        public virtual DbSet<authSesion> authSesion { get; set; }
        public virtual DbSet<authUsuario> authUsuario { get; set; }
        public virtual DbSet<authVoz> authVoz { get; set; }
        public virtual DbSet<DbInt> dbInt { get; set; }
        public virtual DbSet<DbStr> dbStr { get; set; }
        public virtual DbSet<DbTinyint> dbTinyint { get; set; }
        public virtual DbSet<DbSmallint> dbSmallInt { get; set; }
        public virtual DbSet<AuthServidorUsuario> _authServidoresUsuario { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                string cs = ConfigurationManager.ConnectionStrings["nominusAppEntities"].ConnectionString;
                optionsBuilder.UseSqlServer(cs);
            }
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.4-servicing-10062")
                .HasAnnotation("Relational:DefaultSchema", "radical_uuladech");

            modelBuilder.Entity<authAcceso>(entity =>
            {
                entity.HasKey(e => e.gAcceso)
                    .HasName("PK_Accesos");
                entity.ToTable("authAcceso", "dbo");
                entity.Property(e => e.gAcceso).ValueGeneratedNever();
            });

            modelBuilder.Entity<authClaseNavegador>(entity =>
            {
                entity.HasKey(e => e.bClaseNavegador)
                    .HasName("PK_autClaseNavegador");
                entity.ToTable("authClaseNavegador", "dbo");
                entity.Property(e => e.xClaseNavegador)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<authNavegador>(entity =>
            {
                entity.HasKey(e => e.sNavegador)
                    .HasName("PK_autNavegador");
                entity.ToTable("authNavegador", "dbo");
                entity.Property(e => e.sNavegador).ValueGeneratedNever();
                entity.Property(e => e.iLatencia).HasDefaultValueSql("((30))");
                entity.Property(e => e.xNavegador)
                    .IsRequired()
                    .IsUnicode(false);
            });

            modelBuilder.Entity<authProvider>(entity =>
            {
                entity.HasKey(e => e.bProvider);
                entity.ToTable("authProvider", "dbo");
                entity.Property(e => e.providerId)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<authServidor>(entity =>
            {
                entity.HasKey(e => e.sServidor)
                    .HasName("PK_Servidores");

                entity.ToTable("authServidor", "dbo");

                entity.Property(e => e.sServidor).ValueGeneratedNever();

                entity.Property(e => e.xConexion).IsUnicode(false);

                entity.Property(e => e.xDescripcion)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.xModeloAPI)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<authSesion>(entity =>
            {
                entity.HasKey(e => e.gToken);

                entity.ToTable("authSesion", "dbo");

                entity.Property(e => e.gToken).HasDefaultValueSql("(newid())");

                entity.Property(e => e.dFin).HasColumnType("datetime2(0)");

                entity.Property(e => e.dInicio).HasColumnType("datetime2(0)");

                entity.Property(e => e.xTokenMsg)
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<authUsuario>(entity =>
            {
                entity.HasKey(e => e.gUsuario)
                    .HasName("PK_Usuarios");

                entity.ToTable("authUsuario", "dbo");

                entity.HasIndex(e => e.email)
                    .HasName("IX_xEmail")
                    .IsUnique();

                entity.Property(e => e.gUsuario).ValueGeneratedNever();

                entity.Property(e => e.displayName)
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.email)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.phoneNumber)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.photo).IsUnicode(false);

            });

            modelBuilder.Entity<AuthServidorUsuario>(entity =>
            {
                entity.HasKey(uk => new { uk.iPersona, uk.sServidor });
            });


            modelBuilder.Entity<authVoz>(entity =>
            {
                entity.HasKey(e => e.iVoz);

                entity.ToTable("authVoz", "dbo");

                entity.Property(e => e.dVoz).HasColumnType("datetime2(0)");

                entity.Property(e => e.hVoz).IsUnicode(false);
            });

            modelBuilder.Entity<DbInt>(entity => {
                entity.HasKey(k => k.iResultado);
            });
            modelBuilder.Entity<DbStr>(entity => {
                entity.HasKey(k => k.xResultado);
            });
            modelBuilder.Entity<DbSmallint>(entity => {
                entity.HasKey(k => k.sResultado);
            });
            modelBuilder.Entity<DbTinyint>(entity => {
                entity.HasKey(k => k.bResultado);
            });
        }
        public async Task<IQueryable<AuthServidorUsuario>> authServidoresUsuario(Guid gToken)
        {
            IQueryable<AuthServidorUsuario> resp;
            try
            {
                SqlParameter par_gToken = new SqlParameter("gToken", gToken);
                resp = _authServidoresUsuario.FromSqlRaw("exec authServidoresUsuario @gToken", par_gToken);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            await Task.Delay(1);
            return resp;
        }

        public async Task<List<authServidor>> authServidoresPersona(Guid gUsuario)
        {

            List<authServidor> resp = new List<authServidor>();
            var usuario = new SqlParameter("gUsuario", gUsuario);
            var a = usuario;
            resp = authServidor.FromSqlRaw("exec authServidoresPersona @gUsuario", usuario).ToList();
            await Task.Delay(1);
            return resp;
        }

        public async void authCrearEnlace(short sServidor, int iUsuario, string xCorreo)
        {
            SqlParameter par_sServidor = new SqlParameter("sServidor", sServidor);
            SqlParameter par_iUsuario = new SqlParameter("iUsuario", iUsuario);
            SqlParameter par_xCorreo = new SqlParameter("xCorreo", xCorreo);
            Database.ExecuteSqlRaw("exec authCrearEnlace @sServidor,@iUsuario,@xCorreo", par_sServidor, par_iUsuario, par_xCorreo);
            await Task.Delay(1);
        }
    }

    public class nominusAppSP
    {

        private static nominusAppEntities dbA = new nominusAppEntities();

        public static IQueryable<AuthServidorUsuario> authServidoresUsuarioStat(Guid gToken)
        {
            IQueryable<AuthServidorUsuario> resp;
            try
            {
                SqlParameter par_gToken = new SqlParameter("gToken", gToken);
                resp = dbA._authServidoresUsuario.FromSqlRaw("exec authServidoresUsuario @gToken", par_gToken);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resp;
        }

        public async Task<IQueryable<AuthServidorUsuario>> authServidoresUsuario(Guid gToken)
        {
            IQueryable<AuthServidorUsuario> resp;
            try
            {
                SqlParameter par_gToken = new SqlParameter("gToken", gToken);
                resp = dbA._authServidoresUsuario.FromSqlRaw("exec authServidoresUsuario @gToken", par_gToken);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            await Task.Delay(1);
            return resp;
        }

        public static  List<authServidor> authServidoresPersonaStat(Guid gUsuario)
        {
            List<authServidor> resp = new List<authServidor>();
            var par_usuario = new SqlParameter("gUsuario", gUsuario);
            resp = dbA.authServidor.FromSqlRaw("exec authServidoresPersona @gUsuario", par_usuario).ToList();
            return resp;
        }

        public static void authCrearEnlaceStat(short sServidor, int iUsuario, string xCorreo)
        {
            SqlParameter par_sServidor = new SqlParameter("sServidor", sServidor);
            SqlParameter par_iUsuario = new SqlParameter("iUsuario", iUsuario);
            SqlParameter par_xCorreo = new SqlParameter("xCorreo", xCorreo);
            dbA.Database.ExecuteSqlRaw("exec authCrearEnlace @sServidor,@iUsuario,@xCorreo",par_sServidor,par_iUsuario,par_xCorreo);
        }
            
    }

    public class AuthServidorUsuario
    {
        public int iPersona { get; set; }
        public short sServidor { get; set; }
        public string xDescripcion { get; set; }
        public string xConexion { get; set; }
        public string xModeloAPI { get; set; }
        public string xAsistenciaZK { get; set; }
    }

    public class DbInt
    {
        public int iResultado { get; set; }
    }

    public class DbStr
    {
        public string xResultado { get; set; }

    }

    public class DbDateTime
    {
        public DateTime dResultado { get; set; } 
    }

    public class DbTinyint
    {
        public byte bResultado { get; set; }
    }
    public class DbSmallint
    {
        public short sResultado { get; set; }
    }


}
