﻿using System;
using System.Collections.Generic;

namespace api4Nominus.Models.nominus
{
    public partial class asi_Asistencia
    {
        public int iAsistencia { get; set; }
        public int iContrato { get; set; }
        public DateTime dFecha { get; set; }
        public double? fDescuento { get; set; }
        public double? fExtra { get; set; }
        public int? iCalculoPlanilla { get; set; }
        public short? sEstablecimiento { get; set; }
        public string cPlanilla { get; set; }
    }
}
