﻿using System;
using System.Collections.Generic;

namespace api4Nominus.Models.nominus
{
    public partial class perPersona
    {
 
        public int iPersona { get; set; }
        public byte bTipoDocIdentidad { get; set; }
        public string xDocIdentidad { get; set; }
        public string xApellidoPaterno { get; set; }
        public string xApellidoMaterno { get; set; }
        public string xNombres { get; set; }
        public DateTime dNacimiento { get; set; }
        public bool bGenero { get; set; }
        public string xConcatenado { get; set; }

    }
}
