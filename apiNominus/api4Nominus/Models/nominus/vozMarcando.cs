﻿using System;
using System.Collections.Generic;

namespace api4Nominus.Models.nominus
{
    public partial class vozMarcando
    {
        public int iMarcando { get; set; }
        public int iPersona { get; set; }
        public byte bFrase { get; set; }
        public string xFingerPrint { get; set; }

    }
}
