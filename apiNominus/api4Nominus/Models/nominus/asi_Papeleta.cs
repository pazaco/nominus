﻿using System;
using System.Collections.Generic;

namespace api4Nominus.Models.nominus
{
    public partial class asi_Papeleta
    {
        public byte bTipoPapeleta { get; set; }
        public int iPapeleta { get; set; }
        public short? sIncidencia { get; set; }
        public DateTime dCreacion { get; set; }
        public int iEmpleado { get; set; }
        public int? iUsuario { get; set; }
        public int? iJefe { get; set; }
        public DateTime? dJefe { get; set; }
        public DateTime? dRRHH { get; set; }
        public string cNumeroFisico { get; set; }
        public string cNotificar { get; set; }
    }
}
