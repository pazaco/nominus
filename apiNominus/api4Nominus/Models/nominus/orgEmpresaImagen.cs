﻿using System;
using System.Collections.Generic;

namespace api4Nominus.Models.nominus
{
    public partial class orgEmpresaImagen
    {
        public short sEmpresaImagen { get; set; }
        public short sEmpresa { get; set; }
        public byte bTipoImagen { get; set; }
        public string jImagen { get; set; }
        public byte[] iImagen { get; set; }
    }
}
