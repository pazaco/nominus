﻿using System;
using System.Collections.Generic;

namespace api4Nominus.Models.nominus
{
    public partial class asi_FlujoDocente
    {
        public int iFlujoDocente { get; set; }
        public int? iAsistencia { get; set; }
        public short? bControl { get; set; }
        public DateTime? dHoraHorario { get; set; }
        public string cAula { get; set; }
        public string cActividad { get; set; }
    }
}
