﻿using System;
using System.Collections.Generic;

namespace api4Nominus.Models.nominus
{
    public partial class traJefeDirecto
    {
        public int iJefeDirecto { get; set; }
        public int iContrato { get; set; }
        public int iTrabajador { get; set; }
    }
}
