﻿using System;
using System.Collections.Generic;

namespace api4Nominus.Models.nominus
{
    public partial class remCalculoPlanilla
    {
        public int iCalculoPlanilla { get; set; }
        public int iPeriodo { get; set; }
        public int iContrato { get; set; }
    }
}
