﻿using System;
using System.Collections.Generic;

namespace api4Nominus.Models.nominus
{
    public partial class asi_TipoFlujoPapeleta
    {

        public string cTipoFlujo { get; set; }
        public string xTipoFlujo { get; set; }
    }
}
