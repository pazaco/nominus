﻿using System;
using System.Collections.Generic;

namespace api4Nominus.Models.nominus
{
    public partial class asi_HorarioDiario
    {
        public int iHorarioDiario { get; set; }
        public string cEmpleado { get; set; }
        public short? bControl { get; set; }
        public string cHora { get; set; }
        public string cAula { get; set; }
        public string cActividad { get; set; }
    }
}
