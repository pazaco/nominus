﻿using System;
using System.Collections.Generic;

namespace api4Nominus.Models.nominus
{
    public partial class autUsuarioClaim
    {

        public int iUsuario { get; set; }
        public string cClaim { get; set; }
        public string xValor { get; set; }

    }
}
