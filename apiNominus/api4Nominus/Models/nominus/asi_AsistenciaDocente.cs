﻿using System;
using System.Collections.Generic;

namespace api4Nominus.Models.nominus
{
    public partial class asi_AsistenciaDocente
    {
        public int iAsistencia { get; set; }
        public string cAula { get; set; }
        public string cActividad { get; set; }
    }
}
