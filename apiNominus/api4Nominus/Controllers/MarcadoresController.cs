﻿using api4Nominus.Models;
using api4Nominus.Models.asistenciaPK;
using api4Nominus.Models.auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace api4Nominus.Controllers
{
    [RoutePrefix("zkMaestros")]
    public class MarcadoresController : ApiController
    {

        [Route("departamentos/{sServidor}")]
        public List<DEPARTMENTS> GetZkDepartamentos(int sServidor)
        {
            int aaa = sServidor;
            List<DEPARTMENTS> rsp = null;
            using (nominusAppEntities dbA = new nominusAppEntities())
            {
                authServidor _serv = dbA.authServidor.Where(c => c.sServidor == sServidor).FirstOrDefault();
                if (_serv != null)
                {
                    using (ZK_Entities dbZ = new ZK_Entities(_serv.xAsistenciaZK))
                    {
                        rsp = dbZ.DEPARTMENTS.ToList();
                    }
                }
                return rsp;
            }
        }
    }

    [RoutePrefix("rptMarcacion")]
    public class RptMarcacionController: ApiController
    {
        [Route("listaMarcaciones")][HttpPost]
        public List<acc_monitor_log> ListaMarcaciones(ParPeriodoPersona ppp)
        {
            List<acc_monitor_log> rsp = null;
            string SesionPersona = HttpContext.Current.Request.Headers["sesionPersona"];
            Guid? gSesionPersona = null;
            if (SesionPersona != null && SesionPersona != "")
            {
                gSesionPersona = new Guid(SesionPersona);
            }
            using (nominusAppEntities dbA = new nominusAppEntities())
            {
                //TODO preguntar si el gSesionPersona puede consultar datos de la ppp.iPersona
                authServidor _srv = dbA.authServidor.Where(c => c.sServidor == ppp.sServidor).FirstOrDefault();
                if (_srv != null)
                {
                    using (nominaEntities dbN = new nominaEntities(_srv.xConexion))
                    {
                        using (ZK_Entities dbZ = new ZK_Entities(_srv.xAsistenciaZK))
                        {
                            // aqui va todo lo que quieras hacer contra la instancia de nominus que ya tu creaste, teniendo acceso a zk
                            rsp = dbZ.acc_monitor_log.Where(c => c.pin == ppp.iPersona.ToString() && (c.time >= ppp.rango.dDesde || ppp.rango.dDesde == null) && (c.time <= ppp.rango.dHasta || ppp.rango.dHasta == null)).ToList();
                        }
                    }
                }
            }

            return rsp;
        }
        [Route("misMarcaciones")]
        [HttpPost]
        public List<acc_monitor_log> MisMarcaciones(ParPeriodoPersona ppp)
        {
            List<acc_monitor_log> rsp = null;
            string SesionPersona = HttpContext.Current.Request.Headers["sesionPersona"];
            Guid? gSesionPersona = null;
            if (SesionPersona != null && SesionPersona != "")
            {
                gSesionPersona = new Guid(SesionPersona);
            }
            using (nominusAppEntities dbA = new nominusAppEntities())
            {
                authSesion _ses = dbA.authSesion.Where(c => c.gToken == gSesionPersona).FirstOrDefault();
                authAcceso _acc = dbA.authAcceso.Where(c => c.gUsuario == _ses.gUsuario).FirstOrDefault();
                authServidor _srv = dbA.authServidor.Where(c => c.sServidor == _acc.sServidor).FirstOrDefault();
                if (_ses != null && _srv != null)
                {
                    using (nominaEntities dbN = new nominaEntities(_srv.xConexion))
                    {
                        using (ZK_Entities dbZ = new ZK_Entities(_srv.xAsistenciaZK))
                        {
                            // aqui va todo lo que quieras hacer contra la instancia de nominus que ya tu creaste, teniendo acceso a zk
                            rsp = dbZ.acc_monitor_log.Where(c => c.pin == _acc.iPersona.ToString() && (c.time >= ppp.rango.dDesde || ppp.rango.dDesde == null) && (c.time <= ppp.rango.dHasta || ppp.rango.dHasta == null)).ToList();
                        }
                    }
                }
            }


            return rsp;
        }

    }

    [RoutePrefix("clocks")]
    public class ZkClocks : ApiController
    {
        [Route("usuarios")]
        public void GetUsuarios() {

        }
    }


    public class ParPeriodoPersona
    {
        public short? sServidor { get; set; }
        public int? iPersona { get; set; }
        public RangoFechas rango { get; set; }
    }

}
