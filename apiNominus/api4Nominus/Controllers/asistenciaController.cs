﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using api4Nominus.Models;
using api4Nominus.Models.nominus;
using api4Nominus.Models.auth;
using System.Threading.Tasks;
using api4Nominus.Models.asistenciaPK;

namespace api4Nominus.Controllers
{
    [RoutePrefix("asi")]
    public class asistenciaController : ApiController
    {

        [Route("addPoligono"), HttpPost]
        public void addPoligono([FromBody] RspGeoUbicacion poli)
        {
            nominusAppEntities dbA = new nominusAppEntities();

            authServidor _srv = dbA.authServidor.Find((short)8);
            nominaEntities db = new nominaEntities(_srv.xConexion);
            short sPosicion = 0;
            foreach (AgmPunto pto in poli.puntos)
            {
                db.orgGeoEstablecimiento.Add(new orgGeoEstablecimiento
                {
                    cEstablecimiento = poli.cEstablecimiento,
                    sOrtogono = poli.sOrtogono,
                    sPosicion = sPosicion++,
                    fLat = pto.lat,
                    fLon = pto.lng
                });
            }
            db.SaveChanges();
        }

        [Route("delPoligono"), HttpPost]
        public void delPoligono([FromBody] ParPoligono poli)
        {
            nominusAppEntities dbA = new nominusAppEntities();
            authServidor _srv = dbA.authServidor.Find((short)8);
            nominaEntities db = new nominaEntities(_srv.xConexion);
            List<orgGeoEstablecimiento> borrar = db.orgGeoEstablecimiento
                .Where(c => c.cEstablecimiento == poli.cEstablecimiento && c.sOrtogono == poli.sOrtogono)
                .ToList();
            db.orgGeoEstablecimiento.RemoveRange(borrar);
            db.SaveChanges();
        }

           

        [Route("geoUbicacion"), HttpPost]
        public List<RspGeoUbicacion> GeoUBicacion([FromBody] ParCaminar caminar)
        {
            nominusAppEntities dbA = new nominusAppEntities();
            List<RspGeoUbicacion> rsp = null;
            if (caminar != null)
            {
                authServidor _srv = dbA.authServidor.Find((short)8);
                nominaEntities db = new nominaEntities(_srv.xConexion);
                rsp = db.orgGeoEstablecimiento
                    .Join(caminar.establecimientos, c1 => c1.cEstablecimiento, c2 => c2, (c1, c2) => new { c1, c2 })
                    .Select(s => new RspGeoUbicacion { cEstablecimiento = s.c1.cEstablecimiento, sOrtogono = s.c1.sOrtogono, lMatch = false })
                    .Distinct().ToList();
                foreach (RspGeoUbicacion poligono in rsp)
                {
                    List<GeoPunto> ortogono = db.orgGeoEstablecimiento
                        .Where(c => c.cEstablecimiento == poligono.cEstablecimiento && c.sOrtogono == poligono.sOrtogono)
                        .OrderBy(o => o.sPosicion)
                        .Select(s => new GeoPunto { fLat = s.fLat, fLon = s.fLon })
                        .ToList();
                    poligono.lMatch = WareHouse.RayCasting(caminar.punto, ortogono);
                    poligono.puntos = ortogono.Select(cc => new AgmPunto { lat = (double)cc.fLat, lng = (double)cc.fLon }).ToList();
                }
            }
            return rsp;
        }

        [Route("poligono"), HttpPost]
        public List<GeoPunto> Poligono([FromBody] ParPoligono poli)
        {
            nominusAppEntities dbA = new nominusAppEntities();
            authServidor _srv = dbA.authServidor.Find((short)8);
            nominaEntities db = new nominaEntities(_srv.xConexion);
            return db.orgGeoEstablecimiento
                .Where(c => c.cEstablecimiento == poli.cEstablecimiento && c.sOrtogono == poli.sOrtogono)
                .OrderBy(c => c.sPosicion)
                .Select(c => new GeoPunto { fLat = c.fLat, fLon = c.fLon })
                .ToList();
        }

        [Route("diarioEmpleado"), HttpPost]
        public async Task<RspDiarioEmpleado> asiDiarioEmpleado([FromBody] parDiaEmpleado diaempleado)
        {
            nominusAppEntities dbA = new nominusAppEntities();
            authServidor _srv = dbA.authServidor.Find((short)8);
            nominaEntities db = new nominaEntities(_srv.xConexion);
            RspDiarioEmpleado rsp = new RspDiarioEmpleado();
            diaempleado.dDia = diaempleado.dDia.Date;
            asi_Asistencia _asi = db.asi_Asistencia.FirstOrDefault(c => c.iContrato == diaempleado.iEmpleado && c.dFecha == diaempleado.dDia);
            if (_asi != null)
            {
                rsp.oAsistencia = new RspDiarioAsistencia();
                rsp.oAsistencia.iAsistencia = _asi.iAsistencia;
                rsp.oAsistencia.dFecha = _asi.dFecha;
                rsp.oAsistencia.iContrato = _asi.iContrato;
                rsp.oAsistencia.fDescuento = _asi.fDescuento;
                rsp.oAsistencia.fExtra = _asi.fExtra;
                rsp.oAsistencia.cPlanilla = _asi.cPlanilla;
                rsp.oAsistencia.iCalculoPlanilla = _asi.iCalculoPlanilla;
                rsp.oAsistencia.sEstablecimiento = _asi.sEstablecimiento;
                rsp.oAsistencia.aFlujo = (await db.asi_FlujoDiarioEmpleado(_asi.iAsistencia)).ToList();
                rsp.empleado = await WareHouse.EmpleadoUladech(diaempleado.iEmpleado);
            }
            List<asi_Dispositivo> _dsp = db.asi_Dispositivo.ToList();
            rsp.aMarcacion = (await db.asi_MarcacionDiarioEmpleado(diaempleado.iEmpleado, diaempleado.dDia)).ToList();
            return rsp;
        }

        [Route("mensualEmpleado"), HttpPost]
        public async Task<List<asi_CalificacionMensual>> calificacionMensual([FromBody] parDiaEmpleado mesEmpleado)
        {
            nominusAppEntities dbA = new nominusAppEntities();
            authServidor _srv = dbA.authServidor.Find((short)8);
            nominaEntities db = new nominaEntities(_srv.xConexion);
            return (await db.asi_CalificacionMensual(mesEmpleado.iEmpleado, mesEmpleado.dDia)).ToList();
        }

        [Route("planoMarcacion"), HttpPost]
        public List<asi_PlanoMarcacion> PlanoMarcacion([FromBody] ParPlanos parPlano)
        {
            nominusAppEntities dbA = new nominusAppEntities();
            authServidor _srv = dbA.authServidor.Find((short)8);
            nominaEntities db = new nominaEntities(_srv.xConexion);
            return db.asi_PlanoMarcacion(parPlano.dDesde, parPlano.dHasta, parPlano.empleados);
        }
        [Route("planoHorarioControl"), HttpPost]
        public List<asi_PlanoHorarioControl> PlanoHorarioControl([FromBody]  ParPlanos parPlano)
        {
            nominusAppEntities dbA = new nominusAppEntities();
            authServidor _srv = dbA.authServidor.Find((short)8);
            nominaEntities db = new nominaEntities(_srv.xConexion);
            return db.asi_PlanoHorarioControl(parPlano.dDesde, parPlano.dHasta, parPlano.empleados);
        }
        [Route("planoAsistencia"), HttpPost]
        public List<asi_PlanoAsistencia> PlanoAsistencia([FromBody] ParPlanos parPlano)
        {
            nominusAppEntities dbA = new nominusAppEntities();
            authServidor _srv = dbA.authServidor.Find((short)8);
            nominaEntities db = new nominaEntities(_srv.xConexion);
            return  db.asi_PlanoAsistencia(parPlano.dDesde, parPlano.dHasta, parPlano.empleados);
        }

        [Route("grafControlxEstablecimiento/{dDia}"), HttpGet]
        public async Task<List<asi_aggControlxEstablecimiento>> grafCtlEstablecimiento([FromUri] DateTime dDia)
        {
            nominusAppEntities dbA = new nominusAppEntities();
            authServidor _srv = dbA.authServidor.Find((short)8);
            nominaEntities db = new nominaEntities(_srv.xConexion);
            return (await db.asi_aggControlxEstablecimiento(dDia)).ToList();
        }

        [Route("grafMarcacion/{dDia}"), HttpGet]
        public async Task<RspGrafMarcacion> grafMarcacion(DateTime dDia)
        {
            nominusAppEntities dbA = new nominusAppEntities();
            uladechEntities dbU = new uladechEntities();
            RspGrafMarcacion rsp = new RspGrafMarcacion();
            authServidor _srv = dbA.authServidor.Find((short)8);
            nominaEntities db = new nominaEntities(_srv.xConexion);
            rsp.establecimientos = dbU.DEPARTMENTS.ToList();
            rsp.grafMarcados = (await db.asi_grafMarcadoAsync(dDia)).ToList();
            return rsp;
        }

        [Route("grafReclutamiento"), HttpGet]
        public async Task<asi_Reclutamiento> grafReclutamiento()
        {
            nominusAppEntities dbA = new nominusAppEntities();
            authServidor _srv = dbA.authServidor.Find((short)8);
            nominaEntities db = new nominaEntities(_srv.xConexion);
            return (await db.asi_aggReclutamientoGlobal()).FirstOrDefault();
        }

        [Route("revaluarDia/{dDia}"), HttpGet]
        public void revaluarDia([FromUri] DateTime dDia)
        {
            nominusAppEntities dbA = new nominusAppEntities();
            authServidor _srv = dbA.authServidor.Find((short)8);
            nominaEntities db = new nominaEntities(_srv.xConexion);
            db.asi_RevaluarDia(dDia);
        }

        [Route("revaluar/{iAsistencia}"), HttpGet]
        public async Task<string> RevaluarAsistencia([FromUri] int iAsistencia)
        {
            nominusAppEntities dbA = new nominusAppEntities();
            authServidor _srv = dbA.authServidor.Find((short)8);
            nominaEntities db = new nominaEntities(_srv.xConexion);
            return  await db.asi_Revaluar(iAsistencia);
        }

        [Route("agregarMarcacion"), HttpPost]
        public int AgregarMarca([FromBody] parMarca diaEmpleado)
        {
            nominusAppEntities dbA = new nominusAppEntities();
            uladechEntities dbU = new uladechEntities();
            if (diaEmpleado.cEstablecimiento == null) { diaEmpleado.cEstablecimiento = "0000"; }
            if (diaEmpleado.sDispositivo == null) { diaEmpleado.sDispositivo = 4; }
            authServidor _srv = dbA.authServidor.Find((short)8);
            nominaEntities db = new nominaEntities(_srv.xConexion);
            short sEstablecimiento = (short)dbU.DEPARTMENTS.FirstOrDefault(c => c.code == diaEmpleado.cEstablecimiento).DEPTID;

            asi_Marcacion _nvaMarca = new asi_Marcacion
            {
                iPersona = diaEmpleado.iEmpleado,
                dHora = diaEmpleado.dDia,
                sDispositivo = diaEmpleado.sDispositivo,
                sEstablecimiento = sEstablecimiento
            };
            db.asi_Marcacion.Add(_nvaMarca);
            db.SaveChanges();
            return _nvaMarca.iMarcacion;
        }

        [Route("agregarMarcaQR"), HttpPost]
        public async Task<string> AgregarMarcaQR([FromBody] parMarcaQR marcaQR)
        {
            nominusAppEntities dbA = new nominusAppEntities();
            authServidor _srv = dbA.authServidor.Find((short)8);
            nominaEntities db = new nominaEntities(_srv.xConexion);
            string hashLocalUtc = Token.getToken(marcaQR.aula + "&&uladech-asistencia", 128, lUTC: true);
            string hashLocal = Token.getToken(marcaQR.aula + "&&uladech-asistencia", 128, lUTC: false);
            string rsp = "Intente de nuevo por favor...";
            if (marcaQR.hash == hashLocal || marcaQR.hash == hashLocalUtc || true)
            {
                rsp = await db.asi_agregarMarcaQR(marcaQR.aula, marcaQR.hora, marcaQR.iAsistencia);
            }
            return rsp;
        }


        [Route("agregaMarca"), HttpPost]
        public async Task<RspDiarioEmpleado> AgregaMarca([FromBody] parMarca diaEmpleado)
        {
            nominusAppEntities dbA = new nominusAppEntities();
            uladechEntities dbU = new uladechEntities();
            if (diaEmpleado.cEstablecimiento == null) { diaEmpleado.cEstablecimiento = "0000"; }
            if (diaEmpleado.sDispositivo == null) { diaEmpleado.sDispositivo = 4; }
            authServidor _srv = dbA.authServidor.Find((short)8);
            nominaEntities db = new nominaEntities(_srv.xConexion);

            short sEstablecimiento = (short)dbU.DEPARTMENTS.FirstOrDefault(c => c.code == diaEmpleado.cEstablecimiento).DEPTID;

            asi_Marcacion _nvaMarca = new asi_Marcacion
            {
                iPersona = diaEmpleado.iEmpleado,
                dHora = diaEmpleado.dDia,
                sDispositivo = diaEmpleado.sDispositivo,
                sEstablecimiento = sEstablecimiento
            };
            db.asi_Marcacion.Add(_nvaMarca);
            db.SaveChanges();
            parDiaEmpleado pDE = new parDiaEmpleado { iEmpleado = diaEmpleado.iEmpleado, dDia = diaEmpleado.dDia };
            RspDiarioEmpleado de = await asiDiarioEmpleado(pDE);
            string dd = (await db.asi_Revaluar(de.oAsistencia.iAsistencia)).ToString();
            return de;
        }

        [Route("papeleta"), HttpPost]
        public async Task<NuevaPapeleta> CrearPapeleta([FromBody] NuevaPapeleta creaPap)
        {
            nominusAppEntities dbA = new nominusAppEntities();
            authServidor _srv = dbA.authServidor.Find((short)8);
            nominaEntities db = new nominaEntities(_srv.xConexion);
            int? proxPapeleta = db.asi_Papeleta.Where(c => c.bTipoPapeleta == creaPap.papeleta.bTipoPapeleta).Max(c => c.iPapeleta);
            if (proxPapeleta == null) proxPapeleta = 1; else proxPapeleta++;
            creaPap.papeleta.iPapeleta = (int)proxPapeleta;
            creaPap.resolucion.ForEach(c => c.iPapeleta = (int)proxPapeleta);
            creaPap.flujo.iPapeleta = (int)proxPapeleta;
            db.asi_Papeleta.Add(creaPap.papeleta);
            db.SaveChanges();
            creaPap.resolucion.ForEach(c =>
            {
                db.asi_Resolucion.Add(c);
            });
            db.asi_FlujoPapeleta.Add(creaPap.flujo);
            db.SaveChanges();
            await db.asi_CompletarPapeleta(creaPap.papeleta.bTipoPapeleta, creaPap.papeleta.iPapeleta);
            return creaPap;
        }


        [Route("diarioIncidencias"), HttpPost]
        public async Task<List<asi_DiarioIncidencias>> DiarioIncidencias([FromBody] ParDiarioIncidencias parDiarioIncidencias)
        {
            nominusAppEntities dbA = new nominusAppEntities();
            authServidor _srv = dbA.authServidor.Find((short)8);
            nominaEntities db = new nominaEntities(_srv.xConexion);
            return (await db.asi_DiarioIncidencias(parDiarioIncidencias.dFecha, parDiarioIncidencias.sEstablecimiento, parDiarioIncidencias.xTitulo, parDiarioIncidencias.cGenero)).ToList();
        }

        [Route("diarioEstablecimientos/{dia}"), HttpGet]
        public async Task<List<asi_DiarioEstablecimientos>> DiarioEstablecimientos([FromUri] DateTime dia)
        {
            nominusAppEntities dbA = new nominusAppEntities();
            authServidor _srv = dbA.authServidor.Find((short)8);
            nominaEntities db = new nominaEntities(_srv.xConexion);
            return (await db.asi_DiarioEstablecimientos(dia)).ToList();
        }

        [Route("diarioAsistencia"), HttpPost]
        public async Task<rspDiarioAsistencia> DiarioAsistencia([FromBody] parDiarioEstablecimiento parDiario)
        {
            nominusAppEntities dbA = new nominusAppEntities();
            rspDiarioAsistencia rsp = new rspDiarioAsistencia();
            authServidor _srv = dbA.authServidor.Find((short)8);
            nominaEntities db = new nominaEntities(_srv.xConexion);
            rsp.controles = (await db.asi_DiarioControlesAsync(parDiario.dDia, parDiario.sEstablecimiento)).ToList();
            rsp.asistencia = (await db.asi_DiarioAsistenciaAsync(parDiario.dDia, parDiario.sEstablecimiento)).ToList();
            rsp.flujo = (await db.asi_DiarioFlujoAsync(parDiario.dDia, parDiario.sEstablecimiento)).ToList();
            return rsp;
        }


        [Route("indicePapeletas/{iPersona}"), HttpGet]
        public async Task<List<asi_IndicePapeletas>> IndicePapeletas([FromUri] int iPersona)
        {
            nominusAppEntities dbA = new nominusAppEntities();
            authServidor _srv = dbA.authServidor.Find((short)8);
            nominaEntities db = new nominaEntities(_srv.xConexion);
            return (await db.asi_IndicePapeletas(iPersona)).OrderByDescending(c => c.dCreacion).ToList();
        }

        [Route("flujoPapeleta"), HttpPost]
        public async Task<List<asi_detFlujoPapeleta>> FlujoPapeleta([FromBody] ParPapeleta papeleta)
        {
            nominusAppEntities dbA = new nominusAppEntities();
            authServidor _srv = dbA.authServidor.Find((short)8);
            nominaEntities db = new nominaEntities(_srv.xConexion);
            return (await db.asi_detFlujoPapeletaAsync(papeleta.bTipoPapeleta, papeleta.iPapeleta)).ToList();
        }

        [Route("resolucion"), HttpPost]
        public async Task<List<asi_detResolucion>> Resolucion([FromBody] ParPapeleta papeleta)
        {
            nominusAppEntities dbA = new nominusAppEntities();
            authServidor _srv = dbA.authServidor.Find((short)8);
            nominaEntities db = new nominaEntities(_srv.xConexion);
            return (await db.asi_detResolucionAsync(papeleta.bTipoPapeleta, papeleta.iPapeleta)).ToList();
        }
    }

    public class RangoFechas
    {
        public DateTime? dDesde { get; set; }
        public DateTime? dHasta { get; set; }
    }

    public class ParPlanos : RangoFechas
    {
        public List<string> empleados { get; set; }
    }

    public class NuevaPapeleta
    {
        public List<asi_Resolucion> resolucion { get; set; }
        public asi_Papeleta papeleta { get; set; }
        public asi_FlujoPapeleta flujo { get; set; }
    }

    public class parMarcaQR
    {
        public int iAsistencia { get; set; }
        public string aula { get; set; }
        public DateTime hora { get; set; }
        public string hash { get; set; }
    }

    public class RspGrafMarcacion
    {
        public List<DEPARTMENTS> establecimientos { get; set; }
        public List<asi_grafMarcado> grafMarcados { get; set; }
    }

    public class ParPapeleta
    {
        public byte bTipoPapeleta { get; set; }
        public int iPapeleta { get; set; }
    }

    public class ParDiarioIncidencias
    {
        public DateTime dFecha { get; set; }
        public short sEstablecimiento { get; set; }
        public string xTitulo { get; set; }
        public string cGenero { get; set; }
    }

    public class RspDiarioAsistencia : asi_Asistencia
    {
        public List<asi_FlujoDiarioEmpleado> aFlujo { get; set; }
    }

    public class RspDiarioEmpleado
    {
        public RspDiarioAsistencia oAsistencia { get; set; }
        public List<asi_MarcacionDiarioEmpleado> aMarcacion { get; set; }
        public OuchEmpleado empleado { get; set; }
    }


    public class TuplaControl
    {
        public int? iAsistencia { get; set; }
        public short? bControl { get; set; }
    }

    public class parMarca : parDiaEmpleado
    {
        public short? sDispositivo { get; set; }
        public string cEstablecimiento { get; set; }
    }

    public class parDiaEmpleado
    {
        public int iEmpleado { get; set; }
        public DateTime dDia { get; set; }
    }

    public class parDiarioEstablecimiento
    {
        public short sEstablecimiento { get; set; }
        public DateTime dDia { get; set; }
    }

    public class rspDiarioAsistencia
    {
        public List<asi_DiarioControles> controles { get; set; }
        public List<asi_DiarioAsistencia> asistencia { get; set; }
        public List<asi_DiarioFlujo> flujo { get; set; }
    }

}

