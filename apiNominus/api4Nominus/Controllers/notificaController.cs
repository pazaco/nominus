﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using api4Nominus.Models;
using api4Nominus.Models.auth;
using api4Nominus.Models.nominus;

namespace api4Nominus.Controllers
{

    [RoutePrefix("notificaciones")]
    public class notificaController : ApiController
    {


        [Route("calculos")]
        public async Task<List<belCalculosxPeriodo>> GetBelCalculosxPeriodos()
        {
            nominaEntities db = new nominaEntities();

            string SesionPersona = HttpContext.Current.Request.Headers["sesionPersona"];
            string SesionGestor = HttpContext.Current.Request.Headers["sesionGestor"];
            Guid? gSesionPersona = null;
            Guid? gSesionGestor = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            if (SesionGestor != null && SesionGestor != "") gSesionGestor = new Guid(SesionGestor);
            return (await db.belCalculosxPeriodo((Guid)gSesionPersona)).ToList();
        }

        [Route("notificar")]
        public async Task<List<BigWebNotificar>> PostNotificar()
        {
            nominusAppEntities dbA = new nominusAppEntities();
            List<BigWebNotificar> rsp = new List<BigWebNotificar>();
            string SesionPersona = HttpContext.Current.Request.Headers["sesionPersona"];
            Guid? gSesionPersona = null;
            if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
            List<AuthServidorUsuario> _servidoresUsr = (await dbA.authServidoresUsuario((Guid)gSesionPersona)).ToList();
            foreach (AuthServidorUsuario _srv in _servidoresUsr)
            {
                nominaEntities dbD = new nominaEntities(_srv.xConexion);

                List<webNotificar> _notifs = (await dbD.webNotificar(_srv.iPersona)).ToList();

                rsp.AddRange(_notifs.Select(s => new BigWebNotificar
                {
                    sServidor = _srv.sServidor,
                    iNotificacion = s.iNotificacion,
                    dEnvio = s.dEnvio,
                    bTipoNotificacion = s.bTipoNotificacion,
                    iReferencia = s.iReferencia,
                    iContrato = s.iContrato,
                    xAsunto = s.xAsunto,
                    xFaIcono = s.xFaIcono,
                    dLeido = s.dLeido,
                    dFirmado = s.dFirmado,
                    xUrl = s.xUrl
                }));
            }
            return rsp;
        }

    }

    public class BigWebNotificar : webNotificar
    {
        public short sServidor { get; set; }
    }

}
