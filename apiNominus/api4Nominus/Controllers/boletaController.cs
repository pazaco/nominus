﻿using api4Nominus.Models;
using api4Nominus.Models.auth;
using api4Nominus.Models.nominus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace api4Nominus.Controllers
{

    [RoutePrefix("boleta")] public class BoletaController : ApiController
    {

        //    [Route("cabecera")]
        //    public List<remBoletaCabecera> PostCabecera([FromBody] ParCabecera parCabecera)
        //    {
        //        nominaEntities db = new nominaEntities();
        //        string SesionPersona = HttpContext.Current.Request.Headers["sesionPersona"];
        //        string SesionGestor = HttpContext.Current.Request.Headers["sesionGestor"];
        //        Guid? gSesionPersona = null;
        //        Guid? gSesionGestor = null;
        //        if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
        //        if (SesionGestor != null && SesionGestor != "") gSesionGestor = new Guid(SesionGestor);

        //        return db.remBoletaCabecera(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero).ToList();
        //    }

        //    [Route("data2")]
        //    public DataBoleta PostDataBoleta2([FromBody] ParCabecera parCabecera)
        //    {
        //        string SesionPersona = HttpContext.Current.Request.Headers["sesionPersona"];
        //        string SesionGestor = HttpContext.Current.Request.Headers["sesionGestor"];
        //        Guid? gSesionPersona = null;
        //        Guid? gSesionGestor = null;
        //        if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
        //        if (SesionGestor != null && SesionGestor != "") gSesionGestor = new Guid(SesionGestor);
        //        return remBoleta(parCabecera);
        //    }

        //    [Route("data")]
        //    public DataBoleta PostDataBoleta([FromBody] ParCabecera parCabecera)
        //    {
        //        nominusAppEntities dbA = new nominusAppEntities();
        //        string SesionPersona = HttpContext.Current.Request.Headers["sesionPersona"];
        //        Guid? gSesionPersona = null;
        //        if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
        //        JavaScriptSerializer jSer = new JavaScriptSerializer();
        //        authServidor _srv = dbA.authServidor.Find(parCabecera.sServidor);
        //        nominaEntities db_ = new nominaEntities(_srv.xConexion);
        //        remCalculoPlanilla calculo = db_.remCalculoPlanilla.Find(parCabecera.iCalculoPlanilla);
        //        traContrato contrato = db_.traContrato.Find(calculo.iContrato);
        //        DataBoleta datas = new DataBoleta();
        //        datas.cabecera = db_.remBoletaCabecera(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero).ToList();
        //        datas.asistencia = db_.remBoletaCabeceraAsistenciaFechas(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero).ToList();
        //        datas.datosCol1 = db_.remBoletaCabeceraDatos(parCabecera.iCalculoPlanilla, 1).ToList();
        //        datas.datosCol2 = db_.remBoletaCabeceraDatos(parCabecera.iCalculoPlanilla, 2).ToList();
        //        datas.empresa = db_.remBoletaCabeceraEmpresa(contrato.sEmpresa).ToList();
        //        datas.titulo = db_.remBoletaCabeceraTitulo(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero).ToList();
        //        datas.concepto1 = db_.remBoletaConcepto(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero, 1).ToList();
        //        datas.concepto2 = db_.remBoletaConcepto(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero, 2).ToList();
        //        datas.concepto3 = db_.remBoletaConcepto(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero, 3).ToList();
        //        datas.concepto4 = db_.remBoletaConcepto(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero, 4).ToList();
        //        datas.pdf = Pdfs.Boleta(datas, parCabecera, _srv.xConexion);
        //        return datas;
        //    }




        //    [Route("cabeceraAsistenciaFechas")]
        //    public List<remBoletaCabeceraAsistenciaFechas> PostCabeceraAsistFechas([FromBody] ParCabecera parCabecera)
        //    {
        //        nominaEntities db = new nominaEntities();
        //        string SesionPersona = HttpContext.Current.Request.Headers["sesionPersona"];
        //        string SesionGestor = HttpContext.Current.Request.Headers["sesionGestor"];
        //        Guid? gSesionPersona = null;
        //        Guid? gSesionGestor = null;
        //        if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
        //        if (SesionGestor != null && SesionGestor != "") gSesionGestor = new Guid(SesionGestor);
        //        return db.remBoletaCabeceraAsistenciaFechas(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero).ToList();
        //    }

        //    [Route("cabeceraEmpresa/{sEmpresa}")]
        //    public List<string> GetCabeceraEmpresa([FromUri]  short sEmpresa) {
        //        nominaEntities db = new nominaEntities();
        //        List<string> xCabecera = db.remBoletaCabeceraEmpresa(sEmpresa).ToList();
        //        return xCabecera;
        //    }

        //    [Route("cabeceraTitulo")]
        //    public List<string> PostCabeceraTitulo([FromBody] ParCabecera parCabecera)
        //    {
        //        nominaEntities db = new nominaEntities();
        //        List<string> respuesta = new List<string>();   //  []
        //        string SesionPersona = HttpContext.Current.Request.Headers["sesionPersona"];
        //        string SesionGestor = HttpContext.Current.Request.Headers["sesionGestor"];
        //        Guid? gSesionPersona = null;
        //        Guid? gSesionGestor = null;
        //        if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
        //        if (SesionGestor != null && SesionGestor != "") gSesionGestor = new Guid(SesionGestor);
        //        int iPersona = (int)db.perAccesoData(gSesionPersona, gSesionGestor).FirstOrDefault();
        //        if (iPersona >= 0)
        //        {
        //            respuesta = db.remBoletaCabeceraTitulo(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero).ToList();
        //        }
        //        return respuesta;
        //    }

        //    [Route("cabeceraDatos")]
        //    public List<remBoletaCabeceraDatos> PostCabeceraDatos([FromBody] ParCabeceraDatos parCabecera)
        //    {
        //        nominaEntities db = new nominaEntities();
        //        return db.remBoletaCabeceraDatos(parCabecera.iCalculoPlanilla, parCabecera.bColumna).ToList();
        //    }

        //    [Route("concepto")]
        //    public List<remBoletaConcepto> PostConcepto([FromBody] ParBoletaConcepto parBoletaConcepto)
        //    {
        //        nominaEntities db = new nominaEntities();
        //        List<remBoletaConcepto> respuesta = db.remBoletaConcepto(parBoletaConcepto.iCalculoPlanilla, parBoletaConcepto.bMes, parBoletaConcepto.bNumero, parBoletaConcepto.bClaseConcepto).ToList();
        //        return respuesta;
        //    }


        //    [Route("calculos")]
        //    public List<belCalculosxPeriodo> GetBelCalculosxPeriodos()
        //    {
        //        nominaEntities db = new nominaEntities();
        //        string SesionPersona = HttpContext.Current.Request.Headers["sesionPersona"];
        //        string SesionGestor = HttpContext.Current.Request.Headers["sesionGestor"];
        //        Guid? gSesionPersona = null;
        //        Guid? gSesionGestor = null;
        //        if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
        //        if (SesionGestor != null && SesionGestor != "") gSesionGestor = new Guid(SesionGestor);
        //        return db.belCalculosxPeriodo(gSesionPersona).ToList();
        //    }

        //    [Route("indice")]
        //    public IndiceBoletas getIndice()
        //    {
        //        nominaEntities db = new nominaEntities();
        //        string SesionPersona = HttpContext.Current.Request.Headers["sesionPersona"];
        //        string SesionGestor = HttpContext.Current.Request.Headers["sesionGestor"];
        //        Guid? gSesionPersona = null;
        //        Guid? gSesionGestor = null;
        //        if (SesionPersona != null && SesionPersona != "") gSesionPersona = new Guid(SesionPersona);
        //        if (SesionGestor != null && SesionGestor != "") gSesionGestor = new Guid(SesionGestor);
        //        int iPersona = db.autSesion.FirstOrDefault(se => se.gSesion == gSesionPersona).iUsuario;
        //        IndiceBoletas indice = new IndiceBoletas();
        //        if (iPersona >= 0)
        //        {
        //            indice.contratos = db.traContratosxPersona(iPersona).ToList();
        //            indice.calculos = db.belCalculos(gSesionPersona).ToList();
        //        }
        //        return indice;
        //    }

        //    private static DataBoleta remBoleta(ParCabecera parCabecera)
        //    {
        //        nominaEntities db = new nominaEntities();

        //        remCalculoPlanilla calculo = db.remCalculoPlanilla.Find(parCabecera.iCalculoPlanilla);
        //        traContrato contrato = db.traContrato.Find(calculo.iContrato);
        //        DataBoleta datas = new DataBoleta();
        //        datas.cabecera = db.remBoletaCabecera(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero).ToList();
        //        datas.asistencia = db.remBoletaCabeceraAsistenciaFechas(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero).ToList();
        //        datas.datosCol1 = db.remBoletaCabeceraDatos(parCabecera.iCalculoPlanilla, 1).ToList();
        //        datas.datosCol2 = db.remBoletaCabeceraDatos(parCabecera.iCalculoPlanilla, 2).ToList();
        //        datas.empresa = db.remBoletaCabeceraEmpresa(contrato.sEmpresa).ToList();
        //        datas.titulo = db.remBoletaCabeceraTitulo(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero).ToList();
        //        datas.concepto1 = db.remBoletaConcepto(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero, 1).ToList();
        //        datas.concepto2 = db.remBoletaConcepto(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero, 2).ToList();
        //        datas.concepto3 = db.remBoletaConcepto(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero, 3).ToList();
        //        datas.concepto4 = db.remBoletaConcepto(parCabecera.iCalculoPlanilla, parCabecera.bMes, parCabecera.bNumero, 4).ToList();
        //        datas.pdf = Pdfs.Boleta(datas,parCabecera);
        //        return datas;
        //    }

        //}

        //public class ParCabecera
        //{
        //    public byte bMes { get; set; }
        //    public byte bNumero { get; set; }
        //    public int iCalculoPlanilla { get; set; }
        //    public Nullable<short> sServidor { get; set; }
        //}

        //public class ParBoletaConcepto : ParCabecera
        //{
        //    public byte bClaseConcepto { get; set; }
        //}

        //public class ParCabeceraDatos
        //{
        //    public byte bColumna { get; set; }
        //    public int iCalculoPlanilla { get; set; }
        //}

        //public class IndiceBoletas
        //{
        //    public List<traContratosxPersona> contratos { get; set; }
        //    public List<belCalculos> calculos { get; set; }
        //}

        //public class DataBoleta
        //{
        //    public List<remBoletaCabecera> cabecera { get; set; }
        //    public List<remBoletaCabeceraAsistenciaFechas> asistencia { get; set; }
        //    public List<remBoletaCabeceraDatos> datosCol1 { get; set; }
        //    public List<remBoletaCabeceraDatos> datosCol2 { get; set; }
        //    public List<string> empresa { get; set; }
        //    public List<string> titulo { get; set; }
        //    public List<remBoletaConcepto> concepto1 { get; set; }
        //    public List<remBoletaConcepto> concepto2 { get; set; }
        //    public List<remBoletaConcepto> concepto3 { get; set; }
        //    public List<remBoletaConcepto> concepto4 { get; set; }
        //    public outBoleta pdf { get; set; }
        }

}

