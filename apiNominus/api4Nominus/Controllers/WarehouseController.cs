﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using api4Nominus.Models;
using api4Nominus.Models.auth;

namespace api4Nominus.Controllers
{
    [RoutePrefix("warehouse")]
    public class WarehouseController : ApiController
    {
        [Route("servidores/{iPersona}")]
        public async Task<List<authServidor>> GetServidoresPersona([FromUri] int iPersona) => await WareHouse.ServerxPersona(iPersona);

        [Route("servidores")]
        public async Task<List<authServidor>> PostServidoresEmail([FromBody] string xEmail) => await WareHouse.ServerxEmail(xEmail);

        [Route("contratos")]
        public async Task<List<aut_Contratos>> PosContratos([FromBody] string xEmail) => await WareHouse.Contratos(xEmail);
    }
}
