﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using clkNominus.Model;
using clkNominus.zkTech;
using Microsoft.EntityFrameworkCore;

namespace clkNominus.Metodos
{
    class Dispositivos
    {

        public static List<EstadoDispositivo> devices = null;
        public static Guid gAmbito;

        public Dispositivos(Guid gAmb)
        {
            gAmbito = gAmb;
            using (GeneralContext dbg = new GeneralContext())
            {
                List<marDevice> _devs = dbg.marDevice.Where(c => c.gAmbito == gAmbito).ToList();
                devices = _devs.Select(c => new EstadoDispositivo
                {
                    iDevice = c.iDevice,
                    gAmbito = c.gAmbito,
                    xIP = c.xIP,
                    iPort = c.iPort
                }).ToList();
            }
        }

        public static void PrepararDispositivos()
        {
            using (GeneralContext dbg = new GeneralContext())
            {
                if (devices == null)
                {
                    List<marDevice> _devs = dbg.marDevice.Where(c => c.gAmbito == gAmbito).ToList();
                    devices = _devs.Select(c => new EstadoDispositivo
                    {
                        iDevice = c.iDevice,
                        gAmbito = c.gAmbito,
                        xIP = c.xIP,
                        iPort = c.iPort
                    }).ToList();
                }

                List<ZkClient> zkDevices = new List<ZkClient>();

                foreach(EstadoDispositivo _cest in devices)
                {
                    bool isValidIpA = UniversalStatic.ValidateIP(_cest.xIP);
                    if (isValidIpA) {
                        if (!_cest.lConectado)
                        {
                            ZkClient zkCli = new ZkClient();
                            


                        }

                    } else
                    {
                        if (_cest.lConectado)
                        {
                            _cest.lConectado = false;
                        }
                    }

                    if (_cest.lConectado)
                    {

                    }
                }

                


            }


            List<marOperacion> operaciones = Procs.marOperacionesPendientes(gAmbito);
            Console.WriteLine($"Ambito {gAmbito}");


            foreach (marOperacion op in operaciones)
            {
                if (op.sOperacion == 1)
                {
                    bool conectado = ChequearDevice("192.168.0.8", 4370);
                }

            }

        }

        private static bool ChequearDevice(string xIp, int iDevice)
        {


            return true;

        }

        public class EstadoDispositivo : marDevice
        {
            public EstadoDispositivo() {
                cSerial = "";
                lConectado = false;
                lHuella = false;
                lRostro = false;
                lPalma = false;
                lVena = false;
                lRetina = false;
                lVoz = false;
                }
            public string cSerial { get; set; }
            public bool lConectado { get; set; }
            public bool lHuella { get; set; }
            public bool lRostro { get; set; }
            public bool lPalma { get; set; }
            public bool lVena { get; set; }
            public bool lRetina { get; set; }
            public bool lVoz { get; set; }

        }

    }
}
