﻿using clkNominus.Model;
using clkNominus.Model.Devs;
using FluentScheduler;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;

namespace clkNominus
{
    class Program
    {

        public static Guid gAmbito;
        public static int iCliente;
        static void Main()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            IConfigurationRoot configuration = builder.Build();

            Guid gAmbito = Guid.Parse(configuration.GetValue<string>("Config:Ambito"));

            JobManager.Initialize(new NominusTimer());
            ConsoleKeyInfo tecla = Console.ReadKey();
            while (tecla.Modifiers.ToString() != "Control" || tecla.Key.ToString()!="S")
            {
                tecla = Console.ReadKey();
            }

        }


    }
}
