﻿using FluentScheduler;
using System;

namespace clkNominus
{
    class NominusTimer : Registry
    {

        public NominusTimer()
        {
            Schedule(() =>
            {
                Console.WriteLine("Marcaciones {0:HH:mm:ss}", DateTime.Now);
            }).ToRunNow().AndEvery(1).Minutes();

            Schedule(() =>
            {
                Program.NuevasOperaciones();
            }).ToRunNow().AndEvery(4).Seconds();
        }
    }
}
