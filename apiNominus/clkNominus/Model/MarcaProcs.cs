﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace clkNominus.Model
{
    public partial class Procs
    {
        public static List<marOperacion> marOperacionesPendientes(Guid gAmbito)
        {
            List<marOperacion> resp = new List<marOperacion>();
            using (GeneralContext dbd = new GeneralContext()){
                resp = dbd.marOperacion.Where(c => c.gAmbito == gAmbito && c.dEjecutada == null).ToList();
            }
            return resp;
        }
    }

    #region Outputs
    #endregion

    #region Inputs

    #endregion
}
