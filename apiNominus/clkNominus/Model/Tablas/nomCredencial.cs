﻿using System;
using System.Collections.Generic;

namespace clkNominus.Model
{
    public partial class nomCredencial
    {
        public short sCredencial { get; set; }
        public string xServerName { get; set; }
        public string xPrefijo { get; set; }
        public string xUserId { get; set; }
        public byte? bModeloSeguro { get; set; }
    }
}
