﻿using System;

namespace clkNominus.Model
{
    public partial class nomAmbito
    {
        public Guid gAmbito { get; set; }
        public short sCliente { get; set; }
        public short sSede { get; set; }
    }
}
