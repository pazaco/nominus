﻿using System;
using System.Collections.Generic;

namespace clkNominus.Model
{
    public partial class marOperacion
    {
        public int iOperacion { get; set; }
        public Guid gAmbito { get; set; }
        public DateTime? dEjecutada { get; set; }
        public short? sOperacion { get; set; }
        public string xParametros { get; set; }
    }
}
