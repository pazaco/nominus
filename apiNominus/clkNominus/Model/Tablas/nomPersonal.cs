﻿using System;
using System.Collections.Generic;

namespace clkNominus.Model
{
    public partial class nomPersonal
    {
        public int iPersonaNominus { get; set; }
        public string xCorreo { get; set; }
        public byte? bPerfil { get; set; }
    }
}
