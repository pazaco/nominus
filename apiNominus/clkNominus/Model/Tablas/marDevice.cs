﻿using System;

namespace clkNominus.Model
{
    public partial class marDevice
    {
        public int iDevice { get; set; }
        public Guid gAmbito { get; set; }
        public string xIP { get; set; }
        public int iPort { get; set; }
    }
}
