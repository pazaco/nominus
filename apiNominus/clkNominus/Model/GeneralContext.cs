﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace clkNominus.Model
{
    public partial class GeneralContext : DbContext
    {
        public GeneralContext()
        {
        }

        public GeneralContext(DbContextOptions<GeneralContext> options)
            : base(options)
        {
        }

        public virtual DbSet<nomCliente> nomCliente { get; set; }
        public virtual DbSet<nomAmbito> nomAmbito { get; set; }
        public virtual DbSet<nomConexion> nomConexion { get; set; }
        public virtual DbSet<nomCredencial> nomCredencial { get; set; }
        public virtual DbSet<nomTipoDb> nomTipoDb { get; set; }
        public virtual DbSet<nomBuilder> nomBuilder { get; set; }
        public virtual DbSet<nomPersonal> nomPersonal { get; set; }
        public virtual DbSet<nomPersonalPerfil> nomPersonalPerfil { get; set; }
        public virtual DbSet<marOperacion> marOperacion { get; set; }
        public virtual DbSet<marDevice> marDevice { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                var builder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
                IConfigurationRoot configuration = builder.Build();
 
                optionsBuilder.UseSqlServer(configuration.GetConnectionString("general"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<nomCliente>(entity =>
            {
                entity.HasKey(e => e.sCliente);

                entity.Property(e => e.sCliente).ValueGeneratedNever();

                entity.Property(e => e.xCliente)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<nomAmbito>(entity => {
                entity.HasKey(c => c.gAmbito);
            });

            modelBuilder.Entity<nomConexion>(entity =>
            {
                entity.HasKey(e => e.sConexion);

                entity.Property(e => e.bTipoDb).HasDefaultValueSql("((3))");

                entity.Property(e => e.xNombreDb)
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<nomCredencial>(entity =>
            {
                entity.HasKey(e => e.sCredencial);

                entity.Property(e => e.xPrefijo)
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.xServerName)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.xUserId)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<nomTipoDb>(entity =>
            {
                entity.HasKey(e => e.bTipoDb);

                entity.Property(e => e.xTipoDb)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<nomBuilder>(entity =>
            {
                entity.HasKey(e => e.iBuilder);

                entity.Property(e => e.cBuilder)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.jBuilder)
                    .IsRequired()
                    .IsUnicode(false);
            });

            modelBuilder.Entity<nomPersonal>(entity =>
            {
                entity.HasKey(e => e.iPersonaNominus);

                entity.Property(e => e.xCorreo)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<nomPersonalPerfil>(entity =>
            {
                entity.HasKey(e => e.bPerfil);

                entity.Property(e => e.xPerfil)
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<marDevice>(entity =>
            {
                entity.HasKey(e => e.iDevice);
            });

            modelBuilder.Entity<marOperacion>(entity =>
            {
                entity.HasKey(e => e.iOperacion);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
